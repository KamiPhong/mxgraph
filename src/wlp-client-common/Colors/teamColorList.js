const teamListColor = [
    {color: "bg-red"}, {color: "bg-orange"}, 
    {color: "bg-yellow"}, {color: "bg-green"}, 
    {color: "bg-blue"}, {color: "bg-indigo"}, 
    {color: "bg-gray-dark"}, {color: "bg-dark-op"} 
];

const colorTeamDefault = "bg-dark-op";

module.exports = {
    teamListColor, 
    colorTeamDefault
};