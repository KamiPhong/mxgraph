import React, { Component } from 'react';
import KeyCodeConst from 'wlp-client-editor-lib/utils/KeyCodeConst';
class TabContainer extends Component {
    constructor(props) {
        super(props);
        this.containerRef = React.createRef();
        this.elsTabindex = [];
        this.currentTab = -1;
    }

    componentDidMount() {
        this.setElementTabIndex();
        document.addEventListener('keydown', this.onKeyDownHandler);
    }

    setElementTabIndex = (node) => {
        const els = this.containerRef.querySelectorAll('[tabindex]');
        this.elsTabindex = Array.from(els);

        this.sortTabIndex();
    }

    sortTabIndex = () => {
        const elsTabLength = this.elsTabindex.length;
        for (let i = 0; i < elsTabLength - 1; i++) {
            for (let j = 0; j < elsTabLength - i - 1; j++) {
                if (this.elsTabindex[j].getAttribute('tabindex') > this.elsTabindex[j + 1].getAttribute('tabindex')) {
                    const temp = this.elsTabindex[j];
                    this.elsTabindex[j] = this.elsTabindex[j + 1];
                    this.elsTabindex[j + 1] = temp;
                }
            }
        }
    }

    onKeyDownHandler = (evt) => {
        if (evt.shiftKey && evt.keyCode === KeyCodeConst.TAB) {
            this.onShiftTabHandler(evt);
        } else if (evt.keyCode === KeyCodeConst.TAB) {
            this.onTabHandler(evt);
        }
    }

    onTabHandler = (evt) => {
        evt.preventDefault();
        this.setCurrentTabActiveElement();

        let nextTab = 0;

        if (this.currentTab + 1 !== this.elsTabindex.length) {
            nextTab = this.currentTab + 1;
        }

        this.elsTabindex[nextTab].focus();
        if (typeof this.elsTabindex[nextTab].select === 'function') {
            this.elsTabindex[nextTab].select();
        }

        this.currentTab = nextTab;
    }

    onShiftTabHandler = (evt) => {
        evt.preventDefault();
        this.setCurrentTabActiveElement();

        let prevTab = this.elsTabindex.length - 1;
        if (this.currentTab !== 0) {
            prevTab = this.currentTab - 1;
        }

        this.elsTabindex[prevTab].focus();
        if (typeof this.elsTabindex[prevTab].select === 'function') {
            this.elsTabindex[prevTab].select();
        }
    }

    setCurrentTabActiveElement = () => {
        for (let i = 0; i < this.elsTabindex.length; i++) {
            if (this.elsTabindex[i] === document.activeElement) {
                this.currentTab = i;
                break;
            }
        }
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.onKeyDownHandler);
    }

    render() {
        return (
            <div ref={r => this.containerRef = r}>
                {this.props.children}
            </div>
        );
    }
}

export default TabContainer;