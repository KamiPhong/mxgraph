import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';

import ViewTypes from 'wlp-client-mypage-lib/consts/ViewTypes';
import reloadButton  from 'wlp-client-common/images/icon/ic-reloadbutton.svg';
import Link from '../Link';
import ActionLogger from 'wlp-client-common/ActionLogger';

class RefreshButtonSkin extends Component {

    refreshMapList = () => {
        ActionLogger.click('User_Mypage_TopMenu', 'Button_Refresh_Map_List');
        const { callGetMapList,
            currentView,
            selectedCategoryId,
            callGetCategoryList,
            callGetTemplateList } = this.props;

        switch (currentView) {
            case ViewTypes.map:
                callGetMapList();
                break;
            case ViewTypes.template:
                callGetTemplateList(selectedCategoryId);
                callGetCategoryList();
                break;
            default: break;
        }
    }

    render() {
        return (<Fragment>
            <Link onClick={this.refreshMapList} className="mypage-btn-action cursor-df">
                <img src={reloadButton} alt="" className="img-fluid hover-opacity noselect" width="22" />
            </Link>
        </Fragment>);
    }

}

export default withTranslation()(RefreshButtonSkin);