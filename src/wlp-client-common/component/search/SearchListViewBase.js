import React, { Component } from 'react';
import SearchListRenderer from './SearchListRenderer';

class SearchListViewBase extends Component {
    componentDidMount() {
       
    }

    render() {
        const {
            searchResult,
            t,
            valueSearch,
            showSearchListModal,
            setShowSearchListModal,
        } = this.props;

    return (
            searchResult && (
            (searchResult.itemEntries && searchResult.itemEntries.length > 0) ||
            (searchResult.sheetEntries && searchResult.sheetEntries.length > 0) ||
            (searchResult.mapEntries && searchResult.mapEntries.length > 0))
            ) ? (
            <SearchListRenderer 
                t={t}
                valueSearch={valueSearch}
                searchResult={searchResult}
                showSearchListModal={showSearchListModal}
                setShowSearchListModal={setShowSearchListModal}
            />
        ) : (
            <div className="no-record">
                <h3 className="text-search-norecord">{t('search.noResults1')}</h3>
                <p className="text-secondary font-size-13">{t('search.noResults2')}</p>
            </div>
        )
    }
}

export default SearchListViewBase;
