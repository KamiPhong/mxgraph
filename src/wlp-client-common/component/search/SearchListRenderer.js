import React, { Component, Fragment } from 'react';
import SearchListViewModal from 'wlp-client-mypage-lib/components/modals/SearchListViewModal';
import rightArrowIcon2 from 'wlp-client-common/images/rightArrowIcon2@4x.png';
import grayMapIcon from 'wlp-client-common/images/grayMapIcon@4x.png';
import graySheetIcon from 'wlp-client-common/images/graySheetIcon@4x.png';
import grayItemIcon from 'wlp-client-common/images/grayItemIcon@4x.png';
import { SEARCH_RESULT_TYPE, DEFAULT_ALL_RESULT } from 'wlp-client-mypage-lib/consts/UserSetting';
import SearchConfigDto from 'wlp-client-service/dto/search/SearchConfigDto';
import SearchService from 'wlp-client-service/service/SearchService';
import Link from '../Link';
import { openEditorPage } from 'wlp-client-common/utils/EdotorUtil';
import {htmlDecode} from 'wlp-client-common/utils/TextUtils'
import ActionLogger from 'wlp-client-common/ActionLogger';

const searchService = new SearchService();
class SearchListRenderer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchList: [],
            kindView: SEARCH_RESULT_TYPE.MAPS,
            kindViewIcon: grayMapIcon,
        }

        this.modalSearch = React.createRef();
        this.refModal = React.createRef();

        window.addEventListener('resize', this.handleResize)
    }

    findReadingAllResult = async (kindView) => {
        let requestDto = new SearchConfigDto();
        let result = {};
        const { valueSearch } = this.props;
        requestDto.contractId = -1;
        requestDto.text = valueSearch;

        requestDto = {
            ...requestDto,
            "contractId": requestDto.contractId,
            "count": parseInt(DEFAULT_ALL_RESULT), // mặc định tối đa là 3 kết quả có thể chỉnh thêm
            "mapId": null,
            "start": 0,
            "text": requestDto.text 
        };

        switch(kindView) {
            case SEARCH_RESULT_TYPE.MAPS:
                result = await searchService.findFollowingMaps(requestDto);
                break;
            case SEARCH_RESULT_TYPE.SHEETS:
                result = await searchService.findFollowingSheets(requestDto);
                break;
            case SEARCH_RESULT_TYPE.ITEMS:
                result = await searchService.findFollowingItems(requestDto);
                break;
            default:
                break;
        }

        return result;
    }

    getDataResultSearchByOption = async (kindView, dataSearch) => {
        ActionLogger.click('User_Mypage_SearchResult', `Button_Show_Result_${kindView}`);
        const { setShowSearchListModal } = this.props;
        let resultAllDataSearch = {};
        switch(kindView) {
            case SEARCH_RESULT_TYPE.MAPS:
                resultAllDataSearch = await this.findReadingAllResult(SEARCH_RESULT_TYPE.MAPS);
                this.setState({kindViewIcon: grayMapIcon})
                break;
            case SEARCH_RESULT_TYPE.SHEETS:
                resultAllDataSearch = await this.findReadingAllResult(SEARCH_RESULT_TYPE.SHEETS);
                this.setState({kindViewIcon: graySheetIcon})
                break;
            case SEARCH_RESULT_TYPE.ITEMS:
                resultAllDataSearch = await this.findReadingAllResult(SEARCH_RESULT_TYPE.ITEMS);
                this.setState({kindViewIcon: grayItemIcon})
                break;
            default:
                break;
        }

        this.setState({searchList: resultAllDataSearch, kindView: kindView});
        setShowSearchListModal(true);
        this.setHeightBodyModal();
    }

    handleResize = (evt) => {
            this.setHeightBodyModal();
    }

    setHeightBodyModal = () => {
        const { showSearchListModal } = this.props;
        const modalHeader = 40;
        const windowHeightBody = window.innerHeight - 2*modalHeader - 8;
        const windowHeightModal =  window.innerHeight - 35;

        
        if (showSearchListModal) {
            this.modalSearch.current.style.height = windowHeightBody + 'px';
            this.refModal.current.style.height = windowHeightModal + 'px';
            this.refModal.current.style.borderRadius = "0px 0px 5px 5px";
            this.refModal.current.style.minHeight = '550px';
            this.modalSearch.current.style.minHeight = '550px';
        }
    }
    
    render() {
        const { searchResult, t, showSearchListModal, setShowSearchListModal, } = this.props;
        const { searchList, kindViewIcon, kindView } = this.state;

        return (<Fragment>
            {   searchResult && 
                searchResult.mapEntries && 
                searchResult.mapEntries.length > 0 &&
                (
                <div className="data-search"> 
                    <div className="left-search">
                    <div className="header-search"><span className="title-search noselect">{t('search.maps')}</span><span className="noselect">({searchResult.mapHitCount})</span></div>
                        <span className="btn-show-all-results">
                            <img alt="" className="right-arrow-icon noselect" src={rightArrowIcon2} width="5" />
                            <span onClick={() => this.getDataResultSearchByOption(SEARCH_RESULT_TYPE.MAPS, searchResult.mapEntries)}
                                className="links-show-detail popup-fontsize noselect noselect-color-aqua">{t('search.showAllResults')}</span>
                        </span>
                    </div>
                    <div className="right-search popup-fontsize">
                        { searchResult.mapEntries.map((map, index) => {
                            return (
                                <Link key={index} className="right-search-item-link" onClick={() => {
                                        ActionLogger.click('User_Mypage_SearchResult', `Open_Editor_Page_With_Map`);
                                        openEditorPage(map.mapId);
                                    }}>
                                    <div className="right-search-item">
                                        <div className="right-search-header noselect">
                                            <img alt="" className="right-arrow-icon noselect" src={grayMapIcon} width="38.8" />
                                        </div>
                                        <div className="right-search-content noselect">
                                        <span className="noselect">{t('search.map')}:{<span className="noselect" dangerouslySetInnerHTML={{ __html: map.mapName }} />}</span>
                                        </div>
                                        <div className="right-search-footer noselect">
                                            <span className="text-ls-overflow noselect">{t('search.team')}:{map.teamName}</span><br/>
                                            <span className="text-ls-overflow noselect">{t('search.owner')}:{htmlDecode(map.createUserName)}</span>
                                        </div>
                                    </div>
                                </Link>
                            )
                        })}
                    </div>
                </div>
            )}

            {   searchResult && 
                searchResult.sheetEntries && 
                searchResult.sheetEntries.length > 0 &&
                (
                <div className="data-search"> 
                    <div className="left-search">
                    <div className="header-search"><span className="title-search noselect">{t('search.sheets')}</span><span className="noselect">({searchResult.sheetHitCount})</span></div>
                        <span className="btn-show-all-results">
                            <img alt="" className="right-arrow-icon noselect" src={rightArrowIcon2} width="5" />
                            <span onClick={() => this.getDataResultSearchByOption(SEARCH_RESULT_TYPE.SHEETS, searchResult.mapEntries)}
                                className="links-show-detail popup-fontsize noselect noselect-color-aqua">{t('search.showAllResults')}</span>
                        </span>
                    </div>
                    <div className="right-search popup-fontsize">
                        { searchResult.sheetEntries.map((sheet, index) => {
                            return (
                                <Link key={index} className="right-search-item-link" onClick={() => {
                                        ActionLogger.click('User_Mypage_SearchResult', `Open_Editor_Page_With_Sheet`);
                                        openEditorPage(sheet.mapId, sheet.sheetId);
                                    }}>
                                    <div className="right-search-item">
                                        <div className="right-search-header noselect">
                                            <img alt="" className="right-arrow-icon noselect" src={graySheetIcon} width="37" />
                                        </div>
                                        <div className="right-search-content noselect">
                                            <span className="noselect">{t('search.map')}:{<span className="noselect" dangerouslySetInnerHTML={{ __html: sheet.mapName }} />}</span><br/>
                                            <span className="noselect">{t('search.sheet')}:{<span className="noselect" dangerouslySetInnerHTML={{ __html: sheet.sheetName }} />}</span>
                                        </div>
                                        <div className="right-search-footer noselect">
                                            <span className="text-ls-overflow noselect">{t('search.team')}:{sheet.teamName}</span><br/>
                                            <span className="text-ls-overflow noselect">{t('search.author')}:{htmlDecode(sheet.createUserName)}</span>
                                        </div>
                                    </div>
                                </Link>
                            )
                        })}
                    </div>
                </div>
            )}

            {   searchResult && 
                searchResult.itemEntries && 
                searchResult.itemEntries.length > 0 &&
                (
                <div className="data-search"> 
                    <div className="left-search">
                    <div className="header-search"><span className="title-search noselect">{t('search.items')}</span><span className="noselect">({searchResult.itemHitCount})</span></div>
                        <span className="btn-show-all-results noselect">
                            <img alt="" className="right-arrow-icon noselect" src={rightArrowIcon2} width="5" />
                            <span onClick={() => this.getDataResultSearchByOption(SEARCH_RESULT_TYPE.ITEMS, searchResult.mapEntries)}
                            className="links-show-detail popup-fontsize noselect noselect-color-aqua">{t('search.showAllResults')}</span>
                        </span>
                    </div>
                    <div className="right-search popup-fontsize">
                        { searchResult.itemEntries.map((item, index) => {
                            return (
                                <Link key={index} className="right-search-item-link"onClick={() => {
                                        ActionLogger.click('User_Mypage_SearchResult', `Open_Editor_Page_With_Item`);
                                        openEditorPage(item.mapId, item.sheetId, item.itemId);
                                    }}>
                                    <div className="right-search-item">
                                        <div className="right-search-header noselect">
                                            <img alt="grayItemIcon" className="right-arrow-icon noselect" src={grayItemIcon} width="38" />
                                        </div>
                                        <div className="right-search-content noselect">
                                            <span className="noselect">{t('search.map')}:{<span dangerouslySetInnerHTML={{ __html: item.mapName }} />}</span><br/>
                                            <span className="noselect">{t('search.sheet')}:{<span dangerouslySetInnerHTML={{ __html: item.sheetName }} />}</span><br/>
                                            <span className="noselect">{t('search.item')}:{<span dangerouslySetInnerHTML={{ __html: item.itemText }} />}</span>
                                        </div>
                                        <div className="right-search-footer noselect">
                                            <span className="text-ls-overflow noselect">{t('search.team')}:{item.teamName}</span><br/>
                                            <span className="text-ls-overflow noselect">{t('search.author')}:{htmlDecode(item.createUserName)}</span>
                                        </div>
                                    </div>
                                </Link>
                            )
                        })}
                    </div>
                </div>
            )}
           
           {
               showSearchListModal && 
               <SearchListViewModal 
                    refModalSearch={this.modalSearch}
                    refModal={this.refModal} 
                    t={t}
                    searchList={searchList}
                    setShowSearchListModal={setShowSearchListModal}
                    iconView={kindViewIcon}
                    kindView={kindView}
                />
           }
            
        </Fragment>);
    }
}

export default SearchListRenderer;
