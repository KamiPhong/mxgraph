import React from 'react';
import iconclose from 'wlp-client-common/images/iconclose.png';
import LoadModalBodyEnd from '../LoadModalBodyEnd';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';

class CommonModal extends React.Component {
    state = {
        show: this.props.show
    }

    modalBody = React.createRef();

    componentDidUpdate(prevProps) {
        if (
            prevProps.show !== this.props.show &&
            this.props.show !== this.state.show
        ) {
            this.setState({show: this.props.show});
        }

        document.addEventListener('click', this.onDocumentClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.onDocumentClick);
    }

    onDocumentClick = (evt) => {
        if (!checkElementClicked(evt, this.modalBody) && this.props.hideOnBlur) {
            this.onHide_selfHandle();
        }
    }

    onHide_selfHandle = () => {
        this.setState({show: false});
    }

    onBackgroundClickHandler=()=>{
        const {onBackgroundClick} = this.props;
        if( onBackgroundClick && typeof onBackgroundClick ==='function'){
            onBackgroundClick();
        }
    }

    render() {
        const {
            title,
            children,
            onHide,
            className,
            backDropClass,
            backDropModal,
            classNameImage,
            backGroundClass,
            headerClass,
            noBorderClass
        } = this.props;

        const {show} = this.state;
        let modalBodyClassName = 'card border-modal-content modal-action-content noselect noselect-color-777777';
        let itemModalBodyClassName = [modalBodyClassName];
        if (classNameImage) {
            itemModalBodyClassName.push(classNameImage);
        } else {
            itemModalBodyClassName.push('rounded-lg');
        }
        if (noBorderClass) {
            itemModalBodyClassName.push(' ')
        } else {
            itemModalBodyClassName.push('border')
        }

        return show ? (<LoadModalBodyEnd>
            <div className={`modal modal-gr d-block ${backDropModal}`} onClick={this.onBackgroundClickHandler} >
                <div className={`modal-dialog modal-dialog-centered ${className}`} role="document">
                    <div className={`modal-content ${backGroundClass ? backGroundClass : "mypage-modal-bg"}`} ref={r => this.modalBody = r}>
                        <div className={`modal-header ${headerClass ? headerClass : "p-2 border-0 pd-0"}`}>
                            <button type="button" className="close"
                                    onClick={onHide ? onHide : this.onHide_selfHandle}
                                    data-dismiss="modal" aria-label="Close">
                                <img className='noselect' src={iconclose} width="18" alt=""/>
                            </button>
                        </div>
                        <div className="modal-body pt-0">
                            <div className={itemModalBodyClassName.join(' ')}>
                                {title && <h2 className="modal-title noselect">{title}</h2>}
                                {children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={`modal-backdrop fade show ${backDropClass} backdrop-popup-blur`} />
        </LoadModalBodyEnd>) : null;
    }
}


export default CommonModal;