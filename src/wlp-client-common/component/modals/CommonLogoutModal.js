import React from 'react';
import iconclose from 'wlp-client-common/images/iconclose.png';

class CommonLogoutModal extends React.Component {
    state = {
        show: this.props.show
    }

    UNSAFE_componentWillReceiveProps({ show }) {
        this.setState({ show });
    }

    onHide_selfHandle = () => {
        this.setState({ 
            show: false
        });
    }

    render() {
        const { title, children, onHide, className, backDropClass } = this.props;
        const { show } = this.state;
        return show ? (<React.Fragment>
            <div className="modal modal-gr d-block">
                <div className={`modal-dialog modal-dialog-centered modal-logout-ed ${className}`} role="document">
                    <div className="modal-content mypage-modal-bg noselect">
                        <div className="modal-header border-0">
                            <button type="button" className="close" onClick={onHide ? onHide : this.onHide_selfHandle} data-dismiss="modal" aria-label="Close">
                                <img className="noslelect" src={iconclose} width="18" alt="" />
                            </button>
                        </div>
                        <div className="modal-body pt-0">
                            <div className="card border border rounded-lg modal-action-content">
                                {title && <h2 className="modal-title noselect">{title}</h2>}
                                {children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={`modal-backdrop fade show ${backDropClass} backdrop-popup-blur`}></div>
        </React.Fragment>) : null;
    }
}


export default CommonLogoutModal;