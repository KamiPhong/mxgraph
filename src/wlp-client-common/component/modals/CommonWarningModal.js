import React from 'react';
import iconclose from 'wlp-client-common/images/iconclose.png';
import LoadModalBodyEnd from '../LoadModalBodyEnd';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';

class CommonWarningModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: this.props.show,
        }

        this.hideOnBlur = true;
        this.modalBody = React.createRef();
    }

    componentDidUpdate(prevProps) {
        if (this.props.show !== prevProps.show) {
            this.setState({ show: this.props.show });
        }
    }

    componentDidMount() {
        document.addEventListener('click', this.onDocumentClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.onDocumentClick);
    }

    handleHideModal = () => {
        if (typeof (this.props.onHide) === 'function') {
            this.props.onHide();
        } else if (typeof(this.props.onClose) === 'function') {
            this.props.onClose();
        } else {
            this.setState({ show: false });
        }
    }

    onDocumentClick = (evt) => {
        if (!this.props.hideOnBlur) {
            this.hideOnBlur = this.props.hideOnBlur
        }
        
        if (!checkElementClicked(evt, this.modalBody) && this.hideOnBlur) {
            this._onBlur();
        }
    }

    _onBlur = () => {
        if (typeof (this.props.onHide) === 'function') {
            this.props.onHide()
        } if (typeof(this.props.onBlur) === 'function') {
            this.props.onBlur();
        } else {
            this.handleHideModal();
        }
    }

    render() {
        const { title, children, className, backDropClass, contentClassName } = this.props;
        const { show } = this.state;
        let contentClasses = ['card border border rounded-lg'];
        let classes = ['modal-dialog modal-dialog-centered modal-logout-ed modal-ct'];

        if (contentClassName) {
            contentClasses.push(contentClassName);
        }
        if (className) {
            classes.push(className);
        }
        return show ? (<LoadModalBodyEnd>
            <div className="modal modal-gr d-block zindex-1072">
                <div className={classes.join(' ')} role="document">
                    <div className="modal-content mypage-modal-bg w-s-preline" ref={r => this.modalBody = r}>
                        <div className="modal-header">
                            <button type="button" className="close" onClick={this.handleHideModal} data-dismiss="modal" aria-label="Close">
                                <img className="noselect" src={iconclose} width="18" alt="" />
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className={contentClasses.join(' ')}>
                                {title && <h2 className="modal-title noselect">{title}</h2>}
                                {children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={`modal-backdrop fade show ${backDropClass} backdrop-popup-blur`}></div>
        </LoadModalBodyEnd>) : null;
    }
}


export default CommonWarningModal;