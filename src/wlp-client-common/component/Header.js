import React from 'react';
import { withTranslation } from 'react-i18next';
const Header = ({ logo, leftComponent, rightComponent, onClickLogo }) => {
    return (
        <header className="mypage-header d-flex">
            <div className="mypage-header-left">
                <div className="header-left-logo btn-reset btn-custom-link noselect"><img onClick={onClickLogo} src={logo} alt='logo' className="noselect" /></div>
                {leftComponent}
            </div>
            <div className="mypage-header-right ml-auto">
                <div className="d-flex align-items-center h-100">
                    {rightComponent}
                </div>
            </div>
        </header>
    )
}

export default withTranslation()(Header);