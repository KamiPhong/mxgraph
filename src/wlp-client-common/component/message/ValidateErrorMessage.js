import React from 'react';
import { withTranslation } from 'react-i18next';
import i18n from 'i18n';
class ValidateErrorMessage extends React.Component {

    render() {
        const { t, message, classAdd, check,classError, classErrorMessage, isIE} = this.props;
        const isLangEn = i18n.language === 'en_US';
        let button;
        if(check === true){
            button =  <div className="d-flex noselect ">{message}</div>
        }else{
            button = (<div className="d-flex w-100">
                    <strong
                        style={{width: isLangEn ? (isIE ? 92 : 52) : 46}}
                        className={`noselect noselect-color-pink-error fs-11px ${classError}`}>
                        {t('global.errorLabel')}
                        {isLangEn ? <>&nbsp;:&nbsp;</> : ' : '}
                    </strong>
                    <span className={`noselect noselect-color-pink-error fs-11px ${classErrorMessage}`}>{message}</span>
                </div>)
        }
        return (
            <div className={'color-error invaled-error invaled-error-ps ' + (classAdd ? classAdd : "") }>
                {button}
            </div>
        )
    }
}

export default withTranslation()(ValidateErrorMessage);