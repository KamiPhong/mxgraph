import React from 'react';
import { withTranslation } from 'react-i18next';

class ForgotPasswordMessage extends React.Component {

    render() {
        const { message } = this.props;

        return (
            <div className="login-alerts bg-login-alerts">
                <div className="d-flex align-items-center justify-content-center h-100">
                    <span className="text-warning">{message}</span>
                </div>
            </div>
        )
    }
}

export default withTranslation()(ForgotPasswordMessage);