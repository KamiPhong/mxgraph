import React, { Component, createRef } from 'react';
// import { FaCaretRight } from 'react-icons/fa';
import { withTranslation } from 'react-i18next';

import loginLogo from './../images/mypage-login-logo.png';

//Component
import ValidateUtils from 'wlp-client-common/component/ValidateUtils';

//utils
import ValidateErrorMessage from './message/ValidateErrorMessage';
import ForgotPasswordMessage from './message/ForgotPasswordMessage';
import EmailConst from 'wlp-client-common/consts/EmailConst';
import PassConst from 'wlp-client-common/consts/PassConst';
import InputUtils from 'wlp-client-common/utils/InputUtils';
import TextUtils from 'wlp-client-common/utils/TextUtils';
import { hideWarningDialog } from 'wlp-client-common/utils/DialogUtils';

const validateUtils = new ValidateUtils();
const inputUtils = new InputUtils();
const textUtils = new TextUtils();

class LoginPanelSkin extends Component {
    //init component state with immutable object

    constructor() {
        super();
        this.state = {
            mailaddress: '',
            password: '',
            isSaveMailaddress: false,
            touched: {
                mailaddress: false,
                password: false,
            },
            isMailaddress: true,
            currentSelectionStart: 0,
            isTypingJapanese: false,
            isFocusInputMail: false,
            isFocusInputPassword: false
        };
        this.loginForm = createRef();
    }

    // set email address state
    setEmailAddress = (event) => {

        const regex = EmailConst.EMAIL_RESTRICT;

        const val = this.state.mailaddress;
        const { isTypingJapanese } = this.state; 

        const { selectionEnd } =  event.currentTarget;
        const valueEmail = textUtils.filterWithRestrict(event.target.value, EmailConst.EMAIL_RESTRICT);

        const isCharValid = valueEmail === '' || regex.test(valueEmail);
        
        if (valueEmail.length > 100) {
            this.setState(
                {
                    "mailaddress": val
                }, () => {
                    inputUtils.setCaretPosition(this.refs.inputEmail, selectionEnd);
                }
            );
        } else {
            if (isTypingJapanese) {
                this.handleTypingJapanese(valueEmail);
            } else {
                this.handleTypingEnglish(valueEmail, isCharValid, selectionEnd);
            }
        }
    }

    handleTypingJapanese = (valueEmail) => {
        this.setState({
            "mailaddress": valueEmail
        });
    }

    handleTypingEnglish = (valueEmail, isCharValid, selectionEnd) => {
        const val = this.state.mailaddress;

        if (!isCharValid) {
            this.setStateAndCursorPos(val, isCharValid, selectionEnd);
        } else {
            this.setStateAndCursorPos(valueEmail, isCharValid, selectionEnd);
        }
    }

    setStateAndCursorPos = (valueEmail, isCharValid, selectionEnd) => {
        this.setState(
            {
                "mailaddress": valueEmail
            }, () => {
                let cursorPos = valueEmail.length;

                if (isCharValid) {
                    cursorPos = selectionEnd;
                } else {
                    cursorPos = selectionEnd - 1;
                }

                inputUtils.setCaretPosition(this.refs.inputEmail, cursorPos);
            }
        );
    }

    // set password state
    setPassWord = (event) => {
        let valuePassword = textUtils.filterWithRestrict(event.target.value, PassConst.PASS_RESTRICT);
        const isCharValid = valuePassword.indexOf(' ') > 0 ? false : true;
        const { selectionEnd } =  event.currentTarget;
        const val = this.state.password;

        if(valuePassword.length > 100){
            this.setState(
                {
                    password: val
                }, () => {
                    inputUtils.setCaretPosition(this.refs.inputPassword, selectionEnd);
                }
            );
        }else{
            let newValPassword = valuePassword;
            if(!isCharValid){
                newValPassword = valuePassword.slice(0, selectionEnd - 1) + valuePassword.slice(selectionEnd, valuePassword.length);
                this.setState(
                    {
                        password: val.trim()
                    }, () => {
                        let cursorPosPass = val.length;
                        if(isCharValid){
                            cursorPosPass = selectionEnd;
                        }else {
                            cursorPosPass = selectionEnd - 1;
                        }
                        inputUtils.setCaretPosition(this.refs.inputPassword, cursorPosPass);
                    }
                );
            }else{
                this.setState(
                    {
                        password: newValPassword.trim()
                    }, () => {
                        let cursorPosPass = valuePassword.length;
                        if(isCharValid){
                            cursorPosPass = selectionEnd;
                        }else {
                            cursorPosPass = selectionEnd - 1;
                        }
                        inputUtils.setCaretPosition(this.refs.inputPassword, cursorPosPass);
                    }
                );
            }
        }
    }

    // set save mail state
    toggleSaveMailaddress = () => {
        this.setState(() => ({ isSaveMailaddress: !this.state.isSaveMailaddress }))
    }

    shouldMarkEmailError = field => {
        const { mailaddress } = this.state;
        const isEmailValid = validateUtils.validateEmail(mailaddress);
        this.setState(() => ({ isMailaddress: Boolean(isEmailValid) }));
        return isEmailValid;
    };

    handleBlur = field => evt => {
        if (field === "mailaddress") {
            this.setState({
                touched: { ...this.state.touched, [field]: true },
                isFocusInputMail: false
            })
        }
        if (field === "password") {
            this.setState({
                touched: { ...this.state.touched, [field]: true },
                isFocusInputPassword: false
            })
        }
    
    };
    handleFocus = field => evt => {
        if (field === "mailaddress") {
            this.setState({
                isFocusInputMail: true
            })
        }
        if (field === "password") {
            this.setState({
                isFocusInputPassword: true
            })
        }
       
    };

    handleClickSubmit = field => evt => {
        this.setState((touched) => {
            return {
                touched: {
                    ...touched,
                    mailaddress: true,
                    password: true
                }
            }
        });
    };

    loginform_handleSubmit = (evt) => {
        evt.preventDefault();
        let errorPopup = document.getElementById('error-modal');
        if (errorPopup !== null) {
            hideWarningDialog();
            return true;
        }
        
        const { loginForm_requestHandler, isRequesting } = this.props;
        const { mailaddress, password, isSaveMailaddress, isMailaddress } = this.state;

        if (isMailaddress && !this.shouldMarkError('mailaddress') && !this.shouldMarkError('password') && !isRequesting) {
            loginForm_requestHandler(mailaddress, password, isSaveMailaddress);
        }
    }

    shouldMarkError = field => {
        const { mailaddress, password } = this.state;
        const errors = validateUtils.validate(mailaddress, password);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];

        if(field === 'password') {
            return (
                (
                    hasError ||
                    PassConst.PASS_PATTERN.test(password) === false
                ) ? shouldShow : false
            );
        }else if (field === 'mailaddress') {
            return (
                (
                    hasError ||
                    validateUtils.validateEmail(mailaddress) === false
                ) ? shouldShow : false
            );
        }

    };

    handleCompositionStart = (event) => {
        const { selectionEnd } =  event.currentTarget;

        this.setState({
            currentSelectionStart: selectionEnd,
            isTypingJapanese: true
        });
    }

    handleCompositionEnd = (event) => {
        const { selectionEnd } =  event.currentTarget;
        const selectionStart = this.state.currentSelectionStart;
        const valueEmail = event.target.value;

        let japaneseChar = valueEmail.slice(selectionStart, selectionEnd);      

        const isCharValid = valueEmail === '' || textUtils.isLatinCharacters(japaneseChar);
        
        if (!isCharValid) {
            japaneseChar = "";
        }

        let newValEmail = valueEmail.slice(0, selectionStart) + japaneseChar + valueEmail.slice(selectionEnd, valueEmail.length);

        this.setState(
            {
                "mailaddress": newValEmail,
                "currentSelectionStart": selectionEnd,
                "isTypingJapanese": false
            }, () => {
                inputUtils.setCaretPosition(this.refs.inputEmail, !isCharValid ? selectionStart : selectionEnd);
            }
        );

    }

    handlePasteEmail = (event) => {
        const pasteData = textUtils.filterWithRestrict(event.clipboardData.getData('Text'), EmailConst.EMAIL_RESTRICT);
        const isCharValid = textUtils.isLatinCharacters(pasteData);

        if (!isCharValid) {
            event.preventDefault();
        }
    }

    setSaveMailAddress = () => {
        const { mailAddress } = this.props;
        if (mailAddress) {
            this.setState({isSaveMailaddress: true, mailaddress: mailAddress});
        }
    }

    componentDidMount() {
        this.setSaveMailAddress();
    }

    render() {
        const { shouldMarkError } = this;
        const {
            t,
            toggleForgotPasswordScreen,
            isMessageForgotPassword,
            isMessageChangedPassword
        } = this.props;

        const { mailaddress, password, isSaveMailaddress, touched, isFocusInputMail, isFocusInputPassword } = this.state;

        let isEmptyEmailInput = touched['mailaddress'] && mailaddress.length === 0;
        let isNotMailaddress = touched['mailaddress'] && mailaddress.length !== 0 && !validateUtils.validateEmail(mailaddress);
        let isEmptyPassword = shouldMarkError("password");
        return (
            <div className="login-w">
                <div className="container-fluid d-flex align-items-center justify-content-center login">
                    <div className="login-content">
                        <div className="login-logo">
                            <img src={loginLogo} alt="logo-login" className="img-fluid noselect" />
                        </div>
                        <div className="text-center">
                            <h1 className="login-tlt mb-3 font-din-light noselect-logo">Login</h1>
                        </div>
                        <div className="login-form">
                            <form onSubmit={this.loginform_handleSubmit}>
                                <div className="mb-28 position-relative">
                                    <input
                                        ref="inputEmail"
                                        value={mailaddress}
                                        onChange={this.setEmailAddress}
                                        type="text"
                                        maxLength="100"
                                        className={'form-control form-control-shadow' + (mailaddress === '' ? ' input-empty' : '') + (isEmptyEmailInput || isNotMailaddress ? " is-invalid" : "") + (isFocusInputMail ? "" : " noselect")} //add [is-invalid] to show error input
                                        onBlur={this.handleBlur("mailaddress")}
                                        onFocus={this.handleFocus("mailaddress")}
                                        placeholder={t('login.login.mailaddress')} 
                                        onCompositionStart = {this.handleCompositionStart}
                                        onCompositionEnd = {this.handleCompositionEnd}
                                        onPaste = {this.handlePasteEmail}/>
                                    {
                                        isEmptyEmailInput &&
                                        <ValidateErrorMessage message={t('message.login.e0001')} classError={'fs-11px'} classErrorMessage={'fs-11px'} />
                                    }
                                    {
                                        isNotMailaddress &&
                                        <ValidateErrorMessage message={t('message.global.e0002')} classError={'fs-11px'} classErrorMessage={'fs-11px'} />
                                    }
                                </div>
                                <div className="mb-28 position-relative">
                                    <input
                                        ref="inputPassword"
                                        value={password}
                                        onChange={this.setPassWord}
                                        type="password"
                                        maxLength="100"
                                        className={'form-control form-control-shadow' + (password === '' ? ' input-empty' : '') + (isEmptyPassword ? " is-invalid" : "") + (isFocusInputPassword ? "" : " noselect")}
                                        onBlur={this.handleBlur("password")}
                                        onFocus={this.handleFocus("password")}
                                        placeholder={t('login.login.password')} />
                                    {isEmptyPassword && <ValidateErrorMessage message={t('message.login.e0002')} classError={'fs-11px'} classErrorMessage={'fs-11px'} />}
                                </div>
                                <div className="form-group noselect">
                                    <div className="form-check login-form-check noselect">
                                        <label className="form-check-label form-check-guide-login noselect">
                                            <input
                                                checked={isSaveMailaddress}
                                                onChange={this.toggleSaveMailaddress}
                                                type="checkbox"
                                                className="form-check-input resize-cb-rd" />
                                            <span className="checkmark form-check-guide-all form-check-guide-login-icon"></span>
                                            <span className="ml-text-5px noselect-color-white">{t('login.login.savemailaddress')}</span>
                                        </label>
                                    </div>
                                </div>
                                <div className="form-group mb-2 noselect">
                                    <button
                                        onClick={this.handleClickSubmit()}
                                        className="btn btn-dark w-100px noselect-color-white"
                                        type="submit">
                                        {t('login.login.login')}
                                    </button>
                                </div>
                            </form>
                            <hr className="mb-1 mt-2" />
                            <p className="text-white d-flex align-items-center mt-8 text-forgotpassword mb-0">
                                <i className="fas fa-caret-right mr-2 noselect-color-white"></i>
                                <span
                                    onClick={toggleForgotPasswordScreen}
                                    className="color-aqua role-a">
                                    <span className="noselect noselect-color-aqua">{t('login.login.fogotpassword')}</span>
                                </span>
                            </p>
                        </div>
                        {isMessageForgotPassword && <ForgotPasswordMessage message={t('message.login.i0001')} />}
                        {isMessageChangedPassword && <ForgotPasswordMessage message={t('message.login.i0002')} />}
                    </div>
                </div>
            </div>
        )
    }
}

export default withTranslation()(LoginPanelSkin);