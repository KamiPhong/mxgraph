import React from 'react';

const UpIcon = () => {
    return (
        <div className="cp-skew text-uppercase up-icon">
            <div className="cp-skew-left">
                <span className="cp-skew-text noselect noselect-color-white">UP</span>
            </div>
        </div>
    )
}

export default UpIcon;