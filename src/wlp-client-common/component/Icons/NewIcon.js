import React from 'react';

const NewIcon = () => {
    return (
        <div className="cp-skew text-uppercase new-icon">
            <div className="cp-skew-left">
                <span className="cp-skew-text noselect noselect-color-white">NEW</span>
            </div>
        </div>
    )
}

export default NewIcon;