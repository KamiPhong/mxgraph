import React from 'react';
import PropTypes from 'prop-types';
import MouseConsts from 'wlp-client-common/consts/MouseConst';
import zoomSliderVariables from 'wlp-client-editor-lib/assets/scss/zoomSliderVariables.scss';
import { ZoomConfig } from "wlp-client-editor-lib/core/KGraphConfig";
class RangeSlider extends React.Component {
    static propTypes = {
        onMouseDown: PropTypes.func,
        onMouseUp: PropTypes.func,
        onChange: PropTypes.func,
        min: PropTypes.number,
        max: PropTypes.number,
        value: PropTypes.number,
        defaultWidth: PropTypes.number,
        focusWidth: PropTypes.number,
        increaseRate: PropTypes.number
    }

    static defaultProps = {
        min: 0,
        max: 10,
        value: 0,
        defaultWidth: parseFloat(zoomSliderVariables.zoomSliderNormalWidth),
        focusWidth: parseFloat(zoomSliderVariables.zoomSliderOnActivatedWidth),
        increaseRate: 1.4
    }

    constructor(props) {
        super(props);
        this.state = {
            currentValue: this.props.value - this.props.min,
            isMouseDown: false,
            isSwithControl: false,
            stepRange: 0,
            currentWidth: 0,
            slidePadContainerPrevLeft: 0
        }

        this.slidePadContainer = React.createRef();
        this.slidePad = React.createRef();

        this.min = 0;
        this.max = this.props.max - this.props.min;

        this.offsetXOnScroll = 0;
    }

    componentDidMount() {
        window.onmouseup = () => {
            const { isMouseDown } = this.state;
            if (isMouseDown) {
                this.setState({ isMouseDown: false });
            }
        }
        this.doCalculateStepRange()
    }

    componentDidUpdate(oldProps, oldState) {
        const { isMouseDown, currentValue, stepRange, isSwithControl } = this.state;
        const { value, min } = this.props;

        if (oldState.isMouseDown !== isMouseDown) {
            this.isMouseDown_onChange(isMouseDown);
        }

        if (
            value !== oldProps.value &&
            (value - min) !== currentValue &&
            currentValue === oldState.currentValue &&
            !isSwithControl
        ) {
            this.slidePad_updateStyles({ 'transition-property': 'left', 'transition-duration': `0.3s` });
            this.setCurrentValue(value - min);
        }

        if (currentValue !== oldState.currentValue && !isSwithControl) {
            this.currentValue_onChange(currentValue);
        }

        if (stepRange !== oldState.stepRange) {
            this.stepRange_onChange(this.props.value);
        }
    }

    /**
     * @author ThuyTV
     * @description calculate slidePad value and set to component state
     * @param {number} value
     * @updateState currentValue
     */
    setCurrentValue = (value) => {
        const { max, min } = this;
        if (value > max) {
            value = max;
        }
        if (value < min) {
            value = min;
        }

        if (this.state.currentValue !== value) {
            this.setState({ currentValue: value });
        }
    }

    /**
     * @author ThuyTV
     * @description check if slidePad and sldidePadContainer is ref to an elelement or not
     * @return boolean
     */
    isPadExists = () => {
        const { slidePadContainer, slidePad } = this;
        return slidePadContainer.current && slidePad.current && true;
    }

    /**
     * @author ThuyTV
     * @description calculate the step range of slidePad by slidePadContainer current width
     * @param null
     * @return void
     * @stateUpdate currentWidth, stepRange
     * @trigger slidePadContainer_updateStyles
     */
    doCalculateStepRange = (isSwithControl = false) => {
        const slidePadContainerBounding = this.getSlidePadContainerBounding();
        const width = slidePadContainerBounding.width - this.getSlidePadBounding().width;
        const { currentWidth, stepRange, isMouseDown } = this.state;
        const { max } = this;
        
        const nextStepRange = width / max;
        if (isMouseDown) {
            const oldMax = 7;
            const oldNextStepRange = width/oldMax;

            const { defaultWidth } = this.props;
            const translateLeft = (oldNextStepRange - stepRange) * (this.offsetXOnScroll/defaultWidth*oldMax - oldMax/2);
            this.slidePadContainer_updateStyles({ left: `${-translateLeft}px` });
        }

        if (currentWidth !== width) {
            this.setState({
                currentWidth: width,
                stepRange: nextStepRange,
                isSwithControl: isSwithControl
            });
        }

       
        
    }


    /**
     * @author ThuyTV
     * @description rounding the slidePad value on mouse click position
     * @param {number} clientX 
     * @return [number] Value for slidePad
     */
    calculateValueOnClientX(clientX) {
        const { min, max } = this;
        const { stepRange } = this.state;
        const slidePadContainerBounding = this.getSlidePadContainerBounding();
        let newValue = clientX - slidePadContainerBounding.left;
        if (newValue < 0) {
            newValue = min;
        } else if (newValue > slidePadContainerBounding.width) {
            newValue = max;
        } else {
            newValue = Math.round(newValue / stepRange);
        }

        return newValue;
    }

    /**
     * @author ThuyTV
     * @description handle window onMouseMove event
     * @param {MouseEvent} evt
     * @returns null
     * @trigger setCurrentValue 
     */
    window_onMouseMove = (evt) => {
        const clientX = evt.clientX;
        this.setCurrentValue(this.calculateValueOnClientX(clientX));
        this.slidePad_updateStyles({ 'transition-property': 'left', 'transition-duration': `0.1s` });
    }

    /**
     * @author ThuyTV
     * @description callback onChange passing in props
     * @param {number} newCurrentValue
     * @return void
     * @trigger slidePad_translateByValue
     */
    currentValue_onChange = (newCurrentValue) => {
        const { onChange, min, value } = this.props;
        if (
            typeof (onChange) === 'function' &&
            value !== (newCurrentValue + min)
        ) {
            const actualValue = newCurrentValue + min;
            onChange(actualValue);
        }
        this.slidePad_translateByValue(newCurrentValue);
    }

    /**
     * @author ThuyTV
     * @description re-translate slidePad on steprange change
     * @param null
     * @return void
     * @trigger slidePad_translateByValue
     */
    stepRange_onChange = (value) => {
        value = value - 1;
        this.slidePad_translateByValue(value);
    }


    /**
     * @author ThuyTV
     * @description handle isMouseDown stage change, called in ComponentDidUpdate.
     * scale slidepad if mouse is down.
     * un-scale slidepad if mouse is up.
     * callback onMouseDown and onMouseUp from props
     * 
     * @param {boolean} isMouseDown
     * @return void
     * @trigger doCalculateStepRange
     */
    isMouseDown_onChange = (isMouseDown) => {
        const { focusWidth, defaultWidth, onMouseDown, onMouseUp } = this.props;
        const srollBtn = this.slidePad.current;
        if (isMouseDown) {
            if (typeof(onMouseDown) === 'function'){
                onMouseDown();
            }

            this.slidePadContainer_updateStyles({ width: `${focusWidth}px` });
            window.onmousemove = this.window_onMouseMove;
            srollBtn.classList.add('activated');
            this.max = ZoomConfig.SLIDE_MAX_ZOOM - 1;
            
        } else {
            if (typeof(onMouseUp) === 'function'){
                onMouseUp();
            }

            this.slidePadContainer_updateStyles({
                left: 0,
                width: defaultWidth + 'px'
            });

            window.onmousemove = null;
            srollBtn.classList.remove('activated');
            this.slidePad_updateStyles({ 'transition-property': `none` });
            this.max = ZoomConfig.MAX_ZOOM_VALUE - 1;
            
        }

        this.doCalculateStepRange(true);
    }

    /**
     * @author ThuyTV
     * @description set style `left` for the slidepad by multiply value and stepRange
     * @param value
     * @trigger slidePad_updateStyles -> left 
     */
    slidePad_translateByValue = (value) => {
        const { stepRange } = this.state;
        const padLeft = stepRange * value;
        this.slidePad_updateStyles({ left: padLeft + 'px' });
        this.setState({
            isSwithControl: false
        })
    }

    /**
     * @author ThuyTV
     * @description handle slidepad MouseDown and update isMouseDown state -> true
     * @param {MouseEvent} evt
     * @return void
     * @updateState isMouseDown
     */
    slidePad_handleMouseDown = (evt) => {
        this.slidePad_updateStyles({ 'transition-property': `none` });
        if (evt.button === MouseConsts.MOUSE_LEFT_BUTTON) {
            this.offsetXOnScroll = evt.clientX - this.getSlidePadContainerBounding().left;
            this.setState({ isMouseDown: true });
        }
    }

    /**
     * @author ThuyTV
     * @description update slidepad current value by handle mouse positon on click slidePadContainer
     * @param {MouseEvent} evt
     * @return void
     * @trigger setCurrentValue
     */
    slidePadContainer_handleClick = (evt) => {
        const { isMouseDown } = this.state;
        if (evt.target === this.slidePadContainer.current && !isMouseDown) {
            const clientX = evt.clientX;
            this.setCurrentValue(this.calculateValueOnClientX(clientX));
        }
    }

    /**
     * @author ThuyTV
     * @description check is slidePad exists, if exists than get slidePad bounding
     * @param null
     * @return {DOMRect}
     */
    getSlidePadBounding = () => {
        if (!this.isPadExists()) { return {} };
        return this.slidePad.current.getBoundingClientRect();
    }

    /**
     * @author ThuyTV
     * @description update slidePad Style by passed fields
     * @param {object} styles
     * @return void
     * @trigger updateRefStyle
     */
    slidePad_updateStyles = (styles) => {
        if (this.isPadExists()) {
            this.updateRefStyle(this.slidePad, styles);
        }
    }

    /**
     * @author ThuyTV
     * @description check is slidePadContainer exists, if exists than get slidePadContainer bounding
     * @param null
     * @return {DOMRect}
     */
    getSlidePadContainerBounding = () => {
        if (!this.isPadExists()) { return {} };
        return this.slidePadContainer.current.getBoundingClientRect();
    }

    /**
     * @author ThuyTV
     * @description update slidePadContainer Style by passed fields
     * @param {object} styles
     * @return void
     * @trigger updateRefStyle
     */
    slidePadContainer_updateStyles = (styles) => {
        if (this.isPadExists()) {
            this.updateRefStyle(this.slidePadContainer, styles);
        }
    }

    /**
     * @author ThuyTV
     * @description styles of passed React Ref element
     * @param {React.Ref} ref
     * @param {object} styles
     * @return void
     * @trigger updateRefStyle
     */
    updateRefStyle = (ref, styles) => {
        if (
            styles ||
            typeof (styles) == 'object'
        ) {
            for (let key of Object.keys(styles)) {
                const value = styles[key];
                ref.current.style[key] = value;
            }
        };
    }

    render() {
        return (
            <div
                ref={this.slidePadContainer}
                onClick={this.slidePadContainer_handleClick}
                className="slider-container">
                <div className="w-slide-pad" ref={this.slidePad}>
                    <button onMouseDown={this.slidePad_handleMouseDown} className="slide-pad" />
                </div>
            </div>
        )
    }
}

export default RangeSlider;