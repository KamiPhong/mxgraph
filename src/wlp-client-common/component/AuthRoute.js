import React, { useState, useEffect, useCallback } from 'react';
import { Route, useLocation } from 'react-router-dom';
import AuthService from 'wlp-client-service/service/AuthService';
import PrealoadDisplay from './PreloadDisplay';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import i18n from 'i18n';
import ErrorBoundary from 'wlp-client/ErrorBoundary';

const authService = new AuthService();
const strageUtils = new LocalStorageUtils();
const langueUtils = new WlpLanguageUtil();

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const AuthRoute = ({ component: Component, match, ...rest }) => {
    const query = useQuery();
    const [loginFlag, setLoginFlag] = useState(false);
    const [checkLoginFlag, setCheckLoginFlag] = useState(false);
    const [loadingFlag, setLoadingFlag] = useState(true);
    const [next, setNext] = useState(query.get('next'));
    const [loginPageFlag, setLoginPageFlag] = useState(false);
    const [mailAddress, setMailAddress] = useState('');

    const renderComponent = props => {
        if (props.match.params.next && next !== props.match.params.next) {
            setNext(`/${props.match.params.next}`);
        }
        let loadLoginpage = true;
        const isLoginPage = props.match.path.indexOf('/login') === 0;
        if (loginFlag === false && !isLoginPage) {
            setLoadingFlag(false);
        }

        if (loadingFlag) {
            return <ErrorBoundary><PrealoadDisplay loadingFlag={loadingFlag} recallFlag={false}/></ErrorBoundary>
        }
        
        if (isLoginPage === false) {
            setNext(props.match.url);
            loadLoginpage = false;
        } else if(isLoginPage && !next) {
            setNext("/mypage");
        }

        setLoginPageFlag(isLoginPage);

        if (loginFlag === false && isLoginPage) {
            const LoginPanel = React.lazy(() => import('./LoginPanel'));
            return <ErrorBoundary> <LoginPanel {...props} loginFlag={loginFlag} loadLoginpage={loadLoginpage} setLoginState={setLoginFlag}  mailAddress={mailAddress}/> </ErrorBoundary>;
        } 
        
        if (loginFlag === false) {
            return;
        }
        
        return (
            <ErrorBoundary><Component {...rest} {...props} /></ErrorBoundary>
        )
    }

    const handleCheckLogon = useCallback((response) => {
        if (response.isSucceed) {
            langueUtils.setLocaleLanguage(response.sessionDto.localeCode);
            strageUtils.setUserSession(response);
        }

        const savedMailAddress = strageUtils.getSavedMailAddress();

        if (savedMailAddress) {
            setMailAddress(savedMailAddress);
        }
        setLoginFlag(response.isSucceed);
        setLoadingFlag(false);
        setCheckLoginFlag(true);
    }, []);

    useEffect(() => {
        authService
            .checkLoggedOn()
            .then(handleCheckLogon)
    }, [handleCheckLogon]);

    useEffect(() => {
        if (typeof next === 'string') {
            if (next.toLowerCase().indexOf('/editor') === 0) {
                document.title = i18n.t('global.title.editor') + ' / '+ i18n.t('global.title.kboard');
            } else {
                document.title = i18n.t('global.title.mypage') + ' / '+ i18n.t('global.title.kboard');
            }
        }
    }, [next])

    useEffect(() => {
        if (checkLoginFlag) {
            if (loginFlag) {//If loggedin
                if (loginPageFlag) { //If in Login Page
                    window.location = next;
                    
                } else { //If not in login page
            
                }
            } else { //If not loggedin
                if (loginPageFlag) { //If in Login Page
                    

                } else { //If not in login page
                    window.location = `/login?next=${next}`;
                }
            }
        }

    }, [loginFlag, next, loginPageFlag, checkLoginFlag]);

    return (
        <Route {...rest} render={renderComponent} />
    )
};

export default AuthRoute;
