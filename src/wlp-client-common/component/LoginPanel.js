import React, { Component } from 'react';

import LoginPanelSkin from './LoginPanelSkin';
import LogonConfigDto from 'wlp-client-service/dto/auth/LogonConfigDto';
import ResetPasswordConfigDto from 'wlp-client-service/dto/password/ResetPasswordConfigDto';
import ResetPasswordDto from 'wlp-client-service/dto/password/ResetPasswordDto';
import LogonDto from 'wlp-client-service/dto/auth/LogonDto';
import AuthService from 'wlp-client-service/service/AuthService';
//utils
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import GetLocale from 'wlp-client-common/utils/GetLocale';
import ForgotPasswordSkin from './ForgotPasswordSkin';
import PasswordService from 'wlp-client-service/service/PasswordService';
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import i18n from 'i18n';
import PropTypes from 'prop-types';
import ChangePasswordSkin from './ChangePasswordSkin';
import ChangePasswordDto from 'wlp-client-service/dto/password/ChangePasswordDto';
import 'wlp-client-common/component/login.css';
import ActionLogger from 'wlp-client-common/ActionLogger';
import PrealoadDisplay from './PreloadDisplay';
const getLocale = new GetLocale();
const strageUtils = new LocalStorageUtils();
const authService = new AuthService();
const langueUtils = new WlpLanguageUtil();
const passwordService = new PasswordService();

class LoginPanel extends Component {
    state = {
        isForgotPassScreen: false,
        isMessageForgotPassword: false,
        isShowErrorLogon: false,
        isPasswordChange: false,
        isMessageChangedPassword: false,
        newPasswordErrorMessage: '',
        currentPasswordErrorMessage: '',
        isRequesting:false
    }

    constructor(props){
        super(props);
        i18n.changeLanguage(langueUtils.getDefaultLanguage());
    }

    loginForm_requestHandler = async (mailaddress, password, isSaveMailaddress) => {
        this.setState({isRequesting: true});
        const logonConfigDto = new LogonConfigDto();
        logonConfigDto.mailaddress = mailaddress;
        logonConfigDto.password = password;
        logonConfigDto.isSaveMailaddress = isSaveMailaddress;

        const logonData = await authService.logon(logonConfigDto);
   
        if (logonData.isSucceed) {
            if (isSaveMailaddress) {
                strageUtils.setSavedMailAddress(mailaddress);
            } else {
                strageUtils.removeSavedMailAddress();
            }
        }
        
        this.logon_resultHandler(logonData);
    }

    logon_resultHandler = (result = new LogonDto()) => {
        const { setLoginState, setUserInfo } = this.props;
        if (!result.isSucceed ) {
            if (result.changePassword) {
                this.showPasswordChange();
            } else if (result.message !== '') {
                showWarningDialog(
                    result.messageKey,
                    {
                        showIcon: false
                    }
                )
            }
            
        } else if (result.isSucceed) {
            strageUtils.setUserSession(result);
            //update login state
            if (typeof(setLoginState) === 'function'){
                setLoginState(result.isSucceed);
            }
            
            if (typeof(setUserInfo) === 'function'){
                setUserInfo(result);
            }
            // update locale after login
            if (typeof result.sessionDto !== "undefined") {
                langueUtils.setLocaleLanguage(result.sessionDto.localeCode);
            }
        }
        this.setState({isRequesting: false});
    }

    alertModal_handleCancel = () => {
        this.setState(() => ({ isShowErrorLogon: false }));
    }



    resetPassword_requestHandler = async (forgotMailAddress) => {
        const resetPasswordConfigDto = new ResetPasswordConfigDto();
        resetPasswordConfigDto.mailaddress = forgotMailAddress;
        resetPasswordConfigDto.localeCode = getLocale.getLocale();

        const resetPasswordResult = await passwordService.resetPassword(resetPasswordConfigDto);
        this.resetPassword_resultHandler(resetPasswordResult);
    }

    resetPassword_resultHandler = (result = new ResetPasswordDto()) => {
        if (result.isSucceed){
            this.backToLoginScreen();
        }
    }

    toggleForgotPasswordScreen = () => {
        if (!this.state.isForgotPassScreen) {
            ActionLogger.click('Login_ForgotPass', 'OpenForgotScreen_Button');
        } else {
            ActionLogger.click('Login_ForgotPass', 'ReturnLogin_Button');
        }

        this.setState(
            ({ isForgotPassScreen }) => ({
                isForgotPassScreen: !isForgotPassScreen
            })
        )
    }

    hideNewTemplateModal = () => {
        this.setState({ isShowErrorLogon: false })
    }

    backToLoginScreen = () => {
        this.setState(() => ({ isForgotPassScreen: false, isMessageForgotPassword: true }));
        setTimeout(
            function() {
                this.setState({isMessageForgotPassword: false});
            }
            .bind(this),
            3000
        );
    }

    showPasswordChange = () => {
        this.setState({isPasswordChange: true});
    }

    callChangePassword = (requestDto) => {
        this.setState({newPasswordErrorMessage: '', currentPasswordErrorMessage: ''});
        passwordService.loginChangePassword(requestDto).then(response => {
            this.changePasswordResultHandler(response);
        }).catch(err => {
            showWarningDialog(('message.global.e0001'));
        });
    }

    changePasswordResultHandler = (response = new ChangePasswordDto()) => {
        if (!response.isSucceed) {
            if (response.messageKey === 'validate.polycy.password.history.error') {
                // パスワード重複エラーの場合は新規パスワードにエラーを表示
                this.setState({newPasswordErrorMessage: response.message});
            } else {
                this.setState({currentPasswordErrorMessage: response.message});
            }
            
            return;
        }

        this.setState({isPasswordChange: false, isMessageChangedPassword: true});
        setTimeout(() => {
            this.setState({isMessageChangedPassword: false});
        }, 3000);
    }

    cancelBtnClickHandler = () => {
        this.setState({isPasswordChange: false, newPasswordErrorMessage: '', currentPasswordErrorMessage: ''});
    }

    render() {
        const { isForgotPassScreen, isMessageForgotPassword, isShowErrorLogon, isPasswordChange, currentPasswordErrorMessage, newPasswordErrorMessage, isMessageChangedPassword } = this.state;
        let loginFlag = this.props.loginFlag;
        return (
            <React.Fragment>
                {(typeof loginFlag !== "undefined" && (loginFlag === false || loginFlag === true) && this.props.loadLoginpage === true) ? (
                    <div className='form-login'>
                    <div className={isForgotPassScreen ? 'd-block login-bl' : 'd-none'} >
                        <ForgotPasswordSkin
                        isMessageForgotPassword={isMessageForgotPassword}
                        toggleForgotPasswordScreen={this.toggleForgotPasswordScreen}
                        resetPassword_requestHandler={this.resetPassword_requestHandler} />
                    </div>

                    {
                        isPasswordChange &&  <ChangePasswordSkin
                        currentPasswordErrorMessage={currentPasswordErrorMessage}
                        newPasswordErrorMessage={newPasswordErrorMessage}
                        callChangePassword={this.callChangePassword}
                        cancelBtnClickHandler={this.cancelBtnClickHandler}
                    />
                    }
                    <div className={isForgotPassScreen || isPasswordChange ? 'd-none' : 'd-block login-bl '}>
                        <LoginPanelSkin
                            mailAddress={this.props.mailAddress}
                            alertModal_handleCancel={this.alertModal_handleCancel}
                            isShowErrorLogon={isShowErrorLogon}
                            isMessageForgotPassword={isMessageForgotPassword}
                            isMessageChangedPassword={isMessageChangedPassword}
                            toggleForgotPasswordScreen={this.toggleForgotPasswordScreen}
                            loginForm_requestHandler={this.loginForm_requestHandler}
                            isRequesting={this.state.isRequesting} />
                    </div>
                </div>
                    
                ) : (
                    <div></div>
                )}
            </React.Fragment>
            )
    }
} 

LoginPanel.propTypes = {
    setLoginState: PropTypes.func,
    setUserInfo: PropTypes.func
}

export default LoginPanel;