import React, { Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import slide1 from '../images/popup/popup-1.jpg';
import slide2 from '../images/popup/popup-2.jpg';
import slide3 from '../images/popup/popup-3.jpg';
import slide4 from '../images/popup/popup-4.jpg';
import slide5 from '../images/popup/popup-5.jpg';

import slideJa1 from '../images/popup/popupJa-1.jpg';
import slideJa2 from '../images/popup/popupJa-2.jpg';
import slideJa3 from '../images/popup/popupJa-3.jpg';
import slideJa4 from '../images/popup/popupJa-4.jpg';
import slideJa5 from '../images/popup/popupJa-5.jpg';

import btnPrev from '../images/icon/ic-btn-prev.png';
import btnNext from '../images/icon/ic-btn-next.png';
import iconClose from '../images/icon/icon-close.svg';
import Link from './Link';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import i18n from "i18n";
import ActionLogger from 'wlp-client-common/ActionLogger';

const tourView = {
    'en_US': [
        slide1, slide2, slide3, slide4, slide5
    ],
    'ja_JP': [
        slideJa1, slideJa2, slideJa3, slideJa4, slideJa5
    ]
};
const strageUtils = new LocalStorageUtils();
const localeCode = i18n.language;
const tourViewByLanguage = (localeCode === LocaleConst.en_US) ? tourView['en_US'] : tourView['ja_JP'];

class MypageGuideDisplay extends React.Component {

    state = {
        items: tourViewByLanguage,
        index: 0, 
        isNext: true,
        isBtnDisable: {btnNext: false, btnPrev: true},
        isChecked: false,
        isReachLastSlide: true
    }

    // handle event click btn prev
    handlerPrev = () => {
        let index = this.state.index;
        this.setState({isReachLastSlide: false});
        ActionLogger.click('Mypage_TourGuide', 'Prev_Button');

        if ( index < 1 ) { 
            this.setStateGuideDisplay(index, true, false, true);
        } else {
            index = index - 1;
            this.setStateGuideDisplay(index, false, false, false);
            if (index === 0) {
                this.setState({isBtnDisable: {btnPrev: true}});
            }
        }
    }
    
    // handle event click btn next
    handlerNext = () => {
        let index = this.state.index;
        let length = this.state.items.length - 1;

        ActionLogger.click('Mypage_TourGuide', 'Next_Button');

        if (index === length-1) {
            this.setState({isReachLastSlide: false});
        }
        
        if ( index === length ) { 
            this.setStateGuideDisplay(index, false, true, false);
        } else {
            index = index + 1;
            this.setStateGuideDisplay(index, true, false, false);
            if (index === length) {
                this.setState({isBtnDisable: {btnNext: true}});
            }
        }
    }

    // set state events on page
    setStateGuideDisplay = (index, isNext, statusBtnNext, statusBtnPrev) => {
        this.setState({ 
            index: index, 
            isNext: isNext, 
            isBtnDisable: {
                btnNext: statusBtnNext, 
                btnPrev: statusBtnPrev
            }
        });
    }

    // set state events show guide display
    setStatusShowGuideDisplay = (status) => {
        this.setState({ isChecked: status }, () => {
            const { isChecked } = this.state;
            strageUtils.setLocalStrage(strageUtils.SKIP_TOUR_MYPAGE, isChecked);
        });
    }

    // handle event checkbox 
    checkBoxChangeHandler = (e) => {
        ActionLogger.click('Mypage_TourGuide', 'DontShowAgain_Checkbox');

        if (e.target.checked) {
            this.setStatusShowGuideDisplay(true);
        } else {
            this.setStatusShowGuideDisplay(false);
        }
    }

    render() {
        const { isBtnDisable, index, items, isReachLastSlide } = this.state;
        const {tourViewCloseHandler, eventsLetsStartUsingSystem, t } = this.props;
        const src = items[index];
        
        return (
            <Fragment>
                    <div className="mypage-slider d-flex align-items-center justify-content-center">
                    <div className="mypage-slider-content box-shadow-slider">
                    <div id="carouselExampleControls" className="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                        <div className="carousel-inner">
                            <div className="carousel_slide carousel-item active noselect">
                                {
                                    (index === 4) && src && (
                                        <div className="w-btn-now position-absolute">
                                            <Link onClick={eventsLetsStartUsingSystem} className="btn btn-dark box-shadow">{t('mypage.slider.button.now')}</Link>
                                        </div>
                                    )
                                }
                                <img src={src} alt="" className="noselect"/>
                            </div>
                        </div>
                    </div>
                    <div className="w-slider-prev w-btn-slider position-absolute text-center none-select">
                        <Link onClick={this.handlerPrev} disabled={isBtnDisable.btnPrev}
                            className={`btn-slider btn-slider-prev ${isBtnDisable.btnPrev ? 'btn-slider-disabled' : ''}`}>
                                <img src={btnPrev} alt="" width={12} className="noselect" />
                        </Link>
                        <span className={`label-slider noselect ${isBtnDisable.btnPrev ? '' : 'text-slider-disabled'}`}>{t('mypage.slider.previous')}</span>
                    </div>
                    <div className="w-slider-next w-btn-slider position-absolute text-center none-select">
                        <Link onClick={this.handlerNext} disabled={isBtnDisable.btnNext}
                            className={`btn-slider btn-slider-next ${isBtnDisable.btnNext ? 'btn-slider-disabled' : ''} 
                                                                    ${isReachLastSlide ? 'btn-slider-effect' : ''}`}>
                                <img src={btnNext} alt="" width={12} className="noselect" />
                        </Link>
                        <span className={`label-slider noselect ${isBtnDisable.btnNext ? '' : 'text-slider-disabled'}`}>{t('mypage.slider.next')}</span>
                    </div>
                    <div className="slider-checkct position-absolute">
                        <div className="form-check login-form-check">
                        <label className="form-check-label d-flex align-items-end form-check-guide">
                            <input type="checkbox" checked={this.state.isChecked} onChange={this.checkBoxChangeHandler} className="form-check-input resize-cb-rd" defaultValue="checkedValue" />
                            <span className="checkmark form-check-guide-all form-check-guide-icon"></span>
                    <span className="ml-1 noselect">{ t('mypage.tour.skipTour') }</span>
                        </label>
                        </div>
                    </div>
                    <div className="slider-close position-absolute">
                        <Link onClick={tourViewCloseHandler} className="btn-slider-close d-block noselect"><img src={iconClose} alt="" width={32} className="noselect" /></Link> 
                    </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default withTranslation()(MypageGuideDisplay);

