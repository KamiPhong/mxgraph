import React from 'react';

const Link = ({ onClick, href, innerRef, children, ...res }) => {

    const handleOnClick = (e) => {
        if (!href) {
            e.preventDefault();
        }
        if (typeof (onClick) === 'function') {
            onClick(e);
        }
    }

    return <a {...res} ref={innerRef} href={href} onClick={handleOnClick}>{children}</a>
}

export default Link;