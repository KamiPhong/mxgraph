import EmailConst from "wlp-client-common/consts/EmailConst";

class ValidateUtils {

    validate(mailaddress, password) {
        // true means invalid, so our conditions got reversed
        return {
            mailaddress: mailaddress.length === 0,
            password: password.length === 0
        };
    }
    validateEmptyEmail(mailaddress) {
        // true means invalid, so our conditions got reversed
        return {
            mailaddress: mailaddress.length === 0
        };
    }
    validateEmail(value) {
        return EmailConst.EMAIL_PATTERN.test(value);
    }

    validatePassword(password){
        return{
            password: password.length === 0
        }
    }
    validateNewPassword(newPassword){
        let regex = "(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,})";
        return  !Boolean(newPassword.match(regex));
    }

    validateConfirmNewPassword(confirmNew, newPassword){
        let check = false;
        if(confirmNew !== newPassword){
            check = true;
        }
        return{
            confirmNewPassword: check
        }
    }



}

export default ValidateUtils;