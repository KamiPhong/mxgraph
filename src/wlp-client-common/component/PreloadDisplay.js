import React, { useEffect, useState } from 'react';
import TrippleCircle from 'wlp-client-common/images/kakiageTrippleCircle.png';
import TrippleCircleBorder from 'wlp-client-common/images/kakiageTrippleCircleBorder.png';
import KakiageText from 'wlp-client-common/images/kakiageText.png';
import './PreloadDisplay.css';
import CustomEvent from 'wlp-client-common/CustomEvent';

function PrealoadDisplay({loadingFlag, recallFlag}) {
    const [count, setCount] = useState(86);
    const [countDone, setCountDone] = useState(0);
    const [countLoading, setCountLoading] = useState(0);
    const [textLoading, setTextLoading] = useState('LOADING .');

    useEffect(() => {
        if (recallFlag) {
                setCount(100);
                return;
            }
        let timmer = null; 
        if (loadingFlag && count < 100) {
            timmer = setTimeout(() => {
                setCount(count => count + 1);
            }, 50);
        }

        if (!loadingFlag) {
            setCountDone(100);
            setCount(100);
        }

        return () => timmer && clearTimeout(timmer);

    }, [count, loadingFlag])

    useEffect(() => {
        const interLoading = setInterval(() => {
            if ( countLoading < 2 ) {
                setCountLoading(countLoading => countLoading + 1)
            }
            else {
                setCountLoading(0);
            }
        }, 200);
        return () => {
            clearInterval(interLoading);
        }
    }, [countLoading]);

    useEffect(() => {
        let dot = "";
        for (let i = 0; i < countLoading; i++) {
          dot += ".";
        }
        setText(dot);
    }, [countLoading]);
    

    useEffect(() => {
        const mountedEvent = new CustomEvent('preload-display-mounted');
        document.dispatchEvent(mountedEvent);
        return () => {
            const unmountedEvent = new CustomEvent('preload-display-unmounted');
            document.dispatchEvent(unmountedEvent);
        }

    }, []);

    const setText = (dot) => {
        let txt = "LOADING." + dot;
        setTextLoading(txt);
    };

    let classLoading = ['loading-number text-white font-din-l fs-20px'];
    let classLoadingText = ['font-din-l loading-txt fs-12px'];

    return (
        <div className="loading-w" >
            <div className="loading-ct text-center">
                <div className="loading-logo">
                    <img className="spinning-background" src={TrippleCircleBorder} alt="" />
                    <div className="incon-container">
                        <img className="spinning-icon" src={TrippleCircle} alt="" />
                    </div>
                </div>
                <div className="">
                    <img src={KakiageText} alt="" />
                </div>
                <div className={classLoading.join(' ')}>{countDone ? countDone : (count < 100 ? '0'+count : count) }%</div>
                <div className={classLoadingText.join(' ')}>{textLoading}</div>
            </div>
        </div>
    )
}

export default PrealoadDisplay;