import React from 'react';
import { withTranslation } from 'react-i18next';

import loginLogo from './../images/mypage-login-logo.png';
import ValidateErrorMessage from './message/ValidateErrorMessage';
import PassConst from 'wlp-client-common/consts/PassConst';
import TextUtils from 'wlp-client-common/utils/TextUtils';
import WlpStringUtil from 'wlp-client-common/utils/WlpStringUtil';
import LoginChangePasswordConfigDto from 'wlp-client-service/dto/password/LoginChangePasswordConfigDto';
import TabContainer from './Tab/TabContainer';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { IS_TOUCH } from 'wlp-client-common/utils/BrowserUtil';
import EditorClient from 'wlp-client-common/utils/EditorClientUtils';

const textUtils = new TextUtils();
const wlpStringUtil = new WlpStringUtil();

class ChangePasswordSkin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPassword: '',
            newPassword: '',
            reEnterNewPassword: '',
            currentPasswordError: false,
            newPasswordError: false,
            reEnterNewPasswordError: false,
            currentPasswordErrorMessage: '',
            newPasswordErrorMessage: '',
        }

        this.defaultState = {
            currentPassword: '',
            newPassword: '',
            reEnterNewPassword: '',
            currentPasswordError: false,
            newPasswordError: false,
            reEnterNewPasswordError: false,
            currentPasswordErrorMessage: '',
            newPasswordErrorMessage: '',
        }

        this.currentPasswordRef = React.createRef();
        this.newPasswordRef = React.createRef();
        this.reEnterNewPasswordRef = React.createRef();
    }

    onChangeCurrentPassword = (evt) => {
        const currentPassword = textUtils.filterWithRestrict(evt.target.value, PassConst.PASS_RESTRICT);
        this.setState({ currentPassword });
    }

    onChangeNewPassword = (evt) => {
        const newPassword = textUtils.filterWithRestrict(evt.target.value, PassConst.PASS_RESTRICT);
        this.setState({ newPassword });
    }

    onChangeReEnterNewPassword = (evt) => {
        const reEnterNewPassword = textUtils.filterWithRestrict(evt.target.value, PassConst.PASS_RESTRICT);
        this.setState({ reEnterNewPassword });
    }


    currentPasswordFocusOutHandler = () => {
        this.validateCurrentPassword();
    }

    validateCurrentPassword = () => {
        const { currentPassword } = this.state;
        if (currentPassword === '') {
            this.setState({ currentPasswordError: true, currentPasswordErrorMessage: this.props.t('message.personal.e0004') });
        } else {
            this.setState({ currentPasswordError: false, currentPasswordErrorMessage: '' });
        }

        return currentPassword === '';
    };

    newPasswordFocusOutHandler = () => {
        this.validateNewPassword();
    }

    validateNewPassword = () => {
        const { newPassword } = this.state;
        const isPasswordValid = wlpStringUtil.validatePassword(newPassword);
        if (!isPasswordValid) {
            this.setState({ newPasswordError: true, newPasswordErrorMessage: this.props.t('message.personal.e0003') });
        } else {
            this.setState({ newPasswordError: false, newPasswordErrorMessage: '' });
        }

        return !isPasswordValid;
    };

    reEnterNewPasswordFocusOutHandler = () => {
        this.validateReEnterNewPassword()
    }

    validateReEnterNewPassword = () => {
        const { newPassword, reEnterNewPassword } = this.state;
        if (newPassword !== reEnterNewPassword) {
            this.setState({ reEnterNewPasswordError: true });
        } else {
            this.setState({ reEnterNewPasswordError: false });
        }

        return newPassword !== reEnterNewPassword;
    }

    validateChangePasswordAll = () => {
        const promises = [this.validateCurrentPassword, this.validateNewPassword, this.validateReEnterNewPassword];
        const [currentPasswordError, newPasswordError, reEnterNewPasswordError] = promises.map(f => f());

        if (currentPasswordError) {
            if (this.currentPasswordRef instanceof HTMLElement) {
                this.currentPasswordRef.focus();
            }
        } else if (newPasswordError) {
            if (this.newPasswordRef instanceof HTMLElement) {
                this.newPasswordRef.focus();
            }
        } else if (reEnterNewPasswordError) {
            if (this.reEnterNewPasswordRef instanceof HTMLElement) {
                this.reEnterNewPasswordRef.focus();
            }
        }

        return !currentPasswordError && !newPasswordError && !reEnterNewPasswordError;
    }

    callChangePassword = () => {
        const { currentPassword, newPassword } = this.state;
        const requestDto = new LoginChangePasswordConfigDto();
        requestDto.currentPassword = currentPassword;
        requestDto.newPassword = newPassword;
        if (typeof this.props.callChangePassword === 'function') {
            this.props.callChangePassword(requestDto);
        }
    }

    saveChangesBtnClickHandler = () => {
        ActionLogger.click('Login_ChangePassword', 'Save_Button');
        if ((this.validateChangePasswordAll())) {
            this.callChangePassword();
        }
    }

    cancelBtnClickHandler = () => {
        ActionLogger.click('Login_ChangePassword', 'Cancel_Button');
        if (typeof this.props.cancelBtnClickHandler === 'function') {
            this.props.cancelBtnClickHandler();
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.currentPasswordErrorMessage !== prevProps.currentPasswordErrorMessage 
            && this.props.currentPasswordErrorMessage !== '') {
                this.setState({ currentPasswordError: true, currentPasswordErrorMessage: this.props.currentPasswordErrorMessage});
        }

        if (this.props.newPasswordErrorMessage !== prevProps.newPasswordErrorMessage 
            && this.props.newPasswordErrorMessage !== '') {
                this.setState({newPasswordError: true, newPasswordErrorMessage: this.props.newPasswordErrorMessage});
        }
    }

    resetChangePasswordSkin = () => {
        this.setState(this.defaultState);
    }

    render() {
        const { t } = this.props;
        const { currentPassword, newPassword, reEnterNewPassword, currentPasswordError, newPasswordError, reEnterNewPasswordError, currentPasswordErrorMessage, newPasswordErrorMessage } = this.state;
        let currrentPasswordClasses = ['form-control form-control-shadow'];
        let newPasswordClasses = ['form-control form-control-shadow'];
        let reEnterNewPasswordClasses = ['form-control form-control-shadow'];

        if (currentPasswordError) {
            currrentPasswordClasses.push('is-invalid');
        } 

        if (currentPassword === '') {
            currrentPasswordClasses.push('input-empty');
        }

        if (newPasswordError) {
            newPasswordClasses.push('is-invalid');
        } 
        
        if (newPassword === '') {
            newPasswordClasses.push('input-empty');
        }

        if (reEnterNewPasswordError) {
            reEnterNewPasswordClasses.push('is-invalid');
        } 
        
        if (reEnterNewPassword === '') {
            reEnterNewPasswordClasses.push('input-empty');
        }

        if (!IS_TOUCH) {
            currrentPasswordClasses.push('noselect');
            newPasswordClasses.push('noselect');
            reEnterNewPasswordClasses.push('noselect');
        }

        let isIE = EditorClient.IS_IE;

        return (
            <div className="container-fluid d-flex align-items-center justify-content-center login overflow-hidden">
                <div className="login-content change-password-content">
                    <div className="login-logo">
                        <img src={loginLogo} alt="logo-login" className="img-fluid mb-22px" />
                    </div>
                    <div className="text-center">
                    <h1 className="login-tlt mb-3 font-din-light fs-1-84rem mb-11px noselect">{t('login.login.changepassword')}</h1>
                    </div>
                    <p className="line-height-1rem text-center" dangerouslySetInnerHTML={{ __html: t('login.changepassword.title') }}></p>
                    <TabContainer>
                        <div className="login-form changepassword-form">
                            <form>
                                <div className='mt-3 position-relative'>
                                    <p className="line-height-1rem mb-4px text-center noselect" dangerouslySetInnerHTML={{ __html: t('mypage.personalSettings.password.currentPassword') }}></p>
                                    <input
                                        tabIndex="3"
                                        ref={r => this.currentPasswordRef = r}
                                        value={currentPassword}
                                        type="password"
                                        maxLength="64"
                                        className={currrentPasswordClasses.join(' ')}
                                        onBlur={this.currentPasswordFocusOutHandler}
                                        onChange={this.onChangeCurrentPassword} />
                                    {currentPasswordError && <ValidateErrorMessage classAdd="position-relative" message={currentPasswordErrorMessage} />}
                                </div>
                                <p className="line-height-1rem mt-10px ml-negative-8px mr-negative-8px text-center noselect" dangerouslySetInnerHTML={{ __html: '※' + t('validate.polycy.error') }}></p>

                                <div className='mb-15px mt-18px position-relative'>
                                    <p className="line-height-1rem mb-4px text-center noselect" dangerouslySetInnerHTML={{ __html: t('mypage.personalSettings.password.newPassword') }}></p>
                                    <input
                                        tabIndex="4"
                                        ref={r => this.newPasswordRef = r}
                                        value={newPassword}
                                        type="password"
                                        maxLength="64"
                                        className={newPasswordClasses.join(' ')}
                                        onBlur={this.newPasswordFocusOutHandler}
                                        onChange={this.onChangeNewPassword} />
                                    {newPasswordError && <ValidateErrorMessage classError='text-nowrap noselect' classAdd="position-relative" message={newPasswordErrorMessage} isIE={isIE}/>}
                                </div>
                                <div className='mt-3 position-relative'>
                                    <input
                                        tabIndex="0"
                                        ref={r => this.reEnterNewPasswordRef = r}
                                        value={reEnterNewPassword}
                                        type="password"
                                        maxLength="64"
                                        className={reEnterNewPasswordClasses.join(' ')}
                                        placeholder={t('mypage.personalSettings.password.reEnterNewPassword')}
                                        onBlur={this.reEnterNewPasswordFocusOutHandler}
                                        onChange={this.onChangeReEnterNewPassword} />
                                    {reEnterNewPasswordError && <ValidateErrorMessage classAdd="position-relative noselect" message={t('message.personal.e0002')} />}
                                </div>
                            </form>
                            <hr className="mb-9px mt-9px ml-negative-28px mr-negative-28px bc-change-pass mb-10px" />
                            <div className="form-group text-center">
                                <button
                                    tabIndex="1"
                                    type="button"
                                    onClick={this.cancelBtnClickHandler}
                                    className="btn reset-border min-w100 mr-8px btn-cancel-change-pass">{t('global.cancel')}
                                </button>
                                <button
                                    tabIndex="2"
                                    type="button"
                                    onClick={this.saveChangesBtnClickHandler}
                                    className="btn btn-dark ml-10 h-36px reset-border min-w85 m-0 noselect noselect-color-white">{t('global.ok')}
                                </button>
                            </div>
                        </div>
                    </TabContainer>
                </div>
            </div>
        )
    }
}

export default withTranslation()(ChangePasswordSkin);