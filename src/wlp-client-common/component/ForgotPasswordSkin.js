import React, { createRef } from 'react';
import { withTranslation } from 'react-i18next';

import loginLogo from './../images/mypage-login-logo.png';
import leftArrowIcon from './../images/leftArrowIcon.png';
import ValidateUtils from 'wlp-client-common/component/ValidateUtils';
import EmailConst from 'wlp-client-common/consts/EmailConst';
import ValidateErrorMessage from './message/ValidateErrorMessage';
import InputUtils from 'wlp-client-common/utils/InputUtils';
import TextUtils from 'wlp-client-common/utils/TextUtils';

const inputUtils = new InputUtils();
const textUtils = new TextUtils();

const validateUtils = new ValidateUtils();
class ForgotPasswordSkin extends React.Component {
    state = Object.freeze({
        forgotMailAddress: '',
        touchedEmailField: false,
        isMailaddress: true,
        touched: {
            forgotMailAddress: false,
        },
    })

    constructor() {
        super();
        this.forgotForm = createRef();
        this.inputForgotMailAddress = createRef();
    }

    shouldMarkEmailError = field => {
        const { forgotMailAddress } = this.state;
        const isEmailValid = validateUtils.validateEmail(forgotMailAddress);
        this.setState(() => ({ isMailaddress: Boolean(isEmailValid) }));
        return isEmailValid;
    };

    handleBlur = field => evt => {
        this.setState({
            touched: { ...this.state.touched, [field]: true }
        });
    };

    shouldMarkError = field => {
        const { forgotMailAddress } = this.state;
        const errors = validateUtils.validateEmptyEmail(forgotMailAddress);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];

        return (
            (
                hasError ||
                validateUtils.validateEmail(forgotMailAddress) === false
            ) ? shouldShow : false
        );

    };

    setForgotMailAddress = (event) => {
        const regex = EmailConst.EMAIL_RESTRICT;
        let valueEmail = textUtils.filterWithRestrict(event.target.value, EmailConst.EMAIL_RESTRICT);
        const isCharValid = valueEmail === '' || regex.test(valueEmail);
        const { selectionEnd } =  event.currentTarget;
        const val = this.state.forgotMailAddress;

        if(valueEmail.length > 100){
            this.setState(
                {
                    "forgotMailAddress": val
                }, () => {
                    inputUtils.setCaretPosition(this.inputForgotMailAddress.current, selectionEnd);
                }
            );
        }else{
            let newValEmail = valueEmail;
            if(!isCharValid){
                newValEmail = valueEmail.slice(0, selectionEnd - 1) + valueEmail.slice(selectionEnd, valueEmail.length);
                this.setState(
                    {
                        "forgotMailAddress": val
                    }, () => {
                        let cursorPos = val.length;
                        if(isCharValid){
                            cursorPos = selectionEnd;
                        }else {
                            cursorPos = selectionEnd - 1;
                        }
    
                        inputUtils.setCaretPosition(this.inputForgotMailAddress.current, cursorPos);
                    }
                );
            }else{
                this.setState(
                    {
                        "forgotMailAddress": newValEmail
                    }, () => {
                        let cursorPos = valueEmail.length;
                        if(isCharValid){
                            cursorPos = selectionEnd;
                        }else {
                            cursorPos = selectionEnd - 1;
                        }
    
                        inputUtils.setCaretPosition(this.inputForgotMailAddress.current, cursorPos);
                    }
                );
            }
        }
    }

    forgotform_handleSubmit = (evt) => {
        evt.preventDefault();
        const { resetPassword_requestHandler } = this.props;
        const { forgotMailAddress } = this.state;
        const isEmailValid = this.shouldMarkEmailError();
        this.setState((touched) => {
            return {
                touched: {
                    ...touched,
                    forgotMailAddress: true
                }
            }
        });
        this.setState(() => ({
            touchedEmailField: true,
        }), () => {
            if (isEmailValid === true) {
                resetPassword_requestHandler(forgotMailAddress);
            }
        });
    }

    resetInputForgotMailAddress = () => {
        const { t } = this.props;
        this.setState(() => ({ forgotMailAddress: '' }));
        this.inputForgotMailAddress.current.placeholder = t('login.login.mailaddress');
    }

    UNSAFE_componentWillReceiveProps({isMessageForgotPassword}){
        if(
            isMessageForgotPassword && 
            isMessageForgotPassword !== this.props.isMessageForgotPassword
        ){
            if(isMessageForgotPassword){
                this.resetInputForgotMailAddress();
                this.setState((touched) => {
                    return {
                        touched: {
                            ...touched,
                            forgotMailAddress: false
                        }
                    }
                });
            }
        }
    }

    render() {
        const { t, toggleForgotPasswordScreen } = this.props;
        const { forgotMailAddress, touched } = this.state;
        
        let isEmptyEmailInput = touched['forgotMailAddress'] && forgotMailAddress.length === 0;
        let isNotMailaddress = touched['forgotMailAddress'] && forgotMailAddress.length !== 0 && !validateUtils.validateEmail(forgotMailAddress);

        return (
            <div className="container-fluid d-flex align-items-center justify-content-center login">
                <div className="login-content forgot-password-content">
                    <div className="login-logo">
                        <img src={loginLogo} alt="logo-login" className="img-fluid noselect" />
                    </div>
                    <div className="text-center">
                        <h1 className="login-tlt mb-3 font-din-light noselect noselect-logo">{t('login.login.fogotpassword')}</h1>
                    </div>
                    <p className="line-height-1rem noselect noselect-color-white" dangerouslySetInnerHTML={{ __html: t('login.forgotpassword.title') }}></p>
                    <div className="login-form">
                        <form onSubmit={this.forgotform_handleSubmit}>
                            <div className="mb-3 mt-3 position-relative">
                                <input
                                    ref={this.inputForgotMailAddress}
                                    value={forgotMailAddress}
                                    onChange={e => this.setForgotMailAddress(e)}
                                    type="text"
                                    maxLength="100"
                                    className={'form-control form-control-shadow' + (forgotMailAddress === '' ? ' input-empty' : '') + (isEmptyEmailInput || isNotMailaddress ? " is-invalid" : "")} //add [is-invalid] to show error input
                                    onBlur={this.handleBlur("forgotMailAddress")}
                                    placeholder={t('login.login.mailaddress')} />
                                {
                                    isEmptyEmailInput &&
                                    <ValidateErrorMessage message={t('message.login.e0001')} classAdd={'mail-validate-error'} />
                                }
                                {
                                    isNotMailaddress &&
                                    <ValidateErrorMessage message={t('message.global.e0002')} classAdd={'mail-validate-error'}/>
                                }
                            </div>
                            <div className="form-group mb-2">
                                <button
                                    type="submit"
                                    className="btn btn-dark min-w85 noselect-color-white">{t('login.forgotpassword.send')}
                                </button>
                            </div>
                            <hr className="mb-1 mt-2" />
                            <p className="text-white d-flex align-items-center mt-8 noselect">
                                <img src={leftArrowIcon} alt="forgot-password-icon" className="forgot-password-icon noselect" width="6px" height="8px" />
                                <span onClick={toggleForgotPasswordScreen} className="color-aqua role-a">
                                    <span className="noselect-color-aqua">{t('login.forgotpassword.back')}</span>
                                </span>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTranslation()(ForgotPasswordSkin);