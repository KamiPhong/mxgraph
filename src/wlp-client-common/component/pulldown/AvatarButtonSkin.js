import React, { useRef, useState, useEffect } from 'react';
import { Dropdown } from 'antd';
import { FaCaretDown } from 'react-icons/fa';
import { withTranslation } from 'react-i18next';
import presonaluser from 'wlp-client-common/images/avators/noimage.png';
import ActionLogger from 'wlp-client-common/ActionLogger';
import ImageDefault from 'wlp-client-common/utils/ImageDefault';

const AvatarButtonSkin = ({ overlay, user }) => {
    let { picture, name } = user;
    const [activeBtnFlag, setActiveBtnFlag] = useState(false);
    const avatarBtn = useRef(null);

    useEffect(() => {
        if(avatarBtn.current){
            if(activeBtnFlag){
                ActionLogger.click('Mypage_Header', 'Avatar_Button');
                avatarBtn.current.focus();
            }else{
                avatarBtn.current.blur();
            }
        }
    },[activeBtnFlag, avatarBtn]);

    return (
        <div className="dropdown mr-3 dropdown-profile">
            <Dropdown
                className="btn dropdown-toggle btn-sm pt-1 pb-1"
                trigger={['click']}
                overlayClassName="avatar-btn-menu"
                onVisibleChange={setActiveBtnFlag}
                overlay={overlay}>
                <div
                    ref={avatarBtn}
                    tabIndex={1}
                    className="btn profile-dropdown dropdown-toggle btn-sm pt-1 pb-1 remove-arrow-dropdown btn-custom-links">
                    <img alt="1" className="mr-2 img-thumbnail noselect" src={
                        ImageDefault[picture] ?
                            ImageDefault[picture] :
                            picture
                    } onError={(e) => e.target.src = presonaluser} width="21" />
                    <div className="avatar-btn-text placeholder text-truncate noselect noselect-color-white">{name}</div>
                    <FaCaretDown />
                </div>
            </Dropdown>
        </div>
    )
}

export default withTranslation()(AvatarButtonSkin);