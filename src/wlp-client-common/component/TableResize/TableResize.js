import React from 'react';
import ReactDOM from 'react-dom';
import TableColumns from './utils/TableColumns';
import TableThDraw from './TableThDraw';
import { ROW_HEIGHT, TABLE_WIDTH_BALANCE, SCROLLBAR_WIDTH } from 'wlp-client-common/consts/TableConsts';

import { tableResizeEvent } from 'wlp-client-common/events/TableResizeEvent';
import { browser, getStringWidth }  from 'wlp-client-common/utils/BrowserUtil';
import { requestLoading } from 'wlp-client-common/events/RequestLoading';
import { instanceStorage } from 'wlp-client-common/utils/InstanceStorage';
import EditorClient from 'wlp-client-common/utils/EditorClientUtils';

class TableResize extends React.Component {
    scrollThin = 12;
    className = [];
    showSorting = false;
    isScrollIntoView = false;// [huyvq-bug_ominext_66469_fb#18]---Only scrollIntoView for Leader column sorting
    numberRowEmpty = 0;
    rowFocusIndex = -1;
    ROW_HEIGHT_IE = 77;
    clickOrder = false;

    constructor(props) {
        super(props);
        const {columns} = this.props;

        this.state = {
            columns: null,
            style: {},
            rowEmpty: 0,
            rowActivatedIndex: -1,
            rowActivatedValue: null,
            scrollLeft: 0,
            scrollTop: 0,
            scrollBarWidth: 0,
        };

        this.minWidth = 20;
        this.scrollBarThin = 0;
        this.windowHeight = 0
        this.windowWidth = 0

        if (props.className) {
            this.className = props.className.split(' ');
        }
        
        this.className.push('table-resizable no-margin noselect');

        if( typeof columns !== 'undefined'){
            this.columns = new TableColumns(columns);
        }

        this.tooltipHead = React.createRef();
        this.tableContainerRef = Element => {
            this.tableContainer = Element;
            if (this.tableContainer) {
                this.tableContainer.addEventListener("scroll", this.onScrollHandler);
            }
        };
        this.tableDataRef = Element => { this.tableData = Element; }
        this.tableHeaderRef = Element => { this.tableHeader = Element; }

        this.loadingRef = React.createRef();
    }

    componentDidMount = () => {
        this.windowHeight = window.innerHeight
        this.windowWidth = window.innerWidth
        window.addEventListener('resize', this.onWindowResizeHandler);
        this.initStyle();
        this.onDataLoadingHandler();
        this.restoreEventDataFromCached();
    }

    componentDidUpdate = (prevProps, prevState) => {
        this.windowHeight = window.innerHeight
        this.windowWidth = window.innerWidth
        if (this.state !== prevState) {
            if (this.tableContainer.scrollTop !== prevState.scrollTop && this.state.scrollTop !== this.tableContainer.scrollTop && !this.clickOrder && this.props.isShowListMap) {
                switch (browser && browser.name.toLowerCase()) {
                    case "ie":
                    case 'edge':
                        this.tableContainer.scrollTop = prevState.scrollTop;
                        break;
                    default:
                        this.tableContainer.scroll({top: prevState.scrollTop});
                        break;
                }
            }
            if (this.clickOrder && this.tableContainer.getElementsByTagName('table')[0].getElementsByTagName('tbody')[0].lastChild.className !== 'row-empty') {
                const { sortOrder } = this.props;
                if (sortOrder.toString() === 'ASC') {
                    switch (browser && browser.name.toLowerCase()) {
                        case "ie":
                        case 'edge':
                            this.tableContainer.scrollTop = this.tableContainer.scrollHeight;
                            break;
                        default:
                            this.tableContainer.scroll({top: this.tableContainer.scrollHeight});
                            break;
                    }
                }
            }
        }

        if (this.props !== prevProps) {
            const { dataProvider } = this.props;
            const { isScrollIntoView } = this;
    
            if (this.tableData && typeof this.tableData.scrollIntoView === 'function' && isScrollIntoView) {
                this.tableData.scrollIntoView();
            }

            if (this.props.maxWidth !== prevProps.maxWidth) {
                this.columns.setContainerWidth(this.props.maxWidth);
            }
    
            this.checkRowEmpty(dataProvider);
            this.rowFocusIndex = -1;

            // this.onSetScrollVerticalHandler();

            

            if( dataProvider.length > 0 ){
                this.checkRowEmpty(dataProvider);
                this.updateTableState();
            }
            if (this.props.maxWidth !== prevProps.maxWidth) {
                this.updateTableState();
            }
        }
    }

    componentWillUnmount = () => {
        window.removeEventListener('resize', this.onWindowResizeHandler);
        requestLoading.removeListener(requestLoading.IS_LOADING);

        if (this.tableContainer) {
            this.tableContainer.removeEventListener("scroll", this.onScrollHandler);
        }

        const { rowActivatedIndex, rowActivatedValue, scrollLeft, scrollTop } = this.state;
       
        const { name } = this.props;
        if (typeof name !== 'undefined') {
            const dataUmount = {
                rowActivatedIndex: rowActivatedIndex,
                rowActivatedValue: rowActivatedValue,
                scrollLeft: scrollLeft,
                scrollTop: scrollTop,
                columns: this.columns
            };
            instanceStorage.storeData(name, dataUmount);    
        }
    }
 
    /**
     * initialization table/columns style
     */
    initStyle=()=>{
        const { maxWidth } = this.props;

        let table = ReactDOM.findDOMNode(this);
        this.container = table.parentNode;
        let width = 0;

        switch (browser && browser.name.toLowerCase()) {
            case "ie":
            case 'safari':
            case 'firefox':
            case 'edge':
                const computedStyle = window.getComputedStyle(table.parentNode, null);
                width = table.parentNode.clientWidth - parseFloat(computedStyle.paddingLeft) - parseFloat(computedStyle.paddingRight);
                break;
            default:
                width = table.offsetWidth;
                break;
        }

        if (typeof maxWidth === 'number') {
            width = maxWidth;
        }
        
        if (width < 1 && table ) {
            width = table.parentNode.clientWidth
        }

        if (typeof width === 'undefined') {
            return;
        }

        this.columns.setContainerWidth(width);
    }

    /**
     * restore data [tableResizeEvent.ON_UMMOUNT_TABLE]
     * @param {object} data 
     */
    
    restoreEventDataFromCached = () => {
        const { name } = this.props;
        if (typeof name !== 'undefined') {
            const data = instanceStorage.getStoreData(name);
            const { rowActivatedIndex, rowActivatedValue, columns } = data;
            const currentColumns = this.columns.getColumns();
            /**
             * reverse columns data from cached
             */
            if( columns instanceof TableColumns){
                this.columns = columns;
                this.columns.getColumns().map((col, index) => {
                    col.label = currentColumns[index].label;
                });
            }

            /**
             * reverse row activated/scroll from cached
             */
            if (typeof rowActivatedIndex !== 'undefined' && typeof rowActivatedValue !== 'undefined') {
                this.setState({ rowActivatedIndex: rowActivatedIndex, rowActivatedValue: rowActivatedValue }, () => {
                    this.reverseScroll();
                });
            }
        }
    }

    reverseScroll = () => {
        const { name } = this.props;
        const data = instanceStorage.getStoreData(name);
        const { scrollLeft, scrollTop } = data;

        if (scrollTop > 0) {
            this.isScrollIntoView = false;

            switch (browser && browser.name.toLowerCase()) {
                case "ie":
                case 'edge':
                    this.tableContainer.scrollTop = scrollTop;
                    break;
                default:
                    this.tableContainer.scroll({
                        top: scrollTop,
                        left:scrollLeft
                      });
                    break;
            }
        }
        
    }

    /**
     * listing request events to show/hide loading
     */
    onDataLoadingHandler = () => {
        requestLoading.addListener(requestLoading.IS_LOADING, () => {
            if (this.loadingRef.current) {
                this.loadingRef.current.classList.remove('d-none');
            }
            if (this.tableData) {
                this.tableData.classList.add('loading');
            }
        });
        requestLoading.addListener(requestLoading.FINISH_LOADING, () => {
            if (this.loadingRef.current) {
                this.loadingRef.current.classList.add('d-none');
            }
            if (this.tableData) {
                this.tableData.classList.remove('loading');
            }

        });
    }

    updateTableState = () => {
        const style = { width: this.columns.getWidthSum() };
        this.setState(style,()=>{
            this.showScrollBarHandler();
            this.setHeaderStyleHandler();
            this.setBodyStyleHandler();           
        });
    }

    /**
     * check data to set numberRowEmpty
     * @param {itemsData}} dataProvider 
     */
    checkRowEmpty = (dataProvider) => {
        let rowHeightDefault = ROW_HEIGHT;
        const { maxHeight, rowHeight } = this.props;
        
        if (typeof rowHeight !== 'undefined') {
            rowHeightDefault = rowHeight;
        }
        if (typeof dataProvider === 'undefined') {
            dataProvider = this.props.dataProvider;
        }

        if (dataProvider && dataProvider.length < 1) {
            this.numberRowEmpty = 0;
            return;
        }

        if (typeof maxHeight !== 'undefined' && maxHeight) {
            this.numberRowEmpty = Math.round((maxHeight) / rowHeightDefault) - dataProvider.length;
        } else if (!this.tableContainer) {
            const area = ReactDOM.findDOMNode(this).parentNode;
            let groupHeight = 0;
            area.children.forEach(node => {
                groupHeight += node.offsetHeight;
            })
            
            this.numberRowEmpty = Math.round((area.offsetHeight - groupHeight) / rowHeightDefault);
            if (dataProvider) {
                this.numberRowEmpty -= dataProvider.length;
            }

        } else {
            this.numberRowEmpty = Math.round((this.tableContainer.offsetHeight) / rowHeightDefault) - dataProvider.length;
        }

    }

    /**
     * handler on click table header
     * @param {Number} index 
     * @param {TableColumn} col 
     * @param {MouseEvent} event 
     */
    headerOnClickHandler = (index, col, event) => {
        //[huyvq-bug_ominext_66469_fb#18]--- only need scrollIntoView if user clicks sorting the leader column (the current view follows the leader row)
        //to prevent scroll scroll bar jumping to bottom everytime component did mount
        if(col.name === 'leaderFlg') {
            this.isScrollIntoView = true
        }
        let target = event.target;
        if (target.nodeName === 'SPAN' || target.nodeName === 'DIV') {
            if (target.classList.contains('th-resizing')) {
                return;
            }
            target = target.closest('th');
        }

        if (!this.props.dataProvider || this.props.dataProvider.length < 1 || col.sortable !== true) {
            event.preventDefault();
            return;
        }

        this.showSorting = true;

        const data = {
            target: target,
            targetGroup: target.parentNode,
            index: index,
            column: this.columns.getColumn(index)
        };
        
        tableResizeEvent.fireEvent(tableResizeEvent.TABLE_HEADER_CLICK, data);
    }
    

    /**
     * handler event on column resizing
     * @author QuanNH
     * @param {Number} index 
     * @param {Number} newWidth 
     */
    onResizingHandler = (index, newWidth) => {
        this.columns.getColumn(index).setWidth(newWidth, index);
        this.updateTableState();
        this.setHeaderStyleHandler();
    }

    /**
     * handler event on window resizing
     * @author QuanNH
     */
    onWindowResizeHandler = () => {
        if (
            this.props.name === 'memberDg' || 
            window.innerHeight === this.windowHeight ||
            (window.innerHeight != this.windowHeight && window.innerWidth != this.windowWidth)
        ) {
            const {onWindowResizeHandler} = this.props;
            if (typeof onWindowResizeHandler === 'function') {
                onWindowResizeHandler();
            } else {
                this.setTableWidth();
            }
        } else {
            this.windowHeight = window.innerHeight
            this.windowWidth = window.innerWidth
        }
    }

    /**
     * update table width on window resizing
     * @author QuanNH
     */
    setTableWidth = () => {
        const { maxWidth } = this.props;

        let containerWidthValue = 0;
        if (typeof maxWidth === 'number') {
            containerWidthValue = maxWidth;
        } else if (this.tableContainer) {
            const area = this.tableContainer.parentNode;
            containerWidthValue = area.offsetWidth;
        }

        this.columns.setContainerWidth(containerWidthValue);
        this.updateTableState();
    }

    onScrollHandler = (event) => {
        const {scrollLeft, scrollTop} = event.target;
        this.clickOrder = false;
        this.setState({
            scrollLeft: scrollLeft,
            scrollTop: scrollTop
        },()=>{
            this.setHeaderStyleHandler();
        });
    }

    /**
     * do render table body
     */
    renderBody = () => {
        const { dataProvider, rowKey } = this.props;
        const { rowActivatedIndex, rowActivatedValue } = this.state;
        let body = <tr />;
        if (dataProvider && dataProvider.length > 0) {
            return dataProvider.map((row, rowIndex) => {
                let className = 'noselect';
                if (typeof rowKey !== 'undefined') {
                    className = rowActivatedIndex === rowIndex && row[rowKey] === rowActivatedValue ? 'row-active' : '';
                }
                if (this.rowFocusIndex === rowIndex) {
                    className += ` row-focus`;
                }

                return <tr key={rowIndex} onClick={(e) => this.setRowActivated(rowIndex, e)} className={className} >
                    {this.columns.getColumns().map((field, colIndex) => {
                        let className = 'white-space-pre ';
                        if (typeof field.className !== 'undefined') {
                            className += field.className;
                        }
                        return <td key={colIndex} className={className}>{field.render(row, rowIndex)}</td>
                    })}
                </tr>
            });
        }
        return body;
    }

    setRowActivated = (rowIndex, event) => {
        if (rowIndex < 0) {
            this.setState({ rowActivatedIndex: -1, rowActivatedValue: null });
            return;
        }

        if (event.target.nodeName === 'A') {
            return;
        }

        const { dataProvider, rowKey } = this.props;
        if (event.type === 'click') {
            this.rowFocusIndex = rowIndex;
        }

        if (rowKey && dataProvider[rowIndex][rowKey]) {
            this.setState({
                rowActivatedIndex: rowIndex,
                rowActivatedValue: dataProvider[rowIndex][rowKey]
            });
        }
    }

    removeRowFocus = () => {
        if (this.tableContainer) {
            this.rowFocusIndex = -1;
            this.tableContainer.getElementsByClassName('row-focus').forEach(e => {
                e.classList.remove('row-focus');
            });
        }
    }

    renderRowEmpty = (number) => {
         const { rowHeight } = this.props;
        if (typeof number === 'undefined') {
            number = this.numberRowEmpty;
        }
        let rows = [];
        if (number > 0) {
            for (let i = 0; i <= number; i++) {
                let style = {};
                if (typeof rowHeight !== 'undefined') {
                    style.height = rowHeight;
                }
                let row = <tr key={i} className="row-empty" style={style}>
                    {this.columns.getColumns().map((field, index) => {
                        return <td key={index} />;
                    })}
                </tr>;
                rows.push(row);
            }
        }
        return <React.Fragment>
            {rows}
        </React.Fragment>

    }
    
    handleShowTooltip = (evt, label, showTooltip) => {
        const { isShowTooltip } = this.props;
        if (isShowTooltip) {
            if (label && this.tooltipHead) {
                const rect = evt.target.getBoundingClientRect();
                const paddingParent = 4;
                const marginParentToTooltip = 30;
                const marginParentToRight = 40;
                const textWidth = getStringWidth(label, "11px") + paddingParent;

                if (textWidth > evt.target.clientWidth && showTooltip) {
                    this.tooltipHead.current.innerHTML = label;
                    this.tooltipHead.current.style.display = "block";
                    if (textWidth + evt.screenX > window.innerWidth - marginParentToRight) {
                        this.tooltipHead.current.style.right = "5px";
                    } else {
                        this.tooltipHead.current.style.left = evt.clientX + "px";
                    }
                    this.tooltipHead.current.style.top = rect.top + marginParentToTooltip + "px";
                }
            }
        }
    }

    handleHidetooltip = () => {
        if (this.tooltipHead) {
            this.tooltipHead.current.style.display = "none";
            this.tooltipHead.current.innerHTML = "";
            this.tooltipHead.current.style.left = "unset";
            this.tooltipHead.current.style.right = "unset";
        }
    }

    scrollIntoView = (isScroll) => {
        this.isScrollIntoView = isScroll;
    }

    setScroll = (opt) => {
        opt = {
            block: 'center',
            inline:'center'
        };
    }

    /**
     * add class to hide/show scroll bar table body
     */
    showScrollBarHandler=()=>{
        const {columns, tableContainer, numberRowEmpty} = this;
        const {dataProvider} = this.props;
        const classNameNoScrollVertical = 'scroll-x-none';
        const classNameNoScrollHorizontal = 'scroll-y-none';

        if (tableContainer instanceof HTMLElement !== true) {
            return;
        }
        
        if (columns.getContentWidth() <= columns.containerWidth + TABLE_WIDTH_BALANCE) {
            tableContainer.classList.add(classNameNoScrollVertical);
            
        } else {
            tableContainer.classList.remove(classNameNoScrollVertical);
        }

        let scrollBarThin = SCROLLBAR_WIDTH;
        
        if (EditorClient.IS_IE) {
            if (this.props.dataProvider.length * this.ROW_HEIGHT_IE > this.props.maxHeight) {
                tableContainer.classList.remove(classNameNoScrollHorizontal);
                this.numberRowEmpty = 0;
            }
            else {
                tableContainer.classList.add(classNameNoScrollHorizontal);
                scrollBarThin = 0;
            }
        }
        else {
            tableContainer.classList.remove(classNameNoScrollHorizontal);
            if (dataProvider.length < 1 || numberRowEmpty > 0) {
                tableContainer.classList.add(classNameNoScrollHorizontal);
                scrollBarThin = 0;
            }
        }

        let isUpdate = this.columns.setScrollThin(scrollBarThin);
        if( isUpdate ){
            this.updateTableState();
        }
    }

    /**
     * set style for table content
     */
    setBodyStyleHandler = () => {
        const {maxHeight, minWidth } = this.props;
        const {tableContainer} = this;

        if (tableContainer instanceof HTMLElement !== true) {
            return;
        }

        let styles = {
            maxWidth : `${this.columns.getWidthSum()}px`
        };
        if (typeof maxHeight !== 'undefined') {
            styles.maxHeight = `${maxHeight}px`;
        }
        if (typeof minWidth !== 'undefined') { 
            styles.minWidth = `${minWidth}px`;
        }
        Object.assign(tableContainer.style, styles);
    }

    /**
     * set style for table header
     */
    setHeaderStyleHandler = () => {
        const {scrollLeft} = this.state;
        const {tableHeader} = this;
        
        if (tableHeader instanceof HTMLElement !== true) {
            return;
        }

        let styles = { marginLeft:0 };

        if (typeof scrollLeft === 'number' ) {
            styles.marginLeft = `-${scrollLeft}px`;
        }

        styles.width = `${this.columns.getWidthSum()}px`;
        Object.assign(tableHeader.style, styles);
    }

    render=()=>{
        const { sortOrder, sortIndex, dataProvider} = this.props;
        const { columns } = this;
        
        let headerClassName = (browser && browser.name.toLowerCase() === 'ie') ? 'table-th-ie noselect noselect-color-757171' : '';
        
        if( typeof columns === 'undefined' || columns instanceof TableColumns !== true ){
            return <table/>;
        }

        const columnsData = columns.getColumns();
        const columnCount = columns.countColumn();
        return (
            <React.Fragment>
                <div className="table-datagrid-header">
                    <table className={this.className.join(' ')} ref={this.tableHeaderRef} >
                        <thead>
                            <tr>
                                {columnsData.map((col, index) => {
                                    let ordering = <i className="d-none" />;
                                    let styleColumn = col.getStyle();
                                                                       
                                    if (index === columnCount - 1) {
                                        styleColumn.minWidth = styleColumn.minWidth + columns.getScrollThin();
                                    }

                                    let thClassName = headerClassName;

                                    if (typeof sortOrder !== 'undefined' && sortIndex && sortIndex === col.softIndex && this.showSorting) {
                                        let orderingStyle = {};
                                        if (columnCount === index + 1) {
                                            orderingStyle.right = 25;
                                        }
                                        ordering = <i style={orderingStyle} className={sortOrder.toString() === 'DESC' ? "fa fa-caret-down" : "fa fa-caret-up"} aria-hidden="true"></i>;
                                        thClassName += ' sorting';
                                    }

                                    return <th 
                                        key={index} 
                                        className={thClassName} 
                                        style={styleColumn} 
                                        onClick={(event) => this.headerOnClickHandler(index, col, event)} 
                                    >
                                        <span className="noselect noselect-color-757171"
                                            style={{ width: col.defaultWidth - 15, maxWidth: col.defaultWidth - 15, }}
                                            onMouseOver={(e) => this.handleShowTooltip(e, col.label, dataProvider.length)}
                                            onMouseLeave={this.handleHidetooltip}
                                        >
                                            {col.label}
                                        </span>
                                        {ordering}
                                        <TableThDraw columnIndex={index} resizeCallback={this.onResizingHandler} />
                                    </th>;
                                })}
                            </tr>
                        </thead>
                    </table>
                </div>

                <div className={this.props.name === "memberDg" ? "table-datagrid table-member-dg" : "table-datagrid"} ref={this.tableContainerRef} >
                    <table className={this.className.join(' ')} ref={this.tableDataRef} >
                        <thead>
                            <tr>
                                {columnsData.map((col, index) => {
                                    let styleColumn = col.getStyle();
                                    return <th 
                                        key={index} 
                                        className={headerClassName} 
                                        style={styleColumn} 
                                        onClick={(event) => this.headerOnClickHandler(index, col, event)} 
                                    />;
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.children}
                            {this.renderBody()}
                            {this.renderRowEmpty()}
                        </tbody>
                    </table>
                    <div className="tooltip-head" ref={this.tooltipHead} ></div>
                    <div className="table-loading-wrapper d-none" ref={this.loadingRef}>
                        <div className="table-data-loading" />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default TableResize;