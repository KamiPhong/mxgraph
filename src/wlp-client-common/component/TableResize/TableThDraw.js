import React from 'react';

class TableThDraw extends React.Component {
    state = {
        targetColumn: null,
        targetColumnPageX: 0,
        isPressed: false,
        isTablet: 'ontouchstart' in document.documentElement ? true : false
    }

    componentDidMount() {
        document.addEventListener('mouseup', this.documentMouseUpEvent);
        document.addEventListener('mousemove', this.documentMouseMoveEvent);
        document.addEventListener('touchend', this.documentTouchEndEvent)
        document.addEventListener('touchmove', this.documentTouchMoveEvent)
    }

    componentWillUnmount() {
        document.removeEventListener('mouseup', this.documentMouseUpEvent, false);
        document.removeEventListener('mousemove', this.documentMouseMoveEvent, false);
    }


    /**
     * Set variables calculate
     */
    onMousedownHandler = (event) => {
        this.onResizeStartHandler(event, false)
    }

    onTouchStartHandler = (event) => {
        this.onResizeStartHandler(event, true)
    }

    onResizeStartHandler = (event, isTablet) => {
        const touch = isTablet ? (event.touches[0] || event.changedTouches[0]) : null
        const target = event.currentTarget;
        let th = target.parentNode.previousSibling;
        let padding = 0;

        let newState = {
            targetColumn: th,
            targetColumnWidth: th.offsetWidth - padding,
            targetColumnPageX: isTablet ? touch.pageX : event.pageX,
            isPressed: true
        }

        this.setState(newState)
    }

    /**
     * reset variables calculate
     */
    documentMouseUpEvent = (event) => {
        this.setState({
            targetColumn: null,
            targetColumnPageX: 0,
            isPressed: false,
        });
    }

    documentTouchEndEvent = (event) => {
        this.setState({
            targetColumn: null,
            targetColumnPageX: 0,
            isTouched: false,
        });
    }

    /**
     * 
     * mouse move document
     */
    documentMouseMoveEvent = (event) => {
        this.documentOnResizeEvent(event, false)
    }

    documentTouchMoveEvent = (event) => {
        this.documentOnResizeEvent(event, true)
    }

    documentOnResizeEvent = (event, isTablet) => {
        let { targetColumn, targetColumnPageX, targetColumnWidth, isTouched, isPressed } = this.state;
        const touch = isTablet ? (event.touches[0] || event.changedTouches[0]) : null

        if (isPressed) {
            let { resizeCallback, columnIndex } = this.props;

            if (targetColumn === null) {
                return;
            }

            if (targetColumn != null) {
                let resize = (isTablet ? touch.pageX : event.pageX) - targetColumnPageX;

                if (typeof resizeCallback === 'function') {
                    resizeCallback(columnIndex - 1, targetColumnWidth + resize, resize)
                }
            }
        }
    }

    render() {
        let { columnIndex } = this.props;
        let thDrawStyle = this.state.isTablet ? {
            width: '50px',
            position: 'absolute',
            left: '-22px',
            top: '0px',
            zIndex: '10'
        } : {}

        if (columnIndex < 1) {
            return <React.Fragment></React.Fragment>
        }
        return (
            <div className="th-resizing"  onMouseDown={this.onMousedownHandler} onTouchStart={this.onTouchStartHandler}  style={thDrawStyle}>
                {this.state.isTablet && (
                    <img src="/mypage/resize.svg" style={{height: '29px'}}/>
                )}
            </div>
        );
    }
}

export default TableThDraw;