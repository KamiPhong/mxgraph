import TableColumn from './TableColumn';
import { COLUMN_MIN_WIDTH, TABLE_WIDTH_BALANCE } from 'wlp-client-common/consts/TableConsts';
import { tableResizeEvent } from 'wlp-client-common/events/TableResizeEvent';

export default class TableColumns {

    constructor(columns, containerWidth) {
        this.columns = [];
        this.width = 0;
        this.containerWidth = 0;
        this.scrollThin = 0;

        if( typeof columns !== 'undefined'){
            this.init(columns);
            if( typeof containerWidth !== 'undefined'){
                this.setContainerWidth(containerWidth);
            }
        }
    }

    /**
     * initialization columns object
     * @param {array} columns 
     */
    init = (columns) => {
        tableResizeEvent.addListener(tableResizeEvent.DO_RESIZE, this.calWidth);
        
        let softIndex = 0;
        for (let [key, col] of Object.entries(columns)) {
            if (col.sortable !== false) {
                softIndex++;
                col.softIndex = softIndex;
            }
            this.columns.push(new TableColumn(key,col));
        };

    }

    setContainerWidth = (containerWidth) => { 
        this.containerWidth = parseFloat(containerWidth);
        tableResizeEvent.fireEvent(tableResizeEvent.DO_RESIZE);
    }

    calWidth = (index, newWidth) => { 
        if( typeof this.containerWidth !== 'number'){
            return;
        }

        let widthByDefault = 0;
        let colWidthAutoCount = 0;
        let width = 0;
        const {containerWidth, scrollThin} = this;

        this.columns.forEach(col => {
            if (col.defaultWidth > 0) {
                widthByDefault += col.defaultWidth;
            } else { 
                colWidthAutoCount ++;
            }
        });

        let colWidthByAuto = (this.containerWidth - widthByDefault - this.scrollThin) / colWidthAutoCount;

        if (colWidthByAuto < COLUMN_MIN_WIDTH) {
            colWidthByAuto = COLUMN_MIN_WIDTH;
        }

        this.columns.forEach((col, index) => {

            /**
             * set width for column had't default value
             */

            if (col.defaultWidth < 1 && colWidthByAuto > 0 ) {
                col.width = colWidthByAuto;

                if( col.minWidth > colWidthByAuto){
                    col.width = col.minWidth;
                }
            }
            width += col.width;
        });
       
        if( width + scrollThin < containerWidth-TABLE_WIDTH_BALANCE ){
             /**
             * reset default width of last column
             */
            const lastColumn = this.columns[this.columns.length-1];
            width -= lastColumn.width;
            lastColumn.width = containerWidth-width;
            lastColumn.defaultWidth = 0;
            width += lastColumn.width;
        }

        this.width = width;
    }

    getColumns = ()=>{
        return Object.values(this.columns);
    }

    getColumn = (index) => {
        const items = Object.values(this.columns);
        return items[index];
    }

    /**
     * 
     * @param {*} number 
     */
    setScrollThin=(number)=>{
        if( typeof number === 'number' && this.scrollThin !== number){
            this.scrollThin = number;
            tableResizeEvent.fireEvent(tableResizeEvent.DO_RESIZE);
            return true;
        }
        return false;
    }

    getScrollThin=()=>{
        return this.scrollThin;
    }

    getWidthSum=()=>{
        return this.width + this.scrollThin;
    }

    getContentWidth=()=>{
        return this.width;
    }

    countColumn = () => {
        return this.columns.length;
    }

}