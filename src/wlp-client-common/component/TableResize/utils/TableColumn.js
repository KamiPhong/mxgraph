import React from 'react';
import { COLUMN_MIN_WIDTH } from 'wlp-client-common/consts/TableConsts';
import { tableResizeEvent } from 'wlp-client-common/events/TableResizeEvent';


export default class TableColumn {
    unit = 'pixel';
    name = '';
    label = '';
    sortable = true;
    width = 0;
    minWidth = 0;
    defaultWidth = 0;
    itemRenderer = null;
    softIndex = -1;
    className = "";

    constructor(name, column) {
        this.name = name;        
        if( typeof column !== 'undefined' ){
            Object.keys(column).forEach(key => {
                if (typeof this[key] !== 'undefined' ) {
                    this[key] = column[key];
                }
            });

            if (this.width !== 0) {
                this.defaultWidth = this.width;
            }

            if (this.label.length < 1 && typeof column['headerText'] !== 'undefined') {
                this.label = column['headerText'];
            }

            if (typeof column['dataField'] !== 'undefined') {
                this.name = column['dataField'];
            }
        }
    }

    getStyle() {
        const widthValue = this.width ? this.width : this.defaultWidth;
        const width = this.minWidth > widthValue ? this.minWidth : widthValue;

        return {
            width:    width,
            minWidth: width,
            maxWidth: width,
        }
    }

    /**
     * event update width on resize column
     * @param {number} width 
     */
    setWidth = (width, index) => { 
        if (width < COLUMN_MIN_WIDTH) {
            width = COLUMN_MIN_WIDTH;
        }
        this.width = width;
        this.defaultWidth = width;

        tableResizeEvent.fireEvent(tableResizeEvent.DO_RESIZE, index, width);
    }

    render = (field, dataProviderIndex) => { 
        const title = field[this.name];
        if (this.itemRenderer) {
            return React.cloneElement(this.itemRenderer, {labelText: title, data: field, dataField: this.name, dataProviderIndex: dataProviderIndex})
        } else { 
            return title
        }
    }
}