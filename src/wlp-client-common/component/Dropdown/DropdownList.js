import React from 'react';
import { withTranslation } from 'react-i18next';
import Link from '../Link';

class DropdownList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDropList: false
        }
        this.wrapperRefDropList = React.createRef();
    }

    toggleDropdownList = (e) => {
        const dropdownItemHeight = window.innerHeight - e.clientY - 30;

        this.setState(({ showDropList }) => ({
            showDropList: !showDropList
        }), () => {
            if (this.wrapperRefDropList && this.wrapperRefDropList.children[1]) {
                this.wrapperRefDropList.children[1].style.maxHeight= dropdownItemHeight + 'px';
            }
        });
    }

    hideDropdownList = () => {
        this.setState({ showDropList: false });
    }

    setWrapperRefDropList = (node) => {
        this.wrapperRefDropList = node;
    }

    handleClickOutsideDivDropList = (event) => {
        if (this.wrapperRefDropList && !this.wrapperRefDropList.contains(event.target)) {
            this.setState({showDropList: false});
        }
    }

    eventSelectedDropdownItem = (contract) => {
        const { selectedItem, dropdownListItem_handleSelect, onChangeValue } = this.props;
        if (
            selectedItem &&
            selectedItem.contractId !== contract.contractId
        ) {
            dropdownListItem_handleSelect(contract);
            this.hideDropdownList();
        }

        if (onChangeValue && typeof onChangeValue === "function") {
            onChangeValue(contract);
            this.hideDropdownList();
        }
    }

    getSelectedTeamName = () => {
        const {selectedItem, itemList} = this.props;
        let selectedItemName = '';
        if(selectedItem && selectedItem.teamName){
            selectedItemName = selectedItem.teamName;
        } else if(itemList && Array.isArray(itemList) && itemList.length > 0){
            selectedItemName = itemList[0].teamName;
        }

        return selectedItemName;
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutsideDivDropList);
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutsideDivDropList);
    }

    render() {
        const { showDropList } = this.state;

        const {
            itemList,
            editorClass,
            dropDownListOfSheetCopy,
            pulldownIcon,
            className
        } = this.props;

        const selectedItemName = this.getSelectedTeamName();
        const classNameDownListOfSheetCopy = ' d-flex justify-content-between align-items-center btn-black';
        let classNameDropDown = ['btn btn-black btn-sm w-100 btn-modal-dropdown pr-important-10px'];

        if (className) {
            classNameDropDown.push(className);
        }

        if (showDropList) {
            classNameDropDown.push('actived-dropdown');
        } else {
            classNameDropDown.push('dropdown-hover');
        }
        
        return (
            <div
                ref={this.setWrapperRefDropList} 
                className={(dropDownListOfSheetCopy ? " dropdown-gr dropdown-modal" : "" )}>
                <button 
                    className={classNameDropDown.join(' ') + (dropDownListOfSheetCopy ? classNameDownListOfSheetCopy : " btn-dark" ) } 
                    onClick={(e) => this.toggleDropdownList(e)}
                    type="button" >
                    <strong className="text-truncate noselect noselect-color-white">{selectedItemName}</strong>
                    {pulldownIcon && 
                        <img src={pulldownIcon} alt="" className=" noselect noselect-color-white"></img>
                    }
                </button>
                {showDropList && 
                    <div className={"dropdown-menu-bg-black dropdown-box " + ( editorClass ? editorClass : "" ) }>
                        {   itemList && 
                            Array.isArray(itemList) &&
                            itemList.map((item, index) => {
                            const itemName = item.teamName;
                            
                            return (
                                <Link key={index} 
                                    onClick={() => this.eventSelectedDropdownItem(item)}
                                    className={"dropdown-item dropdown-item-box text-truncate noselect " + (dropDownListOfSheetCopy && index === 0 ? "dropdown-item-check border-top-0 dropdown-border-b" : "" )}>
                                        {itemName}
                                </Link>
                            )
                        })}
                    </div>
                }
            </div>
        );
    }
}

export default withTranslation()(DropdownList);