import React from 'react';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';
import LoadModalBodyEnd from '../LoadModalBodyEnd';
class Dropdown extends React.Component {
    static defaultProps = {
        hideOnBlur: true
    }

    dropdownBody = React.createRef();
    openDropdownButton = React.createRef();
    itemRef = React.createRef();

    state = {
        top: 0,
        left: 0,
        width: null,
        isShow: false
    }

    componentDidMount() {
        this.updateDropdownListPosition();
        document.addEventListener('click', this.onDocumentClick);
        window.addEventListener('resize', this.onWindowResize);
        document.addEventListener('wheel', this.onDocumentWheel);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.onDocumentClick);
        window.removeEventListener('resize', this.onWindowResize);
        document.removeEventListener('wheel', this.onDocumentWheel);
    }

    onDocumentClick = (evt) => {
        if (!checkElementClicked(evt, this.openDropdownButton) && 
            !checkElementClicked(evt, this.dropdownBody)) {
            this.doHideDropdownList();
        }
    }

    onWindowResize = () => {
        this.doHideDropdownList();
    }

    onDocumentWheel = (evt) => {
        if (!checkElementClicked(evt, this.openDropdownButton) && 
            !checkElementClicked(evt, this.dropdownBody)) {
            this.doHideDropdownList();
        }
    }

    doShowDropdownList = () => {
        this.setState({
            isShow: true
        });
    }

    doHideDropdownList = () => {
        this.setState({
            isShow: false,
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.isShow === undefined) {
            if (this.state.isShow === true && this.state.isShow !== prevState.isShow) {
                this.updateDropdownListPosition();
                this.setScrollTopActiveItem();
            }
        } else {
            if (this.props.isShow === true && this.props.isShow !== prevProps.isShow) {
                this.updateDropdownListPosition();
                this.setScrollTopActiveItem();
            }
        }
    }

    onClickButtonOpenDialog = (evt) => {
        if (typeof (this.props.onClick) === 'function') {
            this.props.onClick(evt);
        }

        this.doShowModal();
    }

    onClickButtonClose = (evt) => {
        if (typeof (this.props.onClose) === 'function') {
            this.props.onClose(evt);
        }

        this.doHideModal();
    }

    updateDropdownListPosition = () => {
        if (this.dropdownBody && this.openDropdownButton) {
            let top = 0;
            
            const buttonBounds = this.openDropdownButton.getBoundingClientRect();

            let left = buttonBounds.left;

            // tính vùng hiển thị
            const bodyHeight = document.body.clientHeight;
            const dropdownBound = this.dropdownBody.getBoundingClientRect();
            if (buttonBounds.top + buttonBounds.height +  dropdownBound.height > bodyHeight) {
                top = buttonBounds.top - dropdownBound.height;
            } else {
                top = buttonBounds.top + buttonBounds.height;
            }

            this.setState({
                top: top,
                left: left,
                width: buttonBounds.width
            });
        }
    }

    toggleDropdownList = (evt) => {
        if (this.state.isShow) {
            this.doHideDropdownList();
        } else {
            this.doShowDropdownList();
        }
    }

    onClickItem = (index, item) => {
        if (typeof this.props.onClickItem === 'function') {
            this.props.onClickItem(index, item);
        }

        this.doHideDropdownList();
    }

    setScrollTopActiveItem = () => {
        const {itemList, selectedItemName} = this.props;
        if (this.itemRef instanceof HTMLElement) {
            const itemBound = this.itemRef.getBoundingClientRect();
            for (let i = 0; i < itemList.length; i++) {
                if (itemList[i].label === selectedItemName){
                    const scrollTop = itemBound.height * i;
                    this.dropdownBody.scrollTop = scrollTop
                }               
            }
        }
    }

    render() {
        const { isShow, itemList, bodyClassName, selectedItemName, buttonClassName, activeButtonClassName, className } = this.props;

        const show = isShow !== undefined ? isShow : this.state.isShow;
        const { width } = this.state;
        let dropdownWidth = width ? width : '';
        let bodyClasses = [''];
        let buttonClasses = ['noselect noselect-color-464646'];
        let dropdownItemClasses = ['dropdown-item'];
        let classes = [''];

        if (show === true) {
            bodyClasses.push('d-block show');
            if (activeButtonClassName) {
                buttonClasses.push(activeButtonClassName)
            }
        }
        if (bodyClassName) {
            bodyClasses.push(bodyClassName);
        }
        if (buttonClassName) {
            buttonClasses.push(buttonClassName);
        }
        if (className) {
            classes.push(className);
        }

        return (<React.Fragment>
            <button
                className={buttonClasses.join(' ')}
                onClick={this.toggleDropdownList}
                ref={r => this.openDropdownButton = r}
                type="button" >
                {selectedItemName}
            </button>
            <LoadModalBodyEnd>
                <div className={classes.join(' ')}>
                    <div
                        className={bodyClasses.join(' ')}
                        ref={r => this.dropdownBody = r}
                        style={{
                            position: 'absolute',
                            top: this.state.top,
                            left: this.state.left,
                            width: dropdownWidth,
                        }}
                    >
                        {itemList &&
                            Array.isArray(itemList) &&
                            itemList.map((item, index) => {
                                const itemName = item.label;
                                let classes = [dropdownItemClasses.join(' ')];
                                if (selectedItemName === itemName) {
                                    classes.push('active');
                                }
                                return (
                                    <div ref={r => this.itemRef = r} key={index}
                                        onClick={() => this.onClickItem(index, item)}
                                        className={classes.join(' ')}>
                                        {itemName}
                                    </div>
                                )
                            })}
                    </div>
                </div>

            </LoadModalBodyEnd>
        </React.Fragment>);
    }
}

export default Dropdown;