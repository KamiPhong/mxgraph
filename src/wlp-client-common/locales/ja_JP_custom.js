export default ({
    'mypage.teamSettings.accountStatus.usage.unit.person':'人',
    'mypage.slider.next': `次へ`,
    'mypage.slider.previous': `前へ`,
    'mypage.slider.button.now': `今すぐ始める`,
})