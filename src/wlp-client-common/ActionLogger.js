class ActionLogger {
    static actionQueue  = [];

    static formatLog(actionType, screenName, targetName) {
        return `[${actionType}][${screenName}]->[${targetName}]`;
    }

    static logAction(actionType, screenName, targetName){
        if (this.actionQueue.length === 10) {
            this.actionQueue.shift();
        }

        this.actionQueue.push(ActionLogger.formatLog(actionType, screenName, targetName));
        localStorage.setItem('ActionLogger', JSON.stringify(this.actionQueue));
    }

    static click(screenName, targetName) {
        ActionLogger.logAction('Click', screenName, targetName);
    }

    static typing(screenName, targetName) {
        ActionLogger.logAction('Type', screenName, targetName);
    }

    static submit(screenName, targetName) {
        ActionLogger.logAction('Submit', screenName, targetName);
    }
}

let prevActions =  localStorage.getItem('ActionLogger');

if (prevActions) {
    ActionLogger.actionQueue = JSON.parse(prevActions);
}


export default ActionLogger;