export const EDITOR_PATH_PREFIX = '/editor/map';
export const DOWNLOAD_PATH_PREFIX = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_API_HOST + '/download' : '/download';
export const UPLOAD_EXPORT_FILE_PATH_PREFIX = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_API_HOST + '/uploadExportFile' : '/uploadExportFile';
export const DISABLE_UPLOAD_EXPORT_FILE_PATH_PREFIX = '/disableUploadExportFile';
export const MY_PAGE_PATH_PREFIX = '/mypage';
