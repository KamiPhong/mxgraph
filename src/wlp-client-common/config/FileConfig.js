export default {
    maxFileUploadSize: 104857600, //100MB
    maxFileAvatarUploadSize: 5242880, //5MB
    bannedFileNameChars: /~/,
    isFileExtValid: /(.*?)\.(jpg|jpeg|png|gif)$/
}