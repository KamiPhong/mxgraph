const LOG_CONSOLE = false;

function logConsole(...agrs){
    if(LOG_CONSOLE){
        console.log(...agrs);
    }
}

const Logger = {
    logConsole
}

export default Logger;