import EventCenter from "./EventCenter";

class RequestLoading extends EventCenter {
    FINISH_LOADING = 'finish_loading';
    IS_LOADING = 'is_loading';
    
    constructor() {
        super();
        if (RequestLoading.instance) {
            return RequestLoading.instance;
        }

        RequestLoading.instance = this;
    }
}

const requestLoading = new RequestLoading();

export { requestLoading };