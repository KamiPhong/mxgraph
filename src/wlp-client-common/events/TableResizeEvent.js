import EventCenter from "wlp-client-common/events/EventCenter";


class TableResizeEvent extends EventCenter {
    DO_RESIZE = "table_do_resize";
    TABLE_HEADER_CLICK = 'table_header_click';

    constructor() {
        super();
        if (TableResizeEvent.instance) {
            return TableResizeEvent.instance;
        }

        TableResizeEvent.instance = this;
    }
}

const tableResizeEvent = new TableResizeEvent();

export { tableResizeEvent, TableResizeEvent }