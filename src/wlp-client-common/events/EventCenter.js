class EventCenter {
    eventList = {}

    isEventNameValid = (eventName) => {
        return this.eventList[eventName] && Array.isArray(this.eventList[eventName]) && typeof this.eventList[eventName] !== undefined;
    }

    addListener = (eventName, callback) => {
        if (this.isEventNameValid(eventName)) {
            this.eventList[eventName].push(callback);
        } else {
            this.eventList[eventName] = [callback];
        }
    }

    removeListener = (eventName) => {
        if ( typeof this.eventList[eventName] !== undefined) {
            this.eventList[eventName] = [];
        }
        
    }

    fireEvent = (eventName, ...data) => {
        if (!this.isEventNameValid(eventName)) { return; }
        
        if (this.eventList[eventName] !== undefined) {
            for (let i = 0; i < this.eventList[eventName].length; i++) {
                const cb = this.eventList[eventName][i];
                if (!cb && typeof (cb) !== 'function') { return; }
                cb(...data);
            }    
        }
        
    }
}

export default EventCenter;