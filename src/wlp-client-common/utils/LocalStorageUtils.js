import { v4 as uuidv4 } from 'uuid';

class LocalStorageUtils {
    static CLIENT_ID = uuidv4();

    SKIP_TOUR_EDITOR = "skipTourEditor";

    SKIP_TOUR_MYPAGE = "skipTourMypage";

    TEAM_COLOR = "teamColor";

    TEAM_ORDER = "teamOrder";

    RECENT_MAPS = "recentMap";

    LOCAL_STORAGE = "kboard";

    STRAGE_NAME = "mailaddress";

    STRAGE_LOCALE_CODE = "localeCode";

    SET_COOKIE = 'set-cookie-string';

    TEAM_DATA = 'teamData';

    ADD_MEMBER_METHOD = 'selectOptionAddMember';

    SESSION_KEY = 'uss';

    HIDE_LOGOUT = true;

    MAP_USER_INFO = "mapUserInfo";

    DOWNLOAD_RESULT = "downloadResult";

    UPLOAD_EXPORT_FILE_RESULT = "uploadExportFileResult";

    SAVED_MAILADDRESS = 'savedMailAddress';

    ACTION_LOGGER_KEY = 'ActionLogger';

    REMOVE_SELECTION_STATE = 'isRemovingSelection';

    setRemoveSelectionState(value) {
        this.setLocalStrage(this.REMOVE_SELECTION_STATE, value)
    }

    getRemoveSelectionState() {
        return this.getLocalStrage(this.REMOVE_SELECTION_STATE)
    }

    setSavedMailAddress(value){
        this.setLocalStrage(this.SAVED_MAILADDRESS, value);
    }

    getSavedMailAddress(){
        return this.getLocalStrage(this.SAVED_MAILADDRESS);
    }

    removeSavedMailAddress(){
        localStorage.removeItem(this.SAVED_MAILADDRESS);
    }

    setHideLogout(value){
        this.setLocalStrage(this.HIDE_LOGOUT, value);
    }
    getHideLogout(){
        return this.getLocalStrage(this.HIDE_LOGOUT);
    }
    
    setLocalStrage(strageName, dataObject) {
        localStorage.setItem(strageName, JSON.stringify(dataObject));
    }

    setUserSession(session){
        this.setLocalStrage(this.SESSION_KEY, session);
    }

    getUserSession(){
        return this.getLocalStrage(this.SESSION_KEY);
    }

    getUserAction(){
        return this.getLocalStrage(this.ACTION_LOGGER_KEY);
    }

    updateUserSession(data) { 
        const dataPrev = this.getUserSession();
        this.setUserSession(Object.assign(dataPrev, data));
    }

    deleteUserSession(){
        localStorage.removeItem(this.SESSION_KEY);
    }

    getLocalStrage(strageName) {
        const strageData = localStorage.getItem(strageName);
        try {
            return JSON.parse(strageData);
        } catch (e) {
            return strageData;
        }
    }

    getLocaleCode(){
        return this.getLocalStrage(this.STRAGE_LOCALE_CODE);
    }

    getLocalStrageItem(strageName) {
        const strageData = localStorage.getItem(strageName);
        
        if (strageData) {
            return strageData;
        }

        return null;
    }

    deleteLocalStrage(strageName) {
        localStorage.removeItem(strageName);
    }

    setMapUserInfo(value){
        let mapUserInfo = this.getMapUserInfo();
        if (!mapUserInfo || !Array.isArray(mapUserInfo)) {
            mapUserInfo = [];
        }

        if (typeof value.mapId === "undefined") {
            return;
        }

        let newMapUserInfo = mapUserInfo;
        if (newMapUserInfo.length > 0) {
            newMapUserInfo = newMapUserInfo.filter(userInfo => userInfo.mapId !== value.mapId)
        }
        
        newMapUserInfo = [value, ...newMapUserInfo];
        this.setLocalStrage(this.MAP_USER_INFO, newMapUserInfo);
    }

    getMapUserInfo(){
        return this.getLocalStrage(this.MAP_USER_INFO);
    }

    setRecentMaps(mapId) {
        const recentMaps = this.getLocalStrage(this.RECENT_MAPS) || [];
        const indexOfMapInRecentMaps = recentMaps.indexOf(mapId);
    
        let newRecentMaps = recentMaps;
        if (indexOfMapInRecentMaps > -1) {
            newRecentMaps.splice(indexOfMapInRecentMaps, 1);
        }
    
        newRecentMaps = [mapId, ...newRecentMaps];
        newRecentMaps = newRecentMaps.splice(0, 10);
        this.setLocalStrage(this.RECENT_MAPS, newRecentMaps);
    }

}
export default LocalStorageUtils;