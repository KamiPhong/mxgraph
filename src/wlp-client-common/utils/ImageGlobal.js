import alertIcon from 'wlp-client-common/images/icon/alertIcon001.png';

class ImageGlobal {
    static ICON_ALERT = alertIcon;
}

export default ImageGlobal;