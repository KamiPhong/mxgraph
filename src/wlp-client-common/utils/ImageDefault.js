import Default1L from 'wlp-client-common/images/avators/Default1L.png';
import Default2L from 'wlp-client-common/images/avators/Default2L.png';
import Default3L from 'wlp-client-common/images/avators/Default3L.png';
import Default4L from 'wlp-client-common/images/avators/Default4L.png';

export default {
    'Default1L': Default1L,
    'Default2L': Default2L,
    'Default3L': Default3L,
    'Default4L': Default4L,

    'Default1S': Default1L,
    'Default2S': Default2L,
    'Default3S': Default3L,
    'Default4S': Default4L,

    'Default1': Default1L,
    'Default2': Default2L,
    'Default3': Default3L,
    'Default4': Default4L,

    'Default1M': Default1L,
    'Default2M': Default2L,
    'Default3M': Default3L,
    'Default4M': Default4L,
}


export const avatorsWithNames = [
    { src: Default1L, img: 'Default1' },
    { src: Default2L, img: 'Default2' },
    { src: Default3L, img: 'Default3' },
    { src: Default4L, img: 'Default4' },
];