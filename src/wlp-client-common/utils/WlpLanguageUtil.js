import { LANGUAGE_KEY_MAP } from 'wlp-client-common/consts/languageConsts';
import LocalStorageUtils from './LocalStorageUtils';
import LocaleCode from "wlp-client-service/consts/LocaleCode";
import i18n from 'i18n';

const localStorageUtil = new LocalStorageUtils();

class WlpLanguageUtil {

    constructor(){
        this.keyMap = Object.keys(LANGUAGE_KEY_MAP);
    }

    getDefaultLanguage(){
        const languageCode = navigator.language.substr(0, 2);
        for(const langueKey of this.keyMap){
            const langueValue = LANGUAGE_KEY_MAP[langueKey];
            //find browser language in list support language
            if(langueValue.indexOf(languageCode) > -1){
                return langueKey;
            }
        }

        //if browser language is not valid => return default language;
        return this.keyMap[0];
    }

    getLanguaeByLocaleCode(code){
        for(const langueKey of this.keyMap){
            const langueValue = LANGUAGE_KEY_MAP[code];
            //find provided language code in list support language
            if(langueValue.indexOf(navigator.language) > -1){
                return langueKey;
            }
        }

        //if provided language code is not valid => return default language;
        return this.keyMap[0];
    }

    getCurrentLanguage(){
        return localStorageUtil.getLocalStrage(localStorageUtil.STRAGE_LOCALE_CODE);
    }

    getLookupKey(){
        return localStorageUtil.STRAGE_LOCALE_CODE;
    }

    getUserNameByLanguage(firstName, lastName) {
        const language = i18n.language;
        return language === LocaleCode.en_US ? `${firstName} ${lastName}` : `${lastName} ${firstName}`; 
    }

    setLocaleLanguage(lang){
        i18n.changeLanguage(lang);
        localStorageUtil.setLocalStrage(localStorageUtil.STRAGE_LOCALE_CODE, lang);
    }

    getCurrentLanguageForXhrHeader() {
        let currentLang = this.getCurrentLanguage();
        return currentLang.replace('_', '-');
    }
}

export default WlpLanguageUtil;