class SendDownloadFileUtils {

    sendForm = ({ url, method, data }) => {
        const form = document.createElement('form');
        const fileDataField = document.createElement('input');
        fileDataField.value = data.downloadUrl;
        fileDataField.name = 'fileUrl';
        form.appendChild(fileDataField);

        form.method = method;
        form.target = '_blank';
        form.action = url;

        document.body.appendChild(form);
        form.submit();
        document.body.removeChild(form);
    }


}
export default SendDownloadFileUtils;