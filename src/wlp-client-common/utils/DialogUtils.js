import closeIcon from "wlp-client-common/images/icon/closeIcon";
import alertIcon from "wlp-client-common/images/icon/alertIcon";
import i18n from "i18n";
import ImageGlobal from "./ImageGlobal";

import ReactDOM from 'react-dom';
import React from 'react';
import CommonWarningModal from 'wlp-client-common/component/modals/CommonWarningModal';
import Link from "wlp-client-common/component/Link";

const createErrorDialogConfig = {
    modalBodyCssString: "",
    modalBodyClass: "",
    backdropClass: "",
    showCancel: false,
    showIcon: true,
    icon: alertIcon,
    onOKCallback: () => { }
}

/**
 * 
 * @param {String} message 
 * @param {{
 *      modalBodyCssString: String,
        modalBodyClass: String,
        backdropClass: String,
        icon: String,
        showIcon: Boolean,
        onOKCallback: Function
    }} config
 */
export function showWarningDialog(message, config) {
    const {
        modalBodyCssString,
        modalBodyClass,
        backdropClass,
        icon,
        showCancel,
        showIcon,
        onOKCallback,
    } = Object.assign(createErrorDialogConfig, config);
    const ERROR_MODAL_ID = "error-modal";
    const ERROR_MODAL_CLOSE_BTN_CLASS = "close-modal-btn";
    const ERROR_MODAL_CANCLE_BTN_CLASS = "cancle-modal-btn";
    let errorModal = document.getElementById(ERROR_MODAL_ID);
    const errorContent = (`
        <div class="modal modal-gr d-block zindex-1070" id="modal-action">
            <div class="modal-dialog modal-dialog-centered modal-logout-ed" role="document">
                <div class="modal-content mypage-modal-bg noselect">
                    <div class="modal-header border-0">
                        <button type="button" class="close ${ERROR_MODAL_CLOSE_BTN_CLASS}">
                        ${closeIcon}
                        </button>
                    </div>
                    <div class="modal-body pt-0 ${modalBodyClass}" style="${modalBodyCssString}">
                        <div class="card border border rounded-lg modal-action-content" style="padding-top: 20px;">
                            <div class="d-flex align-items-center justify-content-center mb-17px">
                                ${showIcon ? icon : ''}
                                <span class="error-modal-content-body noselect"></span>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-whitefl w-100px noselect-color-white ${ERROR_MODAL_CANCLE_BTN_CLASS} ${showCancel === false ? 'd-none' : ''}">${i18n.t('global.cancel')}</button>
                                <button type="submit" class="btn btn-dark w-100px noselect-color-444 ${ERROR_MODAL_CLOSE_BTN_CLASS}">${i18n.t('global.ok')}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-action-backdrop" class="modal-action-backdrop modal-backdrop modal-backdrop-2nd show ${backdropClass} backdrop-popup-blur"></div>
    `);
    
    errorModal = `<div id="${ERROR_MODAL_ID}">${errorContent}</div>`;

    let modalContainer = document.getElementById('modal-notification');
    if (!modalContainer) {
        modalContainer = document.createElement('div');
        modalContainer.id = 'modal-notification';
        document.body.appendChild(modalContainer);
    }

    let errorPopup = document.getElementById('error-modal');
    if(errorPopup === null) {
        modalContainer.innerHTML += errorModal;
    }

    for (const el of modalContainer.getElementsByClassName('error-modal-content-body')) {
        el.innerText = i18n.t(message).replace(/<br>/, '\n');
    }

    const closeBtns =  modalContainer.getElementsByClassName(ERROR_MODAL_CLOSE_BTN_CLASS);
    const cancelBtn =  modalContainer.getElementsByClassName(ERROR_MODAL_CANCLE_BTN_CLASS)[0];

    cancelBtn.onclick  = () => {
        document.body.removeChild(modalContainer);
    }

    for (const btn of closeBtns){
        btn.onclick = () => {
            onOKCallback();
            document.body.removeChild(modalContainer);
        }
    }

}

export function hideWarningDialog() {
    let modalContainer = document.getElementById('modal-notification');
    let errorModal = document.getElementById('error-modal');
    if(modalContainer && errorModal){
        modalContainer.removeChild(errorModal)
    }
}

const getModalContainer = () => {
    const id = 'warning-dialog';
    let container = document.getElementById(id);

    if (container === null) {
        container = document.body.appendChild(document.createElement("div"));
        container.id = id;
    }

    return container;
}

export function showErrorDialog(message, config) {
    const container = getModalContainer();

    const {
        showCancel,
        onOKCallback,
        onClose
    } = Object.assign({}, config);

    const onCancel = () => {
        if (typeof(onClose) === 'function') {
            onClose();
        }

        ReactDOM.unmountComponentAtNode(container);
    }

    const onSubmit = () => {
        if (typeof (onOKCallback) === 'function') {
            onOKCallback();
        }
        ReactDOM.unmountComponentAtNode(container);
    }

    ReactDOM.render(
        <CommonWarningModal bodyClassName="p-lr-30px p-tb-20px" backDropClass="zindex-1071" containerClassname="zindex-1072" onClose={onCancel} show={true}>
            <div className="d-flex-center-fl mb-21px modal-alert">
                <img src={ImageGlobal.ICON_ALERT} alt=""
                    className="d-inline-block img-alert noselect" />
                <div className="txt-alert fs-12px">
                    <div className="noselect">{i18n.exists(message) ? i18n.t(message) : message}</div>
                </div>
            </div>
            <div className="text-center modal-gr-btn">
                {showCancel && <Link className="btn btn-whitefl" onClick={onCancel}>{i18n.t('global.cancel')}</Link>}
                <Link className="btn btn-black fs-14px" onClick={onSubmit}>{i18n.t('global.ok')}</Link>
            </div>
        </CommonWarningModal>,
        container
    )
}
