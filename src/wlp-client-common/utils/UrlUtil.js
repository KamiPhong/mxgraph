export function normalizeUrl(...agrs) {
    return (
        agrs
            .join('/')
            .replace(/\/+/g, '/')
            .replace(/^http:\//, 'http://')
            .replace(/^https:\//, 'https://')
            .replace(/^https:\//, 'https://')
    );
}