export const fileToBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve({base64File: reader.result.split(',')[1], fileName: file.name, size: file.size});
    reader.onerror = error => reject(error);
});