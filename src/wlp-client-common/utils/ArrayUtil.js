import i18next from 'i18next';
import _ from 'lodash';
import { CONSTANTS } from 'wlp-client-editor-lib/utils/Constants';

export function nestedMap(inputObj, cb) {
    let obj = _.cloneDeep(inputObj);
    const objType = typeof (obj);
    const isObjArray = Array.isArray(obj);

    if (isObjArray) {
        for(const i in obj){
            obj[i] = nestedMap(obj[i], cb);
        }
        return obj;
    }

    if (objType === 'object' && obj !== null) {
        for(const i of Object.keys(obj)){
            obj[i] = nestedMap(obj[i], cb);
        }
        return obj;
    }

    obj = cb(obj);
    return obj;
}

export function dynamicSort(property, sortOrder) {
    return function (a, b) {
        if( typeof a[property] === 'undefined'){
            return;
        }
        const aValue = a[property]===null ? "" : a[property].toString();
        const bValue = b[property]===null ? "" : b[property].toString();
        if(sortOrder === 'DESC'){
            return bValue.localeCompare(aValue,{numeric:"true"});
        }else{
            return aValue.localeCompare(bValue);
        }        
    }
}

export function sortBy (field, order, primer) {
    const key = primer ?
        function(x) {
            return primer(x[field].toLowerCase());
        } :
        function(x) {
            return x[field].toLowerCase();
        };
  
    order = order.toUpperCase() === 'DESC' ? 1 : -1;
  
    return function(a, b) {
       // eslint-disable-next-line no-sequences
       return a = key(a), b = key(b), order * ((a > b) - (b > a));
    }
}

export function chunkingArray(array, maxlength = CONSTANTS.MAX_ITEMS_LENGTH_SOCKET_HANDLER) {
    let result = [];
    const length = array.length;
    const quotient = Math.floor(length/maxlength);
    const remainder = length % maxlength;

    for (let i = 0; i < quotient; i++) {
        result.push(array.slice(maxlength*i, maxlength*i+maxlength));
    }

    if (remainder !== 0) {
        result.push(array.slice(maxlength*quotient, length));
    }
    
    return result;
}

export function sortArrayItems(items, sortConfig) {
    let sortItems = [...items];
    if (sortConfig !== null) {
        const isASC = sortConfig.direction === 'ASC';
        const config = {
            numeric: true,
            sensitivity: 'base',
            ignorePunctuation: false
        };

        sortItems.sort((a, b) => {
            let valueCompareA = String(a[sortConfig.key]);
            let valueCompareB = String(b[sortConfig.key]);
            const valueCompareAForeign = String(a[sortConfig.foreignKey]);
            const valueCompareBForeign = String(b[sortConfig.foreignKey]);

            if(sortConfig.key === 'role') {
                valueCompareA = changeRoleNumberToString(valueCompareA);
                valueCompareB = changeRoleNumberToString(valueCompareB);
            }
            //convert text type number of Japaness to number of La-tinh
            if (Number(convertNumberJpToEn(valueCompareA))) {
                valueCompareA = convertNumberJpToEn(valueCompareA);
            }
            if (Number(convertNumberJpToEn(valueCompareB))) {
                valueCompareB = convertNumberJpToEn(valueCompareB);
            }

            //convert text to binary
            const valueCompareABin = textToBin(valueCompareA); 
            const valueCompareBBin = textToBin(valueCompareB);

            const firstCharactorA = valueCompareA.substr(0,1);
            const firstCharactorB = valueCompareB.substr(0,1);
            
            if (isASC) {
                if (valueCompareA === valueCompareAForeign && valueCompareB === valueCompareBForeign && sortConfig.hasColumnID) {
                    return valueCompareAForeign.localeCompare(valueCompareBForeign, undefined, config);
                }
                
                if (valueCompareA === 'null'&& valueCompareB !== 'null'){
                    return -1;
                }

                if (valueCompareA !== 'null'&& valueCompareB === 'null'){
                    return 1;
                }

                if (checkSpecialChar(firstCharactorA) && !checkSpecialChar(firstCharactorB)) return -1;
                if (!checkSpecialChar(firstCharactorA) && checkSpecialChar(firstCharactorB)) return 1;

                if (parseInt(valueCompareA) && parseInt(valueCompareB) && (isNumber(valueCompareA) && isNumber(valueCompareB))) {
                    if(parseInt(valueCompareA) === parseInt(valueCompareB)){
                        //compare value follow foreign when table haven't id column
                        if (sortConfig.key !== sortConfig.foreignKey) {
                            return compareValues(valueCompareAForeign, valueCompareBForeign);
                        } else {
                            return valueCompareAForeign.localeCompare(valueCompareBForeign, undefined, config);
                        }
                    }
                    else{
                        return valueCompareA.localeCompare(valueCompareB, undefined, config);
                    }   
                }

                if (valueCompareABin > valueCompareBBin ) {
                    return 1;
                }else if(valueCompareABin < valueCompareBBin){
                    return -1;
                }else {
                    //compare value follow foreign when table haven't id column
                    if (sortConfig.key !== sortConfig.foreignKey) {
                        return compareValues(valueCompareAForeign, valueCompareBForeign);
                    }else {
                        return valueCompareAForeign.localeCompare(valueCompareBForeign, undefined, config);
                    }
                }
                
            } else {
                if (valueCompareA === valueCompareAForeign && valueCompareB === valueCompareBForeign && sortConfig.hasColumnID) {
                    return valueCompareBForeign.localeCompare(valueCompareAForeign, undefined, config);
                }

                if (valueCompareA === 'null'&& valueCompareB !== 'null') return 1;
                if (valueCompareA !== 'null'&& valueCompareB === 'null') return -1;

                if (parseInt(valueCompareA) && parseInt(valueCompareB) && (isNumber(valueCompareA) && isNumber(valueCompareB))) {
                    if(parseInt(valueCompareA) === parseInt(valueCompareB)){
                        //compare value follow foreign when table haven't id column
                        if (sortConfig.key !== sortConfig.foreignKey) {
                            return compareValues(valueCompareBForeign, valueCompareAForeign);
                        }else {
                            return valueCompareBForeign.localeCompare(valueCompareAForeign, undefined, config);
                        }
                    }
                    else{
                        return valueCompareB.localeCompare(valueCompareA, undefined, config);
                    }
                }

                if (checkSpecialChar(firstCharactorA) && !checkSpecialChar(firstCharactorB)) return 1;
                if (!checkSpecialChar(firstCharactorA) && checkSpecialChar(firstCharactorB)) return -1;

                if (valueCompareABin > valueCompareBBin ) {
                    return -1;
                }else if(valueCompareABin < valueCompareBBin){
                    return 1;
                }else {
                    //compare value follow foreign when table haven't id column
                    if (sortConfig.key !== sortConfig.foreignKey) {
                        return compareValues(valueCompareBForeign, valueCompareAForeign);
                    }else {
                        return valueCompareBForeign.localeCompare(valueCompareAForeign, undefined, config);
                    }
                }
            }
        });
    }
    return sortItems;
}

/**
 * Convert text to binary
 * @param {string} text 
 */

function textToBin(text) {
    let length = text.length;
    let output = [];
    for (let i = 0; i < length; i++) {
        let bin = text[i].charCodeAt().toString(2);
        output.push(Array(32 - bin.length + 1).join("0") + bin);
    }
    return output.join(" ");
}

/**
 * Convert the Japanese numeric characters to ASCII digital characters
 * @param {string} text 
 */

function convertNumberJpToEn(text){
    const search = '０１２３４５６７８９　';
    const replace = '0123456789 ';

    let regex = RegExp('[' + search + ']', 'g');
    let t = text.replace(regex,
        function (chr) {
            let ind = search.indexOf(chr);
            let r = replace.charAt(ind);    
            return r;
        });
    return t;
}

/**
 * Check for special characters
 * @param {string} text 
 */

function checkSpecialChar(text){
    let format = /^[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]*$/;

    if( text.match(format) ){
        return true;
    }else{
        return false;
    }
}

function changeRoleNumberToString(roleId) {
    let a;
    switch (roleId) {
        case '1':
            a = i18next.t('global.leader');
            break;
        case '2':
            a = i18next.t('global.admin');
            break;
        case '3':
            a = i18next.t('global.member');
            break;
        case '4':
            a = i18next.t('global.partner');
            break;
        case '5':
            a = i18next.t('global.owner');
            break;
        default:
            break;
    }
    return a;
}

function compareValues(a,b) {
    const config = {
        numeric: true,
        sensitivity: 'base',
        ignorePunctuation: false
    };
    const valueCompareABin = textToBin(a); 
    const valueCompareBBin = textToBin(b);

    if (a === 'null'&& b !== 'null'){
        return -1;
    }

    if (a !== 'null'&& b === 'null'){
        return 1;
    }

    if (checkSpecialChar(a.substr(0,1)) && !checkSpecialChar(b.substr(0,1))) return -1;
    if (!checkSpecialChar(a.substr(0,1)) && checkSpecialChar(b.substr(0,1))) return 1;

    if (parseInt(a) && parseInt(b)) {
        return a.localeCompare(b, undefined, config);  
    }

    if (valueCompareABin >= valueCompareBBin ) {
        return 1;
    }else if(valueCompareABin < valueCompareBBin){
        return -1;
    }
}

function isNumber(num) {
    return (typeof num === 'string' || typeof num === 'number') && !isNaN(num - 0) && num !== '';
  };
