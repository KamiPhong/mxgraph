import CharacterConsts from 'wlp-client-common/consts/CharacterConsts';

class TextUtils {

    isLatinCharacters(text) {
        return CharacterConsts.LATIN_CHARACTER.test(text)
    }

    /**
     * @param {String} text
     * @param {String} restrict
     */
    filterWithRestrict = (text, restrict) => {
        if (restrict === undefined) {
            return text;
        }

        const expression = new RegExp(restrict, 'gi');
        const match = (text || '').match(expression);

        if (Array.isArray(match)) {
            return match.join('');
        }

        return '';
    }
}

export function htmlDecode(input) {
    var doc = document.createElement('div');
    doc.innerHTML = input;
    return doc.textContent;
}

export function jsonToQueryString(json) {
    return Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}

export default TextUtils;