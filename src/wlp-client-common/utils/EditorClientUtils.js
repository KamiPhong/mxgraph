const { detect } = require('detect-browser');
const browser = detect();
class EditorClient {
    static IS_IE = browser.name === 'ie';
    static IS_EDGE = browser.name === 'edge';
    static IS_FIREFOX = browser.name === 'firefox';
    static IS_SAFARI = browser.name === 'safari';

}

export default EditorClient;