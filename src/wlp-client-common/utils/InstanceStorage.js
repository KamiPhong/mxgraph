class InstanceStorage {
    storeState = {};
    cacheData = {};
    doResetUi = {};
    resetUI = false;

    constructor() {
        if (InstanceStorage.instance) {
            return InstanceStorage.instance;
        }

        InstanceStorage.instance = this;
    }

    save = (object, name) => {
        const state = object.state;
        this.storeState[name] = state;
        this.doResetUi[name] = false;
    }

    restore = (object, name) => {
        
        if (typeof this.storeState[name] !== 'undefined') {
            let stateMustReset = {};
            if (this.doResetUi[name] && typeof object.resetUiAll === 'function') {
                stateMustReset = object.resetUiAll();
            }

            object.setState({...this.storeState[name],...stateMustReset});
        }
        return;
    }

    storeData = (name, data) => {
        this.cacheData[name] = data;
    }

    getStoreData = (name) => {
        const data = { ...this.cacheData[name] };
        delete this.cacheData[name];
        return data;
    }

    clearStoreData = (name) => { 
        delete this.cacheData[name];
    }

    resetUiAll = () => {
        Object.keys(this.doResetUi).forEach((name, index) => {
            this.doResetUi[name] = true;
        });
    }

    remove = (name) => {
        if (typeof this.storeState[name] !== 'undefined') {
            delete this.storeState[name];    
        }
    }

}

const instanceStorage = new InstanceStorage();

export { 
    instanceStorage
};