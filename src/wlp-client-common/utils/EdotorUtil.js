import { EDITOR_PATH_PREFIX } from 'wlp-client-common/config/EditorConfig';
import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";
import {showWarningDialog} from "wlp-client-common/utils/DialogUtils";
import { normalizeUrl } from './UrlUtil';
import i18n from "i18n";
const localStorageUtils = new LocalStorageUtils();

export function getEditorUrl(mapId, sheetId, itemId) {
    let editorUrl = normalizeUrl(EDITOR_PATH_PREFIX, mapId);
    if (sheetId) {
        editorUrl = editorUrl + `?sheetId=${sheetId}`
        if (itemId) {
            editorUrl = editorUrl + `&itemId=${itemId}`
        }
    }
    return editorUrl;
}

export function openEditorPage(mapId, sheetId = null, itemId = null) {
    if (localStorageUtils.getUserSession()){
        localStorageUtils.setHideLogout(false);
        localStorageUtils.setRecentMaps(mapId);
        window.open(getEditorUrl(mapId, sheetId, itemId), '_blank');
    } else {
        showWarningDialog(i18n.t('session.timeout'), {
                onOKCallback: () => {
                    window.location.href = '/';
                }
            }
        )  
    }
}

