class URLGetparams {
    urlParams = null;

    constructor(url){
        if(url && typeof(url) === 'string'){
            let params = {};

            url.replace(/[?&]+([^=&]+)=([^&]*)/gi, (_,key,value) =>{
                params[key] = value;
            });

            this.urlParams = params;
            // Logger.logConsole(this.urlParams, params)
        }
    }

    getParam = (key) => {
        let value = null;
        if(this.urlParams && this.urlParams[key]){
            value = this.urlParams[key];
        }

        return value;
    }
}

export default URLGetparams;