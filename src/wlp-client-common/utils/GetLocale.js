import LocaleConst from 'wlp-client-common/consts/LocaleConst';

class GetLocale {
    getLocale() {
        if (document.documentElement.lang === "en") {
            return LocaleConst.en_US;
        } else if (document.documentElement.lang === "ja") {
            return LocaleConst.ja_JP;
        }
    }
}
export default GetLocale;