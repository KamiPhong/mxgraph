import { detect } from 'detect-browser';
const browser = detect();


if (window.Element && !Element.prototype.closest) {
    Element.prototype.closest =
    function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i,
            el = this;
        do {
            i = matches.length;
            while (--i >= 0 && matches.item(i) !== el) {};
        } while ((i < 0) && (el = el.parentElement));
        return el;
    };
}

const  getStringWidth = function(txt, font) {
    let element = document.createElement('canvas');
    let context = element.getContext("2d");
    context.font = font;
    
    return context.measureText(txt).width;
}

const IS_TOUCH = 'ontouchstart' in document.documentElement;



export { 
    browser,
    getStringWidth,
    IS_TOUCH
}