import EditorClient from 'wlp-client-common/utils/EditorClientUtils';
class SendExportFileUtils {

    sendForm = ({ url, method, data }) => {
        const limitCharactor = 520000;
        const fileData = data.fileData;
        const form = document.createElement('form');
        const fileNameField = document.createElement('input');
        const fileDataField = document.createElement('input');
        const filePartField = document.createElement('input');
        const quotient = Math.floor(fileData.length/limitCharactor);

        fileNameField.value = data.fileName;
        fileNameField.name = 'fileName';
        form.appendChild(fileNameField);

        fileDataField.value = data.fileData;
        fileDataField.name = 'fileData';
        form.appendChild(fileDataField);

        filePartField.value = EditorClient.IS_SAFARI ? quotient : 0;
        filePartField.name = 'part';
        form.appendChild(filePartField);
       
        if (fileData.length > limitCharactor && EditorClient.IS_SAFARI) {
            let first = 0;
            for (let i = 0; i <= quotient; i++) {
                let last = limitCharactor*(i+1);
                let inputField = document.createElement('input'); 
                inputField.name = 'fileData' + i;
                inputField.value = fileData.substr(first, limitCharactor).trim();
                form.appendChild(inputField);
                first = last;
            } 
        } 

        form.method = method;
        form.target = '_blank';
        form.action = url;

        document.body.appendChild(form);
        form.submit();
        document.body.removeChild(form);
    }


}
export default SendExportFileUtils;