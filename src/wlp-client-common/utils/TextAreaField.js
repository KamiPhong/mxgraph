import React, { Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import InputUtils from './InputUtils';

const inputUtils = new InputUtils();
class TextAreaField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            valTextArea: this.props.value,
            isFocus: false
        };

        this.textareaRef = React.createRef();
    };

    setTextareaMessage = (e) => {
        const valTextArea = e.target.value;
        const { maxLengthStr, setValueTextArea } = this.props; 
        const { selectionEnd } = e.target;
        const isCharValid = valTextArea === '';

        if (valTextArea.length > maxLengthStr) {
            this.setState({valTextArea: this.state.valTextArea}, () => {
                setValueTextArea(this.state.valTextArea);
                inputUtils.setCaretPosition(this.textareaRef.current, selectionEnd);
            });
        } else {
            let newValTextArea = valTextArea;
            if (!isCharValid) {
                newValTextArea = valTextArea.slice(0, selectionEnd) + valTextArea.slice(selectionEnd, valTextArea.length);
            }
            this.setState({valTextArea: newValTextArea}, () => {
                    let cursorPos = selectionEnd;
                    setValueTextArea(this.state.valTextArea);
                    inputUtils.setCaretPosition(this.textareaRef.current, cursorPos);
                }
            );
        }
    };
    onBlur = () => {
        this.setState({
            isFocus: false
        })
    };

    onFocus = () => {
        this.setState({
            isFocus: true
        })
    };

    render() {
        const { colSpan, placeholderText, heightField, classTextArea, maxLengthStr } = this.props;
        const { valTextArea, isFocus } = this.state;

        return (<Fragment>
            <textarea
                ref={this.textareaRef}
                style={{height: heightField}}
                value={valTextArea}
                cols={colSpan}
                maxLength={maxLengthStr}
                onChange={(e) => this.setTextareaMessage(e)}
                placeholder={placeholderText}
                className={classTextArea + (isFocus ? " box-shadow-green" : " noselect")}
                onFocus={this.onFocus}
                onBlur={this.onBlur}>
            </textarea>
        </Fragment>);
    }

}

export default withTranslation()(TextAreaField);