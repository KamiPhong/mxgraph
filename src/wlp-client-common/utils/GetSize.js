class GetSize{
    niceBytes(bytes) {
        const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        if (bytes === 1) return '1 byte';
        
        let l = 0, n = parseInt(bytes, 10) || 0;
        while (n >= 1024 && ++l) {
            n = n / 1024;
        }
        return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
    }
    getFileSizeToMB(bytes){
        return this.bytesToMB(bytes) + '.0MB';
    }
    getDiskSizeToMB(bytes) {
        var result = bytes/1024/1024;
        var p = result.toFixed(2).split(".");

        return p[0].split("").reverse().reduce(function(acc, result, i, orig) {
            return  result === "-" ? acc : result + (i && !(i % 3) ? "," : "") + acc;
        }, "MB");
    }
    getPecent(usedBytes, maxBytes) {
        var result = usedBytes * 100 / maxBytes;
        return (result > 100) ? 100 : parseInt(result);
    }
    getDiskPecent(usedBytes, maxBytes) {
        var result = usedBytes * 100 / maxBytes;
        return (result > 100) ? 100 : parseInt(result) + '%';
    }

    bytesToMB(bytes) {
        let result = bytes/1024/1024;
        if((result - 0.5) > parseInt(result)){
            result +=1;
        }
        return parseInt(result);
    }
}
export default GetSize;