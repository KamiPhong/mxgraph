import ImageDefault from "./ImageDefault";

class ImageUtils {

    convertSvgImageToPNGBase64String = (svgImage, svgSize) => {
        return new Promise((resolve, reject) => {
            let canvas = document.createElement("canvas");
            let ctx = canvas.getContext("2d");
            let img = document.createElement("img");

            canvas.width = svgSize.width;
            canvas.height = svgSize.height;

            let b64SVG = btoa(unescape(encodeURIComponent(svgImage)));
            // img.crossOrigin = "anonymous";
            img.src = "data:image/svg+xml;base64," + b64SVG;
            img.onload = () => {
                ctx.drawImage(img, 0, 0);
                let pngUrlString = canvas.toDataURL("data:image/png;");
                //split text by karma
                const splitedUrlString = pngUrlString.split(',');
                //base64 encoded image
                const base64Data = splitedUrlString[1];
                resolve(base64Data);
            };
            img.onerror = () => {
                reject();
            };
        });
    }

    getProfileImage = (src) => {
        return ImageDefault[src] ? ImageDefault[src] : src;
    }

}

export default ImageUtils;