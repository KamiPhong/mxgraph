export function addClassBatch(elementList, className){
    if(elementList){
        elementList = Array.from(elementList);
    }

    if(Array.isArray(elementList) && elementList.length > 0){
        const elementListCount = elementList.length;
        for(let i = 0; i < elementListCount; i ++){
            if(
                typeof(elementList[i].className) === 'string' && 
                elementList[i].className.indexOf(className) === -1
            ){
                elementList[i].className = elementList[i].className + ' ' + className;
            }
        }
    }
}

export function removeClassBatch(elementList, className){
    if(elementList){
        elementList = Array.from(elementList);
    }

    if(Array.isArray(elementList) && elementList.length > 0){
        const elementListCount = elementList.length;
        for(let i = 0; i < elementListCount; i ++){
            if(
                typeof(elementList[i].className) === 'string' &&
                elementList[i].className.indexOf(className) !== -1
            ){
                elementList[i].className = elementList[i].className.replace(' '+className, '');
            }
        }
    }
}

export function setStyle(element, key, value){
    if(element && element.style){
        if(element.style[key] !== value){
            element.style[key] = value;
        }
    }
}