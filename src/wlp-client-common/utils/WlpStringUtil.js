import TextUtils from 'wlp-client-common/utils/TextUtils';
import PassConst from 'wlp-client-common/consts/PassConst';
const textUtils = new TextUtils();

class WlpStringUtil {

    validatePassword = (target) => {
        return Boolean(target && textUtils.filterWithRestrict(target, PassConst.PASS_PATTERN));
    }
}

export default WlpStringUtil;