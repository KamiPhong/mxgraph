import LocaleConst from "wlp-client-common/consts/LocaleConst";
import i18n from "i18n";

class WlpDateUtil {

    /**
     * <p>日付をフォーマットする。</p>
     * 例 英語の場合"Aug 30, 2008"、日本語の場合"2008/08/30"
     *
     * サーバー → クライアント 処理で、
     * サーバー(Java)側では、ユーザーに設定してあるタイムゾーンの差分を計算したUTCを返す。
     * クライアント(Flex)側では、それを受け取りUTCのまま表示する。
     * (クライアント(Flex)側はOSのタイムゾーンを見て、さらに差分を計算してしまうため。)
     *
     * ロケールがさらに増える場合は、resouceプロジェクトでフォーマットの管理をするように修正。
     *
     * @param target 日付
     * @return フォーマット後の文字列
     */
    formatDate(target) {
        const currentLocale = i18n.language;
        let result = null;
        if (LocaleConst.ja_JP === currentLocale) {
            result = this.formatyyyyMMdd(target);
        } else {
            result = this.formatMMMddyyyy(target);
        }
        return result;
    }

    /**
     * <p>日付時刻をフォーマットする。</p>
     * 例 英語の場合"Aug 30, 2008 13:51"、日本語の場合"2008/08/30  13:51"
     *
     * @param target 日付
     * @return フォーマット後の文字列
     */
    formatDateTime(target, currentLocale) {
        let result = null;
        if (LocaleConst.ja_JP === currentLocale) {
            result = this.formatyyyyMMddHHmm(target);
        } else {
            result = this.formatMMMddyyyyHHmm(target);
        }
        return result;
    }


    /**
     * 日付をフォーマットする。(欧米)
     * 例 "Aug 30, 2008"
     *
     * @param target 日付
     * @return フォーマット後の文字列
     */
    formatMMMddyyyy(target) {
        let result = this.format(target, "MMM dd, yyyy");
        return result;
    }

    /**
     * 日付時刻をフォーマットする。(欧米)
     * 例 "Aug 30, 2008 13:51"
     *
     * @param target 日付
     * @return フォーマット後の文字列
     */
    formatMMMddyyyyHHmm(target) {
        let result = this.format(target, "MMM dd, yyyy HH:mm");
        return result;
    }

    /**
     * 日付時刻をフォーマットする。(欧米)
     * 例 "Aug 30, 2008 13:51:12"
     *
     * @param target 日付
     * @return フォーマット後の文字列
     */
    formatyyyyMMddHHmms(target){
        let result = this.format(target, "yyyy/MM/dd HH:mm:s");
        return result;
    }

    /**
     * 日付をフォーマットする。(日本)
     * 例 "2008/08/30"
     *
     * @param target 日付
     * @return フォーマット後の文字列
     */
    formatyyyyMMdd(target) {
        let result = this.format(target, "yyyy/MM/dd");
        return result;
    }

    /**
     * 日付時刻をフォーマットする。(日本)
     * 例 "2008/08/30 13:51"
     *
     * @param target 日付
     * @return フォーマット後の文字列
     */
    formatyyyyMMddHHmm(target) {
        let result = this.format(target, "yyyy/MM/dd  HH:mm");
        return result;
    }

    isValidDate(d) {
        return d instanceof Date && !isNaN(d);
    }

    format(target, dateTimePattern, utc = true) {
        let result = dateTimePattern;
        const monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        let date = new Date();

        if (target) {
            try {
                const timestamp = Number(target);
                const time = isNaN(timestamp) ? target : timestamp;
                date = new Date(time);
            } catch (e) {
                date = new Date(target);
            }
        }

        if(!this.isValidDate(date)){
            return target;
        }


        let months = date.getMonth() + 1;
        let days = date.getDate();
        let years = date.getFullYear();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();

        if (utc) {
            months = date.getUTCMonth() + 1;
            days = date.getUTCDate();
            years = date.getUTCFullYear();
            hours = date.getUTCHours();
            minutes = date.getUTCMinutes();
            seconds = date.getUTCSeconds();
        }

        if(years.toString().length < 4){
            years = [...Array(4 - years.toString().length).fill('0')].join('') + years;
        }
        
        const formatedTime = {
            MMM: monthShortNames[months - 1],
            MM: months < 10 ? "0" + months : months,
            dd: days < 10 ? "0" + days : days,
            yyyy: years,
            HH: hours < 10 ? "0" + hours : hours,
            mm: minutes < 10 ? "0" + minutes : minutes,
            s: seconds < 10 ? "0" + minutes : seconds
        };

        for (const timeUnit in formatedTime) {
            result = result.replace(timeUnit, formatedTime[timeUnit]);
        }

        return result;
    }

}

export default WlpDateUtil;