
export function integerToHext(num) {
    if (typeof (num) === 'number') {
        return num.toString(16);
    }

    return num;
}

export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default {
    integerToHext,
    getRandomInt
}