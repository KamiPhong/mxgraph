export const checkElementClicked = (event, checkNode) => {
    let isClickOnNode = false;
    let node = event.target;
    while (node) {
        if (node === checkNode) {
            isClickOnNode = true;
        }

        if (node.getAttribute && node.getAttribute('disabled') !== null && isClickOnNode) {
            isClickOnNode = false;
            break;
        }

        if (isClickOnNode) {
            break;
        }
        
        node = node.parentNode;
    }

    return isClickOnNode;
}