
export const COLUMN_MIN_WIDTH = 20;
export const THEAD_HEIGHT = 21;
export const ROW_HEIGHT = 28;
export const TABLE_WIDTH_BALANCE = 2;
export const SCROLLBAR_WIDTH = 12;

