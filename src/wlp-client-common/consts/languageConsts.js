export const LANGUAGE_KEY_MAP = {
    ja_JP: ['ja-JP', 'ja', 'ja-JP'],
    en_US: ['en_US', 'en', 'en-US'],
}