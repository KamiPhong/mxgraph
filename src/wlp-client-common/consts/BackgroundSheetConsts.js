export const SheetBG = {
    'sheetBG1': '01',
    'sheetBG2': '02',
    'sheetBG3': '03'
}

export default {
    [SheetBG.sheetBG1]: 'bg-sheet-1',
    [SheetBG.sheetBG2]: 'bg-sheet-2',
    [SheetBG.sheetBG3]: 'bg-sheet-3'
}