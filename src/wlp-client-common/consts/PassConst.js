export default {
    PASS_RESTRICT: /[!-~]+/,
    PASS_PATTERN: /(^(?=.*[0-9])(?=.*[a-zA-Z])([0-9]|[a-zA-Z]|[!\"#$%&()=+*<|?_]){8,64}$)/
}