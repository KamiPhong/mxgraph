import React, { useState, useEffect, useCallback } from 'react';
import { withTranslation } from 'react-i18next';

import Footer from 'wlp-client-mypage-lib/components/Footer';

//static resource
import MyPageHeader from 'wlp-client-mypage-lib/components/MyPageHeader';
import MapsViewSkin from 'wlp-client-mypage-lib/view/MapsViewSkin';
import MypageGuideDisplay from 'wlp-client-common/component/MypageGuideDisplay';
import MyPageView from 'wlp-client-mypage-lib/consts/MyPageView';
import PersonalSettingView from 'wlp-client-mypage-lib/view/PersonalSettingView';
import TeamSettingView from 'wlp-client-mypage-lib/view/TeamSettingView';
import MypageStateStorage from './MypageStateStorage';
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import i18n from 'i18n';
import InformationPanel from 'wlp-client-mypage-lib/components/InformationPanel';

const languageUtil = new WlpLanguageUtil();


const MainSkin = ({ showTourMypage, tourViewCloseHandler, eventsLetsStartUsingSystem, userInfo, setUserInfo, updateUserSession }) => {
    const [myPageView, setMyPageView] = useState(MyPageView.STATE_MAPS);
    const [selectedContractId, setSelectedContractId] = useState(-1);
    const [teamData, updateTeamData] = useState({});
    const [searchPopup, setSearchPopup] = useState(false);
    const [showBackDropCs, setShowBackDropCs] = useState(false);

    const setTeamData = useCallback((data) => {
        let updatedContractList = userInfo.contractList;
        for (let i = 0; i < updatedContractList.length; i++) {
            if (updatedContractList[i].teamId === data.teamId) {
                updatedContractList[i] = data;
                break;
            }
        }

        updateTeamData({ ...data });

        setUserInfo({ ...userInfo, contractList: updatedContractList });
    }, [setUserInfo, userInfo])

    const userName = userInfo.sessionDto ? languageUtil.getUserNameByLanguage(userInfo.sessionDto.firstName, userInfo.sessionDto.lastName) : ``;
    const userNickName = userInfo.sessionDto && (!userInfo.sessionDto.nickName || userInfo.sessionDto.nickName.length === 0) ? userName : userInfo.userName;

    const user = {
        picture: userInfo.smallImageUrl,
        name: userNickName
    };

    useEffect(() => {
        i18n.changeLanguage(languageUtil.getCurrentLanguage());
    }, [])

    useEffect(() => {
        const backdropAllSc = document.getElementById('backdrop-all-sc');
        if (backdropAllSc) {
            let oneClick = 1;
            backdropAllSc.addEventListener('click', () => {
                if (oneClick === 1) {
                    setSearchPopup(false);
                    setShowBackDropCs(false);
                }
                oneClick = 2;
            })
        }
    }, [showBackDropCs])

    const setViewSkin = useCallback((viewSkin) => {
        MypageStateStorage.setItem(MypageStateStorage.CURRENT_STATE, viewSkin);
        const showClass = "d-block";
        const defaultView = (
            <MapsViewSkin
                classAdd={showClass}
                selectedContractId={selectedContractId}
                setSelectedContractId={setSelectedContractId}
                userInfo={userInfo}
                updateUserSession={updateUserSession} />
        );

        switch (viewSkin) {
            case MyPageView.STATE_MAPS:
                return defaultView

            case MyPageView.STATE_PERSONAL_SETTING:
                return <PersonalSettingView
                    userInfo={userInfo}
                    setUserInfo={setUserInfo}
                    setSearchPopup={setSearchPopup}
                    classAdd={showClass}
                    setMyPageView={setMyPageView} />;

            case MyPageView.STATE_TEAM_SETTING:
                return <TeamSettingView
                    classAdd={showClass}
                    setMyPageView={setMyPageView}
                    teamData={teamData}
                    setSearchPopup={setSearchPopup}
                    setTeamData={setTeamData}
                />
            default:
                return defaultView
        }
    }, [
        setMyPageView,
        selectedContractId,
        teamData,
        setTeamData,
        setUserInfo,
        userInfo
    ])

    const renderView = () => {
        if (showTourMypage) {
            return <MypageGuideDisplay
                tourViewCloseHandler={tourViewCloseHandler}
                eventsLetsStartUsingSystem={eventsLetsStartUsingSystem} />
        }

        return (
            <div className="container-fluid p-0 mypage">
                <MyPageHeader
                    user={user}
                    contractList={userInfo.contractList}
                    setMyPageView={setMyPageView}
                    selectedContractId={selectedContractId}
                    setTeamData={setTeamData}
                    searchPopup={searchPopup}
                    setSearchPopup={setSearchPopup}
                    setShowBackDropCs={setShowBackDropCs}
                />
                {setViewSkin(myPageView)}
                {showBackDropCs && <div id="backdrop-all-sc" className="backdrop-all-sc"></div>}
                <InformationPanel />
                <Footer />
            </div>
        )
    }

    return renderView();
}

export default withTranslation()(MainSkin);