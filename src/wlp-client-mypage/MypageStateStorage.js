import MyPageView from "wlp-client-mypage-lib/consts/MyPageView";
import MypageRequestConst from "wlp-client-mypage/consts/MypageRequestConsts";
import ContractType from "wlp-client-service/consts/ContractType";

let mypageStorage = {};

const HEADER_MENU_TEAMLIST = 'headerMenuTeamList';
const CURRENT_STATE = 'currentState';
const CURRENT_CONTRACT = 'currentContract';
const CURRENT_SETTING_CONTRACT = 'currentSettingContract';
const CURRENT_MAP = 'currentMap';
const MYPAGE_STATE = 'mypageState';

function setItem(name, value){
    mypageStorage[name] = value;
}

function getItem(name){
    return mypageStorage[name];
}

function getRequestContractId(){
    let result = -1;
    const currentState = getItem(CURRENT_STATE);
    if(currentState === MyPageView.STATE_MAPS){
        const currentContract = getItem(CURRENT_CONTRACT);
        if(currentContract){
            result = currentContract.contractId;
        }else if(getItem(CURRENT_MAP)){
            try{
                result = getItem(CURRENT_MAP).contractInfo.contractId;
            }catch(e){}
        }
    }else if(currentState === MyPageView.STATE_PERSONAL_SETTING){
        const headerMenuTeamList = getItem(HEADER_MENU_TEAMLIST);
        if(
            headerMenuTeamList && 
            Array.isArray(headerMenuTeamList) &&
            headerMenuTeamList.length > 0
        ){
            result = headerMenuTeamList[0].contractId;
        }
    }else if(currentState === MyPageView.STATE_TEAM_SETTING){
        const currentSettingContract = getItem(CURRENT_SETTING_CONTRACT);
        if(currentSettingContract){
            result = currentSettingContract.contractId;
        }
    }
    return result;
}

function getRequestContractType(){
    const skinState = getItem(CURRENT_STATE);
    let result = null;
    if (skinState === MyPageView.STATE_MAPS){
        result = ContractType.team;

    } else if (skinState === MyPageView.STATE_PERSONAL_SETTING){
        result = ContractType.personal;

    } else if (skinState === MyPageView.STATE_TEAM_SETTING){
        result = ContractType.team;
    }
    return result;
}

function getRequestCurrentState(){

    let result = null;
    const skinState = getItem(CURRENT_STATE);
    if (skinState === MyPageView.STATE_MAPS){
        result = MypageRequestConst.CURRENT_STATE_MAPS;

    } else if (skinState === MyPageView.STATE_PERSONAL_SETTING){
        result = MypageRequestConst.CURRENT_STATE_CONTROL_PANEL;

    } else if (skinState === MyPageView.STATE_TEAM_SETTING){
        result = MypageRequestConst.CURRENT_STATE_CONTROL_PANEL;
    }
    return result;
}

export default {
    getItem,
    setItem,
    getRequestContractId,
    getRequestContractType,
    getRequestCurrentState,
    HEADER_MENU_TEAMLIST,
    CURRENT_STATE,
    CURRENT_CONTRACT,
    CURRENT_MAP,
    CURRENT_SETTING_CONTRACT,
    MYPAGE_STATE
}
