import React, { Component } from 'react';
import "antd/dist/antd.css";

import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';

//static files
import './assets/css/main.css';
import './assets/scss/reponsive/media-styles.scss';
import './assets/css/custom.css';
import './assets/scss/styles.scss';
import MainSkin from './MainSkin';
import MainContext from './MainContext';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import ContractType from 'wlp-client-service/consts/ContractType';
import UserService from 'wlp-client-service/service/UserService';
import GetUserInfoConfigDto from 'wlp-client-service/dto/user/GetUserInfoConfigDto';
import GetJoinTeamInfoConfigDto from 'wlp-client-service/dto/user/GetJoinTeamInfoConfigDto';
import AuthService from 'wlp-client-service/service/AuthService';
import i18n from 'i18n';
import ActionLogger from 'wlp-client-common/ActionLogger';

const strageUtils = new LocalStorageUtils();
const userService = new UserService();
const skipTourMypage = Boolean(strageUtils.getLocalStrage(strageUtils.SKIP_TOUR_MYPAGE));
const authService = new AuthService();

export default class Main extends Component {
    static PAGE_MEMBER_LIST_PANEL = "pageMemberListPanel";
    static PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL = "pageAccountStatusTeamPanel";
    state = Object.freeze({
        showTourMypage: skipTourMypage === false,
        userInfo: {},
        value: {},
        dataStatus: [],
        [Main.PAGE_MEMBER_LIST_PANEL]: {
            data: [],
            widthTableColumns: []
        },
        [Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL]: {
            widthTableColumns: []
        }
    });
    teamInfoList = null;


    setUserInfo = userInfo => {
        this.setState({ userInfo });
        strageUtils.setUserSession(userInfo);
    }

    closeGuidePanel = () => {
        ActionLogger.click('Mypage_TourGuide', 'Close_Button');

        this.setState({ showTourMypage: false });
    }

    componentDidMount() {
        this.callGetUserInfoData();
        this.callGetJoinTeamStatus();
        document.title = i18n.t('global.title.mypage') + ' / ' + i18n.t('global.title.kboard');
        authService.checkLoggedOn().then(userInfo => {
            strageUtils.updateUserSession(userInfo);
            this.setUserInfo(strageUtils.getUserSession());
        });
       
        
    }

    updateUserSession = () => {
        const userSession = strageUtils.getUserSession();
        const oldContractList = userSession.contractList;

        authService.checkLoggedOn().then(userInfo => {
            let contractList = userInfo.contractList;
            userInfo.contractList = this.getContractListForUserSession(oldContractList,  contractList);
            this.setUserInfo(userInfo);
        });
    }

    getContractListForUserSession = (oldContractList, newContractList) => {
        let newContractAdded = [];
        let isDuplicate = false;
        for (let i = 0; i < newContractList.length; i++) {
            for (let j = 0; j < oldContractList.length; j++) {
                if (newContractList[i].contractId === oldContractList[j].contractId) {
                    isDuplicate = true;
                }
            }
            if (!isDuplicate) {
                newContractAdded.push(newContractList[i]);
            }
            isDuplicate = false;
        }

        return oldContractList.concat(newContractAdded);
    }

    callGetUserInfoData = async () => {
        let requestDto = new GetUserInfoConfigDto();
        let logonInfo = strageUtils.getUserSession();

        requestDto.contractId = -1;
        requestDto.contractType = ContractType.personal;
        requestDto.currentState = CurrentState.CONTROL_PANEL;

        if (logonInfo.sessionDto) {
            requestDto.localeCode = logonInfo.sessionDto.localeCode;
        }

        const result = await userService.getUserInfo(requestDto);

        if (result && result.isSucceed) {
            this.getUserInfoDataResultHandler(result);
        }
    };

    getUserInfoDataResultHandler = (result) => {
        this.setState({
            value: result,
        });
        
    };

    callGetJoinTeamStatus = async () => {
        let requestDto = new GetJoinTeamInfoConfigDto();
        let logonInfo = strageUtils.getUserSession();

        requestDto.contractId = -1;
        requestDto.contractType = ContractType.personal;
        requestDto.currentState = CurrentState.CONTROL_PANEL;

        if (logonInfo.sessionDto) {
            requestDto.localeCode = logonInfo.sessionDto.localeCode;
        }

        const result = await userService.getJoinTeamStatus(requestDto);

        if (result) {
            this.getJoinTeamStatusResultHandler(result);
        }
    }

    getJoinTeamStatusResultHandler = (result) => {
        this.teamInfoList = result.teamInfoList;
    }

    setValuesUserInfo = (valuesChange) => {
        const {value} = this.state;
        const valueUser = Object.assign(value, valuesChange);
        this.setState({value: valueUser})
    }

    setDataMemberListPanel = (memberList) => {
        const memberListPanelState = { ...this.state[Main.PAGE_MEMBER_LIST_PANEL] };
        memberListPanelState.data = memberList;
        this.setState({ [Main.PAGE_MEMBER_LIST_PANEL]: memberListPanelState });
    }

    setWidthTableColumnsMemberListPanel = (widthTableColumns) => {
        const memberListPanelState = { ...this.state[Main.PAGE_MEMBER_LIST_PANEL] };
        memberListPanelState.widthTableColumns = widthTableColumns;
        this.setState({ [Main.PAGE_MEMBER_LIST_PANEL]: memberListPanelState });
    }

    setWidthTableColumnsAccountStatusPersonalPanel = (widthTableColumns) => {
        const accountStatusPersonalPanelState = { ...this.state[Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL] };
        accountStatusPersonalPanelState.widthTableColumns = widthTableColumns;
        this.setState({ [Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL]: accountStatusPersonalPanelState });
    }


    render() {
        return (
            <MainContext.Provider value={{
                values: this.state.value,
                teamInfoList: this.teamInfoList,
                setValuesUserInfo: this.setValuesUserInfo,
                [Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL]: {
                    setWidthTableColumns: this.setWidthTableColumnsAccountStatusPersonalPanel,
                    widthTableColumns: this.state[Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL].widthTableColumns
                },
                [Main.PAGE_MEMBER_LIST_PANEL]: {
                    setDataMemberListPanel: this.setDataMemberListPanel,
                    data: this.state[Main.PAGE_MEMBER_LIST_PANEL].data,
                    setWidthTableColumns: this.setWidthTableColumnsMemberListPanel,
                    widthTableColumns: this.state[Main.PAGE_MEMBER_LIST_PANEL].widthTableColumns
                }
            }}>
                <MainSkin
                    setUserInfo={this.setUserInfo}
                    userInfo={this.state.userInfo}
                    showTourMypage={this.state.showTourMypage}
                    tourViewCloseHandler={this.closeGuidePanel}
                    eventsLetsStartUsingSystem={this.closeGuidePanel}
                    updateUserSession={this.updateUserSession} >
                </MainSkin>
            </MainContext.Provider>
        )
    }
}
