import React, { forwardRef } from 'react';

const MainContext = React.createContext();

export function useMainContext(Component) {
    return forwardRef((props, ref) => {
        return (
            <MainContext.Consumer>
                {(context) => {
                    return <Component ref={ref} {...props} mainContext={context} />;
                }}
            </MainContext.Consumer>
        );
    });
}

export default MainContext;
