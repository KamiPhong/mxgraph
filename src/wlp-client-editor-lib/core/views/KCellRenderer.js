import { mxCellRenderer } from "wlp-client-editor-lib/core/KClient";


class KCellRenderer extends mxCellRenderer {

    redrawLabel(state, forced){
        super.redrawLabel(state, forced);
        let graph = state.view.graph;
        let isEdge = graph.getModel().isEdge(state.cell);
        if( isEdge && state.text !== null ){
            state.text.node.classList.add('noselect');
        }
    }

    getShapesForState(state){
        let shapes = super.getShapesForState(state); 
        if( state.view.graph.getModel().isEdge(state.cell) ){
            return [shapes[1], shapes[0]]
        }
        return shapes;
    }
}

export default KCellRenderer;