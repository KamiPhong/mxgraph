import { mxCell, mxGeometry, mxPoint } from 'wlp-client-editor-lib/core/KClient';
import * as GraphUtil from './KGraphUtil';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import { CONSTANTS } from '../utils/Constants';
import editingPencil from 'wlp-client-editor-lib/assets/images/icon-flash-site/editting-pencil.svg';
import creatorPencil from 'wlp-client-editor-lib/assets/images/icon-flash-site/creator-pencil.png';
import likeIcon from 'wlp-client-editor-lib/assets/images/icon-flash-site/like.png';
import likedIcon from 'wlp-client-editor-lib/assets/images/icon-flash-site/liked.png';
import unLikeIcon from 'wlp-client-editor-lib/assets/images/icon-flash-site/un-like.png';
import { hasRoleByModel } from "wlp-client-editor-lib/utils/CheckRole";
import KItemType from "./KItemType";
import { isImageFile } from 'wlp-client-editor-lib/utils/ResourceThumbnailUtils';

class KItemBase extends mxCell {
    hasRole = false;
    isNew = false;
    selected = false;
    
    constructor(model, relative) {
        let geometry = new mxGeometry(model.x, model.y, GraphUtil.DEFAULT_ITEM_WIDTH, GraphUtil.DEFAULT_ITEM_HEIGHT);
        geometry.relative = (relative != null) ? relative : false;
        // let style = GraphUtil.getItemModelStyle(model);

        super(model.text, geometry);
        if (model.id <= 0) {
            if (model.uid) {
                this.setId(model.uid);
            } else {
                this.setId(null);
            }
        } else {
            this.setId(model.id);
        }
        this.isDeleting = false;
        //validate style, value, geometry of current cell base on data model
        this.setVertex(true);
        this.setConnectable(true);
        this.itemType = null;
        this.data = model;
        this.validate();
        this.hasRole = false;
        this.initAttributeCell();
    }

    initAttributeCell = () => {
        this.editingLabel = null;
        this.creatorLabel = null;
        this.itemAttachFile = null;
        this.goodbutton = null;
        this.iconEditLabel = null;
        this.borderSelect = null;
        this.selectionBorder = null;

        this.createSelectionBorder();
        this.createEditingLabel();
        this.createCreatorLabel();
        this.createAttachFileCell();
        this.createGoodButton();
        this.createIconEditLabel();
    }
    /**
     * <p>対象のアイテム（ベースアイテムを含む）が他のクライアントによってロックされているかどうかを判定する。</p>
     * @return      ロックされている事を示す真偽値。
     */
    checkItemIsLocked = () => {
        if (this.data && this.data.editing) {
            if (this.data.editClientId !== LocalStorageUtils.CLIENT_ID) {
                return true;
            }
        }
        return false;
    }

    setStyleAndShape = (lastEditedStyle, lastEditedShape) => {
        if (lastEditedStyle) {
            this.data.style = lastEditedStyle;
        }
        if (lastEditedShape) {
            this.data.shape = lastEditedShape;
        }
        //update cell style
        this.style = GraphUtil.getItemModelStyle(this.data);
    }

    setLocation = (x, y) => {
        this.data.x = x;
        this.data.y = y;
        //update cell location
        this.geometry.x = x;
        this.geometry.y = y;
    }

    updateModelLocationFromCell = () => {
        this.data.x = this.geometry.x;
        this.data.y = this.geometry.y;
    }
    
    updateModelTextFromCell = () => {
        this.data.text = this.value;
    }

    setModel = (model) => {
        this.data = model;
    }
    setHasRole = (mapId) => {
        this.hasRole = hasRoleByModel(this.data, mapId);
    }

    createEditingLabel = () => {
        //[huyvq-bug_ominext_68794]--- set width of geometry to 0 to prevent editing label overlap iconEditLabel in case of lowest zoom
        //(that make user cant click iconEditLabel). This will not effect editting label because the label is HTML
        //notice that the geometry height can effects the space between editingLabel and item's border
        let geometry = new mxGeometry(0, 0, 0, 14);
        geometry.offset = new mxPoint(0, GraphUtil.EDITING_USER_LABEL_OFFSET_BOTTOM);
        geometry.relative = true;
        // Creates the vertex
        let visible = false;
        let value = null;
        if (this.data && this.data.editUserName) {
            visible = true;
        }
        let vertex = new mxCell(value, geometry, GraphUtil.getEditingLabelStyle());
        vertex.setId(this.id + '_locked');
        vertex.setVertex(true);
        vertex.setConnectable(false);
        vertex.setVisible(visible);
        vertex.isEditingLabel = true;

        this.insert(vertex);
        this.editingLabel = vertex;
        this.setEdingLabelValue(this.data.editUserName);
    }

    setVisibleEditingLabel = (visible) => {
        if (this.editingLabel) {
            this.editingLabel.setVisible(visible);
        }
    }

    setEdingLabelValue = (value) => {
        if (this.editingLabel) {
            this.editingLabel.setValue(GraphUtil.createStickyLabel(editingPencil, value, true));
        }
    }

    createCreatorLabel = () => {
        let geometry = new mxGeometry(0, 0, 100, 14);
        geometry.offset = new mxPoint(0, this.geometry.height + GraphUtil.CREATE_USER_LABEL_OFFSET_TOP);
        geometry.relative = true;
        let vertex = new mxCell(null, geometry, GraphUtil.getEditingLabelStyle());
        vertex.setId(this.id + '_creator');
        vertex.setVertex(true);
        vertex.setVisible(false);
        vertex.setConnectable(false);
        vertex.isCreatorLabel = true;

        this.insert(vertex);
        this.creatorLabel = vertex;
        this.setCreatorLabelValue(this.data.createUserName);
    }

    setVisibleCreatorLabel = (visible) => {
        if (this.creatorLabel && this.creatorLabel.visible !== visible) {
            this.creatorLabel.setVisible(visible);
        }
    }

    setCreatorLabelValue = (value) => {
        if (this.creatorLabel) {
            this.creatorLabel.setValue(GraphUtil.createStickyLabel(creatorPencil, value));
        }
    }

    setGeometry(geometry) {
        super.setGeometry(geometry);

        //update editing label geometry
        if (this.editingLabel) {
            let geo = this.editingLabel.geometry.clone();
            geo.offset = new mxPoint(GraphUtil.EDIT_CREATER_LABEL_MARGIN_LEFT, GraphUtil.EDITING_USER_LABEL_OFFSET_BOTTOM);
            this.editingLabel.setGeometry(geo);
        }

        //update creator label geometry
        if (this.creatorLabel) {
            let geo = this.creatorLabel.geometry.clone();
            geo.offset = new mxPoint(GraphUtil.EDIT_CREATER_LABEL_MARGIN_LEFT, geometry.height + GraphUtil.CREATE_USER_LABEL_OFFSET_TOP);
            this.creatorLabel.setGeometry(geo);
        }

        if (this.itemAttachFile) {
            let geo = this.itemAttachFile.geometry.clone();
            geo.x = (geometry.width - GraphUtil.ATTACH_ITEM_WIDTH)/2;

            this.itemAttachFile.setGeometry(geo);
        }

        if (this.selectionBorder) {
            let geo = this.selectionBorder.geometry.clone();
            geo.width = geometry.width + GraphUtil.SELECTION_BORDER_PADDING;
            geo.height = geometry.height + GraphUtil.SELECTION_BORDER_PADDING;
            this.selectionBorder.setGeometry(geo);
        }
    }

    validate = () => {
        if (this.data) {
            let geo = this.geometry.clone();
            geo.x = this.data.x;
            geo.y = this.data.y;

            this.setGeometry(geo);
            this.setStyle(GraphUtil.getItemModelStyle(this.data))
            this.setValue(this.data.text);
        }
    }

    setIsNew = () => {
        const currentDate = (new Date()).getTime();
        const updateTime = (new Date(this.data.updateTime)).getTime();
        let diffTime = currentDate - updateTime;
        const limitHour = CONSTANTS.NEW_LIMIT * 6;
        this.isNew = (diffTime <= limitHour)
    }

    createAttachFileCell = () => {
        if (this.data.attachFile) {         
            const filename = this.data.attachFile.name;
            let geometry = new mxGeometry(this.geometry.width/2, 3, GraphUtil.ATTACH_ITEM_WIDTH, GraphUtil.ATTACH_ITEM_HEIGHT);
            let attachFile = new mxCell(null, geometry, GraphUtil.getAttachFileStyle(this.data));
            attachFile.setId(this.id + '_attachFile');
            attachFile.setVertex(true);
            attachFile.setConnectable(false);
            attachFile.setVisible(true);
            attachFile.itemType = KItemType.ATTACH_ITEM;
            attachFile.isAttachItemImage = isImageFile(filename) ? true : false;
            attachFile.attachFileData = this.data.attachFile;
            attachFile.attachFileBtnDownload = null;

            if (isImageFile(filename) === false) {
                let geo = new mxGeometry(GraphUtil.ATTACH_ITEM_WIDTH/2 - GraphUtil.BUTTON_DOWNLOAD_ATTACH_ITEM_SIZE/2, GraphUtil.ATTACH_ITEM_HEIGHT/2-GraphUtil.BUTTON_DOWNLOAD_ATTACH_ITEM_SIZE/2, GraphUtil.BUTTON_DOWNLOAD_ATTACH_ITEM_SIZE, GraphUtil.BUTTON_DOWNLOAD_ATTACH_ITEM_SIZE);
                let attachFileDownload = new mxCell(null, geo, GraphUtil.getAttachFileDownloadStyle());
                attachFileDownload.setId(this.id + '_attachFileBtnDownload');
                attachFileDownload.setVertex(true);
                attachFileDownload.setConnectable(false);
                attachFileDownload.setVisible(false);
                attachFileDownload.setValue(GraphUtil.createAttachDownloadLabel());
                attachFileDownload.isAttachFileBtnDownload = true;
        
                attachFile.insert(attachFileDownload);
                attachFile.attachFileBtnDownload = attachFileDownload;
            }
            
            this.insert(attachFile);
            this.itemAttachFile = attachFile;
        }
    }

    createGoodButton = () => {
        if (typeof this.data.likeLevel !== "undefined") {
            const width = this.getGoodButtonWidth();
            let geometry = new mxGeometry(0, 0, width, 21);
            let offsetBottom = GraphUtil.EDITING_USER_LABEL_OFFSET_BOTTOM - GraphUtil.GOOD_BUTTON_MARGIN;
            geometry.offset = new mxPoint(0, offsetBottom);

            geometry.relative = true;

            let cell = new mxCell(null, geometry, GraphUtil.getGoodButtonStyle());

            cell.setId(this.id + '_goodButton');
            cell.setVertex(true);
            cell.setVisible(false);
            cell.setConnectable(false);
            cell.isGoodButton = true;

            this.insert(cell);
            this.goodbutton = cell;

            this.setGoodButtonValue();
        }
    }

    setGoodButtonValue = () => {
        if (this.goodbutton) {
            let value = this.getGoodButtonValue();
            this.goodbutton.setValue(value);
        }
    }

    getGoodButtonValue = (clicking = false) => {
        let goodButtonIcon = this.data.liked ? likedIcon : likeIcon;
        if (clicking === true && this.data.liked) {
            goodButtonIcon = unLikeIcon;
        }
        const goodButtonText = GraphUtil.getTextGoodButton(this.data, clicking);
        return GraphUtil.createGoodButtonLabel(goodButtonIcon, goodButtonText, this.data.likeCount);
    }

    getGoodButtonWidth = (clicking = false) => {
        const phase1 = 1;
        const phase2 = 2;
        const phase3 = 3;
        const likeLevel = this.data.likeLevel;
        let width = GraphUtil.MIN_GOODBUTTON_WIDTH;

        if (likeLevel < phase1) {
            width =  GraphUtil.MIN_GOODBUTTON_WIDTH;
        } else if(likeLevel < phase2) {
            width =  GraphUtil.MAX_GOODBUTTON_WIDTH;
        } else if(likeLevel < phase3) {
            width =  GraphUtil.MAX_GOODBUTTON_WIDTH;
        } else {
            width =  GraphUtil.MIN_GOODBUTTON_WIDTH;
        }

        if (clicking === true && this.data.liked) {
            width = GraphUtil.MAX_GOODBUTTON_WIDTH;
        }

        return width;
    }

    setVisibleGoodbutton = (visible) => {
        if (this.goodbutton) {
            this.goodbutton.setVisible(visible);
        }
    }

    setDataLikedLockState = () => {
        if (typeof this.data.liked === "undefined") {
            this.data.liked = this.likedState;
        }
    }

    createIconEditLabel = () => {
            let geometry = new mxGeometry(0, 0, 18, 18);
            geometry.offset = new mxPoint(-1, -1);
            geometry.relative = true;
    
            let vertex = new mxCell(null, geometry, GraphUtil.getStyleIconEditLabel());
            vertex.setId(this.id + '_iconEditLabel');
            vertex.setVertex(true);
            vertex.setConnectable(false);
            vertex.setVisible(false);
            vertex.isIconEditLabel = true;

            this.insert(vertex);

            this.iconEditLabel = vertex;
    }

    setVisibleIconEditLabel = (visible) => {
        if (this.iconEditLabel && this.hasRole) {
            this.iconEditLabel.setVisible(visible);
        }
    }

    createSelectionBorder = () => {
        if (this.data) {
            const style = GraphUtil.getSelectionBorderStyle();
            const width = this.data.width ? this.data.width : GraphUtil.DEFAULT_ITEM_WIDTH;
            const height = this.data.height ? this.data.height : GraphUtil.DEFAULT_ITEM_HEIGHT;
            let padding = GraphUtil.SELECTION_BORDER_PADDING;

            let geometry = new mxGeometry(0, 0, width + padding, height + padding);
            geometry.offset = new mxPoint(-padding/2, -padding/2);
            geometry.relative = true;

            let vertex = new mxCell('', geometry, style);
            vertex.setId(this.id + '_border');
            vertex.setVertex(true);
            vertex.setVisible(false);
            vertex.setConnectable(false);
            
            this.insert(vertex);

            this.selectionBorder = vertex;
        }
    }
    
    setVisibleSelectionBorder = (visible) => {
        if (this.selectionBorder && this.selectionBorder.visible !== visible) {
            this.selectionBorder.setVisible(visible);
        }
    }
}
export default KItemBase;