import { mxCell, mxGeometry, mxPoint } from "./KClient";
import KItemType from "./KItemType";
import * as GraphUtil from './KGraphUtil';

class KBaseItemLabel extends mxCell {
    constructor(value, geometry, style){
        super(value, geometry, style);
        this.isBaseItemLabel = true;
        this.baseItem = null;
        this.itemType = KItemType.BASE_ITEM_LABEL;

        this.iconResize = null;
        // this.scrollTop = 0;
        this.createIconResize();
    }

    createIconResize = () => {
        let geometry = new mxGeometry(0, 0, 8, 8);
        geometry.offset = new mxPoint(this.geometry.width - 8, this.geometry.height - 8);
        geometry.relative = true;
  
        let vertex = new mxCell(null, geometry, GraphUtil.getStyleIconResizeBaseItemLabel());
        vertex.setId(this.id + '_isIconResize');
        vertex.setVertex(true);
        vertex.setConnectable(false);
        vertex.setVisible(false);
        vertex.isIconResizeBaseItemLabel = true;
        vertex.baseItemLabel = this

        this.insert(vertex);
        this.iconResize = vertex;
    }
    
    setVisibleIconResize = (visible) => {
        if (this.iconResize) {
            this.iconResize.setVisible(visible);
        }
    }

    //[ThuyTV] override mxCell event to set label resize icon value
    setId(id){
        super.setId(id);
        if (this.iconResize instanceof mxCell) {
            this.iconResize.setId(this.id + '_isIconResize');
        }
    }

    setScrollTop = (scrollTop) => {
        this.scrollTop = scrollTop;
    }
}

export default KBaseItemLabel;