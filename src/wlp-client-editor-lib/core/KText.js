import { mxText, mxConstants, mxClient, mxUtils } from "./KClient";
import KConstants, { ITEM_LABEL_PADDING } from "./KConstants";
import KItemType from "./KItemType";
import { convertToHtmlEntities } from '../utils/Utils';
class KText extends mxText {

    //NOTE: [ThuyTV] override mxText function
    updateBoundingBox() {
        super.updateBoundingBox();
        this.updateTextStyle(this.node);
    }

    /**
     * update text align, padding
     */
    updateTextStyle = (node) => {
        let textNode = node;
        const { style, cell } = this.state;
        while (textNode.firstChild) {
            if(mxClient.IS_IE && textNode.firstChild.nodeName !== '#text'){
                textNode = textNode.firstChild;
            } else if(
                textNode.firstChild.firstChild &&
                textNode.firstChild.firstChild.nodeName !== '#text'
            ){
                textNode = textNode.firstChild;
            } else {
                break;
            }
            
        }
        if (textNode.style) {
            let addClasses = style[KConstants.ADD_CLASS];
            if (addClasses) {
                const classes = addClasses.split(' ');
                textNode.firstChild.classList.add(...classes);
            }
            let horizontalAlign = style[KConstants.TEXT_HORIZONTAL_ALIGN];
            if (horizontalAlign && textNode.style.textAlign !== horizontalAlign) {
                textNode.style.textAlign = horizontalAlign;
            }

            const padding = style[mxConstants.STYLE_LABEL_PADDING];
            if (typeof(padding) === 'number' &&
                textNode.style.padding !== padding + 'px'
            ) {
                textNode.style.padding = padding + 'px';
            }

            if (cell.itemType === KItemType.BASE_ITEM_LABEL){
                textNode.style.wordBreak = 'break-all';
                textNode.firstChild.setAttribute("cellid", cell.id);
                if (textNode.style.overflow !== style[mxConstants.STYLE_OVERFLOW]) {
                    textNode.style.overflow = style[mxConstants.STYLE_OVERFLOW];
                }
    
                if (textNode.style.maxWidth !== cell.geometry.width) {
                    textNode.style.maxWidth = cell.geometry.width - ITEM_LABEL_PADDING + 'px';
                    textNode.firstChild.style.maxWidth = cell.geometry.width + 'px';
                }
    
                if (textNode.style.maxHeight !== cell.geometry.height) {
                    textNode.style.maxHeight = cell.geometry.height + 'px';
                    textNode.firstChild.style.maxHeight = cell.geometry.height + 'px';
                }
            } else if ( cell.itemType === KItemType.LINK_ITEM ){
                textNode.firstChild.style.lineHeight = KConstants.DEFAULT_LINE_FONT_SIZE + 'px';
                textNode.parentNode.style.paddingTop = parseInt(textNode.parentNode.style.paddingTop) - 1 + 'px';
            }
        }
    }

    //NOTE: uncomment those line bellow to store

    // init(container) {
    //     super.init(container);
    //     this.textNode = null;
    // }

    // redraw() {
    //     super.redraw();
    //     this.textNode = this.node;

    //     while (this.textNode.firstChild) {
    //         if (mxClient.IS_IE && this.textNode.firstChild.nodeName !== '#text') {
    //             this.textNode = this.textNode.firstChild;
    //         } else if (
    //             this.textNode.firstChild &&
    //             this.textNode.firstChild.nodeName !== '#text'
    //         ) {
    //             this.textNode = this.textNode.firstChild;
    //         } else {
    //             break;
    //         }
    //     }

    //     if (this.textNode instanceof HTMLElement) {
    //         this.textNode.removeEventListener('scroll', this.handleMouseWheel);
    //         this.textNode.addEventListener('scroll', this.handleMouseWheel);

    //         // console.log(this.state.cell.scrollTop);
    //         if (this.state.cell.scrollTop) {
    //             // this.textNode.scrollTop = this.state.cell.scrollTop;
    //         }

    //     }

    // }

    // handleMouseWheel = (evt) => {
    //     // console.log(this.textNode.scrollTop, evt)
    //     if (this.state.cell instanceof KBaseItemLabel) {
    //         this.state.cell.setScrollTop(evt.target.scrollTop);
    //     }
    // }

    redraw() {
        let cell = this.state.cell; 
        const itemTypes = [KItemType.DEFAULT_ITEM, KItemType.BASE_ITEM, KItemType.LABEL_ITEM, KItemType.BASE_ITEM_LABEL]
        if( 
            typeof cell.itemType !== 'undefined' && 
            itemTypes.indexOf( cell.itemType ) > -1
        ) {
            var val = this.value;
            
            if (this.dialect !== mxConstants.DIALECT_STRICTHTML)
            {
                val = mxUtils.htmlEntities(val, false);
            }
            
            // Handles trailing newlines to make sure they are visible in rendering output
            val = mxUtils.replaceTrailingNewlines(val, '<div><br></div>');
            this.value = convertToHtmlEntities(val);
        }
        super.redraw();
    }
}

export default KText;