import { mxCell, mxGeometry, mxPoint } from 'wlp-client-editor-lib/core/KClient';
import * as GraphUtil from './KGraphUtil';
import KItemType from './KItemType';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';

class KLineItem extends mxCell {
    constructor(model) {
        super(model.text, new mxGeometry());

        if (model.id <= 0) {
            if (model.uid) {
                this.setId(model.uid);
            } else {
                this.setId(null);
            }
        } else {
            this.setId(model.id);
        }
        this.setEdge(true);

        this.geometry.relative = true;

        this.style = GraphUtil.getItemLineModelStyle(model);
        this.itemType = KItemType.LINK_ITEM;
        this.data = model;
        this.isDeleting = false;

        this.initAttributeCell();
    }

    initAttributeCell = () => {
        this.iconEditLabel = null;
        this.createIconEditLabel();
    }

    updateModelTextFromCell = () => {
        this.data.text = this.value;
    }
    /**
     * <p>対象のアイテム（ベースアイテムを含む）が他のクライアントによってロックされているかどうかを判定する。</p>
     * @return      ロックされている事を示す真偽値。
     */
    checkItemIsLocked = () => {
        if (this.data && this.data.editing) {
            if (this.data.editClientId !== LocalStorageUtils.CLIENT_ID) {
                return true;
            }
        }
        return false;
    }

    setModel = (model) => {
        this.data = model;
    }

    createIconEditLabel = () => {
        let geometry = new mxGeometry(0, 0, 18, 18);
        geometry.offset = new mxPoint(-9, -9);
        geometry.relative = true;

        let vertex = new mxCell(null, geometry, GraphUtil.getStyleIconEditLabel());
        vertex.setId(this.id + '_iconEditLabel');
        vertex.setVertex(true);
        vertex.setConnectable(false);
        vertex.setVisible(false);
        vertex.isIconEditLabel = true;
        this.insert(vertex);
        this.iconEditLabel = vertex;
    }

    setVisibleIconEditLabel = (visible) => {
        this.iconEditLabel.setVisible(visible);
    }
}

export default KLineItem;