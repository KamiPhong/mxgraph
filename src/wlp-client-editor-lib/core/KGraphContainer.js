import {
    mxCompactTreeLayout,
    mxEvent,
    mxConstants,
    mxKeyHandler,
    mxPerimeter,
    mxPoint,
    mxRectangle,
    mxCellRenderer,
    mxUtils,
    mxStyleRegistry,
    mxSvgCanvas2D,
    mxClient,
} from "wlp-client-editor-lib/core/KClient";

import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";
import ObjectPrototype from '../utils/ObjectPrototype';
import KGraphEvent from './KGraphEvent';
import { KEYCODE } from '../utils/Utils';
import PointUtils from "wlp-client-editor-lib/utils/PointUtils";
import { StageConfig, ZoomConfig } from "wlp-client-editor-lib/core/KGraphConfig";
import KGraphItemShape from './KGraphItemShape';
import KItemStyle from './KItemStyle';
import KGraph from "./KGraph";
import * as KGraphUtil from './KGraphUtil';
import KRubberband from "./KRubberband";
import KItemType from './KItemType';
import KShapeArrowRight from "./shapes/KShapeArrowRight";
import KBaseTriangle from "./shapes/KBaseTriangle";
import Logger from "wlp-client-common/Logger";
import KShapeRoundRect from "./shapes/KShapeRoundRect";
import KShapeArrowLeft from "./shapes/KShapeArrowLeft";
import KShapeDiamon from "./shapes/KShapeDiamon";
import KShapeHandWritingRect from "./shapes/KShapeHandWritingRect";
import KShapeHandWritingEclipse from "./shapes/KShapeHandWritingEclipse";
import KShapeCloud from "./shapes/KShapeCloud";
import KSelectionBorder from "./shapes/KSelectionBorder";
import KText from "./KText";
import KImageExport from "./KImageExport";
import KXmlCanvas2D from "./KXmlCanvas2D";
import KConstants from "./KConstants";
import KGraphEdgeShape from "./KGraphEdgeShape";
import KSquareEdge from "./edges/KSquareEdge";
import KTwoWaysEdge from "./edges/KTwoWaysEdge";
import KTwoWaysReverseEdge from "./edges/KTwoWaysReverseEdge";
import ItemMenuButtonBarHandler from 'wlp-client-editor-lib/handlers/ItemMenuButtonBarHandler';
import {
    FILTER_IS_NEW,
    FILTER_IS_HIGH_LIGHT
} from 'wlp-client-editor-lib/core/filters/KFilterConst';
import KOutline from "./KOutline";
import { OUTLINE_SIZE } from "wlp-client-editor-lib/core/KGraphConfig";
import {
    KPerimeterBaseTriangle,
    KPerimeterShapeArrowRight,
    KPerimeterShapeArrowLeft,
    KPerimeterShapeDiamon,
    KPerimeterShapeHandWritingEclipse,
    KPerimeterShapeHandWritingRect,
    KPerimeterShapeCloud,
    KPerimeterShapeRoundRect,
    KPerimeterConstant
} from "./perimeter/KPerimeterShape";
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import LocaleCode from "wlp-client-service/consts/LocaleCode";
const langueUtils = new WlpLanguageUtil();


const localStorage = new LocalStorageUtils();
const pointUtils = new PointUtils();

class KGraphContainer {
    itemsSelection = [];
    deleting = false;
    limitHourFillColor = 0;

    constructor(graphContainer, sheetModel) {
        ObjectPrototype.appendPrototype(this);
        this.graph = new KGraph(graphContainer);
        this.rubberband = new KRubberband(this.graph);
        this.rubberband.setEnabled(true);

        //NOTE: [ThuyTV] override default text shape of cell renderer
        //defaultTextShape is mxText
        this.graph.cellRenderer.defaultTextShape = KText;

        this.initSettings();
        this.initShapes();
        this.addListener();
        
        // list cell id deleting
        this.deletingCellIds = [];

        if (sheetModel) {
            this.doRender(sheetModel, 10)
            this.graph.sheetParentId = sheetModel.parentId;
        }

        // this.graphEventCallBackDicts = {};
        this.userEventCallBackDicts = {};
        this.itemMenuButtonBarHandler = new ItemMenuButtonBarHandler(this.graph);

        this.sheetdeleting = false;

        return this;
    }
    memberLeaveUnlockItems = (clientId) => {
        this.graph.memberLeaveUnlockItems(clientId)
    }
    doRender = async (sheetModel, step) => {
        //NOTE [ThuyTV]: base item should be add first for sorting position
        this.protectRendering()

        await new Promise((result, reject) => {
            let addBaseItemsLimit = (baseItems) => {
                if(baseItems.length < step) {
                    this.graph.addBaseItems(baseItems);
                    result()
                } else {
                    this.graph.addBaseItems(baseItems.slice(0, step));
                    setTimeout(() => {
                        addBaseItemsLimit(baseItems.slice(step))
                    }, 0)
                }
            }
            addBaseItemsLimit(sheetModel.baseItems)
        })

        await new Promise((result, reject) => {
            let addItemsLimit = (items) => {
                if(items.length < step) {
                    this.graph.addItems(items);
                    if(this.limitHourFillColor !== 0)
                        this.setForcedVisibleItemsByUpdateTimeFilters(this.limitHourFillColor);
                    result()
                }  else {
                    this.graph.addItems(items.slice(0, step));
                    setTimeout(() => {
                        addItemsLimit(items.slice(step))
                    }, 1)
                    if(this.limitHourFillColor !== 0)
                this.setForcedVisibleItemsByUpdateTimeFilters(this.limitHourFillColor);
                }
            }

            addItemsLimit(sheetModel.items)
        })

        let newestItemModel = null;

        if (Array.isArray(sheetModel.items)) {
            for (let i = 0; i < sheetModel.items.length; i ++) {
                const item = sheetModel.items[i];
                if (newestItemModel !== null){
                    if(newestItemModel.updateTime < item.updateTime) {
                        newestItemModel = item;//更新日が最新のモデルをセット
                    }
                } else { 
                    newestItemModel = item;
                }
            }
        }

        if(newestItemModel){
            this.panToViewItem(newestItemModel);
        }

        let itemLineModels = [];
        if (sheetModel.items.length > 0) {
            sheetModel.items.forEach(model => {
                if (typeof model.relationLines !== 'undefined' && model.relationLines.length > 0) {
                    itemLineModels = itemLineModels.concat(model.relationLines);
                }
            })
        }

        if (sheetModel.baseItems.length > 0) {
            sheetModel.baseItems.forEach(model => {
                if (typeof model.relationLines !== 'undefined' && model.relationLines.length > 0) {
                    itemLineModels = itemLineModels.concat(model.relationLines);
                }
            })
        }
        
        this.deployItemLines(itemLineModels);
        this.removeRenderingProtection()
    }

    protectRendering = () => {
        let protectionLayer = document.createElement('div')
        protectionLayer.id = 'protect-rendering'
        document.body.appendChild(protectionLayer)
        document.body.style.cursor = 'wait'

    }

    removeRenderingProtection = () => {
        let protectionLayer = document.getElementById('protect-rendering')
        if(protectionLayer) {
            document.body.removeChild(protectionLayer)
        }
        document.body.style.cursor = 'default'
    }

    getGraph = () => {
        return this.graph;
    }

    addUsereventReciver = (eventName, callBack) => {
        this.userEventCallBackDicts[eventName] = callBack;
    }

    fireUserEvent = (eventName, ...data) => {
        const callBack = this.userEventCallBackDicts[eventName];
        if (callBack && typeof (callBack) === 'function') {
            callBack(...data);
        }
    }

    addGraphEventReceiver = (eventName, callBack) => {
        // this.graphEventCallBackDicts[eventName] = callBack;
        this.graph.addGraphEventListener(eventName, callBack);
    }

    destroy = () => {
        if (typeof this.navigator !== 'undefined') {
            this.navigator.destroy();
        }
        this.graph.clearSelection();
        this.graph.destroy();
    }

    clearSelectionGraph = () => {
        this.graph.clearSelection();
    }

    addListener = () => {

        if (this.graph.isEnabled() !== true) {
            return;
        }

        const keyHandler = new mxKeyHandler(this.graph);

        this.graph.addListener(mxEvent.DOUBLE_CLICK, this.handleMouseDblClick);

        mxEvent.addListener(document, 'keydown', this.handleKeyDownEvent);
        mxEvent.addListener(document, 'keyup', this.stageKeyUpHandler);

        keyHandler.bindKey(KEYCODE.DELETE, (keyboardEvent) => {
            this.handleDeleteKeyPress();
        });

        this.graph.graphZoomHandler.onZoomChange = (currentZoom) => {
            this.fireUserEvent(KGraphEvent.GRAPH_ZOOM, currentZoom);
        }
    }

    getSelectedModelByType = (itemType, checkRole) => {
        if (this.graph.isSelectionEmpty()) {
            return [];
        }
        const cells = this.graph.getSelectionCells();

        let itemModels = [];
        if (typeof checkRole === 'undefined') {
            checkRole = false;
        }
        if (cells.length > 0) {
            for (let i in cells) {
                const cell = cells[i];
                if (cell.itemType === itemType) {
                    if ( !checkRole || (checkRole && cell.hasRole) ) {
                        let geo = cell.geometry;
                        cell.data.width = geo.width;
                        cell.data.height = geo.height;
                        itemModels.push(cell.data);    
                    }
                }
            }
        }
        return itemModels;
    }

    getSelectedItemModel = (hasRole) => {
        return this.getSelectedModelByType(KItemType.DEFAULT_ITEM, hasRole);
    }

    getSelectedBaseItemModel = (hasRole) => {
        return this.getSelectedModelByType(KItemType.BASE_ITEM, hasRole);
    }

    getSelectedLineModel = () => {
        const lineModels =  this.getSelectedModelByType(KItemType.LINK_ITEM);
        if(lineModels.length > 0){
            return lineModels[0];
        }

        return null;
    }
    
    checkModelExistById = (modelId) => {
        const cell = this.graph.getModel().getCell(modelId);
        return cell ? true : false;
    }

    // NOTE: migrate from flash
    stageKeyUpHandler = (event) => {
        const keyCode = event.keyCode;
        let isShiftKey = event.shiftKey;
        const target = event.target;
        
        if( target && target.nodeName === 'TEXTAREA' ){
            return;
        }

        if (isShiftKey && keyCode === KEYCODE.ENTER && !mxClient.IS_TOUCH) {
            if (this.graph.isEnabled() && !this.graph.isItemEditing() && !this.graph.isCellsLocked()) {
                this.fireUserEvent(KGraphEvent.ITEM_DO_CREATE);
            }
        }
    }

    handleMouseDblClick = (sender, evt) => {
        if (
            !this.graph.isEnabled() || 
            this.sheetdeleting === true ||
            this.graph.getSelectionCell() ||
            this.graph.savePngBySelected
        ) {
            return false;
        }

        let isCreateItem = true;
        const event = evt.getProperty('event');
        if (event.target.tagName.toLowerCase() === "svg") {
            isCreateItem = false;
        }

        if (isCreateItem && event.target.tagName.toLowerCase() === "rect") {
            if (event.target.parentNode && event.target.parentNode.getAttribute('backDrop') === 1) {
                const bounds = event.target.getBoundingClientRect();
                const x = event.clientX - bounds.left;
                const y = event.clientY - bounds.top;
                const strokewidth = event.target.getAttribute('stroke-width');
                if (x < 0 || y < 0 || bounds.width - strokewidth < x || bounds.height - strokewidth < y) {
                    isCreateItem = false;
                }
            }
        }

        // must create item when new sheet load, not item and backgroundPageShape undefined
        if (event.target.tagName.toLowerCase() === "svg" && typeof this.graph.view.backgroundPageShape === 'undefined') {
            isCreateItem = true;
        }

        const cell = evt.getProperty('cell');
        //[ThuyTv]: check is item locked by user
        if (cell !== null && isCreateItem) {
            if (this.graph.model.isVertex(cell)) {
                if (
                    cell.itemType === KItemType.ATTACH_ITEM ||
                    cell.itemType === KItemType.DEFAULT_ITEM
                ) {
                    isCreateItem = false;
                } else if (cell.itemType === KItemType.BASE_ITEM || cell.itemType === KItemType.BASE_ITEM_LABEL){
                    isCreateItem = this.graph.isAllBaseItemLocked
                }
            }
        }

        if (isCreateItem) {        
            this.fireUserEvent(KGraphEvent.ITEM_DO_CREATE, sender.graphX, sender.graphY);
        }
    }

    setSheetDeletingFlag = (flag) => {
        this.sheetdeleting = flag;
    }

    handleKeyDownEvent = (evt) => {
        if (
            this.graph.isEnabled() &&
            !this.graph.isMouseDown &&
            !this.graph.isEditing() &&
            evt.ctrlKey === true
        ) {
            const target = evt.target;
            if( target && target.nodeName === 'TEXTAREA' ){
                return;
            }
            if (evt.keyCode === KEYCODE.C) {
                //get selected item models or base item model
                const cells = this.graph.getSelectionCells();
                //NOTE: data model may contains item model, base item model or line or some thing others
                let dataModels = [];
                if (cells) {
                    cells.forEach(cell => {
                        if (cell.data) {
                            let geo = cell.geometry;
                            cell.data.width = geo.width;
                            cell.data.height = geo.height;
                            dataModels.push(cell.data);
                        }
                    });
                }

                const itemType = this.getSelectedItemType();
                if (itemType === KItemType.DEFAULT_ITEM) {
                    this.fireUserEvent(KGraphEvent.ITEM_DO_COPY, dataModels);
                } else if (itemType === KItemType.BASE_ITEM) {
                    this.fireUserEvent(KGraphEvent.BASE_ITEM_DO_COPY, dataModels);
                }

            } else if (evt.keyCode === KEYCODE.V) {
                this.fireUserEvent(KGraphEvent.ITEM_DO_PASTE);
            }
        }
    }

    setSelectAndEditItem = (itemModel) => {
        const item = this.graph.getCellByModelAndType(itemModel, KItemType.DEFAULT_ITEM);
        if (item) {
            this.graph.clearSelection();
            this.graph.selectionModel.setCell(item);
            this.graph.timeoutStartEditingAtCell(item);
        }
    }

    /**
     * Hanle event press delete key on document
     */
    handleDeleteKeyPress = () => {
        this.fireUserEvent(KGraphEvent.ITEM_DO_DELETE);
    }

    /**
     * initial graph settings
     */
    initSettings = () => {
        this.graph.pageFormat = new mxRectangle(
            StageConfig.START_X,
            StageConfig.START_Y,
            StageConfig.MAX_WIDTH,
            StageConfig.MAX_HEIGHT
        );

        this.graph.pageVisible = true;
        this.graph.gridEnabled = false;
        // this.graph.gridSize = 10;
        this.graph.setPanning(true);
        this.graph.setTooltips(true);
        this.graph.setCellsEditable(true);
        this.graph.setEnabled(true);
        // Enables HTML labels
        this.graph.setHtmlLabels(true);
        this.graph.autoSizeCellsOnAdd = true;
        this.graph.getTooltipForCell = function (cell) {
            return null;
        };
        this.graph.view.setScale(ZoomConfig.INIT_SCALE_VALUE);

        this.graph.expandedImage = null;

        this.graph.graphHandler.guidesEnabled = true;
        this.graph.graphHandler.scrollOnMove = false;
        // live preview
        // this.graph.vertexHandler.livePreview = true;

        this.graph.graphHandler.maxLivePreview = 20;

        this.graph.graphZoomHandler.setUseMouseWheel(true);
        this.graph.graphZoomHandler.disableZoomOnCell = true;
        this.graph.panningHandler.useLeftButtonForPanning = true;

        mxConstants.GUIDE_COLOR = '#d891bb';
        mxConstants.GUIDE_STROKEWIDTH = 1;

        // Disables the built-in context menu
        mxEvent.disableContextMenu(document.body);

        this.setSettingLayout();
        this.setSettingStyle();

        this.lastEditedItemShape = KGraphItemShape.RECT;
        this.lastEditedItemStyle = KItemStyle.D;
    }

    /**
     * settings layout default
     */
    setSettingLayout = () => {
        let layout = new mxCompactTreeLayout(this.graph, false);
        // layout.parallelEdgeSpacing = 10;
        // layout.useBoundingBox = false;
        // layout.edgeRouting = false;
        // layout.levelDistance = 60;
        // layout.nodeDistance = 16;
        // layout.parallelEdgeSpacing = 10;
        layout.isVertexMovable = function (cell) {
            return true;
        };
    }

    /**
     * settings cell/egde default
     */
    setSettingStyle = () => {
        mxConstants.VERTEX_SELECTION_COLOR = null;
        mxConstants.CURSOR_MOVABLE_VERTEX = KGraphUtil.DEFAULT_CURSOR;
        mxConstants.CURSOR_MOVABLE_EDGE = KGraphUtil.DEFAULT_CURSOR;
        mxConstants.CURSOR_LABEL_HANDLE = KGraphUtil.DEFAULT_CURSOR;
        mxConstants.LINE_HEIGHT = 1.5;
        mxConstants.OUTLINE_COLOR = 'red';
        mxConstants.OUTLINE_STROKEWIDTH = 1.5;
        mxConstants.DEFAULT_FONTSIZE = 15;

        
        let style = [];
        style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_RECTANGLE;
        style[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
        style[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_MIDDLE;
        style[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
        style[mxConstants.STYLE_FILLCOLOR] = "#fff";
        style[mxConstants.HANDLE_FILLCOLOR] = "#80c6ee";
        style[mxConstants.VERTEX_SELECTION_STROKEWIDTH] = "14";
        style[mxConstants.STYLE_FONTFAMILY] = "Meiryo";
        style[mxConstants.DEFAULT_FONTFAMILY] = "Meiryo";
        // style[mxConstants.STYLE_ROUNDED] = true;
        style[mxConstants.STYLE_STROKEWIDTH] = '3';
        style[mxConstants.STYLE_FONTCOLOR] = '#302c2c';
        style[mxConstants.STYLE_FONTSIZE] = '12';
        style[mxConstants.STYLE_RESIZABLE] = '0';

        this.graph.getStylesheet().putDefaultVertexStyle(style);

        let edgeStyle = [];
        edgeStyle[mxConstants.STYLE_RESIZABLE] = '0';
        this.graph.getStylesheet().putDefaultEdgeStyle(edgeStyle);

        mxConstants.CURSOR_MOVABLE_EDGE = 'pointer';
        mxConstants.CURSOR_LABEL_HANDLE = 'help'; 
    }

    //register shape with cell renderer
    initShapes = () => {
        //Item Shapes
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_SHAPE_ARROW_RIGHT, KShapeArrowRight);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_SHAPE_ARROW_LEFT, KShapeArrowLeft);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_RECT, KShapeRoundRect);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_DIAMOND, KShapeDiamon);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_HANDWRITING_RECT, KShapeHandWritingRect);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_HANDWRITING_ELLIPSE, KShapeHandWritingEclipse);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_CLOUD, KShapeCloud);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_BASE_TRIANGLE, KBaseTriangle);
        mxCellRenderer.registerShape(KGraphItemShape.SHAPE_NAME_SELECTION_BORDER, KSelectionBorder);

        //edge Shapes
        mxStyleRegistry.putValue(KGraphEdgeShape.SHAPE_NAME_SQUARE, KSquareEdge);        
        mxCellRenderer.registerShape(KGraphEdgeShape.SHAPE_NAME_TWO_WAY, KTwoWaysEdge);
        mxCellRenderer.registerShape(KGraphEdgeShape.SHAPE_NAME_TWO_WAY_REVERSE, KTwoWaysReverseEdge);

        // register perimeter shape
        mxStyleRegistry.putValue(KPerimeterConstant.PERIMETER_BASE_TRIANGLE, KPerimeterBaseTriangle);  
        mxStyleRegistry.putValue(KPerimeterConstant.PERIMETER_ARROW_RIGHT, KPerimeterShapeArrowRight);  
        mxStyleRegistry.putValue(KPerimeterConstant.PERIMETER_ARROW_LEFT, KPerimeterShapeArrowLeft);  
        mxStyleRegistry.putValue(KPerimeterConstant.PERIMETER_DIAMOND, KPerimeterShapeDiamon);  
        mxStyleRegistry.putValue(KPerimeterConstant.PERIMETER_HAND_WRITING_ECLIPSE, KPerimeterShapeHandWritingEclipse);  
        mxStyleRegistry.putValue(KPerimeterConstant.PERIMETER_HAND_WRITING_RECT, KPerimeterShapeHandWritingRect);  
        mxStyleRegistry.putValue(KPerimeterConstant.PERIMETER_CLOUD, KPerimeterShapeCloud);  
        mxStyleRegistry.putValue(KPerimeterConstant.RECT, KPerimeterShapeRoundRect);  
    }

    /**
     * create mxOutline, show graph navigator
     * @param {HTML DOM} dom
     */
    showNavigator = (dom) => {
        let width = StageConfig.MAX_WIDTH * OUTLINE_SIZE;
        let height = StageConfig.MAX_HEIGHT * OUTLINE_SIZE
        // add strokewidth
        width = width + 1;
        height = height + 1;
        this.navigator = new KOutline(this.graph, dom, width, height);
        this.navigator.updateOnPan = true;
        this.navigator.setZoomEnabled(false);
        this.navigator.setLabelVisible(true);
    }

    /**
     * @author ThuyTV
     * 
     * @migrate MainBase.as getNearestItemByStageCenter[2639] 
     * @description Returns the midpoint coordinates of the nearest item from any coordinates.
     * @description 任意の座標から最も近くに存在するアイテムの中点座標を返却します。
     * @param cp any coordinates
     * @param cp 任意の座標
     * @return Midpoint coordinates of the nearest item from any coordinates
     * @return 任意の座標から最も近くに存在するアイテムの中点座標
     */
    getNearestItemByStageCenter = (cp) => {
        let cells = this.graph.getModel().cells;
        if(typeof cells === 'object'){
            cells = Object.values(cells);
        }

        if (!cells || !Array.isArray(cells)) { return cp };
        let nearestItemIndex = -1;
        let nearestDistance = Infinity;

        const typesOfItem = [KItemType.BASE_ITEM, KItemType.DEFAULT_ITEM];
        for (let i = 1; i < cells.length; i++) {
            const item = cells[i];
            
            if (!item.vertex || typeof item.itemType === 'undefined' || typesOfItem.indexOf(item.itemType) < 0 ) {
                continue;
            }

            const distance = pointUtils.distance(cp, item.geometry);
            if (nearestDistance > distance) {
                nearestDistance = distance;
                nearestItemIndex = i;
            }
        }
        if (nearestItemIndex === -1) {
            return cp;
        }

        const cellGeometryNearest = cells[nearestItemIndex].geometry;
        return new mxPoint( cellGeometryNearest.x+cellGeometryNearest.width/2, cellGeometryNearest.y + cellGeometryNearest.height/2);
    }

    /**
     * update scale of graph
     * @migrate MainBase.as  expandSliderChangeHandler[2616]
     * @param {Number} step
     * @param {Boolean} isZoomOut
     */
    zoomGraph = (step, isZoomIn) => {
        const { graph } = this;
        const { scale, translate } = graph.view;
        let cx = graph.container.clientWidth / 2 / scale - translate.x;
        let cy = graph.container.clientHeight / 2 / scale - translate.y;

        let cp = new mxPoint(cx, cy);
        cp = this.getNearestItemByStageCenter(cp);

        graph.graphZoomHandler.zoom(isZoomIn, cp.x, cp.y, step);
        graph.stopEditing(false);
    }

    setZoomFactor = (currentZoomValue, maxZoomValue) => {
        const { graph } = this;
        graph.graphZoomHandler.setZoomFactor(currentZoomValue, maxZoomValue);
    }

    /**
     * 「アイテム削除」ボタンのクリックイベントハンドラ
     * @param {mxCell} cell
     */
    deleteItemButtonClickHandler = (cell) => {
        this.fireUserEvent(KGraphEvent.ITEM_DO_DELETE);
    }

    // TODO : merge developer function
    // setItemModelsAttribute = (itemModels, attribute, value) => {
    setItemModelsAttribute = (itemModels, attribute, value) => {
        if (Array.isArray(itemModels)) {
            let updatedItemModels = [...itemModels];
            const modelCount = itemModels.length;

            for (let i = 0; i < modelCount; i++) {
                if (updatedItemModels[i]) {
                    updatedItemModels[i][attribute] = value;
                }
            }

            return updatedItemModels;
        }

        return [];
    }

    /**
    * get all base item locked flag
    */
    getAllBaseItemLockedFlag = () => {
        return this.graph.isAllBaseItemLocked;
    }
    /**
    * set lock all base item from sheet
    */
    setAllBaseItemLocked = (lockedFlag) => {
        this.graph.setAllBaseItemLocked(lockedFlag);
    }
    
    /**
     * do Change style type of item, and fireGraphEvent ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {Number} style
     * @author ThuyTV
     */
    updateItemsShape = (itemModels, shape) => {
        let updatedItemModels = [];
        if (Array.isArray(itemModels)) {
            shape = parseInt(shape);
            updatedItemModels = this.setItemModelsAttribute(itemModels, KConstants.ITEM_SHAPE, shape);
        }
        this.graph.updateItems(updatedItemModels);
        this.graph.fireGraphEvent(KGraphEvent.ITEM_UPDATED, updatedItemModels);

        return [];
    }

    addItems = (models) => {
        if (models && models.length > 0) {
            // the models is mine -> then upate existed item
            let copiedItemModels = [];
            let updatedItemModels = [];
            models.forEach(itemModel => {
                const isItemExisted = this.graph.getCellByModelAndType(itemModel, KItemType.DEFAULT_ITEM);
                if (isItemExisted) {
                    //isItemExisted -> need to update model
                    //TODO: (NamN) NOTE: what if user resizes or changes item before item model created and returned?
                    updatedItemModels.push(itemModel);
                } else {
                    //model create by copy (Note: when copying item, do not creat new item immediately, it sends message to server to create new model first)
                    copiedItemModels.push(itemModel);
                }
            });

            //update item
            if (updatedItemModels.length > 0) {
                this.graph.updateItems(updatedItemModels);
            }
            //create new item
            if (copiedItemModels.length > 0) {
                this.graph.addItems(copiedItemModels);
            }
        }
    }

    /**
    * only update item model
    */
    updateItemsModel = (models, isNotUpdateValue = false) => {
        this.graph.updateItemsModelByType(models, KItemType.DEFAULT_ITEM, isNotUpdateValue);
        this.itemMenuButtonBarHandler.handleItemChange();
    }

    /**
    * only update base item model
    */
    updateBaseItemsModel = (models, isNotUpdateValue = false) => {
        this.graph.updateItemsModelByType(models, KItemType.BASE_ITEM, isNotUpdateValue);
        this.itemMenuButtonBarHandler.handleItemChange();
    }

    /**
    * update all object items
    */
    updateItems = (models) => {
        this.graph.updateItems(models);
        this.itemMenuButtonBarHandler.handleItemChange();
    }

    addBaseItems = (models) => {
        if (models && models.length > 0) {
            let copiedItemModels = [];
            let updatedItemModels = [];

            models.forEach(itemModel => {
                let isItemExisted = this.graph.getCellByModelAndType(itemModel, KItemType.BASE_ITEM);
                if (isItemExisted) {
                    //isItemExisted -> need to update model
                    //TODO: NOTE: what if user resizes or changes item before item model created and returned?
                    updatedItemModels.push(itemModel);
                } else {
                    //model create by copy (Note: when copying item, do not creat new item immediately, it sends message to server to create new model first)
                    copiedItemModels.push(itemModel);
                }
            });
            //update base item
            if (updatedItemModels.length > 0) {
                this.graph.updateBaseItems(updatedItemModels);
            }

            //create new base item
            if (copiedItemModels.length > 0) {
                this.graph.addBaseItems(copiedItemModels);
            }
        }
    }

    /**
    * update base item
    */
    updateBaseItems = (models) => {
        this.graph.updateBaseItems(models);
        this.itemMenuButtonBarHandler.handleItemChange();
    }

    /**
     * do Change style type of item, and fireGraphEvent ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {Number} style
     * @author ThuyTV
     */
    updateItemsStyle = (itemModels, style) => {
        let updatedItemModels = [];
        if (Array.isArray(itemModels)) {
            style = parseInt(style);
            updatedItemModels = this.setItemModelsAttribute(itemModels, KConstants.ITEM_STYLE, style);
        }
        this.graph.updateItems(updatedItemModels);
        this.graph.fireGraphEvent(KGraphEvent.ITEM_UPDATED, updatedItemModels);
    }

    /**
     * do Change item text alignment, and fireGraphEvent ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {String} horizontalAlign
     * @author ThuyTV
     */
    updateItemsTextAlignment = (itemModels, horizontalAlign) => {
        let updatedItemModels = [];
        if (Array.isArray(itemModels)) {
            horizontalAlign = parseInt(horizontalAlign);
            updatedItemModels = this.setItemModelsAttribute(itemModels, KConstants.TEXT_HORIZONTAL_ALIGN, horizontalAlign);
        }

        this.graph.updateItems(updatedItemModels);
        this.graph.fireGraphEvent(KGraphEvent.ITEM_UPDATED, updatedItemModels);
    }

    /**
    * upload file for item complete
    */
    handleItemUploadFileCompleted = (itemModels, uploadResult) => {
        if (itemModels.length > 0 && uploadResult) {
            const itemModel = itemModels[0];
            const s3Path = uploadResult.s3Path;
            const fileName = uploadResult.fileName;
            const virusCheckResult = parseInt(uploadResult.virusCheckResult);

            itemModel.text = fileName;
            this.graph.updateItems(itemModel);
            this.graph.fireGraphEvent(KGraphEvent.ITEM_UPLOAD_FILE_COMPLETED, itemModel, fileName, s3Path, virusCheckResult);
        }
    }

    handleItemAttachFileSettingPassword = (itemModel, password) => {
        if (itemModel && password) {
            this.graph.updateItemsModelByType([itemModel], KItemType.DEFAULT_ITEM);
            this.graph.fireGraphEvent(KGraphEvent.ITEM_SETTING_ATTACH_FILE_PASSWORD, itemModel, password);
        }
    }

    handleItemAttachFileRemovePassword = (itemModel) => {
        if (itemModel) {
            this.graph.fireGraphEvent(KGraphEvent.ITEM_CLEAR_ATTACH_FILE_PASSWORD, itemModel);
        }
    }

    /**
     * do Change item text font size, and fireGraphEvent ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {Boolean} isIncrease
     * @author ThuyTV
     */
    updateItemsFontSize = (itemModels, isIncrease) => {
        let updatedItemModels = [];
        if (Array.isArray(itemModels)) {
            const itemModelsCount = itemModels.length;
            for (let i = 0; i < itemModelsCount; i++) {
                const currentItemTextSize = itemModels[i].textSize;
                let updatedTextSize = currentItemTextSize;
                if (isIncrease) {
                    updatedTextSize = KGraphUtil.fontSizePlus(itemModels[i], KItemType.DEFAULT_ITEM);
                } else {
                    updatedTextSize = KGraphUtil.fontSizeMinus(itemModels[i]);
                }

                if (updatedTextSize !== currentItemTextSize) {
                    itemModels[i][KConstants.ITEM_TEXT_SIZE] = updatedTextSize;
                    updatedItemModels.push(itemModels[i]);
                }
            }
        }

        this.graph.updateItems(updatedItemModels);
        this.graph.fireGraphEvent(KGraphEvent.ITEM_UPDATED, updatedItemModels);
    }


    /**
     * do Change style type of base item, and fireGraphEvent BASE_ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {Number} style
     * @author ThuyTV
     */
    updateBaseItemsShape = (baseItemModel, shape) => {
        let updatedBaseItemModels = [];
        if (Array.isArray(baseItemModel)) {
            shape = parseInt(shape);
            updatedBaseItemModels = this.setItemModelsAttribute(baseItemModel, KConstants.BASEITEM_SHAPE, shape);
        }
        this.graph.updateBaseItems(updatedBaseItemModels);
        this.graph.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, updatedBaseItemModels);
    }


    /**
    * do Change style type of base item, and fireGraphEvent BASE_ITEM_UPDATED
    * @param {ItemModel} itemModels
    * @param {Number} style
    * @author ThuyTV
    */
    updateBaseItemsStyle = (baseItemModels, style) => {
        let updatedBaseItemModels = [];
        if (Array.isArray(baseItemModels)) {
            style = parseInt(style);
            updatedBaseItemModels = this.setItemModelsAttribute(baseItemModels, KConstants.BASEITEM_STYLE, style);
        }
        this.graph.updateBaseItems(updatedBaseItemModels);
        this.graph.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, updatedBaseItemModels);
    }

    /**
     * do Change base item text alignment, and fireGraphEvent ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {String} horizontalAlign
     * @author ThuyTV
     */
    updateBaseItemsTextAlignment = (baseItemModels, horizontalAlign) => {
        let updatedBaseItemModels = [];
        if (Array.isArray(baseItemModels)) {
            horizontalAlign = parseInt(horizontalAlign);
            updatedBaseItemModels = this.setItemModelsAttribute(baseItemModels, KConstants.TEXT_HORIZONTAL_ALIGN, horizontalAlign);
        }
        this.graph.updateBaseItems(updatedBaseItemModels);
        this.graph.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, updatedBaseItemModels);
    }

    /**
     * do Change base item text alignment, and fireGraphEvent ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {String} horizontalAlign
     * @author ThuyTV
     */
    updateBaseItemsVerticalTextAlignment = (baseItemModels, verticalAlign) => {
        let updatedBaseItemModels = [];
        if (Array.isArray(baseItemModels)) {
            verticalAlign = parseInt(verticalAlign);
            updatedBaseItemModels = this.setItemModelsAttribute(baseItemModels, KConstants.TEXT_VERTICAL_ALIGN, verticalAlign);
        }
        this.graph.updateBaseItems(updatedBaseItemModels);
        this.graph.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, updatedBaseItemModels);
    }

    /**
     * do Change item text font size, and fireGraphEvent ITEM_UPDATED
     * @param {ItemModel} itemModels
     * @param {Boolean} isIncrease
     * @author ThuyTV
     */
    updateBaseItemsFontSize = (baseItemModels, isIncrease) => {
        let updatedBaseItemModels = [];
        if (Array.isArray(baseItemModels)) {
            const itemModelsCount = baseItemModels.length;
            for (let i = 0; i < itemModelsCount; i++) {
                const currentBaseItemTextSize = baseItemModels[i].textSize;
                let updatedTextSize = currentBaseItemTextSize;
                if (isIncrease) {
                    updatedTextSize = KGraphUtil.fontSizePlus(baseItemModels[i], KItemType.BASE_ITEM);
                } else {
                    updatedTextSize = KGraphUtil.fontSizeMinus(baseItemModels[i]);
                }

                if (updatedTextSize !== currentBaseItemTextSize) {
                    baseItemModels[i][KConstants.ITEM_TEXT_SIZE] = updatedTextSize;
                    updatedBaseItemModels.push(baseItemModels[i]);
                }
            }
        }

        if(updatedBaseItemModels.length > 0){
            this.graph.updateBaseItems(updatedBaseItemModels);
            this.graph.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, updatedBaseItemModels);
        }
    }

    /**
     * update line color and fireEvent
     * only update one line at the time
     */
    updateLineColor = (lineModel, color) => {
        if (lineModel) {
            lineModel[KConstants.ITEM_LINE_COLOR] = color;
            this.graph.updateItemLine(lineModel);

            //only update one at the time
            this.graph.fireGraphEvent(KGraphEvent.ITEM_LINE_UPDATED, lineModel);
        }
    }

    /**
     * update line color and fireEvent
     * only update one line at the time
     */
    updateLineType = (lineModel, lineType) => {
        if (lineModel) {
            lineModel[KConstants.ITEM_LINE_TYPE] = lineType;
            this.graph.updateItemLine(lineModel);

            this.graph.fireGraphEvent(KGraphEvent.ITEM_LINE_UPDATED, lineModel);
        }
    }

    updateItemLineTextSize = (lineModel, isIncrease) => {
        if(lineModel){
            let currentLineTextSize = lineModel[KConstants.ITEM_TEXT_SIZE];
            let nextLineTextSize = lineModel[KConstants.ITEM_TEXT_SIZE];
            if (isIncrease) {
                nextLineTextSize = KGraphUtil.fontSizePlus(lineModel);
            } else {
                nextLineTextSize = KGraphUtil.fontSizeMinus(lineModel);
            }

            if (nextLineTextSize !== currentLineTextSize) {
                lineModel[KConstants.ITEM_TEXT_SIZE] = nextLineTextSize;
                this.graph.updateItemLine(lineModel);
                this.graph.fireGraphEvent(KGraphEvent.ITEM_LINE_UPDATED, lineModel);
            }
        }
    }

    updateItemLineHorizontalAlign = (lineModel, horizontalAlign) => {
        if(lineModel){
            horizontalAlign = parseInt(horizontalAlign);
            lineModel[KConstants.TEXT_HORIZONTAL_ALIGN] = horizontalAlign;
            this.graph.updateItemLine(lineModel);
            this.graph.fireGraphEvent(KGraphEvent.ITEM_LINE_UPDATED, lineModel);
        }
    }

    updateLineLarge = (lineModel, weight) => {
        if (lineModel) {
            lineModel[KConstants.ITEM_LINE_LARGE] = weight;
            this.graph.updateItemLine(lineModel);

            //only update one at the time
            this.graph.fireGraphEvent(KGraphEvent.ITEM_LINE_UPDATED, lineModel);
        }
    }
    

    /**
     * do insert default item, then return updated model
     * @param {ItemMode} itemModel
     * @param {Number} x
     * @param {Number} y
     */
    addNewItem = (itemModel, x, y) => {
        let newItemModel = null;
        let currentLocale = langueUtils.getCurrentLanguage();
        let textWidth = 0;
        if (currentLocale === LocaleCode.ja_JP) {
            textWidth = 8;
        }
        const strokeWidth = StageConfig.innerStrokeWidth*2;
        if (typeof x !== 'number') {
            x = this.graph.container.offsetWidth / 2;
        }
        if (typeof y !== 'number') {
            y = this.graph.container.offsetHeight / 2;
        }

        let tr = this.graph.view.translate;
        x = Math.round(x / this.graph.view.scale) - tr.x;
        y = Math.round(y / this.graph.view.scale) - tr.y;
        const dx = x + KGraphUtil.DEFAULT_ITEM_WIDTH + textWidth;
        const dy = y + KGraphUtil.DEFAULT_ITEM_HEIGHT;
        
        if (x < StageConfig.START_X) {
            x = StageConfig.START_X;
        } else  if (dx > StageConfig.MAX_WIDTH - strokeWidth){
            x = x + (StageConfig.MAX_WIDTH - strokeWidth - dx);
        }

        if (y < StageConfig.START_Y) {
            y = StageConfig.START_Y;
        } else  if (dy > StageConfig.MAX_HEIGHT - strokeWidth){
            y = y + (StageConfig.MAX_HEIGHT - strokeWidth - dy);
        }

        itemModel.x = x;
        itemModel.y = y;
        let addedItems = this.graph.addItems([itemModel]);

        if (addedItems && Array.isArray(addedItems)) {
            newItemModel = addedItems[0].data;
        }

        return newItemModel;
    }

    setItemEditingState = (itemModels) => {
        let cellList = this.graph.getListCellByModelsAndType(itemModels, KItemType.DEFAULT_ITEM);
        this.graph.setCellsEditingState(cellList);
    }

    setBaseItemsEditingState = (baseItemModels) => {
        let cellList = this.graph.getListCellByModelsAndType(baseItemModels, KItemType.BASE_ITEM);
        this.graph.setCellsEditingState(cellList);
    }
    /**
     * add new base item from user
     */
    addNewBaseItem = (itemModel) => {
        let newBaseItemModel = null;
        let centerPoint = this.getCenterPointGraph();

        if (centerPoint != null) {
            itemModel.x = centerPoint.x;
            itemModel.y = centerPoint.y;
            itemModel.textX = itemModel.x + (itemModel.width - itemModel.textWidth) / 2;
            itemModel.textY = itemModel.y + (itemModel.height - itemModel.textHeight) / 2;

            let baseItemAdded = this.graph.addBaseItems([itemModel]);

            if (baseItemAdded && Array.isArray(baseItemAdded)) {
                newBaseItemModel = baseItemAdded[0].data;
            }
        }

        return newBaseItemModel;
    }

    /**
     * get point center mxgraph
     */
    getCenterPointGraph = () => {
        let centerPoint = null;
        let x = this.graph.container.offsetWidth / 2;
        let y = this.graph.container.offsetHeight / 2;
        let tr = this.graph.view.translate;
        x = Math.round(x / this.graph.view.scale) - tr.x;
        y = Math.round(y / this.graph.view.scale) - tr.y;

        if (
            x >= StageConfig.START_X &&
            y >= StageConfig.START_Y &&
            x <= StageConfig.MAX_WIDTH &&
            y <= StageConfig.MAX_HEIGHT

        ) {
            centerPoint = new mxPoint(x, y);
        }

        return centerPoint;
    }

    /**
     * set select and edit base item
     */
    setSelectAndEditBaseItem = (baseItemModel) => {
        const baseItem = this.graph.getCellByModelAndType(baseItemModel, KItemType.BASE_ITEM);
        if (baseItem) {
            
            this.graph.clearSelection();
            this.graph.selectionModel.setCell(baseItem);
            this.graph.timeoutStartEditingAtCell(baseItem);
        }

    }

    /**
     * @author ThuyTV
     * @description make graph editor editable or uneditable
     * @param {boolean} enabled
     * @return {void}
     */
    setEnabled = (enabled) => {
        if (!enabled) {
            this.graph.clearSelection();
            this.graph.stopEditing(false);
        }
        this.graph.setEnabled(enabled);
        this.graph.setTooltips(enabled);
        // this.updateDataSource({ lockFlag: !enabled });
    }

    deleteItems = (ids) => {
        this.graph.deleteItemsByIds(ids);
        this.deletingCellIds = [];
    }

    deleteItemsByModelsAndItemType = (itemModels, itemType) => {
        const cells = this.graph.getListCellByModelsAndType(itemModels, itemType);
        Logger.logConsole('deleteItemsByModels', cells);
        if (cells.length > 0) {
            this.graph.removeCells(cells);
        }
    }

    /**
     * get request from graph
     * @param {*} bounds 
     * @param {*} scale 
     */
    getRequestRenderImageGraph = (bounds, scale, border, isExportSheet) => {
        const viewScale = this.graph.view.scale;
        border = border*viewScale;
      
        if (isExportSheet === true) {
            if (viewScale === ZoomConfig.MIN_SCALE) {
                bounds.width += KGraphUtil.SHEET_PNG_PADDING*2*viewScale;
                bounds.height += KGraphUtil.SHEET_PNG_PADDING*2*viewScale;
            } else if (viewScale > ZoomConfig.MIN_SCALE && viewScale < ZoomConfig.MIDDLE_SCALE) {
                bounds.width += KGraphUtil.SHEET_PNG_PADDING*viewScale;
                bounds.height += KGraphUtil.SHEET_PNG_PADDING*viewScale;
            }
        }

        const state = this.graph.getView().getState(this.graph.model.root);
        let request = this.getXmlFromState({state, viewScale, scale, border, bounds});

        request.background = "#ffffff";

        return request;
    }

    getXmlFromState = ({state, viewScale, scale, border, bounds}) => {
        let imgExport = new KImageExport();

        // New image export
        let xmlDoc = mxUtils.createXmlDocument();
        let root = xmlDoc.createElement('output');
        xmlDoc.appendChild(root);
        
        // Renders graph. Offset will be multiplied with state's scale when painting state.
        let xmlCanvas = new KXmlCanvas2D(root);

        xmlCanvas.translate(Math.floor((border / scale - bounds.x) / viewScale), Math.floor((border / scale - bounds.y) / viewScale));
        xmlCanvas.scale(scale / viewScale);

        imgExport.drawState(state, xmlCanvas);

        // Puts request data together
        let width = Math.ceil(bounds.width * scale / viewScale + 2 * border);
        let height = Math.ceil(bounds.height * scale / viewScale + 2 * border); 
        
        let xml = mxUtils.getXml(root);

        return {width, height, xml};
    }

    /**
     * Create PNG item with xml data
     * <p>アイテムが存在する領域のみのシート PNG 画像を生成します。</p>
     * @return アイテムが存在する領域のみのシート PNG 画像バイナリ
     */
    getBase64Svg = (bounds, scale) => {
        const background = '#ffffff';
        // const scale = this.graph.view.scale;
        const border = 1;
        // const bounds = this.graph.getGraphBounds();
        const viewScale = this.graph.view.scale;

        let svgImage = this.createSVGImageFromState({
            state: this.graph.getView().getState(this.graph.model.root),
            background,
            scale,
            border,
            bounds,
            viewScale
        })

        const svgImageData = mxUtils.getXml(svgImage);

        const b64SVG = btoa(unescape(encodeURIComponent(svgImageData)));

        return b64SVG;
    }

    createSVGImageFromState = ({ background, state, viewScale, scale, border, bounds }) => {
        const imgExport = new KImageExport();
        Logger.logConsole("imgExport", imgExport)
        let svgDoc = mxUtils.createXmlDocument();
        let svgImage = (svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG, 'svg') : svgDoc.createElement('svg');
        if (background != null) {
            if (svgImage.style != null) {
                svgImage.style.backgroundColor = background;
            }
            else {
                svgImage.setAttribute('style', 'background-color:' + background);
            }
        }
    
        if (svgDoc.createElementNS == null) {
            svgImage.setAttribute('xmlns', mxConstants.NS_SVG);
            svgImage.setAttribute('xmlns:xlink', mxConstants.NS_XLINK);
        }
        else {
            svgImage.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xlink', mxConstants.NS_XLINK);
        }
    
        svgImage.setAttribute('width', (Math.ceil(bounds.width * scale / viewScale) + 2 * border) + 'px');
        svgImage.setAttribute('height', (Math.ceil(bounds.height * scale / viewScale) + 2 * border) + 'px');
        svgImage.setAttribute('version', '1.1');
    
        let group = (svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG, 'g') : svgDoc.createElement('g');
        group.setAttribute('transform', 'translate(0.5,0.5)');
        svgImage.appendChild(group);
        svgDoc.appendChild(svgImage);
    
        let svgCanvas = new mxSvgCanvas2D(group);
        svgCanvas.foEnabled = false;
        svgCanvas.translate(Math.floor((border / scale - bounds.x) / viewScale), Math.floor((border / scale - bounds.y) / viewScale));
        svgCanvas.scale(scale / viewScale);
    
        imgExport.drawState(state, svgCanvas);
        
        return svgImage;
    
    }

    createCsvData = () => {
        let csvData = "";
        let cell = this.graph.getModel().cells;
        Object.entries(cell).forEach(([key, itemval]) => {
            if (itemval.itemType === KItemType.DEFAULT_ITEM || itemval.itemType === KItemType.BASE_ITEM) {
                let itemData = itemval;
                let likeCount = itemData.data.likeCount;
                let text = itemData.data.text;
                if (likeCount) {
                    likeCount = likeCount + ",";
                } else {
                    likeCount = "0,";
                }
                if (text) {
                    csvData = csvData + String(likeCount + text.replace(/\r\n/g, "").replace(/\r/g, "").replace(/\n/g, " ") + "\n");
                }
            }
        });
        //return an async promise
        if(csvData === '') {
            return ''
        }
        return window.btoa(unescape(encodeURIComponent(KConstants.UTF8_BOM_FORMAT + csvData)));
    }

    /**
     * TODO: migrate from flash
     * 現在、選択されているアイテムの矩形を合体させた矩形領域を返します。
     * @param isAllItems この値が true の場合、選択状態を問わずにすべてのアイテム矩形の合計領域を返します。
     */
    createSelectedItemsBouns = (isAllItems) => {
        if (typeof isAllItems === 'undefined') {
            isAllItems = false;
        }
        Logger.logConsole('create Rectangle select multi Item');
    }

    /**
     * TODO: migrate from flash
     * <p>指定アイテムに属するメニューを表示します。</p>
     * @param item IItem オブジェクト
     */
    showMultiSelectedItemMenu = (kItem) => {
        Logger.logConsole(`do showMultiSelectedItemMenu`)
    }

    getSelectedItemType = () => {
        if (this.graph.isSelectionEmpty()) {
            return null;
        }
        const cells = this.graph.getSelectionCells();

        let itemType =  cells[0].itemType;
        if(itemType === KItemType.BASE_ITEM_LABEL){
            itemType = KItemType.BASE_ITEM;
        }

        return itemType;
    }

    addLine = (model) => {
        this.graph.addLine(model);
    }

    updateItemLine = (model) => {
        let cell = this.graph.getCellByModelAndType(model, KItemType.LINK_ITEM);
        if (cell) {
            this.graph.updateItemLine(model);
        }
    }
    
    updateItemLineModel = (model) => {
        this.graph.updateItemsModelByType([model], KItemType.LINK_ITEM);
    }

    deployItemLines = (itemLineModels) => {
        this.graph.deployItemLines(itemLineModels);
    }

    stopEditting = () => {
        this.graph.stopEditing();
    }


    documentFileReferenceDownloadAction = (itemModel, password) => {
        this.graph.fireGraphEvent(KGraphEvent.ITEM_DOWNLOAD_FILE, itemModel, password);
    }

    setForcedVisibleItemFilters = (itemId) => {
        let { graph } = this;
        let cell;

        const cellModel = graph.getModel().cells;
        const arrayCells = Object.values(cellModel);
        const arrayCellsLength = arrayCells.length;
        const convertItemId = parseInt(itemId);

        for (let i = 0; i < arrayCellsLength - 1; i++) {
            if (arrayCells[i].data && arrayCells[i].data.id === convertItemId) {
                cell = arrayCells[i];
                break;
            }
        };
        let cells = [cell]

        graph.setFilterGroupItems(cells, FILTER_IS_NEW);
        graph.moveCenterDragTargetGroupByItem(cell);
    }

    removeItemFilters = (model, itemType) => {
        let cell = this.graph.getCellByModelAndType(model, itemType);
        if (cell) {
            this.graph.removeFilterItem(cell)
        }
        
    }

    setForcedVisibleItemsByUpdateTimeFilters = (limitHour) => {
        this.limitHourFillColor = limitHour;
        let {graph} = this;
        const cells = graph.getModel().cells;
        let cellHightLight = [];
        
        Object.values(cells).forEach(cell => {
            if( [KItemType.DEFAULT_ITEM, KItemType.BASE_ITEM].indexOf(cell.itemType) > -1){
                const model = cell.data;
                let diffTime = 0;
                const currentDateTime = Date.now();
                if (model != null && typeof model.updateTime !== 'undefined') {
                    let milliseconds = Date.parse(model.updateTime);
                    diffTime = currentDateTime - (isNaN(milliseconds) ? model.updateTime : milliseconds);
                }
                if( diffTime < limitHour){
                    cellHightLight.push(cell);
                } else {
                    graph.removeFilterItem(cell);
                }
            }
        });

        graph.setFilterGroupItems(cellHightLight, FILTER_IS_HIGH_LIGHT);
        graph.view.refresh();

    }

    removeFilterGroupItems = (models) => {
        let {graph} = this;
        models.forEach(model=>{
            let cell = graph.getModel().getCell(model.id);
            graph.removeFilterItem(cell);
        })
    }

    /**
     * 
     * @param {ItemModel} models 
     */
    highlightItemsByModelList = (models) => {
        if (Array.isArray(models)) {
            const modelsCount = models.length;
            let highlightItems = [];
            for (let i = 0; i < modelsCount; i ++) {
                const cell = this.graph.getModel().getCell(models[i].id);
                if (cell) {
                    highlightItems.push(cell);
                }
            }

            if (highlightItems.length > 0) {
                this.graph.setFilterGroupItems(highlightItems, FILTER_IS_HIGH_LIGHT);
            }
        }
    }

    removeAllFilterGroupItems = () => {
        let {graph} = this;
        const cells = graph.getModel().cells;
        Object.values(cells).forEach(cell => {
            graph.removeFilterItem(cell);
        });
    }

    /**
     * set save png by selected flag
     */
    setSavePngBySelectedFlag = (lockedFlag) => {
        this.graph.setSavePngBySelectedFlag(lockedFlag);
    }

    setGoodButtonFlag = (goodButtonFlag) => {
        this.graph.setGoodButtonFlag(goodButtonFlag);
    }


    panToViewItem = (itemModel) => {
        if(itemModel){
            let x = -itemModel.x;
            let y = -itemModel.y;
            if(this.graph){
                const centerX =  this.graph.container.clientWidth/2 / this.graph.view.scale;
                const centerY = this.graph.container.clientHeight/2 / this.graph.view.scale;
                x = x + centerX;
                y = y + centerY;
            }
            
            this.graph.view.setTranslate(x, y);
        }
    }

    itemLikeStateChanged = (itemLikeModel, clicking) => {
        let item = this.graph.getCellByCellIdAndType(itemLikeModel.itemId, KItemType.DEFAULT_ITEM);
        if (item != null) {
            const { sessionDto } = localStorage.getUserSession();
            let itemModel = item.data;
            if (itemLikeModel.userId === sessionDto.userId) {
                itemModel.liked = itemLikeModel.liked;
            }
            itemModel.likeCount = itemLikeModel.likeCount;
            itemModel.likeLevel = itemLikeModel.likeLevel;
            item.likedState = itemLikeModel.liked;
            
            this.graph.updateItemsModelByType([itemModel], KItemType.DEFAULT_ITEM);
            this.graph.updateItemLike(item, clicking);
        }
    }

    setPrintingRangeGuideDisplayFlag = (displayFlag) => {
        this.graph.setPrintingRangeGuideDisplayFlag(displayFlag);
    }

    revertCellDeleting = () => {
        if (this.deletingCellIds.length > 0) {
            this.graph.revertAttributeIsDeleting(this.deletingCellIds);
        }
    }

    deleteSelectedCells = () => {
        const selectionCells = this.graph.getSelectionCells();
        let deleteModels = [];
        if (selectionCells.length > 0) {
            let itemType = selectionCells[0].itemType;
            if (!itemType) {
                return null;
            }

            if (itemType === KItemType.BASE_ITEM_LABEL) {
                itemType = KItemType.BASE_ITEM;
            }

            for (let i = 0; i < selectionCells.length; i++) {
                let cell = selectionCells[i];
                if (itemType === cell.itemType && (itemType === KItemType.LINK_ITEM  || cell.hasRole) && 
                    cell.isDeleting === false && this.graph.hasItemMoving() === false) {
                    // handle set delete models
                    deleteModels.push(cell.data);
                    cell.isDeleting = true;
                    this.deletingCellIds.push(cell.id);
                }
            }

            if (deleteModels.length > 0) {
                return {deleteModels, itemType};
            }
        }
        return null;
    }

    getCellModelById = (id) => {
        if (this.graph.isSelectionEmpty()) {
            return null;
        }
        const cell = this.graph.getModel().getCell(id);
        if( cell ){
            return cell.data;
        }
        return null;
    }
}

export default KGraphContainer;
