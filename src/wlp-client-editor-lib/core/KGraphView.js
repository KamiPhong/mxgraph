import { mxGraphView, mxRectangle, mxConstants, mxClient, mxPoint, mxUtils, mxEvent, mxMouseEvent } from "wlp-client-editor-lib/core/KClient";
import KStageShape from "./shapes/KStageShape";
import { StageConfig, PrintRangeGuideConfig } from "wlp-client-editor-lib/core/KGraphConfig";
import KFilterIsNew from "wlp-client-editor-lib/core/filters/KFilterIsNew";
import KFilterIsHighLight from "./filters/KFilterIsHighLight";
import KFilterEdgeHover from "./filters/KFilterEdgeHover";
import KItemLineArrowType from "./KItemLineArrowType";
import KPrintRangeGuide from "./shapes/KPrintRangeGuide";
import { MAIN_LINE_HALF_WIDTH, SHEET_PNG_PADDING } from "./KGraphUtil";
class KGraphView extends mxGraphView {
    // eslint-disable-next-line no-useless-constructor
    constructor(...agrs) {
        super(...agrs);

        this.printRangeGuidePage = null;
    }

    getBackgroundPageBounds = () => {
        const fmt = this.graph.pageFormat;

        const bounds = new mxRectangle(
            (fmt.x + this.translate.x) * this.scale,
            (fmt.y + this.translate.y) * this.scale,
            this.scale * fmt.width,
            this.scale * fmt.height
        );

        return bounds;
    }
    validateBackground() {
        super.validateBackground();
        this.validatePrintRangeGuide();
    }

    getPrintRangeGuideBoundFromPage = () => {
        const fmt = this.graph.pageFormat;
        let width = PrintRangeGuideConfig.PRINT_RANGE_GUIDE_WIDTH;
        let height = PrintRangeGuideConfig.PRINT_RANGE_GUIDE_HEIGHT;

        const bounds = new mxRectangle(
            //[huyvq] --- rescale (add 2000 to x and y) print guide bound to match the rescaled svg postion (in SheetView)
            (fmt.x + this.translate.x) * this.scale + 2000,
            (fmt.y + this.translate.y) * this.scale + 2000,
            this.scale * width,
            this.scale * height
        );

        return bounds;
    }

    validatePrintRangeGuide = () => {
        const { graph } = this;
        if (graph.printingRangeGuideDisplay) {
            let bounds = this.getPrintRangeGuideBoundFromPage();
            if (this.printRangeGuidePage == null) {
                this.printRangeGuidePage = new KPrintRangeGuide(
                    bounds, 
                    "none", 
                    PrintRangeGuideConfig.PRINT_RANGE_GUIDE_COLOR, 
                    PrintRangeGuideConfig.PRINT_RANGE_GUIDE_STROKE_WIDTH, 
                    PrintRangeGuideConfig.PRINT_RANGE_GUIDE_TRIANGLE_WIDTH, 
                    PrintRangeGuideConfig.PRINT_RANGE_GUIDE_TRIANGLE_HEIGHT, 
                    PrintRangeGuideConfig.PRINT_RANGE_GUIDE_RANGE_WIDTH, 
                    PrintRangeGuideConfig.PRINT_RANGE_GUIDE_RANGE_HEIGHT, 
                );
                this.printRangeGuidePage.scale = this.scale;
                //[huyvq] --- append print guide to element id 'print-guide-svg-g'
                this.printRangeGuidePage.init(this.printGuideCont)
                this.printRangeGuidePage.redraw();
            } else {
                this.printRangeGuidePage.scale = this.scale;
                this.printRangeGuidePage.bounds = bounds;
                this.printRangeGuidePage.redraw();
            }
        } else {
            if (this.printRangeGuidePage != null) {
                this.printRangeGuidePage.destroy();
                this.printRangeGuidePage = null;
            }
        }
    }

    validateBackgroundPage() {
        const { graph } = this;
        if (graph.pageVisible) {
            const bounds = this.getBackgroundPageBounds();
            if (this.backgroundPageShape == null) {
                this.backgroundPageShape = this.createBackgroundPageShape(bounds);
                this.backgroundPageShape.init(this.backgroundPane);
                this.backgroundPageShape.redraw();
                
                this.backgroundPageShape.addGestureListener(evt => {
                    if (!mxEvent.isConsumed(evt)) {
                        graph.fireGestureEvent(evt, null);
                    }
                }, evt => {
                    if (!mxEvent.isConsumed(evt)) {
                        graph.fireGestureEvent(evt, null);
                    }
                }, evt => {
                    if (!mxEvent.isConsumed(evt)) {
                        graph.fireGestureEvent(evt, null);
                    }
                });
            }
            else {
                this.backgroundPageShape.scale = this.scale;
                this.backgroundPageShape.bounds = bounds;
                this.backgroundPageShape.redraw();
            }
        }
        else if (this.backgroundPageShape != null) {
            this.backgroundPageShape.destroy();
            this.backgroundPageShape = null;
        }
    }

    createBackgroundPageShape = (bounds) => {
        const vertexStyles = this.graph.getStylesheet().getDefaultVertexStyle();
        let vertextStrokeWidth = 1;
        if (
            vertexStyles &&
            typeof (vertexStyles[mxConstants.STYLE_STROKEWIDTH]) === 'string'
        ) {
            vertextStrokeWidth = Number(vertexStyles[mxConstants.STYLE_STROKEWIDTH]);
        }

        return new KStageShape(
            bounds,
            mxConstants.NONE,
            StageConfig.innerStroke,
            StageConfig.innerStrokeWidth,
            StageConfig.outerStroke,
            StageConfig.outerStrokeWidth,
            vertextStrokeWidth
        );
    }

    //NOTE [ThuyTv]: Technical debs
    createSvg(){
        let container = this.graph.container;
        this.canvas = document.createElementNS(mxConstants.NS_SVG, 'g');
        
        // For background image
        this.backgroundPane = document.createElementNS(mxConstants.NS_SVG, 'g');
        this.canvas.appendChild(this.backgroundPane);

        // Adds two layers (background is early feature)
        this.drawPane = document.createElementNS(mxConstants.NS_SVG, 'g');
        this.canvas.appendChild(this.drawPane);

        this.overlayPane = document.createElementNS(mxConstants.NS_SVG, 'g');
        this.canvas.appendChild(this.overlayPane);
        
        this.decoratorPane = document.createElementNS(mxConstants.NS_SVG, 'g');
        this.canvas.appendChild(this.decoratorPane);
        

        let root = document.createElementNS(mxConstants.NS_SVG, 'svg');
        root.setAttribute("xmlns", "http://www.w3.org/2000/svg");
        // root.setAttribute("viewBox", "0 0 800 1200");
        

        root.style.left = '0px';
        root.style.top = '0px';
        root.style.width = '100%';
        root.style.height = '100%';
        
        // NOTE: In standards mode, the SVG must have block layout
        // in order for the container DIV to not show scrollbars.
        root.style.display = 'block';
        
        //NOTE [ThuyTv] -> [QuanNH]: add grow filter to cell 
        root.appendChild(this.addFilters());

        root.appendChild(this.canvas);
        // Workaround for scrollbars in IE11 and below
        if (mxClient.IS_IE || mxClient.IS_IE11)
        {
            root.style.overflow = 'hidden';
        }

        if (container != null)
        {
            container.appendChild(root);
            this.updateContainerStyle(container);
        }
    }

   

    updateEdgeState(state, geo){
        super.updateEdgeState(state, geo);
        
        let absolutePoints = state.absolutePoints;
        if (state && state.cell && state.cell.data &&
            (state.cell.data.style === KItemLineArrowType.TWO_WAY || state.cell.data.style === KItemLineArrowType.TWO_WAY_REVERSE) 
            && absolutePoints && absolutePoints[0] !== null) {
            const R90 = mxUtils.toRadians(90);
            const absolutePointsSource = absolutePoints[0];
            const absolutePointsTarget = absolutePoints[1];
            const radians = Math.atan2(absolutePointsSource.y - absolutePointsTarget.y, absolutePointsSource.x - absolutePointsTarget.x);

            const source = state.getVisibleTerminalState(true);
            const target = state.getVisibleTerminalState(false);
            
            const A = new mxPoint(source.getCenterX(), source.getCenterY());
            const C = new mxPoint(2*absolutePointsSource.x - A.x,  2*absolutePointsSource.y - A.y);
            
            const A2 = new mxPoint(target.getCenterX(), target.getCenterY());
            const C2 = new mxPoint(2*absolutePointsTarget.x - A2.x,  2*absolutePointsTarget.y - A2.y);
            
            let sourcePointCenter1 = new mxPoint(C.x + MAIN_LINE_HALF_WIDTH*this.scale, C.y);
            let sourcePointCenter2 = new mxPoint(C.x - MAIN_LINE_HALF_WIDTH*this.scale, C.y);

            let targetPointCenter1 = new mxPoint(C2.x + MAIN_LINE_HALF_WIDTH*this.scale, C2.y);
            let targetPointCenter2 = new mxPoint(C2.x - MAIN_LINE_HALF_WIDTH*this.scale, C2.y);
            
            if (Math.abs(radians) !== R90) {
                const targetPoint = this.getOppositePoint(A2, C2);
                const sourcePoint = this.getOppositePoint(A, C);
                if (sourcePoint && targetPoint) {
                    sourcePointCenter1 = sourcePoint.point1;
                    sourcePointCenter2 = sourcePoint.point2;

                    targetPointCenter1 = targetPoint.point1;
                    targetPointCenter2 = targetPoint.point2;
                }
            }
           
            const floatingTerminalSourcePoint = this.getFloatingTerminalPoints(state, source, target, sourcePointCenter1, targetPointCenter1);
            const floatingTerminalTargetPoint = this.getFloatingTerminalPoints(state, source, target, sourcePointCenter2, targetPointCenter2);

            if (radians >= R90 || radians < -R90) {
                absolutePoints[0] = floatingTerminalTargetPoint[1];
                absolutePoints[1] = floatingTerminalTargetPoint[0];
                absolutePoints[2] = floatingTerminalSourcePoint[1];
                absolutePoints[3] = floatingTerminalSourcePoint[0];
            } else if (radians === -R90) {
                absolutePoints[0] = floatingTerminalTargetPoint[0];
                absolutePoints[1] = floatingTerminalTargetPoint[1];
                absolutePoints[2] = floatingTerminalSourcePoint[0];
                absolutePoints[3] = floatingTerminalSourcePoint[1];
            } else {
                absolutePoints[0] = floatingTerminalSourcePoint[1];
                absolutePoints[1] = floatingTerminalSourcePoint[0];
                absolutePoints[2] = floatingTerminalTargetPoint[1];
                absolutePoints[3] = floatingTerminalTargetPoint[0];
                
            }
        }
    }

    /**
     * get opposite point (B, B) of triangle ACB ACB'
     */
    getOppositePoint = (A, C) => {
        const dx = C.x - A.x;
        const dy = C.y - A.y;
        const AC = Math.sqrt(dx * dx + dy * dy);
        const BC = MAIN_LINE_HALF_WIDTH*this.scale;
        // console.log("AC", AC)
        // AC*CB = (a - A.x)*(a - A.x) + (b-A.y)*(b-A.y)
        // a*a + b*b - 2*A.x*a - 2*A.y*b + A.x*A.x  + A.y*a.y - AC*AC + BC*BC = 0;
        // x^2 + y2 + ax  + by + c = 0
        const a = - 2*A.x;
        const b = - 2*A.y;
        const c = A.x*A.x  + A.y*A.y - AC*AC + BC*BC;

    
        // (a-C.x)*(a-C.x) + (b-C.y)*(b-C.y) - CB*CB = 0;
        // a*a + b*b - 2*Cx*a - 2*Cy*b + C.x*C.x + C.y*C.y - CB*CB = 0;
        const a1 = - 2*C.x;
        const b1 = - 2*C.y;
        const c1 = C.x*C.x + C.y*C.y - BC*BC;

        const point = this.cacularQuadraticEquation2Hidden(a,b,c,a1,b1,c1);

        return point;
    }


    /**
     * 
     * x^2 + y2 + ax + by + c = 0
     * x^2 + y2 + a1x + b1y + c1 = 0
     */
    cacularQuadraticEquation2Hidden  = (a, b, c, a1, b1, c1) => {
        const i = a - a1;
        const j = b - b1;
        const k = c - c1;

        // e*y^2 + f*y + g = 0;
        const e = (j*j)/(i*i) + 1;
        const f = (2*k*j)/(i*i) - a*j/i + b;
        const g = (k*k)/(i*i)  + c - a*k/i;

        const y = this.cacularQuadraticEquation(e,f,g);
        // console.log("y", y)
        if (y.length > 0) {
            const y1 = y[0];
            const y2 = y[1];
            
            const x1 = (-k-j*y1)/i;
            const x2 = (-k-j*y2)/i;

            const point1 = new mxPoint(x1, y1);
            const point2 = new mxPoint(x2, y2);

            return {point1, point2} 
        }
        
        return null;
    }

    /**
     * cacular quadratic equation 2
     * ay^2 + by + c = 0
     */
    cacularQuadraticEquation(a, b, c) {
        let y = [];

        const y1 = (-1 * b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
        if (!isNaN(y1)) {
            y.push(y1);
        }

        const y2 = (-1 * b - Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
        if (!isNaN(y1)) {
            y.push(y2);
        }

        return y;
    }

    /**
     * lấy giá trị từ tâm item tới 2 cạnh (ABC và AB'C tam giác nội tiếp)
     */
    getFloatingTerminalPoints = (state, source, target, sourceCenter, targetCenter) => {
        const pt = [];

        if (target != null) {
            pt.push(this.getKFloatingTerminalPoint(state, target, source, false, targetCenter));
        }
        
        if (source != null) {
            pt.push(this.getKFloatingTerminalPoint(state, source, target, true, sourceCenter));
        }

        return pt;
    }

    /**
     * get floating terminal point
     * @param {*} edge 
     * @param {*} start 
     * @param {*} end 
     * @param {*} source 
     * @param {*} sourceCenter 
     */
     getKFloatingTerminalPoint = (edge, start, end, source, sourceCenter) => {
        start = this.getTerminalPort(edge, start, source);

        let next = sourceCenter;
        const orth = this.graph.isOrthogonal(edge);
        const alpha = mxUtils.toRadians(Number(start.style[mxConstants.STYLE_ROTATION] || '0'));
        const center = new mxPoint(start.getCenterX(), start.getCenterY());
        
        if (alpha !== 0) {
            const cos = Math.cos(-alpha);
            const sin = Math.sin(-alpha);
            next = mxUtils.getRotatedPoint(next, cos, sin, center);
        }
        
        let border = parseFloat(edge.style[mxConstants.STYLE_PERIMETER_SPACING] || 0);
        border += parseFloat(edge.style[(source) ?
            mxConstants.STYLE_SOURCE_PERIMETER_SPACING :
            mxConstants.STYLE_TARGET_PERIMETER_SPACING] || 0);

        let pt = this.getPerimeterPoint(start, next, alpha === 0 && orth, border);

        if (alpha !== 0) {
            const cos = Math.cos(alpha);
            const sin = Math.sin(alpha);
            pt = mxUtils.getRotatedPoint(pt, cos, sin, center);
        }

        return pt;
    }
   

    
    //Fuction name should be createFilters
    addFilters(){
        let defs = document.createElementNS(mxConstants.NS_SVG, "defs");
        const filterIsNew = new KFilterIsNew();
        defs.appendChild(filterIsNew.getElement());

        const filterIsHighLight = new KFilterIsHighLight();
        defs.appendChild(filterIsHighLight.getElement());

        const filterEdgeHover = new KFilterEdgeHover();
        defs.appendChild(filterEdgeHover.getElement());
        
        return defs;
    }

    getGraphBoundsForPNGExport = () => {
        const baseItemParentState = this.validateCellState(this.validateCell(this.graph.baseItemParent));
        const itemParentState = this.validateCellState(this.validateCell(this.graph.itemParent));

        let bbox = this.exportBoundingBoxFromState(itemParentState, true);
        let baseItemBB = this.exportBoundingBoxFromState(baseItemParentState, true);

        if (bbox && typeof(bbox.add) === 'function') {
            bbox.add(baseItemBB);
        }

        if (bbox === null && baseItemBB) {
            bbox = baseItemBB;
        }

        return bbox;
    }

    exportBoundingBoxFromState = (state, recurse) => {
        recurse = recurse === undefined ? true : recurse;
        let bbox = null;
        if (state != null) {
            
            if (state.shape != null && state.shape.boundingBox != null) {
                if (this.isBoundsInStage(state.shape.boundingBox)) {
                    bbox = state.shape.boundingBox.clone();
                }
            }

            // Adds label bounding box to graph bounds
            if (state.text != null && state.text.boundingBox != null) {
                if (this.isBoundsInStage(state.text.boundingBox)) {
                    if (bbox != null) {
                        bbox.add(state.text.boundingBox);
                    } else {
                        bbox = state.text.boundingBox.clone();
                    }
                }
            }

            if (recurse) {
                let model = this.graph.getModel();
                let childCount = model.getChildCount(state.cell);
                for (let i = 0; i < childCount; i++) {
                    let bounds = this.exportBoundingBoxFromState(this.getState(model.getChildAt(state.cell, i)));
                        
                    if (bounds) {
                        if (bbox == null) {
                            bbox = bounds;
                        }

                        bbox.add(bounds);
                    }
                }
            }
        }
        
        return bbox;
    }

    isBoundsInStage = (bounds) => {
        let boundsInState = true;
        
        if (bounds) {
            try {
                let { x, y, width, height } = bounds;
                let tr = this.translate;
       
                x = Math.round(x / this.scale) - tr.x;
                y = Math.round(y / this.scale) - tr.y;

                let dx = x + width/this.scale;
                let dy = y + height/this.scale;

                if (
                    x < StageConfig.START_X - SHEET_PNG_PADDING*this.scale || 
                    dx > StageConfig.MAX_WIDTH ||
                    y < StageConfig.START_Y - SHEET_PNG_PADDING*this.scale ||
                    dy > StageConfig.MAX_HEIGHT 
                ) {
                    boundsInState = false;
                }

            } catch(e) { 
                
            }

        } else {
            boundsInState = false;
        }

        return boundsInState;
    }

    destroy() {
        super.destroy();
        this.printRangeGuidePage = null;
    }
}

export default KGraphView;