/**
 * ItemLine クラスの、矢印線種を示す文字列クラスです。
 */
const KItemLineArrowType = {
    /** <p>未定義を示す整数値です。</p> */
    NONE: -1,
    /** <p>「直線矢印なし」を示す整数値です。</p> */
    LINEAR_NONE: 0,
    /** <p>「直線 from -> to 」を示す整数値です。</p> */
    LINEAR_FROM_TO: 1,
    /** <p>「直線 from <- to 」を示す整数値です。</p> */
    LINEAR_TO_FROM: 2,
    /** <p>「直線 from <-> to 」を示す整数値です。</p> */
    LINEAR_ONE_WAY_BIDIRECTIONAL: 3,
    /** <p>「直角直線矢印なし」を示す整数値です。</p> */
    SQUARE_NONE: 4,
    /** <p>「直角直線 from -> to 」を示す整数値です。</p> */
    SQUARE_FROM_TO: 5,
    /** <p>「直角直線 from <- to 」を示す整数値です。</p> */
    SQUARE_TO_FROM: 6,
    /** <p>「直角直線 from <-> to 」を示す整数値です。</p> */
    SQUARE_ONE_WAY_BIDIRECTIONAL: 7,
    /** <p>「双方向 2 本直線 ( 時計回り ) 」を示す整数値です。</p> */
    TWO_WAY: 8,
    /** <p>「双方向 2 本直線 ( 逆時計回り ) 」を示す整数値です。</p> */
    TWO_WAY_REVERSE: 9,
    /** <p>「双方向 2 本直線 ( 時計回り ) 片方矢印塗りつぶし 」を示す整数値です。</p> */
    TWO_WAY_FILL: 10,
    /** <p>「双方向 2 本直線 ( 逆時計回り ) 片方矢印塗りつぶし 」を示す整数値です。</p> */
    TWO_WAY_REVERSE_FILL: 11
};

export default KItemLineArrowType;