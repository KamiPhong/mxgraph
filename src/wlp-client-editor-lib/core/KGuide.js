import { mxConstants, mxGuide, mxPolyline, mxPoint } from "wlp-client-editor-lib/core/KClient";
import KItemType from "./KItemType";
class KGuide extends mxGuide {

     /**
     * remove dashed shapes.
     */
    createGuideShape = (horizontal) => {
        var guide = new mxPolyline([], mxConstants.GUIDE_COLOR, mxConstants.GUIDE_STROKEWIDTH);
        return guide;
    }
    
    /**
     * remove aly key
     */
    isEnabledForEvent = (evt) => {
        let selection = this.graph.getSelectionCells();
        let isEnable = selection.length === 1 ? true : false;
        if (selection.length === 2) {
            let firstCell = selection[0];
            if (firstCell.itemType === KItemType.BASE_ITEM || firstCell.itemType === KItemType.BASE_ITEM_LABEL) {
                isEnable = true;
            }
        }
        return isEnable;
    }

    /**
    * override function move mxguide
    * add params halfStrokeWidth
    */
    move = (bounds, delta, gridEnabled, clone, halfStrokeWidth) => {
        if (this.states != null && (this.horizontal || this.vertical) && bounds != null && delta != null)
        {
            var scale = this.graph.getView().scale;
            var tt = this.getGuideTolerance(gridEnabled) * scale;
            var b = bounds.clone();
            b.x += delta.x;
            b.y += delta.y;
            var overrideX = false;
            var stateX = null;
            var valueX = null;
            var overrideY = false;
            var stateY = null;
            var valueY = null;
            var ttX = tt;
            var ttY = tt;
            var left = b.x;
            var right = b.x + b.width;
            var center = b.getCenterX();
            var top = b.y;
            var bottom = b.y + b.height;
            var middle = b.getCenterY();
            halfStrokeWidth = halfStrokeWidth*scale
            // Snaps the left, center and right to the given x-coordinate
            function snapX(x, state, centerAlign, halfStrokeWidth, rightAlign)
            {
                x += this.graph.panDx;
                var override = false;
                
                if (centerAlign && Math.abs(x - center) <= ttX)
                {
                    delta.x = x - bounds.getCenterX();
                    ttX = Math.abs(x - center);
                    override = true;
                }
                else if (!centerAlign)
                {
                    if (Math.abs(x - left) <= ttX)
                    {
                        delta.x = x - bounds.x + halfStrokeWidth;
                        ttX = Math.abs(x - left);
                        override = true;
                    }
                    else if (rightAlign && Math.abs(x - right) <= ttX)
                    {
                        delta.x = x - bounds.x - bounds.width - halfStrokeWidth;
                        ttX = Math.abs(x - right);
                        override = true;
                    }
                }
                
                if (override)
                {
                    stateX = state;
                    valueX = Math.round(x - this.graph.panDx);
                    
                    if (this.guideX == null)
                    {
                        this.guideX = this.createGuideShape(true);
                        
                        // Makes sure to use either VML or SVG shapes in order to implement
                        // event-transparency on the background area of the rectangle since
                        // HTML shapes do not let mouseevents through even when transparent
                        this.guideX.dialect = (this.graph.dialect !== mxConstants.DIALECT_SVG) ?
                            mxConstants.DIALECT_VML : mxConstants.DIALECT_SVG;
                        this.guideX.pointerEvents = false;
                        this.guideX.init(this.graph.getView().getOverlayPane());
                    }
                }
                
                overrideX = overrideX || override;
            };
            
            // Snaps the top, middle or bottom to the given y-coordinate
            function snapY(y, state, centerAlign, halfStrokeWidth, bottomAlign)
            {
                y += this.graph.panDy;
                var override = false;
                
                if (centerAlign && Math.abs(y - middle) <= ttY)
                {
                    delta.y = y - bounds.getCenterY();
                    ttY = Math.abs(y -  middle);
                    override = true;
                }
                else if (!centerAlign)
                {
                    if (Math.abs(y - top) <= ttY)
                    {
                        delta.y = y - bounds.y + halfStrokeWidth;
                        ttY = Math.abs(y - top);
                        override = true;
                    }
                    else if (bottomAlign && Math.abs(y - bottom) <= ttY)
                    {
                        delta.y = y - bounds.y - bounds.height - halfStrokeWidth;
                        ttY = Math.abs(y - bottom);
                        override = true;
                    }
                }
                
                if (override)
                {
                    stateY = state;
                    valueY = Math.round(y - this.graph.panDy);
                    
                    if (this.guideY == null)
                    {
                        this.guideY = this.createGuideShape(false);
                        
                        // Makes sure to use either VML or SVG shapes in order to implement
                        // event-transparency on the background area of the rectangle since
                        // HTML shapes do not let mouseevents through even when transparent
                        this.guideY.dialect = (this.graph.dialect !== mxConstants.DIALECT_SVG) ?
                            mxConstants.DIALECT_VML : mxConstants.DIALECT_SVG;
                        this.guideY.pointerEvents = false;
                        this.guideY.init(this.graph.getView().getOverlayPane());
                    }
                }
                
                overrideY = overrideY || override;
            };

            for (var i = 0; i < this.states.length; i++)
            {
                var state =  this.states[i];
                let stateHaftstrokeWidth = (state.style.strokeWidth)/2*scale;
                
                // Logger.logConsole(state);
                if (state != null && !this.isStateIgnored(state))
                {
                    // Align x
                    if (this.horizontal)
                    {
                        snapX.call(this, state.x - stateHaftstrokeWidth, state, true, halfStrokeWidth, false);
                        snapX.call(this, state.x + state.width + stateHaftstrokeWidth, state, false, halfStrokeWidth, false);
                        snapX.call(this, state.x - stateHaftstrokeWidth, state, false, halfStrokeWidth, true);
                        snapX.call(this, state.getCenterX(), state, false, halfStrokeWidth, false);
                    }
        
                    // Align y
                    if (this.vertical)
                    {
                        snapY.call(this, state.y - stateHaftstrokeWidth, state, true, halfStrokeWidth, false);
                        snapY.call(this, state.y + state.height + stateHaftstrokeWidth, state, false, halfStrokeWidth, false);
                        snapY.call(this, state.y - stateHaftstrokeWidth, state, false, halfStrokeWidth, true);
                        snapY.call(this, state.getCenterY(), state, false, halfStrokeWidth, false);
                    }
                }
            }

            // Moves cells to the raster if not aligned
            this.graph.snapDelta(delta, bounds, !gridEnabled, overrideX, overrideY);

            // Redraws the guides
            var c = this.graph.container;
            const deltaMore = 55*scale;
            if (!overrideX && this.guideX != null)
            {
                this.guideX.node.style.visibility = 'hidden';
            }
            else if (this.guideX != null)
            {
                var minY = null;
                var maxY = null;
                
                if (stateX != null && bounds != null)
                {
                    minY = Math.min(bounds.y + delta.y - this.graph.panDy, stateX.y) - deltaMore;
                    maxY = Math.max(bounds.y + bounds.height + delta.y - this.graph.panDy, stateX.y + stateX.height) + deltaMore;
                }
                
                if (minY != null && maxY != null)
                {
                    this.guideX.points = [new mxPoint(valueX, minY), new mxPoint(valueX, maxY)];
                }
                else
                {
                    this.guideX.points = [new mxPoint(valueX, -this.graph.panDy), new mxPoint(valueX, c.scrollHeight - 3 - this.graph.panDy)];
                }
                
                this.guideX.stroke = this.getGuideColor(stateX, true);
                this.guideX.node.style.visibility = 'visible';
                this.guideX.redraw();
            }
            
            if (!overrideY && this.guideY != null)
            {
                this.guideY.node.style.visibility = 'hidden';
            }
            else if (this.guideY != null)
            {
                var minX = null;
                var maxX = null;
                
                if (stateY != null && bounds != null)
                {
                    minX = Math.min(bounds.x + delta.x - this.graph.panDx, stateY.x) - deltaMore;
                    maxX = Math.max(bounds.x + bounds.width + delta.x - this.graph.panDx, stateY.x + stateY.width) + deltaMore;
                }
                
                if (minX != null && maxX != null)
                {
                    this.guideY.points = [new mxPoint(minX, valueY), new mxPoint(maxX, valueY)];
                }
                else
                {
                    this.guideY.points = [new mxPoint(-this.graph.panDx, valueY), new mxPoint(c.scrollWidth - 3 - this.graph.panDx, valueY)];
                }
                
                this.guideY.stroke = this.getGuideColor(stateY, false);
                this.guideY.node.style.visibility = 'visible';
                this.guideY.redraw();
            }

            delta = this.getDelta(bounds, stateX, delta.x, stateY, delta.y)
        }
        
        return delta;
    }
    
}

export default KGuide;