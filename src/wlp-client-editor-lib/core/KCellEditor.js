import KClient, { mxCellEditor, mxEvent, mxConstants, mxClient } from "./KClient";
import KItemType from "./KItemType";
import * as GraphUtil from './KGraphUtil';
import Logger from "wlp-client-common/Logger";
import KConstants, { ITEM_LABEL_PADDING } from "./KConstants";
import KeyCodeConst from "wlp-client-editor-lib/utils/KeyCodeConst";
import EditorConsts from "wlp-client-common/consts/EditorConsts";
import {getCellSize} from '../utils/KUtils';
import KItemBase from './KItemBase';
import KLineItem from './KLineItem';
import KBaseItemLabel from './KBaseItemLabel';
import {convertToHtmlEntities} from '../utils/Utils';
import KeyCode from '../consts/KeyCode'
import HtmlCode from '../consts/HtmlCode'
import CellLabelConst from "wlp-client-editor-lib/consts/CellLabelConst";

class KCellEditor extends mxCellEditor {

    constructor(graph) {
        super(graph);

        this.verticalAlign = null;
    }

    //NOTE: [ThuyTV] override mxCellEditor function
    installListeners(elt) {
        this.cellResizeThread = null;

        mxEvent.addListener(elt, "keypress", this.resizeCellOnTyping);
        mxEvent.addListener(elt, "keydown", this.resizeCellOnTyping);
        mxEvent.addListener(elt, "blur", this.onTextareBlurHandler);
        mxEvent.addListener(elt, "keydown", this.checkBaseItemAddSpace);
        super.installListeners(elt);
    }

    //NOTE: [ThuyTV] override mxCellEditor function
    startEditing(cell, trigger) {
        if (this.textarea == null) {
            this.initTextArea(cell);
        }
        super.startEditing(cell, trigger);
        //change text align of editor element
        if (cell && this.textarea) {
            const state = this.graph.view.createState(cell);
            const style = state.style;

            //get current cell align
            const horizontalAlign = style[KConstants.TEXT_HORIZONTAL_ALIGN];

            if (this.textarea.style.textAlign !== horizontalAlign) {
                this.textarea.style.textAlign = horizontalAlign;
            }

            if (this.textarea instanceof HTMLElement) {
                const isOverflow = this.textarea.clientWidth < this.textarea.scrollWidth || this.textarea.clientHeight < this.textarea.scrollHeight;
                if (!isOverflow && !KClient.IS_IE) {
                    let verticalAlign =  GraphUtil.getStyleValue(cell.style, 'verticalAlign');
                    this.verticalAlign = GraphUtil.transformToFlexAlign(verticalAlign);
                    this.textarea.style.justifyContent = this.verticalAlign;
                }

                if (cell && cell.itemType === KItemType.BASE_ITEM_LABEL) {
                    if( KClient.IS_IE ){
                        this.textarea.style.whiteSpace = 'pre-wrap';
                    } else {
                        this.textarea.style.whiteSpace = 'break-spaces';
                    }

                }
                if(mxClient.IS_FF) {
                    if(this.textarea.innerText.length > 1 ) {
                        this.selectText(this.textarea)
                    }
                }
            }

            if (this.textNode) {
                let textNode = this.textNode;

                while (textNode.firstChild) {
                    if(mxClient.IS_IE && textNode.firstChild.nodeName !== '#text'){
                        textNode = textNode.firstChild;
                    } else if(
                        textNode.firstChild &&
                        textNode.firstChild.nodeName !== '#text'
                    ){
                        textNode = textNode.firstChild;
                    } else {
                        break;
                    }
                }

                if (textNode instanceof HTMLElement) {
                    this.textarea.scrollTop = textNode.scrollTop;
                }

            }
            const fontSize = style[mxConstants.STYLE_FONTSIZE] || mxConstants.DEFAULT_FONTSIZE;
            this.textarea.style.lineHeight = Math.round(fontSize * mxConstants.LINE_HEIGHT) + 'px';
        }

    }

    selectText(node) {
        if (document.body.createTextRange) {
            const range = document.body.createTextRange();
            range.moveToElementText(node);
            range.select();
        } else if (window.getSelection) {
            const selection = window.getSelection();
            const range = document.createRange();
            range.selectNodeContents(node);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }

    initTextArea = (cell) => {
        this.init();
        this.textarea.classList.add("color-inherit");

        if (cell && cell.itemType === KItemType.BASE_ITEM_LABEL) {
            this.textarea.style.minWidth = cell.geometry.width + 'px';
            this.textarea.style.maxHeight = cell.geometry.height + 'px';
            this.textarea.style.padding = `${ITEM_LABEL_PADDING}px`;
            // this.textarea.style.overflow = 'hidden';
            this.textarea.style.wordBreak = 'break-all';
            this.textarea.style.overflowY = "scroll";
            this.textarea.classList.add("hidden-scrollbar");
            this.autoSize = false;

            //TODO: IE not perfectly work with flex
            //try other way to set vertical align for text editor
            if (!KClient.IS_IE){
                let horizontalAlign = GraphUtil.getStyleValue(cell.style, 'align');
                this.textarea.style.display = 'flex';
                this.textarea.style.flexDirection = 'column';
                this.textarea.style.alignItems =  GraphUtil.transformToFlexAlign(horizontalAlign);
            }
        } else {
            this.autoSize = true;
            const state = this.graph.view.createState(cell);
            const style = state.style;
            if (style[mxConstants.STYLE_LABEL_PADDING]) {
                let padding =  style[mxConstants.STYLE_LABEL_PADDING];

                this.textarea.style.padding = padding + 'px';
            }
        }

        if (this.textarea instanceof HTMLElement) {
            this.textarea.addEventListener('keypress', this.handleKeyPress);
            this.textarea.addEventListener('keyup', this.handleKeyPress);
        }
    }

    /**
     * 
     * @param {MouseEvent} evt 
     */
    handleKeyPress = (evt) => {
        if (evt.target instanceof HTMLElement && this.verticalAlign) {
            if (evt.target.clientHeight < evt.target.scrollHeight) {
                evt.target.style.justifyContent = '';
            } else {
                evt.target.style.justifyContent = this.verticalAlign;
            }
        }
    }

    checkBaseItemAddSpace = (evt) => {
        const keyCodeSpaces = [ KeyCode.SPACE_KEYCODE, KeyCode.SPACE_JAPANESE_KEYCODE ]
        if( keyCodeSpaces.indexOf(evt.keyCode) > -1 && this.editingCell.itemType === KItemType.BASE_ITEM_LABEL ){
            document.querySelectorAll('.currentHeight').forEach(e => e.parentElement.removeChild(e));
            document.querySelectorAll('.newHeight').forEach(e => e.parentElement.removeChild(e));

            var cln = this.textarea.cloneNode(true);
            const regex = new RegExp(/\s/, "g");
            cln.innerHTML = cln.innerHTML.replace(regex, HtmlCode.SPACE)
            cln.style.maxHeight = 'none'
            cln.style.height = 'auto'
            cln.style.visibility = 'hidden';
            
            cln.classList.add('currentHeight')
            document.body.appendChild(cln);
            const currentHeight = cln.offsetHeight;
            document.body.removeChild(cln);
            cln = cln.cloneNode(true)
            cln.classList.remove('currentHeight')
            const space = document.createTextNode(' ');
            if( cln && cln.children.length > 0){
  
                let elChild = cln.children[ cln.children.length - 1];
                if( elChild.nodeName === 'DIV'){
                    if( elChild.children.length > 0){
                        elChild.insertBefore( space, elChild.children[0]);
                    } else {
                        elChild.innerHTML += ' ' + HtmlCode.SPACE;
                    }
                   
                } else {
                    cln.innerHTML += ' ' + HtmlCode.SPACE;
                }
            } else {
                cln.innerHTML += ' ' + HtmlCode.SPACE;
            }
            cln.classList.add('newHeight')
            document.body.appendChild(cln);
            const newHeight = cln.offsetHeight;
            document.body.removeChild(cln);

            if( newHeight > currentHeight && newHeight > CellLabelConst.TEXT_LABEL_MIN_HEIGHT ){
                evt.preventDefault();
                return
            }
            
        }
    }

    resizeCellOnTyping = (evt, x, y) => {

        const value = evt.target.innerText;
        let editingOffset = window.getSelection().getRangeAt(0).startOffset;

        /**
         * @author : ThuyTV
         * @do : don't re-calculate size if initput key is space
         * 
         * @author : QuanNH
         * @date : 04/08/2020
         * @dofix : thay đổi kích thước của item khi thêm ký tự khoảng trắng vào đầu chuỗi ký tự 
         */

        if (evt.keyCode === KeyCodeConst.SPACE && editingOffset > value.length -1 ) {
            return;
        }

        if (this.cellResizeThread !== null) {
            clearTimeout(this.cellResizeThread);
        }

        //async for better user experience
        this.cellResizeThread = setTimeout(() => {
            const elt = this.textarea;

            // @TODO fix bug thẻ br và div thêm vào từ sự kiện enter, bị lỗi enter 2 lần
            if( elt && elt.children.length > 0){
                for( let i=0; i <= elt.children.length; i++){
                    let elChild = elt.children[i];
                    if( elChild && elChild.nodeName === 'DIV'){
                        for( let ii=0; ii <= elChild.children.length; ii++){
                            let elChild2 = elChild.children[ii];
                            if( elChild2 && elChild.children.length === 1 && elChild2.nodeName === 'BR' && elChild.innerText.trim().length > 1){
                                elChild.removeChild(elChild2);
                            }
                        }
                    }
                }
            }
            
            if (this.editingCell && elt) {
                if (this.graph.model.isEdge(this.editingCell)) {
                    return;
                }

                if (this.editingCell.itemType === KItemType.DEFAULT_ITEM) {

                    this.graph.getModel().beginUpdate();
                    let editingCell = this.editingCell;
                    const state = this.graph.view.createState(editingCell);
                    const style = state.style;
                    const geo = editingCell.geometry.clone();

                    let size = getCellSize(this.graph, this.editingCell, elt.innerHTML);
                    geo.width = size.width;
                    geo.height = size.height;

                    // change editor horizontal align
                    // this.graph.setCellStyles(mxConstants.STYLE_ALIGN, style[mxConstants.STYLE_ALIGN], [editingCell]);
                    // if (geo.width > GraphUtil.DEFAULT_ITEM_WIDTH) {
                    //     if (style[mxConstants.STYLE_ALIGN] !== mxConstants.ALIGN_LEFT) {
                    //         this.graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_LEFT, [editingCell]);
                    //     }
                    // } else if (style[mxConstants.STYLE_ALIGN] !== mxConstants.ALIGN_CENTER) {
                    //     this.graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER, [editingCell]);
                    // }

                    // change editor vertical align
                    if (geo.height > GraphUtil.DEFAULT_ITEM_HEIGHT) {
                        if (style[mxConstants.STYLE_VERTICAL_ALIGN] !== mxConstants.ALIGN_TOP) {
                            this.graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP, [editingCell]);
                        }
                    } else if (style[mxConstants.STYLE_VERTICAL_ALIGN] !== mxConstants.ALIGN_MIDDLE) {
                        this.graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE, [editingCell]);
                    }

                    this.graph.getModel().setGeometry(editingCell, geo);
                    this.graph.getModel().endUpdate();
                }
            }
        }, 0);
    }

    getInitialValue(state, trigger) {
        let cell = state.cell;
        let result = this.graph.getEditingValue(cell, trigger);

        if( cell ){
            const isReplaceSpace = ( cell instanceof KItemBase || cell instanceof KBaseItemLabel );
            result = convertToHtmlEntities(result, isReplaceSpace)
        }
        
        return result;
    }

    //NOTE: [ThuyTV] override mxCellEditor function
    applyValue(state, value) {
        Logger.logConsole('[ThuyTv] override mxCellEditor->applyValue');
        let cell = state.cell;

        if( cell ){
            if( cell instanceof KItemBase || cell instanceof KBaseItemLabel){
                //replace multiple white space and replace with single one
                //keeps newline characters
                
                value = value.replace(/\n/gm, '__nl_')
                             .replace(/__nl_/gm, '\n');

                value = value.substring(0, EditorConsts.SHARE_TEXTAREA_MAXIMUM_CHARACTER);
            } else if ( cell instanceof KLineItem ){
                value = value.replace(/\t/g, "    ");
            }
            
        }

        super.applyValue(state, value);
    }

    /**
     * trả về event blur handler trong graph để kết thúc edit.
     * issue : event khi click vào khu vực ngoài graph nhưng không kết thúc edit label.
     */
    onTextareBlurHandler = () => {
        const {graph} = this;
        graph.handleWindowBlur();
    }

    resize = () => {
        super.resize();
        this.setTextareMinWidth();
    }

    setTextareMinWidth = () => {
        var state = this.graph.getView().getState(this.editingCell);
        if (state !== null)
        {
            const style = state.style;
            var scaleX = this.textarea.getBoundingClientRect().width / this.textarea.offsetWidth;
            const fontSize = style[mxConstants.STYLE_FONTSIZE] || mxConstants.DEFAULT_FONTSIZE;

            // fix con trỏ chuột trạng thái editing quá nhỏ không nhìn thấy trên firefox
            // trường hợp này đặc biệt và chỉ xảy ra ở Firefox,
            // @todo: sau khi set lại scale value default khi mở load map, sẽ cần quay lại và tính toán lại case này.
            if( KClient.IS_FIREFOX){
                if( fontSize*scaleX < 5){

                }
                if( parseFloat(scaleX) < 0.55 ){
                    this.textarea.style.fontSize = `${fontSize*1.1}px`;
                    this.textarea.style.minWidth = '5px';
                }
                
                if (fontSize*scaleX < 8){
                    this.textarea.style.fontSize = `${fontSize*1.2}px`;
                    this.textarea.style.minWidth = '15px';
                }

                if (fontSize*scaleX < 7){
                    this.textarea.style.fontSize = `${fontSize*1.5}px`;
                    this.textarea.style.minWidth = '30px';
                }
            }
        }
    }
}

export default KCellEditor;
