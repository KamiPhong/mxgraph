import { mxOutline, mxRectangle, mxEvent, mxUtils, mxClient, mxConstants, mxRectangleShape, mxMouseEvent } from "./KClient";
import { StageConfig, ZoomConfig } from "wlp-client-editor-lib/core/KGraphConfig";

class KOutline extends mxOutline {
    constructor(source, container, width, height) {
        super();
        this.source = source;
        this.gridContainer = null;
        this.labelsVisible = true;
        this.containerBounds = new mxRectangle(0, 0, width, height);
        this.isMouseDown = false;
        if (container != null) {
            this.init(container);
        }
    }

    //[ThuyTV] override mxOutline function
    init(container) {
        this.container = container;
        this.outline = this.createGraph(container);
        
        this.outline.setHtmlLabels(true);

        // Do not repaint when suspended
        let outlineGraphModelChanged = this.outline.graphModelChanged;
        this.outline.graphModelChanged = mxUtils.bind(this, function (changes) {
            if (!this.suspended && this.outline != null) {
                outlineGraphModelChanged.apply(this.outline, arguments);
            }
        });

        // Enables faster painting in SVG
        if (mxClient.IS_SVG) {
            let node = this.outline.getView().getCanvas().parentNode;
            node.setAttribute('shape-rendering', 'optimizeSpeed');
            node.setAttribute('image-rendering', 'optimizeSpeed');
        }

        // Hides cursors and labels
        this.outline.labelsVisible = this.labelsVisible;
        this.outline.setEnabled(false);

        this.updateHandler = mxUtils.bind(this, function (sender, evt) {
            if (!this.suspended && !this.active) {
                this.update();
            }
        });

        // Updates the scale of the outline after a change of the main graph
        this.source.getModel().addListener(mxEvent.CHANGE, this.updateHandler);
        this.outline.addMouseListener(this);

        // Adds listeners to keep the outline in sync with the source graph
        let view = this.source.getView();
        view.addListener(mxEvent.SCALE, this.updateHandler);
        view.addListener(mxEvent.TRANSLATE, this.updateHandler);
        view.addListener(mxEvent.SCALE_AND_TRANSLATE, this.updateHandler);
        view.addListener(mxEvent.DOWN, this.updateHandler);
        view.addListener(mxEvent.UP, this.updateHandler);

        // Updates blue rectangle on scroll
        mxEvent.addListener(this.source.container, 'scroll', this.updateHandler);

        this.panHandler = mxUtils.bind(this, function (sender) {
            if (this.updateOnPan) {
                this.updateHandler.apply(this, arguments);
            }
        });
        this.source.addListener(mxEvent.PAN, this.panHandler);

        this.source.addListener(mxEvent.REFRESH, this.refreshHandler);

        //[ThuyTV] add grid
        this.createGrid(3, 3);

        this.bounds = new mxRectangle(0, 0, 0, 0);
        
        this.initSelectionBorder();
        this.update(false);
    }

    /**
     * [ThuyTV]
     * @param {Boolean} visible
     */
    setLabelVisible = (visible) => {
        visible = Boolean(visible);
        if (this.outline.labelsVisible !== visible) {
            this.outline.labelsVisible = visible;
        }
    }

    //[ThuyTV]
    // Creates the blue rectangle for the viewport
    initSelectionBorder = () => {
        this.selectionBorder = this.createSelectionBorder();

        mxEvent.addGestureListeners(this.selectionBorder.node, this.selectionGestureHandler);

        // Creates a small blue rectangle for sizing (sizer handle)
        this.sizer = this.createSizer();

        mxEvent.addGestureListeners(this.sizer.node, this.selectionGestureHandler);

        this.selectionBorder.node.style.display = (this.showViewport) ? '' : 'none';
        this.sizer.node.style.display = this.selectionBorder.node.style.display;
        this.selectionBorder.node.style.cursor = 'move';
    }

    //create html selection border and init shape to view
    createSelectionBorder() {
        let selectionBorder = new mxRectangleShape(this.bounds, null, mxConstants.OUTLINE_COLOR, mxConstants.OUTLINE_STROKEWIDTH);
        selectionBorder.dialect = mxConstants.DIALECT_STRICTHTML;

        if (this.forceVmlHandles) {
            selectionBorder.isHtmlAllowed = function () {
                return false;
            };
        }

        selectionBorder.init(this.container);
        return selectionBorder;
    }

    createSizer() {
        let sizer = super.createSizer();

        if (this.forceVmlHandles) {
            sizer.isHtmlAllowed = function () {
                return false;
            };
        }

        sizer.init(this.outline.getView().getOverlayPane());

        if (this.enabled) {
            sizer.node.style.cursor = 'nwse-resize';
        }

        return sizer;
    }

    // Refreshes the graph in the outline after a refresh of the main graph
    refreshHandler(sender) {
        this.outline.setStylesheet(this.source.getStylesheet());
        this.outline.refresh();
    }

    //[ThuyTv]: Migrate from mxOutline
    // Handles event by catching the initial pointer start and then listening to the
    // complete gesture on the event target. This is needed because all the events
    // are routed via the initial element even if that element is removed from the
    // DOM, which happens when we repaint the selection border and zoom handles.
    selectionGestureHandler = (evt) => {
        let t = mxEvent.getSource(evt);
        let redirect = mxUtils.bind(this, function (evt) {
            this.outline.fireMouseEvent(mxEvent.MOUSE_MOVE, new mxMouseEvent(evt));
        });

        let redirect2 = mxUtils.bind(this, function (evt) {
            mxEvent.removeGestureListeners(t, null, redirect, redirect2);
            this.outline.fireMouseEvent(mxEvent.MOUSE_UP, new mxMouseEvent(evt));
        });

        mxEvent.addGestureListeners(t, null, redirect, redirect2);
        this.outline.fireMouseEvent(mxEvent.MOUSE_DOWN, new mxMouseEvent(evt));
    }

    //[ThuyTV] override mxOutline function
    update(revalidate) {
        let scale = this.computesScale();
        if (scale > 0) {
            if (this.outline.getView().scale !== scale) {
                this.outline.getView().scale = scale;
                revalidate = true;
            }

            const navView = this.outline.getView();
            const sourceView = this.source.getView();
            if (navView.currentRoot !== sourceView.currentRoot) {
                navView.setCurrentRoot(sourceView.currentRoot);
            }

            let bounds = this.getSelectionBounds();
            let computedBounds = this.computesBounds(bounds.x, bounds.y, bounds.width, bounds.height);
            this.redrawSelectionBorder(computedBounds);

            // Updates the bounds of the zoom handle at the bottom right
            const b = this.sizer.bounds;
            const b2 = new mxRectangle(
                this.bounds.x + this.bounds.width - b.width / 2,
                this.bounds.y + this.bounds.height - b.height / 2,
                b.width,
                b.height
            );

            if (b.x !== b2.x || b.y !== b2.y || b.width !== b2.width || b.height !== b2.height) {
                this.sizer.bounds = b2;

                // Avoids update of visibility in redraw for VML
                if (this.sizer.node.style.visibility !== 'hidden') {
                    this.sizer.redraw();
                }
            }

            if (revalidate) {
                this.outline.view.revalidate();
            }
        }

    }

    mouseDown(sender, me) {
        this.isMouseDown = true;

        const boundsCopy =  Object.assign({}, this.bounds);
        let bounds = this.getSelectionBounds();
        let scale = this.outline.getView().scale;
        let tx = (bounds.width - boundsCopy.width);
        let ty = (bounds.height - boundsCopy.height);
    
        if (bounds.x + bounds.width > this.containerBounds.width) {
            tx = -tx;
        }
        if (bounds.y + bounds.height > this.containerBounds.height) {
            ty = -ty;
        }

        bounds.x = bounds.x - tx;
        bounds.y = bounds.y - ty;

        this.redrawSelectionBorder(bounds);
        const currTranslate = this.source.getView().translate;
        //reset translate
        this.source.getView().setTranslate(
            Math.round(currTranslate.x - tx / scale),
            Math.round(currTranslate.y - ty / scale)
        );

        super.mouseDown(sender, me);
    }

    mouseUp(sender, me) {
        this.isMouseDown = false;
        super.mouseUp(sender, me);
    }

    //[ThuyTV] override mxOutline function
    getTranslateForEvent(me) {
        let delta = super.getTranslateForEvent(me);
        const limits = {
            left: 0,
            top: 0,
            bottom: this.containerBounds.height,
            right: this.containerBounds.width
        }
        const nextX = this.bounds.x + delta.x;
        const nextY = this.bounds.y + delta.y;

        const bounds = {
            left: nextX,
            top: nextY,
            right: nextX + this.bounds.width - mxConstants.OUTLINE_STROKEWIDTH,
            bottom: nextY + this.bounds.height - mxConstants.OUTLINE_STROKEWIDTH
        }

        //is reach left side limit?
        if (bounds.left < limits.left) {
            delta.x = delta.x - nextX;
        }
        //is reach top limit?
        if (bounds.top < limits.top) {
            delta.y = delta.y - nextY;
        }

        //is reach right side limit?
        if (bounds.right > limits.right) {
            delta.x = delta.x - (bounds.right - limits.right);
        }

        //is reach bottom limit?
        if (bounds.bottom > limits.bottom) {
            delta.y = delta.y - (bounds.bottom - limits.bottom);
        }
        return delta;
    }

    computesScale() {
        const scale = this.containerBounds.width / StageConfig.MAX_WIDTH;
        return scale;
    }

    redrawSelectionBorder(bounds) {
        if (bounds !== null) {
            this.bounds = bounds;
            const currentBounds = this.selectionBorder.bounds;   
            if (   
                currentBounds.x !== this.bounds.x ||
                currentBounds.y !== this.bounds.y ||
                currentBounds.width !== this.bounds.width ||
                currentBounds.height !== this.bounds.height) {
                this.selectionBorder.bounds = this.bounds;
                this.selectionBorder.redraw();
            }
        }
    }

    getSelectionBounds() {
        //Prepares local variables for computations
        const navView = this.outline.getView();
        const t2 = navView.translate;
        const sourceView = this.source.getView();
        const scale = sourceView.scale;
        const boundsCopy =  Object.assign({}, this.bounds);  
        const scale2 = scale / navView.scale;
        const scale3 = 1.0 / navView.scale;
        const sourceContainer = this.source.container;
        const t = this.source.view.translate;

        let x = (t2.x - t.x - this.source.panDx) / scale3;
        let y = (t2.y - t.y - this.source.panDy) / scale3;
        let width = (sourceContainer.clientWidth) / scale2;
        let height = (sourceContainer.clientHeight) / scale2;

        if (scale === ZoomConfig.MIN_SCALE && boundsCopy.width === this.containerBounds.width && this.isMouseDown) {
            width = boundsCopy.width;
            x = 0;
        }
        
        return new mxRectangle(x, y, width, height);
    }

    computesBounds = (x, y, width, height) => {
        if (x < 0) {
            width = width + x;
            x = 0;
        }

        if (y < 0) {
            height = height + y;
            y = 0;
        }

        const limitW = this.containerBounds.width;
        const limitH = this.containerBounds.height;
        if (x + width > limitW) {
            width = width - (x + width - limitW);
        }

        if (y + height > limitH) {
            height = height - (y + height - limitH);
        }
        
        return new mxRectangle(x, y, width, height);
    }

    gridCellClickHandler = (evt) => {
        const x = Number(evt.target.dataset.x);
        const y = Number(evt.target.dataset.y);
        const bounds = this.getSelectionBounds();
        const tx = x - bounds.width/2;
        const ty = y - bounds.height/2;

        this.source.getView().setTranslate(
            -tx /this.outline.view.scale, 
            -ty / this.outline.view.scale
        );
    }

    createGrid(cols, rows) {
        if (!this.container) {
            return;
        }

        let scale = this.computesScale();
        const colSize = Math.floor(StageConfig.MAX_WIDTH / cols * scale);
        const rowSize = Math.floor(StageConfig.MAX_HEIGHT / rows * scale);

        this.container.style.position = 'relative';
        this.container.style.userSelect = 'none';
        this.gridContainer = document.createElement('div');
        let val = 1;
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < cols; j++) {
                let cell = this.createGridCell(colSize, rowSize);
                const x = Math.floor(j * colSize);
                const y = Math.floor(i * rowSize);
                const centerX = x + colSize/2;
                const centerY = y + rowSize/2;

                cell.style.left = x + 'px';
                cell.style.top = y + 'px';
                cell.innerHTML = val;
                cell.setAttribute('data-x', centerX);
                cell.setAttribute('data-y', centerY);
                cell.onclick = this.gridCellClickHandler;
                this.gridContainer.appendChild(cell);
                val = val + 1;
            }
        }

        this.container.appendChild(this.gridContainer);
    }

    createGridCell(width, height) {
        let div = document.createElement('div');
        div.style.width = width + 'px';
        div.style.height = height + 'px';
        div.style.position = 'absolute';
        div.style.display = 'flex';
        div.style.justifyContent = 'center';
        div.style.alignItems = 'center';
        div.classList.add('outline-grid-cell');
        div.classList.add('noselect');

        return div;
    }

    destroy(){
        if(this.gridContainer && this.gridContainer.parentNode){
            this.gridContainer.parentNode.removeChild(this.gridContainer);
        }

        super.destroy();
    }
}

export default KOutline;