import KItemBase from './KItemBase'
import ItemShape from './KGraphItemShape';
import KItemType from './KItemType';
import {  mxGeometry, mxCell } from "wlp-client-editor-lib/core/KClient";
import * as GraphUtil from './KGraphUtil';
/**
 * BaseItem クラスは、ベースアイテム専用のクラスです。
 */
export default class KItem extends KItemBase {
    
    // eslint-disable-next-line no-useless-constructor
    constructor(model) {
        super(model);
        
        this.itemType = KItemType.DEFAULT_ITEM;
        this.style = GraphUtil.getItemModelStyle(model);
        this.setGeometry(new mxGeometry(model.x, model.y, GraphUtil.DEFAULT_ITEM_WIDTH, GraphUtil.DEFAULT_ITEM_HEIGHT));
        this.likedState = model.liked
    }
    
    
    /**
     * 
     * @param {mxCell} cell
     */
    importVertex = (cell) => {
        let mxCellStyle = cell.style;
        let cellProperties = mxCellStyle.split(';');
        if (cellProperties.length > 0) cellProperties.forEach((p) => {
            let val = p.split('=');
            if (val.length === 2) {
                switch (val[0]) {
                    case 'shape':
                        this.shape = parseInt(ItemShape.getConstantFromString(val[1]));
                        break;
                    case 'style':
                        this.style = parseInt(val[1]);
                        break;
                    default: break;
                }
            }
        });
        this.x = cell.geometry.x;
        this.y = cell.geometry.y;
        this.text = cell.value;
    }

    
}