
const { default: KFilter } = require("./KFilter");
const FILTER_IS_NEW = 'is_new';
class KFilterIsNew extends KFilter  {
    constructor(){
        super(FILTER_IS_NEW, '#ff9900')
    }
}

export { FILTER_IS_NEW };
export default KFilterIsNew;