
import KFilter from './KFilter'
const FILTER_EDGE_HOVER = 'edge_hover';
class KFilterEdgeHover extends KFilter  {
    constructor(){
        super(FILTER_EDGE_HOVER, '#ff9900')
    }

    gaussianBlur = () => {
        let feGaussianBlur = this.createElement('feGaussianBlur');
        feGaussianBlur.setAttribute("result", 'blur-out');
        feGaussianBlur.setAttribute("in", 'SourceAlpha');
        feGaussianBlur.setAttribute("stdDeviation", 3);
        return feGaussianBlur;
    }

    blend = () => {
        let feBlend = this.createElement('feBlend');
        feBlend.setAttribute("in", 'SourceGraphic');
        feBlend.setAttribute("in2", 'color-out');
        feBlend.setAttribute("mode", "normal");
        return feBlend;
    }
    
    colorMatrix = ()=>{
        let feColorMatrix = this.createElement('feColorMatrix');
        feColorMatrix.setAttribute("in", 'the-shadow');
        feColorMatrix.setAttribute("result", 'color-out');
        feColorMatrix.setAttribute("type", 'matrix');
        feColorMatrix.setAttribute("values", '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .6 0');
        return feColorMatrix;
    }

    offset = ()=>{
        let feOffset = this.createElement('feOffset');
        feOffset.setAttribute("result", 'the-shadow');
        feOffset.setAttribute("in", 'blur-out');
        feOffset.setAttribute("dx", 5);
        feOffset.setAttribute("dy", 5);
        return feOffset;
    }

    merge=()=>{
        let feMerge = this.createElement('feMerge');

        let feMergeNode = this.createElement('feMergeNode');
        let feMergeNodeSource = this.createElement('feMergeNode');
        feMergeNodeSource.setAttribute("in","SourceGraphic");

        feMerge.appendChild(feMergeNode);
        feMerge.appendChild(feMergeNodeSource);
        return feMerge;
    }

    getElement = ()=>{
        let filter = this.createElement('filter');
        filter.setAttribute("id",this.id);
        filter.setAttribute("filterUnits","userSpaceOnUse");
        filter.appendChild(this.gaussianBlur());
        filter.appendChild(this.offset());
        filter.appendChild(this.colorMatrix());
        filter.appendChild(this.blend());
        filter.appendChild(this.merge());

        return filter;

    }
}

export { FILTER_EDGE_HOVER };
export default KFilterEdgeHover;