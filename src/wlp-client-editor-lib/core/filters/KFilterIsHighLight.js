
const { default: KFilter } = require("./KFilter");
const FILTER_IS_HIGH_LIGHT = 'is_high_light';

class KFilterIsHighLight extends KFilter  {
    constructor(){
        super(FILTER_IS_HIGH_LIGHT, '#66ffd9')
    }
}

export {FILTER_IS_HIGH_LIGHT};
export default KFilterIsHighLight;