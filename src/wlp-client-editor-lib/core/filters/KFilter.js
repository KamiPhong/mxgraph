import { mxConstants } from "wlp-client-editor-lib/core/KClient";
import * as KGraphUtil from 'wlp-client-editor-lib/core/KGraphUtil';

class KFilter {
    filterUnits = 'userSpaceOnUse'
    color = null;

    constructor(id, color){
        this.id = id;
        this.color = color;
    }

    createElement = (name)=>{
        return document.createElementNS(mxConstants.NS_SVG, name)
    }
    
    gaussianBlur = () => {
        let feGaussianBlur = this.createElement('feGaussianBlur');
        feGaussianBlur.setAttribute("in", 'SourceGraphic');
        feGaussianBlur.setAttribute("stdDeviation", 10);
        return feGaussianBlur;
    }

    offset = ()=>{
        let feOffset = this.createElement('feOffset');
        feOffset.setAttribute("dx", 0);
        feOffset.setAttribute("dy", 0);
        feOffset.setAttribute("result", 'offsetblur');
        return feOffset;
    }

    flood = ()=>{
        let feFlood = this.createElement('feFlood');
        if( this.color ){
            feFlood.setAttribute("flood-color", this.color);
        }
        return feFlood;
    }

    composite=()=>{
        let feComposite = this.createElement('feComposite');
        feComposite.setAttribute("in2","offsetblur");
        feComposite.setAttribute("operator","in");

        return feComposite;
    }

    merge=()=>{
        let feMerge = this.createElement('feMerge');

        let feMergeNode = this.createElement('feMergeNode');
        let feMergeNodeSource = this.createElement('feMergeNode');
        feMergeNodeSource.setAttribute("in","SourceGraphic");

        feMerge.appendChild(feMergeNode);
        feMerge.appendChild(feMergeNodeSource);
        return feMerge;
    }

    getElement = ()=>{
        let filter = this.createElement('filter');
        filter.setAttribute("id",this.id);
        
        switch (KGraphUtil.browser && KGraphUtil.browser.name) {
            case 'safari':
                filter.setAttribute("x","-25%");
                filter.setAttribute("y","-25%");
                filter.setAttribute("width","200%");
                filter.setAttribute("height","200%");

              break;
            default:
                filter.setAttribute("filterUnits","userSpaceOnUse");

                break;
        }

        filter.appendChild(this.gaussianBlur());
        filter.appendChild(this.offset());
        filter.appendChild(this.flood());
        filter.appendChild(this.composite());
        filter.appendChild(this.merge());
        return filter;

    }
}

export default KFilter;