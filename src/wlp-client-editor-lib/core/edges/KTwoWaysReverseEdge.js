import { mxArrowConnector } from "../KClient";
import { DEFAULT_ARROW_WIDTH, ARROW_TAIL_LENGTH } from "../KGraphUtil";

class KTwoWaysReverseEdge extends mxArrowConnector {

    paintEdgeShape(c, pts) {
        // Geometry of arrow
        let strokeWidth = this.strokewidth;
        const endWidth = DEFAULT_ARROW_WIDTH;
        const edgeWidth = strokeWidth;
        const openEnded = this.isOpenEnded();
        const spacing = (openEnded) ? 0 : this.arrowSpacing + strokeWidth / 2;
        const endSize = ARROW_TAIL_LENGTH / 3;

        if (pts.length < 4) {
            return;
        }

        const p0 = pts[0];
        const p1 = pts[1];
        const p2 = pts[2];
        const p3 = pts[3];
        const dx = p0.x - p1.x;
        const dy = p0.y - p1.y;
        const dist = Math.sqrt(dx * dx + dy * dy);
        const nx = dx / dist;
        const ny = dy / dist;

        const dx2 = p2.x - p3.x;
        const dy2 = p2.y - p3.y;
        const dist2 = Math.sqrt(dx2 * dx2 + dy2 * dy2);
        const nx2 = dx2 / dist2;
        const ny2 = dy2 / dist2;

        if (dist === 0 || dist2 === 0) {
            return;
        }

        c.begin();

        c.moveTo(p1.x, p1.y);
        this.painTailedMarker(c, p0.x, p0.y, -nx, -ny, endSize, endWidth, edgeWidth, spacing);
        
        c.moveTo(p2.x, p2.y);
        this.painTailedMarker(c, p3.x, p3.y, nx2, ny2, endSize, endWidth, edgeWidth, spacing);

        c.close();
        c.fillAndStroke();
    }

    painTailedMarker = (c, ptX, ptY, nx, ny, size, arrowWidth, edgeWidth, spacing) => {
        const widthArrowRatio = edgeWidth / arrowWidth;
        const orthx = edgeWidth * ny / 2;
        const orthy = -edgeWidth * nx / 2;
        const spaceX = (spacing + size) * nx;
        const spaceY = (spacing + size) * ny;
        const tailLength = ARROW_TAIL_LENGTH;
        const tailX = nx * tailLength;
        const tailY = ny * tailLength;

        c.lineTo(ptX , ptY);
        c.moveTo(ptX - orthx + spaceX, ptY - orthy + spaceY);
        c.lineTo(ptX - orthx / widthArrowRatio + spaceX + tailX, ptY - orthy / widthArrowRatio + spaceY + tailY);
        c.lineTo(ptX + spacing * nx, ptY + spacing * ny);
        c.lineTo(ptX + orthx / widthArrowRatio + spaceX + tailX, ptY + orthy / widthArrowRatio + spaceY + tailY);
        c.lineTo(ptX + orthx + spaceX, ptY + orthy + spaceY);
    }
}

export default KTwoWaysReverseEdge;