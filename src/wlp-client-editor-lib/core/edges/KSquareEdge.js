import { mxEdgeStyle } from "../KClient";

function KSquareEdge(state, source, target, points, result) {
    if (source && target) {
        const rect1 = source.cellBounds;
        const rect2 = target.cellBounds;
        if (rect1 && rect2) {
            //horizontal
            if (rect1.x <= rect2.x + rect2.width && rect1.x + rect1.width >= rect2.x) {
                mxEdgeStyle.TopToBottom(state, source, target, points, result);
            } else {
                //vertical
                mxEdgeStyle.SideToSide(state, source, target, points, result);
            }
        }
    }
}

export default KSquareEdge;