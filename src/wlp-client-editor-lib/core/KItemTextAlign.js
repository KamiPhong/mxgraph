let KItemTextAlign = {
        LEFT : 0,
        CENTER : 1,
        RIGHT : 2,
        VALUE_LEFT: 'left',
        VALUE_CENTER: 'center',
        VALUE_RIGHT: 'right'
}
export default KItemTextAlign