import { mxEvent } from "wlp-client-editor-lib/core/KClient";
import { ZoomConfig } from "wlp-client-editor-lib/core/KGraphConfig";
import ItemMenuButtonBarHandler from 'wlp-client-editor-lib/handlers/ItemMenuButtonBarHandler';
import KBaseItem from 'wlp-client-editor-lib/core/KBaseItem'

class KGraphZoomHandler {
    constructor(graph) {
        this.graph = graph;
        this.useMouseWheel = false;
        this.onZoomChange = null;
        this.disableZoomOnCell = false;
        this.isMouseOnCell = false;
        this.initialScale = null;
        this.itemMenuButtonBarHandler = new ItemMenuButtonBarHandler(this.graph);
        this.init();

        this.graph.addListener(mxEvent.GESTURE, this.gestureHandler);
    }

    gestureHandler = (sender, eo) => {
        const evt = eo.getProperty('event');

        if (!mxEvent.isConsumed(evt) && evt.type === 'gesturestart') {
            this.initialScale = this.graph.view.scale;

            // Forces start of panning when pinch gesture starts
            if (!this.active && this.mouseDownEvent != null) {
                this.start(this.mouseDownEvent);
                this.mouseDownEvent = null;
            }
        }
        else if (evt.type === 'gestureend' && this.initialScale != null) {
            this.initialScale = null;
        }

        if (this.initialScale != null) {
            this.gestureZoom(evt);
        }
    }

    gestureZoom = (evt) => {
        let value = Math.round(this.initialScale * evt.scale * 100) / 100;

        if (ZoomConfig.MIN_SCALE != null) {
            value = Math.max(ZoomConfig.MIN_SCALE, value);
        }

        if (ZoomConfig.MAX_SCALE != null) {
            value = Math.min(ZoomConfig.MAX_SCALE, value);
        }

        if (this.graph.view.scale != value) {
            this.graph.zoomTo(value);
            mxEvent.consume(evt);
            const nextZoom = Math.max(ZoomConfig.MIN_ZOOM_VALUE, Math.min(ZoomConfig.MAX_ZOOM_VALUE, Math.floor(value / ZoomConfig.STEP_ZOOM)));
            if (this.currentZoom !== nextZoom) {
                this.currentZoom = nextZoom;
                if (this.onZoomChange && typeof (this.onZoomChange) === 'function') {
                    this.onZoomChange(this.currentZoom, this.graph.view.scale);
                    this.itemMenuButtonBarHandler.handleZoom();
                }
            }
        }
    }

    setUseMouseWheel = (isUse) => {
        const prevValue = this.useMouseWheel;
        this.useMouseWheel = Boolean(isUse);
        this.onZoomMouseWheelChange(prevValue, this.useMouseWheel);
    }
    

    // setIsMouseOnCell = (isOn) => {
    //     this.isMouseOnCell = isOn;
    // }

    onZoomMouseWheelChange = (prevValue, currentValue) => {
        if (prevValue !== currentValue) {
            if (currentValue === true) {
                mxEvent.addListener(this.graph.container, 'wheel', this.onMouseWheel);
            } else {
                mxEvent.removeListener(this.graph.container, 'wheel', this.onMouseWheel);
            }
        }
    }

    /**
     * @author ThuyTV
     * @description calculate cumulativeZoomFactor base on scale
     * @param {boolean} zoomIn
     * @return {float}
     */
    calculateZoomFactor = (zoomIn, step = 1) => {
        const { graph } = this;
        let cumulativeZoomFactor = 1;
        const zoomFactor = 1 + (ZoomConfig.STEP_ZOOM * step);
        if (zoomIn) {
            // Uses to 5% zoom steps for better grid rendering in webkit
            // and to avoid rounding errors for zoom steps
            cumulativeZoomFactor *= zoomFactor;
            cumulativeZoomFactor = Math.round(graph.view.scale * cumulativeZoomFactor * 20) / 20 / graph.view.scale;
        }
        else {
            // Uses to 5% zoom steps for better grid rendering in webkit
            // and to avoid rounding errors for zoom steps
            cumulativeZoomFactor /= zoomFactor;
            cumulativeZoomFactor = Math.round(graph.view.scale * cumulativeZoomFactor * 20) / 20 / graph.view.scale;
        }

        return Math.max(0.4, Math.min(graph.view.scale * cumulativeZoomFactor, 140)) / graph.view.scale;
    }

    /**
     * @author ThuyTV
     * @description handle zoom in or out base on dx and dy
     * @param {boolean} isZoomIn
     * @param {number} dx
     * @param {number} dy
     * @return void
     */
    zoom = (isZoomIn, x, y, step = 1) => {
        const { graph } = this;

        if (
            (isZoomIn && graph.view.scale >= ZoomConfig.MAX_SCALE) ||
            (!isZoomIn && graph.view.scale <= ZoomConfig.MIN_SCALE)
        ) {
            return;
        }

        if (typeof(x) !== 'number') {
            x = graph.container.offsetWidth / 2;
        }
        
        if (typeof(y) !== 'number') {
            y = x = graph.container.offsetHeight / 2;
        }
        
        const currentScale = graph.view.scale;
        const currentTranslate = graph.view.translate;
        const delta = isZoomIn ? 1 : -1;
       
        x = Math.round((x + currentTranslate.x) * currentScale);
        y = Math.round((y + currentTranslate.y) * currentScale);
        let factor = ( step * this.stepZoom * delta);
        let nextScale = Math.max( ZoomConfig.MIN_SCALE, Math.min( ZoomConfig.MAX_SCALE, currentScale + factor ) );
        this.currentZoom += step * delta;

        if (this.maxZoom == ZoomConfig.MAX_ZOOM_VALUE) {
            if (this.currentZoom === ZoomConfig.MIN_ZOOM_VALUE) {
                nextScale = ZoomConfig.MIN_SCALE
            } else if (this.currentZoom === ZoomConfig.MAX_ZOOM_VALUE) {
                nextScale = ZoomConfig.MAX_SCALE
            }
        }
        
        factor = nextScale/currentScale;
        
        let f = 1;
        if (factor > 1) {
            f = -(factor - 1) / (nextScale);
        } else {
            f = (1 / factor - 1) / (currentScale);
        }

        graph.view.scaleAndTranslate(nextScale, currentTranslate.x + x*f, currentTranslate.y + y*f);

        if (this.onZoomChange && typeof (this.onZoomChange) === 'function') {
            this.onZoomChange(this.currentZoom, this.graph.view.scale);
            this.itemMenuButtonBarHandler.handleZoom();
        }
    }

    /**
     * @author ThuyTV
     * @description zoom on mouseWheel
     * @param {MouseWheelEvent} evt
     * @return void
     */
    onMouseWheel = (evt) => {
        if(this.disableZoomOnCell && this.isMouseOnCell){
            return; 
        }
        const isUp = evt.deltaY < 0;
        const { graph } = this;

        let point = graph.getPointForEvent(evt, false);
        this.zoom(isUp, point.x, point.y);
    }

    initZoomFactor = () => {
        this.currentZoom = ZoomConfig.INIT_ZOOM_VALUE;
        this.maxZoom = ZoomConfig.MAX_ZOOM_VALUE;
        this.minZoom = ZoomConfig.MIN_ZOOM_VALUE;
        this.stepZoom = ZoomConfig.STEP_ZOOM;
    }

    setZoomFactor = (currentZoomValue, maxZoomValue) => {
        this.currentZoom = currentZoomValue;
        this.maxZoom = maxZoomValue
        this.stepZoom = ZoomConfig.MAX_SCALE/maxZoomValue
    }

    initMouseMoveListener = () => {
        this.graph.addMouseListener({
            mouseDown: () => {},
            mouseUp: () => {},
            mouseMove: (sender, me) => {
                if (me.getCell()) {
                    if (me.getCell() instanceof KBaseItem && (this.graph.isAllBaseItemLocked || !this.graph.isEnabled())) {
                        this.isMouseOnCell = false;
                    } else {
                        this.isMouseOnCell = true;
                    }

                } else {
                    this.isMouseOnCell = false;
                }
            }
        });
    }

    init = () => {
        this.initZoomFactor();
        this.initMouseMoveListener();
    }

    destroy = () => {
        mxEvent.removeListener(this.graph.container, 'wheel', this.onMouseWheel);
    }

}

export default KGraphZoomHandler;