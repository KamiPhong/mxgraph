import mxGraphFactory from 'mxgraph';
import KItemType from 'wlp-client-editor-lib/core/KItemType';
const { mxGraphSelectionModel } = new mxGraphFactory();
class KSelectionModel extends mxGraphSelectionModel {
    addCells(cells) {
        let selectableCells = this.getSelectableCells(cells);
        if (selectableCells.length > 0) {
            this.setCells(selectableCells);
        }
    }

    /**
     * QuanNH overwrite super function to toggle select multi item same type
     * @param {*} cell 
     */
    addCell(cell){
        if (cell != null) {
            let selectableCells = this.getSelectableCells([cell]);
            let cellsSelected = this.cells.slice();
            let cellSelected = cellsSelected[0];

            if (cellsSelected.length > 0 && cellSelected.itemType === KItemType.BASE_ITEM_LABEL) {
                cellSelected = cellSelected.baseItem;
            }
            
            
            if (selectableCells.length > 0 ) {
           
                // bỏ selection EDGE trước đó, chỉ set selection duy nhất 1 EDGE 
                if (cell.itemType === KItemType.LINK_ITEM && cellSelected && cellSelected.itemType===KItemType.LINK_ITEM) {
                    this.removeCells(cellsSelected);
                    super.addCells([cell]);
                    return;
                }
                
                // nếu trước đó đang selected EDGE, thì bỏ selelection EDGE đi, và set selelection cho Item/ItemBase
                if (
                    (cell.itemType === KItemType.DEFAULT_ITEM || cell.itemType === KItemType.BASE_ITEM || cell.itemType === KItemType.BASE_ITEM_LABEL) && 
                    cellSelected && cellSelected.itemType === KItemType.LINK_ITEM
                ) {
                    this.removeCells(cellsSelected);
                    cellsSelected = [];
                }
    
                if (cellsSelected.length > 0 && cellSelected.itemType !== cell.itemType) {
                    if (cell.itemType === KItemType.BASE_ITEM_LABEL && cellSelected.itemType === KItemType.BASE_ITEM) {
                        super.addCells([cell, cell.baseItem]);
                    }
                    return;
                } else if (cell.itemType === KItemType.BASE_ITEM) {
                    if (cellsSelected.length > 0 && cellSelected.itemType !== cell.itemType) {
                        return;
                    }

                    super.addCells([cell, cell.baseItemLabel]);   
                } else if (cell.itemType === KItemType.BASE_ITEM_LABEL) {
                    if (cellsSelected.length > 0  && cellSelected.itemType !== cell.baseItem.itemType) {
                        return;
                    }

                    super.addCells([cell, cell.baseItem]); 
                } else {
                    super.addCells([cell]);    
                } 
            }
            
        }
    }

    //----Override setCells from mxGraphSelectionModel----
    setCells(cells) {
        let selectableCells = this.getSelectableCells(cells)
        if (selectableCells.length > 0) {
            cells = selectableCells
            if (cells != null) {
                if (this.singleSelection) {
                    cells = [this.getFirstSelectableCell(cells)]
                }

                let tmp = []
                for (let i = 0; i < cells.length; i ++) {
                    if (this.graph.isCellSelectable(cells[i])) {
                        tmp.push(cells[i])
                    }
                }

                let cellRemove = this.cells
                if (
                    this.cells.length > 0 && 
                    (this.cells[0].itemType == KItemType.DEFAULT_ITEM || this.cells[0].itemType == KItemType.BASE_ITEM || this.cells[0].itemType == KItemType.BASE_ITEM_LABEL) && 
                    tmp && tmp[0].itemType == KItemType.LINK_ITEM
                ) {
                    cellRemove = []
                    //[huyvq-cr_63347_fb#16]--- create edge handler in case line is selected while another items/ base items are still slected
                    const lineState = this.graph.view.getState(tmp[0])
                    this.graph.createEdgeHandler(lineState)
                }

                this.changeSelection(tmp, cellRemove)
            }
        }
    }

    removeCell(cell){
        if (cell) {
            if (cell.itemType === KItemType.BASE_ITEM) {
                const baseItemLabel = cell.baseItemLabel;
                this.removeCells([cell, baseItemLabel]);
            } else if (cell.itemType === KItemType.BASE_ITEM_LABEL) {
                const baseItem = cell.baseItem;
                this.removeCells([cell, baseItem]);
            } else {
                super.removeCell(cell);
            }
        }
    }

    getSelectableCells = (cells) => {
        let selectableCells = [];
        if (Array.isArray(cells)) {
            const cellsCount = cells.length;
            let addedCellIds = [];
            for (let i = 0; i < cellsCount; i ++) {
                let cell = cells[i];
                if (cell.isBaseItemLabel) {
                    cell = cell.baseItem;
                }

                if (this.cells.indexOf(cell) > -1 || (addedCellIds.length > 0 && addedCellIds.includes(cell.data.id))) {
                    continue;    
                }

                if (cell.data && 
                    cell.data.id && 
                    cell.data.id > 0 && 
                    cell.checkItemIsLocked() === false &&
                    this.graph.isCellLocked(cell) === false &&
                    this.graph.isItemLockByUser(cell) === false 
                ) {
                    addedCellIds.push(cell.data.id);
                    if (cell.itemType === KItemType.BASE_ITEM) {
                        selectableCells.push(cell.baseItemLabel);
                    }
                    selectableCells.push(cell);
                }
            }
        }

        return selectableCells;
    }
}
export default KSelectionModel;