let KItemStyle = {
  NONE  :-1,
  A   :0,
  B   :1,
  C   :2,
  D   :3,
  E   :4,
  F   :5,
  G   :6,
  H   :7,
  I   :8,
  J   :9,
  K   :10,
  B_A :11,
  B_B :12,
  B_C :13,
  B_D :14,
  B_E :15,
}

export default KItemStyle;