let KItemTextVerticalAlign = {
        TOP : 0,
        MIDDLE : 1,
        BOTTOM : 2,
        VALUE_TOP: 'top',
        VALUE_MIDDLE: 'center',
        VALUE_BOTTOM: 'bottom'
}
export default KItemTextVerticalAlign