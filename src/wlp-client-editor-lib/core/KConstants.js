
//horizontal align for base item and item
export const TEXT_HORIZONTAL_ALIGN = 'textHorizontalAlign';

//vertical align for base item
export const TEXT_VERTICAL_ALIGN = 'textVerticalAlign';

export const ITEM_STYLE = 'style';

export const ITEM_SHAPE = 'shape';

export const ITEM_TEXT_SIZE = 'textSize';

export const BASEITEM_STYLE = 'style';

export const BASEITEM_SHAPE = 'shape';

export const BASEITEM_TEXT_SIZE = 'textSize';

export const DEFAULT_ITEM_FONT_SIZE = 15;

export const DEFAULT_FONT_FAMILY = 'Meiryo';

export const ITEM_LINE_COLOR = 'color';

export const ITEM_LINE_TYPE = 'style';

export const ITEM_LINE_LARGE = 'large';

export const UTF8_BOM_FORMAT = '\uFEFF';

export const ADD_CLASS = 'addClass';

export const THREAD_DELAY = 50;

export const ITEM_LABEL_PADDING = 4;

export default {
    TEXT_HORIZONTAL_ALIGN,
    TEXT_VERTICAL_ALIGN,
    ITEM_STYLE,
    ITEM_SHAPE,
    ITEM_TEXT_SIZE,
    BASEITEM_STYLE,
    BASEITEM_SHAPE,
    BASEITEM_TEXT_SIZE,
    DEFAULT_ITEM_FONT_SIZE,
    DEFAULT_FONT_FAMILY,
    ITEM_LINE_COLOR,
    ITEM_LINE_TYPE,
    ITEM_LINE_LARGE,
    UTF8_BOM_FORMAT
}