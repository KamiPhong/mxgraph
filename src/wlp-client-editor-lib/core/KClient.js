import mxGraphFactory from 'mxgraph';
const { detect } = require('detect-browser');
const browser = detect();
const factory = new mxGraphFactory();

factory.mxClient.IS_SF = browser.name === 'safari';

export const { mxClient, mxEditor, mxDefaultToolbar, mxDefaultPopupMenu, mxDefaultKeyHandler, mxCellHighlight, mxCellMarker, mxCellTracker, mxConnectionHandler, mxConstraintHandler, mxEdgeHandler, mxEdgeSegmentHandler, mxElbowEdgeHandler, mxGraphHandler, mxHandle, mxKeyHandler, mxPanningHandler, mxPopupMenuHandler, mxRubberband, mxSelectionCellsHandler, mxTooltipHandler, mxVertexHandler, mxCodec, mxCodecRegistry, mxCircleLayout, mxCompactTreeLayout, mxCompositeLayout, mxEdgeLabelLayout, mxFastOrganicLayout, mxGraphLayout, mxParallelEdgeLayout, mxPartitionLayout, mxRadialTreeLayout, mxStackLayout, mxHierarchicalLayout, mxSwimlaneLayout, mxGraphAbstractHierarchyCell, mxGraphHierarchyEdge, mxGraphHierarchyModel, mxGraphHierarchyNode, mxSwimlaneModel, mxCoordinateAssignment, mxHierarchicalLayoutStage, mxMedianHybridCrossingReduction, mxMinimumCycleRemover, mxSwimlaneOrdering, mxCell, mxCellPath, mxGeometry, mxGraphModel, mxActor, mxArrow, mxArrowConnector, mxCloud, mxConnector, mxCylinder, mxDoubleEllipse, mxEllipse, mxHexagon, mxImageShape, mxLabel, mxLine, mxMarker, mxPolyline, mxRectangleShape, mxRhombus, mxShape, mxStencil, mxStencilRegistry, mxSwimlane, mxText, mxTriangle, mxAbstractCanvas2D, mxAnimation, mxAutoSaveManager, mxClipboard, mxConstants, mxDictionary, mxDivResizer, mxDragSource, mxEffects, mxEvent, mxEventObject, mxEventSource, mxForm, mxGuide, mxImage, mxImageBundle, mxImageExport, mxLog, mxMorphing, mxMouseEvent, mxObjectIdentity, mxPanningManager, mxPoint, mxPopupMenu, mxRectangle, mxResources, mxSvgCanvas2D, mxToolbar, mxUndoableEdit, mxUndoManager, mxUrlConverter, mxUtils, mxVmlCanvas2D, mxWindow, mxXmlCanvas2D, mxXmlRequest, mxCellEditor, mxCellOverlay, mxCellRenderer, mxCellState, mxCellStatePreview, mxConnectionConstraint, mxEdgeStyle, mxGraph, mxGraphSelectionModel, mxGraphView, mxLayoutManager, mxMultiplicity, mxOutline, mxPerimeter, mxPrintPreview, mxStyleRegistry, mxStylesheet, mxSwimlaneManager, mxTemporaryCellStates } = factory;

function checkBrowser(brw) {
    return browser && browser.name === brw;
}

const KClient = {
    IS_IE: checkBrowser('ie'),
    IS_EDGE: checkBrowser('edge'),
    IS_FIREFOX: checkBrowser('firefox'),
    IS_CHROME: checkBrowser('chrome'),
    TOUCH_MOVE: 'touchmove',
    TOUCH_START: 'touchstart',
    TOUCH_END: 'touchend'
}

export default  KClient;