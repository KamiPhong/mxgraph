import KItemLineArrowType from "./KItemLineArrowType";

let KGraphEdgeShape = {
    SHAPE_NAME_SQUARE: 'ksquarededge',
    SHAPE_NAME_TWO_WAY: 'ktwowayedge',
    SHAPE_NAME_TWO_WAY_REVERSE: 'ktwowayreverseedge',

    getString(int) {
        let shape;
        switch (int) {
            case KItemLineArrowType.SQUARE_NONE:
            case KItemLineArrowType.SQUARE_TO_FROM:
            case KItemLineArrowType.SQUARE_FROM_TO:
            case KItemLineArrowType.SQUARE_ONE_WAY_BIDIRECTIONAL:
                shape = this.SHAPE_NAME_SQUARE;
                break;
            case KItemLineArrowType.TWO_WAY:
                shape = this.SHAPE_NAME_TWO_WAY;
                break;
            case KItemLineArrowType.TWO_WAY_REVERSE:
                shape = this.SHAPE_NAME_TWO_WAY_REVERSE;
                break;
            default:
                shape = 'none';
                break;
        }
        return shape;
    },
};

export default KGraphEdgeShape;