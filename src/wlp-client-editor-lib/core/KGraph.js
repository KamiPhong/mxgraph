import { mxGraph, mxEvent, mxEventObject, mxConstants, mxRectangle, mxPoint, mxUtils, mxClient } from "wlp-client-editor-lib/core/KClient";
import KGraphHandler from "./KGraphHandler";
import KGraphView from "./KGraphView";
import KPanningHandler from "./KPanningHandler";
import KGraphZoomHandler from "./KGraphZoomHandler";
import KSelectionModel from './KSelectionModel';

import KItem from './KItem';
import KBaseItem from './KBaseItem';
import KLineItem from './KLineItem'
import KGraphEvent from "./KGraphEvent";

import KItemType from "./KItemType";
import Logger from "wlp-client-common/Logger.js";
import KCellEditor from "./KCellEditor";
import * as KGraphUtil from './KGraphUtil';
import EdgeContextMenuHandler from 'wlp-client-editor-lib/handlers/EdgeContextMenuHandler';
import ItemContextMenuHandler from 'wlp-client-editor-lib/handlers/ItemContextMenuHandler';

import RelationLineKind from 'wlp-client-service/consts/RelationLineKind';
import KPopupMenuHandler from "wlp-client-editor-lib/handlers/KPopupMenuHandler";
import KCellRenderer from "./views/KCellRenderer";
import unLikeIcon from 'wlp-client-editor-lib/assets/images/icon-flash-site/un-like.png';
import i18n from "i18n";
import KItemBase from "./KItemBase";
import {getCellSize} from '../utils/KUtils';
import {convertToHtmlEntities} from '../utils/Utils';
import { THREAD_DELAY } from "./KConstants";
import {createLoadingMouse} from 'wlp-client-service/service/request';
import { CONSTANTS } from 'wlp-client-editor-lib/utils/Constants';
import ActionLogger from "wlp-client-common/ActionLogger";
import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";

const localStorage = new LocalStorageUtils();
class KGraph extends mxGraph {
    constructor(container) {
        super(container);
        this.setPanning(true);

        this.graphZoomHandler = this.createGraphZoomHandler();
        this.isShiftDown = false;
        this.addWindowKeyListener();

        this.getSelectionModel().addListener(mxEvent.CHANGE, this.handleModelSelectionChanged);

        this.addListener(mxEvent.CELLS_MOVED, this.handleCellMoved);

        this.addListener(mxEvent.LABEL_CHANGED, this.handleCellLabelChanged);

        this.addListener(mxEvent.RESIZE_CELLS, this.handleResizeCells);

        this.addListener(mxEvent.CLICK, this.handleClick);

        this.addListener(mxEvent.EDITING_STARTED, this.handleStartEditItem);

        this.addListener(mxEvent.EDITING_STOPPED, this.handleStopEditItem);

        mxEvent.addListener(window, 'blur', this.handleWindowBlur);
        mxEvent.addListener(container, 'scroll', this.handleContainerScroll);

        mxEvent.addListener(container,'dragover', this.handleDrapOverImage);
        mxEvent.addListener(container, 'drop', this.handleDropImage);

        this.callbackDics = {};

        this.itemLineDataList = [];
        // default true
        this.isAllBaseItemLocked = true;
        
        this.savePngBySelected = false;

        this.isShowGoodButton = false;

        // hide/show printing range guide
        this.printingRangeGuideDisplay = false;

        this.itemParent = this.createParentForCells();
        this.baseItemParent = this.createParentForCells();
        this.baseItemLabelParent = this.createParentForCells();
        this.handleModelSelectionChangedThred = null;
        this.showCreatorLabelThred = null;

        this.addCells([this.baseItemParent, this.baseItemLabelParent, this.itemParent]);
        this.orderCells(true, [ this.baseItemParent, this.baseItemLabelParent, this.itemParent]);
        this.loadingMouse = null;
        this.loadingItemsLenght = 0;
        this.loadingItemsDelay = [];
        this.startEditingAtCellTheard = null;
        this.isSelectionProcess = false;
        this.sheetParentId = null;
        this.addListener(mxEvent.TAP_AND_HOLD, (sender, evt) => {
            if (evt.getProperty('cell')) {
                this.isSelectionProcess = true;
            }
        });
    }

    handleDropImage = (evt) => {
        console.log('drop ney');
    }

    handleDrapOverImage = (evt) => {
        console.log('drOver ney')
        if (this.isEnabled()) {
            evt.stopPropagation();
            evt.preventDefault();
        }
    }

    handleContainerScroll = (evt) => {
        let node = evt.target;
        while(node.parentNode){
            //disable container scroll
            node.scrollTop = 0;
            node.scrollLeft = 0;
            node = node.parentNode;
        }   
    }

    createParentForCells = () => {
        return this.getDefaultParent().clone();
    }

    handleResizeCells = (sender, evt) => {
        let cells = evt.getProperty("cells");
        if (cells.length > 0) {
            for (var i = 0; i < cells.length; i++) {
                let cell = cells[i];
                if (cell.itemType === KItemType.BASE_ITEM) {
                    cell.updateModelSizeFromCell()
                    this.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, [cell.data]);
                } else if (cell.itemType === KItemType.BASE_ITEM_LABEL) {
                    let baseItem = cell.baseItem;
                    baseItem.updateModelLabelSizeFromCell()
                    this.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, [baseItem.data]);
                }
            }
        }
    }
    
    handleClick = (sender, evt) => {
        let cell = evt.getProperty("cell");
        if (this.graphHandler.edgeCreating) {
            this.graphHandler.edgeCreating = false;
        } else if (cell != null && this.model.isVertex(cell)) {
            if (cell.itemType === KItemType.ATTACH_ITEM && cell.isAttachItemImage === true) {
                this.fireGraphEventDownLoadFile(cell)
            } else if (cell.isAttachFileBtnDownload) {
                this.fireGraphEventDownLoadFile(cell.parent)
            } else if (cell.isGoodButton === true) {
                this.itemGoodButtonClickHandler(cell)
            } else if (cell.isIconEditLabel === true) {
                this.itemIconEditLabelClickHandler(cell);
            } else if (cell.itemType === KItemType.DEFAULT_ITEM) {
                const event = evt.getProperty("event");
                const target = event.target;
                if (target.getAttribute('href') !== null && target.getAttribute('href') === "event:itemLinkTextClick") {
                    window.open(target.innerText, '_blank');
                }
            }
        } 
    }


    fireGraphEventDownLoadFile = (cell) => {
        this.fireGraphEvent(KGraphEvent.ITEM_ATTACH_FILE_CLICK, cell);
    }

    itemGoodButtonClickHandler = (cell) => {
        let item = cell.parent;
        let itemModel = item.data;
        this.fireGraphEvent(KGraphEvent.ITEM_LIKE, itemModel);
    }

    addGraphEventListener = (eventName, callback) => {
        this.callbackDics[eventName] = callback;
    }

    handleWindowBlur = (evt) => {
        //stop editing on change browser window
        this.stopEditing(false);
    }

    fireGraphEvent = (eventName, ...agrs) => {
        const callback = this.callbackDics[eventName];
        if (callback && typeof (callback) === 'function') {
            callback(...agrs);
        }
    }

    /**
     * handle cell selected changed
     */
    handleModelSelectionChanged = (sender, evt) => {
        let removedCells = [];
        let addedCells = [];
        let isBaseitem = false;
        if (typeof evt.getProperty('added') !== "undefined") {
            removedCells = evt.getProperty('added');
            if (removedCells.length > 0 && 
                (removedCells[0].itemType === KItemType.BASE_ITEM || removedCells[0].itemType === KItemType.BASE_ITEM_LABEL)) {
                isBaseitem = true;
            }
        }

        if (typeof evt.getProperty('removed') !== "undefined") {
            addedCells = evt.getProperty('removed');
            if (addedCells.length > 0 && 
                (addedCells[0].itemType === KItemType.BASE_ITEM || addedCells[0].itemType === KItemType.BASE_ITEM_LABEL)) {
                isBaseitem = true;
            }
        }

        let totalItem = isBaseitem ? (addedCells.length + removedCells.length)/2 : addedCells.length + removedCells.length;
        if (totalItem > CONSTANTS.MAX_ITEMS_LENGTH_SOCKET_HANDLER) {
            this.loadingMouse = createLoadingMouse();
            this.loadingMouse.show();
            this.loadingItemsLenght = totalItem;
        }
    
        this.handleSelectionRemoved(removedCells);
        this.handleSelectionAdded(addedCells);
    };

    handleSelectionRemoved = (removedCells) => {
        if (removedCells && removedCells.length > 0) {
            let unselectedItemModels = [];
            let unselectedBaseItemModels = [];
            let unselectedItemLineModels = [];
            let unselectedBaseItemLabels = [];

            for (let i = 0; i < removedCells.length; i++) {
                let cell = removedCells[i];
                if (cell.isBaseItemLabel) {
                    unselectedBaseItemLabels.push(cell)
                    // cell = cell.baseItem;
                }
 
                //only cell which has model id should be un-locked
                if (cell.data && cell.data.id && cell.data.id > 0 && cell.isDeleting === false && this.getModel().getCell(cell.id)) {
                    if (cell.itemType === KItemType.DEFAULT_ITEM) {
                        unselectedItemModels.push(cell.data);
                        cell.setVisibleIconEditLabel(false);
                    } else if (cell.itemType === KItemType.BASE_ITEM) {
                        unselectedBaseItemModels.push(cell.data);
                        cell.setVisibleIconEditLabel(false);
                        cell.baseItemLabel.setVisibleIconResize(false);
                    } else if (cell.itemType === KItemType.LINK_ITEM) {
                        unselectedItemLineModels.push(cell.data);
                        cell.setVisibleIconEditLabel(false);
                    }
                    
                }

                //un-show creator label of cell
                // trong bug 58188: delay một chút để xem user có di chuyển item ngay khi select cell hay không
                // vì hiển thị cellCreator bị setTimout, nên chỗ này ẩn cellCreator chạy trước, nên setTimout ở đây để đảm bảo chạy sau khi cellCreator đã hiển thị xong
                // @TODO: cần loại bỏ setTimout trong tương lai
                setTimeout(() => {
                    this.setCellCreatorVisible(cell, false);
                }, THREAD_DELAY);
                
            }

            let isRemovingSelection = localStorage.getRemoveSelectionState()
            
            if (unselectedItemModels.length > 0 && isRemovingSelection != 'true') {
                this.fireGraphEvent(KGraphEvent.ITEM_UNLOCKED, unselectedItemModels);   
            }

            if (unselectedBaseItemModels.length > 0 && isRemovingSelection != 'true') {
                this.fireGraphEvent(KGraphEvent.BASE_ITEM_UNLOCKED, unselectedBaseItemModels);
            }

            if (unselectedItemLineModels.length > 0 && isRemovingSelection != 'true') {
                this.fireGraphEvent(KGraphEvent.ITEM_LINE_UNLOCKED, unselectedItemLineModels[0]);
            }
            localStorage.setRemoveSelectionState('false')
        }
    }

    handleSelectionAdded = (addedCells) => {
        let selectionCells = this.getSelectionCells();

        if (typeof addedCells !== "undefined" && addedCells && addedCells.length > 0) {
            let selectedItemModels = [];
            let selectedBaseItemModels = [];
            let selectedItemLineModels = [];
  
            for (let i = 0; i < addedCells.length; i++) {
                let cell = addedCells[i];
                //only cell which has model id should be locked
                if (cell.data && cell.data.id && cell.data.id > 0 && cell.isDeleting === false) {
                    if (
                        cell.itemType === KItemType.DEFAULT_ITEM &&
                        selectedItemModels.indexOf(cell.data) === -1
                    ) {
                        selectedItemModels.push(cell.data);
                        cell.setVisibleIconEditLabel(true);
                    } else if (
                        cell.itemType === KItemType.BASE_ITEM &&
                        selectedBaseItemModels.indexOf(cell.data) === -1
                    ) {
                        selectedBaseItemModels.push(cell.data);
                        cell.setVisibleIconEditLabel(true);
                        cell.baseItemLabel.setVisibleIconResize(true);
                    } else if (
                        cell.itemType === KItemType.LINK_ITEM &&
                        selectedItemLineModels.indexOf(cell.data) === -1
                    ) {
                        selectedItemLineModels.push(cell.data);
                        cell.setVisibleIconEditLabel(true);
                    } 
                }
            }

            this.sendCellsToFront(addedCells);
            
            if (selectedItemModels.length > 0) {
                //TODO: fire item selected call back
                this.fireGraphEvent(KGraphEvent.ITEM_LOCKED, selectedItemModels);
            }
            
            if (selectedItemLineModels.length > 0) {
                
                this.fireGraphEvent(KGraphEvent.ITEM_LINE_LOCKED, selectedItemLineModels[0]);
            }

            if (selectedBaseItemModels.length > 0) {
                this.fireGraphEvent(KGraphEvent.BASE_ITEM_LOCKED, selectedBaseItemModels);
            }

            if (this.showCreatorLabelThred) {
                clearTimeout(this.showCreatorLabelThred);
            }

            //[ThuyTV]: delay một chút để xem user có di chuyển item ngay khi select cell hay không
            //Trong trường hợp user di chuyển item ngay sau khi chọn thì không cần hiển thị creatorLabel
            this.showCreatorLabelThred = setTimeout(() => {
                this.graphHandler.addEditTextBtnToStates(selectionCells);
                if (KGraphUtil.isSelectedSingleBaseItem(selectionCells)) {
                    if (!this.graphHandler.isMovingCells || 
                        // moving only base item label => still show cell creator of baseitem 
                        (this.graphHandler.isMovingCells && this.graphHandler.cells && this.graphHandler.cells[0].itemType === KItemType.BASE_ITEM_LABEL)) {
                        for (let i = 0; i < selectionCells.length; i ++) {
                            this.setCellCreatorVisible(selectionCells[i], true);
                        }
                    }
                } else if (KGraphUtil.isSelectedSingleItem(selectionCells)) {
                    if (!this.graphHandler.isMovingCells) {
                        this.setCellCreatorVisible(selectionCells[0], true);
                    }
                } else {
                    for (let i = 0; i < selectionCells.length; i ++) {
                        this.setCellCreatorVisible(selectionCells[i], false);
                    }
                }
            }, THREAD_DELAY);
        }        
    }

    /**
     * handle cell moved
     */
    handleCellMoved = (sender, evt) => {
        ActionLogger.click('User_Editor_SheetLinkView', 'Move_Item_On_Sheet');
        if (!this.isMouseDown) {
            let cells = evt.getProperty("cells");
            let movedItemModels = [];
            let movedItemBaseModels = [];
            if (cells && cells.length > 0) {
                cells.forEach((cell) => {
                    // Logger.logConsole("cell moved", cell)
                    //only item and base item has data
                    if (cell.data && cell.isDeleting === false) {
                        if (cell.itemType === KItemType.DEFAULT_ITEM) {
                            cell.updateModelLocationFromCell();
                            movedItemModels.push(cell.data);
                        }
                        if (cell.itemType === KItemType.BASE_ITEM) {
                            cell.updateModelLocationFromCell();
                            movedItemBaseModels.push(cell.data);
                        }
                    }
                    // TODO: NAMNH label di chuyển update base item
                    if (cell.isBaseItemLabel) {
                        // Logger.logConsole("cell.parent", cell.parent)
                        let baseItem = cell.baseItem;
             
                        // baseItem.insert(cell);
                        baseItem.updateModelLocationFromCellLabel();
                        movedItemBaseModels.push(baseItem.data);
                    }
                });
            }
            if (movedItemModels.length > 0) {
                this.fireGraphEvent(KGraphEvent.ITEM_UPDATED, movedItemModels, mxEvent.CELLS_MOVED);
            }
            if (movedItemBaseModels.length > 0) {
                this.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, movedItemBaseModels, mxEvent.CELLS_MOVED);
            }
        }
    }


    /**
     * handle cell label chaned
     */
    handleCellLabelChanged = (sender, evt) => {
        let cell = evt.getProperty('cell');
        if (cell) {
            if (cell.isBaseItemLabel) {
                let baseItem = cell.baseItem;
                baseItem.updateModelTextFromCellLabel();
                this.fireGraphEvent(KGraphEvent.BASE_ITEM_UPDATED, [baseItem.data], mxEvent.LABEL_CHANGED);
            }

            if (cell.data) {
                if (cell.data && cell.itemType === KItemType.DEFAULT_ITEM) {
                    cell.updateModelTextFromCell();
                    this.fireGraphEvent(KGraphEvent.ITEM_UPDATED, [cell.data], mxEvent.LABEL_CHANGED);
                    this.autoCellSize(cell);
                }
                // item line update label
                if (cell.itemType === KItemType.LINK_ITEM) {
                    cell.updateModelTextFromCell();
                    this.fireGraphEvent(KGraphEvent.ITEM_LINE_UPDATED, cell.data, mxEvent.LABEL_CHANGED);
                }
            }

            this.getModel().beginUpdate();
            try {
                if (cell) {
                    this.getModel().setValue(cell, mxUtils.htmlEntities(cell.value, false));

                    if (cell.itemType === KItemType.DEFAULT_ITEM) {
                        this.autoCellSize(cell);
                    } else if (cell.itemType === KItemType.BASE_ITEM_LABEL) {
                        let baseItem = cell.baseItem;
                        this.getModel().setValue(baseItem, mxUtils.htmlEntities(cell.value, false));
                    }
                }   

            } finally {
                this.getModel().endUpdate();
            }
        }
    }

    createGraphZoomHandler() {
        return new KGraphZoomHandler(this);
    }

    createGraphHandler() {
        return new KGraphHandler(this);
    }

    createGraphView() {
        return new KGraphView(this);
    }

    createPanningHandler() {
        return new KPanningHandler(this);
    }

    createVertexHandler(state) {
        return new ItemContextMenuHandler(state);
    }

    createEdgeHandler(state, edgeStyle) {
        return new EdgeContextMenuHandler(state)
    }

    createCellEditor() {
        return new KCellEditor(this);
    }

    createPopupMenuHandler(){
        return new KPopupMenuHandler(this);
    }

    createCellRenderer(){
        return new KCellRenderer();
    }

    addWindowKeyListener = () => {
        mxEvent.addListener(window, 'keydown', this.windowKeyDownHanlder);
        mxEvent.addListener(window, 'keyup', this.windowKeyUpHandler);
    }

    removeWindowKeyListener = () => {
        mxEvent.removeListener(window, 'keydown', this.windowKeyDownHanlder);
        mxEvent.removeListener(window, 'keyup', this.windowKeyUpHandler);
    }

    /**
     * [ThuyTV] override mxGraph function 
     */

    panGraph(dx, dy){
        super.panGraph(dx, dy);
    }

    /**
     * @author ThuyTV
     * @description listen for windown keydown events 
     * @param {KeyboardEvent} evt 
     */
    windowKeyDownHanlder = (evt) => {
        this.isShiftDown = evt.shiftKey;
    }

    /**
     * @author ThuyTV
     * @description listen for windown keyup events 
     * @param {KeyboardEvent} evt 
     */
    windowKeyUpHandler = (evt) => {
        this.isShiftDown = evt.shiftKey;
    }

    /**
     * <p>ItemModelからIItemインスタンスを取得する。</p>
     * @param model 対象のItemModel
     * @return      IItemインスタンス
     */
    getCellByModelAndType = (model, itemType) => {
        if (model) {
            let cell = null;
            //first get cell by model id
            if (model.id > 0) {
                cell = this.getCellByCellIdAndType(model.id, itemType);
                // cell = this.getModel().getCell(model.id);
                // if (!cell || !cell.data || cell.itemType !== itemType) {
                //     cell = null;
                // }
            }
            //if cell still null, try to get cell by uid
            if (cell == null) {
                if (model.uid != null) {
                    cell = this.getModel().getCell(model.uid);
                    if (!cell || !cell.data || cell.itemType !== itemType) {
                        cell = null;
                    }
                }
            }
            return cell;
        }
        return null;
    }

    memberLeaveUnlockItems = (clientId) => {
        const cells = this.model.cells
        let cellsSelectedByLeftMember = []
        let cellsSelectedByLeftMemberModels 
        for (const cellId in cells) {
            if(cells[cellId].data && cells[cellId].data.editClientId === clientId) {
                cells[cellId].data.editClientId = null
                cells[cellId].data.editing = false
                cells[cellId].data.editStartTime = null
                cells[cellId].data.editUserName = null
                cellsSelectedByLeftMember.push(cells[cellId])
                }
            }

        this.setCellsEditingState(cellsSelectedByLeftMember)
    }

    getCellByCellIdAndType = (id, itemType) => {
        let cell = null;
        cell = this.getModel().getCell(id);
        if (!cell || !cell.data || cell.itemType !== itemType) {
            cell = null;
        }
        return cell;
    }

    getListCellByModelsAndType = (modelList, itemType) => {
        let cellList = [];
        if (Array.isArray(modelList)) {
            let modelCount = modelList.length;
            for (let i = 0; i < modelCount; i++) {
                let model = modelList[i];
                let cell = this.getCellByModelAndType(model, itemType);
                if (cell) {
                    cellList.push(cell);
                }
            }
        }

        return cellList;
    }
    
    autoCellSize = (cell) => {
        this.getModel().beginUpdate();
        try {
            let geo = cell.geometry.clone();
            
            //udpate size for KItem
            if (cell.itemType === KItemType.DEFAULT_ITEM) {
                if ( cell.value ) {
                    const value = convertToHtmlEntities(cell.value);
                    let size = getCellSize(this, cell, value);

                    const style = this.getCellStyle(cell);

                    //[ThuyTv]: change cell vertical align to top if height is larger than default height
                    //otherwise, cell vertical align must be middle
                    if (size.height > KGraphUtil.DEFAULT_ITEM_HEIGHT) {
                        if (style[mxConstants.STYLE_VERTICAL_ALIGN] !== mxConstants.ALIGN_TOP) {
                            this.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP, [cell]);
                        }
                    } else if (style[mxConstants.STYLE_VERTICAL_ALIGN] !== mxConstants.ALIGN_MIDDLE) {
                        this.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE, [cell]);
                    }
                    geo.width = size.width;
                    geo.height = size.height;
                } else {
                    geo.width = KGraphUtil.DEFAULT_ITEM_WIDTH
                    geo.height = KGraphUtil.DEFAULT_ITEM_HEIGHT
                }
            }

            cell.setGeometry(geo);
           
        } finally {
            this.getModel().endUpdate();
        }

        //update size for KBaseItem
    }

    setCellsEditingState = (cells) => {
        if (Array.isArray(cells)) {
            if (this.loadingMouse && this.loadingItemsLenght > 0) {
                this.loadingItemsDelay = this.loadingItemsDelay.concat(cells);
                if (this.loadingItemsDelay.length < this.loadingItemsLenght) {
                    return;
                }
                cells = this.loadingItemsDelay;
            }
            this.getModel().beginUpdate();
            try {
                const cellsCount = cells.length;
                let cellsLabels = [];
                for (let i = 0; i < cellsCount; i++) {
                    let cell = cells[i];
                    let model = cell.data;
                    cellsLabels.push(cell.editingLabel);
                    
                    cell.setEdingLabelValue(model.editUserName);
                    cell.setVisibleEditingLabel(model.editing);
                    cell.setDataLikedLockState();
                    cell.setGoodButtonValue();
                    if (cell.itemType === KItemType.DEFAULT_ITEM && cell.goodbutton) {
                        let goodButton = cell.goodbutton;
                        let geo = goodButton.geometry.clone();
                        let offsetBottom = model.editing ? KGraphUtil.EDITING_USER_LABEL_OFFSET_BOTTOM*2 - KGraphUtil.GOOD_BUTTON_MARGIN : KGraphUtil.EDITING_USER_LABEL_OFFSET_BOTTOM - KGraphUtil.GOOD_BUTTON_MARGIN

                        geo.width = cell.getGoodButtonWidth();
                        geo.offset = new mxPoint(0, offsetBottom);
                        goodButton.setGeometry(geo);
                        
                        cellsLabels.push(goodButton);
                    }
                }
                this.sendCellsToFront(cellsLabels);
            }
            finally {
                this.getModel().endUpdate();
                this.graphHandler.addEditingLabelToStates(cells);
                if (this.loadingMouse !== null) {
                    this.loadingMouse.hide();
                    this.loadingMouse = null;
                    this.loadingItemsDelay = [];
                    this.loadingItemsLenght = 0;
                }
            }
        }
    }

    setCellCreatorVisible = (cell, visible) => {
        if (
            cell instanceof KItemBase && 
            this.isCellVisible(cell.creatorLabel) !== visible
        ) {
            this.getModel().beginUpdate();
            try {
                cell.setVisibleCreatorLabel(visible);
                this.sendCellsToFront([cell.creatorLabel]);
            }
            finally {
                this.getModel().endUpdate();
                if (visible) {
                    this.graphHandler.addCreatorLabelToStates([cell]);
                }
            }
        }
    }



    /**
     * add items model to graph
     */
    addItems = (itemModels) => {
        let existedItems = [];
        let newItems = [];
        let cellsInEditing = [];
        this.getModel().beginUpdate();
        try {
            let graphModels = this.getModel();
            for (let index in itemModels) {
                let itemModel = itemModels[index];

                if (graphModels[itemModel.id]) {
                    //item is existed
                    existedItems.push(itemModel);
                    continue;
                }
                //render new cells
                let parent = this.itemParent;

                let cell = new KItem(itemModel);
                this.addCell(cell, parent);
                this.autoCellSize(cell);

                cell.setModel(itemModel);
                cell.setHasRole(this.sheetParentId)

                if (this.isShowGoodButton) {
                    cell.goodbutton.visible = true;
                }

                newItems.push(cell);

                if (itemModel.id > 0 && itemModel.editing) {
                    cellsInEditing.push(cell);
                }
            }

            if(newItems.length > 0){
                //normal item allways on top of other items
                // this.sendCellsToFront(newItems);
            }

        } finally {
            this.getModel().endUpdate();
            for (let i = 0; i < newItems.length; i ++) {
                this.autoCellSize(newItems[i]);
            }
        }
        if (existedItems.length > 0) {
            this.updateItems(existedItems);
        }

        if (cellsInEditing.length > 0) {
            this.setCellsEditingState(cellsInEditing);
        }

        return newItems;
    }
    /////////////////////////////////////
    //Override mxGraph fireMouseEvent
    ////////////////////////////////////
    fireMouseEvent(evtName, me, sender) {
        if (this.isEventSourceIgnored(evtName, me)) {
            if (this.tooltipHandler != null) {
                this.tooltipHandler.hide();
            }
            
            return;
        }
        
        if (sender == null) {
            sender = this;
        }

        // Updates the graph coordinates in the event
        me = this.updateMouseEvent(me, evtName);
        // Updates the graph coordinates to sender
        if (me.graphX !== null && me.graphY !== null) {
            sender.graphX = me.graphX;
            sender.graphY = me.graphY;
        }

        ////////////////////////////
        ////START
        ///////////////////////////
        if (this.isEventSourceIgnored(evtName, me))
        {
            if (this.tooltipHandler != null)
            {
                this.tooltipHandler.hide();
            }
            
            return;
        }
        
        if (sender == null)
        {
            sender = this;
        }

        // Updates the graph coordinates in the event
        me = this.updateMouseEvent(me, evtName);

        // Detects and processes double taps for touch-based devices which do not have native double click events
        // or where detection of double click is not always possible (quirks, IE10+). Note that this can only handle
        // double clicks on cells because the sequence of events in IE prevents detection on the background, it fires
        // two mouse ups, one of which without a cell but no mousedown for the second click which means we cannot
        // detect which mouseup(s) are part of the first click, ie we do not know when the first click ends.
        if ((!this.nativeDblClickEnabled && !mxEvent.isPopupTrigger(me.getEvent())) || (this.doubleTapEnabled &&
            mxClient.IS_TOUCH && (mxEvent.isTouchEvent(me.getEvent()) || mxEvent.isPenEvent(me.getEvent()))))
        {
            var currentTime = new Date().getTime();
            
            // NOTE: Second mouseDown for double click missing in quirks mode
            if ((!mxClient.IS_QUIRKS && evtName == mxEvent.MOUSE_DOWN) || (mxClient.IS_QUIRKS && evtName == mxEvent.MOUSE_UP && !this.fireDoubleClick))
            {
                if (this.lastTouchEvent != null && this.lastTouchEvent != me.getEvent() &&
                    currentTime - this.lastTouchTime < this.doubleTapTimeout &&
                    Math.abs(this.lastTouchX - me.getX()) < this.doubleTapTolerance &&
                    Math.abs(this.lastTouchY - me.getY()) < this.doubleTapTolerance &&
                    this.doubleClickCounter < 2)
                {
                    this.doubleClickCounter++;
                    var doubleClickFired = false;
                    
                    if (evtName == mxEvent.MOUSE_UP)
                    {
                        if (me.getCell() == this.lastTouchCell && this.lastTouchCell != null)
                        {
                            this.lastTouchTime = 0;
                            var cell = this.lastTouchCell;
                            this.lastTouchCell = null;

                            // Fires native dblclick event via event source
                            // NOTE: This fires two double click events on edges in quirks mode. While
                            // trying to fix this, we realized that nativeDoubleClick can be disabled for
                            // quirks and IE10+ (or we didn't find the case mentioned above where it
                            // would not work), ie. all double clicks seem to be working without this.
                            if (mxClient.IS_QUIRKS)
                            {
                                me.getSource().fireEvent('ondblclick');
                            }
                            
                            this.dblClick(me.getEvent(), cell);
                            doubleClickFired = true;
                        }
                    }
                    else
                    {
                        this.fireDoubleClick = true;
                        this.lastTouchTime = 0;
                    }

                    // Do not ignore mouse up in quirks in this case
                    if (!mxClient.IS_QUIRKS || doubleClickFired)
                    {
                        mxEvent.consume(me.getEvent());
                        return;
                    }
                }
                else if (this.lastTouchEvent == null || this.lastTouchEvent != me.getEvent())
                {
                    this.lastTouchCell = me.getCell();
                    this.lastTouchX = me.getX();
                    this.lastTouchY = me.getY();
                    this.lastTouchTime = currentTime;
                    this.lastTouchEvent = me.getEvent();
                    this.doubleClickCounter = 0;
                }
            }
            else if ((this.isMouseDown || evtName == mxEvent.MOUSE_UP) && this.fireDoubleClick)
            {
                this.fireDoubleClick = false;
                var cell = this.lastTouchCell;
                this.lastTouchCell = null;
                this.isMouseDown = false;
                
                // Workaround for Chrome/Safari not firing native double click events for double touch on background
                var valid = (cell != null) || ((mxEvent.isTouchEvent(me.getEvent()) || mxEvent.isPenEvent(me.getEvent())) &&
                    (mxClient.IS_GC || mxClient.IS_SF));
                
                if (valid && Math.abs(this.lastTouchX - me.getX()) < this.doubleTapTolerance &&
                    Math.abs(this.lastTouchY - me.getY()) < this.doubleTapTolerance)
                {
                    this.dblClick(me.getEvent(), cell);
                }
                else
                {
                    mxEvent.consume(me.getEvent());
                }
                
                return;
            }
        }

        if (!this.isEventIgnored(evtName, me, sender))
        {
            // Updates the event state via getEventState
            me.state = this.getEventState(me.getState());
            this.fireEvent(new mxEventObject(mxEvent.FIRE_MOUSE_EVENT, 'eventName', evtName, 'event', me));


            //////////////////////////////////////////////////////////
            // Stops editing for all events other than from cellEditor
            //[huyvq]---Move stops editing before mouse down
            //////////////////////////////////////////////////////////
            if (evtName == mxEvent.MOUSE_DOWN && this.isEditing() && !this.cellEditor.isEventSource(me.getEvent()))
            {
                this.stopEditing(!this.isInvokesStopCellEditing());
            }
            //=============================
            
            if ((mxClient.IS_OP || mxClient.IS_SF || mxClient.IS_GC || mxClient.IS_IE11 ||
                (mxClient.IS_IE && mxClient.IS_SVG) || me.getEvent().target != this.container))
            {
                if (evtName == mxEvent.MOUSE_MOVE && this.isMouseDown && this.autoScroll && !mxEvent.isMultiTouchEvent(me.getEvent))
                {
                    this.scrollPointToVisible(me.getGraphX(), me.getGraphY(), this.autoExtend);
                }
                else if (evtName == mxEvent.MOUSE_UP && this.ignoreScrollbars && this.translateToScrollPosition &&
                        (this.container.scrollLeft != 0 || this.container.scrollTop != 0))
                {
                    var s = this.view.scale;
                    var tr = this.view.translate;
                    this.view.setTranslate(tr.x - this.container.scrollLeft / s, tr.y - this.container.scrollTop / s);
                    this.container.scrollLeft = 0;
                    this.container.scrollTop = 0;
                }
                
                if (this.mouseListeners != null)
                {
                    var args = [sender, me];
        
                    // Does not change returnValue in Opera
                    if (!me.getEvent().preventDefault)
                    {
                        me.getEvent().returnValue = true;
                    }
                    
                    for (var i = 0; i < this.mouseListeners.length; i++)
                    {
                        var l = this.mouseListeners[i];
                        
                        if (evtName == mxEvent.MOUSE_DOWN)
                        {
                            l.mouseDown.apply(l, args);
                        }
                        else if (evtName == mxEvent.MOUSE_MOVE)
                        {
                            l.mouseMove.apply(l, args);
                        }
                        else if (evtName == mxEvent.MOUSE_UP)
                        {
                            l.mouseUp.apply(l, args);
                        }
                    }
                }
                
                // Invokes the click handler
                if (evtName == mxEvent.MOUSE_UP)
                {
                    this.click(me);
                }
            }
            
            // Detects tapAndHold events using a timer
            if ((mxEvent.isTouchEvent(me.getEvent()) || mxEvent.isPenEvent(me.getEvent())) &&
                evtName == mxEvent.MOUSE_DOWN && this.tapAndHoldEnabled && !this.tapAndHoldInProgress)
            {
                this.tapAndHoldInProgress = true;
                this.initialTouchX = me.getGraphX();
                this.initialTouchY = me.getGraphY();
                
                var handler = function()
                {
                    if (this.tapAndHoldValid)
                    {
                        this.tapAndHold(me);
                    }
                    
                    this.tapAndHoldInProgress = false;
                    this.tapAndHoldValid = false;
                };
                
                if (this.tapAndHoldThread)
                {
                    window.clearTimeout(this.tapAndHoldThread);
                }
        
                this.tapAndHoldThread = window.setTimeout(mxUtils.bind(this, handler), this.tapAndHoldDelay);
                this.tapAndHoldValid = true;
            }
            else if (evtName == mxEvent.MOUSE_UP)
            {
                this.tapAndHoldInProgress = false;
                this.tapAndHoldValid = false;
            }
            else if (this.tapAndHoldValid)
            {
                this.tapAndHoldValid =
                    Math.abs(this.initialTouchX - me.getGraphX()) < this.tolerance &&
                    Math.abs(this.initialTouchY - me.getGraphY()) < this.tolerance;
            }

            // // Stops editing for all events other than from cellEditor
            // if (evtName == mxEvent.MOUSE_DOWN && this.isEditing() && !this.cellEditor.isEventSource(me.getEvent()))
            // {
            //     this.stopEditing(!this.isInvokesStopCellEditing());
            // }

            this.consumeMouseEvent(evtName, me, sender);
        }

        ////////////////////////////
        ////STOP
        ////////////////////////////
    }
    updateItems = (itemModels) => {
        this.getModel().beginUpdate();
        try {
            for (let index in itemModels) {
                let itemModel = itemModels[index];

                let cell = this.getCellByModelAndType(itemModel, KItemType.DEFAULT_ITEM);

                //this cell has no id
                if (cell) {
                    if (cell.data && cell.data.id <= 0) {
                        this.getModel().cells[itemModel.id] = this.getModel().cells[cell.data.uid]; // Assign new key 
                        delete this.getModel().cells[cell.data.uid]; // Delete old key
                        cell.setId(itemModel.id);
                    }
                    cell.setModel(itemModel);
                    cell.setHasRole(this.sheetParentId)
                    
                    // update attach file
                    if (cell.data.attachFile && cell.itemAttachFile === null) {
                        cell.createAttachFileCell();
                    }

                    //update cell geometry
                    let geo = cell.geometry.clone();
                    geo.x = itemModel.x;
                    geo.y = itemModel.y;
                    this.getModel().setGeometry(cell, geo);
                    //update cell style
                    this.getModel().setStyle(cell, KGraphUtil.getItemModelStyle(itemModel));
                    //update cell value
                    this.getModel().setValue(cell, itemModel.text);

                    cell.setIsNew();
                    
                }
            }
        } finally {
            this.getModel().endUpdate();
            
            for (let i = 0; i < itemModels.length; i ++) {
                let cell = this.getCellByModelAndType(itemModels[i], KItemType.DEFAULT_ITEM);
                this.autoCellSize(cell);
            }
        }
    }

    /**
    * only update cell data
    */
    updateItemsModelByType = (itemModels, itemType, isNotUpdateValue = false) => {
        this.getModel().beginUpdate();
        try {
            for (let index in itemModels) {
                let itemModel = itemModels[index];
                if (itemType === KItemType.BASE_ITEM) {
                    itemModel = this.replaceBaseItemText(itemModel)
                }
                let cell = this.getCellByModelAndType(itemModel, itemType);
                if (cell) {
                    cell.setModel(itemModel);

                    // must update item text from server
                    if (itemType === KItemType.BASE_ITEM && isNotUpdateValue === false) {
                        let baseitemLabel = cell.baseItemLabel;
                        this.getModel().setValue(baseitemLabel, itemModel.text);
                        this.getModel().setValue(cell, itemModel.text);
                        // this.autoCellSize(baseitemLabel);
                    } else if (itemType === KItemType.DEFAULT_ITEM && isNotUpdateValue === false) {
                        this.getModel().setValue(cell, itemModel.text);
                        this.autoCellSize(cell);
                        cell.setIsNew();
                    } else if (itemType === KItemType.LINK_ITEM) {
                        this.getModel().setValue(cell, mxUtils.htmlEntities(itemModel.text, false));
                    }
                }   
            }
        } finally {
            this.getModel().endUpdate();
        }
    }

    deleteItemsByIds = (ids) => {
        this.getModel().beginUpdate();
        try {
            let graphModels = this.getModel();
            let cells = [];

            for (let id of ids) {
                const cell = graphModels.cells[id];
                if (typeof cell !== 'undefined' && cell) {
                    cells.push(cell);
                    if (cell.itemType === KItemType.BASE_ITEM && typeof cell.baseItemLabel !== 'undefined') {
                        cells.push(cell.baseItemLabel);
                    }
                }
            }
            this.removeCells(cells);
        } finally {
            this.getModel().endUpdate();
        }
    }

    replaceBaseItemText = (baseItemModel) => {
        baseItemModel.text = baseItemModel.text.replace('href="event:itemLinkTextClick"', 'href="event:itemLinkTextClick" class="baseItemLink"');
        return baseItemModel;
    }

    /**
     * add base item to cell
     */
    addBaseItems = (baseItemModels) => {
        let existedItems = [];
        let newBaseItems = [];
        let cellsInEditing = [];
        let baseItemLabels = [];
        this.getModel().beginUpdate();
        try {
            let graphModels = this.getModel();
            const parent = this.baseItemParent;
            const parentBaseItemLabel = this.baseItemLabelParent;
            for (let index in baseItemModels) {
                let baseItemModel = baseItemModels[index];
                baseItemModel = this.replaceBaseItemText(baseItemModel)
                if (graphModels[baseItemModel.id]) {
                    existedItems.push(baseItemModel);
                    continue;
                }
                
                let cell = new KBaseItem(baseItemModel);
                cell.setModel(baseItemModel);
                cell.setHasRole(this.sheetParentId)
                this.addCell(cell, parent);

                // add label base item
                newBaseItems.push(cell);
                baseItemLabels.push(cell.getChildrenLabel());
                this.addCell(cell.getChildrenLabel(), parentBaseItemLabel);
                if (baseItemModel.id > 0 && baseItemModel.editing) {
                    cellsInEditing.push(cell);
                }
            }
            
            if (baseItemLabels.length > 0) {
                //[ThuyTv]: baseitem allways in the back of other items
                this.sendCellsToBack(baseItemLabels);
            }
        } finally {
            this.getModel().endUpdate();
        }

        if (existedItems.length > 0) {
            this.updateBaseItems(existedItems);
        }

        if (cellsInEditing.length > 0) {
            this.setCellsEditingState(cellsInEditing);
        }

        return newBaseItems;
    }

    /**
     * update base item  model => cell data
     */
    updateBaseItems = (baseItemModels) => {
        let cellChange = [];
        this.getModel().beginUpdate();
        try {
            for (let index in baseItemModels) {
                let baseItemModel = baseItemModels[index];
                baseItemModel = this.replaceBaseItemText(baseItemModel)
                let cell = this.getCellByModelAndType(baseItemModel, KItemType.BASE_ITEM);
                //this cell has no id
                if (cell) {
                    if (cell.data && cell.data.id <= 0) {
                        this.getModel().cells[baseItemModel.id] = this.getModel().cells[cell.data.uid]; // Assign new key 
                        delete this.getModel().cells[cell.data.uid]; // Delete old key
                        cell.setId(baseItemModel.id);
                    }
                    
                    // cell.data = baseItemModel;
                    cell.setModel(baseItemModel);
                    cell.setHasRole(this.sheetParentId)

                    //update cell geometry
                    let geo = cell.geometry.clone();
                    geo.x = baseItemModel.x;
                    geo.y = baseItemModel.y;
                    geo.width = baseItemModel.width;
                    geo.height = baseItemModel.height;
                    this.getModel().setGeometry(cell, geo);

                    // update cell label
                    let baseitemLabel = cell.baseItemLabel;
                    baseitemLabel.baseItem = cell;
                    let geoLabel = baseitemLabel.geometry.clone();
                    geoLabel.width = baseItemModel.textWidth
                    geoLabel.height = baseItemModel.textHeight
                    geoLabel.x = baseItemModel.textX;
                    geoLabel.y = baseItemModel.textY;
                   
                    cellChange.push(cell);
                    
                    this.getModel().setGeometry(baseitemLabel, geoLabel);

                    //update cell style
                    this.getModel().setStyle(cell, KGraphUtil.getItemModelStyle(baseItemModel));
                    this.getModel().setStyle(cell.baseItemLabel, KGraphUtil.getBaseItemLabelStyle(baseItemModel));

                    //update baseItemLabel value
                    this.getModel().setValue(cell, baseItemModel.text);
                    this.getModel().setValue(cell.baseItemLabel, baseItemModel.text);

                    cell.setIsNew();
                }
            }
        } finally {
            this.getModel().endUpdate();
        }
    }

    //NOTE: [ThuyTV] override mxGraph dblClick
    //disable edit cell label on double click
    dblClick(evt, cell) {
        ActionLogger.click('User_Editor_SheetView', 'Double_Click_To_Sheet_View');
        Logger.logConsole('[ThuyTV] override mxGraph dblClick');
        if (!mxEvent.isMultiTouchEvent(evt)){
            const mxe = new mxEventObject(mxEvent.DOUBLE_CLICK, 'event', evt, 'cell', cell);
            if(!cell || (cell && cell.itemType !== KItemType.LINK_ITEM)){
                this.fireEvent(mxe);
            }
            
            // Handles the event if it has not been consumed
            if (this.isEnabled() && !mxEvent.isConsumed(evt) && !mxe.isConsumed()) {
                if (mxEvent.IS_TOUCH) {
                    this.startEditingAtCell(cell, evt);
                }
                mxEvent.consume(evt);
            }
        }

    }

    /**
     * @description overwrite function click of mxGraph
     * @author QuanNH
     * @param {object} me 
     */
    click(me){
        var evt = me.getEvent();
        var cell = me.getCell();

        if (this.graphHandler.cellAttachTarget) {
            const cellAttachTarget = this.graphHandler.cellAttachTarget;
            this.graphHandler.cellAttachTarget = null;  
            if (cell === null) {
                return;
            }
            if (this.model.isVertex(cell) && cell.id !== cellAttachTarget.id) {
                return;
            }
        }

        // return false when click label of edge
        if (this.getModel().isEdge(cell) && evt.target.nodeName === 'DIV') {
            return;
        } else {
            super.click(me);
        }
        
    }

    createSelectionModel() {
        // Logger.logConsole("override funtion create selection model");
        let selectionModel = new KSelectionModel(this);
        return selectionModel;
        //return new mxGraphSelectionModel(this);
    }

  

   
    /**
    * set flag isBaseItemLock
    */
    setAllBaseItemLocked = (lockedFlag) => {
        this.isAllBaseItemLocked = lockedFlag;
        this.clearSelection();
    }

    /**
     * override function mxgraph isCellLocked
     */
    isCellLocked = (cell) => {
        if (this.model.isVertex(cell) && this.isItemLocked(cell)) {
            return true;
        }

        return super.isCellLocked(cell);
    }


    /**
     * Returns true if the given cell may not be moved, sized, bended, disconnected, edited or selected.
     * check item is locked if cell select is label => check base item
     */
     isItemLocked = (cell) => {
        ActionLogger.click('User_Editor_SheetLinkView', 'Select_Lock_Item');
         let isLocked = false;
        // if cell is base item label
        if (cell.isBaseItemLabel === true) {
            const baseItem = cell.baseItem;
            if (baseItem.data && baseItem.data.id && (baseItem.checkItemIsLocked() === true || this.isAllBaseItemLocked === true)) {
                isLocked =  true;
            }
        } else if (cell.itemType === KItemType.BASE_ITEM && this.isAllBaseItemLocked === true) {
            isLocked = true;
        } 
        // else if (this.isItemLockByUser(cell)){
        //     isLocked =  true;
        // }

        return isLocked;
    }

    isItemLockByUser = (cell) => {
        let isLockedByUser = false;
        if(cell && cell.isBaseItemLabel === true){
            cell = cell.baseItem;
        }
        
        if (cell && cell.data) {
            //item is existed on server
            if (cell.data.id > 0) {
                if (cell.checkItemIsLocked() === true) {
                    isLockedByUser =  true;
                }
            } else {
                //item is not exists on server
                isLockedByUser =  true;
            }
        }

        return isLockedByUser;
    }

    isCellMovable(cell) {
        if (this.isItemLockByUser(cell) === true) {
            return false;
        }  

        return super.isCellMovable();    
    }

    /**
     * check item is editting 
     */
    isItemEditing = () => {
        let cells = this.getSelectionCells();
        if (cells.length > 0) {
            for (let i = 0; i < cells.length; i++) {
                let cell = cells[i];
                if (this.isEditing(cell)) {
                    return true;
                }
            }
        }
        return false;
    }


    destroy = () => {
        if (this.graphZoomHandler) {
            this.graphZoomHandler.destroy();
        }

        super.destroy();

        this.removeWindowKeyListener();
        let values = Object.values(this);
        for (let i = 0; i < values.length; i++) {
            if (values[i] && Array.isArray(values[i].eventListeners)) {
                values[i].eventListeners = [];
            }
        }

        this.eventListeners = [];
        mxEvent.removeAllListeners(window);
        mxEvent.removeAllListeners(document);
    }

    /**
     * merge from MainBase.as/deployItemLines
     * @param {*} cell 
     */

    deployItemLines = (itemLineModels) => {
        if (Array.isArray(itemLineModels)) {
            for (let i = 0; i < itemLineModels.length; i++) {
                this.addLine(itemLineModels[i]);
            }
        }
    }

    addLine = (lineModel) => {
        let newItems = [];
        const graphModel = this.getModel();
        graphModel.beginUpdate();
        const cells = Object.values(graphModel.cells);
        if (graphModel.getCell(lineModel.id)) {
            this.updateItemLine(lineModel);
        } else {
            try {
                //render new cells
                let parent = this.getDefaultParent();
                const itemTypeLinkAllow = [KItemType.DEFAULT_ITEM, KItemType.BASE_ITEM];
                let source = null;
                let target = null;

                for (let i = 0; i < cells.length; i ++) {
                    if (cells[i] instanceof KItem || cells[i] instanceof KBaseItem) {
                        if (lineModel.startId === cells[i].data.id) {
                            source = cells[i];
                        } else if (lineModel.endId === cells[i].data.id) {
                            target = cells[i]
                        }

                        if (source && target) {
                            break;
                        }
                    }
                }

                if( 
                    source && target &&
                    itemTypeLinkAllow.indexOf(source.itemType) > -1 && 
                    itemTypeLinkAllow.indexOf(target.itemType) > -1 
                ){
                    let edge = new KLineItem(lineModel);

                    edge.data.kind = source.itemType === KItemType.DEFAULT_ITEM ? RelationLineKind.ITEM : RelationLineKind.BASE_ITEM;
                    this.addCell(edge, parent, null, source, target);
                    if(source.data){
                        if(!Array.isArray(source.data.relationLines)){
                            source.data.relationLines = [];
                        }

                        if(source.data.relationLines.indexOf(lineModel) === -1){
                            source.data.relationLines.push(lineModel);
                        }
                    }
                    graphModel.setValue(edge, mxUtils.htmlEntities(lineModel.text, false));

                    newItems.push(edge);
                    this.sendCellsToBack(newItems);
                }
                
            } finally {
                graphModel.endUpdate();
            }
        }
        this.sendCellsToBack(newItems);
        return newItems;
    }

    deleteItemLine = (id) => {
        const graphModel = this.getModel();
        graphModel.beginUpdate();
        try {
            const cell = graphModel.getCell(id);
            this.removeCells([cell]);
        } finally {
            graphModel.endUpdate();
        }
    }

    createItemLine = (fromCell, toCell, itemType) => {
        this.fireGraphEvent(KGraphEvent.ITEM_LINE_DO_CREATE, fromCell.data, toCell.data, itemType);
    }


    /**
    * update item line
    */
    updateItemLine = (lineModel) => {
        this.getModel().beginUpdate();
        try {
            let cell = this.getCellByModelAndType(lineModel, KItemType.LINK_ITEM);
            if (cell) {
                if (cell.data && cell.data.id <= 0) {
                    this.getModel().cells[lineModel.id] = this.getModel().cells[cell.data.uid]; // Assign new key 
                    delete this.getModel().cells[cell.data.uid]; // Delete old key
                    cell.setId(lineModel.id);
                }
                
                cell.data = lineModel;

                //update cell style
                this.getModel().setStyle(cell, KGraphUtil.getItemLineModelStyle(lineModel));

                //update baseItemLabel value
                this.getModel().setValue(cell, mxUtils.htmlEntities(lineModel.text, false));
            }

        } finally {
            this.getModel().endUpdate();
        }
    }
    /**
    * filter get selection item and base item
    */
    getSelectionItems = () => {
        let cells = this.getSelectionCells();
        let selectionItems = [];
        for (let i = 0; i < cells.length; i++) {
            let cell = cells[i];
            if (cell.itemType === KItemType.BASE_ITEM || cell.itemType === KItemType.DEFAULT_ITEM) {
                selectionItems.push(cell);
            }
        }
        return selectionItems;
    }

    sendCellsToFront = (cells) => {
        if(Array.isArray(cells)){
            try{
                this.getModel().beginUpdate();
                this.orderCells(false, cells);
            }finally{
                this.getModel().endUpdate();
            }
        }
    }

    sendCellsToBack = (cells) => {
        if(Array.isArray(cells)){
            try{
                this.getModel().beginUpdate();
                this.orderCells(true, cells);
            }finally{
                this.getModel().endUpdate();
            }
        }
    }

    orderCells(back, cells){
        if (cells.length === 1 && cells[0] && cells[0].itemType === KItemType.LINK_ITEM ) {
            back = true;
        }
        super.orderCells(back, cells);
    }

    getCellsOverlapping = (rectangle, parent, result) => { 
        result = (result != null) ? result : [];

        if ( rectangle instanceof mxRectangle &&  (rectangle.width > 0 || rectangle.height > 0) )
        {
            var model = this.getModel();
            if (parent == null)
            {
                parent = this.getCurrentRoot();
                
                if (parent == null)
                {
                    parent = model.getRoot();
                }
            }
            
            if (parent != null)
            {
                var childCount = model.getChildCount(parent);
                
                for (var i = 0; i < childCount; i++)
                {
                    var cell = model.getChildAt(parent, i);
                    var state = this.view.getState(cell);
                    
                    if (state != null && this.isCellVisible(cell)) {
                        let compareRect = null;
                        if( typeof cell.geometry !== 'undefined'){
                            compareRect = cell.geometry.toScale(this.view.scale);
                        }

                        if (
                            model.isVertex(cell) &&
                            (cell.itemType === KItemType.BASE_ITEM || cell.itemType === KItemType.DEFAULT_ITEM) &&
                            rectangle.isOverlap(compareRect)
                        )
                        {
                            result.push(cell);
                        }
                        else
                        {
                            this.getCellsOverlapping(rectangle, cell, result);
                        }
                    }
                }
            }
        }
        
        return result;
    }

    setFilterGroupItems = (cells, filterId) => {
        cells.forEach(cell=>{
            const state = this.view.getState(cell);
            if( typeof state !== 'undefined' && state){
                let group = state.shape.node;
                if( cell.itemType === KItemType.LINK_ITEM){
                    if ( this.doFixEdgeShadowOnBrowser(group, cell) !== true){
                        state.shape.node.setAttribute('filter', `url(#${filterId})`);
                        state.shape.filterBlur = true;
                    }
                } else {
                    state.shape.node.setAttribute('filter', `url(#${filterId})`);
                    state.shape.filterBlur = true;

                }
            }
            
        })
    }


    doFixEdgeShadowOnBrowser=(nodeGroup, cell)=>{
        let fixOnBrowser = false;
        if ( nodeGroup.childNodes.length < 1 ){
            return false;
        }
        
        let shadow = nodeGroup.cloneNode(true);

        switch (KGraphUtil.browser && KGraphUtil.browser.name) {
            case 'safari':
            case 'ie':
                if( KGraphUtil.browser.name === 'ie'){
                    shadow.setAttribute('class', 'graph-line-shadow');
                } else {
                    shadow.classList.add("graph-line-shadow");
                }
                shadow.setAttribute("stroke-opacity",0.2);
                shadow.setAttribute("fill-opacity",0.2);
                shadow.setAttribute("transform",` translate(3 3)`);

                if( 
                    KGraphUtil.browser.name === 'ie' && 
                    typeof cell !== 'undefined' && 
                    cell.target.geometry.y !== cell.source.geometry.y && 
                    cell.target.geometry.x !== cell.source.geometry.x
                ){
                    fixOnBrowser = false;
                } else {
                    fixOnBrowser = true;
                }
                break;

            default:
                fixOnBrowser = false;
                break;
        }
        if( fixOnBrowser ){
            nodeGroup.parentNode.insertBefore(shadow, nodeGroup);
        }
        return fixOnBrowser;
    }

    removeFilterItem = (cell) => {
        const state = this.view.getState(cell);
        if( typeof state !== 'undefined' && state){
            if( state.shape ){
                state.shape.node.removeAttribute('filter');
                state.shape.filterBlur = false;
                const elms = document.getElementsByClassName("graph-line-shadow");
                elms.forEach(elm=>{
                    elm.parentNode.removeChild(elm);
                });
            }
            
        }
        
    }


    moveCenterDragTargetGroupByItem = (cell) => {
        this.scrollCellToVisible(cell,true);
    }

    /**
    * overide function getCellContainmentArea in base mxgraph
    * với base item label thì không giới hạn khoảng không gian trong parent, có thể resize ra ngoài parent
    */
    getCellContainmentArea = (cell) => {
        if (cell != null && this.model.isVertex(cell) && cell.itemType === KItemType.BASE_ITEM_LABEL) {
            return null;
        }
        super.getCellContainmentArea(cell);
    }

    /**
    * overide function isExtendParent in base mxgraph
    * nếu là base item label thì khi resize không cần extend size của parent theo nó
    */
    isExtendParent = (cell) => {
        if (cell != null && this.model.isVertex(cell) && cell.itemType === KItemType.BASE_ITEM_LABEL) {
            return false;
        }
        super.isExtendParent(cell);
    }

    setSavePngBySelectedFlag = (flag) => {
        this.savePngBySelected = flag;
        if (flag === true) {
            this.clearSelection();   
            this.stopEditing(false);
        }
        this.setCellsLocked(flag);
    }

    setGoodButtonFlag = (goodButtonFlag) => {
        this.isShowGoodButton = goodButtonFlag;
        
        let updateCells = [];
        this.getModel().beginUpdate();
        try {
            let cells = this.getModel().cells;
            Object.keys(cells).map(function(key, index) {
                let cell = cells[key];
                if (cell.itemType === KItemType.DEFAULT_ITEM) {
                    cell.setVisibleGoodbutton(goodButtonFlag);
                    updateCells.push(cell);
                }
            });
        }
        finally {
            this.getModel().endUpdate();
        }
        this.sendCellsToFront(updateCells);

    }

    updateItemLike = (item, clicking) => {
        let goodbutton = item.goodbutton;
        let updateCells = [];
        this.getModel().beginUpdate();
        try {
            item.setGoodButtonValue();

            let geo = goodbutton.geometry.clone();
            geo.width = item.getGoodButtonWidth();
            goodbutton.setGeometry(geo);
            updateCells.push(goodbutton);
        }
        finally {
            this.getModel().endUpdate();
        }

        if (updateCells.length) {
            this.sendCellsToFront(updateCells);
        }

        if (clicking === true && item.data.liked && !mxClient.IS_TOUCH) {
            let state = this.getView().getState(goodbutton);
            state.shape.bounds.width =  KGraphUtil.MAX_GOODBUTTON_WIDTH*state.view.scale;
            let goodText =  i18n.t('text.stateUndo');
            let goodIcon =  unLikeIcon;
            state.text.value = KGraphUtil.createGoodButtonLabel(goodIcon, goodText, item.data.likeCount);
            if (state.shape) {
                state.shape.apply(state);
                state.shape.redraw();
            }
            if (state.text) {
                state.text.apply(state);
                state.text.redraw();
            }

            this.graphHandler.goodItemHightLighted = state;
        }
       
    }

    setPrintingRangeGuideDisplayFlag = (displayFlag) => {
        this.printingRangeGuideDisplay = displayFlag;
        this.view.validatePrintRangeGuide();
    }

    /**
     * handle icon item edit label
     * @param {*} cell 
     */
    itemIconEditLabelClickHandler = (cell) => {
        let parent = cell.parent;
        this.setCellIconEditLabelVisible(parent, false);
        if (parent.itemType === KItemType.BASE_ITEM) {
            parent = parent.baseItemLabel
        }
        this.startEditingAtCell(parent);
    }
    
    handleStartEditItem = (sender, evt) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Button_Edit_RelationLine_Text');
        let cell = evt.getProperty("cell");
        if (cell != null && this.model.isVertex(cell)) {
            if (cell.itemType === KItemType.DEFAULT_ITEM ) {
                this.setCellIconEditLabelVisible(cell, false);
            } else if (cell.itemType === KItemType.BASE_ITEM_LABEL) {
                this.setCellIconEditLabelVisible(cell.baseItem, false);
                cell.setVisibleIconResize(true);
                // this.setCellIconEditResizeVisible(cell, false);
            }
        }
    }

    handleStopEditItem = (sender, evt) => {
        let cells = this.getSelectionCells();
        if (cells.length > 0) {
            for (let i = 0; i < cells.length; i++) {
                let cell = cells[i];
                if (cell.iconEditLabel && cell.iconEditLabel.visible === false) {
                    this.setCellIconEditLabelVisible(cell, true);
                }
            }
        }
    }

    setCellVisible = (cell, visible) => {
        this.getModel().beginUpdate();
        try {
            cell.setVisible(visible);
            this.sendCellsToFront([cell]);
        } finally {
            this.getModel().endUpdate();
        }
    }

    setCellIconEditLabelVisible = (cell, visible) => {
        this.getModel().beginUpdate();
        try {
            cell.setVisibleIconEditLabel(visible);
            this.sendCellsToFront([cell.iconEditLabel]);
        } finally {
            this.getModel().endUpdate();
        }
    }

    /**
     * QuanNH overwrite parent prototype
     * @description allow toggle item when press Shift
     * @param {*} evt 
     */
    isToggleEvent(evt) {
        if (this.graphHandler.clearSelectionWhenShiftDown === true) {
            this.graphHandler.clearSelectionWhenShiftDown = false;
        }
        return mxEvent.isShiftDown(evt);
    }

    selectCellForEvent(cell, evt) {
        if (this.graphHandler.clearSelectionWhenShiftDown === true) {
            this.graphHandler.clearSelectionWhenShiftDown = false;
            return;
        }
        
        super.selectCellForEvent(cell, evt);
    }

    /**
     * revert attr is deleting of cell when delete error
     */
    revertAttributeIsDeleting = (cellids) => {
        for (let id of cellids) {
            const cell = this.getModel().cells[id];
            if (typeof cell !== 'undefined' && cell && cell.isDeleting === true) {
                cell.isDeleting = false;
            }
        }
    }

    timeoutStartEditingAtCell = (cell) => {
        let editCell = cell.itemType === KItemType.BASE_ITEM ? cell.baseItemLabel : cell ;
        this.startEditingAtCellTheard = setTimeout(() => {     
            this.startEditingAtCell(editCell);
        }, 0);
    }

    clearTimeoutTheard = () => {
        clearTimeout(this.showCreatorLabelThred);
        clearTimeout(this.startEditingAtCellTheard);
    }
    /**
     * check has item moving
     */
    hasItemMoving = () => {
        let cells = this.graphHandler.cells;
        if (cells && cells.length > 0) {
            if (cells.length === 1 && cells[0].itemType === KItemType.BASE_ITEM_LABEL) {
                return false;
            }
            return true;
        }
        return false;
    }
}

export default KGraph;