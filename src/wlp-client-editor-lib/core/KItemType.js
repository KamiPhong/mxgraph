let KItemType = {
  DEFAULT_ITEM : "defaultItem",
  LINK_ITEM    : "linkItem",
  BASE_ITEM    : "baseItem",
  BASE_ITEM_LABEL    : "baseItemLabel",
  MOVIE_ITEM   : "movieItem",
  MEMO_ITEM    : "memoItem",

  ATTACH_ITEM  : "attachItem",



  LABEL_ITEM   : "labelItem",
  PHOTO_ITEM   : "photoItem",
  DOC_ITEM     : "docItem",
  XLS_ITEM     : "xlsItem",
  PPT_ITEM     : "pptItem",
  PDF_ITEM     : "pdfItem",
  TXT_ITEM     : "txtItem",
  CSV_ITEM     : "csvItem",
}

export default KItemType;
