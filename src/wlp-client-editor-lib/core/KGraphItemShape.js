let KGraphItemShape = {
    NONE: -1,
    RECT: 0,
    ELLIPSE: 1,
    SHAPE_ARROW_RIGHT: 2,
    SHAPE_ARROW_LEFT: 3,
    DIAMOND: 4,
    CLOUD: 5,
    HANDWRITING_ELLIPSE: 6,
    HANDWRITING_RECT: 7,
    BASE_RECT: 8,
    BASE_ROUND_RECT: 9,
    BASE_TRIANGLE: 10,
    BASE_ELLIPSE: 11,

    SHAPE_RECTANGLE: 'rectangle',
    SHAPE_NAME_RECT: 'kroundrect',
    SHAPE_NAME_ELLIPSE: 'ellipse',
    SHAPE_NAME_SHAPE_ARROW_RIGHT: 'karrowright',
    SHAPE_NAME_SHAPE_ARROW_LEFT: 'karrowleft',
    SHAPE_NAME_DIAMOND: 'kdiamon',
    SHAPE_NAME_CLOUD: 'kcloud',
    SHAPE_NAME_HANDWRITING_ELLIPSE: 'khandwritingeclipse',
    SHAPE_NAME_HANDWRITING_RECT: 'khandwritingrect',
    SHAPE_NAME_BASE_RECT: 'kroundrect',
    SHAPE_NAME_BASE_ROUND_RECT: 'kroundrect',
    SHAPE_NAME_BASE_TRIANGLE: 'basetriangle',
    SHAPE_NAME_BASE_ELLIPSE: 'ellipse',
    SHAPE_NAME_SELECTION_BORDER: 'selectionBorder',

    getString(int) {
        let shape;
        switch (int) {
            case this.RECT:
                shape = this.SHAPE_NAME_RECT;
                break;
            case this.ELLIPSE:
                shape = this.SHAPE_NAME_ELLIPSE;
                break;
            case this.BASE_RECT:
                shape = this.SHAPE_RECTANGLE;
                break;
            case this.BASE_ROUND_RECT:
                shape = this.SHAPE_RECTANGLE;
                break;
            case this.BASE_TRIANGLE:
                shape = this.SHAPE_NAME_BASE_TRIANGLE;
                break;
            case this.BASE_ELLIPSE:
                shape = this.SHAPE_NAME_BASE_ELLIPSE;
                break;
            case this.SHAPE_ARROW_RIGHT:
                shape = this.SHAPE_NAME_SHAPE_ARROW_RIGHT;
                break;
            case this.SHAPE_ARROW_LEFT:
                shape = this.SHAPE_NAME_SHAPE_ARROW_LEFT;
                break;
            case this.DIAMOND:
                shape = this.SHAPE_NAME_DIAMOND;
                break;
            case this.HANDWRITING_RECT:
                shape = this.SHAPE_NAME_HANDWRITING_RECT;
                break;
            case this.HANDWRITING_ELLIPSE:
                shape = this.SHAPE_NAME_HANDWRITING_ELLIPSE;
                break;
            case this.CLOUD:
                shape = this.SHAPE_NAME_CLOUD;
                break;
            default:
                shape = this.SHAPE_NAME_RECT;
                break;
        }
        return shape;
    },

    getConstantFromString(str) {
        let shapeIndex = this.NONE;
        switch (str) {
            case 'ellipse':
                shapeIndex = this.ELLIPSE;
                break;

            case 'rectangle':
            default:
                shapeIndex = this.RECT;
                break;
        }

        return shapeIndex;
    }
};

export default KGraphItemShape;