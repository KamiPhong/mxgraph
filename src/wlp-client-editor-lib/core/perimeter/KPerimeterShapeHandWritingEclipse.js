import {  mxUtils, mxPoint } from "../KClient";

function KPerimeterShapeHandWritingEclipse(bounds, vertex, next, orthogonal) {
    
    const x = bounds.x;
    const y = bounds.y;
    const w = bounds.width;
    const h = bounds.height;
    const cx = bounds.getCenterX();
    const cy = bounds.getCenterY();
    
    const scaleW = w / 142.7;
    const scaleH = h / 84.15;

    const points = [
		new mxPoint(x + 115.6 * scaleW, y + 69.05 * scaleH),
		new mxPoint(x + 102.85 * scaleW, y + 75.15 * scaleH),
        new mxPoint(x + 80 * scaleW, y + 80.95 * scaleH),
        new mxPoint(x + 64.7 * scaleW, y + 84.15 * scaleH),
        new mxPoint(x + 80 * scaleW, y +  80.45 * scaleH),
        new mxPoint(x + 59.1 * scaleW, y +  77.1 * scaleH),
        new mxPoint(x + 27.65 * scaleW, y +  74.05 * scaleH),
        new mxPoint(x + 5.5 * scaleW, y +  62 * scaleH), // fix
        new mxPoint(x + 4.5 * scaleW, y +  60.3 * scaleH), // fix
        new mxPoint(x + 2 * scaleW, y +  58.05 * scaleH),
        new mxPoint(x + (-1 * scaleW), y +  38.2 * scaleH),
        new mxPoint(x + 10.3 * scaleW, y +  22.45 * scaleH),
        // new mxPoint(x + 12.6 * scaleW, y +  22 * scaleH),// fix
        // new mxPoint(x + 22.6 * scaleW, y +  18 * scaleH), // add more
        new mxPoint(x + 25.6 * scaleW, y +  10 * scaleH), // fix
        new mxPoint(x + 53.55 * scaleW, y +  1.65 * scaleH),
        new mxPoint(x + 85.45 * scaleW, y +  (1 * scaleH)),
        new mxPoint(x + 111.5 * scaleW, y +  5.45 * scaleH),
        new mxPoint(x + 128.9 * scaleW, y +  14 * scaleH),
        new mxPoint(x + 139.55 * scaleW, y +  31.15 * scaleH),
        new mxPoint(x + 140.55 * scaleW, y +  43.6 * scaleH),
        new mxPoint(x + 136.55 * scaleW, y + 53 * scaleH),
        new mxPoint(x + 115.6 * scaleW, y + 69.05 * scaleH),
	];

    let p1 = new mxPoint(cx, cy);

    if (orthogonal)
    {
        if (next.x < x || next.x > x + w)
        {
            p1.y = next.y;
        }
        else
        {
            p1.x = next.x;
        }
    }

    return mxUtils.getPerimeterPoint(points, p1, next);
}

export default KPerimeterShapeHandWritingEclipse;