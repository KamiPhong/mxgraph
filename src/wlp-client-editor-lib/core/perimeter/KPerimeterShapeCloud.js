import {  mxUtils, mxPoint } from "../KClient";

function KPerimeterShapeCloud(bounds, vertex, next, orthogonal) {
    
    const x = bounds.x;
    const y = bounds.y;
    const w = bounds.width;
    const h = bounds.height;
    const cx = bounds.getCenterX();
    const cy = bounds.getCenterY();
    
    const scaleW = w / 137.75;
    const scaleH = h / 83.4;

    const points = [
		new mxPoint(x + 87.55 * scaleW,y +  0.9 * scaleH),
        new mxPoint(x + 94.5 * scaleW,y +  2.3 * scaleH),
        new mxPoint(x + 100.45 * scaleW,y +  6.95 * scaleH),
        new mxPoint(x + 103.45 * scaleW,y +  9.25 * scaleH),
        new mxPoint(x + 105 * scaleW,y +  11.3 * scaleH),
        new mxPoint(x + 109.35 * scaleW,y +  11.8 * scaleH),
        new mxPoint(x + 114.65 * scaleW,y +  12.65 * scaleH),
        new mxPoint(x + 119.2 * scaleW,y +  14.55 * scaleH),
        new mxPoint(x + 128.8 * scaleW,y +  20.55 * scaleH),
        new mxPoint(x + 135.8 * scaleW,y +  34.35 * scaleH),
        new mxPoint(x + 137.75 * scaleW,y +  48.15 * scaleH),
        new mxPoint(x + 127.3 * scaleW,y +  59 * scaleH),
        new mxPoint(x + 124 * scaleW,y +  62.4 * scaleH),
        new mxPoint(x + 119.85 * scaleW,y +  65.1 * scaleH),
        new mxPoint(x + 116.4 * scaleW,y +  67.1 * scaleH),
        new mxPoint(x + 116 * scaleW,y +  68.6 * scaleH),
        new mxPoint(x + 113.5 * scaleW,y +  70.7 * scaleH),
        new mxPoint(x + 108.6 * scaleW,y +  74.95 * scaleH),
        new mxPoint(x + 98.45 * scaleW,y +  78.05 * scaleH),
        new mxPoint(x + 88.3 * scaleW,y +  81.15 * scaleH),
        new mxPoint(x + 80 * scaleW,y +  79.95 * scaleH),
        new mxPoint(x + 77.45 * scaleW,y +  79.55 * scaleH),
        new mxPoint(x + 75.3 * scaleW,y +  78.8 * scaleH),
        new mxPoint(x + 73.75 * scaleW,y +  78.1 * scaleH),
        new mxPoint(x + 71.15 * scaleW,y +  79.5 * scaleH),
        new mxPoint(x + 67.1 * scaleW,y +  80.8 * scaleH),
        new mxPoint(x + 59.05 * scaleW,y +  83.4 * scaleH),
        new mxPoint(x + 51.9 * scaleW,y +  82.85 * scaleH),
        new mxPoint(x + 43.05 * scaleW,y +  82.15 * scaleH),
        new mxPoint(x + 35.6 * scaleW,y +  76.95 * scaleH),
        new mxPoint(x + 33.25 * scaleW,y +  75.3 * scaleH),
        new mxPoint(x + 31.35 * scaleW,y +  73.4 * scaleH),
        new mxPoint(x + 29.9 * scaleW,y +  71.85 * scaleH),
        new mxPoint(x + 24.15 * scaleW,y +  71.9 * scaleH),
        new mxPoint(x + 17.7 * scaleW,y +  69.6 * scaleH),
        new mxPoint(x + 9.75 * scaleW,y +  64.95 * scaleH),
        new mxPoint(x +  1.2 * scaleW,y +  53.2 * scaleH),
        new mxPoint(x + -(1.35 * scaleW),y +  41.4 * scaleH),
        new mxPoint(x + 4.65 * scaleW,y +  31.45 * scaleH),
        new mxPoint(x + 8.2 * scaleW,y +  26.5 * scaleH),
        new mxPoint(x + 12.4 * scaleW,y +  23.85 * scaleH),
        new mxPoint(x + 12.6 * scaleW,y +  23.1 * scaleH),
        new mxPoint(x + 13.15 * scaleW,y +  21.85 * scaleH),
        new mxPoint(x + 14.25 * scaleW,y +  19.35 * scaleH),
        new mxPoint(x + 16 * scaleW,y +  16.95 * scaleH),
        new mxPoint(x + 21.55 * scaleW,y +  9.2 * scaleH),
        new mxPoint(x + 31.5 * scaleW,y +  5.15 * scaleH),
        new mxPoint(x + 41.4 * scaleW,y +  1.05 * scaleH),
        new mxPoint(x + 53.4 * scaleW,y +  2.05 * scaleH),
        new mxPoint(x + 59.4 * scaleW,y +  2.55 * scaleH),
        new mxPoint(x + 63.4 * scaleW,y +  3.85 * scaleH),
        new mxPoint(x + 64.95 * scaleW,y +  2.95 * scaleH),
        new mxPoint(x + 67.05 * scaleW,y +  1.9 * scaleH),
        new mxPoint(x + 69.6 * scaleW,y +  1.2 * scaleH),
        new mxPoint(x + 77.75 * scaleW,y +  (-1.05 * scaleH)),
        new mxPoint(x + 87.55 * scaleW,y +  0.9 * scaleH),
        new mxPoint(x + 87.55 * scaleW,y +  0.9 * scaleH),
	];

    let p1 = new mxPoint(cx, cy);

    if (orthogonal)
    {
        if (next.x < x || next.x > x + w)
        {
            p1.y = next.y;
        }
        else
        {
            p1.x = next.x;
        }
    }

    return mxUtils.getPerimeterPoint(points, p1, next);
}

export default KPerimeterShapeCloud;