import { mxConstants } from 'wlp-client-editor-lib/core/KClient';
import KPerimeterBaseTriangle from './KPerimeterBaseTriangle';
import KPerimeterShapeArrowRight from './KPerimeterShapeArrowRight';
import KPerimeterShapeArrowLeft from './KPerimeterShapeArrowLeft';
import KPerimeterShapeDiamon from './KPerimeterShapeDiamon';
import KPerimeterShapeHandWritingEclipse from './KPerimeterShapeHandWritingEclipse';
import KPerimeterShapeCloud from './KPerimeterShapeCloud';
import KPerimeterShapeHandWritingRect from './KPerimeterShapeHandWritingRect';
import KPerimeterShapeRoundRect from './KPerimeterShapeRoundRect';
import KGraphItemShape from '../KGraphItemShape';

const KPerimeterConstant = {
    PERIMETER_BASE_TRIANGLE: "KPerimeterBaseTriangle",
    PERIMETER_ARROW_RIGHT: "KPerimeterShapeArrowRight",
    PERIMETER_ARROW_LEFT: "KPerimeterShapeArrowLeft",
    PERIMETER_DIAMOND: "KPerimeterShapeDiamon",
    PERIMETER_HAND_WRITING_ECLIPSE: "KPerimeterShapeHandWritingEclipse",
    PERIMETER_HAND_WRITING_RECT: "KPerimeterShapeHandWritingRect",
    PERIMETER_CLOUD: "KPerimeterShapeCloud",
    PERIMETER_ROUND_RECT: "KPerimeterShapeRoundRect",
}

let getStringPerimeterFromShape = (int) => {
    switch (int) {
        case KGraphItemShape.RECT:
        case KGraphItemShape.BASE_ROUND_RECT:
            return mxConstants.PERIMETER_ROUND_RECT
        case KGraphItemShape.BASE_RECT:
            return mxConstants.PERIMETER_RECTANGLE
        case KGraphItemShape.BASE_TRIANGLE:
            return KPerimeterConstant.PERIMETER_BASE_TRIANGLE
        case KGraphItemShape.ELLIPSE:
        case KGraphItemShape.BASE_ELLIPSE:
            return mxConstants.PERIMETER_ELLIPSE
        case KGraphItemShape.SHAPE_ARROW_RIGHT:
            return KPerimeterConstant.PERIMETER_ARROW_RIGHT
        case KGraphItemShape.SHAPE_ARROW_LEFT:
            return KPerimeterConstant.PERIMETER_ARROW_LEFT
        case KGraphItemShape.DIAMOND:
            return KPerimeterConstant.PERIMETER_DIAMOND;
        case KGraphItemShape.HANDWRITING_ELLIPSE:
            return KPerimeterConstant.PERIMETER_HAND_WRITING_ECLIPSE
        case KGraphItemShape.HANDWRITING_RECT:
            return KPerimeterConstant.PERIMETER_HAND_WRITING_RECT
        case KGraphItemShape.CLOUD:
            return KPerimeterConstant.PERIMETER_CLOUD
        default:
            return mxConstants.PERIMETER_RECTANGLE
    }
}

export {
    KPerimeterBaseTriangle,
    KPerimeterShapeArrowRight,
    KPerimeterShapeArrowLeft,
    KPerimeterShapeDiamon,
    KPerimeterShapeHandWritingEclipse,
    KPerimeterShapeHandWritingRect,
    KPerimeterShapeCloud,
    KPerimeterShapeRoundRect,
    getStringPerimeterFromShape,
    KPerimeterConstant
}