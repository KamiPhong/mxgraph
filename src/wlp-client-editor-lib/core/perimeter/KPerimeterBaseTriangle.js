import { mxUtils, mxPoint } from "../KClient";

function KPerimeterBaseTriangle(bounds, vertex, next, orthogonal) {
    const x = bounds.x;
    const y = bounds.y;
    const w = bounds.width;
    const h = bounds.height;
    const cx = bounds.getCenterX();
    const cy = bounds.getCenterY();
    
    const start = new mxPoint(x, y + h);
    const corner = new mxPoint(x + w/2, y);
    const end = new mxPoint(x + w, y + h);
    
    const points = [start,  corner, end, start];

    let p1 = new mxPoint(cx, cy);

    if (orthogonal)
    {
        if (next.x < x || next.x > x + w)
        {
            p1.y = next.y;
        }
        else
        {
            p1.x = next.x;
        }
    }
    return mxUtils.getPerimeterPoint(points, p1, next);
}

export default KPerimeterBaseTriangle;