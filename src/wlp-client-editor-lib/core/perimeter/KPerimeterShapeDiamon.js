import {  mxUtils, mxPoint } from "../KClient";

function KPerimeterShapeDiamon(bounds, vertex, next, orthogonal) {
    
    const x = bounds.x;
    const y = bounds.y;
    const w = bounds.width;
    const h = bounds.height;
    const cx = bounds.getCenterX();
    const cy = bounds.getCenterY();
    
    const points = [
		new mxPoint(x, y + h/2),
		new mxPoint(x + w/2, y + h),
		new mxPoint(x + w, y + h/2),
		new mxPoint(x + w/2, y),
        new mxPoint(x, y + h/2),
	];

    let p1 = new mxPoint(cx, cy);

    if (orthogonal)
    {
        if (next.x < x || next.x > x + w)
        {
            p1.y = next.y;
        }
        else
        {
            p1.x = next.x;
        }
    }

    return mxUtils.getPerimeterPoint(points, p1, next);
}

export default KPerimeterShapeDiamon;