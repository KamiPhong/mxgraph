import {  mxUtils, mxPoint } from "../KClient";

function KPerimeterShapeHandWritingRect(bounds, vertex, next, orthogonal) {
    
    const x = bounds.x;
    const y = bounds.y;
    const w = bounds.width;
    const h = bounds.height;
    const cx = bounds.getCenterX();
    const cy = bounds.getCenterY();
    
    const scaleW = w/132.6;
    const scaleH = h/64.75;

    const points = [
		new mxPoint(x + 132.6 * scaleW, y +  21.45 * scaleH),
        new mxPoint(x + 130.95 * scaleW, y +  64.75 * scaleH),
        new mxPoint(x + 78.55 * scaleW, y +  66.6 * scaleH),
        new mxPoint(x + 52.3 * scaleW, y +  65.9 * scaleH),
        new mxPoint(x + 26.15 * scaleW, y +  67.9 * scaleH),
        new mxPoint(x + 0.2 * scaleW, y +  67.55 * scaleH),
        new mxPoint(x + 0.0 * scaleW, y +  43.45 * scaleH),
        new mxPoint(x + 0.55 * scaleW, y +  19.1 * scaleH),
        new mxPoint(x + 1.9 * scaleW, y +  1.6 * scaleH),
        new mxPoint(x + 0.1 * scaleW, y +  1.65 * scaleH),
        new mxPoint(x + 0.1 * scaleW, y +  1.1 * scaleH),
        new mxPoint(x + 2.0 * scaleW, y +  1.05 * scaleH),
        new mxPoint(x + 2.05 * scaleW, y +  0.1 * scaleH),
        new mxPoint(x + 2.4 * scaleW, y +  0.1 * scaleH),
        new mxPoint(x + 2.3 * scaleW, y +  1.05 * scaleH),
        new mxPoint(x + 59.0 * scaleW, y +  0.05 * scaleH),
        new mxPoint(x + 83.65 * scaleW, y +  0.85 * scaleH),
        new mxPoint(x + 132.8 * scaleW, y +  0 * scaleH),
        new mxPoint(x + 132.6 * scaleW, y +  21.45 * scaleH),
        new mxPoint(x + 132.6 * scaleW, y +  21.45 * scaleH),
	];

    let p1 = new mxPoint(cx, cy);

    if (orthogonal)
    {
        if (next.x < x || next.x > x + w)
        {
            p1.y = next.y;
        }
        else
        {
            p1.x = next.x;
        }
    }

    return mxUtils.getPerimeterPoint(points, p1, next);
}

export default KPerimeterShapeHandWritingRect;