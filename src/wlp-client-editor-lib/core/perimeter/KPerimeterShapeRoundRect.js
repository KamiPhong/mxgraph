import { mxUtils, mxPoint } from "../KClient";
import KItemType from '../KItemType';

function KPerimeterShapeRoundRect(bounds, vertex, next, orthogonal) {
    const x = bounds.x;
    const y = bounds.y;
    const w = bounds.width;
    const h = bounds.height;
    const cx = bounds.getCenterX();
    const cy = bounds.getCenterY();
 
    let roundFactor = vertex.cell.itemType === KItemType.BASE_ITEM ? 9*vertex.view.scale : 5*vertex.view.scale;

    const points = [
        new mxPoint(x, y + roundFactor),
        new mxPoint(x, y + h - roundFactor),
        new mxPoint(x + roundFactor, y + h),
        new mxPoint(x + w - roundFactor, y + h),
        new mxPoint(x + w, y + h - roundFactor),
        new mxPoint(x + w, y + roundFactor),
        new mxPoint(x + w - roundFactor, y),
        new mxPoint(x + roundFactor, y),
        new mxPoint(x, y + roundFactor),
    ];

    let p1 = new mxPoint(cx, cy);

    if (orthogonal)
    {
        if (next.x < x || next.x > x + w)
        {
            p1.y = next.y;
        }
        else
        {
            p1.x = next.x;
        }
    }
    return mxUtils.getPerimeterPoint(points, p1, next);
}

export default KPerimeterShapeRoundRect;