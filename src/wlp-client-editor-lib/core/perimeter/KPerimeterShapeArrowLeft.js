import { mxUtils, mxPoint } from "../KClient";

function KPerimeterShapeArrowLeft(bounds, vertex, next, orthogonal) {
    const x = bounds.x;
    const y = bounds.y;
    const w = bounds.width;
    const h = bounds.height;
    const cx = bounds.getCenterX();
    const cy = bounds.getCenterY();
    
	const hatWidth = 30*vertex.view.scale;
	
    const points = [
        new mxPoint(x + hatWidth, y),
        new mxPoint(x, y + h/2),
		new mxPoint(x + hatWidth, y + h),
		new mxPoint(x + w, y + h),
		new mxPoint(x + w, y),
		new mxPoint(x + hatWidth, y),
	];

    let p1 = new mxPoint(cx, cy);

    if (orthogonal)
    {
        if (next.x < x || next.x > x + w)
        {
            p1.y = next.y;
        }
        else
        {
            p1.x = next.x;
        }
    }
    return mxUtils.getPerimeterPoint(points, p1, next);
}

export default KPerimeterShapeArrowLeft;