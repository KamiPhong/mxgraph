export const ITEM_COLORS = {
    BOUNDING_BOX: '#b2b2b2'
}

export const OUTLINE_SIZE = 0.04; //4% of editor

const MAX_WIDTH = 4500;
const MAX_HEIGHT = 3700;
const START_X = 0;
const START_Y = 0;
const innerStroke = '#ffffff';
const innerStrokeWidth = 8;
const outerStroke = 'gray';
const outerStrokeWidth = 1;

const MIN_SCALE = 0.25;
const MAX_SCALE = 2.05;
const MIDDLE_SCALE = 2.05;
const MAX_ZOOM_VALUE = 8;
const MIN_ZOOM_VALUE = 1;
const INIT_ZOOM_VALUE = 3;

const translateFactor = 5;
const STEP_ZOOM = MAX_SCALE / MAX_ZOOM_VALUE;
const INIT_SCALE_VALUE = INIT_ZOOM_VALUE*STEP_ZOOM;

const SLIDE_MAX_ZOOM = 32;
const SLIDE_SCROLL_STEP = (MAX_SCALE - MIN_SCALE) / SLIDE_MAX_ZOOM;

const MIN_TRANSLATE_LEFT = MAX_WIDTH/translateFactor;
const MAX_TRANSLATE_RIGHT = -(MAX_WIDTH - MAX_WIDTH/translateFactor);
const MIN_TRANSLATE_TOP = MAX_HEIGHT/translateFactor;
const MAX_TRANSLATE_BOTTOM = -(MAX_HEIGHT -MAX_WIDTH/translateFactor);

const PRINT_RANGE_GUIDE_WIDTH = 4480;
const PRINT_RANGE_GUIDE_HEIGHT = 3168;
const PRINT_RANGE_GUIDE_COLOR = '#929292';
const PRINT_RANGE_GUIDE_STROKE_WIDTH = 10;
const PRINT_RANGE_GUIDE_TRIANGLE_WIDTH = 230;
const PRINT_RANGE_GUIDE_TRIANGLE_HEIGHT = 160;
const PRINT_RANGE_GUIDE_RANGE_WIDTH = 660;
const PRINT_RANGE_GUIDE_RANGE_HEIGHT = 470;


export const PrintRangeGuideConfig = {
    PRINT_RANGE_GUIDE_WIDTH,
    PRINT_RANGE_GUIDE_HEIGHT,
    PRINT_RANGE_GUIDE_COLOR,
    PRINT_RANGE_GUIDE_STROKE_WIDTH,
    PRINT_RANGE_GUIDE_TRIANGLE_WIDTH,
    PRINT_RANGE_GUIDE_TRIANGLE_HEIGHT,
    PRINT_RANGE_GUIDE_RANGE_WIDTH,
    PRINT_RANGE_GUIDE_RANGE_HEIGHT
}

export const ZoomConfig = {
    STEP_ZOOM,
    MIN_SCALE,
    MAX_SCALE,
    MIDDLE_SCALE,
    MAX_ZOOM_VALUE,
    MIN_ZOOM_VALUE,
    INIT_ZOOM_VALUE,
    INIT_SCALE_VALUE,
    SLIDE_MAX_ZOOM,
    SLIDE_SCROLL_STEP
}

export const StageConfig = {
    MAX_WIDTH,
    MAX_HEIGHT,
    START_X,
    START_Y,
    MIN_TRANSLATE_LEFT,
    MAX_TRANSLATE_RIGHT,
    MIN_TRANSLATE_TOP,
    MAX_TRANSLATE_BOTTOM,
    innerStroke,
    innerStrokeWidth,
    outerStroke,
    outerStrokeWidth,
}