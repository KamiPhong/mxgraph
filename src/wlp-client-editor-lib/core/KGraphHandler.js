import { mxGraphHandler, mxEvent, mxConstants, mxCellHighlight, mxDictionary, mxUtils, mxRectangle } from "wlp-client-editor-lib/core/KClient";
import KGuide from "./KGuide";
import KItemType from './KItemType';
import * as KGraphUtil from './KGraphUtil';
import { imgContextDownload } from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import ItemLineMenuButtonBarEventCenter from "wlp-client-editor-lib/events/ItemLineMenuButtonBarEventCenter.js";
import likeIcon from 'wlp-client-editor-lib/assets/images/icon-flash-site/like.png';
import likedIcon from 'wlp-client-editor-lib/assets/images/icon-flash-site/liked.png';
import unLikeIcon from 'wlp-client-editor-lib/assets/images/icon-flash-site/un-like.png';
import i18n from "i18n";

import {
    FILTER_EDGE_HOVER
} from 'wlp-client-editor-lib/core/filters/KFilterConst';
import KItem from "./KItem";
import KBaseItem from "./KBaseItem";
import KBaseItemLabel from "./KBaseItemLabel";
import KItemBase from "./KItemBase";
import KLineItem from "./KLineItem";

const itemLineMenuButtonBarEventCenter = new ItemLineMenuButtonBarEventCenter();

class KGraphHandler extends mxGraphHandler {
    guidesEnabled = true;

    constructor(graph) {
        super(graph);
        this.isCreateEdge = false;
        this.itemLineFrom = null;
        this.edgeCreating = false;

        this.attachItemHighlighted = null
        this.attachItemDom = null

        this.goodItemHightLighted = null;
        this.iconEditLabel = null;

        this.cellsFilter = [];

        this.unSelectionCells = null;
        this.unSelectionCellsAfterMouseUp = null;
        this.clearSelectionWhenShiftDown = false;
        this.multiselected = false;
        this.isMovingCells = false;
        this.isStarted = false;
        this.cellAttachTarget = null;
    }

    /**
     * @author QuanNH
     * @description overide mouseDown of mxGraphHandler
     * @param {object} sender 
     * @param {object} me 
     */
    mouseDown(sender, me) {
        const cellUnderMouse = me.getCell()

        let graph = this.graph;
        let selectedCells = graph.getSelectionCells()

        //---remove line from selection before moving selection---
        if(cellUnderMouse && (selectedCells.includes(cellUnderMouse) || cellUnderMouse instanceof KLineItem)) {
            let selectedLineItem = selectedCells.filter(cell => cell instanceof KLineItem)[0]
            if(selectedLineItem) {
                graph.selectionModel.removeCell(selectedLineItem)
            }
        }
        
        if (!me.isConsumed() && this.isEnabled() && this.graph.isEnabled() &&
    		me.getState() != null && !mxEvent.isMultiTouchEvent(me.getEvent()))
        {
            const cellTarget = this.getInitialCellForEvent(me);
            if (cellTarget.itemType === KItemType.LINK_ITEM) {
                itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_DO_CLOSE);
            }
            
            if (graph.getSelectionCount() > 0 && (cellTarget.itemType === KItemType.ATTACH_ITEM || cellTarget.isAttachFileBtnDownload)) {
                this.cellAttachTarget = cellTarget;
            }

            // return false when click label of edge
            if (graph.getModel().isEdge(cellTarget) && me.evt.target.nodeName === 'DIV') {
                return false;
            } else {
                super.mouseDown(sender, me);
            }
        }
    }

    
    /**
     * @author ThuyTV
     * @description overide mouseMove of mxGraphHandler
     * @param {object} sender 
     * @param {object} me 
     */
    mouseMove(sender, me) {
        let graph = this.graph;
        this.highlightAttachItem(me);
        this.highlightGoodItem(me);
        this.mouseOverIconEditHandler(me);
        this.setEdgeFilter(false);
        if (!me.isConsumed() && graph.isMouseDown && this.cell != null &&
            this.first != null && this.bounds != null && !this.suspended) {
            
            // Stops moving if a multi touch event is received
            // QuanNH add disable drag Edge/ItemLine
            if (mxEvent.isMultiTouchEvent(me.getEvent()) || this.cell.isEdge()) {
                this.reset();
                return;
            }

            this.isMovingCells = true;

            let delta = this.getDelta(me);
            let tol = graph.tolerance;

            if (this.unSelectionCells && this.cells && this.cells.length > 0) {
                this.unSelectionCellsWhenShiftMove(this.unSelectionCells, this.cells);
            }
            
            if (this.shape != null || this.livePreviewActive || Math.abs(delta.x) > tol || Math.abs(delta.y) > tol) {
                // Highlight is used for highlighting drop targets
                if (this.highlight == null) {
                    this.highlight = new mxCellHighlight(this.graph,
                        mxConstants.DROP_TARGET_COLOR, 3);
                }

                let clone = graph.isCloneEvent(me.getEvent()) && graph.isCellsCloneable() && this.isCloneEnabled();
                let gridEnabled = graph.isGridEnabledEvent(me.getEvent());
                let cell = this.cell;
                let hideGuide = true;
                let target = null;
                this.cloning = clone;
                
                if (cell && this.graph.getModel()) {
                    if (cell instanceof KItemBase || cell instanceof KBaseItemLabel || cell instanceof KItem) {
                        const selectedCells = graph.getSelectionCells()
                        for(let i = 0; i < selectedCells.length; i++) {
                            this.graph.getModel().setVisible(selectedCells[i].creatorLabel, false)
                        }
                    }
                }

                // hidden good button when move
                if (graph.isDropEnabled() && this.highlightEnabled) {
                    // Contains a call to getCellAt to find the cell under the mouse
                    target = graph.getDropTarget(this.cells, me.getEvent(), cell, clone);
                }

                let state = graph.getView().getState(target);
                let highlight = false;

                if (state != null && (graph.model.getParent(this.cell) !== target || clone)) {
                    if (this.target !== target) {
                        this.target = target;
                        this.setHighlightColor(mxConstants.DROP_TARGET_COLOR);
                    }

                    highlight = true;
                } else {
                    this.target = null;

                    if (this.connectOnDrop && cell != null && this.cells.length === 1 &&
                        graph.getModel().isVertex(cell) && graph.isCellConnectable(cell)) {
                        state = graph.getView().getState(cell);

                        if (state != null) {
                            let error = graph.getEdgeValidationError(null, this.cell, cell);
                            let color = (error == null) ?
                                mxConstants.VALID_COLOR :
                                mxConstants.INVALID_CONNECT_TARGET_COLOR;
                            this.setHighlightColor(color);
                            highlight = true;
                        }
                    }
                }

                if (state != null && highlight) {
                    this.highlight.highlight(state);
                }
                else {
                    this.highlight.hide();
                }

                if (this.livePreviewActive && clone) {
                    this.resetLivePreview();
                    this.livePreviewActive = false;
                }
                else if (this.maxLivePreview >= this.cells.length && !this.livePreviewActive && this.allowLivePreview) {
                    if (!clone || !this.livePreviewActive) {
                        this.setHandlesVisibleForCells(this.graph.getSelectionCells(), false);
                        this.livePreviewActive = true;
                        this.livePreviewUsed = true;
                    }
                }
                else if (!this.livePreviewUsed && this.shape == null) {
                    this.shape = this.createPreviewShape(this.bounds);
                }

                if (this.guide != null && this.useGuidesForEvent(me)) {
                    const styleDefault = this.graph.getStylesheet().getDefaultVertexStyle();
                    let halfStrokeWidth = styleDefault.strokeWidth/2;
                    if (cell != null) {
                        let stateCell = graph.getView().getState(cell);
                        halfStrokeWidth = (stateCell.style.strokeWidth)/2;
                    }
                   
                    delta = this.guide.move(this.bounds, delta, gridEnabled, clone, halfStrokeWidth);
                    hideGuide = false;
                }
                else {
                    delta = this.graph.snapDelta(delta, this.bounds, !gridEnabled, false, false);
                }

                if (this.guide != null && hideGuide) {
                    this.guide.hide();
                }

                // namnh: hidden event 
                // Constrained movement if shift key is pressed
                // if (graph.isConstrainedEvent(me.getEvent())) {
                //     if (Math.abs(delta.x) > Math.abs(delta.y)) {
                //         delta.y = 0;
                //     }
                //     else {
                //         delta.x = 0;
                //     }
                // }
      
                // THUYTV: giới hạn khu vực kéo thả trong container
                if (this.currentDx !== delta.x || this.currentDy !== delta.y) {
                    const backgroundShape = graph.view.backgroundPageShape;
                    let { strokewidth, vertextStrokeWidth, scale, bounds } = backgroundShape;
                    strokewidth = strokewidth * scale;
                    vertextStrokeWidth = vertextStrokeWidth * scale;

                    const distanceToLeft = (this.bounds.x + delta.x) - bounds.x;
                    const distanceToRight = (bounds.x + bounds.width - strokewidth * 2 - vertextStrokeWidth * 2) - (this.bounds.x + this.bounds.width + delta.x);
                    const distanceToTop = (this.bounds.y + delta.y) - bounds.y;
                    const distanceToBottom = (bounds.y + bounds.height - strokewidth - vertextStrokeWidth) - (this.bounds.y + this.bounds.height + delta.y);

                    if (distanceToLeft <= 0) {
                        delta.x = delta.x - distanceToLeft;
                    } else if (distanceToRight <= 0) {
                        delta.x = delta.x + distanceToRight
                    }


                    if (distanceToTop <= 0) {
                        delta.y = delta.y - distanceToTop;
                    } else if (distanceToBottom <= 0) {
                        delta.y = delta.y + distanceToBottom
                    }

                    this.currentDx = delta.x;
                    this.currentDy = delta.y;
                    
                    this.updatePreview();
                }

                // this.updateItemLabels(this.cell);
            }
            
            this.updateHint(me);
            graph.consumeMouseEvent(mxEvent.MOUSE_MOVE, me);

            // Cancels the bubbling of events to the container so
            // that the droptarget is not reset due to an mouseMove
            // fired on the container with no associated state.
            mxEvent.consume(me.getEvent());
        }
        else if ((this.isMoveEnabled() || this.isCloneEnabled()) && this.updateCursor && !me.isConsumed() &&
            (me.getState() != null || me.sourceState != null) && !graph.isMouseDown) {
            let cursor = graph.getCursorForMouseEvent(me);
            const cellTarget = me.getCell();
             if (cursor == null && graph.isEnabled() && 
                (graph.isCellMovable(cellTarget) || (cellTarget.itemType === KItemType.BASE_ITEM_LABEL && graph.isItemLocked(cellTarget)))) {
                if (graph.getModel().isEdge(cellTarget)) {
                    
                    if( me.evt.target.nodeName === 'DIV'){
                        cursor = 'default';
                    } else {
                        cursor = mxConstants.CURSOR_MOVABLE_EDGE;
                        const cells = [cellTarget];
                        this.setEdgeFilter(true, cells);
                    }
                }
                else {
                    cursor = mxConstants.CURSOR_MOVABLE_VERTEX;
                }
            }

            // Sets the cursor on the original source state under the mouse
            // instead of the event source state which can be the parent
            if (cursor != null && me.sourceState != null) {
                me.sourceState.setCursor(cursor);
            }
        }
    }
    
    /**
     * remove selection cell not move from unSelectionCells
     * @param {*} unSelectionCells 
     * @param {*} moveCells 
     */
    unSelectionCellsWhenShiftMove = (unSelectionCells, moveCells) => {
        let mustUnSelectionCells = [];
        let moveCellIds = [];
        let unSelectionCellIds = [];
        let isMoveOnlyBaseItemLabel = false;

        for (let i = 0; i < moveCells.length; i++) {
            let moveCell = moveCells[i];
            let moveCellId = null;

            if (moveCell.itemType === KItemType.DEFAULT_ITEM || moveCell.itemType === KItemType.BASE_ITEM) {
                moveCellId = moveCell.id
            }
            // else if (moveCell.itemType === KItemType.BASE_ITEM_LABEL) {
            //     moveCellId = moveCell.baseItem.id;
            //     if (moveCells.length === 1 ) {
            //         isMoveOnlyBaseItemLabel = true;
            //     }
            // }
            moveCellIds.push(moveCellId);
        }
       
        for (let j = 0; j < unSelectionCells.length; j++) {
            let unSelectionCell = unSelectionCells[j];
            let unSelectionCellId = null;

            if (unSelectionCell.itemType === KItemType.DEFAULT_ITEM || unSelectionCell.itemType === KItemType.BASE_ITEM) {
                unSelectionCellId = unSelectionCell.id
            }
            // else if (unSelectionCell.itemType === KItemType.BASE_ITEM_LABEL) {
            //     unSelectionCellId = unSelectionCell.baseItem.id;
            // }   

            unSelectionCellIds.push(unSelectionCellId);

            if (isMoveOnlyBaseItemLabel === true || (moveCellIds.length > 0 && moveCellIds.includes(unSelectionCellId) === false)) {
                mustUnSelectionCells.push(unSelectionCell);
            }
        }

        if (mustUnSelectionCells.length) {
            this.graph.removeSelectionCells(mustUnSelectionCells);
        }

        // if (unSelectionCellIds.length === 1 && moveCellIds.length === 1 && unSelectionCellIds[0] === moveCellIds[0]) {
        //     this.graph.removeSelectionCells(unSelectionCells);
        // }

        // move only 1 item
        if (moveCells.length === 1) {
            this.graph.getModel().setVisible(moveCells[0].creatorLabel, false);
        }
        // move only 1 base item
        // if (moveCells.length === 2 && (moveCells[0].itemType === KItemType.BASE_ITEM || moveCells[0].itemType.BASE_ITEM_LABEL)) {
        //     let creatorLabel = moveCells[0].itemType === KItemType.BASE_ITEM_LABEL ? moveCells[0].baseItem.creatorLabel : moveCells[0].creatorLabel ;
        //     this.graph.getModel().setVisible(creatorLabel, false);
        // }
        
        
        this.unSelectionCells = null;
    }

    /**
     * overide moveCells: because baseitemlabel not is child baseitem so add baseitem to cell
     */
    moveCells(cells, dx, dy, clone, target, evt) {
        let moveCells = [];
        for (let i = 0; i < cells.length; i++) {
            let cell = cells[i];
            moveCells.push(cell);
            if (cell.itemType === KItemType.BASE_ITEM && !(this.cell instanceof KItem)) {// not push in case shift down and mouse on an item
                moveCells.push(cell.baseItemLabel);
            }
        }
        super.moveCells(moveCells, dx, dy, clone, target, evt);
    }
     /**
     * get cell for this.cells
     * @param {*} initialCell 
     * ovrride from mxgraphHandler
     */
    getCells(initialCell) {
        if (this.graph.isCellMovable(initialCell) && this.graph.model.isVertex(initialCell)) {
            let isShiftDown = this.graph.isShiftDown;
            let selectedCells = this.graph.getSelectionCells();
            
            // if shift + click
            if (isShiftDown === true) {
                if (selectedCells.length > 0 && initialCell.itemType !== selectedCells[0].itemType) {
                    if (selectedCells[0].itemType === KItemType.LINK_ITEM) {
                        return [];
                    }
                    if (
                        ((selectedCells[0].itemType === KItemType.BASE_ITEM || selectedCells[0].itemType === KItemType.BASE_ITEM_LABEL) && initialCell.itemType === KItemType.DEFAULT_ITEM) || 
                        selectedCells[0].itemType === KItemType.DEFAULT_ITEM
                    ) {
                        return selectedCells;
                    }
                    
                }
     
                // if click on the move + has selection cell
                if (this.delayedSelection === true && selectedCells.length > 0) {
                    let cellmovable = [];
                    // remove initial cell in selection
                    if (initialCell.itemType === KItemType.DEFAULT_ITEM) {
                        for (let i = 0; i < selectedCells.length; i++) {
                            let cell = selectedCells[i];
                            if (cell.id !== initialCell.id) {
                                cellmovable.push(cell);
                            }
                        }

                        if (cellmovable.length === 0) {
                            this.graph.removeSelectionCells([initialCell]);
                            this.clearSelectionWhenShiftDown = true;
                            return [];
                        }

                        this.unSelectionCells = [initialCell];
                    }  else if (initialCell.itemType === KItemType.BASE_ITEM && this.graph.isItemLocked(initialCell) === false ) {
                        for (let i = 0; i < selectedCells.length; i++) {
                            let cellId = null;
                            let cell = selectedCells[i];
                            if (cell.itemType === KItemType.BASE_ITEM) {
                                cellId = cell.id;
                            }
     
                            if (cellId && cellId !== initialCell.id) {
                                cellmovable.push(cell);
                            }
                        }

                        if (cellmovable.length === 0) {
                            this.graph.removeSelectionCells([initialCell, initialCell.baseItemLabel]);
                            this.clearSelectionWhenShiftDown = true;
                            return [];
                        }

                        this.unSelectionCells = [initialCell, initialCell.baseItemLabel];
                    // if initialCell is baseitemlabel: remove initial baseItemLabel and BaseItem 
                    } else if (initialCell.itemType === KItemType.BASE_ITEM_LABEL && this.graph.isItemLocked(initialCell.baseItem) === false) {
                        this.unSelectionCellsAfterMouseUp = [initialCell, initialCell.baseItem];
                        return [initialCell];
                    }
                    
                    if (cellmovable.length > 0) {
                        return cellmovable;
                    } 
                } 
            }

            // nếu có 2 baseitem trở lên, chỉ lấy baseitem trong selection, loại bỏ label
            if ((initialCell.itemType === KItemType.BASE_ITEM && selectedCells.length > 2) && this.graph.isItemLocked(initialCell) === false) {
                let cellmovable = []
                for (let i = 0; i < selectedCells.length; i++) {
                    let cell = selectedCells[i];
                    if (cell.itemType === KItemType.BASE_ITEM) {
                        cellmovable.push(cell);
                    }
                }
                return cellmovable;
            }

            // khi baseitem hoặc label di chuyển thì chỉ di chuyển 
            if (this.delayedSelection) {
                if ((initialCell.itemType === KItemType.BASE_ITEM && selectedCells.length <= 2 && this.graph.isItemLocked(initialCell) === false) ||
                 (initialCell.itemType === KItemType.BASE_ITEM_LABEL && this.graph.isItemLocked(initialCell) === false)) {
                    return [initialCell];
                }
                return selectedCells;
            } else {
                if (initialCell.itemType === KItemType.BASE_ITEM_LABEL && this.graph.isItemLocked(initialCell) === false) {
                    return [initialCell];
                }
                if (selectedCells.length > 1 && initialCell.itemType === KItemType.DEFAULT_ITEM) {
                    return selectedCells; 
                } 
            }

            
        }
        // console.log("initialCell", initialCell)
        if ((initialCell.itemType === KItemType.BASE_ITEM && this.graph.isItemLocked(initialCell) === true) ||
            initialCell.itemType === KItemType.BASE_ITEM_LABEL && this.graph.isItemLocked(initialCell.baseItem) === true ||
            initialCell.itemType === KItemType.ATTACH_ITEM || initialCell.isAttachFileBtnDownload) {
            return [];
        }   

        return super.getCells(initialCell);
    }

    addEditingLabelToStates = (cells) => {
        if (this.isStarted && Array.isArray(cells)) {
            for (let i = 0; i < cells.length; i ++) {
                if (cells[i] instanceof KItemBase) {
                    this.cellCount += this.addStates(cells[i].editingLabel, this.allCells);
                }
            }
        }
    }

    addCreatorLabelToStates = (cells) => {
        if (this.isStarted && Array.isArray(cells)) {
            for (let i = 0; i < cells.length; i ++) {
                if (cells[i] instanceof KItemBase) {
                    this.cellCount += this.addStates(cells[i].creatorLabel, this.allCells);
                } else if (cells[i] instanceof KBaseItemLabel) {
                    this.cellCount += this.addStates(cells[i].baseItem.creatorLabel, this.allCells);
                }
            }
        }
    }

    addEditTextBtnToStates = (cells) => {
        if (this.isStarted && Array.isArray(cells)) {
            for (let i = 0; i < cells.length; i ++) {
                if (cells[i] instanceof KItemBase) {
                    this.cellCount += this.addStates(cells[i], this.allCells);
                } else if (cells[i] instanceof KBaseItemLabel) {
                    this.cellCount += this.addStates(cells[i].baseItem, this.allCells);
                }
            }
        }
    }

    start(cell, x, y, cells){
        this.cell = cell;
        this.first = mxUtils.convertPoint(this.graph.container, x, y);
        this.cells = (cells != null) ? cells : this.getCells(this.cell);
        this.bounds = this.graph.getView().getBounds(this.cells);
        this.pBounds = this.getPreviewBounds(this.cells);
        this.allCells = new mxDictionary();
        this.cloning = false;
        this.cellCount = 0;
        this.multiselected = KGraphUtil.isMultipleSelected(this.graph);
        this.isStarted = true;
        // if move only base item label => no add state other cell
        if (this.cells.length === 1 && this.cells[0].itemType === KItemType.BASE_ITEM_LABEL) {
            this.isStarted = false;
        }
        

        for (let i = 0; i < this.cells.length; i++) {
            this.cellCount += this.addStates(this.cells[i], this.allCells);
        }
        // console.log("this.cellCount", this.cellCount)
        // NAMNH: thêm điều enable guide với những cell là item (có itemtype)
        if (this.guidesEnabled && typeof this.cell.itemType !== undefined && this.cell.itemType) {
            // NAMNH: custom khởi tạo KGuide 
            this.guide = new KGuide(this.graph, this.getGuideStates());
            let parent = this.graph.model.getParent(cell);
            let ignore = this.graph.model.getChildCount(parent) < 2;

            // Uses connected states as guides
            let connected = new mxDictionary();
            let opps = this.graph.getOpposites(this.graph.getEdges(this.cell), this.cell);

            for (let i = 0; i < opps.length; i++) {
                let state = this.graph.view.getState(opps[i]);

                if (state != null && !connected.get(state)) {
                    connected.put(state, true);
                }
            }

            this.guide.isStateIgnored = mxUtils.bind(this, function (state) {
                let p = this.graph.model.getParent(state.cell);
                return state.cell != null && (((!this.cloning &&
                    this.isCellMoving(state.cell)) ||
                    (state.cell !== (this.target || parent) && !ignore &&
                        !connected.get(state) &&
                        (this.target == null || this.graph.model.getChildCount(
                            this.target) >= 2) && p !== (this.target || parent))) ||
                    // NAMNH: loại bỏ những item không cùng loại với item moving 
                    (state.cell.itemType !== this.cell.itemType));
            });
        }
    }

    
    /**
     * [ThuyTV]: Override mxGraphhandler function
     * @param {mxCell} cell
     * @param {mxDictionary} dict 
     */
    addStates(cell, dict) {
        if (cell instanceof KItemBase) {
            super.addStates(cell.baseItemLabel, dict);
            if (!this.allCells.get(cell.iconEditLabel)) {
                if (cell.hasRole) {
                    this.graph.getModel().setVisible(cell.iconEditLabel, true);
                }
            }
        }

        if (!this.allCells.get(cell.selectionBorder)) {
            this.graph.getModel().setVisible(cell.selectionBorder, true);
        }


        return super.addStates(cell, dict);
    }

    reset() {
        // if (typeof this.cell !== "undefined" && this.cell !== null && this.cell.isBaseItemLabel === true) {
            // let baseItemLabel = this.cell;
            // let baseItem = baseItemLabel.baseItem;
            // nếu label bị xóa parent là base item => update lại baseitem là parent cho label
            // if (baseItemLabel.parent && !baseItemLabel.parent.data) {
            //     this.graph.getModel().beginUpdate();
            //     try {
                    // update lại vị trí của label baseitem theo offset của baseitem
                    // let geo = baseItemLabel.geometry.clone();
                    // geo.x = geo.x - baseItem.geometry.x;
                    // geo.y = geo.y - baseItem.geometry.y;

                    // baseItemLabel.setGeometry(geo);
                    // baseItem.insert(baseItemLabel);
            //     } finally {
            //         this.graph.getModel().endUpdate();
            //     }
            // }
        // }

        if (!KGraphUtil.isMultipleSelected(this.graph)) {
            const selectedCells = this.graph.getSelectionCells();
            
            for (let i = 0; i < selectedCells.length; i ++) {
                if (
                    selectedCells[i] instanceof KItemBase && 
                    !this.graph.isCellVisible(selectedCells[i].creatorLabel)
                ) {
                    this.graph.getModel().setVisible(selectedCells[i].creatorLabel, true);
                }
            }
        }

        this.isStarted = false;
        super.reset();
    }
    /**
    * override function mouseUp mxGraphHandler
    */
    mouseUp(sender, me) {
        let selectedCells = this.cells;
        let isCreateEdge = false;
        let isCollision = false;
        let isCreateEdgeWithMultiSelect = false;
        let invalidCellsId = [];

        if (this.isMovingCells) {
            this.isMovingCells = false;
        }

        if (this.delayedSelection === false) {
            selectedCells = this.graph.getSelectionCells();
        }

        if (Array.isArray(selectedCells) && selectedCells.length > 0) {
            let firstCell = selectedCells[0];
            let creatorLabel = null;

            if (!this.multiselected && firstCell) {
                if (firstCell.itemType === KItemType.BASE_ITEM_LABEL) {
                    creatorLabel = firstCell.baseItem.creatorLabel;
                } else {
                    creatorLabel = firstCell.creatorLabel;
                }

                if (creatorLabel && !KGraphUtil.isMultipleSelected(this.graph)) {
                    this.graph.getModel().setVisible(creatorLabel, true);
                }
            }

            if (!this.multiselected || (isCreateEdgeWithMultiSelect && this.multiselected)) {
                for (let i = 0; i < selectedCells.length; i ++) {
                    if (selectedCells[i] instanceof KItem || selectedCells[i] instanceof KBaseItem) {
                        invalidCellsId.push(selectedCells[i].data.id);
                    }
                }
            }
        }


        if (this.cell && invalidCellsId.length > 0 && !(this.cell instanceof KBaseItemLabel)) {
            let source = this.cell;

            const sourceGeo = source.geometry;
            const scale = this.graph.getView().scale;
            const dx = this.roundLength(this.currentDx / scale);
            const dy = this.roundLength(this.currentDy / scale);

            const pannedGeo = new mxRectangle(sourceGeo.x + dx, sourceGeo.y + dy, sourceGeo.width, sourceGeo.height);

            const collisionCells = [];
            
            const cells = Object.values(this.graph.getModel().cells);
            const cellsCount = cells.length;

            for (let i = 0; i < cellsCount; i ++) {
                const cell = cells[i];
                if (
                    (cell instanceof KItem || cell instanceof KBaseItem) && 
                    invalidCellsId.indexOf(cell.data.id) === -1
                ) {
                    if (cell.geometry.isOverlap(pannedGeo)) {
                        collisionCells.push(cell);
                    }
                }
            }

            let tr = this.graph.view.translate;
            const mouseX = Math.round(me.graphX / scale) - tr.x;
            const mouseY = Math.round(me.graphY / scale) - tr.y;
            const mousePos = new mxRectangle(mouseX, mouseY, 1, 1);

            if (collisionCells.length > 0) {
                const collisionCount = collisionCells.length;

                for (let i = 0; i < collisionCount; i++) {
                    const cell = collisionCells[i];
                    
                    if ( cell.itemType !== source.itemType ) {
                        continue;
                    }

                    isCollision = true;

                    if (mousePos.isOverlap(cell.geometry)) {
                        isCreateEdge = true;
                    }

                    //[ThuyTV] Ensure that two collision item doesn't have edge connected with each other
                    if (Array.isArray(source.edges) && isCreateEdge) {
                        for (let j = 0; j < source.edges.length; j++) {
                            const edge = source.edges[j];

                            if (edge.source.data.id === cell.data.id || edge.target.data.id === cell.data.id) {
                                isCreateEdge = false;
                                break;
                            }
                        }
                    }

                    //[ThuyTV] chỉ tạo 1 edge tại một thời điểm
                    if (isCreateEdge) {
                        this.graph.createItemLine(source, cell, source.itemType);
                        this.edgeCreating = true;
                        break;
                    }
                }
            }
        }

        if (isCollision) {
            this.reset();
            this.consumeMouseEvent(mxEvent.MOUSE_UP, me);
        } else {
            super.mouseUp(sender, me);
        }

        if (this.unSelectionCellsAfterMouseUp) {
            this.graph.removeSelectionCells(this.unSelectionCellsAfterMouseUp);
            this.unSelectionCellsAfterMouseUp = null;
        }
    }

    updateItemLabels = (cell) => { 
        if ( [KItemType.DEFAULT_ITEM, KItemType.BASE_ITEM].indexOf(cell.itemType) > -1) {
            if (!this.isCellMoving(cell.editingLabel)) {
                this.addStates(cell.editingLabel, this.allCells);
            }
        }
    }

    /**
     * highlight attach item when mouse hover it 
     */
    highlightAttachItem = (me) => {
        let cell = me.getCell();
        
        // nếu cell có attach file hoặc cell là attach file thì highglight
        if (cell != null && this.graph.model.isVertex(cell) && 
            (cell.itemType === KItemType.DEFAULT_ITEM && cell.itemAttachFile || // cell has attach item
                cell.itemType === KItemType.ATTACH_ITEM || //cell is attach item
                cell.isAttachFileBtnDownload // cell is btn download attach file
            )) {

            // show/hide btn download attach file or highlight attach file is image.
            if (this.attachItemHighlighted === null) {
                let graph = this.graph;
                let state = me.getState();
                if (cell.itemAttachFile) {
                    state = graph.getView().getState(cell.itemAttachFile);
                } 
                
                if (cell.itemType === KItemType.ATTACH_ITEM && cell.isAttachItemImage === true) {
                    state.style = mxUtils.clone(state.style);
                    state.style[mxConstants.STYLE_OPACITY] = 70;
                    if (state.shape) {
                        state.shape.apply(state);
                        state.shape.redraw();
                    }
                } else if (cell.itemType === KItemType.ATTACH_ITEM && cell.isAttachItemImage === false) {
                    this.graph.setCellVisible(cell.attachFileBtnDownload, true);
                } else if (cell.itemType === KItemType.DEFAULT_ITEM && cell.itemAttachFile && cell.itemAttachFile.isAttachItemImage === false) {
                    this.graph.setCellVisible(cell.itemAttachFile.attachFileBtnDownload, true);
                }
                
                this.attachItemHighlighted = state;
            } 

            // highlight btn download attach file
            let btnDownloadState = null;
            let opacity = 65;
            if (cell && cell.isAttachFileBtnDownload) {
                btnDownloadState = this.graph.getView().getState(cell);
                opacity = 40;
            } else {
                if (cell.itemType === KItemType.ATTACH_ITEM && cell.attachFileBtnDownload) {
                    btnDownloadState = this.graph.getView().getState(cell.attachFileBtnDownload);
                } else if (cell.itemType === KItemType.DEFAULT_ITEM && cell.itemAttachFile && cell.itemAttachFile.isAttachItemImage === false) {
                    btnDownloadState = this.graph.getView().getState(cell.itemAttachFile.attachFileBtnDownload);
                }
            }
            if (btnDownloadState) {
                btnDownloadState.style = mxUtils.clone(btnDownloadState.style);
                btnDownloadState.style[mxConstants.STYLE_OPACITY] = opacity;
                if (btnDownloadState.shape) {
                    btnDownloadState.shape.apply(btnDownloadState);
                    btnDownloadState.shape.redraw();
                }
            }

            return;
        }

        if (this.attachItemHighlighted !== null) {
            let state = this.attachItemHighlighted;
            let cell = state.cell;
            if (cell.isAttachItemImage) {
                state.style = mxUtils.clone(state.style);
                state.style[mxConstants.STYLE_OPACITY] = 100;
                if (state.shape) {
                    state.shape.apply(state);
                    state.shape.redraw();
                }
            } else {
                if (cell.itemType === KItemType.DEFAULT_ITEM) {
                    this.graph.setCellVisible(cell.itemAttachFile.attachFileBtnDownload, false);
                } else if (cell.itemType === KItemType.ATTACH_ITEM) {
                    this.graph.setCellVisible(cell.attachFileBtnDownload, false);
                }
            }
            
            this.attachItemHighlighted = null
        }
    }

    highlightGoodItem = (me) => {
        let cell = me.getCell();
        // nếu cell có attach file hoặc cell là attach file thì highglight
        if (cell != null && this.graph.model.isVertex(cell) && cell.isGoodButton === true) {
            if (this.goodItemHightLighted === null) {
                let state = me.getState();
                state.style = mxUtils.clone(state.style);
                state.style[mxConstants.STYLE_OPACITY] = 85;
                state.shape.bounds.width = cell.parent.data.liked ? KGraphUtil.MAX_GOODBUTTON_WIDTH*state.view.scale : cell.parent.getGoodButtonWidth()*state.view.scale;
                if (state.shape) {
                    state.shape.apply(state);
                    state.shape.redraw();
                }

                let goodText = cell.parent.data.liked ? i18n.t('text.stateUndo') : KGraphUtil.getTextGoodButton(cell.parent.data);
                let goodIcon = cell.parent.data.liked ? unLikeIcon : likeIcon;
                state.text.value = KGraphUtil.createGoodButtonLabel(goodIcon, goodText, cell.parent.data.likeCount);
                if (state.text) {
                    state.text.apply(state);
                    state.text.redraw();
                }

                this.goodItemHightLighted = state;
            } 
            return;
        }

        if (this.goodItemHightLighted !== null) {
            let goodItemHightLighted = this.goodItemHightLighted;
            let cell = this.graph.getModel().getCell(goodItemHightLighted.cell.id);
            if (cell.isGoodButton) {
                let state = this.graph.getView().getState(cell);
                state.style = mxUtils.clone(state.style);
                state.style[mxConstants.STYLE_OPACITY] = 100;
                state.shape.bounds.width = cell.parent.getGoodButtonWidth()*state.view.scale;
                if (state.shape) {
                    state.shape.apply(state);
                    state.shape.redraw();
                }
                
                let goodButtonIcon = cell.parent.data.liked ? likedIcon : likeIcon;
                const goodButtonText = KGraphUtil.getTextGoodButton(cell.parent.data);
                state.text.value = KGraphUtil.createGoodButtonLabel(goodButtonIcon, goodButtonText, cell.parent.data.likeCount);
                if (state.text) {
                    state.text.apply(state);
                    state.text.redraw();
                }
                this.goodItemHightLighted = null
            } 
            
        }
    }

    /**
     * @author QuanNH
     * @description set filter shadow for edge
     * @param {Boolean} useFilter 
     * @param {Array} cells 
     */
    setEdgeFilter(useFilter, cells){
        if( cells ){
            this.cellsFilter =  cells;
        }

        if( useFilter ){
            this.graph.setFilterGroupItems(this.cellsFilter, FILTER_EDGE_HOVER);
        } else {
            this.cellsFilter.forEach(cell=>{
                this.graph.removeFilterItem(cell);
            });
            
            this.cellsFilter = [];
            
        }
        
    }
    
    mouseOverIconEditHandler = (me) => {
        const cell = me.getCell();
        if (cell != null && this.graph.model.isVertex(cell) && cell.isIconEditLabel === true) {
            if (this.iconEditLabel === null) {
            
                const state = me.getState();
                state.style = mxUtils.clone(state.style);
                state.style[mxConstants.STYLE_OPACITY] = 80;
                if (state.shape) {
                    state.shape.apply(state);
                    state.shape.redraw();
                }
                this.iconEditLabel = state;
            }
            return;
        }
        if (this.iconEditLabel !== null) {
            let iconEditLabel = this.iconEditLabel;
            let cell = this.graph.getModel().getCell(iconEditLabel.cell.id);
            if (cell && cell.isIconEditLabel) {
                let state = this.graph.getView().getState(cell);
                if( state ){
                    state.style = mxUtils.clone(state.style);
                    state.style[mxConstants.STYLE_OPACITY] = 100;
                    if (state.shape) {
                        state.shape.apply(state);
                        state.shape.redraw();
                    }
                }
                
                this.iconEditLabel = null
            }
        }
    }

}

export default KGraphHandler;