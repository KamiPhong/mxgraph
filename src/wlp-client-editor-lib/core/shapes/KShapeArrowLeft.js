import { mxShape } from "../KClient";

class KShapeArrowLeft extends mxShape {
    constructor(bounds, fill, stroke, strokewidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h) {
        let hatWidth = 30;
        c.moveTo(w, 0);
        c.lineTo(hatWidth, 0);
        c.lineTo(0, h / 2);
        c.lineTo(hatWidth, h);
        c.lineTo(w, h);
        c.lineTo(w, 0);
        c.close();
    }
}


export default KShapeArrowLeft;