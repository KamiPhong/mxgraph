import { mxShape } from "../KClient";

class KShapeHandWritingEclipse extends mxShape {
    constructor(bounds, fill, stroke, strokewidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h) {
        const scaleW = w / 142.7;
        const scaleH = h / 84.15;

        c.moveTo(115.6 * scaleW, 69.05 * scaleH)
        c.quadTo(102.85 * scaleW, 75.15 * scaleH, 80 * scaleW, 80.95 * scaleH)
        c.lineTo(64.7 * scaleW, 84.15 * scaleH)
        c.lineTo(80 * scaleW, 80.45 * scaleH)
        c.quadTo(86.5 * scaleW, 78.8 * scaleH, 94.7 * scaleW, 76.35 * scaleH)
        c.lineTo(59.1 * scaleW, 77.1 * scaleH)
        c.quadTo(57.65 * scaleW, 77.05 * scaleH, 56.2 * scaleW, 77 * scaleH)
        c.quadTo(11.35 * scaleW, 75.3 * scaleH, 3.35 * scaleW, 58.05 * scaleH)
        c.quadTo(-5.9 * scaleW, 38.2 * scaleH, 10.3 * scaleW, 21.45 * scaleH)
        c.quadTo(25.6 * scaleW, 5.7 * scaleH, 53.55 * scaleW, 1.65 * scaleH)
        c.quadTo(85.45 * scaleW, -3 * scaleH, 110.5 * scaleW, 5.45 * scaleH)
        c.quadTo(135.9 * scaleW, 14 * scaleH, 139.55 * scaleW, 31.15 * scaleH)
        c.quadTo(142.7 * scaleW, 43.6 * scaleH, 136.55 * scaleW, 53 * scaleH)
        c.close();
    }
}


export default KShapeHandWritingEclipse;