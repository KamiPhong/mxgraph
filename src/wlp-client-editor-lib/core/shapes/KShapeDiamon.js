import { mxShape } from "../KClient";

class KShapeDiamon extends mxShape {
    constructor(bounds, fill, stroke, strokewidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h) {
        c.moveTo(w / 2, 0);
        c.lineTo(w, h / 2);
        c.lineTo(w / 2, h);
        c.lineTo(0, h / 2);
        c.lineTo(w / 2, 0);
        c.close();
    }
}


export default KShapeDiamon;