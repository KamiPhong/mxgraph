import { mxShape } from "../KClient";

class KShapeHandWritingRect extends mxShape {
    constructor(bounds, fill, stroke, strokewidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h) {
        const scaleW = w/132.6;
        const scaleH = h/64.75;

        c.moveTo(132.6 * scaleW, 21.45 * scaleH)
        c.lineTo(130.95 * scaleW, 64.75 * scaleH)
        c.lineTo(78.55 * scaleW, 66.6 * scaleH)
        c.lineTo(52.3 * scaleW, 65.9 * scaleH)
        c.lineTo(26.15 * scaleW, 67.9 * scaleH)
        c.lineTo(0.2 * scaleW, 67.55 * scaleH)
        c.lineTo(0.0 * scaleW, 43.45 * scaleH)
        c.lineTo(0.55 * scaleW, 19.1 * scaleH)
        c.lineTo(1.9 * scaleW, 1.6 * scaleH)
        c.lineTo(0.1 * scaleW, 1.65 * scaleH)
        c.lineTo(0.1 * scaleW, 1.1 * scaleH)
        c.lineTo(2.0 * scaleW, 1.05 * scaleH)
        c.lineTo(2.05 * scaleW, 0.1 * scaleH)
        c.lineTo(2.4 * scaleW, 0.1 * scaleH)
        c.lineTo(2.3 * scaleW, 1.05 * scaleH)
        c.lineTo(59.0 * scaleW, 0.05 * scaleH)
        c.lineTo(83.65 * scaleW, 0.85 * scaleH)
        c.lineTo(132.8 * scaleW, 0 * scaleH)
        c.lineTo(132.6 * scaleW, 21.45 * scaleH)
        c.close();
    }
}


export default KShapeHandWritingRect;