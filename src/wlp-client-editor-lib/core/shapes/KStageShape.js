import { mxConstants, mxUtils, mxShape, mxEvent } from "wlp-client-editor-lib/core/KClient";

class KStageShape extends mxShape {
    constructor(bounds, fill, innerStroke, innerStrokeWidth, outerStroke, outerStokeWidth, vertextStrokeWidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = innerStroke;
        this.strokewidth = (innerStrokeWidth != null) ? innerStrokeWidth : 1;
        this.outerStroke = outerStroke;
        this.outerStokeWidth = outerStokeWidth;
        this.vertextStrokeWidth = vertextStrokeWidth;
        this.backgroundPageNode = null;

        this.gestureStart = null;
        this.gestureChange = null;
        this.gestureEnd = null;
    }


    paintBackground = (c, x, y, w, h) => {
        let events = true;

        if (this.style != null) {
            events = mxUtils.getValue(this.style, mxConstants.STYLE_POINTER_EVENTS, '1') === '1';
        }

        if (events || (this.fill != null && this.fill !== mxConstants.NONE) ||
            (this.stroke != null && this.stroke !== mxConstants.NONE)) {
            if (!events && (this.fill == null || this.fill === mxConstants.NONE)) {
                c.pointerEvents = false;
            }

            const vertexStrokeWD2 = this.vertextStrokeWidth/2;
            const strokeWD2 = this.strokewidth/2;
            x = x - strokeWD2 - vertexStrokeWD2;
            y = y - strokeWD2 - vertexStrokeWD2;
            w = w - this.strokewidth;
            h = h + vertexStrokeWD2;
            
            

            const borderStrokeWidthD2 = this.outerStokeWidth/2;
            const br = {//border rect
                x: x - borderStrokeWidthD2 - strokeWD2,
                y: y - borderStrokeWidthD2 - strokeWD2,
                w: w + this.strokewidth + borderStrokeWidthD2 * 2,
                h: h + this.strokewidth + borderStrokeWidthD2 * 2
            }
            const s = c.state;
            if (!this.backgroundPageNode) {
                this.backgroundPageNode = c.createElement('rect');
                mxEvent.addGestureListeners(this.backgroundPageNode, this.gestureStart, this.gestureChange, this.gestureEnd);
            }

            this.backgroundPageNode.setAttribute('x', c.format((br.x + s.dx) * s.scale));
            this.backgroundPageNode.setAttribute('y', c.format((br.y + s.dy) * s.scale));
            this.backgroundPageNode.setAttribute('width', c.format(br.w * s.scale));
            this.backgroundPageNode.setAttribute('height', c.format(br.h * s.scale));
            this.backgroundPageNode.setAttribute('fill', 'none');
            this.backgroundPageNode.setAttribute('stroke', this.outerStroke);
            this.backgroundPageNode.setAttribute('pointer-events', 'all');
            this.backgroundPageNode.setAttribute('stroke-width', this.outerStokeWidth);

            c.rect(x, y, w, h);
            c.fillAndStroke();
            c.root.appendChild(this.backgroundPageNode);
            c.root.setAttribute('backDrop', '1');
            c.fillAndStroke();
        }
    }

    addGestureListener = (start, change, end) => {
        if (typeof start === 'function') {
            this.gestureStart = start;
        }
        if (typeof change === 'function') {
            this.gestureChange = change;
        }
        if (typeof end === 'function') {
            this.gestureEnd = end;
        }
    }

}

export default KStageShape;