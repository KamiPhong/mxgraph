import { mxShape } from "../KClient";

class KBaseTriangle extends mxShape {
    constructor(bounds, fill, stroke, strokewidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h) {
        c.moveTo(0,h);
        c.lineTo(0.5*w, 0);
        c.lineTo(w, h);
        c.close();
    }
}


export default KBaseTriangle;