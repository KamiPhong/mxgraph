import { mxShape } from "wlp-client-editor-lib/core/KClient";

class KPrintRangeGuide extends mxShape {
    constructor(bounds, fill, stroke, strokewidth, triangleWidth, triangleHeight, rangeWidth, rangeHeight) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
        this.triangleWidth = triangleWidth;
        this.triangleHeight = triangleHeight;
        this.rangeWidth = rangeWidth;
        this.rangeHeight = rangeHeight;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();

    }

    redrawPath(c, x, y, w, h) {
        const sw = this.strokewidth/2;
        c.moveTo(0, 0);
        c.lineTo(w, 0);

        this.drawRangeLine(c, x, y, w, h, 0);
        this.drawRangeLine(c, x, y, w, h, 1);
        this.drawRangeLine(c, x, y, w, h, 2);

        c.moveTo(0, h + sw);
        c.lineTo(0, 0 - sw);
        c.close();
    }

    drawRangeLine = (c, x, y, w, h, line) => {
        const tw = this.triangleWidth;
        const th = this.triangleHeight;
        const sw = this.strokewidth/2;
        let fw = this.rangeWidth*line;
        let fh = this.rangeHeight*line
        let wr = w - fw;
        let hr = h - fh;

        c.moveTo(wr, 0 - sw);
        c.lineTo(wr, hr - th + sw/5);
        c.moveTo(wr, hr - th - sw);
        c.lineTo(wr - tw, hr - th - sw);
        c.moveTo(wr - tw + sw, hr - th - sw);
        c.lineTo(wr - tw + sw, hr - sw*2);
        c.moveTo(wr, hr - th - sw/2);
        c.lineTo(wr - tw + sw, hr - sw*2);
        c.moveTo(wr - tw + sw*2, hr - sw*2);
        c.lineTo(0, hr);

    }

}

export default KPrintRangeGuide;