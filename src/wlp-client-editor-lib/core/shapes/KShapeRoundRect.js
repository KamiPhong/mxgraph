import { mxShape } from "../KClient";

class KShapeRoundRect extends mxShape {
    constructor(bounds, fill, stroke, strokewidth){
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h){
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h){
        let roundFactor = 5;

        c.moveTo(0, roundFactor); 
        c.lineTo(0, h - roundFactor);
        c.quadTo(0, h, roundFactor, h);
        c.lineTo(w - roundFactor, h);
        c.quadTo(w, h, w, h - roundFactor);
        c.lineTo(w, roundFactor);
        c.quadTo(w, 0, w - roundFactor, 0);
        c.lineTo(roundFactor, 0);
        c.quadTo(0, 0, 0, roundFactor);
        c.close();
    }
}


export default KShapeRoundRect;