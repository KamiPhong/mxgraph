import { mxCylinder, mxUtils } from "wlp-client-editor-lib/core/KClient";

class KTailedRect extends mxCylinder {
    constructor(bounds, fill, stroke, strokewidth = '#b2b2b2', tail = 1.4, padding = 8) {
        super(bounds, fill, stroke, strokewidth);
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = strokewidth != null ? strokewidth : 1;
        this.padding = padding;
        this.tail = tail;
    }

    redrawPath(path, x, y, w, h, isForeground) {
        w = w + this.padding;
        h = h + this.padding;

        if (isForeground) {
            path.moveTo(0, -this.tail);
            path.lineTo(0, h + this.tail);
            path.moveTo(-this.tail, h);
            path.lineTo(w + this.tail, h);
            path.moveTo(w, h + this.tail);
            path.lineTo(w, -this.tail);
            path.moveTo(w + this.tail, 0);
            path.lineTo(-this.tail, 0);
            path.close();
        }
    }

    redraw() {
        const prevStrokeWidth = this.strokewidth;
        const currentPadding = this.padding;
        const currentTail = this.tail;

        this.bounds.x = this.bounds.x - (this.padding * this.scale / 2);
        this.bounds.y = this.bounds.y - (this.padding * this.scale / 2);

        const resizeUnit = 1.5;
        this.bounds.x -= resizeUnit;
        this.bounds.y -= resizeUnit;
        this.bounds.width += resizeUnit*2;
        this.bounds.height += resizeUnit*2

        super.redraw();
        // this.redrawTools();

        this.strokewidth = prevStrokeWidth;
        this.padding = currentPadding;
        this.tail = currentTail;
    }


    //TODO: [ThuyTv] Refactor context menu

    redrawTools = () => {
        let canvas = this.createCanvas();
        if (canvas) {
            // Specifies if events should be handled
            canvas.pointerEvents = this.pointerEvents;
            //TODO: [ThuyTV] make context menu work
            if (this.node !== canvas.root) {
                // Forces parsing in IE8 standards mode - slow! avoid
                this.node.insertAdjacentHTML(
                    "beforeend",
                    canvas.root.outerHTML
                );
            }

            if (this.node.nodeName === "DIV" && document.documentMode === 8) {
                // Makes DIV transparent to events for IE8 in IE8 standards
                // mode (Note: Does not work for IE9 in IE8 standards mode
                // and not for IE11 in enterprise mode)
                this.node.style.filter = "";

                // Adds event transparency in IE8 standards
                mxUtils.addTransparentBackgroundFilter(this.node);
            }

            this.destroyCanvas(canvas);
        }
    };
}

export default KTailedRect;
