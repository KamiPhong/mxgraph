import { mxShape } from "../KClient";

class KShapeCloud extends mxShape {
    constructor(bounds, fill, stroke, strokewidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }


    //[ThuyTv] override redrawpath 
    //create cloud shape
    redrawPath(c, x, y, w, h) {
        const scaleW = w / 137.75;
        const scaleH = h / 83.4;

        c.moveTo(87.55 * scaleW, 0.9 * scaleH)
        c.quadTo(94.5 * scaleW, 2.3 * scaleH, 100.45 * scaleW, 6.95 * scaleH)
        c.quadTo(103.45 * scaleW, 9.25 * scaleH, 105 * scaleW, 11.3 * scaleH)
        c.lineTo(109.35 * scaleW, 11.8 * scaleH)
        c.quadTo(114.65 * scaleW, 12.65 * scaleH, 119.2 * scaleW, 14.55 * scaleH)
        c.quadTo(133.8 * scaleW, 20.55 * scaleH, 135.8 * scaleW, 34.35 * scaleH)
        c.quadTo(137.75 * scaleW, 48.15 * scaleH, 127.3 * scaleW, 59 * scaleH)
        c.quadTo(124 * scaleW, 62.4 * scaleH, 119.85 * scaleW, 65.1 * scaleH)
        c.lineTo(116.4 * scaleW, 67.1 * scaleH)
        c.quadTo(116 * scaleW, 68.6 * scaleH, 113.5 * scaleW, 70.7 * scaleH)
        c.quadTo(108.6 * scaleW, 74.95 * scaleH, 98.45 * scaleW, 78.05 * scaleH)
        c.quadTo(88.3 * scaleW, 81.15 * scaleH, 80 * scaleW, 79.95 * scaleH)
        c.quadTo(77.45 * scaleW, 79.55 * scaleH, 75.3 * scaleW, 78.8 * scaleH)
        c.lineTo(73.75 * scaleW, 78.1 * scaleH)
        c.quadTo(71.15 * scaleW, 79.5 * scaleH, 67.1 * scaleW, 80.8 * scaleH)
        c.quadTo(59.05 * scaleW, 83.4 * scaleH, 51.9 * scaleW, 82.85 * scaleH)
        c.quadTo(43.05 * scaleW, 82.15 * scaleH, 35.6 * scaleW, 76.95 * scaleH)
        c.quadTo(33.25 * scaleW, 75.3 * scaleH, 31.35 * scaleW, 73.4 * scaleH)
        c.lineTo(29.9 * scaleW, 71.85 * scaleH)
        c.quadTo(24.15 * scaleW, 71.9 * scaleH, 17.7 * scaleW, 69.6 * scaleH)
        c.quadTo(4.75 * scaleW, 64.95 * scaleH, 1.2 * scaleW, 53.2 * scaleH)
        c.quadTo(-2.35 * scaleW, 41.4 * scaleH, 4.65 * scaleW, 31.45 * scaleH)
        c.quadTo(8.2 * scaleW, 26.5 * scaleH, 12.4 * scaleW, 23.85 * scaleH)
        c.quadTo(12.6 * scaleW, 23.1 * scaleH, 13.15 * scaleW, 21.85 * scaleH)
        c.quadTo(14.25 * scaleW, 19.35 * scaleH, 16 * scaleW, 16.95 * scaleH)
        c.quadTo(21.55 * scaleW, 9.2 * scaleH, 31.5 * scaleW, 5.15 * scaleH)
        c.quadTo(41.4 * scaleW, 1.05 * scaleH, 53.4 * scaleW, 2.05 * scaleH)
        c.quadTo(59.4 * scaleW, 2.55 * scaleH, 63.4 * scaleW, 3.85 * scaleH)
        c.lineTo(64.95 * scaleW, 2.95 * scaleH)
        c.quadTo(67.05 * scaleW, 1.9 * scaleH, 69.6 * scaleW, 1.2 * scaleH)
        c.quadTo(77.75 * scaleW, -1.05 * scaleH, 87.55 * scaleW, 0.9 * scaleH)
        c.close();
    }
}


export default KShapeCloud;