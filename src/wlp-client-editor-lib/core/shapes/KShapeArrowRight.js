import { mxShape } from "../KClient";

class KShapeArrowRight extends mxShape {
    constructor(bounds, fill, stroke, strokewidth){
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
    }

    paintVertexShape(c, x, y, w, h){
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h){
        let hatWidth = 30; //hat always equal 15% of width
        w = w - hatWidth;
        c.moveTo(0, 0); //move penceil to top-left corner
        c.lineTo(0, h); //draw straight line from top-left to bottom left
        c.lineTo(w, h);
        c.lineTo(w+hatWidth, h/2);
        c.lineTo(w, 0);
        c.lineTo(0, 0);
        c.close();
    }
}


export default KShapeArrowRight;