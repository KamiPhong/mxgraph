import { mxShape } from "../KClient";

class KSelectionBorder extends mxShape {
    constructor(bounds, fill, stroke, strokewidth) {
        super();
        this.bounds = bounds;
        this.fill = fill;
        this.stroke = stroke;
        this.strokewidth = (strokewidth != null) ? strokewidth : 1;
        this.tail = 5;
    }

    paintVertexShape(c, x, y, w, h) {
        c.translate(x, y);
        c.begin();
        this.redrawPath(c, x, y, w, h);
        c.fillAndStroke();
    }

    redrawPath(c, x, y, w, h) {
        c.moveTo(0, -this.tail);
        c.lineTo(0, h + this.tail);
        c.moveTo(-this.tail, h);
        c.lineTo(w + this.tail, h);
        c.moveTo(w, h + this.tail);
        c.lineTo(w, -this.tail);
        c.moveTo(w + this.tail, 0);
        c.lineTo(-this.tail, 0);
        c.close();
    }
}


export default KSelectionBorder;