const ItemLineWeightType = {
    /** <p>未定義</p> */
    NONE : -1,
    /** <p>細</p> */
    LINE_WEIGHT_SMALL  : 1,
    /** <p>中</p> */
    LINE_WEIGHT_MIDDLE : 2,
    /** <p>太</p> */
    LINE_WEIGHT_LARGE  : 4
}

export default ItemLineWeightType;