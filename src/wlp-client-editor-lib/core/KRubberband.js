import { mxRubberband, mxRectangle,  mxUtils, mxConstants, mxEvent, mxMouseEvent, mxClient } from "wlp-client-editor-lib/core/KClient";
import ItemMenuButtonBarHandler from 'wlp-client-editor-lib/handlers/ItemMenuButtonBarHandler';
import KItemType from "./KItemType";
import SheetPullDownListEventCenter from 'wlp-client-editor-lib/events/SheetPullDownListEventCenter';
import ActionLogger from "wlp-client-common/ActionLogger";
import KBaseItemLabel from "./KBaseItemLabel";
import KBaseItem from "./KBaseItem";
const sheetPullDownListEventCenter = new SheetPullDownListEventCenter();

// import KItem from './KItem'
class KRubberband extends mxRubberband {

    constructor(graph) {
        super(graph);

        this.onMouseDown = null;
        this.itemMenuButtonBarHandler = new ItemMenuButtonBarHandler(graph);
        this.selectX = 0;
        this.selectY = 0;
        this.selectWidth = 0;
        this.selectHeight = 0;

        this.init();

        this.itemType = null;
        this.createRubberbandOnBaseitem = false;

        // [ThuyTV]: workaround for rubberband on touch devices
        // Panning should be stopped to avoid conflict
        this.graph.addListener(mxEvent.TAP_AND_HOLD, (sender, evt) => {
            const mouseEvt = evt.getProperty('event');
            this.mouseDown(this.graph, new mxMouseEvent(mouseEvt, this.graph.getStateForTouchEvent(mouseEvt)));
        });
    }

    init = () => {
        this.itemMenuButtonBarHandler.hide();
        // TODO: hot fix to do refactor item menu 
        this.graph.container.getElementsByClassName('item-context-menu').forEach(elm => {
            elm.style.display = 'none';
        });
    }

    mouseDown = (sender, me) => {
        ActionLogger.click('User_Editor_SheetLinkView', 'Select_Many_Item');
        // case shift + mouse down on baseitem lock
        if (me.state && 
            me.state.cell &&
            (me.state.cell.itemType === KItemType.BASE_ITEM && this.graph.isItemLocked(me.state.cell) ||
                me.state.cell.itemType === KItemType.BASE_ITEM_LABEL && this.graph.isItemLocked(me.state.cell.baseItem))  && 
            this.graph.isShiftDown &&
            this.isEnabled() && 
            this.graph.isEnabled()) {
                this.handleMouseDown(me);
                return;
        }

        //Fix bug khi locksheet thì không Save the selected section as PNG format.
        if(this.graph.isShiftDown && this.isEnabled()) {
            if (!this.graph.isEnabled()) {
                this.itemMenuButtonBarHandler.hide();
                this.graph.setCellsLocked(true);

                this.handleMouseDown(me);
                return;
            }
        }

        if (this.graph.getSelectionCells() === 0) {
            this.itemMenuButtonBarHandler.hide();
        }

        this.graph.orderCells(false,this.graph.getSelectionCells());

        if (this.onMouseDown && typeof (this.onMouseDown) === 'function') {
            this.onMouseDown(sender, me);
        }

        if (!me.state || 
            (me.state && me.state.cell && this.graph.isItemLocked(me.state.cell))
        ) {
            
            //NOTE: [ThuyTV] dirty-hands
            //put graph clear selection in the end of callstack
            //it will cause some trouble if this function being in the front of callstack
            setTimeout(() => this.graph.clearSelection(), 0);
            if (this.graph.isShiftDown || (mxClient.IS_TOUCH && this.graph.tapAndHoldValid)) {
                super.mouseDown(sender, me);
            }
        }
    }

    handleMouseDown = (me) => {
        this.createRubberbandOnBaseitem = true;
        const cellUnderMouse = me.getCell()
        if(!(cellUnderMouse instanceof KBaseItemLabel) && !(cellUnderMouse instanceof KBaseItem)) {
            setTimeout(() => {
                this.graph.clearSelection()
            }, 0);
        }

        let offset = mxUtils.getOffset(this.graph.container);
        let origin = mxUtils.getScrollOrigin(this.graph.container);
        origin.x -= offset.x;
        origin.y -= offset.y;
        this.start(me.getX() + origin.x, me.getY() + origin.y);
        me.consume(false);
    }
    
    mouseMove = (sender, me) => {
        if (this.first != null) {
            if (this.createRubberbandOnBaseitem == true) {
                
                let origin = mxUtils.getScrollOrigin(this.graph.container);
                let offset = mxUtils.getOffset(this.graph.container);
                origin.x -= offset.x;
                origin.y -= offset.y;
                let x = me.getX() + origin.x;
                let y = me.getY() + origin.y;
                let dx = this.first.x - x;
                let dy = this.first.y - y;
                let tol = this.graph.tolerance;
                
                if (this.div != null || Math.abs(dx) > tol ||  Math.abs(dy) > tol) {
                    if (this.div == null) {
                        this.div = this.createShape();
                    }
                    
                    mxUtils.clearSelection();
                    
                    this.update(x, y);
                    me.consume();
                }
            } else {
                super.mouseMove(sender, me);
            }

            this.selectX = this.x;
            this.selectY = this.y;
            this.selectWidth = this.width;
            this.selectHeight = this.height;

            if (this.graph.savePngBySelected === false) {
                let cells = this.getCellInDiv(this.selectX, this.selectY, this.selectWidth, this.selectHeight);
                if (this.itemType === null && cells.length) {
                    this.itemType = cells[0].itemType;
                }

                if (cells.length === 1) {
                    this.itemType = cells[0].itemType;
                } else if( cells.length > 1){
                    cells = cells.filter((cell)=>{
                        return cell.itemType === this.itemType
                    });
                }

                if (this.graph.isEnabled() && cells.length > 0) {
                    this.itemMenuButtonBarHandler.redrawTools(cells);
                } else {
                    this.itemMenuButtonBarHandler.hide();
                } 
            }
        }
    }

    getCellInDiv = (x, y, width, height, parent, result) => {
        result = (result != null) ? result : [];

        if (width > 0 || height > 0) {
            const model = this.graph.getModel();
            if (parent == null) {
                parent = this.graph.getCurrentRoot();

                if (parent == null) {
                    parent = model.getRoot();
                }
            }

            if (parent != null) {
                const childCount = model.getChildCount(parent);
                for (let i = 0; i < childCount; i++) {
                    const cell = model.getChildAt(parent, i);
                    const state = this.graph.view.getState(cell);

                    if (state != null && this.graph.isCellVisible(cell) && !this.graph.isItemLocked(cell) && cell.itemType !== null) {
                        const deg = mxUtils.getValue(state.style, mxConstants.STYLE_ROTATION) || 0;
                        let box = state;

                        if (deg !== 0) {
                            box = mxUtils.getBoundingBox(box, deg);
                        }

                        const rect1 = new mxRectangle(x, y, width, height);
                        const rect2 = new mxRectangle(box.x, box.y, box.width, box.height);

                        if (rect1.x < rect2.x + rect2.width &&
                            rect1.x + rect1.width > rect2.x &&
                            rect1.y < rect2.y + rect2.height &&
                            rect1.y + rect1.height > rect2.y &&
                            (cell.itemType === KItemType.DEFAULT_ITEM || cell.itemType === KItemType.BASE_ITEM) && 
                            cell.data && cell.data.editing != true
                            ) {
                            result.push(cell)
                        } else {
                            this.getCellInDiv(x, y, width, height, cell, result);
                        }
                    }
                }
            }
        }
        return result;
    }

    mouseUp(sender, me) {
        super.mouseUp(sender, me)

        if (super.isActive()) {
            this.resetSelect();
        }
    }

    /**
     * change color div selected
     */
    repaint() {
        super.repaint();
        if (this.div != null) {
            let color = this.graph.savePngBySelected === true ? "#aed6b0" : "#d9a9c5";
            this.div.style.background = color;
            this.div.style.borderColor = color
        }
    }

    execute(evt){
        if (this.graph.savePngBySelected === true) {
            const bounds = new mxRectangle(this.selectX, this.selectY, this.selectWidth, this.selectHeight);
            const scale = this.graph.view.scale;
            sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG_BY_SELECTED, bounds, scale);
            return;
        } 
        
        let cells = this.getCellInDiv(this.selectX, this.selectY, this.selectWidth, this.selectHeight);
        const itemTypesAllowSelect = [KItemType.DEFAULT_ITEM, KItemType.BASE_ITEM];

        cells = cells.filter((cell)=>{
            return cell.itemType === this.itemType && itemTypesAllowSelect.indexOf(cell.itemType) > -1;
        });
     
        if (cells.length <= 1) {
            if (cells.length === 1) {
                this.graph.selectCellsForEvent(cells, evt);
            }
            return this.itemMenuButtonBarHandler.hide();
        }

        this.graph.selectCellsForEvent(cells, evt);
        this.itemType = null;
        
    }

    resetSelect = () => {
        this.selectX = 0;
        this.selectY = 0;
        this.selectWidth = 0;
        this.selectHeight = 0;
    }

    reset() {
        // this.itemType = null;
        super.reset();
    }
    destroy() {
        if (!this.destroyed) {
            this.createRubberbandOnBaseitem = false;
        }
        super.destroy();
    }
}

export default KRubberband;