import { getRandomInt } from "wlp-client-common/utils/NumericUtils";
import { mxPanningHandler, mxEvent, mxEventObject, mxPoint, mxClient } from "wlp-client-editor-lib/core/KClient";

class KPanningHandler extends mxPanningHandler {
    disableOnShiftDown = true;

    constructor(graph) {
        super();
        if (graph != null) {
            this.graph = graph;
            this.graph.addMouseListener(this);

            this.graph.addListener(mxEvent.FIRE_MOUSE_EVENT, this.forcePanningHandler);
            
            // If user is press and hold finger on touch devices
            // Panning should be stop and start Rubberband for multiple selection
            this.graph.addListener(mxEvent.TAP_AND_HOLD, () => {
                if (this.active) {
                    this.reset();
                }
            });

            // Stops scrolling on every mouseup anywhere in the document
            mxEvent.addListener(document, 'mouseup', this.mouseUpListener);
        }
    }

    forcePanningHandler = (sender, evt) => {
        const evtName = evt.getProperty('eventName');
        const me = evt.getProperty('event');
        if (evtName === mxEvent.MOUSE_DOWN && this.isForcePanningEvent(me)) {
            this.start(me);
            this.active = true;
            this.fireEvent(new mxEventObject(mxEvent.PAN_START, 'event', me));
            me.consume();
        }
    }

    isForcePanningEvent(me){
        if (me.state && 
            me.state.cell &&
            me.state.view &&
            me.state.view.graph) {
            if (typeof(me.state.view.graph.isEnabled) === 'function' &&
                me.state.view.graph.isEnabled() === false) {
                return true;
            }

            if (typeof(me.state.view.graph.isCellLocked) === 'function' &&
                me.state.view.graph.isCellLocked(me.state.cell) &&
                this.graph.isShiftDown === false) {
                return true;
            }
        } else if (mxClient.IS_TOUCH) {
            return true;
        }

        return super.isForcePanningEvent(me);
    }

    mouseDown(sender, me){
        if (this.disableOnShiftDown && this.graph.isShiftDown) {
            return;
        }

        if (this.graph.getSelectionCount() > 0) {
            super.mouseDown(sender, me)
        } else {
             //setTimeout is for preventing the mousedown event being consume
            //if the event be consumed, it will cause all other mousedown listener stop being trigger
            setTimeout(() => super.mouseDown(sender, me), 0);
        }
    }


    /**
     * NOTE: [ThuyTV] Override mxPanningHandler
     * @param {*} sender 
     * @param {*} me 
     */
    mouseMove(sender, me) {
        if (this.active) {
            let panPosition = this.computePanActive(me);
            this.dx = panPosition.x;
            this.dy = panPosition.y;

            if (this.previewEnabled) {
                this.graph.panGraph(this.dx + this.dx0, this.dy + this.dy0);
            }

            this.fireEvent(new mxEventObject(mxEvent.PAN, 'event', me));
        } else if (this.panningTrigger) {
            const panPosition = this.computePanTrigger(me);
            this.dx = panPosition.x;
            this.dy = panPosition.y;
            const tmp = this.active;

            // Panning is activated only if the mouse is moved
            // beyond the graph tolerance
            this.active = Math.abs(this.dx) > this.graph.tolerance || Math.abs(this.dy) > this.graph.tolerance;

            if (!tmp && this.active) {
                this.fireEvent(new mxEventObject(mxEvent.PAN_START, 'event', me));
            }
        }

        if (this.active || this.panningTrigger) {
            this.graph.clearTimeoutTheard();
            me.consume();
        }
    }

    computePanActive = (me) => {
        let output = new mxPoint(this.dx, this.dy);
        if (this.active) {
            let dx = me.getX() - this.startX;
            let dy = me.getY() - this.startY;

            // Applies the grid to the panning steps
            if (this.useGrid) {
                dx = this.graph.snap(dx);
                dy = this.graph.snap(dy);
            }

            const panLimit = this.compoutePanningLimit(dx, dy);
            output.x = panLimit.x;
            output.y = panLimit.y;
        }

        return output;
    }

    computePanTrigger = (me) => {
        let output = new mxPoint(this.dx, this.dy);
        
        if (this.panningTrigger) {
            output.x = me.getX() - this.startX;
            output.y = me.getY() - this.startY;
        }

        return output;
    }

    compoutePanningLimit = (dx, dy) => {
        const tr = this.graph.getView().getTranslate();
        const scale = this.graph.getView().getScale();
        //limit top
        const top = (tr.x * scale + dx) + this.graph.pageFormat.width * scale;
        const left = (tr.y * scale + dy) + this.graph.pageFormat.height * scale;

        const offsetLimit = getRandomInt(155, 165);
        const right = (tr.x * scale + dx) - (this.graph.container.offsetWidth - offsetLimit);
        const bottom = (tr.y * scale + dy) - (this.graph.container.offsetHeight - offsetLimit);

        if (right > 0) {
            dx -= right;
        }

        if (top < offsetLimit) {
            dx += offsetLimit - top;
        }

        if (bottom > 0) {
            dy -= bottom;
        }

        if (left < offsetLimit) {
            dy += offsetLimit - left;
        }

        return new mxPoint(dx, dy);
    }

    mouseUpListener = (evt) => {
        if (this.active) {
            this.reset();
        }
    }
}

export default KPanningHandler;