import ItemShape from './KGraphItemShape';
import ItemStyle from './KItemStyle';
import { mxGeometry, mxConstants, mxImageExport, mxUtils, mxSvgCanvas2D, mxRectangle, mxClient } from 'wlp-client-editor-lib/core/KClient';
import { isImageFile, getLargeIcon } from 'wlp-client-editor-lib/utils/ResourceThumbnailUtils';
import KConstants from './KConstants';
import KItemTextAlign from './KItemTextAlign';
import KItemTextVerticalAlign from './KItemTextVerticalAlign';
import KItemLineArrowType from './KItemLineArrowType';
import KItemType from './KItemType';
import KGraphEdgeShape from './KGraphEdgeShape';
import { getLineColor } from './KItemLineColor';
import { detect } from 'detect-browser';
import i18n from "i18n";
import imgContextDownload from 'wlp-client-editor-lib/assets/images/context-download.png'
import { getStringPerimeterFromShape } from "./perimeter/KPerimeterShape";
import { ITEM_COLORS } from "wlp-client-editor-lib/core/KGraphConfig";


const imgEditText = "/editor/edit-text.png";
const imgResizeSmall = "/editor/icon-resize-small-transparent.png";
// const imgContextDownload = "/editor/context-download.png";

const ITEM_STROKE_WIDTH = 1;
let DEFAULT_ITEM_SHAPE = ItemShape.RECT;

// default with inner stroke
let DEFAULT_ITEM_WIDTH = 120 - ITEM_STROKE_WIDTH;
let DEFAULT_ITEM_HEIGHT = 60 - ITEM_STROKE_WIDTH;

let MAX_ITEM_WIDTH = 500;
let MAX_ITEM_HEIGHT = 500;


let DEFAULT_BASE_ITEM_WIDTH = 100;
let DEFALT_BASE_ITEM_HEIGHT = 50;
let MAX_BASE_ITEM_WIDTH = 4500;
let MAX_BASE_ITEM_HEIGHT = 3700;
let MIN_BASE_ITEM_WIDTH = 165;
let MIN_BASE_ITEM_HEIGHT = 110;

let MAX_BASE_ITEM_LABEL_WIDTH = 4500;
let MAX_BASE_ITEM_LABEL_HEIGHT = 3700;
let MIN_BASE_ITEM_LABEL_WIDTH = 100;
let MIN_BASE_ITEM_LABEL_HEIGHT = 50;


let MAX_FONT_SIZE = 40;
let MIN_FONT_SIZE = 4;
let MAX_BASE_ITEM_FONT_SIZE = 40;
let DEFAULT_FONT_SIZE = 15;

let DEFAULT_LINE_WEIGHT = 1;

let ATTACH_ITEM_WIDTH = 90;
let ATTACH_ITEM_HEIGHT = 67.5;

let EDITING_USER_LABEL_OFFSET_BOTTOM = -22.5;
let GOOD_BUTTON_MARGIN = 2;
let CREATE_USER_LABEL_OFFSET_TOP = 39.5;

let DEFAULT_ARROW_WIDTH = 15;
let ARROW_TAIL_LENGTH = 16;
let MAIN_LINE_HALF_WIDTH = 32;

let DEFAULT_CURSOR = 'default';

let MAX_GOODBUTTON_WIDTH = 80;
let MIN_GOODBUTTON_WIDTH = 60;

let SHEET_PNG_PADDING = 10;

const EDIT_CREATER_LABEL_MARGIN_LEFT = -3.2;

const CONTEXT_MENU_MOVE_LEFT = 1;
const CONTEXT_MENU_MOVE_TOP = 9;

const BUTTON_DOWNLOAD_ATTACH_ITEM_SIZE = 28;
const SELECTION_BORDER_PADDING = 10;
const getModelTextSize = (itemModel) => {
    if (itemModel.textSize < MIN_FONT_SIZE) {
        return MIN_FONT_SIZE
    }

    if (itemModel.itemType === KItemType.BASE_ITEM) {
        if (itemModel.textSize > MAX_BASE_ITEM_FONT_SIZE) {
            return MAX_BASE_ITEM_FONT_SIZE;
        }
    } else {
        if (itemModel.textSize > MAX_FONT_SIZE) {
            return MAX_FONT_SIZE;
        }
    }

    return itemModel.textSize;
}

let getItemModelStyle = (itemModel) => {
    // console.log("itemModel", itemModel)
    let styleIdex = itemModel.style;
    //TODO: need to implement other shape
    let shape = itemModel.shape;

    let styleObj = {
        shape: ItemShape.getString(shape),
        whiteSpace: 'nowrap'
    };
    
    if (shape === ItemShape.BASE_RECT || shape === ItemShape.BASE_TRIANGLE) {
        styleObj[mxConstants.STYLE_ROUNDED] = 0;
    }

    if (shape === ItemShape.BASE_ROUND_RECT) {
        styleObj[mxConstants.STYLE_ROUNDED] = 1;
        styleObj[mxConstants.STYLE_ARCSIZE] = 6;
    }

    styleObj[mxConstants.STYLE_PERIMETER] = getStringPerimeterFromShape(shape);
    styleObj[mxConstants.STYLE_PERIMETER_SPACING] = 2;


    styleObj[mxConstants.STYLE_SPACING] = 10;
    styleObj[KConstants.TEXT_VERTICAL_ALIGN] = getItemModelTextVerticalAlign(itemModel);
    styleObj[KConstants.TEXT_HORIZONTAL_ALIGN] = getItemModelTextHorizontalAlign(itemModel);
    styleObj[KConstants.ADD_CLASS] = 'noselect';

    const horizontalAlign = itemModel[KConstants.TEXT_HORIZONTAL_ALIGN];
    switch (horizontalAlign) {
        case KItemTextAlign.LEFT:
            styleObj[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_LEFT;
            break;
        case KItemTextAlign.CENTER:
            styleObj[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
            break;
        case KItemTextAlign.RIGHT:
            styleObj[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_RIGHT;
            break;
        default: 
            styleObj[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_LEFT;
            break;
    }

    styleObj.verticalLabelPosition = 'middle';

    styleObj.fontSize = getModelTextSize(itemModel);
    
    // attachFile
    if (itemModel.attachFile) {
        styleObj[mxConstants.STYLE_SPACING_TOP] = ATTACH_ITEM_HEIGHT;
    }

    switch (styleIdex) {
        case ItemStyle.A:
            styleObj.strokeColor = "#333333";
            styleObj.fontColor = "#333333";
            break;
        case ItemStyle.B:
            styleObj.strokeColor = "#85bcb8";
            styleObj.fontColor = "#4b8c88";
            break;
        case ItemStyle.C:
            styleObj.strokeColor = "#d7c280";
            styleObj.fontColor = "#a0883a";
            break;

        case ItemStyle.E:
            styleObj.strokeColor = "#f13322";
            styleObj.fontColor = "#f13322";
            break;
        /**
         * with background
         */
        case ItemStyle.F:
            styleObj.strokeColor = "#333333";
            styleObj.fillColor = "#333333";
            styleObj.fontColor = "#ffffff";
            break;
        case ItemStyle.G:
            styleObj.strokeColor = "#85bcb8";
            styleObj.fillColor = "#85bcb8";
            styleObj.fontColor = "#ffffff";
            break;
        case ItemStyle.H:
            styleObj.strokeColor = "#d7c280";
            styleObj.fillColor = "#d7c280";
            styleObj.fontColor = "#876822";
            break;
        case ItemStyle.I:
            styleObj.strokeColor = "#929292";
            styleObj.fillColor = "#929292";
            styleObj.fontColor = "#ffffff";
            break;
        case ItemStyle.J:
            styleObj.strokeColor = "#f13322";
            styleObj.fillColor = "#f13322";
            styleObj.fontColor = "#ffffff";
            break;
        case ItemStyle.B_A:
            // style[mxConstants.STYLE_SHAPE] = ItemShape.getString(itemModel.shape);
            styleObj[mxConstants.STYLE_FILLCOLOR] = "#e3e3df";
            styleObj[mxConstants.STYLE_STROKECOLOR] = "#e3e3df";
            styleObj[mxConstants.STYLE_NOLABEL] = 1;

            break;
        case ItemStyle.B_B:
            styleObj[mxConstants.STYLE_FILLCOLOR] = "#cfded9";
            styleObj[mxConstants.STYLE_STROKECOLOR] = "#cfded9";
            styleObj[mxConstants.STYLE_NOLABEL] = 1;
            // update..
            break;
        case ItemStyle.B_C:
            styleObj[mxConstants.STYLE_FILLCOLOR] = "#eae3c9";
            styleObj[mxConstants.STYLE_STROKECOLOR] = "#eae3c9";
            styleObj[mxConstants.STYLE_NOLABEL] = 1;
            // update..
            break;
        case ItemStyle.B_D:
            styleObj[mxConstants.STYLE_FILLCOLOR] = "#828282";
            styleObj[mxConstants.STYLE_STROKECOLOR] = "#828282";
            styleObj[mxConstants.STYLE_NOLABEL] = 1;
            // update..
            break;
        case ItemStyle.B_E:
            styleObj[mxConstants.STYLE_FILLCOLOR] = "#f6ddda";
            styleObj[mxConstants.STYLE_STROKECOLOR] = "#f6ddda";
            styleObj[mxConstants.STYLE_NOLABEL] = 1;
            // update..
            break;
        case ItemStyle.D: //default
        default:
            styleObj.strokeColor = "#929292";
            styleObj.fontColor = "#313232";
            break;
        
    }
    styleObj.style = styleIdex;
    // console.log("styleObj", styleObj)
    return Object.keys(styleObj).map(key => key + '=' + styleObj[key]).join(';');
}

const getItemModelTextVerticalAlign = (itemModel) => {
    //default vertical align is center
    let value = KItemTextVerticalAlign.VALUE_MIDDLE;

    if (itemModel) {
        const verticalAlign = itemModel[KConstants.TEXT_VERTICAL_ALIGN];
        if (verticalAlign !== undefined) {
            switch (verticalAlign) {
                case KItemTextVerticalAlign.TOP:
                    value = KItemTextVerticalAlign.VALUE_TOP;
                    break;
                case KItemTextVerticalAlign.MIDDLE:
                    value = KItemTextVerticalAlign.VALUE_MIDDLE;
                    break;
                case KItemTextVerticalAlign.BOTTOM:
                    value = KItemTextVerticalAlign.VALUE_BOTTOM;
                    break;
                default: break;
            }
        }
    }

    return value;
}

const getItemModelTextHorizontalAlign = (itemModel) => {
    //default horizontal align is center
    let value = KItemTextAlign.VALUE_LEFT;

    if (itemModel) {
        const horizontalAlign = itemModel[KConstants.TEXT_HORIZONTAL_ALIGN];
        if (horizontalAlign !== undefined) {
            switch (horizontalAlign) {
                case KItemTextAlign.LEFT:
                    value = KItemTextAlign.VALUE_LEFT;
                    break;
                case KItemTextAlign.CENTER:
                    value = KItemTextAlign.VALUE_CENTER;
                    break;
                case KItemTextAlign.RIGHT:
                    value = KItemTextAlign.VALUE_RIGHT;
                    break;
                default: break;
            }
        }
    }

    return value;
}

const getEditingLabelStyle = () => {
    let style = `
        imageAlign=left;
        fillColor=none;
        strokeWidth=0;
        stroke=transparent;
        labelPosition=center;
        fontSize=8;
        fontColor=#000000;
        align=left;
    `;

    //replace all spaceses, newline included (mxgraph will throw error if there are any whitespace in style)
    style = style.replace(/\s+/gm, '');
    return style;
}

const getGoodButtonStyle = () => {
    let style = `
        imageAlign=left;
        fillColor=none;
        strokeWidth=0;
        stroke=transparent;
        labelPosition=center;
        fontSize=9;
        fontColor=#ffffff;
        align=left;
        fillColor=#8d8d8d;
        rounded=1;
    `;

    //replace all spaceses, newline included (mxgraph will throw error if there are any whitespace in style)
    style = style.replace(/\s+/gm, '');
    return style;
}

const getAttachFileDownloadStyle = () => {
    let style = `
        imageAlign=center;
        strokeWidth=0;
        stroke=transparent;
        labelPosition=center;
        fontColor=#ffffff;
        align=left;
        fillColor=#535252;
        rounded=1;
        arcSize=20;
        opacity=65;
    `;

    //replace all spaceses, newline included (mxgraph will throw error if there are any whitespace in style)
    style = style.replace(/\s+/gm, '');
    return style;
}

const getStyleIconEditLabel = (itemModel) => {
    let styleObj = {}
    styleObj[mxConstants.STYLE_IMAGE] = imgEditText;
    styleObj[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    styleObj[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
    styleObj[mxConstants.STYLE_MOVABLE] = 0;
    styleObj[mxConstants.STYLE_IMAGE_ALIGN] = mxConstants.ALIGN_CENTER;
    styleObj[mxConstants.STYLE_IMAGE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP;
    styleObj[mxConstants.STYLE_IMAGE_HEIGHT] = 18;
    styleObj[mxConstants.STYLE_IMAGE_WIDTH] = 18;

    return Object.keys(styleObj).map(key => key + '=' + styleObj[key]).join(';');
}

const getStyleIconResizeBaseItemLabel = () => {
    let styleObj = {}
    styleObj[mxConstants.STYLE_IMAGE] = imgResizeSmall;
    styleObj[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    styleObj[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
    styleObj[mxConstants.STYLE_MOVABLE] = 0;
    styleObj[mxConstants.STYLE_IMAGE_ALIGN] = mxConstants.ALIGN_CENTER;
    styleObj[mxConstants.STYLE_IMAGE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP;
    styleObj[mxConstants.STYLE_IMAGE_HEIGHT] = 8;
    styleObj[mxConstants.STYLE_IMAGE_WIDTH] = 8;

    return Object.keys(styleObj).map(key => key + '=' + styleObj[key]).join(';');
}

const getAttachFileStyle = (itemModel) => {
    let styleObj = {}
    let thumbnailUri = itemModel.attachFile.thumbnailUri;
    let filename = itemModel.attachFile.name;

    styleObj[mxConstants.STYLE_IMAGE] = isImageFile(filename) ? thumbnailUri : getLargeIcon(filename);
    styleObj[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_IMAGE;
    styleObj[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
    styleObj[mxConstants.STYLE_MOVABLE] = 0;
    styleObj[mxConstants.STYLE_IMAGE_ALIGN] = mxConstants.ALIGN_CENTER;
    styleObj[mxConstants.STYLE_IMAGE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP;
    styleObj[mxConstants.STYLE_IMAGE_HEIGHT] = '70';

    return Object.keys(styleObj).map(key => key + '=' + styleObj[key]).join(';');
}

const createStickyLabel = (image, value, isDot) => {
    let htmlString = `
            <div class="d-flex align-items-end sticky-label" >
                <span class="d-flex align-items-end" >
                    ${isDot ? '<strong class="noselect">....</strong>' : ''}
                    <img src="${image}"/ class='noselect'>
                </span>
                <div class='noselect'>${value}</div>
            </div>
    `;
    htmlString = htmlString.replace(/\n/gi, '');
    
    return htmlString;
}

const createGoodButtonLabel = (image, value, likeCount) => {
    const containerStyle = `
        border-radius: 3px;
        padding: 2px 3px;
        cursor: default;
    `;
    let htmlString = `
            <div class="d-flex align-items-end" 
                style="${containerStyle}">
                <span class="d-flex align-items-end" style="width: 12px;">
                    <img src="${image}" style="width: auto; height: 10px" class='noselect'/>
                </span>
                <div style="height: 10px;" class='noselect'>${value}</div>
                <div style="height: 10px; margin-left:7px;" class='noselect'>${likeCount}</div>
            </div>
    `;
    htmlString = htmlString.replace(/\n/gi, '');
    
    return htmlString;
}

const createAttachDownloadLabel = () => {
    const containerStyle = `
        border-radius: 6px;
        padding: 3px;
        textAlign:center;
        cursor: default;
    `;
    let htmlString = `
            <div class="d-flex align-items-end" 
                style="${containerStyle}">
                <span class="d-flex align-items-end" style="width: 18px;">
                    <img src="${imgContextDownload}" style="width: auto; height: 18px"/>
                </span>
            </div>
    `;
    htmlString = htmlString.replace(/\n/gi, '');
    
    return htmlString;
}

let getItemModelGeometry = (itemModel) => {
    return new mxGeometry(itemModel.x, itemModel.y, DEFAULT_ITEM_WIDTH, DEFAULT_ITEM_HEIGHT);
}

let getSelectionBorderStyle = () => {
    let styleObj = {};
    styleObj.shape = ItemShape.SHAPE_NAME_SELECTION_BORDER;
    styleObj.strokeWidth = 1.4;
    styleObj.strokeColor = ITEM_COLORS.BOUNDING_BOX;
    return Object.keys(styleObj).map(key => key + '=' + styleObj[key]).join(';');
}

let getBaseItemLabelStyle = (baseItemModel, fillColor = "none") => {
    let style = {
        shape: 'rectangle',
        whiteSpace: 'wrap',
    };

    style.verticalAlign = getItemModelTextVerticalAlign(baseItemModel);
    style.align = getItemModelTextHorizontalAlign(baseItemModel);
    style[mxConstants.STYLE_FILLCOLOR] = fillColor;

    // style[mxConstants.STYLE_FONTCOLOR] = "#b2b2b2";
    // style[mxConstants.STYLE_FILL_OPACITY] = "40";
    style[mxConstants.STYLE_STROKECOLOR] = "none";
    style[mxConstants.STYLE_STROKEWIDTH] = 1;
    style[mxConstants.STYLE_ROUNDED] = 0;
    style[mxConstants.STYLE_FONTSIZE] = getModelTextSize(baseItemModel);
    style[mxConstants.STYLE_FILL_OPACITY] = 60;
    style[mxConstants.STYLE_OVERFLOW] = 'hidden';
    style[KConstants.ADD_CLASS] = 'hidden-scrollbar';
    style[mxConstants.STYLE_LABEL_PADDING] = 0;
    style[mxConstants.STYLE_SPACING] = 0;

    switch (baseItemModel.style) {
        case ItemStyle.B_A:
            style[mxConstants.STYLE_FONTCOLOR] = "#969695";
            break;
        case ItemStyle.B_B:
            style[mxConstants.STYLE_FONTCOLOR] = "#7ca198";
            break;
        case ItemStyle.B_C:
            style[mxConstants.STYLE_FONTCOLOR] = "#aea895";
            break;
        case ItemStyle.B_D:
            style[mxConstants.STYLE_FONTCOLOR] = "#ffffff";
            break;
        case ItemStyle.B_E:
            style[mxConstants.STYLE_FONTCOLOR] = "#d09189";
            break;
        default:
        case ItemStyle.D:
            style[mxConstants.STYLE_FONTCOLOR] = "#b2b2b2";
            break;
    }

    return Object.keys(style).map(key => key + '=' + style[key]).join(';');
}
let getBaseItemLabelSelectionStyle = (baseItemModel, type) => {
    if (type === mxConstants.STYLE_STROKECOLOR) {
        switch (baseItemModel.style) {
            case ItemStyle.B_A:
            case ItemStyle.B_B:
            case ItemStyle.B_C:
            case ItemStyle.B_E:
                return "#b2b2b2"
            case ItemStyle.B_D:
                return "#ffffff";
            default:
                return "#b2b2b2"
        }
    }
}


const getItemLineModelStyle = (itemLineModel) => {
    let style = {
        whiteSpace: 'nowrap',
    };

    style[mxConstants.STYLE_FONTCOLOR] = "#333333";
    style[mxConstants.STYLE_STROKECOLOR] = `#${getLineColor(itemLineModel.color)}`;
    style[mxConstants.STYLE_FILLCOLOR] = style[mxConstants.STYLE_STROKECOLOR];
    style[mxConstants.STYLE_STROKEWIDTH] = itemLineModel.large;
    style[KConstants.TEXT_HORIZONTAL_ALIGN] = getItemModelTextHorizontalAlign(itemLineModel);
    style.fontSize = getModelTextSize(itemLineModel);
    style[mxConstants.STYLE_FONTFAMILY] = KConstants.DEFAULT_FONT_FAMILY;
    style[mxConstants.DEFAULT_FONTFAMILY] = KConstants.DEFAULT_FONT_FAMILY;
    /**
     * fix fontSize for edge label
     */
    // style.fontSize = mxConstants.DEFAULT_FONTSIZE;

    switch (itemLineModel.style) {
        default:
        case KItemLineArrowType.LINEAR_NONE:
            style[mxConstants.STYLE_STARTARROW] = 'none';
            style[mxConstants.STYLE_ENDARROW] = 'none';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.LINEAR_FROM_TO: 
            style[mxConstants.STYLE_STARTARROW] = 'none';
            style[mxConstants.STYLE_ENDARROW] = 'block';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.LINEAR_TO_FROM: 
            style[mxConstants.STYLE_STARTARROW] = 'block';
            style[mxConstants.STYLE_ENDARROW] = 'none';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.LINEAR_ONE_WAY_BIDIRECTIONAL: 
            style[mxConstants.STYLE_STARTARROW] = 'block';
            style[mxConstants.STYLE_ENDARROW] = 'block';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.SQUARE_NONE: 
            style[mxConstants.STYLE_STARTARROW] = 'none';
            style[mxConstants.STYLE_ENDARROW] = 'none';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.SQUARE_FROM_TO: 
            style[mxConstants.STYLE_STARTARROW] = 'none';
            style[mxConstants.STYLE_ENDARROW] = 'block';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.SQUARE_TO_FROM: 
            style[mxConstants.STYLE_STARTARROW] = 'block';
            style[mxConstants.STYLE_ENDARROW] = 'none';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.SQUARE_ONE_WAY_BIDIRECTIONAL: 
            style[mxConstants.STYLE_STARTARROW] = 'block';
            style[mxConstants.STYLE_ENDARROW] = 'block';
            style[mxConstants.STYLE_EDGE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        case KItemLineArrowType.TWO_WAY: 
        case KItemLineArrowType.TWO_WAY_REVERSE: 
            style[mxConstants.STYLE_SHAPE] = KGraphEdgeShape.getString(itemLineModel.style);
            break;
        
    }

    style[mxConstants.STYLE_STROKEWIDTH] = itemLineModel[KConstants.ITEM_LINE_LARGE];

    return Object.keys(style).map(key => key + '=' + style[key]).join(';');
}

const createSVGImageFromState = ({ background, state, viewScale, scale, border, bounds }) => {
    const imgExport = new mxImageExport();

    let svgDoc = mxUtils.createXmlDocument();
    let svgImage = (svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG, 'svg') : svgDoc.createElement('svg');
    if (background != null) {
        if (svgImage.style != null) {
            svgImage.style.backgroundColor = background;
        }
        else {
            svgImage.setAttribute('style', 'background-color:' + background);
        }
    }

    if (svgDoc.createElementNS == null) {
        svgImage.setAttribute('xmlns', mxConstants.NS_SVG);
        svgImage.setAttribute('xmlns:xlink', mxConstants.NS_XLINK);
    }
    else {
        svgImage.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xlink', mxConstants.NS_XLINK);
    }

    svgImage.setAttribute('width', (Math.ceil(bounds.width * scale / viewScale) + 2 * border) + 'px');
    svgImage.setAttribute('height', (Math.ceil(bounds.height * scale / viewScale) + 2 * border) + 'px');
    svgImage.setAttribute('version', '1.1');

    let group = (svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG, 'g') : svgDoc.createElement('g');
    group.setAttribute('transform', 'translate(0.5,0.5)');
    svgImage.appendChild(group);
    svgDoc.appendChild(svgImage);

    let svgCanvas = new mxSvgCanvas2D(group);
    svgCanvas.translate(Math.floor((border / scale - bounds.x) / viewScale), Math.floor((border / scale - bounds.y) / viewScale));
    svgCanvas.scale(scale / viewScale);

    imgExport.drawState(state, svgCanvas);

    return svgImage;

}
/**
 * <p>任意のアイテムのフォントサイズを大きくします。</p>
 * @param item IStyleClient ( 任意のアイテム、関係線 )
 * @param itemType
 */
function fontSizePlus(item, itemType) {
   
    let size = item[KConstants.ITEM_TEXT_SIZE];
    if (itemType === KItemType.BASE_ITEM) {
        if (size + 2 <= MAX_BASE_ITEM_FONT_SIZE) {
            size += 2;
        }
        else {
            size = MAX_BASE_ITEM_FONT_SIZE;
        }
    }
    else {
        // console.log({item, itemType})
        if (size + 2 <= MAX_FONT_SIZE) {
            size += 2;
        }
        else {
            size = MAX_FONT_SIZE;
        }
    }

    return size;
}

/**
 * <p>任意のアイテムのフォントサイズを小さくします。</p>
 * @param item IStyleClient ( 任意のアイテム、関係線 )
 */
function fontSizeMinus(item) {
    let size = item[KConstants.ITEM_TEXT_SIZE];
    if (size - 2 >= MIN_FONT_SIZE) {
        size -= 2;
    }
    else {
        size = MIN_FONT_SIZE;
    }
    return size
}

function getStyleValue(style, key){
    let valueRegx = new RegExp(`${key}=(.*?)(;|$)`);
    let matchValues = style.match(valueRegx);
    let outputValue = null;
    if(matchValues){
        outputValue = matchValues[1];
    }
    return outputValue;
}

function transformToFlexAlign(align){
    let transformedAlign = {
        top: 'flex-start',
        bottom: 'flex-end',
        right: 'flex-end',
        left: 'flex-start',
        center: 'center'
    }[align];
    
    return transformedAlign;
}

mxRectangle.prototype.isOverlap = function (rectangle) { 
    const box = this;
    if (rectangle instanceof mxRectangle !== true) {
        return false;
    }
    
    return Math.max(box.x, rectangle.x) < Math.min(box.x+box.width , rectangle.x+rectangle.width) &&
           Math.max(box.y, rectangle.y) < Math.min(box.y+box.height, rectangle.y+rectangle.height );
}

mxRectangle.prototype.toScale = function(scale){
    const rect = this;
    return new mxRectangle(rect.x*scale, rect.y*scale, rect.width*scale, rect.height*scale);
}

 
const getTextGoodButton = (itemModel, clicking) => {
    const phase1 = 1;
    const phase2 = 2;
    const phase3 = 3;
    const likeLevel = itemModel.likeLevel;
    let resultState = "";
    if (likeLevel < phase1) {
        resultState =  i18n.t('text.good');
    } else if(likeLevel < phase2) {
        resultState =  i18n.t('text.veryGood');
    } else if(likeLevel < phase3) {
        resultState =  i18n.t('text.excelent');
    } else {
        resultState =  i18n.t('text.great');
    }

    if (clicking && itemModel.liked === true && !mxClient.IS_TOUCH) {
        resultState =  i18n.t('text.stateUndo');
    }

    return resultState; 
}

const measureTextSize = (state, content, style) => {
    let div = document.createElement("DIV");
    
    div.innerHTML = content
    div.style.position = `absolute`;
    if (typeof style === 'object') {
        let styleKeys = Object.keys(style);
        for (let i = 0; i < styleKeys.length; i ++) {
            div.style[styleKeys[i]] = style[styleKeys[i]]; 
        }
    }

    div.style.width = `auto`;
    div.style.whiteSpace = 'pre-line';
    div.style.contentEditable='true'

    div.style.fontSize = `${state.style.fontSize}px`;
    div.style.fontFamily = `${state.style.fontFamily}`;
    
    document.body.appendChild(div);
    const bounds = div.getBoundingClientRect();
    document.body.removeChild(div);
    return bounds;
}

const isMultipleSelected = (graph) => {
    const selectedCells = graph.getSelectionCells();

    if (Array.isArray(selectedCells) && selectedCells.length > 0) {
        if (selectedCells[0]) {
            return !isSelectedSingleItem(selectedCells) && !isSelectedSingleBaseItem(selectedCells);
        }
    }

    return false;
}

const isSelectedSingleItem = (selectedCells) => {
    return selectedCells.length === 1 && selectedCells[0].itemType === KItemType.DEFAULT_ITEM;
}

const isSelectedSingleBaseItem = (selectedCells) => {
    let isSingleSelected = false;
    if (selectedCells.length === 1) {
        isSingleSelected = (selectedCells[0].itemType === KItemType.BASE_ITEM);
    } else if (selectedCells.length === 2) {
        if (selectedCells[0].itemType === KItemType.BASE_ITEM) {
            isSingleSelected = true;
        } else if (KItemType.BASE_ITEM_LABEL === selectedCells[0].itemType) {
            isSingleSelected = true;
        }
    }

    return isSingleSelected;
}

const browser = detect();

export {
    DEFAULT_ITEM_SHAPE,
    DEFAULT_ITEM_WIDTH,
    DEFAULT_ITEM_HEIGHT,
    MAX_ITEM_HEIGHT,
    MAX_ITEM_WIDTH,
    DEFAULT_BASE_ITEM_WIDTH,
    DEFALT_BASE_ITEM_HEIGHT,
    MAX_BASE_ITEM_FONT_SIZE,
    MIN_FONT_SIZE,
    MAX_FONT_SIZE,
    DEFAULT_FONT_SIZE,
    DEFAULT_LINE_WEIGHT,
    ATTACH_ITEM_WIDTH,
    ATTACH_ITEM_HEIGHT,
    BUTTON_DOWNLOAD_ATTACH_ITEM_SIZE,
    EDITING_USER_LABEL_OFFSET_BOTTOM,
    GOOD_BUTTON_MARGIN,
    CREATE_USER_LABEL_OFFSET_TOP,
    DEFAULT_ARROW_WIDTH,
    ARROW_TAIL_LENGTH,
    MAIN_LINE_HALF_WIDTH,
    DEFAULT_CURSOR,
    MAX_BASE_ITEM_WIDTH,
    MAX_BASE_ITEM_HEIGHT,
    MIN_BASE_ITEM_HEIGHT,
    MIN_BASE_ITEM_WIDTH,
    MAX_BASE_ITEM_LABEL_HEIGHT,
    MIN_BASE_ITEM_LABEL_HEIGHT,
    MIN_BASE_ITEM_LABEL_WIDTH,
    MAX_BASE_ITEM_LABEL_WIDTH,
    MAX_GOODBUTTON_WIDTH,
    MIN_GOODBUTTON_WIDTH,
    SHEET_PNG_PADDING,
    EDIT_CREATER_LABEL_MARGIN_LEFT,
    CONTEXT_MENU_MOVE_LEFT,
    CONTEXT_MENU_MOVE_TOP,
    SELECTION_BORDER_PADDING,
    getItemModelStyle,
    getItemModelGeometry,
    getBaseItemLabelStyle,
    getEditingLabelStyle,
    getAttachFileStyle,
    createStickyLabel,
    getItemLineModelStyle,
    createSVGImageFromState,
    fontSizePlus,
    fontSizeMinus,
    getStyleValue,
    transformToFlexAlign,
    getBaseItemLabelSelectionStyle,
    getGoodButtonStyle,
    createGoodButtonLabel,
    getTextGoodButton,
    createAttachDownloadLabel,
    getStyleIconEditLabel,
    getStyleIconResizeBaseItemLabel,
    measureTextSize,
    browser,
    isMultipleSelected,
    isSelectedSingleItem,
    isSelectedSingleBaseItem,
    getAttachFileDownloadStyle,
    getSelectionBorderStyle
}
