import { mxXmlCanvas2D } from "./KClient";

class KXmlCanvas2D extends mxXmlCanvas2D {
    
    filterBlur(){
        let elem = this.createElement('filterblur');
        elem.setAttribute('type', "highlight");
        
        this.root.appendChild(elem);
    }
}
export default KXmlCanvas2D;