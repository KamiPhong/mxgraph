import { mxImageExport, mxShape } from "./KClient";
import { imgEditText, imgResizeSmall, imgResizeSmallTransparent } from 'wlp-client-editor-lib/utils/ResourceIconUtils';
class KImageExport extends mxImageExport {
    
    drawCellState(state, canvas) {
        super.drawCellState(state, canvas);
        if (state.selectionShape) {
            this.drawSelectionShape(state, canvas);
        }
    }

    drawSelectionShape = (state, canvas) => {
        if (state.selectionShape != null) {
            canvas.save();
            state.selectionShape.paint(canvas);
            canvas.restore();
        }
    }

    // TODO: fix export png
    drawShape(state, canvas) {
        let itemDraw = true;
        if (state.cell.isGoodButton || state.cell.isCreatorLabel || state.cell.isEditingLabel) {
            itemDraw = false;
        }

        if (state.cell.isIconEditLabel) {
            if (state.cell.hasRole === false) {
                itemDraw = false;
            } else {
                state.shape.image = imgEditText;
            }
        }
        
        if (state.cell.isIconResizeBaseItemLabel) {
            state.shape.image = imgResizeSmall;
        }

        if (itemDraw === true) {
            if (state.shape instanceof mxShape && state.shape.checkBounds()) {
                canvas.save();
                state.shape.paint(canvas);
                if (typeof state.shape.filterBlur !== 'undefined' && state.shape.filterBlur === true) {
                    canvas.filterBlur();
                }
                canvas.restore();
            } else { 
                super.drawShape(state, canvas);
            }
        }

        if (state.cell.isIconResizeBaseItemLabel) {
            state.shape.image = imgResizeSmallTransparent;
        }
    }

    drawText(state, canvas) {
        let itemDraw = true;
        if (state.cell.isGoodButton || state.cell.isCreatorLabel || state.cell.isEditingLabel) {
            itemDraw = false;
        }


        if (itemDraw === true) {     
            if (state.cell !== null && state.cell.isBaseItemLabel === true && state.text !== null) {
                state.text.bounds.y = state.shape.bounds.y;
            }
            super.drawText(state, canvas);
        }
    }

}

export default KImageExport;