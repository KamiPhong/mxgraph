import { integerToHext } from "wlp-client-common/utils/NumericUtils";

const KItemLineColor = {
    NONE: '-1',
    A: 0x333333,
    B: 0x89bcb8,
    C: 0xd6c17c,
    D: 0xa7a7a7,
    E: 0xec3015
}

export function getLineColor(colorCode){
    const colorList = Object.values(KItemLineColor);
    if(colorList.indexOf(colorCode) > -1){
        return integerToHext(colorCode);
    }else {
        return integerToHext(KItemLineColor.A);
    }
}

export default KItemLineColor;
