
import {  mxGeometry } from "wlp-client-editor-lib/core/KClient";
import KItemBase from './KItemBase';
import KItemType from './KItemType';
import * as GraphUtil from './KGraphUtil';
import KBaseItemLabel from "./KBaseItemLabel";
/**
 * BaseItem クラスは、ベースアイテム専用のクラスです。
 */
export default class KBaseItem extends KItemBase {
    constructor(model) {
        super(model);
        
        this.setGeometry(new mxGeometry(model.x, model.y, model.width, model.height));
        this.itemType = KItemType.BASE_ITEM;
        // this.geometry.relative = true;
        this.style = GraphUtil.getItemModelStyle(model);
        this.text = "";
        this.baseItemLabel = null;
        this.createBaseItemLabel();
    }

    getChildrenLabel = () => {
        return this.baseItemLabel;
    }

    createBaseItemLabel = () => {
        if(this.data){
            const { textX, textY, text, textWidth, textHeight } = this.data;
            const style = GraphUtil.getBaseItemLabelStyle(this.data);
            let geometry = new mxGeometry(textX, textY, textWidth, textHeight);
            // geometry.relative = true;
            let vertex = new KBaseItemLabel(text, geometry, style);
            vertex.setId(this.id + '_label');
            vertex.setVertex(true);
            vertex.setVisible(true);
            vertex.setConnectable(false);
            vertex.baseItem = this;
    
            // this.insert(vertex);
            this.baseItemLabel = vertex;
        }
    }

    setGeometry(geometry){
        super.setGeometry(geometry);
        // Logger.logConsole('KBaseItem.setGeometry', geometry)
    }

    /**
    * override updateModelTextFromCell in KItemBase 
    */
    updateModelTextFromCellLabel = () => {
        this.data.text = this.baseItemLabel.value;
    }

    updateModelSizeFromCell = () => {
        this.data.width = this.geometry.width;
        this.data.height = this.geometry.height;
    }

    updateModelLabelSizeFromCell = () => {
        this.data.textWidth = this.baseItemLabel.geometry.width;
        this.data.textHeight = this.baseItemLabel.geometry.height;
    }

    updateModelLocationFromCellLabel = () => {
        // nếu parent của label là baseitem
        // if (this.baseItemLabel.parent && this.baseItemLabel.parent.data) {
        //     this.data.textX = this.baseItemLabel.geometry.x + this.geometry.x;
        //     this.data.textY = this.baseItemLabel.geometry.y + this.geometry.y;
        // } else {
            this.data.textX = this.baseItemLabel.geometry.x ;
            this.data.textY = this.baseItemLabel.geometry.y;
        // }
    }

    updateModelLocationFromCell = () => {
        this.data.x = this.geometry.x;
        this.data.y = this.geometry.y;

        // this.data.textX = this.baseItemLabel.geometry.x;
        // this.data.textY = this.baseItemLabel.geometry.y;
    }
}