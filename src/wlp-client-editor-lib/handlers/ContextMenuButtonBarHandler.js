class ContextMenuButtonBarHandler {
    constructor(graph) {
        this.graph = graph;
        this.zoom = false;
        this.scale = 1;
    }
}

export default ContextMenuButtonBarHandler;
