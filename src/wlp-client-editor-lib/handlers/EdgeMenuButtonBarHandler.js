import ContextMenuButtonBarHandler from './ContextMenuButtonBarHandler';
import { setStyle, addClassBatch, removeClassBatch } from 'wlp-client-common/utils/DOMUtils';

const dBlock = 'block';
const dNone = 'none';

class EdgeMenuButtonBarHandler extends ContextMenuButtonBarHandler {
    constructor(graph) {
        super(graph);

        this.itemLineMenuButtonBar = document.getElementById("itemLineMenuButtonBar");
        this.itemLineMenuControl = document.getElementById("itemLineMenuControl");
        this.buttonRemove = null;
        this.borderGroup = document.getElementById("itemMenuBorderGroupItem");
        this.menuControl = document.getElementById("itemLineMenuControl");

    }

    handleZoom = () => {
        this.zoom = true;
        this.redrawTools();
    }

    redrawTools(cells) {
        const { graph, zoom, itemLineMenuButtonBar } = this;
        if (zoom === true && itemLineMenuButtonBar && itemLineMenuButtonBar.style.display === "none") {
            return;
        }
        if (graph && graph.view) {
            const scale = graph.view.scale;
            this.scale = scale;
            if (!cells) {
                cells = graph.getSelectionCells();
            }
            
            if (cells.length > 0) {
                const firsItemState = graph.getView().getState(cells[0], true);
                const data = cells[0].data;
                const geo = firsItemState.cellBounds;
                const translate = graph.getView().getTranslate();
                const containerBounds = this.itemLineMenuControl.getBoundingClientRect();
                const x = geo.x  + translate.x + geo.width/2 - containerBounds.width/2 / scale;
                const y = geo.y + translate.y + 85 + geo.height/2;
                
                setStyle(this.itemLineMenuButtonBar, 'display', dBlock);
                setStyle(this.itemLineMenuButtonBar, 'transform', `scale(${scale})`);

                setStyle(this.itemLineMenuControl, 'top', `${y}px`);
                setStyle(this.itemLineMenuControl, 'left', `${x}px`);
                setStyle(this.itemLineMenuControl, 'position', `absolute`);

                this.menuControl.getElementsByClassName('context-action').forEach((item) => {
                    this.setActiveOption(data, item);
                });

            } else {
                setStyle(this.itemLineMenuButtonBar, 'display', dNone);
            }
        }
    }

    setActiveOption = (data, item) => {
        if (data) {
            const type = data.style;
            const weight = data.large;
            const style = data.color;
            const textalign = data.textHorizontalAlign;

            let isActive = false;
            
            if (item.dataset.type !== undefined) {
                if (parseInt(item.dataset.type) === type) {
                    isActive = true;
                }
            } else if (item.dataset.style !== undefined) {
                if (parseInt(item.dataset.style) === style) {
                    isActive = true;
                }
            } else if (item.dataset.textalign !== undefined) {
                if (parseInt(item.dataset.textalign) === textalign) {
                    isActive = true;
                }
            } else if (item.dataset.weight !== undefined) {
                if (parseInt(item.dataset.weight) === weight) {
                    isActive = true;
                }
            }

            if (isActive) {
                addClassBatch([item], 'actived');
            } else {
                removeClassBatch([item], 'actived');
            }
        }
    }

}


export default EdgeMenuButtonBarHandler;