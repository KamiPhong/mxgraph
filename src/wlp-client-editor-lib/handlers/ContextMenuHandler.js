import {
    mxVertexHandler, mxClient, mxUtils
} from "wlp-client-editor-lib/core/KClient";

import { imgEditText } from 'wlp-client-editor-lib/utils/ResourceIconUtils';

class ContextMenuHandler extends mxVertexHandler {
    init(){
        this.initElement();
        super.init();
    }

    initElement(){
        this.domNode = document.createElement('div');
        this.editText = this.createImage(imgEditText);
        this.editText.style.display = 'none';
    }

    createBtnResize(src) {
        let div = document.createElement('div');
        let img = this.createImage(src);
        div.appendChild(img);
        return div;
    }

    createImage(src){
        if (mxClient.IS_IE && !mxClient.IS_SVG) {
            let img = document.createElement('div');
            img.style.backgroundImage = 'url(' + src + ')';
            img.style.backgroundPosition = 'center';
            img.style.backgroundRepeat = 'no-repeat';
            img.style.display = (mxClient.IS_QUIRKS) ? 'inline' : 'inline-block';
            return img;
        } else {
            return mxUtils.createImage(src);
        }
    }
}

export default ContextMenuHandler;