import mxGraphFactory from 'mxgraph';
const { mxPopupMenuHandler } = new mxGraphFactory();

class KPopupMenuHandler extends mxPopupMenuHandler {
    mouseUp (sender, me){
        return false;
    }
}

export default KPopupMenuHandler;