import { mxEvent } from "wlp-client-editor-lib/core/KClient";

import EdgeMenuButtonBarHandler from "./EdgeMenuButtonBarHandler.js";
import ContextMenuHandler from './ContextMenuHandler';
import KLineItem from "wlp-client-editor-lib/core/KLineItem.js";

class EdgeContextMenuHandler extends ContextMenuHandler {

    init(){
        super.init();

        this.graph.container.appendChild(this.domNode);
        this.redrawTools();
    }

    redraw(){
        super.redraw();
        this.selectionBorder.scale = this.graph.view.scale;
        this.selectionBorder.scale = 1;
        this.redrawTools();
    };

    redrawTools(){
        const { state, graph } = this;
        const { cell } = state;
        if (state != null) {
            const selectionCells = graph.getSelectionCells();

            this.domNode.style.top = '0px';
            this.domNode.className = 'context-gicon';

            //[huyvq-cr_63347_fb#16]--- check if the last element of selectedCells is a line then create EdgeMenuButtonBarHandler
            if (selectionCells.length === 1 || selectionCells[selectionCells.length - 1] instanceof KLineItem) {

                let itemMenuButtonBarHandler = new EdgeMenuButtonBarHandler(this.graph);
                itemMenuButtonBarHandler.redrawTools([cell]);
            }
        }
    }

    destroy(){
        super.destroy();
        const { graph } = this;
        let itemMenuButtonBar = document.getElementById("itemLineMenuButtonBar");
        if (itemMenuButtonBar != null) {
            const cell = graph.getSelectionCell();

            if (!cell || cell.isEdge() !== true) {
                itemMenuButtonBar.style.display = "none";
            }
        }
        if (this.domNode != null) {
            this.domNode.parentNode.removeChild(this.domNode);
            this.domNode = null;
        }

        // update comment remove listener for element on destroy
        mxEvent.removeAllListeners(this.editText);

    }
};

export default EdgeContextMenuHandler;