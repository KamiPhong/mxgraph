import { mxUtils, mxEvent, mxConstants, mxRectangle } from "wlp-client-editor-lib/core/KClient";

import KTailedRect from "wlp-client-editor-lib/core/shapes/KTailedRect";
import ItemMenuButtonBarHandler from "./ItemMenuButtonBarHandler.js";
import { imgResize, imgResizeSmall } from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import KItemType from 'wlp-client-editor-lib/core/KItemType';
import * as KGraphUtil from 'wlp-client-editor-lib/core/KGraphUtil';
import ContextMenuHandler from './ContextMenuHandler';
import { LightenDarkenColor } from "wlp-client-common/utils/ColorUtils";
import KItemBase from "wlp-client-editor-lib/core/KItemBase.js";
import KBaseItem from "wlp-client-editor-lib/core/KBaseItem.js";
import ActionLogger from 'wlp-client-common/ActionLogger';
import { StageConfig, ZoomConfig } from "wlp-client-editor-lib/core/KGraphConfig";
import KBaseItemLabel from "wlp-client-editor-lib/core/KBaseItemLabel.js";
class ItemContextMenuHandler extends ContextMenuHandler {

    constructor(state) {
        super(state);

        this.firstX = null;
        this.firstY = null;

        this.baseItemLabelHightLight = null;
        this.isDraggingCell = false;

        super.livePreview = true;

        this.graph.addListener(mxEvent.EDITING_STARTED, this.handleStartEditItem);
    }

    initElement(){
        super.initElement();
        this.buttonResize = this.createBtnResize(imgResize);
        this.buttonResize.style.display = 'none';

        this.buttonResizeSmall = this.createBtnResize(imgResizeSmall);
        this.buttonResizeSmall.style.display = 'none';
    }

    init(){
        let { state} = this;
        this.itemMenuButtonBarHandler = new ItemMenuButtonBarHandler(state.view.graph);
        const cell = state.cell;
        super.init();

        if (cell.itemType === KItemType.DEFAULT_ITEM || cell.itemType === KItemType.BASE_ITEM) {
            cell.setVisibleSelectionBorder(true);
        }
        
        const buttonResize = state.cell.isBaseItemLabel === true ? this.buttonResizeSmall : this.buttonResize;
        mxEvent.addGestureListeners(buttonResize,
            mxUtils.bind(this, function (evt) {
                this.start(mxEvent.getClientX(evt), mxEvent.getClientY(evt), 7);
                this.graph.isMouseDown = true;
                this.graph.isMouseTrigger = mxEvent.isMouseEvent(evt);
                mxEvent.consume(evt);
            })
        );

        this.domNode.appendChild(this.buttonResize);
        this.domNode.appendChild(this.buttonResizeSmall);

        this.graph.container.appendChild(this.domNode);

        this.redrawTools(); 
    }

    redraw(){
        //[ThuyTV]: update scale for tailed rect
        // this.selectionBorder.scale = this.graph.view.scale;
        super.redraw();
        this.redrawTools();

        if (this.isDraggingCell) {
            this.onCellDragging();
        }

        // if (this.selectionBorder !== null) {
            // this.state.selectionShape = this.selectionBorder;
        // }
    };

    onCellDragging() {
        if (this.graph.graphHandler && this.graph.graphHandler.cell instanceof KItemBase || this.graph.graphHandler.cell instanceof KBaseItemLabel) {
            this.buttonResize.style.display = 'none';
            this.itemMenuButtonBarHandler.hide();
        }
    }

    onCellDraggingEnd(){
        this.itemMenuButtonBarHandler.redrawTools();
    }

    createSelectionShape(bounds){
        let shape = new KTailedRect(bounds, null, "none", 0, 0);
        shape.isDashed = false;
        
        return shape;
        
    }

    mouseMove(sender, me){
        if (!me.isConsumed() && this.graph.isMouseDown){
            this.isDraggingCell = true;
        }
        // console.log("move")
        // console.log("this.selectionBorder", this.selectionBorder)
        super.mouseMove(sender, me);
        
        // if (this.selectionBorder) {
        //     this.selectionBorder.node.style.display = 'block';
        //     this.selectionBorder.init(this.state.shape.node);
        // }
        
    }

    mouseUp(sender, me){
        super.mouseUp(sender, me);
        if(this.isDraggingCell){
            this.onCellDraggingEnd();
        }
        this.isDraggingCell = false;
    }

    redrawTools(){
        const { state } = this;
        if (state != null) {
            let cell = state.cell;

            this.setStateStyle();
            
            this.firstX = state.x;
            this.firstX = state.y;
            // const selectionItems = graph.getSelectionItems();

            this.domNode.style.top = '0px';
            this.domNode.className = 'context-gicon';
            this.domNode.className = 'noselect';
            
            this.showButtonResize();
            if (cell.isBaseItemLabel === true) {
                // this.selectionBorder.scale = 0;
            }

            if (!this.isDraggingCell) {
                this.itemMenuButtonBarHandler.redrawTools();
            }
            
            if (state.cell.isBaseItemLabel === true) {

            }
        }
    }

    showButtonResize() {
        const { graph, state } = this;
        const cell = state.cell;
        const selectionItems = graph.getSelectionItems();
        const buttonResize = cell.isBaseItemLabel === true ? this.buttonResizeSmall : this.buttonResize;
        const defaultWidth = cell.isBaseItemLabel === true ? 8 : 14;
        if ((cell.itemType === KItemType.BASE_ITEM || cell.isBaseItemLabel === true)) {
            if (selectionItems.length > 1 && cell.itemType === KItemType.BASE_ITEM) {
                // hidden button resize when select multi baseItems
                buttonResize.style.display = 'none';
                return;
            }
            let img = buttonResize.children[0];
            
            const iconWidth = defaultWidth * this.state.view.scale;
            const spaceResize = this.state.view.scale < 1 ?  4/this.state.view.scale : 4;

            img.style.width = iconWidth + 'px';
            img.style.marginLeft = spaceResize + 'px';
            if (this.state.view.scale >= 1 || this.state.view.scale <= ZoomConfig.MIN_SCALE) {
                img.style.marginTop = spaceResize + 'px';
            } else {
                img.style.marginTop = '0px';
            }
            
            buttonResize.style.cursor = 'context-menu';
            buttonResize.style.position = "absolute";
            buttonResize.style.left = this.state.x + this.state.width - iconWidth - spaceResize + 'px';
            buttonResize.style.top = this.state.y + this.state.height - iconWidth - spaceResize + 'px';
            buttonResize.style.width = iconWidth + spaceResize + 'px';
            buttonResize.style.height = iconWidth + spaceResize + 'px';
            buttonResize.style.display = 'block';
        }
    }
    
    handleStartEditItem = (sender, evt) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Button_Edit_Text');
        let cell = evt.getProperty("cell");
        if (cell != null && this.graph.model.isVertex(cell) && cell.itemType === KItemType.BASE_ITEM_LABEL && this.buttonResizeSmall) {
            this.buttonResizeSmall.style.display = 'none';
        }
    }

    /**
     * NamNH: override function union mxVertexHandler class
     */
    union(bounds, dx, dy, index, gridEnabled, scale, tr, constrained, centered) {
        gridEnabled = (gridEnabled != null) ? gridEnabled && this.graph.gridEnabled : this.graph.gridEnabled;
	
        if (this.singleSizer) {
            let x = bounds.x + bounds.width + dx;
            let y = bounds.y + bounds.height + dy;
            
            if (gridEnabled)
            {
                x = this.graph.snap(x / scale) * scale;
                y = this.graph.snap(y / scale) * scale;
            }
            
            let rect = new mxRectangle(bounds.x, bounds.y, 0, 0);
            rect.add(new mxRectangle(x, y, 0, 0));
            
            return rect;
        }
        else
        {
            const cell = this.state.cell;
            let minHeight = cell.itemType === KItemType.BASE_ITEM ? KGraphUtil.MIN_BASE_ITEM_HEIGHT*scale : KGraphUtil.MIN_BASE_ITEM_LABEL_HEIGHT*scale
            let maxHeight = cell.itemType === KItemType.BASE_ITEM ? KGraphUtil.MAX_BASE_ITEM_HEIGHT*scale : KGraphUtil.MAX_BASE_ITEM_LABEL_HEIGHT*scale
            let minWidth = cell.itemType === KItemType.BASE_ITEM ? KGraphUtil.MIN_BASE_ITEM_WIDTH*scale : KGraphUtil.MIN_BASE_ITEM_LABEL_WIDTH*scale
            let maxWidth = cell.itemType === KItemType.BASE_ITEM ? KGraphUtil.MAX_BASE_ITEM_WIDTH*scale : KGraphUtil.MAX_BASE_ITEM_LABEL_WIDTH*scale
            let w0 = bounds.width;
            let h0 = bounds.height;
            let left = bounds.x - tr.x * scale;
            let right = left + w0;
            let top = bounds.y - tr.y * scale;
            let bottom = top + h0;
            let cx = left + w0 / 2;
            let cy = top + h0 / 2;
            let strokeHeight = (StageConfig.innerStrokeWidth*2)*scale;
            let strokeWidth = (StageConfig.innerStrokeWidth*2+4)*scale;
            
            if (index > 4 /* Bottom Row */)
            {
                bottom = bottom + dy;
                
                if (gridEnabled)
                {
                    bottom = this.graph.snap(bottom / scale) * scale;
                }
                else
                {
                    bottom = Math.round(bottom / scale) * scale;
                }
            }
            else if (index < 3 /* Top Row */)
            {
                top = top + dy;
                
                if (gridEnabled)
                {
                    top = this.graph.snap(top / scale) * scale;
                }
                else
                {
                    top = Math.round(top / scale) * scale;
                }
            }
            
            if (index === 0 || index === 3 || index === 5 /* Left */)
            {
                left += dx;
                
                if (gridEnabled)
                {
                    left = this.graph.snap(left / scale) * scale;
                }
                else
                {
                    left = Math.round(left / scale) * scale;
                }
            }
            else if (index === 2 || index === 4 || index === 7 /* Right */)
            {
                right += dx;
                
                if (gridEnabled)
                {
                    right = this.graph.snap(right / scale) * scale;
                }
                else
                {
                    right = Math.round(right / scale) * scale;
                }
            }

            if (right > StageConfig.MAX_WIDTH - strokeWidth) {
                right = StageConfig.MAX_WIDTH - strokeWidth;
            }
            if (bottom > StageConfig.MAX_HEIGHT - strokeHeight) {
                bottom = StageConfig.MAX_HEIGHT - strokeHeight;
            }

            let width = right - left;
            let height = bottom - top;
            
            if (constrained)
            {
                let geo = this.graph.getCellGeometry(this.state.cell);

                if (geo != null)
                {
                    let aspect = geo.width / geo.height;
                    
                    if (index === 1 || index === 2 || index === 7 || index === 6)
                    {
                        width = height * aspect;
                    }
                    else
                    {
                        height = width / aspect;
                    }
                    
                    if (index === 0)
                    {
                        left = right - width;
                        top = bottom - height;
                    }
                }
            }

            if (centered)
            {
                width += (width - w0);
                height += (height - h0);
                
                let cdx = cx - (left + width / 2);
                let cdy = cy - (top + height / 2);

                left += cdx;
                top += cdy;
                right += cdx;
                bottom += cdy;
            }

            // Flips over left side
            // NamNH: override dont flips 
            if (width < minWidth) {
                width = minWidth;
            } else if (width > maxWidth) {
                width = maxWidth;
            }
            
            // Flips over top side
            // NamNH: override dont flips 
            if (height < minHeight) {
                // top += height;
                height = minHeight;
            } else if (height > maxHeight) {
                height = maxHeight;
            }

            var result = new mxRectangle(left + tr.x * scale, top + tr.y * scale, width, height);
            
            if (this.minBounds != null)
            {
                result.width = Math.max(result.width, this.minBounds.x * scale + this.minBounds.width * scale +
                    Math.max(0, this.x0 * scale - result.x));
                result.height = Math.max(result.height, this.minBounds.y * scale + this.minBounds.height * scale +
                    Math.max(0, this.y0 * scale - result.y));
            }
            
            // không thay đổi vị trí khi resize
            result.x = bounds.x;
            result.y = bounds.y;

            return result;
        }
    }


    updateLivePreview(me) {
        super.updateLivePreview(me);
        const cell = this.state.cell;
        const buttonResize = cell.isBaseItemLabel === true ? this.buttonResizeSmall : this.buttonResize;
        const defaultWidth = cell.isBaseItemLabel === true ? 8 : 14;
        if (buttonResize) {
            buttonResize.style.left = this.bounds.x + this.bounds.width - defaultWidth + 'px';
            buttonResize.style.top = this.bounds.y + this.bounds.height - defaultWidth + 'px';
            if (cell instanceof KBaseItem) {
                this.itemMenuButtonBarHandler.hide();
            }
            
            if (cell.data) {
                cell.setVisibleCreatorLabel(false);
            }

            if (cell.selectionBorder) {
                let selectionBorder = this.graph.getView().getState(cell.selectionBorder);
                selectionBorder.shape.bounds.width = this.bounds.width + KGraphUtil.SELECTION_BORDER_PADDING*this.state.view.scale;
                selectionBorder.shape.bounds.height = this.bounds.height + KGraphUtil.SELECTION_BORDER_PADDING*this.state.view.scale;
                if (selectionBorder.shape) {
                    selectionBorder.shape.apply(selectionBorder);
                    selectionBorder.shape.redraw();
                }
            }
        } 
       
        if (Array.isArray(cell.children)){
            const childrenCount = cell.children.length;
            for( let i = 0; i < childrenCount; i ++){
                let state = this.state.view.states.get(cell.children[i]);
                this.bringPreviewToFront(state);
            }
        }
    }

    bringPreviewToFront(state){
        if ( !state ){
            return;
        }
        if ((state.text != null && state.text.node != null &&
            state.text.node.nextSibling != null) ||
            (state.shape != null && state.shape.node != null &&
            state.shape.node.nextSibling !== null && (state.text == null ||
            state.shape.node.nextSibling !== state.text.node)))
        {
            if (state.shape != null && state.shape.node != null)
            {
                state.shape.node.parentNode.appendChild(state.shape.node);
            }
            
            if (state.text != null && state.text.node != null)
            {
                state.text.node.parentNode.appendChild(state.text.node);
            }
        }
    }

    start(x, y, index) {
        super.start(x, y, index);
        this.livePreviewActive = true;
    }
    

    destroy(){
        this.graph.removeListener(this.handleStartEditItem);
        this.graph.removeListener(this.handleStopEditItem);
        const { graph } = this;
        const cell = this.state.cell
        let itemMenuButtonBar = document.getElementById("itemMenuButtonBar");

        if (cell.itemType === KItemType.DEFAULT_ITEM || cell.itemType === KItemType.BASE_ITEM) {
            cell.setVisibleSelectionBorder(false);
        }

        if (itemMenuButtonBar != null) {
            const cell = graph.getSelectionCell();

            if (!cell || cell.isVertex() !== true) {
                itemMenuButtonBar.style.display = "none";
            }
        }   

        
        this.setStateStyle(true);
        // update comment remove listener for element on destroy
        mxEvent.removeAllListeners(this.buttonResize);
        // mxEvent.removeAllListeners(this.editText);

        if (this.domNode != null) {
            this.domNode.parentNode.removeChild(this.domNode);
            this.domNode = null;
        }

        this.state.selectionShape = null;
        
        super.destroy();
    }

    /**
     * set style for cell state
     */
    setStateStyle(destroy = false){
        let {state} = this;
        let cell = state.cell
        if (cell.itemType === KItemType.BASE_ITEM_LABEL) {
            const baseItem = cell.baseItem;
            const basetItemStyle = this.graph.getCellStyle(baseItem);
            if (destroy === true) {
                state.style = mxUtils.clone(state.style);
                state.style[mxConstants.STYLE_STROKECOLOR] = "none"
                state.style[mxConstants.STYLE_FILLCOLOR] = "none"
            } else {
                state.style[mxConstants.STYLE_FILLCOLOR] = LightenDarkenColor(basetItemStyle[mxConstants.STYLE_FILLCOLOR], 90)
                state.style[mxConstants.STYLE_STROKECOLOR] =  KGraphUtil.getBaseItemLabelSelectionStyle(baseItem.data, mxConstants.STYLE_STROKECOLOR);
            }
            if (state.shape) {
                state.shape.apply(state);
                state.shape.redraw();
            }
        }
    }
};


export default ItemContextMenuHandler;