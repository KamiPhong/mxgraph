import KItemType from 'wlp-client-editor-lib/core/KItemType';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import {getExtensionFile, bytesToSize} from 'wlp-client-editor-lib/utils/ResourceThumbnailUtils';
import ContextMenuButtonBarHandler from "./ContextMenuButtonBarHandler";
import i18n from "i18n";
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import { addClassBatch, removeClassBatch } from 'wlp-client-common/utils/DOMUtils';
import * as GraphUtil from '../core/KGraphUtil';
import { THREAD_DELAY } from 'wlp-client-editor-lib/core/KConstants';
const wlpDateUtil = new WlpDateUtil();

const dNone = 'none';
const dBlock = 'block';
class ItemMenuButtonBarHandler extends ContextMenuButtonBarHandler {
    constructor(graph) {
        super(graph);
        this.itemMenuButtonBar = document.getElementById("itemMenuButtonBar");
        this.buttonRemove = document.getElementById("itemMenuButtonDelete");
        this.borderTopGroup = document.getElementById("itemMenuBorderTopGroupItem");
        this.borderRightGroup = document.getElementById("itemMenuBorderRightGroupItem");
        this.borderBottomGroup = document.getElementById("itemMenuBorderBottomGroupItem");
        this.borderLeftGroup = document.getElementById("itemMenuBorderLeftGroupItem");
        this.menuControl = document.getElementById("itemMenuControl");
        this.itemMenuUploadImg = document.getElementById("itemMenuUploadImg");
        this.itemMenuDownloadImg = document.getElementById("itemMenuDownloadImg");
        this.downloadControl = document.getElementById("download-control");
        this.btnsOpenShapeMenu = document.getElementsByClassName('open-shape-menu');
        this.btnsOpenStyleMenu = document.getElementsByClassName('open-style-menu');
        this.btnsOpenTextMenu = document.getElementsByClassName('open-text-menu');
        this.btnsUploadImage = document.getElementsByClassName('open-upload-image');
        this.itemProperties = document.getElementById('itemProperties');
        this.btnsOpenProperty = document.getElementsByClassName('open-property-menu');
        this.btnRemovePassword = document.getElementsByClassName('control-property');

        this.redrawThred = null;
    }

    handleZoom = () => {
        this.zoom = true;
        this.redrawTools();
    }

    handleItemChange = () => {
        this.zoom = true;
        this.redrawTools();
    }
    /**
     * show group menu
    */
    redrawThredHandler = (cells) => {
        const { graph, zoom, itemMenuButtonBar } = this;
        if (zoom === true && itemMenuButtonBar && itemMenuButtonBar.style.display === "none") {
            return;
        }

        if (graph && typeof graph.view !== "undefined") {
            const scale = graph.view.scale;
            this.scale = scale;
            // if cell is null try get cell from selection 
            if (!cells) {
                cells = graph.getSelectionItems();
            }

            if (Array.isArray(cells) && cells.length > 0) {
                const firstItem = cells[0];
                const data = firstItem.data;
                const disableClass = 'disabled';
                itemMenuButtonBar.style.display = dBlock;
                itemMenuButtonBar.style.transform = `scale(${scale})`;
                const stateCells = graph.getView().getCellStates(cells);
                
                let scalePadding = (scale > 1.25) ? scale*0.75 : (scale < 0.75) ?  scale*1.25 : (scale <= 0.65) ? scale*1.5 : scale;
                const padding = 10*scalePadding;
                let xRight = 0;
                let yRight = 0;
                let xLeft = 0;
                let yLeft = 0;
                let w = 0;
                let h = 0;
                let x = 0;
                let y = 0;
                let width = 0;
                let height = 0;

                if (stateCells.length === 1) {
                    let stCell = stateCells[0];
                    x = stCell.x;
                    y = stCell.y;
                    width = stCell.width;
                    height = stCell.height;
                } else if (stateCells.length > 1) {
                    for (let i = 0; i < stateCells.length; i++) {
                        let state = stateCells[i];
                        let statex = state.x
                        let statey = state.y

                        let statexW = state.x + state.width;
                        let statexH = state.y + state.height;
                        if (xRight < statexW) {
                            xRight = statexW;
                            w = state.width;
                        }
                        if (yRight < statexH) {
                            yRight = statexH;
                            h = state.height;
                        }
                        if (xLeft === 0 || xLeft > statex) {
                            xLeft = statex;
                        }
                        if (yLeft === 0 || yLeft > statey) {
                            yLeft = statey;
                        }
                    }

                    width = xRight - xLeft;
                    height = yRight - yLeft;
                    x = xLeft;
                    y = yLeft;
                }
                // console.log({x,y, width, height})
                // console.log("stateCells", stateCells)


                if (cells.length > 1 && this.borderTopGroup) {
                    let borderScale = (scale >=1.5) ? scale*0.75 : (scale < 0.75) ? scale*1.25 : scale;
                    const styleborder = 6 * borderScale + 'px solid #e2d1db';
                    const stylePosition = 'absolute';
                    const styleRadius = 4 * scale + 'px';

              
                    this.borderTopGroup.style.width = width/scale + padding*1.8 + 'px';
                    this.borderTopGroup.style.left = x/scale - padding + 'px';
                    this.borderTopGroup.style.top = y/scale - padding + 'px';
                    this.borderTopGroup.style.borderTop = styleborder;
                    this.borderTopGroup.style.display = dBlock;
                    this.borderTopGroup.style.position = stylePosition;
                    this.borderTopGroup.style.borderRadius = styleRadius;

                    
                    this.borderRightGroup.style.height = height/scale + padding*2 + 'px';
                    this.borderRightGroup.style.left = x/scale + width/scale + padding/2 + 'px';
                    this.borderRightGroup.style.top = y/scale - padding + 'px';
                    this.borderRightGroup.style.borderLeft = styleborder;
                    this.borderRightGroup.style.display = dBlock;
                    this.borderRightGroup.style.position = stylePosition;
                    this.borderRightGroup.style.borderRadius = styleRadius;

                    
                    this.borderBottomGroup.style.width = width/scale + padding*2 + 'px';
                    this.borderBottomGroup.style.left = x/scale - padding  + 'px';
                    this.borderBottomGroup.style.top = y/scale + height/scale + padding/2 + 'px';
                    this.borderBottomGroup.style.borderBottom = styleborder;
                    this.borderBottomGroup.style.display = dBlock;
                    this.borderBottomGroup.style.position = stylePosition;
                    this.borderBottomGroup.style.borderRadius = styleRadius;

                    
                    this.borderLeftGroup.style.height = height/scale + padding*1.8 + 'px';
                    this.borderLeftGroup.style.left = x/scale - padding  + 'px';
                    this.borderLeftGroup.style.top = y/scale - padding + 'px';
                    this.borderLeftGroup.style.borderLeft = styleborder;
                    this.borderLeftGroup.style.display = dBlock;
                    this.borderLeftGroup.style.position = stylePosition;
                    this.borderLeftGroup.style.borderRadius = styleRadius;

                } else {
                    this.borderTopGroup.style.display = dNone;
                    this.borderRightGroup.style.display = dNone;
                    this.borderBottomGroup.style.display = dNone;
                    this.borderLeftGroup.style.display = dNone;
                }

                // style button remove
                if (this.buttonRemove) {
                    this.buttonRemove.style.left = (x + width) / scale - 3 + 'px';
                    this.buttonRemove.style.top = y/scale - 12.5 + 'px';
                    this.buttonRemove.style.width = 16 + 'px';
                    this.buttonRemove.style.cursor = 'context-menu';
                    this.buttonRemove.style.position = "absolute";
                    this.buttonRemove.style.display = dBlock;
                    this.buttonRemove.style.opacity = 0.8;
                }
                

                // style menu control
                if (this.menuControl) {
                    this.menuControl.style.left = `${(x / scale) - GraphUtil.CONTEXT_MENU_MOVE_LEFT}px`;
                    this.menuControl.style.top = (y + height ) / scale + GraphUtil.CONTEXT_MENU_MOVE_TOP + 'px';
                    this.menuControl.style.cursor = 'context-menu';
                    this.menuControl.style.position = "absolute";
                    this.menuControl.style.display = dBlock;
                    // this.menuControl.style.height = 23 * scale + "px";

                    // set show/hide item/baseitem action
                    let itemDisplay = dBlock;
                    let baseItemDisplay = dNone;
                    if (firstItem.itemType === KItemType.DEFAULT_ITEM) {
                        itemDisplay = dBlock;
                        baseItemDisplay = dNone;
                        this.itemMenuUploadImg.style.opacity = 1;
                    } else {
                        itemDisplay = dNone;
                        baseItemDisplay = dBlock;
                    }

                    document.getElementsByClassName('item-options').forEach(el => {
                        el.style.display = itemDisplay;
                    });
                    
                    document.getElementsByClassName('baseitem-option').forEach(el => {
                        el.style.display = baseItemDisplay;
                    });

                    // set postion list option menu
                    const menuControlActions = this.menuControl.getElementsByClassName('menu-control-action');
                    if (firstItem.itemType === KItemType.DEFAULT_ITEM) {
                        removeClassBatch(menuControlActions, 'base-item-style');
                    } else {
                        addClassBatch(menuControlActions, 'base-item-style');
                    }


                    this.menuControl.getElementsByClassName('context-action').forEach((item) => {
                        this.setActiveOption(firstItem, item);
                    })
                    
                    if (firstItem.itemType === KItemType.DEFAULT_ITEM && data && data.attachFile) {
                        this.itemMenuUploadImg.style.display = dNone;
                        this.itemMenuDownloadImg.style.display = dBlock;
                        this.downloadControl.setAttribute("aria-label", i18n.t('editor.itemMenu.download'));
                    } else {
                        this.itemMenuUploadImg.style.display = dBlock;
                        this.itemMenuDownloadImg.style.display = dNone;
                        this.downloadControl.setAttribute("aria-label", i18n.t('editor.itemMenu.file'));
                    }

                }

                let isHasRoleWithItem = true;
                const cellsCount = cells.length;

                if (firstItem.itemType === KItemType.DEFAULT_ITEM){
                    for (let i = 0; i < cellsCount; i ++) {
                        if (cells[i] && cells[i].hasRole === false) {
                            isHasRoleWithItem = false;
                            break;
                        }
                    }
                }

                //[ThuyTv]: if user does'nt have role with item
                //disable all actions except property
                if (
                    isHasRoleWithItem === false &&
                    firstItem.itemType === KItemType.DEFAULT_ITEM
                ) {
                    addClassBatch(this.btnsOpenShapeMenu, disableClass);
                    addClassBatch(this.btnsOpenStyleMenu, disableClass);
                    addClassBatch(this.btnsOpenTextMenu, disableClass);
                    addClassBatch(this.btnsUploadImage, disableClass);
                    addClassBatch(this.btnRemovePassword, disableClass);
                    this.buttonRemove.style.display = dNone;
                } else {
                    removeClassBatch([this.itemMenuUploadImg], disableClass);
                    removeClassBatch(this.btnsOpenShapeMenu, disableClass);
                    removeClassBatch(this.btnsOpenStyleMenu, disableClass);
                    removeClassBatch(this.btnsOpenTextMenu, disableClass);
                    removeClassBatch(this.btnsUploadImage, disableClass);
                    removeClassBatch(this.btnRemovePassword, disableClass);

                    //[ThuyTv]: if selection cells is more than one
                    // disable upload and property btns
                    if (cells.length > 1) {
                        addClassBatch(this.btnsUploadImage, disableClass);
                        addClassBatch(this.btnsOpenProperty, disableClass);
                    } else {
                        removeClassBatch(this.btnsOpenProperty, disableClass);
                        removeClassBatch(this.btnsUploadImage, disableClass);
                    }

                    //[ThuyTv]: cannot upload image at base item
                    if (firstItem.itemType === KItemType.BASE_ITEM){
                        addClassBatch(this.btnsUploadImage, disableClass);
                        let btnRemoveDisplay = dBlock;
                        for (let i = 0; i < cellsCount; i ++) {
                            if (cells[i] && cells[i].hasRole === false) {
                                btnRemoveDisplay = dNone;
                                break;
                            }
                        }
                        this.buttonRemove.style.display = btnRemoveDisplay;
                    }  
                }

                

                // setting menu property
                if (cellsCount === 1 && data && this.itemProperties) {
                    const itemModel = data;
                    const attachFile = itemModel.attachFile;
                    const isItem = firstItem.itemType === KItemType.DEFAULT_ITEM ? true : false;
                    let attachFileContentElement = this.itemProperties.getElementsByClassName('attachFile-content')[0];
                    let itemTitleProperty = this.itemProperties.getElementsByClassName('item-title-property')[0];
                    let settingPassword = this.itemProperties.getElementsByClassName('setting-password')[0];
                    let removePassword = this.itemProperties.getElementsByClassName('remove-password')[0];
                    
                    itemTitleProperty.innerHTML = isItem ? i18n.t('editor.itemPropertiy.base.itemLabel') : i18n.t('editor.itemPropertiy.base.baseItemLabel');

                    if (attachFile) {
                        attachFileContentElement.style.display = "block";
                    } else {
                        attachFileContentElement.style.display = "none";
                    }

                    this.itemProperties.getElementsByClassName('fill-data').forEach((element) => {
                        if (element.classList.contains('property-created-on')) {
                            element.innerHTML = (i18n.language === LocaleConst.en_US) ? wlpDateUtil.format(itemModel.createTime, "MMM dd, yyyy HH:mm", false) : wlpDateUtil.format(itemModel.createTime, "yyyy/MM/dd  HH:mm", false);
                        }

                        if (element.classList.contains('property-Last-update')) {
                            element.innerHTML = (i18n.language === LocaleConst.en_US) ? wlpDateUtil.format(itemModel.updateTime, "MMM dd, yyyy HH:mm", false) : wlpDateUtil.format(itemModel.updateTime, "yyyy/MM/dd  HH:mm", false);
                        }

                        if (attachFile && isItem) {
                            if (element.classList.contains('file-name')) {
                                element.innerHTML = attachFile.name.length > 14 ? attachFile.name.slice(0, 14) + "..." : attachFile.name;
                            }
    
                            if (element.classList.contains('file-extension')) {
                                element.innerHTML = getExtensionFile(attachFile.name);
                            }
    
                            if (element.classList.contains('file-size')) {
                                element.innerHTML = bytesToSize(attachFile.size);
                            }
                        }
                    })

                    if (isItem) {
                        if (itemModel.hasAttachFilePassword) {
                            settingPassword.style.display = "none";
                            removePassword.style.display = "block";
                        } else {
                            settingPassword.style.display = "block";
                            removePassword.style.display = "none";
                        }
                    } else {
                        settingPassword.style.display = "none";
                        removePassword.style.display = "none";
                    }
                }
            }
        }
    }

    redrawTools = (cells) => {
        let isMovingCells = this.graph.graphHandler.isMovingCells;
        let handlerCells = this.graph.graphHandler.cells
        if (handlerCells && handlerCells.length === 1 && handlerCells[0].itemType === KItemType.BASE_ITEM_LABEL) {
            isMovingCells = false;
        }
        this.redrawThred = setTimeout(() => {
            if (!isMovingCells) {
                this.redrawThredHandler(cells);
            }
        }, THREAD_DELAY);;
    }


    setActiveOption = (cell, item) => {
        let data = cell.data;
        if (data) {
            let shape = data.shape;
            let style = data.style;
            let textalign = data.textHorizontalAlign;
            let textvalign = data.textVerticalAlign;

            let isActive = false; 

            if (typeof item.dataset.shape !== 'undefined') {
                if (parseInt(item.dataset.shape) === shape) {
                    isActive = true;
                }
            } else if (typeof item.dataset.style !== 'undefined') {
                if (parseInt(item.dataset.style) === style) {
                    isActive = true;
                }
            } else if (typeof item.dataset.textalign !== 'undefined') {
                if (parseInt(item.dataset.textalign) === textalign) {
                    isActive = true;
                }
            } else if (typeof item.dataset.textvalign !== 'undefined') {
                if (parseInt(item.dataset.textvalign) === textvalign) {
                    isActive = true;
                }
            }

            if (isActive) {
                addClassBatch([item], 'actived');
            } else {
                removeClassBatch([item], 'actived');
            }
        }
    }
    /**
     * hidden group menu
    */
    hide = () => {
        if (this.itemMenuButtonBar && this.itemMenuButtonBar.style.display !== dNone) {
            this.itemMenuButtonBar.style.display = dNone;
        }
    }
}

export default ItemMenuButtonBarHandler;