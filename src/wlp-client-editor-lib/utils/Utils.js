
import { } from './HTMLElementPrototype';
import KEYCODE from './KeyCodeConst';
import { mxUtils, mxClient } from 'wlp-client-editor-lib/core/KClient';
import HtmlCode from '../consts/HtmlCode';

const ENV = {
    PRODUCTION: 'production',
    STAGING: 'staging',
    LOCAL: 'logcal',
}
export {
    KEYCODE
};

export function isProductEnv() {
    if (process.env.REACT_APP_ENV === ENV.PRODUCTION || process.env.REACT_APP_ENV === ENV.STAGING) {
        return true;
    }
    return false;
}

export function unEscapeHtml(string) {
    string = string.replace(/(<([^>]+)>)/ig, '');
    
    return string
        .replace(/&amp;/g, "&")
        .replace(/&lt;/g, "<")
        .replace(/&gt;/g, ">")
        .replace(/&quot;/g, "\"")
        .replace(/&#039;/g, "'");
}

/**
 * jp.co.kokuyo.wlp.editor.component.Item::setItemText
 * @param {*} string 
 */
export function convertToHtmlEntities( string ){
    string = String(string || '');
    let onlyBreakLine = false;
    // const lastCharIsLineBreak = (string.charAt(string.length - 1).match(/\n/g)||[]).length;
    const numberOfLineBreaks = (string.match(/\n/g)||[]).length;
    if (numberOfLineBreaks === string.length && string.length > 0) {
        onlyBreakLine = true;
    }


    // Workaround for trailing line breaks being ignored in the editor
    if (!mxClient.IS_QUIRKS && document.documentMode !== 8 && document.documentMode !== 9 &&document.documentMode !== 10){
        string = mxUtils.replaceTrailingNewlines(string, '<div><br></div>');
    }
    
    string = string.replace(/\n/gi, '<br>');
    
    const regexAnchor = new RegExp(/<a [^>]+>(.*?)<\/a>/, "g");
    let regex;
    if( regexAnchor.test(string) ){
        regex = new RegExp(/<a [^>]+>\u0020<\/a>/, "g");
    } else {
        regex = new RegExp(/\u0020/, "g");
    }
    // thêm ký tự khoảng trắng để thẻ HTML hiểu được là khoảng trắng để bôi đen và di chuyển chuột theo khoảng trắng
    string =  string.replace(regex, HtmlCode.SPACE)
    // if string has only breakline, add breakline for first char
    if (onlyBreakLine) {
        string = '<div><br></div>' + string;
    }

    return string;
}