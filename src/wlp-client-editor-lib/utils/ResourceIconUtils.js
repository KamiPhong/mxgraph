import imgNaviHeaderHide from 'wlp-client-editor-lib/assets/images/navi-header-hide.png'
import imgNaviHeaderShow from 'wlp-client-editor-lib/assets/images/navi-header-show.png'

import imgShapeOption from 'wlp-client-editor-lib/assets/images/context-style.png'
import imgContextText from 'wlp-client-editor-lib/assets/images/context-text.png'
import imgStyleOption from 'wlp-client-editor-lib/assets/images/context-background.png'
import imgContextUpload from 'wlp-client-editor-lib/assets/images/context-upload.png'
import imgContextDownload from 'wlp-client-editor-lib/assets/images/context-download.png'
import imgContextUploadDisabled from 'wlp-client-editor-lib/assets/images/context-upload-disabled.png'
import imgContextSetting from 'wlp-client-editor-lib/assets/images/context-setting.png'
import imgContextSettingDisabled from 'wlp-client-editor-lib/assets/images/context-setting-disabled.png'

import imgRectangle from 'wlp-client-editor-lib/assets/images/cell/rectangle.png'
import imgEllipse from 'wlp-client-editor-lib/assets/images/cell/ellipse.png'
import imgRectangleRight from 'wlp-client-editor-lib/assets/images/cell/rectangle-right.png'
import imgRectangleLeft from 'wlp-client-editor-lib/assets/images/cell/rectangle-left.png'
import imgRhombus from 'wlp-client-editor-lib/assets/images/cell/rhombus.png'
import imgCloud from 'wlp-client-editor-lib/assets/images/cell/cloud.png'
import imgBaseTriangle from 'wlp-client-editor-lib/assets/images/cell/base_triangle.png'
import imgBaseRect from 'wlp-client-editor-lib/assets/images/cell/base_rect.png'
// import imgBaseRoundRect from 'wlp-client-editor-lib/assets/images/cell/rectangle.png'
// import imgBaseEllipse from 'wlp-client-editor-lib/assets/images/cell/ellipse.png'
import imgEllipseDraw from 'wlp-client-editor-lib/assets/images/cell/ellipse-draw.png'
import imgRectangleDraw from 'wlp-client-editor-lib/assets/images/cell/rectangle-draw.png'
import imgClose from 'wlp-client-editor-lib/assets/images/close.png'
import imgEditText from 'wlp-client-editor-lib/assets/images/edit-text.png'
import imgResize from 'wlp-client-editor-lib/assets/images/icon-resize.png'
import imgResizeSmall from 'wlp-client-editor-lib/assets/images/icon-resize-small.png'
import imgResizeSmallTransparent from 'wlp-client-editor-lib/assets/images/icon-resize-small-transparent.png'
import iconLoadingProgress from 'wlp-client-editor-lib/assets/images/spiner-icon.png'
import imgSheetBlank from 'wlp-client-editor-lib/assets/images/sheet_blank.png'

/**
 * image item style
 */
import imgStyleA from 'wlp-client-editor-lib/assets/images/style/a.png';
import imgStyleB from 'wlp-client-editor-lib/assets/images/style/b.png';
import imgStyleC from 'wlp-client-editor-lib/assets/images/style/c.png';
import imgStyleD from 'wlp-client-editor-lib/assets/images/style/d.png';
import imgStyleE from 'wlp-client-editor-lib/assets/images/style/e.png';
import imgStyleF from 'wlp-client-editor-lib/assets/images/style/f.png';
import imgStyleG from 'wlp-client-editor-lib/assets/images/style/g.png';
import imgStyleH from 'wlp-client-editor-lib/assets/images/style/h.png';
import imgStyleI from 'wlp-client-editor-lib/assets/images/style/i.png';
import imgStyleJ from 'wlp-client-editor-lib/assets/images/style/j.png';
import imgStyleBA from 'wlp-client-editor-lib/assets/images/style/B_A.png';
import imgStyleBB from 'wlp-client-editor-lib/assets/images/style/B_B.png';
import imgStyleBC from 'wlp-client-editor-lib/assets/images/style/B_C.png';
import imgStyleBD from 'wlp-client-editor-lib/assets/images/style/B_D.png';
import imgStyleBE from 'wlp-client-editor-lib/assets/images/style/B_E.png';

/**
 * text align
 */
import downText from 'wlp-client-editor-lib/assets/images/text/down_text.png'
import upText from 'wlp-client-editor-lib/assets/images/text/up_text.png'
import textBottom from 'wlp-client-editor-lib/assets/images/text/text_bottom.png'
import textCenter from 'wlp-client-editor-lib/assets/images/text/text_center.png'
import textLeft from 'wlp-client-editor-lib/assets/images/text/text_left.png'
import textMiddle from 'wlp-client-editor-lib/assets/images/text/text_middle.png'
import textRight from 'wlp-client-editor-lib/assets/images/text/text_right.png'
import textTop from 'wlp-client-editor-lib/assets/images/text/text_top.png'

import imgEditUserIcon from 'wlp-client-editor-lib/assets/images/edit-user.png'
import imgUpdateUserIcon from 'wlp-client-editor-lib/assets/images/update-user.png'

import itemLineTypeOption from 'wlp-client-editor-lib/assets/images/itemLineType.png';
import itemLineWeightOption from 'wlp-client-editor-lib/assets/images/itemLineWeight.png';
import itemLineCloseOption from 'wlp-client-editor-lib/assets/images/itemLineClose.png';

import lineTypeLinearNone from 'wlp-client-editor-lib/assets/images/style/LINEAR_NONE.png';
import lineTypeLinearFromTo from 'wlp-client-editor-lib/assets/images/style/LINEAR_FROM_TO.png';
import lineTypeLinearToFrom from 'wlp-client-editor-lib/assets/images/style/LINEAR_TO_FROM.png';
import lineTypeLinearOneWayBidirectional from 'wlp-client-editor-lib/assets/images/style/LINEAR_ONE_WAY_BIDIRECTIONAL.png';
import lineTypeSquareNone from 'wlp-client-editor-lib/assets/images/style/SQUARE_NONE.png';
import lineTypeSquareFromTo from 'wlp-client-editor-lib/assets/images/style/SQUARE_FROM_TO.png';
import lineTypeSquareToFrom from 'wlp-client-editor-lib/assets/images/style/SQUARE_TO_FROM.png';
import lineTypeSquareOneWayBidirectional from 'wlp-client-editor-lib/assets/images/style/SQUARE_ONE_WAY_BIDIRECTIONAL.png';
import lineTwoWay from 'wlp-client-editor-lib/assets/images/style/TWO_WAY.png';
import lineTwoWayReverse from 'wlp-client-editor-lib/assets/images/style/TWO_WAY_REVERSE.png';


import ItemLineColorTypeA from 'wlp-client-editor-lib/assets/images/style/line_color_a.png';
import ItemLineColorTypeB from 'wlp-client-editor-lib/assets/images/style/line_color_b.png';
import ItemLineColorTypeC from 'wlp-client-editor-lib/assets/images/style/line_color_c.png';
import ItemLineColorTypeD from 'wlp-client-editor-lib/assets/images/style/line_color_d.png';
import ItemLineColorTypeE from 'wlp-client-editor-lib/assets/images/style/line_color_e.png';

import ItemLineWeightSmall from 'wlp-client-editor-lib/assets/images/weight/LINE_WEIGHT_SMALL.png';
import ItemLineWeightMiddle from 'wlp-client-editor-lib/assets/images/weight/LINE_WEIGHT_MIDDLE.png';
import ItemLineWeightLarge from 'wlp-client-editor-lib/assets/images/weight/LINE_WEIGHT_LARGE.png';

export const BASE64_IMAGE_1PX = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQYV2P4////fwAJ+wP9BUNFygAAAABJRU5ErkJggg==";
export const DEFAULT_STEP_SIZE = 25;

export {
    imgNaviHeaderHide,
    imgNaviHeaderShow,
    imgShapeOption,
    imgContextText,
    imgStyleOption,
    imgContextUpload,
    imgContextDownload,
    imgContextSetting,
    imgRectangle,
    imgEllipse,
    imgRectangleRight,
    imgRectangleLeft,
    imgRhombus,
    imgCloud,
    imgEllipseDraw,
    imgRectangleDraw,
    imgClose,
    imgEditText,
    imgResize,
    imgBaseTriangle,
    imgBaseRect,
    imgResizeSmall,
    imgResizeSmallTransparent,
    iconLoadingProgress,
    imgSheetBlank,
    // imgBaseRoundRect,
    // imgBaseEllipse,

    imgStyleA,
    imgStyleB,
    imgStyleC,
    imgStyleD,
    imgStyleE,
    imgStyleF,
    imgStyleG,
    imgStyleH,
    imgStyleI,
    imgStyleJ,
    imgStyleBA,
    imgStyleBB,
    imgStyleBC,
    imgStyleBD,
    imgStyleBE,

    downText,
    upText,
    textBottom,
    textCenter,
    textLeft,
    textMiddle,
    textRight,
    textTop,

    imgEditUserIcon,
    imgUpdateUserIcon,

    imgContextUploadDisabled,
    imgContextSettingDisabled,

    itemLineTypeOption,
    itemLineWeightOption,
    itemLineCloseOption,

    lineTypeLinearNone,
    ItemLineColorTypeA,
    ItemLineColorTypeB,
    ItemLineColorTypeC,
    ItemLineColorTypeD,
    ItemLineColorTypeE,
    lineTypeLinearFromTo,
    lineTypeLinearToFrom,
    lineTypeLinearOneWayBidirectional,
    lineTypeSquareNone,
    lineTypeSquareFromTo,
    lineTypeSquareToFrom,
    lineTypeSquareOneWayBidirectional,
    lineTwoWay,
    lineTwoWayReverse,

    ItemLineWeightSmall,
    ItemLineWeightMiddle,
    ItemLineWeightLarge,
}