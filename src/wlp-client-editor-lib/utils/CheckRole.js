import ContractType from 'wlp-client-service/consts/ContractType';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";


const localStorage = new LocalStorageUtils();


/**
 * TODO: migrate from flash [wlp-clien-editor/src/MainBase.as][5421]
 * 指定アイテムモデルに対してユーザーが権限を持つか査定する。
 */
function hasRoleByModel (model, mapId) {
    const mapUserInfo = localStorage.getMapUserInfo();
    if (!localStorage.getUserSession() || !mapUserInfo || !Array.isArray(mapUserInfo)) {
        return false;
    }

    const { sessionDto } = localStorage.getUserSession();
    if (typeof model === 'undefined' || !sessionDto) {
        return false;
    }

    if (typeof model.createUserId !== 'undefined') {
        const commonUserInfo = mapUserInfo.filter(userInfo => userInfo.mapId === mapId)[0]
        if (typeof commonUserInfo === "undefined" || !commonUserInfo) {
            return false;
        }
        const currentContractUserRole = commonUserInfo.contractUserRole;
        if (sessionDto.userId === model.createUserId) {
            return true;
        } else if (commonUserInfo.contractType === ContractType.personal) {
            //B2C
            if (commonUserInfo.contractUserRole !== RoleType.B_TO_C_OWNER) {
                //オーナーではない
                return false;
            } else {
                //オーナー
                return true;
            }
        } else if (currentContractUserRole !== RoleType.LEADER && currentContractUserRole !== RoleType.ADMIN) {
            //B2B ヒラ
            return false;
        } else {
            //管理責任者 or 管理担当者
            return true;
        }
    }
    //論外
    return false;
}

export { hasRoleByModel };