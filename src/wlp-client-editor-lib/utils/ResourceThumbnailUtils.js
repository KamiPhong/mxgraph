const HTML = "/static/media/icon_html.png";
const XML = "/static/media/icon_xml.png";
const TXT = "/static/media/icon_txt.png";
const CSS = "/static/media/icon_css.png";
const JS = "/static/media/icon_js.png";
const BAT = "/static/media/icon_bat.png";
const VBS = "/static/media/icon_vbs.png";
const PPT = "/static/media/icon_ppt.png";
const DOC = "/static/media/icon_doc.png";
const XLS = "/static/media/icon_xls.png";
const PDF = "/static/media/icon_pdf.png";
const EPS = "/static/media/icon_eps.png";
const AI = "/static/media/icon_ai.png";
const PSD = "/static/media/icon_psd.png";
const SVG = "/static/media/icon_svg.png";
const GIF = "/static/media/icon_gif.png";
const JPG = "/static/media/icon_jpg.png";
const PNG = "/static/media/icon_png.png";
const BMP = "/static/media/icon_bmp.png";
const TIFF = "/static/media/icon_tiff.png";
const PIC = "/static/media/icon_pic.png";
const ICO = "/static/media/icon_ico.png";
const FLA = "/static/media/icon_fla.png";
const FLV = "/static/media/icon_flv.png";
const MOV = "/static/media/icon_mov.png";
const MPG = "/static/media/icon_mpg.png";
const WMV = "/static/media/icon_wmv.png";
const SWF = "/static/media/icon_swf.png";
const AVI = "/static/media/icon_avi.png";
const MP3 = "/static/media/icon_mp3.png";
const MP4 = "/static/media/icon_mp4.png";
const WAV = "/static/media/icon_wav.png";
const WMA = "/static/media/icon_wma.png";
const MIDI = "/static/media/icon_midi.png";
const AIFF = "/static/media/icon_aiff.png";
const LZH = "/static/media/icon_lzh.png";
const ZIP = "/static/media/icon_zip.png";
const TAR = "/static/media/icon_tar.png";
const JAVA = "/static/media/icon_java.png";
const EXE = "/static/media/icon_exe.png";
const DLL = "/static/media/icon_dll.png";
const CSV = "/static/media/icon_csv.png";
const BIN = "/static/media/icon_bin.png";
const NONE = "/static/media/icon_none.png";

export function isImageFile(fileName) {
    const ext = getExtensionFile(fileName);
    switch(ext) {
        case "gif":
        case "jpg":
        case "jpeg":
        case "png":
            return true;
        default:
            return false;
    }
}
export function getExtensionFile(fileName) {
    return fileName.split('.').pop();;
}

export function bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
    if (bytes < 1) {
        return "0B";
    }

    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
    
    if (i === 0) return `${bytes} ${sizes[i]})`

    return `${(bytes / (1024 ** i)).toFixed(2)} ${sizes[i]}`
}

export function getLargeIcon(fileName) {
    let result = "";
    const ext = getExtensionFile(fileName);

    switch (ext) {
        case "html":
        case "htm":
            result = HTML;
            break;
        case "xml":
            result = XML;
            break;
        case "txt":
            result = TXT;
            break;
        case "css":
            result = CSS;
            break;
        case "js":
            result = JS;
            break;
        case "bat":
            result = BAT;
            break;
        case "vbs":
            result = VBS;
            break;
        case "ppt":
        case "pptx":
            result = PPT;
            break;
        case "doc":
        case "docx":
            result = DOC;
            break;
        case "xls":
        case "xlsx":
            result = XLS;
            break;
        case "pdf":
            result = PDF;
            break;
        case "eps":
            result = EPS;
            break;
        case "ai":
            result = AI;
            break;
        case "psd":
            result = PSD;
            break;
        case "svg":
            result = SVG;
            break;
        case "gif":
            result = GIF;
            break;
        case "jpg":
        case "jpeg":
            result = JPG;
            break;
        case "png":
            result = PNG;
            break;
        case "bmp":
            result = BMP;
            break;
        case "tiff":
            result = TIFF;
            break;
        case "pic":
            result = PIC;
            break;
        case "ico":
            result = ICO;
            break;
        case "fla":
            result = FLA;
            break;
        case "flv":
            result = FLV;
            break;
        case "mov":
            result = MOV;
            break;
        case "mpg":
        case "mpeg":
            result = MPG;
            break;
        case "wmv":
            result = WMV;
            break;
        case "swf":
            result = SWF;
            break;
        case "avi":
            result = AVI;
            break;
        case "mp3":
            result = MP3;
            break;
        case "mp4":
            result = MP4;
            break;
        case "wav":
            result = WAV;
            break;
        case "wma":
            result = WMA;
            break;
        case "midi":
            result = MIDI;
            break;
        case "aiff":
            result = AIFF;
            break;
        case "lzh":
            result = LZH;
            break;
        case "zip":
            result = ZIP;
            break;
        case "tar":
            result = TAR;
            break;
        case "java":
            result = JAVA;
            break;
        case "exe":
            result = EXE;
            break;
        case "dll":
            result = DLL;
            break;
        case "csv":
            result = CSV;
            break;
        case "bin":
            result = BIN;
            break;
        default:
            result = NONE;
            break;
    }
    return result;
} 
