
class ObjectPrototype {
  appendPrototype=(object)=>{
    const prototype = this.__proto__;
    for (var funcName in prototype){
      object.__proto__[funcName] = prototype[funcName]
    }
  }
};

ObjectPrototype.prototype.bindOne = function (eventName, callback) {
  var { callbackDict } = this;

  if (typeof callbackDict === 'undefined') {
    this.callbackDict = {};
  }

  if (typeof this.callbackDict[eventName] === 'undefined' || this.callbackDict[eventName].length > 0 ) {
    this.callbackDict[eventName] = [];
  }
  this.callbackDict[eventName].push(callback);
}


ObjectPrototype.prototype.bind = function(eventName, callback){
  var {callbackDict} = this;

  if( typeof callbackDict === 'undefined'){
    this.callbackDict = {};
  }

  if( typeof this.callbackDict[eventName] === 'undefined' ){
    this.callbackDict[eventName] = [];
  }
  this.callbackDict[eventName].push(callback);
}

ObjectPrototype.prototype.trigger = function(eventName){
  const that = this;
  let params = [];
  for (var i = 1; i < arguments.length; i++) {
    params.push(arguments[i])
  }

  if( typeof that.callbackDict !== 'undefined' && typeof that.callbackDict[eventName] !== 'undefined' ){
    that.callbackDict[eventName].forEach( callback=>{
      if( typeof callback === 'function'){
        callback.apply(null,params)
      }
    });
  }
}

ObjectPrototype.prototype.unbind = function(eventName){
  if( typeof this.callbackDict[eventName] !== 'undefined' ){
    this.callbackDict[eventName] = [];
  }
  return this;
}

ObjectPrototype.prototype.importFromObject = function (object) {
  if (typeof this.dataSource !== 'undefined' && this.dataSource) {
    object = this.dataSource;
  }
  for (var property in object) {
    if ( this.hasOwnProperty(property)) {
      this[property] = object[property];
    }
  }
}


export default new ObjectPrototype();