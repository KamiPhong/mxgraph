class PointUtils {

    /**
     * @author ThuyTV
     * @description calculate distance between two points 
     * @param {mxPoint} point1
     * @param {mxPoint} point2
     * @return {float}
     */
    distance = (point1, point2) => {
        return Math.sqrt((point2.x - point1.x) ** 2 + (point2.y - point1.y) ** 2)
    }

    trueDistance = (point1, point2) => {
        return (point2.x - point1.x) + (point2.y - point1.y);
    }
}

export default PointUtils;