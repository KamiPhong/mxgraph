HTMLElement.prototype.hasClass = function (className) {
    if (typeof this.className !== 'undefined') {
        return this.className.includes(className)
    }
    return false;
}