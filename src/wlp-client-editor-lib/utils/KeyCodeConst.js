const KeyCodeConst = {
    ENTER: 13,
    DELETE: 46,
    V: 86,
    C: 67,
    BACKSPACE: 8,
    SPACE: 32,
    ARROW_RIGHT: 39,
    ARROW_LEFT: 37,
    TAB: 9,
};

export default KeyCodeConst;