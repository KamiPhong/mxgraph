
import { mxConstants } from 'wlp-client-editor-lib/core/KClient';
import KItemType from 'wlp-client-editor-lib/core/KItemType';
import * as KGraphUtil from '../core/KGraphUtil';
import _ from 'lodash';

/**
 * 
 * @param {mxGraph} graph 
 * @param {mxCell} cell 
 * @param {string} value 
 */
export function getCellSize(graph, cell, value){
	const baseValue = cell.value;
	if( typeof value === 'string'){
		value = value.replace(/<br>/g, "\n");
		cell.setValue(value);
	}

	let size = graph.getPreferredSizeForCell(cell);
	if (cell.itemType === KItemType.DEFAULT_ITEM) {
		if (!cell.value) {
			size.width = KGraphUtil.DEFAULT_ITEM_WIDTH;
			size.height = KGraphUtil.DEFAULT_ITEM_HEIGHT;
		} else {
			const style = graph.getCellStyle(cell);

			//update size width if over default range
			if (size.width < KGraphUtil.DEFAULT_ITEM_WIDTH) {
				size.width = KGraphUtil.DEFAULT_ITEM_WIDTH;
			} else if (size.width > KGraphUtil.MAX_ITEM_WIDTH) {
				size.width = KGraphUtil.MAX_ITEM_WIDTH;
			}

			//[ThuyTv]: change cell align to left if width is larger than default width
			//otherwise, cell align must be center
			if (size.width > KGraphUtil.DEFAULT_ITEM_WIDTH) {
				if (style[mxConstants.STYLE_LABEL_PADDING]){
					size.width = size.width + style[mxConstants.STYLE_LABEL_PADDING] / 2;
				}
			}

			//update size height if over default range
			if (size.height < KGraphUtil.DEFAULT_ITEM_HEIGHT) {
				size.height = KGraphUtil.DEFAULT_ITEM_HEIGHT;
			} else if (size.height > KGraphUtil.MAX_ITEM_HEIGHT) {
				size.height = KGraphUtil.MAX_ITEM_HEIGHT;
			}

			//[ThuyTv]: change cell vertical align to top if height is larger than default height
			//otherwise, cell vertical align must be middle
			if (size.height > KGraphUtil.DEFAULT_ITEM_HEIGHT) {
				if (style[mxConstants.STYLE_LABEL_PADDING]){
					size.height = size.height + style[mxConstants.STYLE_LABEL_PADDING] / 2;
				}
			}
		}

		if (typeof value === 'string' && value.length < 1 ) {
			size.width = KGraphUtil.DEFAULT_ITEM_WIDTH;
			size.height = KGraphUtil.DEFAULT_ITEM_HEIGHT;
		}
	} else if (cell.itemType === KItemType.BASE_ITEM) {

		if (size.height < KGraphUtil.MIN_BASE_ITEM_HEIGHT) {
			size.height = KGraphUtil.MIN_BASE_ITEM_HEIGHT;
		} else if (size.height > KGraphUtil.MAX_BASE_ITEM_HEIGHT) {
			size.height = KGraphUtil.MAX_BASE_ITEM_HEIGHT;
		}

		if (size.width < KGraphUtil.MIN_BASE_ITEM_WIDTH) {
			size.width = KGraphUtil.MIN_BASE_ITEM_WIDTH;
		} else if (size.width > KGraphUtil.MAX_BASE_ITEM_WIDTH) {
			size.width = KGraphUtil.MAX_BASE_ITEM_WIDTH;
		}
	} else if (cell.itemType === KItemType.BASE_ITEM_LABEL) {
		
		if (size.height < KGraphUtil.MIN_BASE_ITEM_LABEL_HEIGHT) {
			size.height = KGraphUtil.MIN_BASE_ITEM_LABEL_HEIGHT;
		} else if (size.height > KGraphUtil.MAX_BASE_ITEM_LABEL_HEIGHT) {
			size.height = KGraphUtil.MAX_BASE_ITEM_LABEL_HEIGHT;
		}

		if (size.width < KGraphUtil.MIN_BASE_ITEM_LABEL_WIDTH) {
			size.width = KGraphUtil.MIN_BASE_ITEM_LABEL_WIDTH;
		} else if (size.width > KGraphUtil.MAX_BASE_ITEM_LABEL_WIDTH) {
			size.width = KGraphUtil.MAX_BASE_ITEM_LABEL_WIDTH;
		}
	}

	cell.setValue(baseValue);
	return size;
}