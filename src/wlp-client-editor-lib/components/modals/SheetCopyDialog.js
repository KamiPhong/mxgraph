import React from "react";
import { withTranslation } from 'react-i18next';

import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import DropdownList from 'wlp-client-common/component/Dropdown/DropdownList';
import Link from 'wlp-client-common/component/Link';
import SearchService from 'wlp-client-service/service/SearchService';
import SearchConfigDto from 'wlp-client-service/dto/search/SearchConfigDto';
import SheetEventCenter from 'wlp-client-editor-lib/events/SheetEventCenter';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import { DEFAULT_ALL_RESULT } from 'wlp-client-mypage-lib/consts/UserSetting';
import {
    menuPulldownIcon
} from 'wlp-client-editor-lib/consts/Image';
import { showWarningDialog, showErrorDialog } from "wlp-client-common/utils/DialogUtils";
import SheetConsts from 'wlp-client-editor-lib/consts/SheetConsts';
import i18n from "i18n";
import ActionLogger from 'wlp-client-common/ActionLogger';

const searchService = new SearchService();
const sheetEventCenter = new SheetEventCenter();

class SheetCopyDialog extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            items: [],
            selectedItem: null,
            keywords: "",
            dropDownListOfSheetCopy: false
        }
    }

    componentDidMount() {
        const { mapInfo } = this.props;
        if (typeof mapInfo !== 'undefined') {
            this.findCopyDestinationMaps();
        }
    }

    onChangeHandler = (event) => {
        ActionLogger.typing('User_Editor_Header', 'Search_Input_Dialog_Copy_sheet');
        this.setState({ keywords: event.target.value });
    }

    onSubmitHandler = (event) => {
        event.preventDefault();
        this.findCopyDestinationMaps();
    }

    handleCopySheet = () => {
        ActionLogger.click('User_Editor_Header', 'Button_OK_Dialog_Copy_sheet');
        const { currentSheet, cancelHandler, mapInfo, sheetList } = this.props;
        const { selectedItem } = this.state;
        let sheetModel = { ...currentSheet };
        sheetModel.contractId = mapInfo.contractId;
        sheetModel.parentId = selectedItem.mapId;

        if (sheetList.length >= SheetConsts.LIMIT_SHEET && (currentSheet.parentId === sheetModel.parentId)) {
            showErrorDialog(i18n.t('editor.alert.create.max.sheet'));
            cancelHandler(); // Close Dialog
            return;
        }
        delete sheetModel.isSuccess;
        cancelHandler(); // Close Dialog
        sheetEventCenter.fireEvent(sheetEventCenter.SHEET_ADD_BY_COPY, sheetModel);
    }

    findCopyDestinationMaps = async () => {
        const { mapInfo } = this.props;
        const { items, keywords } = this.state;
        let dropDownItems = items;
        let dropDownBorder = false;
        let requestDto = new SearchConfigDto();

        requestDto.contractId = mapInfo.contractId;
        requestDto.text = keywords;
        requestDto = {
            ...requestDto,
            "contractId": requestDto.contractId,
            "count": parseInt(DEFAULT_ALL_RESULT),
            "mapId": null,
            "start": 0,
            "text": requestDto.text
        };
        searchService.findCopyDestinationMaps(requestDto).then(result => {
            dropDownItems = []; //clear array
            if (!requestDto.text) {
                dropDownItems.push({
                    teamName: mapInfo.name,
                    mapId: mapInfo.id
                });
                dropDownBorder = true;
            }
            result.entries.map((item) => {
                if (item.mapId !== mapInfo.id) {
                    dropDownItems.push({
                        teamName: item.mapName,
                        mapId: item.mapId,
                    });
                }
            })
            this.setState({
                items: dropDownItems,
                selectedItem: dropDownItems[0],
                dropDownListOfSheetCopy: dropDownBorder
            })
        }).catch(e => {
            showWarningDialog('message.global.e0001', {
                onOKCallback: () => window.location.reload()
            })
        });
    }

    onChangeValue = (contract) => {
        ActionLogger.click('User_Editor_Header', 'Selected_Map_Dialog_Copy_sheet');
        this.setState(() => ({
            selectedItem: contract,
        }));
    }

    render() {
        const { t, isShow, cancelHandler, commonUserInfo } = this.props;

        const { items, selectedItem, keywords, dropDownListOfSheetCopy } = this.state

        return (
            <CommonModal
                backDropModal="modal-action"
                className="modal-437-ed modal-437-ed-editor"
                onHide={cancelHandler}
                show={isShow}
                backGroundClass="bg-modal bg-modal-editor"
                headerClass=" "
                noBorderClass={true}
            >
                { commonUserInfo.contractUserRole !== RoleType.PARTNER &&
                    <div className="w-319px w-319px-editor mx-auto">
                        <div className="header-search header-search-editor modal-search modal-search-editor w-100">
                            <form onSubmit={this.onSubmitHandler}>
                                <input type="text" className="form-control-header form-control-header-editor" value={keywords} onChange={this.onChangeHandler} placeholder={t("editor.sheetCopy.keywordPrompt")} />
                            </form>
                        </div>
                        <div className="dropdown dropdown-editor dropdown-gr dropdown-gr-editor dropdown-modal dropdown-modal-editor">
                            <DropdownList
                                itemList={items}
                                selectedItem={selectedItem}
                                editorClass="w-100 dropdown-pl-35 dropdown-pl-35-editor position-absolute overflow-auto rounded"
                                dropDownListOfSheetCopy={dropDownListOfSheetCopy}
                                onChangeValue={this.onChangeValue}
                                pulldownIcon={menuPulldownIcon}
                            />
                        </div>
                    </div>
                }
                <div className="text-center mb-19px mb-19px-editor">
                    { commonUserInfo.contractUserRole === RoleType.PARTNER &&
                        <div className="fs-12 fs-12-editor line-height-1rem line-height-1rem-editor">{t("editor.sheetCopy.message1")}</div>
                    }
                    <div className="fs-12 fs-12-editor line-height-1rem line-height-1rem-editor">{t("editor.sheetCopy.message2")}</div>
                </div>
                <div className="text-center modal-gr-btn modal-gr-btn-editor">
                    <Link className="btn btn-editor btn-whitefl btn-whitefl-editor" onClick={()=> {
                        ActionLogger.click('User_Editor_Header', 'Button_Cancel_Dialog_Copy_sheet');
                        cancelHandler();
                    }}>{t("global.cancel")}</Link>
                    <Link className="btn btn-editor btn-black btn-black-editor" data-toggle="modal" data-target="#modal-newmap" onClick={this.handleCopySheet}>{t("global.ok")}</Link>
                </div>
            </CommonModal>
        );
    }
}

export default withTranslation()(SheetCopyDialog);