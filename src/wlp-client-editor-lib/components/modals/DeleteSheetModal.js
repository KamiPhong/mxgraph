import * as React from "react";
import CommonModal from "../../../wlp-client-common/component/modals/CommonModal";
import alertIcon001 from 'wlp-client-common/images/alertIcon001.png';
import {withTranslation} from "react-i18next";
import SheetPullDownListEventCenter from "wlp-client-editor-lib/events/SheetPullDownListEventCenter";

const sheetPulldownButtonEventCenter = new SheetPullDownListEventCenter();

class DeleteSheetModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowConfirmDeleteModal: false,
        };
        this.deleteSheetModel = this.props.currentSheet
    }
    
    deleteModal_handleOkBtn = () => {
        // this.removeShadow();
        this.setState({isShowConfirmDeleteModal: true});
    };
    
    submitOkButton = () => {
        sheetPulldownButtonEventCenter.fireEvent(sheetPulldownButtonEventCenter.SHEET_PULLDOWN_DELETE_SHEET, this.deleteSheetModel);
        setTimeout(() => {
            this.removeShadow();
            this.props.cancelHandler();
        }, 0);
    }
    
    renderConfirmModal = () => {
        const {t} = this.props;
        return (<CommonModal
            show={true}
            backGroundClass="bg-modal bg-modal-editor"
            backDropModal="modal-action"
            headerClass=" "
            noBorderClass=" "
            classNameImage=" "
            onHide={this.cancelHandler}>
            <div className="ltext-center d-flex-center-fl d-flex-center-fl-editor mb-21px mb-21px-editor modal-alert">
                <img src={alertIcon001} alt="" className="d-inline-block img-alert img-alert-editor noselect"/> <span
                className="fs-12 fs-12-editor line-height-1rem line-height-1rem-editor txt-alert txt-alert-editor noselect">{t('editor.c0002')}</span>
            </div>
            <div className="text-center modal-gr-btn modal-gr-btn-editor">
                <button type="submit" onClick={this.cancelHandler}
                        className="btn btn-editor btn-whitefl btn-whitefl-editor">{t('global.cancel')}</button>
                <button type="submit" onClick={this.submitOkButton} className="btn btn-editor btn-black btn-black-editor">{t('global.ok')}</button>
            </div>
        </CommonModal>);
    };

    renderWaringModal = () => {
        const {t} = this.props;
        return <CommonModal
            show={true}
            backGroundClass="bg-modal bg-modal-editor"
            backDropModal="modal-action"
            onHide={this.cancelHandler}>
            <div className="text-center d-flex-center-fl d-flex-center-fl-editor mb-21px mb-21px-editor modal-alert">
                <img src={alertIcon001} alt="" className="d-inline-block img-alert img-alert-editor noselect"/> 
                <span className="fs-12 fs-12-editor line-height-1rem line-height-1rem-editor txt-alert txt-alert-editor white-space-preline white-space-preline-editor text-left noselect">{t('editor.c0001')}</span>
            </div>
            <div className="text-center modal-gr-btn modal-gr-btn-editor">
                <button type="submit" onClick={this.cancelHandler}
                        className="btn btn-editor btn-whitefl btn-whitefl-editor">{t('global.cancel')}</button>
                <button type="submit" onClick={this.deleteModal_handleOkBtn}
                        className="btn btn-editor btn-black btn-black-editor">{t('global.ok')}</button>
            </div>
        </CommonModal>;
    };

    cancelHandler=()=>{
        this.removeShadow();
        const { cancelHandler} = this.props;
        cancelHandler();
    }

    removeShadow = () =>{
        const rootNode = document.getElementById('root');
        rootNode.classList.remove('blur_shadow');

    }

    renderModal = () => {
        const {isShowConfirmDeleteModal} = this.state;
        const rootNode = document.getElementById('root');
        rootNode.classList.add('blur_shadow');

        return isShowConfirmDeleteModal ?
          this.renderConfirmModal() :
          this.renderWaringModal();  
    };
    render() {
        const {isShow} = this.props;
        return isShow && this.renderModal();
    }
}

export default withTranslation()(DeleteSheetModal);