import React from "react";
import CommonModal from "wlp-client-common/component/modals/CommonModal";
import { withTranslation } from "react-i18next";
import Link from "wlp-client-common/component/Link";
import ActionLogger from 'wlp-client-common/ActionLogger';

const MAX_LINE = 100;
const MAX_CHART = 8000;

class CreateItemFromDocDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            docValue: '',
            isEditing: null,
            focusTextareaItem: false,
        }
    }

    fieldOnFocus = (evt) => {
        this.setState({ 
            isEditing: true,
            focusTextareaItem: true
        });
    }

    fieldOnBlur = () => {
        this.setState({ 
            isEditing: null,
            focusTextareaItem: false
        });
    }

    createItem_handleClick = () => {
        ActionLogger.click('User_Editor_Header', 'Button_OK_Dialog_Create_Item_From_Doc');
        if(this.state.docValue !== ''){
            if(typeof(this.props.onCreateItems) === 'function'){
                this.props.onCreateItems(this.state.docValue);
                this.setState({docValue: ''});
            }
        }

        if(typeof(this.props.onCancel) === 'function'){
            this.props.onCancel();
        }
    }


    /**
     * Set limit for textarea {100 lines}
     * @param {InputEvent} evt
     */
    onDocChange = (evt) => {
        ActionLogger.typing('User_Editor_Header', 'Item_Input_Dialog_Create_Item_From_Doc');

        let limit = MAX_LINE; // max line
        let value = evt.target.value;
        let lines = value.split("\n");
        let valueWithoutNewline = value.replace(/\n/g, ''); 
        let maxChart = MAX_CHART; // max Character
        
        if (valueWithoutNewline.length > maxChart) return;

        let resultValue = lines.slice(0, limit).join("\n");

        this.setState({docValue: resultValue});
    }

    render() {
        const { t, onCancel, isShow } = this.props;
        const { isEditing, docValue,focusTextareaItem } = this.state;
        let textClasses = ['form-control form-control-shadow form-control-editor form-control-shadow-editor placeholder-italic placeholder-italic-editor modal-textarea modal-textarea-editor form-control-textarea'];

        if (isEditing === null && docValue) {
            textClasses.push('form-control-valid');
        }
        return (<CommonModal 
                onHide={onCancel} show={isShow}
                backGroundClass="bg-modal bg-modal-editor"
                backDropModal="modal-action"
                noBorderClass={true}
                classNameImage=''
            >
            <div className="w-393px w-393px-editor mx-auto mb-21px mb-21px-editor">
                <textarea
                    onFocus={this.fieldOnFocus}
                    onBlur={this.fieldOnBlur}
                    value={this.state.docValue}
                    onChange={this.onDocChange}
                    className={textClasses.join(' ')}
                    cols="30" 
                    rows="7" 
                    placeholder={!focusTextareaItem ? t('editor.itemCreateAssist.prompt') : ''}>
                    {t('editor.itemCreateAssist.prompt')}
                </textarea>
            </div>
            <div className="text-center modal-gr-btn modal-gr-btn-editor">
                <Link className="btn btn-editor btn-whitefl btn-whitefl-editor" onClick={() => {
                    ActionLogger.click('User_Editor_Header', 'Button_Cancel_Dialog_Create_Item_From_Doc');
                    onCancel();
                }}>{t('global.cancel')}</Link>
                <Link className="btn btn-editor btn-black btn-black-editor minw-100px" onClick={this.createItem_handleClick} >{t('editor.header.edit.createItem')}</Link>
            </div>
        </CommonModal>);
    }
}

export default withTranslation()(CreateItemFromDocDialog);