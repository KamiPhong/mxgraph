import React from 'react';
import SheetTabList from './SheetTabList';
import ListUserJoinedMap from './ListUserJoinedMap';
import { withMainContext } from 'wlp-client-editor/MainContext';
import SheetModel from 'wlp-client-service/service/model/SheetModel';

import SheetService from 'wlp-client-service/service/SheetService';
import SheetEventCenter from 'wlp-client-editor-lib/events/SheetEventCenter';
import MapContainerContext from './MapContainerContext';
import MapHeader from './MapHeader';
import ContractType from 'wlp-client-service/consts/ContractType';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import SheetView from './SheetView';
import SheetCopyDialog from 'wlp-client-editor-lib/components/modals/SheetCopyDialog';
import SheetPullDownListEventCenter from 'wlp-client-editor-lib/events/SheetPullDownListEventCenter';
import WlpFault from 'wlp-client-service/service/model/WlpFault';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import ErrorTypes from 'wlp-client-service/consts/ErrorTypes';
import Logger from 'wlp-client-common/Logger';
import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";
import { getCookieByName } from 'wlp-client-common/utils/CookiesUtil';

const sheetEventCenter = new SheetEventCenter();
const sheetService = new SheetService();
const sheetPullDownListEventCenter = new SheetPullDownListEventCenter();

const localStorage = new LocalStorageUtils();
class MapContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sheetList: [],
            currentSheetId: null,
            currentSheet: new SheetModel(),
            mapInfo: null,
            sheetDeleteAvailable: false,
            copyBeyondMapAvailable: false,
            sheetLockButtonVisible: false,
            isSheetCopyOpen: false,
            snapshotAvailable: false,
            isMapOwner: false,
            isSheetLocked: false,
            hideLogoutFlg: true,
            loungeMemberList: []
        };

        this.sheetTabList = React.createRef();
        this.sheetView = React.createRef();
        this.userJoinMapCount = {};
        this.lastUserSession = {};
    }

    componentDidMount() {
        this.initSheetPullDownListItemHanlers();
        this.initLastUserSession();
    }

    //sheet pull down list item handlers
    initSheetPullDownListItemHanlers = () => {
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_COPY_SHEET, this.toggleShowSheetCopy);

    }

    initLastUserSession = () => {
        let sessionId = getCookieByName("JSESSIONID");
        this.lastUserSession = sessionId;
    }

    setHideLogoutFlag = (hideLogoutFlg) => {
        this.setState({ hideLogoutFlg })
    };

    memberLeaveUnlockItems = (user) => {
        this.sheetView.memberLeaveUnlockItems(user)
    }

    //migrated from flash
    /**
     * <p>権限設定を行う。</p>
     */
    setAuthority = (commonUserInfo) => {
        const { contractType, contractUserRole } = commonUserInfo;
        if (contractType === ContractType.personal) {
            const isMapOwner = contractUserRole === RoleType.B_TO_C_OWNER;

            this.setState({
                sheetDeleteAvailable: isMapOwner,
                copyBeyondMapAvailable: isMapOwner,
                sheetLockButtonVisible: isMapOwner,
                isMapOwner: isMapOwner,
                snapshotAvailable: true,
                isSheetLocked: isMapOwner
            });
        } else {
            if (contractUserRole !== RoleType.LEADER && contractUserRole !== RoleType.ADMIN) {
                this.setState({
                    sheetDeleteAvailable: false,
                    snapshotAvailable: false,
                    copyBeyondMapAvailable: contractUserRole === RoleType.MEMBER,
                    sheetLockButtonVisible: false,
                    isMapOwner: false,
                    isSheetLocked: false
                });
            } else {
                this.setState({
                    sheetDeleteAvailable: true,
                    snapshotAvailable: false,
                    copyBeyondMapAvailable: true,
                    sheetLockButtonVisible: true,
                    isMapOwner: true,
                    isSheetLocked: true
                });
            }
        }
    }

    isSheetListValid = (sheetList) => {
        return sheetList && Array.isArray(sheetList);
    }

    setSheetList = (sheetList) => {
        this.setState({ sheetList: [...sheetList] });
    }

    /**
     * switch to other sheet
     */
    switchSheet = (sheetId) => {
        if (sheetId !== this.state.currentSheetId) {
            //set current sheet id
            this.setState({ currentSheetId: sheetId });
            this.doFetchSheet(sheetId);
        }
    }

    /**
     * fetch sheet data by id
     */
    doFetchSheet = (sheetId) => {
        sheetService.getInfo(sheetId)
            .then(this.fetchSheetResult);
    }

    /**
     * fetch sheet data hander
     */
    fetchSheetResult = (getSheetDto) => {
        if (getSheetDto.isSucceed && getSheetDto.sheet) {
            // Logger.logConsole('sheet ', getSheetDto.sheet);
            this.setCurrentSheet(getSheetDto.sheet);
            sheetEventCenter.fireEvent(sheetEventCenter.SHEET_SWITCH, getSheetDto.sheet.id);
        }
    }

    getUserSession = () => {
        let sessionId = getCookieByName("JSESSIONID");
        let storage = localStorage.getUserSession();
        if (!storage || !sessionId || (this.lastUserSession && this.lastUserSession !== sessionId)) {
            showWarningDialog(
                "session.timeout", 
                { 
                    showCancel: false,
                    onOKCallback: () => {
                        window.location.reload();
                    }
                }
            );
            return false;
        }


        return storage;
    }

    /**
     * set map when get map done
     */
    setMap = (mapInfo) => {
        if (mapInfo == null) {
            return;
        }
        //set  map info
        this.setState({ mapInfo });
        //set sheet list
        this.setSheetList(mapInfo.sheets);
        //set current active sheet
        let currentSheetId = null;

        if (mapInfo.currentSheetId) {
            currentSheetId = mapInfo.currentSheetId;
        } else {
            currentSheetId = mapInfo.sheets[0].id;
        }

        if (currentSheetId != null) {
            this.switchSheet(currentSheetId);
        }
    }

    /**
     * set sheet when fetch sheet done
     */
    setCurrentSheet = (sheetModel) => {
        this.setState({
            isSheetLocked: sheetModel.lockFlag,
            currentSheet: sheetModel
        });
    }

    setStatusForSheetCopy = (isSheetCopyOpen) => {
        this.setState({ isSheetCopyOpen })
    }

    toggleShowSheetCopy = () => {
        const { isSheetCopyOpen } = this.state;
        this.setStatusForSheetCopy(!isSheetCopyOpen);
    }

    lockSheet = (sheet) => {
        if (!this.getUserSession()) return;
        sheetEventCenter.fireEvent(sheetEventCenter.SHEET_LOCK, sheet);
    }

    unLockSheet = (sheet) => {
        if (!this.getUserSession()) return;
        sheetEventCenter.fireEvent(sheetEventCenter.SHEET_UNLOCK, sheet);
    }


    render() {
        const { callSheetMoved, pulldownIcon, toggleLockSheet, guideDisplay } = this.props;
        const { currentSheetId, sheetList, currentSheet, loungeMemberList } = this.state;
        const {
            sheetDeleteAvailable,
            sheetLockButtonVisible,
            isSheetCopyOpen,
            mapInfo,
            isSheetLocked,
        } = this.state;

        const { commonUserInfo } = this.props.mainContext;

        return (<MapContainerContext.Provider value={{
            currentSheet: currentSheet,
            mapInfo: mapInfo,
            commonUserInfo: commonUserInfo,
            isSheetLocked: isSheetLocked,
            setCurrentSheet: this.setCurrentSheet,
            getUserSession: this.getUserSession
        }}>
            <React.Fragment>
                <MapHeader
                    hideLogoutFlg={this.state.hideLogoutFlg}
                    sheetLockButtonVisible={sheetLockButtonVisible}
                    sheetDeleteAvailable={sheetDeleteAvailable} />

                <div className="main-content main-content-editor">
                    <SheetTabList
                        unLockSheet={this.unLockSheet}
                        lockSheet={this.lockSheet}
                        ref={stl => { this.sheetTabList = stl; }}
                        switchSheet={this.switchSheet}
                        currentSheetId={currentSheetId}
                        currentSheet={currentSheet}
                        callSheetMoved={callSheetMoved}
                        getUserSession={this.getUserSession}
                        sheetLockButtonVisible={sheetLockButtonVisible}
                        sheetList={sheetList} />
                    <SheetView
                        unLockSheet={this.unLockSheet}
                        lockSheet={this.lockSheet}
                        isLoading={guideDisplay}
                        ref={sv => { this.sheetView = sv; }}
                        isSheetLocked={this.state.isSheetLocked}
                        sheetLockButtonVisible={sheetLockButtonVisible}
                        currentSheet={currentSheet}
                        pulldownIcon={pulldownIcon}
                        toggleLockSheet={toggleLockSheet} />

                    {!guideDisplay &&
                        <ListUserJoinedMap activeUsers={loungeMemberList} />
                    }

                    {isSheetCopyOpen &&
                        <SheetCopyDialog
                            cancelHandler={this.toggleShowSheetCopy}
                            mapInfo={mapInfo}
                            currentSheet={currentSheet}
                            commonUserInfo={commonUserInfo}
                            isShow={isSheetCopyOpen}
                            sheetList={sheetList} />
                    }
                </div>
            </React.Fragment>
        </MapContainerContext.Provider>)
    }

    /**
     *  //TODO: migrate from flash
     * <p>シート追加時のメッセージ受信処理。</p>
     * @param sheet     追加されたシート情報
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     * @param {WlpFault} fault
     *
     */
    sheetAdded = (sheetModel, isMine, fault) => {
        if(fault){
            this.onFault(fault);
        }else {
            const { sheetList } = this.state;
            const newSheetList = [...sheetList, sheetModel];
            this.setState({ sheetList: newSheetList })
            if (isMine) {
                this.switchSheet(sheetModel.id);
            }
        }
    }

    /**
     *  //TODO: migrate from flash
     * <p>シート追加時のメッセージ受信処理。</p>
     * @param sheet     追加されたシート情報
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     * @param {WlpFault} fault
     *
     */
    sheetCopied = (sheetModel, isMine, fault) => {
        if (fault) {
            this.onFault(fault);
        } else {
            const { sheetList, mapInfo } = this.state;
            if (sheetModel.parentId === mapInfo.id) {
                const newSheetList = [...sheetList, sheetModel];
                this.setState({ sheetList: newSheetList })
                if (isMine) {
                    this.switchSheet(sheetModel.id);
                }
            }
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>シート更新時のメッセージ受信処理。</p>
     * @param sheet     更新されたシート情報
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     */
    sheetUpdated = (sheetModel, isMine) => {
        if (sheetModel) {
            const { sheetList, currentSheet } = this.state;
            let updatedSheetList = [...sheetList];
            let updatedCurrentSheet = currentSheet
            if (sheetModel.id === this.state.currentSheetId) {
                updatedCurrentSheet = { ...currentSheet, ...sheetModel };
                this.setState({
                    currentSheet: updatedCurrentSheet,
                })
            }

            if (this.isSheetListValid(updatedSheetList)) {
                for (let i = 0; i < updatedSheetList.length; i++) {
                    if (updatedSheetList[i] && sheetModel.id === updatedSheetList[i].id) {
                        updatedSheetList[i] = sheetModel;
                        break;
                    }
                }

                //set sheetList and udpate currentSheet
                this.setState({
                    sheetList: updatedSheetList
                });
            }
        }
    }

    beforeHandleRemoveSheet = () => {
        this.sheetView.beforeHandleRemoveSheet();
    }

    /**
     * //TODO: migrate from flash
     * <p>シート削除時のメッセージ受信処理。</p>
     * @param sheet     削除されたシート
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     */
    sheetRemoved(sheetModels) {
        const { sheetList, currentSheetId } = this.state;
        if (this.isSheetListValid(sheetList) === false || this.isSheetListValid(sheetModels) === false) {
            return;
        }

        let deletedItem = sheetModels[0];
        let addedItem = sheetModels[1];

        let copiedSheetList = [...sheetList];
        const isSwitchSheet = currentSheetId === deletedItem.id;

        //NOTE: trên flash khi chỉ còn 1 sheet tồn tại mà thực hiện xóa sheet
        //Flash sẽ xóa và thêm sheet luôn sau đó submit amfstream một mảng có 2 item
        //Item đầu tiên sẽ là sheet vừa xóa, item thứ 2 là sheet vừa thêm mới.
        //Sau khi chuyển đồi hoàn toàn sang socket thí mảng nhận được sẽ chỉ có 1 item
        //Item thêm mới sẽ được submit bằng action add sheet
        for (let i = 0; i < sheetList.length; i++) {
            let sheet = sheetList[i];
            if (sheet !== null && deletedItem.id === sheet.id) {
                if (isSwitchSheet) {
                    let nextSheet = copiedSheetList[i + 1];
                    if (i === (sheetList.length - 1)) {
                        nextSheet = copiedSheetList[i - 1];
                    }

                    if (addedItem) {
                        nextSheet = addedItem;
                        copiedSheetList.push(addedItem);
                    }

                    if (nextSheet) {
                        this.switchSheet(nextSheet.id);
                    }
                }

                copiedSheetList.splice(i, 1);
                break;
            }
        }

        this.setSheetList(copiedSheetList);
        this.sheetView.setSheetDeletingFlag(false);
    }

    /**
     * //TODO: migrate from flash
     * <p>シート順序を移動する。</p>
     * @param sheet     移動するシート情報
     * @param locked    
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     */
    sheetLockStateChanged = (sheet) => {
        let updateSheetList = this.state.sheetList;
        for (let i = 0; i < updateSheetList.length; i++) {
            let sheetInList = updateSheetList[i];
            if (sheet.id === sheetInList.id) {
                updateSheetList[i] = sheet;
                break;
            }
        }
        this.setSheetList(updateSheetList)
        this.updateSheetLockState(sheet)
    }

    /**
     * <p>シートのロック状態を更新する。</p>
     * @param sheet 対象のシート情報
     */
    updateSheetLockState = (sheet) => {
        const { mapInfo } = this.state;
        const { readonly } = mapInfo;
        if (readonly) {
            this.setState({
                isSheetLocked: true
            })
        } else {
            let updateCurrentSheet = this.state.currentSheet;
            if (sheet.id === updateCurrentSheet.id) {
                updateCurrentSheet.lockFlag = sheet.lockFlag;
                this.setCurrentSheet(updateCurrentSheet);
            }
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>シート順序移動時のメッセージ受信処理。</p>
     * @param ids       順に並んだシートIDの配列
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     *
     */
    sheetMoved = (ids) => {
        const { sheetList } = this.state;
        let sortedSheetList = sheetList.sort((sheet1, sheet2) => {
            if( sheet1 === null || sheet2 === null){
                return false;
            }
            return ids.indexOf(sheet1.id) - ids.indexOf(sheet2.id);
        });
        this.setSheetList(sortedSheetList);
    }

    /******************************************** ITEM EVENTS *************************************************************** */

    /**
     * //TODO: migrate from flash
     * <p>アイテム追加のメッセージ受信処理。</p>
     * @param models    追加されたアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemAdded = (models, isMine, isFault) => {
        this.sheetView.itemAdded(models, isMine, isFault);
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテム更新のメッセージ受信処理。</p>
     * @param models    更新があったアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemUpdated = (models, isMine) => {
        this.sheetView.itemUpdated(models, isMine);
    }

    itemAttachFileUploaded = (model, isMine) => {
        this.sheetView.itemAttachFileUploaded(model, isMine);
    }

    /**
    * socket item attach file password changed
    */
    itemAttachFilePasswordChanged = (model, isMine, isFault) => {
        Logger.logConsole({model, isMine, isFault})
        this.sheetView.itemAttachFilePasswordChanged(model, isMine, isFault);
    }
    /**
     * //TODO: migrate from flash
     * <p>アイテム削除のメッセージ受信処理。</p>
     * @param models    削除されたアイテムIDの配列
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemRemoved = (removedItemIds, isMine, fault) => {
        this.sheetView.itemRemoved(removedItemIds, isMine, fault);
    }


    /**
     * //TODO: migrate from flash
     * <p>アイテムロック状態変更のメッセージ受信処理。</p>
     * @param models    ロック状態が変更されたアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemLockStateChanged = (models, isMine, fault) => {
        this.sheetView.itemLockStateChanged(models, isMine, fault);
    }


    /**
     * //TODO: migrate from flash
     * <p>アイテムのgood状態変更のメッセージ受信処理。</p>
     * @param itemLikeModel    good状態の更新情報
     * @param isMine           自分の操作によって送信されたメッセージであることを示す。
     */
    itemLikeStateChanged = (itemLikeModel, isMine, fault) => {
        this.sheetView.itemLikeStateChanged(itemLikeModel, isMine, fault);
        Logger.logConsole("itemLikeStateChanged ", itemLikeModel);
    }

    /******************************************** ITEM BASE EVENTS *************************************************************** */
    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム追加のメッセージ受信処理。</p>
     * @param models    追加されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemAdded = (models, isMine, fault) => {
        
        this.sheetView.baseItemAdded(models, isMine, fault);
    }

    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム更新のメッセージ受信処理。</p>
     * @param models    更新されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemUpdated = (models, isMine, fault) => {
        this.sheetView.baseItemUpdated(models, isMine, fault);
    }

    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム削除のメッセージ受信処理。</p>
     * @param removedItemIds    削除されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemRemoved = (removedItemIds, isMine, fault) => {
        this.sheetView.baseItemRemoved(removedItemIds, isMine, fault);
    }


    /**
     * //TODO: migrate from flash
     * <p>ベースアイテムロック状態変更のメッセージ受信処理。</p>
     * @param models    ロック状態が変更されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemLockStateChanged = (models, isMine, fault) => {
        this.sheetView.baseItemLockStateChanged(models, isMine, fault);
    }

    /******************************************** LINE EVENTS *************************************************************** */
    /**
     * //TODO: migrate from flash
     * <p>関係線作成のメッセージ受信処理。</p>
     * @param model     作成された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineAdded = (model, isMine, fault) => {
        this.sheetView.lineAdded(model, isMine, fault);
    }

    /**
     * //TODO: migrate from flash
     * <p>関係線更新のメッセージ受信処理。</p>
     * @param model     更新された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineUpdated = (model, isMine, fault) => {
        this.sheetView.lineUpdated(model, isMine, fault);
    }


    /**
     * //TODO: migrate from flash
     * <p>関係線削除のメッセージ受信処理。</p>
     * @param model     削除された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineRemoved = (model, isMine, fault) => {
        this.sheetView.lineRemoved(model, isMine, fault);
    }


    /**
     * //TODO: migrate from flash
     * <p>関係線ロック状態変更のメッセージ受信処理。</p>
     * @param model     ロック状態が変更された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineLockStateChanged = (model, isMine, fault) => {
        this.sheetView.lineLockStateChanged(model, isMine, fault);
    }

    /******************************************** LOUNGE EVENTS *************************************************************** */

    loungeMemberList = (users) => {
        this.userJoinMapCount = {};
        this.setUserJoinedMap(users);
    }

    loungeMemberJoin = (users, isMine, isFault) => {
        if (Array.isArray(users)) {
            this.setUserJoinedMap(users.concat(this.state.loungeMemberList));
        }
    }

    loungeMemberLeave = (removedUser) => {
        this.userJoinMapCount[removedUser.id] -= 1;

        if (this.userJoinMapCount[removedUser.id] === 0) {
            let uniqueUsers = [];
            for (let i = 0; i < this.state.loungeMemberList.length; i ++) {
                const jonedMember = this.state.loungeMemberList[i];
                if (jonedMember.id !== removedUser.id) {
                    uniqueUsers.push(jonedMember);
                }
            }
    
            this.setState({ loungeMemberList: uniqueUsers });
        }
        
    }

    setUserJoinedMap = (joinedUsers) => {
        if (Array.isArray(joinedUsers)) {
            let uniqueUsers = [];
            let currentUserId = null;
            let isHasNewUserJoined = false;
            const userSession = this.getUserSession();

            if (userSession && userSession.sessionDto) {
                currentUserId = userSession.sessionDto.userId;
            }

            for (let i = 0; i < joinedUsers.length; i ++) {
                const addUser = joinedUsers[i];
                if (!this.userJoinMapCount[addUser.id]) {
                    this.userJoinMapCount[addUser.id] = 1;
                    let action = currentUserId === addUser.id ? 'unshift' : 'push';
                    uniqueUsers[action](addUser);
                    isHasNewUserJoined = true;
                } else {
                    this.userJoinMapCount[addUser.id] += 1;
                }
            }

            if (isHasNewUserJoined) {
                this.setState({ loungeMemberList: uniqueUsers });
            }
        }
    }

    /******************************************** ERROR EVENTS *************************************************************** */

    /**
     * handle error from socket
     * @param {WlpFault} fault
     */
    onFault = (sendedData, isMine, fault) => {
        if (fault instanceof WlpFault) {
            const warningConfig = {};
            if (fault.sendFrom === ErrorTypes.SERVER_ERROR) {
                warningConfig.onOKCallback = () => window.close();
            } else if (fault.sendFrom === ErrorTypes.CLIENT_ERROR || fault.messageKey === 'session.timeout') {
                warningConfig.onOKCallback = () => window.location.reload();
            }

            showWarningDialog(fault.messageKey, warningConfig);
        }
    }
}

export default withMainContext(MapContainer);