import React from "react";
import 'wlp-client-editor-lib/assets/scss/mxgraph.scss';
import ExpandSlider from './ExpandSlider';
import SheetNavigator from './SheetNavigator';
import ItemMenuButtonBar from './menu/ItemContextMenu/ItemMenuButtonBar';
import ItemLineMenuButtonBar from './menu/ItemLineContextMenu/ItemLineMenuButtonBar';
import KGraphContainer from '../core/KGraphContainer';
import KItemStyle from '../core/KItemStyle';
import KGraphItemShape from '../core/KGraphItemShape';

import SheetShareButton from './SheetShareButton';
import SheetLockButton from './SheetLockButton';
import SheetUnLockButton from './SheetUnLockButton';
import BackgroundSheetConsts, { SheetBG } from "wlp-client-common/consts/BackgroundSheetConsts";

import ItemModel from "wlp-client-service/service/model/ItemModel";
import BaseItemModel from "wlp-client-service/service/model/BaseItemModel";
import ItemLikeModel from "wlp-client-service/service/model/ItemLikeModel";
import KGraphEvent from '../core/KGraphEvent';
import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";
import { ZoomConfig } from "wlp-client-editor-lib/core/KGraphConfig";
import MainSkinContext from 'wlp-client-editor/MainSkinContext';
import { v4 as uuidv4 } from 'uuid';
import { withMapContainerContext } from "./MapContainerContext";
import KItemKind from "wlp-client-editor-lib/core/KItemKind";
import SheetEventCenter from 'wlp-client-editor-lib/events/SheetEventCenter';
import EditPullDownListEventCenter from 'wlp-client-editor-lib/events/EditPullDownListEventCenter';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
import AccountPullDownListEventCenter from 'wlp-client-editor-lib/events/AccountPullDownListEventCenter';
import i18n from "i18n";
import SheetProperty from "./SheetProperty";
import SheetPullDownListEventCenter from "../events/SheetPullDownListEventCenter";
import SheetViewEventCenter from "../events/SheetViewEventCenter";
import KItemType from '../core/KItemType';
import { CONSTANTS } from 'wlp-client-editor-lib/utils/Constants';
import { StageConfig } from "wlp-client-editor-lib/core/KGraphConfig";
import RelationLineModel from "wlp-client-service/service/model/RelationLineModel";
import RelationLineKind from 'wlp-client-service/consts/RelationLineKind';
import ItemLineMenuButtonBarEventCenter from "wlp-client-editor-lib/events/ItemLineMenuButtonBarEventCenter";

import FileService from 'wlp-client-service/service/FileService';
import KConstants from "wlp-client-editor-lib/core/KConstants";
import KItemLineColor from "wlp-client-editor-lib/core/KItemLineColor";
import KItemLineArrowType from "wlp-client-editor-lib/core/KItemLineArrowType";
import KItemTextAlign from "wlp-client-editor-lib/core/KItemTextAlign";
import { DEFAULT_FONT_SIZE, DEFAULT_LINE_WEIGHT } from "wlp-client-editor-lib/core/KGraphUtil";
import { showWarningDialog } from "wlp-client-common/utils/DialogUtils";
import PullDownButtonEvent from 'wlp-client-editor-lib/events/PullDownButtonEvent';
import SheetService from 'wlp-client-service/service/SheetService';
import CommonModal from "wlp-client-common/component/modals/CommonModal";
import DownloadAttachFileConfigDto from 'wlp-client-service/dto/sheet/DownloadAttachFileConfigDto';
import UploadExportFileDto from 'wlp-client-service/dto/sheet/UploadExportFileDto';
import { DOWNLOAD_PATH_PREFIX, UPLOAD_EXPORT_FILE_PATH_PREFIX } from 'wlp-client-common/config/EditorConfig';
import DisplayPulldownListEventCenter from "wlp-client-editor-lib/events/DisplayPulldownListEventCenter";
import KItemTextVerticalAlign from "wlp-client-editor-lib/core/KItemTextVerticalAlign";
import CreateItemFromDocDialog from "./modals/CreateItemFromDocDialog";
import { imgClose, BASE64_IMAGE_1PX } from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import { mxEvent, mxClient } from "wlp-client-editor-lib/core/KClient";
import Logger from "wlp-client-common/Logger";
import { isProductEnv, unEscapeHtml } from 'wlp-client-editor-lib/utils/Utils';
import ActionLogger from "wlp-client-common/ActionLogger";

const editPullDownListEventCenter = new EditPullDownListEventCenter();
const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();
const sheetPullDownListEventCenter = new SheetPullDownListEventCenter();
const itemLineMenuButtonBarEventCenter = new ItemLineMenuButtonBarEventCenter();
const accountPullDownListEventCenter = new AccountPullDownListEventCenter();
const localStorage = new LocalStorageUtils();

const sheetEventCenter = new SheetEventCenter();
const sheetViewEventCenter = new SheetViewEventCenter();
const fileService = new FileService();
const pullDownButtonEvent = new PullDownButtonEvent();
const sheetService = new SheetService();
const displayPulldownEventCenter = new DisplayPulldownListEventCenter();

class SheetView extends React.Component {

    static contextType = MainSkinContext;
    // pasting = false;
    /**
     * シート上にあるすべての「6 日前までをハイライト」アイテムのフィルタの 表示 / 非表示を示すブール値
     */
    isForcedVisibleNewItemFiltersBy6D = false;

    /**
     * シート上にあるすべての「2 日前までをハイライト」アイテムのフィルタの 表示 / 非表示を示すブール値
     */
    isForcedVisibleNewItemFiltersBy2D = false;
    //[huyvq]--- create ref for print guide container
    printGuideRef = React.createRef()

    constructor(props) {
        super(props);
        this.state = {
            currentZoomValue: ZoomConfig.INIT_ZOOM_VALUE,
            maxZoomValue: ZoomConfig.MAX_ZOOM_VALUE,
            isSheetPropertyOpen: false,
            showDownloadFileInputPasswordModal: false,
            isCreateItemFromDocumentDialogOpen: false,
            downloadFileModel: null,
            attachFileImageModal: {
                enable: false,
                imageUrl: ""
            },
            forceFilterEnabled: true,
        };

        this.lastEditedItemShape = KGraphItemShape.NONE;
        this.lastEditedItemStyle = KItemStyle.NONE;
        this.lastItemFontSize = DEFAULT_FONT_SIZE;
        this.lastItemTextHorizontalAlign = KItemTextAlign.LEFT;

        this.lastBaseItemShapeType = KGraphItemShape.BASE_RECT;
        this.lastBaseItemStyleType = KItemStyle.B_A;
        this.lastBaseItemFontSize = DEFAULT_FONT_SIZE;
        this.lastBaseItemFontAlign = KItemTextAlign.LEFT;
        this.lastBaseItemFontVerticalAlign = KItemTextVerticalAlign.MIDDLE;

        this.lastItemLineColor = KItemLineColor.A;
        this.lastItemLineArrowType = KItemLineArrowType.LINEAR_NONE;
        this.lastItemLineWeight = DEFAULT_LINE_WEIGHT;
        this.lastItemLineTextSize = DEFAULT_FONT_SIZE;
        this.lastItemLineTextHorizontalAlign = KItemTextAlign.LEFT;

        this.clipboardModels = [];
        this.highlightItems = [];
        this.downloadFilePassword = "";
        this.showAttachFileImage = false;
        
        this.lastItemEvt = null;
        this.lastBaseItemEvt = null;
        this.pasting = false;
    }

    /**
     * @author ThuyTV
     * @description set current zoom value in state;
     * @param {number} currentZoomValue;
     * @return void
     */
    setCurrentZoomValue = (currentZoomValue) => {
        this.setState({ 
            currentZoomValue: currentZoomValue,
        });
    }

    setMaxZoomValue = (maxZoomValue) => {
        this.setState({ maxZoomValue });
    }

    resetZoomFactor = () => {
        this.setState({
            currentZoomValue: ZoomConfig.INIT_ZOOM_VALUE,
            maxZoomValue: ZoomConfig.MAX_ZOOM_VALUE
        })
    }

    /**
     * do render graph
     */
    renderGraph = () => {

        const { graphContainer, sheetNavigatorContainer } = this.refs;
        const { currentSheet } = this.props;

        if (typeof this.renderer !== 'undefined') {
            this.renderer.destroy();
            this.renderer = null;
        }
        this.renderer = new KGraphContainer(graphContainer, currentSheet);

        this.renderer.showNavigator(sheetNavigatorContainer.refs.navigator);

        //add graph item events
        this.renderer.addUsereventReciver(KGraphEvent.ITEM_DO_CREATE, this.doCreateNewItem);
        this.renderer.addUsereventReciver(KGraphEvent.ITEM_DO_COPY, this.doCopySelectedItems);
        this.renderer.addUsereventReciver(KGraphEvent.ITEM_DO_PASTE, this.doPasteItem);
        this.renderer.addUsereventReciver(KGraphEvent.ITEM_DO_DELETE, this.deleteSelectedItems);
        
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_ADDED_BY_COPY, this.handleItemAddedByCopy);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_LOCKED, this.handleItemLocked);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_UNLOCKED, this.handleItemUnlocked);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_UPDATED, this.handleItemUpdated);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_REMOVED, this.handleItemRemoved);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_UPLOAD_FILE_COMPLETED, this.handleItemUploadFileCompleted);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_DOWNLOAD_FILE, this.handleItemDownloadFile);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_SETTING_ATTACH_FILE_PASSWORD, this.handleItemSettingAttachFilePassword);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_CLEAR_ATTACH_FILE_PASSWORD, this.handleItemClearAttachFilePassword);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_ATTACH_FILE_CLICK, this.handleItemAttachFileClick);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_LIKE, this.handleLikeItem);

        this.renderer.addGraphEventReceiver(KGraphEvent.BASE_ITEM_LOCKED, this.handleBaseItemLocked);
        this.renderer.addGraphEventReceiver(KGraphEvent.BASE_ITEM_UNLOCKED, this.handleBaseItemUnlocked);
        this.renderer.addGraphEventReceiver(KGraphEvent.BASE_ITEM_UPDATED, this.handleBaseItemUpdated);
        this.renderer.addGraphEventReceiver(KGraphEvent.BASE_ITEM_REMOVED, this.handleItemRemoved);

        //add graph base item event
        this.renderer.addUsereventReciver(KGraphEvent.BASE_ITEM_DO_ADD_NEW, this.doCreateNewBaseItem);
        this.renderer.addUsereventReciver(KGraphEvent.BASE_ITEM_DO_COPY, this.doCopySelectedItems);
        this.renderer.addUsereventReciver(KGraphEvent.GRAPH_ZOOM, this.handleZoomValueChange);

        // add grap item line events
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_LINE_DO_CREATE, this.doCreateItemLine);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_LINE_LOCKED, this.handleItemLineLocked);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_LINE_UNLOCKED, this.handleItemLineUnlock);
        this.renderer.addGraphEventReceiver(KGraphEvent.ITEM_LINE_UPDATED, this.handleItemLineUpdated);

        const isEnabled = !this.props.isSheetLocked && !this.props.isLoading;
        this.renderer.setEnabled(isEnabled);

        this.resetZoomFactor();

        const params = new URLSearchParams(window.location.search);
        this.initialItemId = params.get('itemId');
        if( this.initialItemId !== null && this.state.forceFilterEnabled){
            this.renderer.setForcedVisibleItemFilters(this.initialItemId);
        }

        // set default lock base item is true when load sheet
        this.renderer.setAllBaseItemLocked(true);
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_LOCK_BASE_ITEM_ENABLE, true);

        // set default lock base item is true when load sheet
        this.renderer.setSavePngBySelectedFlag(false);
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG_BY_SELECTED_ENABLE, false);
        
        // this.renderer.setGoodButtonFlag(false);
        displayPulldownEventCenter.fireEvent(displayPulldownEventCenter.TOOGLE_DISPLAY_PULLDOWN_SHOW_GOODS_FLAG, false);

        // this.renderer.setPrintingRangeGuideDisplayFlag(false);
        

        this.checkForcedVisibleNewItemFilters();
        this.setState({
            forceFilterEnabled: false,
        })
        //[huyvq]---pass print guide container ref to graph view
        this.renderer.graph.view.printGuideCont = this.printGuideRef.current 
        // if (this.renderer.graph.view.printGuideCont.childElementCount)
        if (this.renderer.graph.view.printGuideCont.childElementCount) {
            if (!('remove' in Element.prototype)) {
                Element.prototype.remove = function() {
                    if (this.parentNode) {
                        this.parentNode.removeChild(this);
                    }
                };
            } 
            this.renderer.graph.view.printGuideCont.childNodes[0].remove()
        }

        displayPulldownEventCenter.fireEvent(displayPulldownEventCenter.TOOGLE_PULLDOWN_SHOW_PRINTING_RANGE_GUIDE, false);
    }

    /**
     * event on did mount
     */
    componentDidMount = () => {
        const { currentSheet } = this.props;
        if (currentSheet.id && typeof this.renderer === 'undefined') {
            this.renderGraph();
        }

        editPullDownListEventCenter.addListener(editPullDownListEventCenter.EDIT_PULLDOWN_DO_CREATE_BASE_ITEM, this.doCreateNewBaseItem);
        editPullDownListEventCenter.addListener(editPullDownListEventCenter.EDIT_PULLDOWN_DELETE_ITEM, this.deleteSelectedItems);
        editPullDownListEventCenter.addListener(editPullDownListEventCenter.EDIT_PULLDOWN_CREATE_ITEM, this.doCreateNewItem);
        editPullDownListEventCenter.addListener(editPullDownListEventCenter.EDIT_PULLDOWN_COPY_ITEM, this.doCopySelectedItems);
        editPullDownListEventCenter.addListener(editPullDownListEventCenter.EDIT_PULLDOWN_PASTE_ITEM, this.doPasteItem);
        editPullDownListEventCenter.addListener(editPullDownListEventCenter.EDIT_PULLDOWN_CREATE_ITEM_BY_DOCUMENT, this.handleCreateItemFromDoc);
  
        displayPulldownEventCenter.addListener(displayPulldownEventCenter.DISPLAY_PULLDOWN_HIGHLIGHTS_NEW_ITEM_BEFORE_2D, this.setForcedVisibleItems2DFilters);
        displayPulldownEventCenter.addListener(displayPulldownEventCenter.DISPLAY_PULLDOWN_HIGHLIGHTS_NEW_ITEM_BEFORE_6D, this.setForcedVisibleItems6DFilters);
        displayPulldownEventCenter.addListener(displayPulldownEventCenter.DISPLAY_PULLDOWN_SHOW_GOODS, this.handeGoodButtonToogle);
        displayPulldownEventCenter.addListener(displayPulldownEventCenter.DISPLAY_PULLDOWN_SHOW_PRINTING_RANGE_GUIDE, this.handlePrintingRangeGuideDisplay);

        // item menu button remove item click
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_REMOVE, this.deleteSelectedItems);
        // item menu button change shape click
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_SHAPE_CHANGE, this.changeSelectedItemsShape);
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_STYLE_CHANGE, this.changeSelectedItemsStyle);
        //handle item text horizontal align change
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_TEXT_ALIGN_CHANGE, this.itemTextChangeHandler);
        //handle base item vertical align change
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_TEXT_VERTICAL_ALIGN_CHANGE, this.itemVerticalTextAlignChangeHandler);
        //handle item and base item text size change
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_TEXT_SIZE_CHANGE, this.itemTextSubMenuFontSizeChangeHandler);
        // handle item upload file result
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_UPLOAD_FILE_COMPLETED, this.itemUploadFileCompletedHandler);
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_CLICKED, this.handleItemMenuClicked);
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_DOWNLOAD_FILE, this.itemDownloadFileHandler);
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_PROPERTY_SETTING_PASSWORD, this.itemSettingPasswordHandler);
        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_PROPERTY_REMOVE_PASSWORD, this.itemRemovePasswordHandler);
        
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_SHOW_PROPERTY, this.handleShowSheetProperty);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_LOCK_BASE_ITEM, this.handleSheetPullDownLockBaseItem);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG, this.exportPNGFromSheet);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_CHANGE_SAVE_PNG_BY_SELECTED_FLAG, this.handleToggleSavePNGBySelectedFlag);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG_BY_SELECTED, this.exportPngBySelected);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_CSV, this.handleExportDataCSV);
        sheetViewEventCenter.addListener(sheetViewEventCenter.DO_CLOSE_SHEET_PROPERTY, this.handleShowSheetProperty);
        //handle out focus(look) item when change sheet'name 
        sheetViewEventCenter.addListener(sheetViewEventCenter.CLEAR_SELECT_ITEMS, this.clearSelectItems);
        
        //handle item and base item text size change
        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_SHAPE_CHANGE, this.lineTypeChangeHandler);
        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_COLOR_CHANGE, this.lineColorChangeHandler);
        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_TEXT_SIZE_CHANGE, this.lineSizeChangeHandler);
        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_TEXT_HORIZONTAL_ALIGN_CHANGE, this.lineTextAlignChangeHandler);
        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_REMOVE, this.deleteSelectedItemLine);
        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_WEIGHT_CHANGE, this.lineWeightChangeHandler);
        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_CLICKED, this.handleItemMenuClicked);

        sheetEventCenter.addListener(sheetEventCenter.REMOVE_ALL_SELECTION, this.removeAllSelection)
        sheetEventCenter.addListener(sheetEventCenter.STOP_EDITING, this.stopEdittingHandler)

        accountPullDownListEventCenter.addListener(accountPullDownListEventCenter.ACCOUNT_PULLDOWN_LOG_OUT, this.handleLogout)
    }

    stopEdittingHandler = () => {
        this.renderer.graph.stopEditing(false)
    }

    memberLeaveUnlockItems = (user) => {
        const graph = this.renderer.graph
        const cells = graph.selectionModel.cells
        this.renderer.memberLeaveUnlockItems(user.clientId);
    }

    handleLogout = () => {  
        if(this.renderer) {
            //[huyvq]--- Remove all selection before logout
            const graph = this.renderer.graph
            const selectedCells = graph.getSelectionCells()
            graph.selectionModel.removeCells(selectedCells)
        }
    }

    removeAllSelection = () => {
        const selectedItems = this.renderer.getSelectedItemModel()
        const selectedBaseItems = this.renderer.getSelectedBaseItemModel()
        const selectedLines = this.renderer.getSelectedLineModel()

        if(
            (selectedItems && selectedItems.length > 0) || 
            (selectedBaseItems && selectedBaseItems.length > 0) || 
            (selectedLines && selectedLines.length > 0)
        ) {
            localStorage.setRemoveSelectionState('true')
        }
        
        sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LOCK_CHANGED, selectedItems, false)
        sheetEventCenter.fireEvent(sheetEventCenter.BASE_ITEM_LOCK_CHANGED, selectedBaseItems, false)
        sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LINE_UNLOCKED, selectedLines)
    }

    /**
     * event after component update
     */
    componentDidUpdate = (prevProps) => {
        const { currentSheet, isSheetLocked } = this.props;
        const { currentSheet: prevSheet } = prevProps;

        if (currentSheet && prevSheet
            && typeof currentSheet !== 'undefined'
            && currentSheet.id !== prevSheet.id
        ) {
            this.renderGraph();
        }
        this.lockFlagListener(isSheetLocked, prevProps.isSheetLocked);

        if (this.props.isLoading !== prevProps.isLoading) {
            this.setLoadingState(this.props.isLoading);
        }
        
        // if (menuAction && menuAction !== prevMenuAction) {
        //     this.listenMenuAction();
        // }
    }

    changeSelectedItemsShape = (shape) => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        let baseItemModels = this.renderer.getSelectedBaseItemModel();
        if (itemModels.length > 0) {
            this.renderer.updateItemsShape(itemModels, shape);
        }

        if (baseItemModels.length > 0) {
            this.renderer.updateBaseItemsShape(baseItemModels, shape);
        }
    }

    changeSelectedItemsStyle = (style) => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        let baseItemModels = this.renderer.getSelectedBaseItemModel();
        if (itemModels.length > 0) {
            this.renderer.updateItemsStyle(itemModels, style);
        }

        if (baseItemModels.length > 0) {
            this.renderer.updateBaseItemsStyle(baseItemModels, style);
        }
    }

    itemTextChangeHandler = (horizontalAlign) => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        let baseItemModels = this.renderer.getSelectedBaseItemModel();

        if (itemModels.length > 0) {
            this.renderer.updateItemsTextAlignment(itemModels, horizontalAlign);
        }

        if (baseItemModels.length > 0) {
            this.renderer.updateBaseItemsTextAlignment(baseItemModels, horizontalAlign);
        }
    }

    itemVerticalTextAlignChangeHandler = (verticalAlign) => {
        this.renderer.stopEditting();
        let baseItemModels = this.renderer.getSelectedBaseItemModel();

        if (baseItemModels.length > 0) {
            this.renderer.updateBaseItemsVerticalTextAlignment(baseItemModels, verticalAlign);
        }
    }

    handleItemMenuClicked = (menuType) => {
        this.renderer.stopEditting();
    }

    handleItemAttachFileClick = (item) => {
        if (item && item.parent && item.parent.itemType === KItemType.DEFAULT_ITEM) {
            let itemModel = item.parent.data;
            this.downloadAttachFileFromItemModel(itemModel)
            this.showAttachFileImage = item.isAttachItemImage;
        }
    }

    /**
    * handle download slection item
    */
    itemDownloadFileHandler = () => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        if (itemModels.length === 1) {
            this.downloadAttachFileFromItemModel(itemModels[0])
        }
    }
    
    downloadAttachFileFromItemModel = (itemModel) => {
        if (itemModel) {
            if (itemModel.hasAttachFilePassword === true) {
                this.showDownloadFileInputPasswordModal(itemModel);
            } else {
                this.downloadAttachFileHandler(itemModel, this.downloadFilePassword);
            }
        }
    }
    
    clearSelectItems = () => {
        this.renderer.clearSelectionGraph();
    }

    handleOkDownloadFileInputPassword = () => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Ok_Password_Dowload_File');
        let itemModels = this.renderer.getSelectedItemModel();
        let itemModel = this.state.downloadFileModel;
        if (itemModels.length === 1) {
            itemModel = itemModels[0];
        } 

        if (itemModel) {
            this.downloadAttachFileHandler(itemModel, this.downloadFilePassword);
            this.hideDownloadFileInputPasswordModal();
        }
    }

    downloadAttachFileHandler = (itemModel, password) => {
        this.renderer.stopEditting();
        let attachFile = itemModel.attachFile;
        if (attachFile.scanResult === 0) {
            this.renderer.documentFileReferenceDownloadAction(itemModel, password);
        } else {
            showWarningDialog("editor.c0004", {
                showCancel: true,
                onOKCallback: () => {
                    return this.renderer.documentFileReferenceDownloadAction(itemModel, password);
                }
            })
        }      
    }
    
    /**
    * handle item upload file completed
    */
    itemUploadFileCompletedHandler = (uploadResult) => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        if (uploadResult && itemModels.length) {
            const virusCheckResult = parseInt(uploadResult.virusCheckResult);
            if (virusCheckResult === 0) {
                this.renderer.handleItemUploadFileCompleted(itemModels, uploadResult);
            } else if (virusCheckResult === 5) {
                // closeUploadingDisplay();
                showWarningDialog("editor.e0003");
            } else {
                showWarningDialog("editor.c0003");
            }
        }
    }

    /**
    * handle item setting password for attach file
    */
    itemSettingPasswordHandler = (password) => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        if (itemModels.length === 1) {
            const { sessionDto } = localStorage.getUserSession();
            let itemModel = itemModels[0];

            itemModel.attachFilePasswordUserId = sessionDto.userId;

            this.renderer.handleItemAttachFileSettingPassword(itemModel, password);
        }
    }

    itemRemovePasswordHandler = () => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        if (itemModels.length === 1) {
            let itemModel = itemModels[0];
            this.renderer.handleItemAttachFileRemovePassword(itemModel);
        }
    }
    /**
     * handle font size btn cliked in item context menu
     * @param {Boolean} isIncrease
     */
    itemTextSubMenuFontSizeChangeHandler = (isIncrease) => {
        this.renderer.stopEditting();
        let itemModels = this.renderer.getSelectedItemModel();
        let baseItemModels = this.renderer.getSelectedBaseItemModel();

        if (itemModels.length > 0) {
            this.renderer.updateItemsFontSize(itemModels, isIncrease);
        }

        if (baseItemModels.length > 0) {
            this.renderer.updateBaseItemsFontSize(baseItemModels, isIncrease);
        }
    }

    lineTypeChangeHandler = (lineType) => {
        this.renderer.stopEditting();
        const lineItemModel = this.renderer.getSelectedLineModel();
        if(lineItemModel){
            this.renderer.updateLineType(lineItemModel, lineType);
        }
    }


    lineColorChangeHandler = (color) => {
        this.renderer.stopEditting();
        const lineItemModel = this.renderer.getSelectedLineModel();
        if(lineItemModel){
            this.renderer.updateLineColor(lineItemModel, color);
        }
    }

    lineSizeChangeHandler = (isIncrease) => {
        this.renderer.stopEditting();
        const lineItemModel = this.renderer.getSelectedLineModel();
        if(lineItemModel){
            this.renderer.updateItemLineTextSize(lineItemModel, isIncrease);
        }
    }

    lineTextAlignChangeHandler = (horizontalAlign) => {
        this.renderer.stopEditting();
        const lineItemModel = this.renderer.getSelectedLineModel();
        if (lineItemModel) {
            this.renderer.updateItemLineHorizontalAlign(lineItemModel, horizontalAlign);
        }
    }
    
    lineWeightChangeHandler = (weight) => {
        this.renderer.stopEditting();
        const lineItemModel = this.renderer.getSelectedLineModel();
        if(lineItemModel){
            this.lastItemLineWeight = weight;
            this.renderer.updateLineLarge(lineItemModel, weight);
        }
    }

    addHighlightItem = (itemId) => {
        this.highlightItems.push(itemId);
    }

    /**
     * create new item then add to graph
     */
    doCreateNewItem = (x, y) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        const { currentSheet } = this.props;
        const isSheetLocked = currentSheet.lockFlag;

        if (isSheetLocked) { return; }

        let newModel = this.createNewItemModel();
        const newItemModelValue = newModel.text;
        
        //default text when create new cell is blank
        newModel.text = null;
        let newItemModel = this.renderer.addNewItem(newModel, x, y);

        if (newItemModel) {
            
            //set text to send to server 
            newModel.text = newItemModelValue;
            this.pasting = false;
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_ADDED_NEW, [newItemModel]);
        }

    }
    /**
     * sheet property
     */
    handleShowSheetProperty = () => {
        const { isSheetPropertyOpen } = this.state;
        if (isSheetPropertyOpen) {
            this.setState({ isSheetPropertyOpen: false }, () => {
                sheetViewEventCenter.fireEvent(sheetViewEventCenter.SHEET_PROPERTY_CLOSED);
            });
        } else {
            this.setState({ isSheetPropertyOpen: true }, () => {
                sheetViewEventCenter.fireEvent(sheetViewEventCenter.SHEET_PROPERTY_OPENED);
            });
        }

    }
    
    /**
     * sheet pull down thay đổi flag của savePngBySelected
     */
    handleToggleSavePNGBySelectedFlag = (savePngBySelectedFlag) => {
        this.renderer.setSavePngBySelectedFlag(savePngBySelectedFlag);
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG_BY_SELECTED_ENABLE, savePngBySelectedFlag);
    }

    /**
     * exrport png by selected on graph
     */
    exportPngBySelected = async (bounds, scale) => {
        const border = 1;
        let request = await this.renderer.getRequestRenderImageGraph(bounds, scale, border, false);

        // Handle Lost Intenet Connection
        if (!navigator.onLine) {
            this.exportFileNotExist();
            return;
        }

        fileService.invokeXmlToBase64(request).then(this.createRequestUploadExportFilePng).catch(error => {
            showWarningDialog("editor.w0002")
        });
    }

    
    /**
     * Export PNG from sheet
     * <p>シートの PNG エクスポート</p>
     */
    exportPNGFromSheet = async () => {
        const bounds = this.renderer.graph.view.getGraphBoundsForPNGExport();
        const scale = 1;
        const border = 10;
        // export file 1x1 px when data empty
        if (!bounds) {
            return this.createRequestUploadExportFilePng(BASE64_IMAGE_1PX);
        }
        // Handle export PNG image
        // let base64Svg = await this.renderer.getBase64Svg(bounds, scale);
        let request = await this.renderer.getRequestRenderImageGraph(bounds, scale, border, true);
        // Handle Lost Intenet Connection
        if (!navigator.onLine) {
            return this.exportFileNotExist();
        }

        fileService.invokeXmlToBase64(request).then(this.createRequestUploadExportFilePng).catch(error => {
            showWarningDialog("editor.w0002")
        });
    }

    exportFileNotExist = () => {
        const { currentSheet } = this.props;
        const sheetName = currentSheet.name ? currentSheet.name : CONSTANTS.UNTITLED_NAME;
        const fileName = `${sheetName}.png`;
        let requestDto = new UploadExportFileDto();

        requestDto.fileName = fileName;
        fileService.sendExportFile(requestDto);
    }

    createRequestUploadExportFilePng = async (converResult) => {
        const { currentSheet } = this.props;
        let sheetName = currentSheet.name ? currentSheet.name : CONSTANTS.UNTITLED_NAME;
        const fileName = sheetName + '.png' ;
        let requestDto = new UploadExportFileDto();

        requestDto.fileData = converResult;
        requestDto.fileName = fileName;

        this.uploadExportFile(requestDto);
    }

    handleExportDataCSV = async () => {
        const { currentSheet } = this.props;
        let requestDto = new UploadExportFileDto();
        let csvDataExport = this.renderer.createCsvData();
        let sheetName = currentSheet.name ? currentSheet.name : CONSTANTS.UNTITLED_NAME;
        requestDto.fileData = csvDataExport;
        requestDto.fileName = `${sheetName}.csv`;

        // Handle Lost Intenet Connection
        if (!navigator.onLine) {
            this.exportFileNotExist();
            return;
        }

        this.uploadExportFile(requestDto)
    }

    uploadExportFile = async (request) => {
        if (isProductEnv() === true) {
            fileService.sendExportFile(request);
        } else {
            await sheetService.uploadExportFile(request).then(this.uploadExportFileResult).catch(error => {
                showWarningDialog("editor.w0002")
            });
        }        
    }

    uploadExportFileResult = (uploadResult) => {
        if (uploadResult) {
            localStorage.setLocalStrage(localStorage.UPLOAD_EXPORT_FILE_RESULT, uploadResult)
            window.open(UPLOAD_EXPORT_FILE_PATH_PREFIX, '_blank');
            // let newWindow =  window.open('/uploadExportFile', '_blank');
            // newWindow.document.write(uploadResult);
        }
    }

    renderSheetProperty = () => {
        return (this.state.isSheetPropertyOpen &&
            <SheetProperty isSheetLocked={this.props.isSheetLocked} />
        );
    }

    doCopySelectedItems = () => {
        const itemType = this.renderer.getSelectedItemType();
        if (!itemType) {
            return;
        }
        let modelSelections = this.renderer.getSelectedModelByType(itemType);

        let copiedItemModels = [];
        if (modelSelections.length < 1) {
            return;
        }
        modelSelections = this.convertItemText(modelSelections);
        modelSelections.forEach((model) => {
            if (model != null) {
                if (itemType === KItemType.DEFAULT_ITEM &&
                    (typeof model.hasAttachFilePassword === 'undefined' || model.hasAttachFilePassword)
                ) {
                    return;
                }

                if( typeof model.relationLines !== 'undefined' && model.relationLines.length > 0 ){
                    let relationLines = [];
                    for (let index = 0; index < model.relationLines.length; index++) {
                        const line = model.relationLines[index];

                        // lấy edge model mới nhất trong graph
                        const lineModel = this.renderer.getCellModelById(line.id);
                        if( lineModel ){
                            relationLines.push(lineModel);
                        }
                    }

                    if( relationLines.length > 0 ){
                        model.relationLines = relationLines;
                    }
                }
                let modelCopied = { ...model };
                copiedItemModels.push(modelCopied);
            }
        });
        if (copiedItemModels.length > 0) {
            this.clipboardModels = copiedItemModels;
            this.clipboardModelsType = itemType;
            editPullDownListEventCenter.fireEvent(editPullDownListEventCenter.EDIT_PULLDOWN_PASTE_ITEM_ENABLE, true);
        }
    }

    doPasteItem = () => {
        const { clipboardModels, clipboardModelsType } = this;
        const { currentSheet, mapInfo } = this.props.mapContainerContext;
        const { sessionDto } = localStorage.getUserSession();

        let itemModels = [];
        let dx = 0;
        let dy = 0;
        let tdx = 0;
        let tdy = 0;
        let sdx = 0;
        let sdy = 0;
        const strokeWidth = StageConfig.innerStrokeWidth*2;
        if (clipboardModels.length < 1) {
            return;
        }
        clipboardModels.forEach(model => {
            dx = model.x + CONSTANTS.COPY_ITEM_OFFSET;
            dy = model.y + CONSTANTS.COPY_ITEM_OFFSET;

            if (clipboardModelsType === KItemType.BASE_ITEM) {
                tdx = model.textX + CONSTANTS.COPY_ITEM_OFFSET;
                tdy = model.textY + CONSTANTS.COPY_ITEM_OFFSET;
                sdx = dx - tdx;
                sdy = dy - tdy;

                if (dx < 0) {
                    dx = 0;
                } else if (dx > StageConfig.MAX_WIDTH - model.width - strokeWidth) {
                    dx = StageConfig.MAX_WIDTH - model.width - strokeWidth;
                }

                if (dy < 0) {
                    dy = 0;
                } else if (dy > StageConfig.MAX_HEIGHT - model.height - strokeWidth) {
                    dy = StageConfig.MAX_HEIGHT - model.height - strokeWidth;
                }

                model.x = dx;
                model.y = dy;

                model.textX = dx - sdx;
                model.textY = dy - sdy;

            } else {
                if (dx < 0) {
                    dx = 0;
                }

                if (dx < 0) {
                    dx = 0;
                } else if (dx > StageConfig.MAX_WIDTH - model.width - strokeWidth) {
                    dx = StageConfig.MAX_WIDTH - model.width - strokeWidth;
                }

                if (dy < 0) {
                    dy = 0;
                } else if (dy > StageConfig.MAX_HEIGHT - model.height - strokeWidth) {
                    dy = StageConfig.MAX_HEIGHT - model.height - strokeWidth;
                }

                model.x = dx;
                model.y = dy;
            }

            model.sheetId = currentSheet.id;
            model.mapId = mapInfo.id;
            model.contractId = mapInfo.contractId;
            model.createUserId = sessionDto.userId;
            model.createUserName = sessionDto.name;
            model.createTime = Date();
            model.updateTime = Date();
            
            itemModels.push(model);
        });

        this.pasting = true;
        if (clipboardModelsType === KItemType.DEFAULT_ITEM) {
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_ADDED_NEW, itemModels);
        } else if (clipboardModelsType === KItemType.BASE_ITEM) {
            sheetEventCenter.fireEvent(sheetEventCenter.BASE_ITEM_ADDED_NEW, itemModels);
        }
    }

    hideModalCreateitemFromDoc = () => {
        this.setState({isCreateItemFromDocumentDialogOpen: false});
    }

    handleCreateItemFromDoc = () => {
        this.setState({isCreateItemFromDocumentDialogOpen: true});
    }

    /**
     * [ThuyTv]: Create new items by text string
     * each newline in text string is an item
     * @param {String} docs
     */
    docreateItemsFromDocs = (docs) => {
        let newItemsValues = docs.split('\n');
        const newItemsValuesCount = newItemsValues.length;
        let newItems = [];
        let row = 0;
        let col = 0;
        const BASEX = 10;
        const BASEY = 32;
        const XSPAN = 532;
        const YSPAN = 92;
        const MAX = 100;
        for(let i = 0; i < newItemsValuesCount; i ++){
            if(newItemsValues[i] === ''){
                continue;
            }

            const newItemModel = this.createNewItemModel();

            //is item out of stage y
            if((row * YSPAN + YSPAN) > (StageConfig.MAX_HEIGHT)){
                row = 0;
                col = col + 1;
            }

            newItemModel.text = newItemsValues[i];
            newItemModel.x = col * XSPAN + BASEX;
            newItemModel.y = row * YSPAN + BASEY;
            newItemModel.style = this.lastEditedItemStyle;
            newItemModel.shape = this.lastEditedItemShape;
            newItemModel.textSize = this.lastItemFontSize;
            newItemModel.textHorizontalAlign = this.lastItemTextHorizontalAlign;

            newItems.push(newItemModel);

            if(newItems.length >= MAX){
                break;
            } 
            row = row + 1;
        }

        this.renderer.addItems(newItems);
        this.pasting = false;
        // const EVENT_MAX = 50;
        // if (newItems.length > EVENT_MAX) {
        //     let halfLenght = Math.floor(newItems.length / 2);
        //     let firstHalf = newItems.splice(0, halfLenght);
        //     let secondHalf = newItems;
        //     let newItemArray = [firstHalf, secondHalf];
        //     let newItemArrayCount = newItemArray.length
        //     for (let i = 0; i < newItemArrayCount; i++) {
        //         sheetEventCenter.fireEvent(sheetEventCenter.ITEM_ADDED_NEW, newItemArray[i]);
        //     }
        // } else {
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_ADDED_NEW, newItems);
        // }
    }

    /**
    * sheet pull down thay đổi flag lock base item
    */
    handleSheetPullDownLockBaseItem = () => {
        let isAllBaseItemLocked = !this.renderer.getAllBaseItemLockedFlag();
        this.renderer.setAllBaseItemLocked(isAllBaseItemLocked);
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_LOCK_BASE_ITEM_ENABLE, isAllBaseItemLocked);    
    }

    /**
     * create a new base item
     */
    doCreateNewBaseItem = () => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;
        
        let model = this.createNewBaseItemModel();
        let baseItemModel = this.renderer.addNewBaseItem(model);
        if (baseItemModel) {
            // TODO: NAMNH Refactor
            // if (this.lastBaseItemFontSize >= CONSTANTS.MIN_FONT_SIZE) {
            //     baseItem.setStyle("fontSize", this.lastBaseItemFontSize);
            // }
            this.pasting = false;
            sheetEventCenter.fireEvent(sheetEventCenter.BASE_ITEM_ADDED_NEW, [baseItemModel]);
            sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_LOCK_BASE_ITEM_ENABLE, false);
            // this.renderer.setSelectAndEditBaseItem(baseItem);
        }
    }

    /**
     * create new base item model
     */
    createNewBaseItemModel = () => {
        let model = new BaseItemModel();
        const { currentSheet, mapContainerContext } = this.props;
        const { sessionDto } = localStorage.getUserSession();

        model.contractId = mapContainerContext.mapInfo.contractId;
        model.mapId = mapContainerContext.mapInfo.id;
        model.sheetId = currentSheet.id;
        if (model.createUserName.length < 1) {
            model.createUserName = sessionDto.name;
            model.editUserName = sessionDto.name;
        }

        model.uid = uuidv4();
        model.text = i18n.t('editor.newBaseItem.label');

        model.textHorizontalAlign = this.lastBaseItemFontAlign;
        model.textVerticalAlign = this.lastBaseItemFontVerticalAlign;
        model.textSize = this.lastBaseItemFontSize;
        model.shape = this.lastBaseItemShapeType;
        model.style = this.lastBaseItemStyleType;

        return model;
    }

    handleItemAddedByCopy = (items) => {
        Logger.logConsole("handleItemAddedByCopy", items);
    }

    handleItemLocked = (items) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;
        // let loadingMouse = createLoadingMouse();
        
        // loadingMouse.hide();
        const selectedItem = this.renderer.getSelectedItemModel();
        editPullDownListEventCenter.fireEvent(editPullDownListEventCenter.SET_ITEM_SELECTED, selectedItem.length !== 0);
        pullDownButtonEvent.fireEvent(pullDownButtonEvent.HIDE_MENU);
        sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LOCK_CHANGED, items, true);
        // loadingMouse.hide();
    }

    handleItemUnlocked = (items) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        const selectedItem = this.renderer.getSelectedItemModel();

        editPullDownListEventCenter.fireEvent(editPullDownListEventCenter.SET_ITEM_SELECTED, selectedItem.length !== 0);
        pullDownButtonEvent.fireEvent(pullDownButtonEvent.HIDE_MENU);
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_DO_CLOSE);
        sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LOCK_CHANGED, items, false);
    }

    handleBaseItemLocked = (items) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if (items && items.length > 0) {
            items = this.convertItemText(items);
        }
        sheetEventCenter.fireEvent(sheetEventCenter.BASE_ITEM_LOCK_CHANGED, items, true);
        editPullDownListEventCenter.fireEvent(editPullDownListEventCenter.SET_ITEM_SELECTED, true);
    }

    handleBaseItemUnlocked = (items) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if (items && items.length > 0) {
            items = this.convertItemText(items);
        }
        sheetEventCenter.fireEvent(sheetEventCenter.BASE_ITEM_LOCK_CHANGED, items, false);
        editPullDownListEventCenter.fireEvent(editPullDownListEventCenter.SET_ITEM_SELECTED, false);
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_DO_CLOSE);
    }


    convertItemText = (items) => {
        items.forEach(item => {
            let itemText = item.text.toString();
            let cleanText = unEscapeHtml(itemText);
            item.text = cleanText;
        })
        return items;
    }

    handleItemUpdated = (items, evt) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;
        
        if (Array.isArray(items) && items.length > 0) {
            this.lastItemEvt = evt;
            if (evt !== mxEvent.LABEL_CHANGED) {
                items = this.convertItemText(items);
            }
            if (evt !== mxEvent.CELLS_MOVED) {
                //store last edited item info
                let lastItem = items[items.length - 1];
                this.lastEditedItemShape = lastItem.shape;
                this.lastEditedItemStyle = lastItem.style;
                this.lastItemFontSize = lastItem.textSize;
                this.lastItemTextHorizontalAlign = lastItem.textHorizontalAlign;
            }

            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_UPDATED, items);

            if (evt === mxEvent.CELLS_MOVED){
                itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_DO_CLOSE);
            }
        }
        
    }

    handleBaseItemUpdated = (baseItemModels, evt) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if (Array.isArray(baseItemModels) && baseItemModels.length > 0) {
            this.lastBaseItemEvt = evt;
            if (evt !== mxEvent.LABEL_CHANGED) {
                baseItemModels = this.convertItemText(baseItemModels);
            }

            if (evt !== mxEvent.CELLS_MOVED) {
                let lastBaseItem = baseItemModels[baseItemModels.length - 1];
                this.lastBaseItemFontAlign = lastBaseItem.textHorizontalAlign;
                this.lastBaseItemFontVerticalAlign = lastBaseItem.textVerticalAlign;
                this.lastBaseItemFontSize = lastBaseItem.textSize;
                this.lastBaseItemShapeType = lastBaseItem.shape;
                this.lastBaseItemStyleType = lastBaseItem.style;
            }

            sheetEventCenter.fireEvent(sheetEventCenter.BASE_ITEM_UPDATED, baseItemModels);

            if (evt === mxEvent.CELLS_MOVED){
                itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_DO_CLOSE);
            }
        }
    }

    handleLikeItem = (itemModel) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if (itemModel) {
            const { mapContainerContext } = this.props;
            let itemLikeModel = new ItemLikeModel();
            itemLikeModel.contractId = mapContainerContext.mapInfo.contractId;
            itemLikeModel.itemId = itemModel.id;
            itemLikeModel.mapId = mapContainerContext.mapInfo.id;
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LIKE_CHANGED, itemLikeModel);
        }   
    }

    handleItemRemoved = (ids, isMine) => {
        Logger.logConsole("handleItemRemoved", ids);
    }

    handleItemUploadFileCompleted = (itemModel, fileName, s3Path, virusCheckResult) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if (itemModel && fileName && s3Path) {
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_UPLOAD_ATTACH_FILE, itemModel, fileName, s3Path, virusCheckResult);
        }
    }

    /**
    * call api download attach file 
    */
    handleItemDownloadFile = (itemModel, password) => {
        // Logger.logConsole("itemModel", itemModel)
        if (itemModel != null) {
            const { mapContainerContext } = this.props;

            let request = new DownloadAttachFileConfigDto();

            request.contractId   = mapContainerContext.mapInfo.contractId;
            request.mapId = mapContainerContext.mapInfo.id;
            request.itemId       = itemModel.id;
            request.filePassword = password;

            this.downloadAttachFile(request)
        }
    }

    downloadAttachFile = async (request) => {
        await sheetService.downloadAttachFile(request).then(this.downloadAttachFileResult);
    }

    downloadAttachFileResult = (downloadResult) => {
        let showAttachFileImage  = this.showAttachFileImage;
        if (downloadResult.isSucceed === true) {
            if (showAttachFileImage === true) {
                this.setState({
                    attachFileImageModal: {
                        enable: true,
                        imageUrl: downloadResult.downloadUrl
                    }
                });
                // Logger.logConsole("showAttachFileImage", showAttachFileImage)
            } else {
                if (isProductEnv() === true) {
                    fileService.sendDownloadFile(downloadResult);
                } else {
                    localStorage.setLocalStrage(localStorage.DOWNLOAD_RESULT, downloadResult)
                    window.open(DOWNLOAD_PATH_PREFIX, '_blank');
                }
            }

        } else {
            // showWarningDialog()
        }
        this.showAttachFileImage = false;
    }

    
    handleItemSettingAttachFilePassword = (itemModel, password) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;
        if (itemModel && password) {
            Logger.logConsole({itemModel, password})
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_SET_ATTACH_FILE_PASSWORD, itemModel, password);
        }
    }

    handleItemClearAttachFilePassword = (itemModel) => {
        if (itemModel) {
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_CLEAR_ATTACH_FILE_PASSWORD, itemModel);
        }
    }
    /**
     * TODO: migrate from flash
     * 選択中のアイテムを削除します。
     */
    deleteSelectedItems = (selectedItems) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;
        
        if (typeof selectedItems === 'undefined') {
            const itemModels = this.renderer.deleteSelectedCells();
            if (itemModels !== null) {
                const models = itemModels.deleteModels;
                const itemType = itemModels.itemType;
                if (itemType === KItemType.DEFAULT_ITEM) {
                    sheetEventCenter.fireEvent(sheetEventCenter.ITEM_REMOVED, models);
                } else if (itemType === KItemType.BASE_ITEM) {
                    sheetEventCenter.fireEvent(sheetEventCenter.BASE_ITEM_REMOVED, models);
                } else if (itemType === KItemType.LINK_ITEM) {
                    const itemLineModel = models[0];
                    sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LINE_REMOVE, itemLineModel);
                }
            }
        }
    }

    doZoomGraph = (step, isZoomIn) => {
        
        this.renderer.zoomGraph(step, isZoomIn);
    }

    setZoomFactor = (currentZoomValue, maxZoomValue) => {
        this.setCurrentZoomValue(currentZoomValue);
        this.setMaxZoomValue(maxZoomValue);
        this.renderer.setZoomFactor(currentZoomValue, maxZoomValue);
    }

    /**
     * @author ThuyTV
     * @description handle graph zoom value change
     * @param {number} currentZoom
     * @return void
     */
    handleZoomValueChange = (currentZoom) => {
        this.setCurrentZoomValue(currentZoom);
    }

    beforeHandleRemoveSheet = () => {
        this.renderer.clearSelectionGraph();
        this.setSheetDeletingFlag(true);
    }

    setSheetDeletingFlag = (flag) => {
        this.renderer.setSheetDeletingFlag(flag);
    }

    /**
     * @author ThuyTV
     * @description update mxgraph enable state if lock flag change
     * @param {boolean} currentLockFlag
     * @param {boolean} prevLockFlag
     * @return {void}
     */
    lockFlagListener = (currentLockFlag, prevLockFlag) => {
        if (this.renderer && (currentLockFlag !== prevLockFlag)) {
            let isEnabled = !currentLockFlag && !this.props.isLoading;
            this.renderer.setEnabled(isEnabled);
        }
    };

    setLoadingState = (isLoading) => {
        let isEnabled = !this.props.isSheetLocked && !isLoading;
        if (this.renderer && typeof this.renderer.setEnabled === 'function') {
            this.renderer.setEnabled(isEnabled);
        }
    }

    renderBackgroundSheet = (sheetProperty) => {
        const classNameContentSheet = ['content-sheet content-sheet-editor'];

        if (sheetProperty) {
            const backGroundClass = BackgroundSheetConsts[sheetProperty.backgroundUri] || BackgroundSheetConsts[SheetBG.sheetBG1];
            classNameContentSheet.push(backGroundClass);
            return classNameContentSheet.join(" ");
        }
    }

    renderLockButton = () => {
        const { currentSheet, unLockSheet, lockSheet } = this.props;

        if (currentSheet.lockFlag) {
            return <SheetUnLockButton unLockSheet={() => {
                ActionLogger.click('User_Editor_SheetView', 'Unlock_Sheet_Button');
                unLockSheet(currentSheet)}} />;
        }

        return <SheetLockButton lockSheet={() => {
                ActionLogger.click('User_Editor_SheetView', 'Lock_Sheet_Button');
                lockSheet(currentSheet)}} />

    }

    renderAttachFileImageModal = () => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Show_Attach_File_Image_Modal');
        const width = window.innerWidth*3/4;
        const height = window.innerHeight*4/5;
        return (<CommonModal
            show={true}
            backGroundClass="bg-modal bg-modal-editor image-modal"
            backDropModal="modal-action"
            onHide={this.hideAttachFileImageModal}
            onBackgroundClick={this.hideAttachFileImageModal}
        >
            
            <div className="text-center modal-gr-btn modal-gr-btn-editor">
                <img src={imgClose} className="image-modal-close" onClick={() => this.hideAttachFileImageModal()} width="20" alt="" />
                <img src={this.state.attachFileImageModal.imageUrl} style={{maxWidth:width, maxHeight:height}} alt="" />
               
            </div>
        </CommonModal>);
    }

    hideAttachFileImageModal = () => {
        this.setState({
            attachFileImageModal: {
                enable: false,
                imageUrl: ""
            }
        })
    }

    onBlurDownloadFileInputPassword = () => {
        this.forceUpdate();
    }

    /**
    * render show input download file
    */
    renderDownloadFileInputPasswordModal = () => {
        const inputClasses = ["form-control form-control-shadow form-control-editor form-control-shadow-editor placeholder-italic placeholder-italic-editor modal-textarea modal-textarea-editor"];
        if (this.downloadFilePassword === "") {
            inputClasses.push("form-control-empty form-control-empty-editor")
        } else {
            inputClasses.push("form-control-valid")
        }

        return (<CommonModal
            show={true}
            backGroundClass="bg-modal bg-modal-editor"
            backDropModal="modal-action"
            onHide={()=>{
                ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Icon_Close_Modal_Password_Dowload');
                this.hideDownloadFileInputPasswordModal();
                }}>
            <div className="w-343px w-343px-editor mx-auto mb-21px mb-21px-editor">
                <p className="fs-12px fs-12px-editor">{i18n.t('editor.itemPropertiy.attachfile.download')}</p>
                <input 
                    type="password" 
                    name="input-password" 
                    onChange={(e) => this.onChangeDownloadFileInputPassword(e)} 
                    onBlur={this.onBlurDownloadFileInputPassword}
                    className={inputClasses.join(' ')} />
            </div>
            <div className="text-center modal-gr-btn modal-gr-btn-editor">
                <button type="submit" onClick={() => {
                    ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Cancel_Modal_Password_Dowload');
                    this.hideDownloadFileInputPasswordModal()
                }}
                        className="btn btn-editor btn-editor btn-whitefl btn-whitefl-editor">{i18n.t('editor.itemPropertiy.attachfile.cancel')}</button>
                <button type="submit" onClick={this.handleOkDownloadFileInputPassword} className="btn btn-editor btn-black btn-black-editor">{i18n.t('global.ok')}</button>
            </div>
        </CommonModal>);
    }


    onChangeDownloadFileInputPassword = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Input_Password_Field');
        this.downloadFilePassword = e.target.value;
    }

    showDownloadFileInputPasswordModal = (itemModel) => {
        this.setState({
            showDownloadFileInputPasswordModal: true,
            downloadFileModel: itemModel
        })
    }

    hideDownloadFileInputPasswordModal = () => {
        this.setState({
            showDownloadFileInputPasswordModal: false,
            downloadFileModel: null
        })
        this.downloadFilePassword = "";
    }

    graphContainerOnMouseDown = (evt) => {
        //close pulldown menu while user touch the graph container
        pullDownButtonEvent.fireEvent(pullDownButtonEvent.HIDE_MENU);
    }

    graphContainerTouchStart = (evt) => {
        //close pulldown menu while user touch the graph container
        pullDownButtonEvent.fireEvent(pullDownButtonEvent.HIDE_MENU);
    }

    /**
     * React Component Render
     */
    render = () => {
        const { pulldownIcon, currentSheet, sheetLockButtonVisible } = this.props;
        const { mapInfo } = this.props.mapContainerContext;
        return (<React.Fragment>
            <div className={this.renderBackgroundSheet(currentSheet)}>
                <div className="list-group-ac list-group-ac-editor">
                    <div className="d-flex">
                        {sheetLockButtonVisible && this.renderLockButton()}
                        <SheetShareButton pulldownIcon={pulldownIcon} currentSheet={currentSheet} mapInfo={mapInfo}/>
                    </div>
                </div>
                <div className="container-wrapper bg-transparent">
                    <div
                        onMouseDown={this.graphContainerOnMouseDown}
                        onTouchStart={this.graphContainerTouchStart}
                        className="container-graph" ref="graphContainer" >
                        <ItemMenuButtonBar ref="contextGroupMenuContainer" />
                        <ItemLineMenuButtonBar ref="contextGroupEdgeMenuContainer" />
                        {/* [huyvq]---container for print-guide */}
                        <div
                            ref="printGuide"
                            style={{
                                        position: "absolute", 
                                        zIndex: "2",
                                        pointerEvents: "none",
                                        width: "100vw",
                                        height: "100vh",
                            }}
                        >
                            <svg 
                                style={{width: "5000px", height: "5000px", position: "absolute", left: "-2000px", top: "-2000px"}}  // pull svg position back to left then rescale the print guide bound (in KGraph View) (add 2000 to x and y) to prevent breaking print guide on dragging graph 
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <g id="print-guide-svg-g" ref={this.printGuideRef}></g>
                            </svg>
                        </div>
                    </div>

                    <ExpandSlider
                        currentZoomValue={this.state.currentZoomValue}
                        maxZoomValue={this.state.maxZoomValue}
                        minZoomValue={ZoomConfig.MIN_ZOOM_VALUE}
                        doZoomGraph={this.doZoomGraph}
                        setZoomFactor={this.setZoomFactor}
                    />
                        
                    <SheetNavigator ref="sheetNavigatorContainer" />
                </div>
            </div>
            <CreateItemFromDocDialog 
                onCancel={this.hideModalCreateitemFromDoc}
                onCreateItems={this.docreateItemsFromDocs}
                isShow={this.state.isCreateItemFromDocumentDialogOpen}
            />
            {this.renderSheetProperty()}
            {this.state.showDownloadFileInputPasswordModal && this.renderDownloadFileInputPasswordModal()}
            {this.state.attachFileImageModal.enable && this.renderAttachFileImageModal()}
        </React.Fragment>)
    }

    /**
     * create new item model
     */
    createNewItemModel = () => {
        const { currentSheet, mapInfo } = this.props.mapContainerContext;
        const { sessionDto } = localStorage.getUserSession();

        let model = new ItemModel();
        model.text = i18n.t('editor.newItem.label');
        model.uid = uuidv4();

        if (this.lastEditedItemShape !== KGraphItemShape.NONE) {
            model.shape = this.lastEditedItemShape;
        } else {
            model.shape = KGraphItemShape.RECT;
        }
        if (this.lastEditedItemStyle !== KItemStyle.NONE) {
            model.style = this.lastEditedItemStyle;
        } else {
            model.style = KItemStyle.D;
        }

        model.kind = KItemKind.TEXT;
        model.textHorizontalAlign = this.lastItemTextHorizontalAlign;

        if(this.lastItemFontSize !== 0){
            model.textSize = this.lastItemFontSize;
        }else {
            model.textSize = KConstants.DEFAULT_ITEM_FONT_SIZE;
        }

        model.sheetId = currentSheet.id;
        model.mapId = mapInfo.id;
        model.contractId = mapInfo.contractId;
        model.updateUserId = sessionDto.userId;
        model.createUserId = sessionDto.userId;
        model.createUserName = sessionDto.name;
        model.updateUserName = sessionDto.name;

        return model;
    }

    /******************************************** ITEM EVENTS *************************************************************** */

    /**
     * //TODO: migrate from flash
     * <p>アイテム追加のメッセージ受信処理。</p>
     * @param models    追加されたアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     * @param fault boolean | is error
     */
    itemAdded = (models, isMine, fault) => {
        if (fault) {
            if (isMine) {
                this.renderer.deleteItemsByModelsAndItemType(models, KItemType.DEFAULT_ITEM);
            }
            this.onFault(fault);
        } else {
            const isPasting = this.pasting;
            // if (this.pasting) {
            //     this.pasting = false;
            // }
           
            this.renderer.addItems(models);
            if (isMine && isPasting !== true) {
                //Select item added new by current user
                if (models && models.length === 1) {
                    this.renderer.setSelectAndEditItem(models[0]);
                }
            }
        }

        let itemLineModels = [];
        if (models.length > 0) {
            models.forEach(model => {
                if (typeof model.relationLines !== 'undefined' && model.relationLines.length > 0) {
                    itemLineModels = itemLineModels.concat(model.relationLines);
                }
            })
        }
        this.renderer.deployItemLines(itemLineModels);
        this.checkForcedVisibleNewItemFilters(models);
        //TODO: (namnx) update models to this model list of sheets
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテム更新のメッセージ受信処理。</p>
     * @param models    更新があったアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemUpdated = (models, isMine) => {
        if (!isMine) {
            this.renderer.updateItems(models);
        } else {
            let isNotUpdateValue = this.lastItemEvt === mxEvent.CELLS_MOVED ? true : false; 
            this.renderer.updateItemsModel(models, isNotUpdateValue);
            this.modelUpdatedRemoveFilter(models, KItemType.DEFAULT_ITEM);
            
        }
        this.renderer.removeFilterGroupItems(models);
        this.checkForcedVisibleNewItemFilters(models);
    }

    /**
    * item attach file uploaded
    */
    itemAttachFileUploaded = (model, isMine) => {
        // isMine = false 
        this.itemUpdated([model], false)
    }

    /**
    * item attach file uploaded
    */
    itemAttachFilePasswordChanged = (model, isMine, fault) => {
        Logger.logConsole({model, isMine, fault})
        if (fault) {
            return this.onFault(fault);
        }

        this.itemUpdated([model], isMine);
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテム削除のメッセージ受信処理。</p>
     * @param models    削除されたアイテムIDの配列
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemRemoved = (removedItemIds, isMine, fault) => {
        if (fault) {
            this.renderer.revertCellDeleting();
            this.onFault(fault);
        } else {
            this.renderer.deleteItems(removedItemIds);
            if (isMine) {
                this.renderer.showMultiSelectedItemMenu(this.renderer.createSelectedItemsBouns());
            }
            editPullDownListEventCenter.fireEvent(editPullDownListEventCenter.SET_ITEM_SELECTED, false);
        }
    }


    /**
     * //TODO: migrate from flash
     * <p>アイテムロック状態変更のメッセージ受信処理。</p>
     * @param models    ロック状態が変更されたアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemLockStateChanged = (models, isMine, fault) => {
        //[huyvq-bug_ominext_69300]---check if 1 item lock state is changed and the editClientId is not belong to this current tab
        //to remove the selection when 2 or more clients click 1 item at the same time
        if(models.length === 1 && LocalStorageUtils.CLIENT_ID != models[0].editClientId) {
            const selectedCells = this.renderer.graph.getSelectionCells()

            //[huyvq-bug_ominext_69300]--- remove the locked item from selection
            for (let i = 0; i < selectedCells.length; i++) {
                if(selectedCells[i].data && selectedCells[i].data.id === models[0].id) {
                    selectedCells[i].data.preventSendUnlock = true
                    this.renderer.graph.selectionModel.removeCell(selectedCells[i])
                }
            }
        }

        if (fault) {
            this.onFault(fault);
        } else {
            if (Array.isArray(models) && models.length > 0) {
                if (!isMine) {
                    this.renderer.updateItems(models);
                } else {
                    this.renderer.updateItemsModel(models, true);
                }
                this.renderer.setItemEditingState(models);

                if (this.isForcedVisibleNewItemFiltersBy2D || this.isForcedVisibleNewItemFiltersBy6D) {
                    this.renderer.highlightItemsByModelList(models);
                }
            }
        }
    }


    /**
     * //TODO: migrate from flash
     * <p>アイテムのgood状態変更のメッセージ受信処理。</p>
     * @param itemLikeModel    good状態の更新情報
     * @param isMine           自分の操作によって送信されたメッセージであることを示す。
     */
    itemLikeStateChanged = (itemLikeModel, isMine, fault) => {
        if (fault) {
            return this.onFault(fault);
        } 
        this.renderer.itemLikeStateChanged(itemLikeModel, isMine);
    }

    /******************************************** ITEM BASE EVENTS *************************************************************** */
    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム追加のメッセージ受信処理。</p>
     * @param models    追加されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemAdded = (models, isMine, fault) => {
        if (fault) {
            if (isMine) {
                this.renderer.deleteItemsByModelsAndItemType(models, KItemType.BASE_ITEM);
            }
            return this.onFault(fault);
        } 
        this.renderer.addBaseItems(models);
        const isPasting = this.pasting;
        // if (this.pasting) {
        //     this.pasting = false;
        // }
        if (isMine) {
            // set unlock base item after create by isMine 
            if (this.renderer.getAllBaseItemLockedFlag() && isPasting !== true) {
                this.renderer.setAllBaseItemLocked(false);
            }
            // set selection and edit base item
            if (models && models.length === 1 && isPasting !== true) { 
                this.renderer.setSelectAndEditBaseItem(models[0]);
            }
        }
        let itemLineModels = [];
        if (models.length > 0) {
            models.forEach(model => {
                if (typeof model.relationLines !== 'undefined' && model.relationLines.length > 0) {
                    itemLineModels = itemLineModels.concat(model.relationLines);
                }
            })
        }
        this.renderer.deployItemLines(itemLineModels);
        this.checkForcedVisibleNewItemFilters(models);
    }

    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム更新のメッセージ受信処理。</p>
     * @param models    更新されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemUpdated = (models, isMine, fault) => {
        if (fault) {
            return this.onFault(fault);
        } 

        if (!isMine) {
            this.renderer.updateBaseItems(models);
        } else {
            let isNotUpdateValue = this.lastItemEvt === mxEvent.CELLS_MOVED ? true : false; 
            this.renderer.updateBaseItemsModel(models, isNotUpdateValue);
            this.modelUpdatedRemoveFilter(models, KItemType.BASE_ITEM);
        }
        this.checkForcedVisibleNewItemFilters(models);
        /**
         * TODO : migrate from FLASH
        if(selectedItems.length == 1 && isMine && baseItemPropertyMenu.visible) {
            if(selectedItems[0].data.id == model.id) {
                baseItemPropertyMenu.model = model;
            }
        }
        */
    }

    modelUpdatedRemoveFilter = (models, itemType) => {
        if (this.initialItemId) {
            let removeFilter = false;
            let modelFilter = null;

            for (let i = 0; i < models.length; i++) {
                let model = models[i];
                if (model.id === this.initialItemId) {
                    removeFilter = true;
                    modelFilter = model;
                }
            }
            if (removeFilter && modelFilter) {
                this.renderer.removeItemFilters(modelFilter, itemType);
                this.initialItemId = null;
            }
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム削除のメッセージ受信処理。</p>
     * @param removedItemIds    削除されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemRemoved = (removedItemIds, isMine, fault) => {
        if (fault) {
            this.renderer.revertCellDeleting();
            return this.onFault(fault);
        } 
        this.renderer.deleteItems(removedItemIds);
        if (isMine) {
            this.renderer.showMultiSelectedItemMenu(this.renderer.createSelectedItemsBouns());
        }
        editPullDownListEventCenter.fireEvent(editPullDownListEventCenter.SET_ITEM_SELECTED, false);
    }

    /**
     * //TODO: migrate from flash
     * <p>ベースアイテムロック状態変更のメッセージ受信処理。</p>
     * @param models    ロック状態が変更されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemLockStateChanged = (models, isMine, fault) => {
        //[huyvq-bug_ominext_69300]---check if 1 base item lock state is changed and the editClientId is not belong to this current tab
        //to remove the selection when 2 or more clients click 1 base item at the same time
        if(models.length === 1 && LocalStorageUtils.CLIENT_ID != models[0].editClientId) {
            const selectedCells = this.renderer.graph.getSelectionCells()

            //[huyvq-bug_ominext_69300]--- remove the locked base item from selection
            for (let i = 0; i < selectedCells.length; i++) {
                if(selectedCells[i].data && selectedCells[i].data.id === models[0].id) {
                    selectedCells[i].data.preventSendUnlock = true
                    this.renderer.graph.selectionModel.removeCell(selectedCells[i])
                }
            }
        }

        if (fault) {
            this.onFault(fault);
        } else {
            if (!isMine) {
                this.renderer.updateBaseItems(models);
            } else {
                this.renderer.updateBaseItemsModel(models, true);
            }
            this.renderer.setBaseItemsEditingState(models);
        }
    }

    /******************************************** LINE EVENTS *************************************************************** */
    /**
     * //TODO: migrate from flash
     * <p>関係線作成のメッセージ受信処理。</p>
     * @param model     作成された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineAdded = (model, isMine, fault) => {
        if (fault){
            this.onFault(fault);
        } else {
            this.renderer.addLine(model,fault);
            if (isMine) {
                this.renderer.updateItemLine(model,fault);
            }
        }       
    }

    /**
     * //TODO: migrate from flash
     * <p>関係線更新のメッセージ受信処理。</p>
     * @param model     更新された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineUpdated = (model, isMine, fault) => {
        if (fault){
            this.onFault(fault);
        } else {
            if (!isMine) {
                this.renderer.updateItemLine(model,fault);
            } else {
                this.renderer.updateItemLineModel(model,fault);
            }
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>関係線削除のメッセージ受信処理。</p>
     * @param model     削除された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineRemoved = (model, isMine, fault) => {
        if (fault){
            this.renderer.revertCellDeleting();
            this.onFault(fault);
        } else {
            this.renderer.graph.deleteItemLine(model.id);
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>関係線ロック状態変更のメッセージ受信処理。</p>
     * @param model     ロック状態が変更された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineLockStateChanged = (model, isMine, fault) => {
        //[huyvq-bug_ominext_69300]---check if 1 line lock state is changed and the editClientId is not belong to this current tab
        //to remove the selection when 2 or more clients click 1 line at the same time
        if(model && LocalStorageUtils.CLIENT_ID != model.editClientId) {
            const selectedCells = this.renderer.graph.getSelectionCells()
            //[huyvq-bug_ominext_69300]--- remove the locked line from selection
            for (let i = 0; i < selectedCells.length; i++) {
                if(selectedCells[i].data && selectedCells[i].data.id === model.id) {
                    selectedCells[i].data.preventSendUnlock = true
                    this.renderer.graph.selectionModel.removeCell(selectedCells[i])
                }
            }
        }

        if (fault) {
            return this.onFault(fault);
        } else {
            this.lineUpdated(model, isMine);
        }
    }

    doCreateItemLine = (fromModel, toModel, itemType) => {
        const { currentSheet, mapInfo } = this.props.mapContainerContext;
        const { sessionDto } = localStorage.getUserSession();

        if (currentSheet.lockFlag) { return; }

        let lineModel = new RelationLineModel();
        lineModel.startId = fromModel.id;
        lineModel.endId = toModel.id;
        lineModel.kind = itemType === KItemType.BASE_ITEM ? RelationLineKind.BASE_ITEM : RelationLineKind.ITEM;
        lineModel.sheetId = currentSheet.id;
        lineModel.mapId = mapInfo.id;
        lineModel.contractId = mapInfo.contractId;
        lineModel.createUserName = sessionDto.name;
        lineModel.updateUserName = sessionDto.name;
        lineModel.uid = uuidv4();
        lineModel.text = "";
        lineModel.color = this.lastItemLineColor;
        lineModel.style = this.lastItemLineArrowType;
        lineModel.large = this.lastItemLineWeight;
        lineModel.textHorizontalAlign = this.lastItemLineTextHorizontalAlign;
        lineModel.textSize = this.lastItemLineTextSize;

        sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LINE_ADD_NEW, lineModel);
    }

    handleItemLineLocked = (itemLineModel) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if ( typeof itemLineModel.id !== 'undefined') {
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LINE_LOCKED, itemLineModel, true);
        }
    }

    handleItemLineUnlock = (itemLineModel) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if ( typeof itemLineModel.id !== 'undefined') {
            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LINE_UNLOCKED, itemLineModel, true);
            itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_DO_CLOSE);
        }
        
    }

    handleItemLineUpdated = (itemLineModel, evt) => {
        const { getUserSession } = this.props.mapContainerContext;
        const sessionDto = getUserSession();
        if (!sessionDto) return;

        if (itemLineModel) {
            this.lastItemLineColor = itemLineModel.color;
            this.lastItemLineArrowType = itemLineModel.style;
            this.lastItemLineTextSize = itemLineModel.textSize;
            this.lastItemLineWeight = itemLineModel.large;
            this.lastItemLineTextHorizontalAlign = itemLineModel.textHorizontalAlign;

            sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LINE_UPDATED, itemLineModel);
        }
    }

    /******************************************** ERROR EVENTS *************************************************************** */

    /**
     * handle error from socket
     * @param {WlpFault} fault
     * 
     */
    onFault = (fault) => {
        itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_DO_CLOSE);
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_DO_CLOSE);

        showWarningDialog(fault.messageKey, {
            onOKCallback: () => window.location.reload()
        })
    }
    
    deleteSelectedItemLine = ()=>{
        const itemLineModels = this.renderer.getSelectedModelByType(KItemType.LINK_ITEM);
        if( itemLineModels.length > 0 ){
            const itemLineModel = itemLineModels[0];
            const startItemOfItemLine = this.renderer.checkModelExistById(itemLineModel.startId);
            if (startItemOfItemLine) {
                sheetEventCenter.fireEvent(sheetEventCenter.ITEM_LINE_REMOVE, itemLineModel);
            }
        }
    }

    /**
     * 
     * @param {Boolean} isHightLight
     */
    setForcedVisibleItems2DFilters = (isHightLight) => {
        this.isForcedVisibleNewItemFiltersBy2D = isHightLight;
        this.isForcedVisibleNewItemFiltersBy6D = false;
        const limitHour = CONSTANTS.NEW_LIMIT * 2;
        this.setForcedVisibleItemsFilters(limitHour, isHightLight);
    }

    setForcedVisibleItems6DFilters = (isHightLight) => {
        this.isForcedVisibleNewItemFiltersBy6D = isHightLight;
        this.isForcedVisibleNewItemFiltersBy2D = false;
        const limitHour = CONSTANTS.NEW_LIMIT * 6;
        this.setForcedVisibleItemsFilters(limitHour, isHightLight);
    }

    handeGoodButtonToogle = (goodButtonFlag) => {
        this.renderer.setGoodButtonFlag(goodButtonFlag)
    }

    handlePrintingRangeGuideDisplay = (displayFlag) => {
        this.renderer.setPrintingRangeGuideDisplayFlag(displayFlag)
    }

    setForcedVisibleItemsFilters = (limitHour, isHightLight) => {
        if( isHightLight ){
            this.renderer.setForcedVisibleItemsByUpdateTimeFilters(limitHour);
        } else{
            this.renderer.removeAllFilterGroupItems();
        }
    }

    checkForcedVisibleNewItemFilters = (models) => {
        if (this.isForcedVisibleNewItemFiltersBy2D || this.isForcedVisibleNewItemFiltersBy6D) {
            if (typeof models !== 'undefined') {
                this.renderer.highlightItemsByModelList(models);
            } else { 
                const limitHour = this.isForcedVisibleNewItemFiltersBy6D ? CONSTANTS.NEW_LIMIT * 6 : CONSTANTS.NEW_LIMIT * 2;
                this.setForcedVisibleItemsFilters(limitHour, true);
            }
        }
    }
}


export default withMapContainerContext(SheetView);