import React from "react";
import RoleType from "wlp-client-mypage-lib/consts/RoleType";
import { withTranslation } from "react-i18next";
import ActionLogger from "wlp-client-common/ActionLogger";
import ImageDefault from "wlp-client-common/utils/ImageDefault";

class CheckBoxMemeberList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            role: '',
            checked: true
        }
    }
    getSharedUserRole = (role) => {
        const { t } = this.props;
        if (role === RoleType.LEADER) {
            return t('global.leader');
        }
        if (role === RoleType.ADMIN) {
            return t('global.admin');
        }
        if (role === RoleType.MEMBER) {
            return t('global.member');
        }
        if (role === RoleType.PARTNER) {
            return t('global.partner');
        }
        if (role === RoleType.B_TO_C_OWNER) {
            return t('global.owner');
        }
    }

    handlerCheckboxClicked = () => {
        ActionLogger.click('User_Editor_SheetLinkView', 'Checkbox_Share_Sheet');
        const { item, index, updateUserSelected } = this.props;
        const checked = !item.checked;
        updateUserSelected(index, checked);
    }

    render() {

        const { t, item } = this.props;
        let userName = item.name;
        if (item.role === RoleType.B_TO_C_OWNER) {
            userName = <React.Fragment><b><i>{item.name + t('global.owner')}</i></b></React.Fragment>
        }
        let isCheckboxCheckedItem = item.checked;
        return (<React.Fragment>
            <div className="member-share-item member-share-item-editor">
                <label className="checkbox-cus checkbox-cus-editor">
                    <input type="checkbox" className="checkbox-cus-inp checkbox-cus-inp-editor" value={item.userId}
                        onChange={() => this.handlerCheckboxClicked()} 
                        checked={isCheckboxCheckedItem}
                    />
                    <span className="checkbox-cus-checkmark checkbox-cus-checkmark-editor"></span>
                </label>
                <img className='noselect' src={
                    item && item.smallImageUri && ImageDefault[item.smallImageUri] ?
                    ImageDefault[item.smallImageUri] :
                    item.smallImageUri
                } alt="" />
                <div className="text-truncate text-truncate-editor w-98 noselect">
                    {userName}
                </div>
                <div className="member-share-date member-share-date-editor noselect"> {item.sheetLoginTime}
                </div>
                <label className={"member-share-label member-share-label-editor font-weight-bold noselect" + (item.role === RoleType.LEADER || item.role === RoleType.ADMIN ? " label-dark label-dark-editor" : " label-white label-white-editor")}>
                    {this.getSharedUserRole(item.role)}
                </label>
            </div>
        </React.Fragment>
        )
    }
}
export default withTranslation()(CheckBoxMemeberList);