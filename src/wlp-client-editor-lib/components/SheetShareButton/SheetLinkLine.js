import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import GetMapConfigDto from 'wlp-client-service/dto/map/GetMapConfigDto';
import MapService from 'wlp-client-service/service/MapService';
import GetMapRecordDetailDto from 'wlp-client-service/dto/map/GetMapRecordDetailDto';
import CheckBoxMemeberList from './CheckboxMemberList';
import SendNotificationConfigDto from 'wlp-client-service/dto/sheet/SendNotificationConfigDto';
import SheetService from 'wlp-client-service/service/SheetService';
import EditorConsts from "../../../wlp-client-common/consts/EditorConsts";
import { CONSTANTS } from 'wlp-client-editor-lib/utils/Constants';
import { chunkingArray } from 'wlp-client-common/utils/ArrayUtil';
import ActionLogger from 'wlp-client-common/ActionLogger';
const mapService = new MapService();
const sheetService = new SheetService();
class SheetLinkView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedSharedUsersList: [],
            messageArea: '',
            disableButtonSendMessage: false,
            messageResultDisapear: false,
            textAreaFocusFlag: false,
        }
        this.listMemberSrollTop = React.createRef();
        this.messageAreaRef = React.createRef();
    }

    callGetSelectedMapRecordDetail = async () => {
        const { currentSheet, mapInfo } = this.props;
        if (currentSheet && mapInfo) {
            let requestDto = new GetMapConfigDto();
            requestDto.mapId = currentSheet.parentId;
            requestDto.sheetId = currentSheet.id;
            requestDto.contractId = mapInfo.contractId;

            const listSharedUsers = await mapService.getSharedUsers(requestDto);
            this.callGetMapRecordDetail_handlResult(listSharedUsers);
        }
    }

    callGetMapRecordDetail_handlResult = (listSharedUsers = new GetMapRecordDetailDto()) => {
        this.setState({
            selectedSharedUsersList: listSharedUsers.users
        });
    }

    componentDidUpdate(preProps) {
        const { currentSheet } = this.props;
        if (preProps.currentSheet && currentSheet) {
            if (preProps.currentSheet.parentId !== currentSheet.parentId) {
                this.callGetSelectedMapRecordDetail();
            }
        }
    }

    sendMessageNotify = () => {
        ActionLogger.click('User_Editor_SheetLinkView', 'Send_Mail_Share_Link_Button');
        if (this.state.selectedSharedUsersList) {
            let notifyUser = this.getSelectedNotifyUser();
            if (notifyUser.length > 0) {
                this.sendEmailToSelectedMember(notifyUser);
            }
        }
    }

    getSelectedNotifyUser = () => {
        let listSharedUsersList = this.state.selectedSharedUsersList;
        let notifyUser = [];
        listSharedUsersList.forEach(user => {
            if (user.checked === true) {
                let userData = { ...user };
                delete userData.checked;
                notifyUser.push(userData);
            }
        });
        return notifyUser;
    }

    sendEmailToSelectedMember = async (selectedUsers) => {
        const { currentSheet } = this.props;
        let arrayItems = chunkingArray(selectedUsers);
        let sendMessageNotifyResult;
        let requestDto = new SendNotificationConfigDto();
        requestDto.message = this.state.messageArea;
        requestDto.sheetId = `${currentSheet.id}`;

        for (let i = 0; i < arrayItems.length; i++) {
            requestDto.users = arrayItems[i];
            sendMessageNotifyResult = await sheetService.sendNotification(requestDto);
        }
        
        this.handleDisplayResponseMessage(sendMessageNotifyResult);
    }


    handleChangeMessageArea = (message) => {
        ActionLogger.click('User_Editor_SheetLinkView', 'Change_Message_Share_Message_TextArea');
        this.setState({
            messageArea: message
        });
    }

    handleDisplayResponseMessage = (sendMessageNotifyResult) => {
        if (sendMessageNotifyResult.isSucceed === true) {
            setTimeout(() => {
                this.setState({
                    sendMessageResult: true,
                    messageResultDisapear: true
                })
            }, 800)
            this.disapearMessageResult();
        }
    }
    disapearMessageResult() {
        setTimeout(() => {
            this.setState({
                messageResultDisapear: false,
                messageArea: ''
            })
        }, 1000)
        setTimeout(() => {
            const { selectedSharedUsersList } = this.state;
            const messageArea = this.messageAreaRef.current;
            selectedSharedUsersList.forEach(user => {
                user.checked = true;
            });

            this.setState({
                sendMessageResult: false,
                selectedSharedUsersList: selectedSharedUsersList
            });
            this.listMemberSrollTop.current.scrollTop = 0;
            messageArea.classList.remove('form-control-valid');
        }, 2500)
    }

    handleButtonClearClicked = () => {
        ActionLogger.click('User_Editor_SheetLinkView', 'Clear_Popup_Share_Link_Button');
        const { selectedSharedUsersList } = this.state;
        const messageArea = this.messageAreaRef.current;
        selectedSharedUsersList.forEach(user => {
            user.checked = false;
        });
        this.setState({
            messageArea: '',
            disableButtonSendMessage: true,
            selectedSharedUsersList: selectedSharedUsersList
        });
        messageArea.classList.remove('form-control-valid');
        this.listMemberSrollTop.current.scrollTop = 0;
    }

    handleDisableSendMessageButton = (status) => {
        this.setState({
            disableButtonSendMessage: status
        });
    }

    updateUserSelected = (index, checked) => {
        let listSharedUsersList = this.state.selectedSharedUsersList;
        listSharedUsersList[index].checked = checked;
        const numberSelectUser = listSharedUsersList.filter(user => user.checked === true);
        this.setState({
            selectedSharedUsersList: listSharedUsersList,
            disableButtonSendMessage: !numberSelectUser.length > 0
        });
    }

    handleBlurMessageArea = (e) => {
        const { t } = this.props;
        if (e) {
            e.target.placeholder = t('editor.link.messagePrompt')
            const event = e.target;
            if (event.value) {
                event.classList.add('form-control-valid');
            }
            else {
                event.classList.remove('form-control-valid');
            }
            event.classList.add('noselect');
        }
        this.setState({
            textAreaFocusFlag: false
        })
    }

    handleFocusMessageArea = (e) => {
        e.target.placeholder = '';
        e.target.classList.remove('form-control-valid');
        e.target.select();
        this.setState({
            textAreaFocusFlag: true
        })
    }
    
    handleCheckLimitMessageLength = (e) => {
        const message = e.target.value;
        if (message.length > EditorConsts.SHARE_TEXTAREA_MAXIMUM_CHARACTER) {
            let messageLimited = message.substring(0, EditorConsts.SHARE_TEXTAREA_MAXIMUM_CHARACTER);
            e.target.value = messageLimited;
        }
    }
    render() {
        const { t, isShow, top } = this.props;
        const { textAreaFocusFlag } = this.state;
        const shareUserList = this.state.selectedSharedUsersList.length;
        let textAreaClasses = textAreaFocusFlag ? 'form-control form-control-shadow form-control-editor form-control-shadow-editor placeholder-italic placeholder-italic-editor bg-transparent' : 'form-control form-control-shadow form-control-editor form-control-shadow-editor placeholder-italic placeholder-italic-editor bg-transparent noselect'
        return (
            <div className={"dropdown-menu dropdown-menu-editor dropdown-menu-right dropdown-menu-bg-black dropdown-menu-bg-black-editor dropdown-share dropdown-share-editor" + (isShow ? " d-block" : " d-none")}
                aria-labelledby="dropdownMenuButton"
                style={{
                    position: 'absolute',
                    top: top,
                }}>
                <h3 className="tlt-dropdown tlt-dropdown-editor noselect">{t('editor.link.sendSheetUrl')}</h3>
                <div className="card card-editor card-member-share card-member-share-editor">
                    {this.state.selectedSharedUsersList && (
                        <div className={"list-member-share list-member-share-editor" + (shareUserList > CONSTANTS.LIMIT_DISPLAY_USER ? " list-member-share-scroll list-member-share-scroll-editor" : "")} ref={this.listMemberSrollTop}>
                            {this.state.selectedSharedUsersList.map((item, index) => {
                                if (typeof item.checked === 'undefined') {
                                    item.checked = true;
                                }
                                return <CheckBoxMemeberList key={index}
                                    item={item}
                                    index={index}
                                    updateUserSelected={this.updateUserSelected}
                                />
                            })}
                        </div>
                    )}

                    <div className={"form-share form-share-editor" + (this.state.sendMessageResult === true ? " d-none" : "")}>
                        <textarea name=""
                            maxLength={EditorConsts.SHARE_TEXTAREA_MAXIMUM_CHARACTER}
                            className={textAreaClasses}
                            placeholder={t('editor.link.messagePrompt')}
                            onFocus={(e) => this.handleFocusMessageArea(e)}
                            onBlur={(e) => this.handleBlurMessageArea(e)}
                            onChange={(e) => this.handleChangeMessageArea(e.target.value)} value={this.state.messageArea}
                            ref={this.messageAreaRef}
                            onClick={(e) => this.handleCheckLimitMessageLength(e)}
                        />

                        <div className="group-btn-share group-btn-share-editor">
                            <button onClick={() => this.handleButtonClearClicked()} className="btn btn-editor btn-whitefl btn-whitefl-editor noselect">{t('editor.link.clear')}</button>
                            <button onClick={() => this.sendMessageNotify()}
                                className={"btn btn-editor btn-black btn-black-editor noselect"
                                    + (this.state.disableButtonSendMessage === true ? " btn-disabled btn-disabled-editor" : "")}>{t('editor.link.send')}</button>
                        </div>
                    </div>
                    <div className={"alert-share alert-share-editor" + (this.state.sendMessageResult === true ? " d-block" : "")}>
                        <div className={"alert-share-ct alert-share-ct-editor" + (this.state.messageResultDisapear === true ? " fade-eff fade-eff-editor" : "")}>
                            <strong>{t('editor.link.complete')}</strong>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTranslation()(SheetLinkView);