import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import EditorConsts from 'wlp-client-common/consts/EditorConsts';
import propTypes from 'prop-types';
import InputUtils from 'wlp-client-common/utils/InputUtils';
import SheetNameConst from 'wlp-client-common/consts/SheetNameConst';
import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";
import MainSkinContext from 'wlp-client-editor/MainSkinContext';
import KEYCODE from '../../utils/KeyCodeConst';
import SheetEventCenter from 'wlp-client-editor-lib/events/SheetEventCenter';

const inputUtils = new InputUtils();
const localStorage = new LocalStorageUtils();
const sheetEventCenter = new SheetEventCenter();

class SheetTabListItem extends Component {
    static contextType = MainSkinContext;
    static defaultProps = {
        showLine: false,
        showLineRight: false
    };
    constructor(props) {
        super(props);
        const { sheet } = this.props;
        const { name } = sheet;

        this.state = {
            showInputSheetName: false,
            isDragOverLeft: false,
            showToolTip: false,
            sheetName: name
        };
    }

    tabBox = React.createRef();

    

    handleOnChange = (event) => {
        const e = event;
        this.handleSetInputName(e, false);
    }

    handleOnPaste = (event) => {
        // Get pasted data via clipboard API
        event.clipboardData.getData('text/plain').slice(0, EditorConsts.SHEET_NAME_MAXIMUM_CHARACTER);
    }

    handleKeyDown = (event) => {
        const e = event;
        if (e.keyCode === KEYCODE.ENTER) {
            this.handleSetInputName(e, true);
        }
    }

    handleSetInputName = (event, isEnter) => {
        const regex = SheetNameConst.SHEET_NAME_RESTRICT;
        const stateSheetName = this.state.sheetName;
        const isEnterPress = isEnter;

        const { selectionEnd } = event.currentTarget;
        const valSheetName = event.target.value;

        const isCharValid = valSheetName === '' || regex.test(valSheetName);

        if (valSheetName.length > 200) {
            this.setState(
                {
                    sheetName: stateSheetName
                }, () => {
                    inputUtils.setCaretPosition(this.inputSheetName, selectionEnd);
                }
            );
        } else {
            if (!isCharValid) {
                this.setStateAndCursorPos(stateSheetName, isCharValid, selectionEnd, isEnterPress);
            } else {
                this.setStateAndCursorPos(valSheetName, isCharValid, selectionEnd, isEnterPress);
            }
        }
    }

    setStateAndCursorPos = (value, isCharValid, selectionEnd, isEnterPress) => {
        this.setState(
            {
                sheetName: value
            }, () => {
                let cursorPos = value.length;

                if (isCharValid) {
                    cursorPos = selectionEnd;
                } else {
                    cursorPos = selectionEnd - 1;
                }

                if (isEnterPress) {
                    this.handleUpdateSheetName(value);
                } else {
                    inputUtils.setCaretPosition(this.inputSheetName, cursorPos);
                }
            }
        );
    }

    handleBlur = (event) => {
        const val = event.target.value;

        this.handleUpdateSheetName(val);
    }

    handleUpdateSheetName = (newSheetName) => {
        const { updateSheet, sheet } = this.props;
        sheet.name = newSheetName;
        updateSheet(sheet);
        this.setState({
            showInputSheetName: false
        })
    }

    onDragOverTabLeft = () => {
        this.setState({
            isDragOverLeft: true
        })
    }
    onDragOverTabRight = () => {
        this.setState({
            isDragOverLeft: false
        })
    }

    componentDidUpdate(oldProps, oldStates) {
        const { showToolTip } = this.state;
        const { sheet } = this.props;
        const tabBoxWidth = this.tabBox.current.offsetWidth;
        const tabBoxScrollWidth = this.tabBox.current.scrollWidth;
        if (tabBoxWidth < tabBoxScrollWidth && !showToolTip) {
            this.setState({showToolTip: true});
        }
        if (tabBoxWidth >= tabBoxScrollWidth && showToolTip) {
            this.setState({showToolTip: false});
        }
        if(sheet && oldProps.sheet){
            if(sheet.name !== oldProps.sheet.name){
                this.setState({
                    sheetName: sheet.name
                })
            }
        }
    }

    onDragOverTabLeft = () => {
        this.setState({
            isDragOverLeft: true
        })
    }
    onDragOverTabRight = () => {
        this.setState({
            isDragOverLeft: false
        })
    }

    onClickHandler = (e) => {
        sheetEventCenter.fireEvent(sheetEventCenter.STOP_EDITING)
        if(!e.target.closest('.tab-sheet').className.includes('active')) {
            sheetEventCenter.fireEvent(sheetEventCenter.REMOVE_ALL_SELECTION);
        }
        const { switchSheet, sheet } = this.props;
        if (typeof switchSheet === 'function') {
            switchSheet(sheet.id);
        }
    }

    onMouseDownHandler = (e) => {
        this.onClickHandler(e)
    }

    onDoubleClickHandler = (e) => {
        const { isActive, sheet } = this.props;
        const { lockFlag } = sheet;

        if (!lockFlag && isActive) {
            if (!this.state.showInputSheetName) {
                this.setState({
                    showInputSheetName: true
                }, () => {
                    this.inputSheetName.focus();
                    this.inputSheetName.select();
                });
            }
        }
        this.props.handleChangeSheetNameCurrent();
    }

    render() {
        const {
            sheet,
            isActive,
            onDragEnd,
            onDragOver,
            onDragStart,
            onTouchStart,
            onTouchMove,
            onTouchEnd,
            sheetLockButtonVisible,
            index
        } = this.props;
        const { showToolTip } = this.state;
        const {
            name,
            lockFlag,
            backgroundUri,
            lastEditTime
        } = sheet;

        const classNames = ['tab-sheet'];

        const lastLogoutTime = localStorage.getUserSession() !== null ? localStorage.getUserSession().sessionDto.lastLogoutTime : null;

        if (lockFlag && sheetLockButtonVisible) {
            classNames.push('locked-active');
        }

        if (isActive) {
            classNames.push('active active-editor');
        }

        if (parseInt(backgroundUri) === EditorConsts.SHEET_BG_GREY) {
            if (classNames.includes("active active-editor")) {
                classNames.push('tab-sheet-bg3');
            } else {
                classNames.push('tab-sheet-bg2 tab-sheet-bg2-editor');
            }
        }

        let isDraggable = this.state.showInputSheetName ? false : true;
        isDraggable = isActive;

        return (<Fragment>
            <div
                className={classNames.join(' ')}
                
                onClick={this.onClickHandler}
                onDoubleClick={this.onDoubleClickHandler}
                onMouseDown={this.onMouseDownHandler}
                data-index={index}
                draggable={isDraggable}
                onDragEnd={onDragEnd}
                onDragOver={onDragOver}
                onDragStart={onDragStart}
                onTouchStart={onTouchStart}
                onTouchMove={onTouchMove}
                onTouchEnd={onTouchEnd}
            >
                <div className="tab-box tab-box-editor" >
                    <div ref={this.tabBox} title={showToolTip ? name : ""} className={"tab-box-tlt tab-box-tlt-editor text-truncate text-truncate-editor noselect" + (lastLogoutTime < lastEditTime ? " text-shadow-green text-shadow-green-editor" : "")}>{name}</div>
                    <div className="tab-box-locked tab-box-locked-editor"></div>
                    <div className={"tab-box-rename tab-box-rename-editor" + (this.state.showInputSheetName ? " d-block" : "")}>
                        <input
                            disabled={!this.state.showInputSheetName}
                            type="text"
                            placeholder={this.state.sheetName}
                            value={this.state.sheetName}
                            className="tab-box-inp tab-box-inp-editor"
                            maxLength={EditorConsts.SHEET_NAME_MAXIMUM_CHARACTER}
                            ref={(input) => { this.inputSheetName = input; }}
                            onChange={this.handleOnChange}
                            onPaste={this.handleOnPaste}
                            onBlur={this.handleBlur}
                            onKeyDown={this.handleKeyDown}
                        />
                    </div>

                </div>
            </div>
        </Fragment>);
    }

}

SheetTabListItem.propTypes = {
    sheet: propTypes.object,
    isActive: propTypes.bool,
    switchSheet: propTypes.func
};

export default withTranslation()(SheetTabListItem);