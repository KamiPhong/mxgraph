import React, { Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';

import slide1 from 'wlp-client-common/images/slider/slider-img-1.jpg';
import slide2 from 'wlp-client-common/images/slider/slider-img-2.jpg';
import slide3 from 'wlp-client-common/images/slider/slider-img-3.jpg';

import slideJa1 from 'wlp-client-common/images/slider/slider-img-1-jp.jpg';
import slideJa2 from 'wlp-client-common/images/slider/slider-img-2-jp.jpg';
import slideJa3 from 'wlp-client-common/images/slider/slider-img-3-jp.jpg';

import btnPrev from 'wlp-client-common/images/icon/icon-btn-prev.png';
import btnNext from 'wlp-client-common/images/icon/icon-btn-next.png';
import iconClose from 'wlp-client-common/images/icon/icon-close-wt.png';
import Link from 'wlp-client-common/component/Link';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import i18n from "i18n";
import ActionLogger from 'wlp-client-common/ActionLogger';

const tourView = {
    'en_US': [
        slide1, slide2, slide3
    ],
    'ja_JP': [
        slideJa1, slideJa2, slideJa3
    ]
};
const strageUtils = new LocalStorageUtils();
const localeCode = i18n.language;
const tourViewByLanguage = (localeCode === LocaleConst.en_US) ? tourView['en_US'] : tourView['ja_JP'];

class EditorGuideDisplay extends React.Component {

    state = {
        items: tourViewByLanguage,
        index: 0, 
        isNext: true,
        isBtnDisable: {btnNext: false, btnPrev: true},
        isChecked: false,
        isReachLastSlide: true
    }

    // handle event click btn prev
    handlerPrev = () => {
        ActionLogger.click('User_Editor_GuideDisplay', 'Previous_Button');
        let index = this.state.index;
        this.setState({isReachLastSlide: false});

        if ( index < 1 ) { 
            this.setStateGuideDisplay(index, true, false, true);
        } else {
            index = index - 1;
            this.setStateGuideDisplay(index, false, false, false);
            if (index === 0) {
                this.setState({isBtnDisable: {btnPrev: true}});
            }
        }
    }
    
    // handle event click btn next
    handlerNext = () => {
        ActionLogger.click('User_Editor_GuideDisplay', 'Next_Button');
        let index = this.state.index;
        let length = this.state.items.length - 1;
        if (index === length-1) {
            this.setState({isReachLastSlide: false});
        }
        
        if ( index === length ) { 
            this.setStateGuideDisplay(index, false, true, false);
        } else {
            index = index + 1;
            this.setStateGuideDisplay(index, true, false, false);
            if (index === length) {
                this.setState({isBtnDisable: {btnNext: true}});
            }
        }
    }

    // set state events on page
    setStateGuideDisplay = (index, isNext, statusBtnNext, statusBtnPrev) => {
        this.setState({ 
            index: index, 
            isNext: isNext, 
            isBtnDisable: {
                btnNext: statusBtnNext, 
                btnPrev: statusBtnPrev
            }
        });
    }

    // set state events show guide display
    setStatusShowGuideDisplay = (status) => {
        this.setState({ isChecked: status }, () => {
            const { isChecked } = this.state;
            strageUtils.setLocalStrage(strageUtils.SKIP_TOUR_EDITOR, isChecked);
        });
    }

    // handle event checkbox 
    checkBoxChangeHandler = (e) => {
        ActionLogger.click('User_Editor_GuideDisplay', 'DontShowAgain_Checkbox');
        if (e.target.checked) {
            this.setStatusShowGuideDisplay(true);
        } else {
            this.setStatusShowGuideDisplay(false);
        }
    }

    render() {
        const { isBtnDisable, index, items, isReachLastSlide } = this.state;
        const {tourViewCloseHandler, eventsLetsStartUsingSystem, t } = this.props;
        const src = items[index];
        return (
            <Fragment>
                <div className="w-slide-guidle w-slide-guidle-editor">
                    <div className="slide-guidle-content slide-guidle-content-editor box-shadow-slider box-shadow-slider-editor">
                        <div id="carouselExampleControls" className="carousel slide carousel-fade" data-ride="carousel"
                            data-interval="false">
                            <div className="carousel-inner">
                                <div className="carousel-item carousel-item-editor active active-editor">
                                    {
                                        (index === 4) && src && (
                                            <div className="w-btn-now w-btn-now-editor position-absolute">
                                                <Link href="#" onClick={eventsLetsStartUsingSystem} className="btn btn-editor btn-dark btn-dark-editor box-shadow">{t('mypage.slider.button.now')}</Link>
                                            </div>
                                        )
                                    }
                                    <img src={src} alt="" className="noselect"/>
                                </div>
                            </div>
                        </div>

                        <div className="w-slider-prev w-slider-prev-editor w-btn-slider w-btn-slider-editor">
                            <Link onClick={this.handlerPrev} disabled={isBtnDisable.btnPrev}
                                className={`btn-slider btn-slider-editor btn-slider-prev btn-slider-prev-editor ${isBtnDisable.btnPrev ? 'btn-slider-disabled btn-slider-disabled-editor' : ''}`}>
                                    <img src={btnPrev} alt="" width={12} className="noselect" />
                            </Link>
                            <span className={`label-slider label-slider-editor noselect ${isBtnDisable.btnPrev ? 'label-slider-disabled label-slider-disabled-editor' : ''}`}>{t('mypage.slider.previous')}</span>
                        </div>
                        <div className="w-slider-next w-slider-next-editor w-btn-slider w-btn-slider-editor">
                            <Link onClick={this.handlerNext} disabled={isBtnDisable.btnNext}
                                className={`btn-slider btn-slider-editor btn-slider-next btn-slider-next-editor ${isBtnDisable.btnNext ? 'btn-slider-disabled btn-slider-disabled-editor' : ''} 
                                                                        ${isReachLastSlide ? 'btn-slider-effect btn-slider-effect-editor' : ''}`}>
                                    <img src={btnNext} alt="" width={12} className="noselect" />
                            </Link>
                            <span className={`label-slider label-slider-editor noselect ${isBtnDisable.btnNext ? 'label-slider-disabled label-slider-disabled-editor' : ''}`}>{t('mypage.slider.next')}</span>
                        </div>


                        <div className="slider-checkct slider-checkct-editor">
                            <label className="checkbox-cus checkbox-cus-editor checkbox-cus-text checkbox-cus-text-editor">
                                <input type="checkbox" checked={this.state.isChecked} onChange={this.checkBoxChangeHandler} className="checkbox-cus-inp checkbox-cus-inp-editor"/>
                                <span className="checkbox-cus-checkmark checkbox-cus-checkmark-editor checkbox-cus-checkmark-lg checkbox-cus-checkmark-lg-editor"></span>
                                <div className="checkbox-cus-txt checkbox-cus-txt-editor noselect">{t('editor.tour.skipTour')}</div>
                            </label>
                        </div>
                        <div className="slider-close slider-close-editor">
                            <Link onClick={() => {
                                ActionLogger.click('User_Editor_GuideDisplay', 'Button_Close');
                                tourViewCloseHandler();
                            }} className="btn-slider-close btn-slider-close-editor d-block"><img src={iconClose} alt="" width={32} /></Link> 
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default withTranslation()(EditorGuideDisplay);

