import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import PulldownButton from '../PulldownButton';
import Link from 'wlp-client-common/component/Link';
import DeleteSheetModal from "../modals/DeleteSheetModal";
import { withMapContainerContext } from '../MapContainerContext';
import SheetPullDownListEventCenter from 'wlp-client-editor-lib/events/SheetPullDownListEventCenter';
import SheetViewEventCenter from "../../events/SheetViewEventCenter";
import ActionLogger from 'wlp-client-common/ActionLogger';

const sheetPullDownListEventCenter = new SheetPullDownListEventCenter();
const sheetViewEventCenter = new SheetViewEventCenter();
class SheetPullDownList extends Component {
    state = {
        isSheetDeleteOpen: false,
        isSheetPropertyCheck: false,
        baseItemLockCheckIconEnable: true,
        savePngBySelectedEnable: false
        
    }
    componentDidMount() {
        sheetViewEventCenter.addListener(sheetViewEventCenter.SHEET_PROPERTY_OPENED, this.showCheckMarkSheetProperty);
        sheetViewEventCenter.addListener(sheetViewEventCenter.SHEET_PROPERTY_CLOSED, this.hideCheckMarkSheetProperty);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_LOCK_BASE_ITEM_ENABLE, this.setBaseItemLockCheckIconEnable);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG_BY_SELECTED_ENABLE, this.setSavePngBySelectedEnable);
    }

    sheetCreate = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Create_Sheet');
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_ADD_SHEET);
    }

    sheetCopy = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Copy_Sheet');
        if(!this.props.isSheetLocked){
            sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_COPY_SHEET);
        }
    }

    saveAsPng = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Save_As_Png');
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG);
    }

    toggleSavePngBySelectedFlag = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Save_Png_By_Selected');
        const savePngBySelectedEnable = !this.state.savePngBySelectedEnable;
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_CHANGE_SAVE_PNG_BY_SELECTED_FLAG, savePngBySelectedEnable);
        this.setSavePngBySelectedEnable(savePngBySelectedEnable)
    }

    saveAsCsv = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Save_As_Csv');
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_CSV);
    }

    sheetDelete = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Sheet_Delete');
        if(this.props.sheetDeleteAvailable && !this.props.isSheetLocked){
            this.setState({isSheetDeleteOpen: true});
        }
    }

    sheetLock = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Sheet_Lock');
        if(this.props.sheetLockButtonVisible){
            sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_LOCK_SHEET);
        }
    }

    sheetLockBaseItem = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Sheet_Lock_Base_Item');
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_LOCK_BASE_ITEM);
    }

    showSheetProperty = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_Sheet_Properties');
        sheetPullDownListEventCenter.fireEvent(sheetPullDownListEventCenter.SHEET_PULLDOWN_SHOW_PROPERTY);
    };

    hideDeleteSheetModal = () => {
        this.setState({isSheetDeleteOpen: false});
    }
    
    showCheckMarkSheetProperty = () => {
        this.setState({isSheetPropertyCheck: true});
    }
    
    setBaseItemLockCheckIconEnable = (isEnable) => {
        this.setState({
            baseItemLockCheckIconEnable: isEnable
        })
    }

    setSavePngBySelectedEnable = (isEnable) => {
        this.setState({
            savePngBySelectedEnable: isEnable
        })
    }
    
    hideCheckMarkSheetProperty = () => {
        this.setState({isSheetPropertyCheck: false});
    }
    
    renderDeleteSheetModel = () => {
        const { currentSheet } = this.props.mapContainerContext;
        return (
            this.state.isSheetDeleteOpen &&
            <DeleteSheetModal
                currentSheet={currentSheet}
                cancelHandler={this.hideDeleteSheetModal}
                isShow={this.state.isSheetDeleteOpen} />
        );
    };
    
    renderWorksheetItems = () => {
        const { 
            t, 
            sheetDeleteAvailable,
            sheetLockButtonVisible,
            isSheetLocked
        } = this.props;
        
        const checkMark = 'dropdown-item-check dropdown-item-check-editor';
        const disabledClass = 'dropdown-item-disabled dropdown-item-disabled-editor noselect';
        let lockSheetClasses = ['dropdown-item dropdown-item-editor dropdown-border-b dropdown-border-b-editor noselect'];
        let lockBaseItemClasses = ['dropdown-item dropdown-item-editor dropdown-item-h dropdown-border-b dropdown-border-b-editor noselect'];
        let showSheetPropertyClasses = ['dropdown-item dropdown-item-editor noselect'];
        let sheetDeleteClasses = ['dropdown-item dropdown-item-editor noselect'];
        let sheetCopyClasses = ['dropdown-item dropdown-item-editor noselect'];
        let sheetSavePngBySelected = ['dropdown-item dropdown-item-editor noselect'];
        if(this.state.isSheetPropertyCheck){
            showSheetPropertyClasses.push(checkMark);
        }
        if (isSheetLocked) {
            lockSheetClasses.push(checkMark);
            sheetDeleteClasses.push(disabledClass);
            sheetCopyClasses.push(disabledClass)
        }

        if(!sheetLockButtonVisible){
            lockSheetClasses.push(disabledClass);
        }

        if(!sheetDeleteAvailable){
            sheetDeleteClasses.push(disabledClass);
        }
        if (this.state.baseItemLockCheckIconEnable) {
            lockBaseItemClasses.push("dropdown-item-check dropdown-item-check-editor")
        }
        if (this.state.savePngBySelectedEnable) {
            sheetSavePngBySelected.push("dropdown-item-check dropdown-item-check-editor")
        }

        const isDisableDeleteBtn = sheetDeleteClasses.indexOf(disabledClass) !== -1;
        const isDisableLockBtn = lockSheetClasses.indexOf(disabledClass) !== -1;
        const isDisableCopyBtn = sheetCopyClasses.indexOf(disabledClass) !== -1;
        return (
            <React.Fragment>
                <Link className="dropdown-item dropdown-item-editor noselect" onClick={this.sheetCreate}><span>{t('editor.header.sheet.create')}</span></Link>
                <Link className={sheetCopyClasses.join(' ')} disabled={isDisableCopyBtn} onClick={this.sheetCopy}><span>{t('editor.header.sheet.copy')}</span></Link>
                <Link className="dropdown-item dropdown-item-editor noselect" onClick={this.saveAsPng}><span>{t('editor.header.sheet.png')}</span></Link>
                <Link className={sheetSavePngBySelected.join(' ')} onClick={this.toggleSavePngBySelectedFlag}><span>{t('editor.header.sheet.pngbyselected')}</span></Link>
                <Link className="dropdown-item dropdown-item-editor noselect" onClick={this.saveAsCsv}><span>{t('editor.header.sheet.csv')}</span></Link>
                <Link className={sheetDeleteClasses.join(' ')} disabled={isDisableDeleteBtn} onClick={this.sheetDelete}><span>{t('editor.header.sheet.remove')}</span></Link>
                <Link className={lockSheetClasses.join(' ')} disabled={isDisableLockBtn} onClick={this.sheetLock}>{t('editor.header.sheet.lock')}</Link>
                <Link className={lockBaseItemClasses.join(' ')} onClick={this.sheetLockBaseItem}>
                    <span> {t('editor.header.sheet.lockBaseItem')}</span>
                </Link>
                <Link className={showSheetPropertyClasses.join(' ')} onClick={this.showSheetProperty}><span>{t('editor.header.sheet.property')}</span></Link>

            </React.Fragment>
        );
    }

    render() {
        const { t } = this.props;

        return <React.Fragment>
            <div onClick={() => {
                    ActionLogger.click('User_Editor_Header', 'Sheet_Menu');
                }}>
                {this.renderDeleteSheetModel()}
                <PulldownButton title={t('editor.header.sheet.label')}
                    classDropDown="dropdown-header-1"
                    classDropDownMenu="dropdown-pl-35 dropdown-pl-35-editor"
                    pullDownItems={this.renderWorksheetItems()} />
            </div>
        </React.Fragment>
    }
}

export default withTranslation()(withMapContainerContext(SheetPullDownList));