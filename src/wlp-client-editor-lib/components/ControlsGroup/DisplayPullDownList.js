import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import PulldownButton from '../PulldownButton';
import Link from 'wlp-client-common/component/Link';
import DisplayPulldownListEventCenter from 'wlp-client-editor-lib/events/DisplayPulldownListEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';

const displayPulldownEventCenter = new DisplayPulldownListEventCenter();

class DisplayPullDownList extends Component {
    state = {
        isForcedVisibleNewItemFiltersBy2D: false,
        isForcedVisibleNewItemFiltersBy6D: false,
        menuDisplayGoodShowEnable: false,
        menuPrintingRangeGuidEnable: false
        
    }

    componentDidMount() {
        displayPulldownEventCenter.addListener(displayPulldownEventCenter.TOOGLE_DISPLAY_PULLDOWN_SHOW_GOODS_FLAG, this.toogleGoodshow);
        displayPulldownEventCenter.addListener(displayPulldownEventCenter.TOOGLE_PULLDOWN_SHOW_PRINTING_RANGE_GUIDE, this.tooglePrintingRangeGuide);
    }

    toogleGoodshow = (isToogleState = true) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Toggle_Good');
        let goodShowEnable = isToogleState ? !this.state.menuDisplayGoodShowEnable : this.state.menuDisplayGoodShowEnable;
        displayPulldownEventCenter.fireEvent(displayPulldownEventCenter.DISPLAY_PULLDOWN_SHOW_GOODS, goodShowEnable);
        this.setMenuDisplayGoodShowEnable(goodShowEnable)
    }

    setMenuDisplayGoodShowEnable = (flag) => {
        this.setState({
            menuDisplayGoodShowEnable: flag
        })
    }

    forcedvisiblenewitemBefore2D = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_HighLight_Last_2_Day');
        const { isForcedVisibleNewItemFiltersBy2D } = this.state;
        this.setState({
            isForcedVisibleNewItemFiltersBy2D: !isForcedVisibleNewItemFiltersBy2D,
            isForcedVisibleNewItemFiltersBy6D:false,
        },()=>{
            displayPulldownEventCenter.fireEvent(displayPulldownEventCenter.DISPLAY_PULLDOWN_HIGHLIGHTS_NEW_ITEM_BEFORE_2D, this.state.isForcedVisibleNewItemFiltersBy2D);
        })
        
    }

    forcedvisiblenewitemBefore6D = () => {
        ActionLogger.click('User_Editor_Header', 'Menu_HighLight_Last_6_Day');
        const { isForcedVisibleNewItemFiltersBy6D } = this.state;
        this.setState({
            isForcedVisibleNewItemFiltersBy2D: false,
            isForcedVisibleNewItemFiltersBy6D: !isForcedVisibleNewItemFiltersBy6D
        },()=>{
            displayPulldownEventCenter.fireEvent(displayPulldownEventCenter.DISPLAY_PULLDOWN_HIGHLIGHTS_NEW_ITEM_BEFORE_6D, this.state.isForcedVisibleNewItemFiltersBy6D);
        })
    }

    tooglePrintingRangeGuide = (isToogleState = true) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Toggle_Printing_Range');
        let menuPrintingRangeGuidEnable = isToogleState ? !this.state.menuPrintingRangeGuidEnable : this.state.menuPrintingRangeGuidEnable;
        this.setMenuPrintingRangeGuideEnable(menuPrintingRangeGuidEnable)
        displayPulldownEventCenter.fireEvent(displayPulldownEventCenter.DISPLAY_PULLDOWN_SHOW_PRINTING_RANGE_GUIDE, menuPrintingRangeGuidEnable);
    }

    setMenuPrintingRangeGuideEnable = (flag) => {
        this.setState({
            menuPrintingRangeGuidEnable: flag
        })
    }
    
    renderDisplayItems = () => {
        const { t } = this.props;
        const{ isForcedVisibleNewItemFiltersBy2D, isForcedVisibleNewItemFiltersBy6D, menuDisplayGoodShowEnable, menuPrintingRangeGuidEnable} = this.state;

        const menuClass = 'dropdown-item dropdown-item-editor noselect';
        let forcedvisiblenewitemBefore2DMenuClass = menuClass;
        let forcedvisiblenewitemBefore6DMenuClass = menuClass;
        let visibleMenuDisplayGoodShow = menuClass;
        let visibleMenuPrintingRangeGuide = menuClass;

        if ( isForcedVisibleNewItemFiltersBy2D ) {
            forcedvisiblenewitemBefore2DMenuClass += ' dropdown-item-check dropdown-item-check-editor';
        }

        if ( isForcedVisibleNewItemFiltersBy6D ) {
            forcedvisiblenewitemBefore6DMenuClass += ' dropdown-item-check dropdown-item-check-editor';
        }

        if (menuDisplayGoodShowEnable) {
            visibleMenuDisplayGoodShow += ' dropdown-item-check dropdown-item-check-editor';
        }

        if (menuPrintingRangeGuidEnable) {
            visibleMenuPrintingRangeGuide += ' dropdown-item-check dropdown-item-check-editor';
        }

        return (<React.Fragment>
            <Link className={visibleMenuDisplayGoodShow} onClick={this.toogleGoodshow}>{t('editor.header.display.goodshow')}</Link>
            <Link className={forcedvisiblenewitemBefore2DMenuClass} onClick={this.forcedvisiblenewitemBefore2D}>{t('editor.header.display.forcedvisiblenewitemBefore2D')}</Link>
            <Link className={forcedvisiblenewitemBefore6DMenuClass} onClick={this.forcedvisiblenewitemBefore6D}>{t('editor.header.display.forcedvisiblenewitemBefore6D')}</Link>
            <Link className={visibleMenuPrintingRangeGuide} onClick={this.tooglePrintingRangeGuide}>{t('editor.header.display.showPrintingRangeGuide')}</Link>
        </React.Fragment>)
    }

    render() {
        const { t } = this.props;

        return (
            <div onClick={() => {
                    ActionLogger.click('User_Editor_Header', 'Display_Menu');
                }}>
                <PulldownButton
                title={t('editor.header.display.label')}
                classDropDown="dropdown-header-3"
                classDropDownMenu="dropdown-pl-35 dropdown-pl-35-editor"
                pullDownItems={this.renderDisplayItems()} />
            </div>
        );
    }
}

export default withTranslation()(DisplayPullDownList);