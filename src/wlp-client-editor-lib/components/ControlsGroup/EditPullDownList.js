import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import PulldownButton from '../PulldownButton';
import Link from 'wlp-client-common/component/Link';
import { withMapContainerContext } from '../MapContainerContext';
import EditPullDownListEventCenter from 'wlp-client-editor-lib/events/EditPullDownListEventCenter';
import SheetPullDownListEventCenter from 'wlp-client-editor-lib/events/SheetPullDownListEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';
const editPulldownListEventCenter = new EditPullDownListEventCenter();
const sheetPullDownListEventCenter = new SheetPullDownListEventCenter();
class EditPullDownList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasItemSelected:false,
            pasteItemEnable:false,
            isSavePngBySelectedEnable:false
        }
    }

    componentDidMount = (prevProps) => {
        editPulldownListEventCenter.addListener(editPulldownListEventCenter.SET_ITEM_SELECTED, this.setItemSelected);
        editPulldownListEventCenter.addListener(editPulldownListEventCenter.EDIT_PULLDOWN_PASTE_ITEM_ENABLE, this.setPasteItemEnable);
        sheetPullDownListEventCenter.addListener(sheetPullDownListEventCenter.SHEET_PULLDOWN_SAVE_PNG_BY_SELECTED_ENABLE, this.setIsSavePngBySelectedEnable);
        
    }

    setItemSelected = (hasItem)=>{
        this.setState({hasItemSelected:hasItem});
    }

    setIsSavePngBySelectedEnable = (savePngBySelectedFlag) => {
        this.setState({
            isSavePngBySelectedEnable: savePngBySelectedFlag
        });
    }
    setPasteItemEnable = (enable)=>{
        this.setState({pasteItemEnable:enable});
    }

    menuClick = (eventName) => {
        // const { triggerMenuClick } = this.context
        // if (typeof triggerMenuClick === 'function') {
        //     triggerMenuClick(eventName);
        // }
    }

    menuCopyClick = (e) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Copy');
        e.preventDefault();
        editPulldownListEventCenter.fireEvent(editPulldownListEventCenter.EDIT_PULLDOWN_COPY_ITEM);
    }

    menuPasteClick = (e) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Pase');
        e.preventDefault();
        editPulldownListEventCenter.fireEvent(editPulldownListEventCenter.EDIT_PULLDOWN_PASTE_ITEM);
    }

    menuDeleteClick = (e) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Delete');
        e.preventDefault();
        editPulldownListEventCenter.fireEvent(editPulldownListEventCenter.EDIT_PULLDOWN_DELETE_ITEM);
    }

    menuCreateItemClick = (e) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Create_Item');
        e.preventDefault();
        editPulldownListEventCenter.fireEvent(editPulldownListEventCenter.EDIT_PULLDOWN_CREATE_ITEM);
    }

    menuCreateFromDocCLick = (e) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Create_Item_From_Doc');
        e.preventDefault();
        editPulldownListEventCenter.fireEvent(editPulldownListEventCenter.EDIT_PULLDOWN_CREATE_ITEM_BY_DOCUMENT);
    }

    menuCreateBaseItemClick = (e) => {
        ActionLogger.click('User_Editor_Header', 'Menu_Create_Base_Item');
        e.preventDefault();
        editPulldownListEventCenter.fireEvent(editPulldownListEventCenter.EDIT_PULLDOWN_DO_CREATE_BASE_ITEM);
    }

    

    renderEditItems = () => {
        const { t } = this.props;
        const {hasItemSelected, pasteItemEnable} = this.state;
        let menuClassName = "dropdown-item dropdown-item-editor dropdown-item-sk dropdown-item-sk-editor";
        let itemActionMenuClassName = [menuClassName];
        let itemCopingMenuClassName = [menuClassName];
        itemActionMenuClassName.push('noselect');
        itemCopingMenuClassName.push('noselect');

        //TODO: disable hoặc enable button copy và item select
        // const { itemSelections, itemCopy } = this.context;
        if ( hasItemSelected !== true ) {
            itemActionMenuClassName.push('dropdown-item-disabled dropdown-item-disabled-editor');
        }

        if (pasteItemEnable !== true) {
            itemCopingMenuClassName.push("dropdown-item-disabled dropdown-item-disabled-editor");
        }

        return (
            <React.Fragment>
                <Link className="dropdown-item dropdown-item-editor dropdown-item-sk dropdown-item-sk-editor noselect" onClick={this.menuCreateItemClick} ><span>{t('editor.itemCreateAssist.create')}</span><span>Shift+Enter</span></Link>
                <Link className="dropdown-item dropdown-item-editor noselect" onClick={this.menuCreateFromDocCLick} >{t('editor.header.edit.createItemFromDoc')}</Link>
                <Link className="dropdown-item dropdown-item-editor border-bottom-dropdown border-bottom-dropdown-editor noselect" onClick={this.menuCreateBaseItemClick} >{t('editor.header.edit.createBaseitem')}</Link>

                <Link className={itemActionMenuClassName.join(' ')} onClick={this.menuCopyClick} ><span>{t('editor.header.edit.copy')}</span><span>Ctrl+C</span></Link>
                <Link className={itemCopingMenuClassName.join(' ')} onClick={this.menuPasteClick} ><span>{t('editor.header.edit.paste')}</span><span>Ctrl+V</span></Link>
                <Link className={itemActionMenuClassName.join(' ')} onClick={this.menuDeleteClick} ><span>{t('editor.header.edit.remove')}</span><span>Delete</span></Link>
            </React.Fragment>
        );
    }

    render() {
        const { pulldownIcon, t, isSheetLocked } = this.props;
        const {isSavePngBySelectedEnable} = this.state;

        return (
            <div onClick={() => {
                    ActionLogger.click('User_Editor_Header', 'Edit_Menu');
                }}>
                <PulldownButton title={t('editor.header.edit.label')}
                    classDropDown="dropdown-header-2"
                    classDropDownMenu="dropdown-px-15 dropdown-px-15-editor"
                    pulldownIcon={pulldownIcon}
                    pullDownItems={this.renderEditItems()}
                    classLink={isSheetLocked ||  isSavePngBySelectedEnable ? "disabled" : ""}
                    hasItemSelected={this.state.hasItemSelected}
                />
            </div>
        );
    }
}

export default withTranslation()(withMapContainerContext(EditPullDownList));