import React from 'react';
import SearchConfigDto from 'wlp-client-service/dto/search/SearchConfigDto';
import grayItemIcon from 'wlp-client-common/images/grayItemIcon@4x.png';
import { DEFAULT_ALL_RESULT } from 'wlp-client-mypage-lib/consts/UserSetting';
import SearchResultListItem from './SearchResultListItem';
import { withMapContainerContext } from "./MapContainerContext";
import rightArrowIcon2 from 'wlp-client-common/images/icon/rightArrowIcon2.png';
import SearchService from 'wlp-client-service/service/SearchService';
import SearchListItemViewModal from './SearchListItemViewModal';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';


const searchService = new SearchService();
class SearchResultPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            searchList: [],
            kindViewIcon: grayItemIcon,
            showSearchListModal:false,
            isShowAllResult :false,
        };
    }

    findReadingAllResult = async (startIndex) => {
        let requestDto = new SearchConfigDto();
        const { valueSearch } = this.props;
        const { mapInfo } = this.props.mapContainerContext;
        requestDto.contractId = requestDto.contractId || -1;
        requestDto.count = parseInt(DEFAULT_ALL_RESULT);
        requestDto.mapId = mapInfo.id;
        requestDto.text = valueSearch;
        requestDto.start = startIndex || 0;

        const result = await searchService.findItemsInEditor(requestDto);
        return result;
    }
    getDataResultSearchByOption = async (startIndex) => {
        ActionLogger.click('User_Editor_SearchResult', 'Show_All_Result');
        let resultAllDataSearch = await this.findReadingAllResult(startIndex);

        this.setState({
            kindViewIcon: grayItemIcon,
            searchList: resultAllDataSearch,
            showSearchListModal: true
        });

        this.setShowSearchListModal(true);
    }

    setShowSearchListModal = (status) => {
        this.setState({
            isShowAllResult: status
        });
    }

    onMouseDown = (evt) => {
        if (!checkElementClicked(evt, this.SearchResultPanelRef)) {
            this.props.hideSearchResultPanel();
        }
    }

    setSearchResultPanelRef = (ref) => {
        this.SearchResultPanelRef = ref;
        this.props.setSearchResultPanelRef(ref);
    }
 
    render(){
        let { 
            t,
            searchResult, 
            setSearchListItemViewModalRef,
            valueSearch} = this.props;    
        return (<React.Fragment>
            {(searchResult.entries && searchResult.entries.length > 0 && !this.state.isShowAllResult) && (
                <div className="modal-backdrop" 
                    onMouseDown={this.onMouseDown} 
                    onTouchStart={this.onMouseDown} 
                    style={{background: 'transparent', zIndex: 80, position: 'absolute'}}>
                    <div className="results-modal results-modal-editor show show-editor" ref={this.setSearchResultPanelRef}>
                        <div className="results-ct-list">

                            <div className="results-ct results-ct-editor">

                                <div className="results-left results-left-editor">
                                    <div className="results-left-tlt results-left-tlt-editor noselect"><strong>{t('search.items')}</strong><span>({searchResult.hitCount})</span></div>
                                    <div className="results-left-all results-left-all-editor">
                                        <div className="d-flex align-items-start noselect">
                                            <img src={rightArrowIcon2} alt="" />
                                            <button className="btn-reset btn-reset-editor noselect"
                                                onClick={() => this.getDataResultSearchByOption()}
                                            >{t('search.showAllResults')}</button>
                                        </div>
                                    </div>
                                </div>
                                <SearchResultListItem searchItems={searchResult.entries} t={t} valueSearch={valueSearch} />
                            </div>
                        </div>
                    </div>
                </div>
            )}
            {(!searchResult.entries || searchResult.entries.length === 0) && (
                <div className="modal-backdrop" onMouseDown={this.props.hideSearchResultPanel} onTouchStart={this.props.hideSearchResultPanel} style={{background: 'transparent', zIndex: 80}}>
                    <div className="no-results-modal no-results-modal-editor show show-editor">
                        <div className="no-results-modal-ct no-results-modal-ct-editor text-center">
                            <div className="font-weight-bold fs-16 fs-16-editor color-nors-tlt color-nors-tlt-editor">{t('search.noResults1')}</div>
                            <div className="fs-12 fs-12-editor color-nors color-nors-editor">{t('search.noResults2')}</div>
                        </div>
                    </div>
                </div>   
            )}
            {
                this.state.showSearchListModal&& 
                <SearchListItemViewModal 
                    refModalSearch={this.modalSearch}
                    t={t}
                    setSearchListItemViewModalRef={setSearchListItemViewModalRef}
                    searchList={this.state.searchList}
                    showSearchListModal={this.state.showSearchListModal}
                    iconView={this.state.kindViewIcon}
                    kindView={this.state.kindView}
                    getDataResultSearchByOption = {this.getDataResultSearchByOption}
                    setShowSearchListModal =  {this.setShowSearchListModal}
                    isShowAllResult={this.state.isShowAllResult}
                    valueSearch={valueSearch}
                 />
                
            }
        </React.Fragment>)
    }
}
export default withMapContainerContext(SearchResultPanel);