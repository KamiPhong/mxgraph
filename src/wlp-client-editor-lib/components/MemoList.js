import React from 'react';
import { withTranslation } from 'react-i18next';
import iconCrArrow1 from 'wlp-client-common/images/icon/icon-cr-arrow-1.png';
import iconRefresh from 'wlp-client-common/images/icon/icon-refresh.png';
import Link from 'wlp-client-common/component/Link';
import LinkCamiApp from 'wlp-client-common/consts/LinkCamiAppConsts';

class MemoList extends React.Component {

    render() {
        const { toggleOpen, t, pulldownIcon } = this.props;

        return (<React.Fragment>
            <div className="sidebar sidebar-editor">
                <Link className="sidebar-btntoggle sidebar-btntoggle-editor btn-reset btn-reset-editor" onClick={toggleOpen}>
                    <img src={iconCrArrow1} alt="" />
                </Link>
                <div className="sidebar-top sidebar-top-editor">
                    <div className="dropdown dropdown-editor">
                        <Link className="btn btn-editor btn-sm col-136px col-136px-editor btn-option btn-option-editor" id="dropdownMenuButton"
                            data-toggle="dropdown dropdown-editor" aria-haspopup="true" aria-expanded="false">
                            <span>{t('editor.option.label')}</span>
                            <img src={pulldownIcon} alt="" />
                        </Link>
                        <div className="dropdown-menu dropdown-menu-editor dropdown-menu-bg-black dropdown-menu-bg-black-editor dropdown-px-16 dropdown-px-16-editor dropdown-menu-pd dropdown-menu-pd-editor"
                            aria-labelledby="dropdownMenuButton">
                            <Link className="dropdown-item dropdown-item-editor " data-toggle="modal modal-editor">Option 1</Link>
                            <Link className="dropdown-item dropdown-item-editor " data-toggle="modal modal-editor">Option 2</Link>
                        </div>
                    </div>
                    <Link className="btn btn-editor btn-sm btn-refresh btn-refresh-editor btn-dark-gr btn-dark-gr-editor d-flex-center-fl d-flex-center-fl-editor ml-10px ml-10px-editor">
                        <img src={iconRefresh} alt="" />
                    </Link>
                </div>
                <div className="sidebar-ct sidebar-ct-editor">
                    <div className="box-ct box-ct-editor h-100">
                        <h2 className="sidebar-tlt sidebar-tlt-editor">
                            {t('editor.memo.label.title')}
                        </h2>
                        <p className="color-white color-white-editor mb-17px mb-17px-editor">{t('editor.memo.label.description')}</p>
                        <p className="color-white color-white-editor mb-0">{t('editor.memo.label.download')}</p>
                        <ul className="list-ver list-ver-editor">
                            <li>
                                <Link className="btn-reset btn-reset-editor " href={LinkCamiApp.IOS_CAMIAPP}  target="_blank">
                                    <span>{t('editor.memo.link.ios')}</span>
                                </Link>
                            </li>
                            <li>
                                <Link className="btn-reset btn-reset-editor" href={LinkCamiApp.ANDROID_CAMIAPP} target="_blank">
                                    <span>{t('editor.memo.link.android')}</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </React.Fragment>)
    }
}

export default withTranslation()(MemoList);