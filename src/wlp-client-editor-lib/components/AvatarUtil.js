import ImageDefault from "wlp-client-common/utils/ImageDefault";

class AvatarUtil {
    getSource = (url) => {
        if(url === null || url === ""){
            return ImageDefault['Default1S']
        }else {
            if(this.checkDefaultAvatar(url)){
                return ImageDefault[url];
            }else{
                return url;
            }
        }
        
    };

    checkDefaultAvatar = (url) => {
        return ImageDefault[url] !== undefined;
    }
}

export  default AvatarUtil; 