import React from 'react';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import {htmlDecode} from 'wlp-client-common/utils/TextUtils'
import i18n from "i18n";

const wlpDateUtil = new WlpDateUtil();

class ShowAllSearchItem extends React.Component {
    constructor(props) {
        super(props);
        this.content = React.createRef();
    }

    rowRender(leftContent, rightContent, leftIsBold, rightIsBold) {
        const { t, showAllItem } = this.props;

        if (leftIsBold) {
            leftContent = <React.Fragment>{t('search.item')}:<span ref={this.content}>
                {this.highlightSearch()}
            </span></React.Fragment>
        }

        if (rightIsBold) {
            rightContent = <React.Fragment>{t('search.item')}:<span ref={this.content}>
                {this.highlightSearch()}
            </span></React.Fragment>
        }

        return (<div className="media-body-item media-body-item-editor">
            {
                showAllItem ?
                    <div className="media-body-item-l media-body-item-l-editor noselect">{leftContent}</div> :
                    <div className="media-body-item-l media-body-item-l-editor noselect">{leftContent}</div>
            }
            <div className="media-body-item-r media-body-item-r-editor noselect">{htmlDecode(rightContent)}</div>
        </div>);
    }

    highlightSearch = (highlightContent) => {
        const { item, valueSearch } = this.props;
        if (!highlightContent) {
            const regSearchValue = new RegExp("(\\b" + valueSearch + "\\b)", "gim");
            let innerSearchText = item.plainItemText;
            const innerSearchSpan = innerSearchText.replace(/(<span>|<\/span>)/igm, "");
            const newInnerSearchSpan = innerSearchSpan.replace(regSearchValue, "<span style='font-weight: bold'>$1</span>");
            return <span dangerouslySetInnerHTML={{ __html: newInnerSearchSpan }} />;
        } else {
            return highlightContent;
        }
    }

    render() {
        const { t, item, showAllItem } = this.props;
        const localeCode = i18n.language;
        return (
            <React.Fragment>
                {this.rowRender(`${t('search.map')}:${item.mapName}`, `${t('search.team')}:${item.teamName}`, false, false)}
                {this.rowRender(`${t('search.sheet')}:${item.sheetName}`, `${t('search.author')}:${item.createUserName}`, false, false)}

                {
                    showAllItem ?
                        this.rowRender(`${item.plainItemText}`, `${t('search.lastUpdated')} ${wlpDateUtil.formatDateTime(item.updateDate, localeCode)}`, true, false) :
                        this.rowRender(`${item.plainItemText}`, '', true, false)
                }
            </React.Fragment>
        );
    }
}
export default ShowAllSearchItem;