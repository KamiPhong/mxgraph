import React, { Component } from 'react';
import {
    imgNaviHeaderHide,
    imgNaviHeaderShow
} from '../utils/ResourceIconUtils'
import { StageConfig } from 'wlp-client-editor-lib/core/KGraphConfig';
import i18n from 'i18n';
import { OUTLINE_SIZE } from 'wlp-client-editor-lib/core/KGraphConfig';
import ActionLogger from 'wlp-client-common/ActionLogger';

let zoomStyle = {
    width: StageConfig.MAX_WIDTH * OUTLINE_SIZE,
    height: StageConfig.MAX_HEIGHT * OUTLINE_SIZE
}
let padding = 5;

export default class SheetNavigator extends Component {

    state = {
        isShowNavigator: true
    }

    toggleNavigatorBody = () => {
        ActionLogger.click('User_Editor_SheetView', 'Control_Toggle_Navigator');
        const isShow = this.state.isShowNavigator;
        this.setState({isShowNavigator: !isShow});
    }

    render = () => {
        const { isShowNavigator } = this.state;
        return (
            <div className="navi-outline">
                <div className="navigator-header" style={{width: zoomStyle.width + padding * 2}}>
                    <span className="navigator-title noselect">{i18n.t('editor.navigator.title')}</span>
                    <span className="navi-header-controll">
                        <img src={isShowNavigator ? imgNaviHeaderHide : imgNaviHeaderShow } alt="imgNaviHeaderHide" className='noselect' onClick={this.toggleNavigatorBody}/>
                    </span>
                </div>
                <div className={`navigator-body ${isShowNavigator ? '' : 'd-none'}`}>
                    <div className="navi-container" style={{padding: padding}}>
                        <div className="navi-content" style={zoomStyle} ref="navigator"></div>
                    </div>
                </div>
            </div>
        );
    }
}