import React, { Component} from 'react';
import { withTranslation } from 'react-i18next';
import Link from 'wlp-client-common/component/Link';

class SheetUnLockButton extends Component {
    render() {
        const { t, unLockSheet } = this.props;

        return (<React.Fragment>
            <Link className="btn btn-editor btn-dark-gr btn-dark-gr-editor btn-lock btn-lock-editor btn-shadow active active-editor" onClick={unLockSheet}>
                <div className="btn-lock-ic btn-lock-ic-editor"></div>
                <div className="btn-lock-txt btn-lock-txt-editor">{t('editor.lock.buttonLabel')}</div>
            </Link>     
        </React.Fragment>)
    }
}

export default withTranslation()(SheetUnLockButton);