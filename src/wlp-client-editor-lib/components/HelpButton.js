import React from 'react';
import Link from 'wlp-client-common/component/Link';
import ActionLogger from 'wlp-client-common/ActionLogger';

class HelpButton extends React.Component{

    render(){
        const { helpIcon } = this.props;
        return (<React.Fragment>
            <Link
                onClick={() => {
                    ActionLogger.click('User_Editor_Header', 'Help_Button');
                }}
                className="btn btn-editor editor-header-right-btn editor-header-right-btn-editor h-100 noselect"
                href="https://www.accelatech.com/support/KB/kb_manual.html" 
                target="_blank" 
                tabIndex={3} title="" style={{ cursor: "default" }}>
                    <img src={helpIcon} alt="" />
                </Link>
        </React.Fragment>)
    }
}

export default HelpButton;