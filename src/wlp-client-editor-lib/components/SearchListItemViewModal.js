import React from 'react';
import { withMapContainerContext } from "./MapContainerContext";
import backIcon2 from 'wlp-client-common/images/icon/backIcon2.png';
import { DEFAULT_ALL_RESULT } from 'wlp-client-mypage-lib/consts/UserSetting';
import SearchResultItem from './SearchResultItem';
import ActionLogger from 'wlp-client-common/ActionLogger';

class SearchListItemViewModal extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
            page : 1,
            resultFrom : 0,
            resultTo: DEFAULT_ALL_RESULT,
            totalPage : 1,
        };

        const { searchList } = this.props;
        let to = this.state.resultTo;
        if (to > searchList.hitCount) {
            to = searchList.hitCount;
        }

        this.displayNumberOfSearchResult(this.state.resultFrom, to);
    }

    hideShowSearchListModal(){
        ActionLogger.click('User_Editor_SearchResult', `Hide_Search_List_Modal_Button`);
        this.props.setShowSearchListModal(false);
    }

    paginateSearchResultNext =(status) =>{
        ActionLogger.click('User_Editor_SearchResult', `Paginate_Search_Result_Next`);
        if(status === "active"){
            const {searchList} = this.props;
            let numberOfPage =  Math.ceil(searchList.hitCount / DEFAULT_ALL_RESULT);
            if(numberOfPage > this.state.page){
                this.setState({
                    page: parseInt(this.state.page + 1),
                }, () => {
                    this.pagingResultNext(this.state.page);
                });
            }
        }
    }

    paginateSearchResultPrev = (status) =>{
        ActionLogger.click('User_Editor_SearchResult', `Paginate_Search_Result_Prev`);
        if(status === "active"){
            if(this.state.page !== 0){
                this.setState({
                    page: parseInt(this.state.page - 1),
                }, () => {
                    this.pagingResultPrev(this.state.page);
                });
            }
        }
    }

    pagingResultPrev = (page) => {
        const {searchList} = this.props;
        let from = parseInt(this.state.resultFrom - 1 - DEFAULT_ALL_RESULT);
        if(from <0){
            from = 0;
        }
        let to = from + DEFAULT_ALL_RESULT;
        if(from === 0 && to > searchList.hitCount){
            to = searchList.hitCount;
        }
        this.props.getDataResultSearchByOption(from);
        this.displayNumberOfSearchResult(from, to);
    }
    pagingResultNext = (page) => {
        const {searchList} = this.props;
        let paginateIndex = DEFAULT_ALL_RESULT

        if(page > 2 ){
            paginateIndex = this.state.resultTo;
        }

        let from = paginateIndex;
        if(from > searchList.hitCount){
            from = searchList.entries.length;
        }
        let to = from + DEFAULT_ALL_RESULT;
        if(to > searchList.hitCount || to < searchList.hitCount){
            to = searchList.hitCount;
        }
        this.props.getDataResultSearchByOption(from);
        this.displayNumberOfSearchResult(from, to);
    }

    displayNumberOfSearchResult = (from, to) =>{
        this.setState({
            resultFrom : parseInt(from) + 1,
            resultTo:to,
        });
    }

    componentDidMount = () => {
        const { searchList } = this.props;
        this.setState({
            totalPage: Math.ceil(searchList.hitCount / DEFAULT_ALL_RESULT),
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.isShowAllResult !== prevProps.isShowAllResult) {
            if (this.props.isShowAllResult) {
                //[ThuyTV] prevent body scroll while result modal is showed
                document.body.classList.add('overflow-hidden');
            } else {
                document.body.classList.remove('overflow-hidden');
            }
        }
    }

    render(){
        const {
            t,
            searchList, 
            setSearchListItemViewModalRef,
            isShowAllResult,
            valueSearch} = this.props;
        return (
            <React.Fragment>
                {isShowAllResult &&
                    <div className="results-all-modal results-all-modal-editor show show-editor" ref={rlim => setSearchListItemViewModalRef(rlim) }>
                        <div className="results-all-list results-all-list-editor">
                            <div className="results-all-header results-all-header-editor">
                                <button className="btn-reset btn-reset-editor btn-header-left btn-header-left-editor header-left header-left-editor noselect"
                                onClick={() => this.hideShowSearchListModal()}>
                                <img src={backIcon2} alt="" /> <strong>{t('search.back')}</strong></button>
                                <div className="results-header-right results-header-right-editor">
                                    {searchList.hitCount > 0  &&
                                        <div className="results-total results-total-editor noselect">
                                            <span>{this.state.resultFrom}-{this.state.resultTo}</span>
                                            <span>of</span>
                                            <span>{searchList.hitCount}</span>
                                        </div>
                                    }
                                    <div className="results-pagi results-pagi-editor">
                                        <div className="results-pagi-btn results-pagi-btn-editor">
                                            <button className={"btn-reset btn-reset-editor results-pagi-prev results-pagi-prev-editor "+((this.state.resultFrom > DEFAULT_ALL_RESULT) ? "active active-editor" : "")}
                                            onClick={() => this.paginateSearchResultPrev(((this.state.resultFrom > DEFAULT_ALL_RESULT) ? "active active-editor" : ""))}
                                            ></button>
                                            <button className={"btn-reset btn-reset-editor results-pagi-next results-pagi-next-editor "+(this.state.page < this.state.totalPage ? "active active-editor" : "")} 
                                            onClick={() => this.paginateSearchResultNext((this.state.page < this.state.totalPage ? "active active-editor" : ""))}
                                            ></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {searchList.hitCount > 0  &&
                                <div className="results-all-ct results-all-ct-editor">
                                    <ul className="results-list results-list-editor ul-reset ul-reset-editor results-list-full results-list-full-editor">
                                        {searchList.entries.map((item, index) => {
                                             return (<SearchResultItem item={item} key={index} t={t} valueSearch={valueSearch}/>)
                                            
                                        })}
                                    </ul>
                                </div>
                            }
                        </div>
                    </div>
                }
            </React.Fragment>
        )
    }
}

export default withMapContainerContext(SearchListItemViewModal);