import React, { forwardRef } from 'react';

const MapContainerContext = React.createContext({});

export function withMapContainerContext(Component) {
    return forwardRef((props, ref) => {
        return (
            <MapContainerContext.Consumer>
                {(context) => {
                    return <Component ref={ref} {...props} mapContainerContext={context} />;
                }}
            </MapContainerContext.Consumer>
        );
    });
}

export default MapContainerContext;