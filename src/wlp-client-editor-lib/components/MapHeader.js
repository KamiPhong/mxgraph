import React from 'react';
import LogoImg from './LogoImg';
import HeaderMenuList from './HeaderMenuList';
import SearchInput from './SearchInput';
import logo from 'wlp-client-common/images/logo.jpg';
import helpIcon from 'wlp-client-common/images/icon/helpIcon.svg';
import HelpButton from './HelpButton';
import SheetPullDownList from './ControlsGroup/SheetPullDownList';
import EditPullDownList from './ControlsGroup/EditPullDownList';
import DisplayPullDownList from './ControlsGroup/DisplayPullDownList';
import MainSkinContext from "../../wlp-client-editor/MainSkinContext";
import { withMapContainerContext } from "./MapContainerContext";
import { MY_PAGE_PATH_PREFIX } from 'wlp-client-common/config/EditorConfig';
import ActionLogger from 'wlp-client-common/ActionLogger';

class MapHeader extends React.Component {
    static contextType = MainSkinContext;
    constructor(props) {
        super(props);
        this.state = {
            showBackDropCs: false,
            isShowSearchResultPanel: false
        };

        this.header = React.createRef();
        this.searchResultPanel = React.createRef();
        this.searchListItemViewModal = React.createRef();
    }

    setSearchResultPanelRef = (ref) => {
        this.searchResultPanel = ref;
    }
    setSearchListItemViewModalRef = (ref) => {
        this.searchListItemViewModal = ref;
    }


    setShowSearchResultPanel = (isShow) => {
        this.setState({ isShowSearchResultPanel: isShow });
    }

    hideSearchResultPanel = () => {
        this.setShowSearchResultPanel(false);
    }

    documentClickHandler = (evt) => {
        if (!this.state.isShowSearchResultPanel) {
            return;
        }

        let currentNode = evt.target;
        let isCloseSearchPanel = false;
        while (currentNode.parentNode) {

            if (
                currentNode === this.header ||
                currentNode === this.searchResultPanel ||
                currentNode === this.searchListItemViewModal
            ) {
                isCloseSearchPanel = true;
                break;
            }
            currentNode = currentNode.parentNode;
        }

        if (this.state.isShowSearchResultPanel !== isCloseSearchPanel) {
            if (currentNode.className !== "results-all-modal results-all-modal-editor show show-editor") {
                this.setShowSearchResultPanel(isCloseSearchPanel);
            }

        }
    }

    logoClickHandler = () => {
        ActionLogger.click('User_Editor_Header', 'Logo_K-board');
        window.open(MY_PAGE_PATH_PREFIX, '_blank');
    }

    componentDidMount() {
        document.addEventListener('click', this.documentClickHandler);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.documentClickHandler);
    }

    render() {
        const {
            sheetDeleteAvailable,
            sheetLockButtonVisible,
            mapContainerContext,
            hideLogoutFlg,
        } = this.props;
        const { isShowSearchResultPanel } = this.state;
        const { isSheetLocked } = this.props.mapContainerContext;
        return (<React.Fragment>
            <div className="header" ref={hd => this.header = hd}>
                <div className="header-left header-left-editor">
                    <LogoImg logo={logo} onClickLogo={this.logoClickHandler} />
                    <SheetPullDownList
                        isSheetLocked={isSheetLocked}
                        sheetLockButtonVisible={sheetLockButtonVisible}
                        sheetDeleteAvailable={sheetDeleteAvailable} />
                    <EditPullDownList isSheetLocked={isSheetLocked} />
                    <DisplayPullDownList />
                </div>
                <div className="header-right header-right-editor">
                    <div className="d-flex h-100">
                        <HeaderMenuList hideLogoutFlg={hideLogoutFlg} />
                        <SearchInput
                            setSearchResultPanelRef={this.setSearchResultPanelRef}
                            setSearchListItemViewModalRef={this.setSearchListItemViewModalRef}
                            hideSearchResultPanel={this.hideSearchResultPanel}
                            isShowSearchResultPanel={isShowSearchResultPanel}
                            setShowSearchResultPanel={this.setShowSearchResultPanel}
                            mapContainerContext={mapContainerContext} />
                        <HelpButton helpIcon={helpIcon} />
                    </div>
                </div>
            </div>
        </React.Fragment>)
    }
}

export default withMapContainerContext(MapHeader);