import React from 'react';
import LoadModalBodyEnd from 'wlp-client-common/component/LoadModalBodyEnd';
import ImageUtils from 'wlp-client-common/utils/ImageUtils';
import { mxClient } from 'wlp-client-editor-lib/core/KClient';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';
const imageUtils = new ImageUtils();

export default class ToolTipMember extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isShowToolTip: false,
            mousePos: {
                x: 0,
                y: 0
            }
        }

        this.hideToolTipTimer = null;
        this.userAvatarRef = null;

        this.tooltipSize = this.computeToltipSize();
    }

    _onMouseLeaveHandler = () => {
        if (!mxClient.IS_TOUCH) {
            this.resetTooltipTimeout();
            if (this.state.isShowToolTip) {
                this.setState({ isShowToolTip: false });
            }
        }
    }

    resetTooltipTimeout = () => {
        if (this.hideToolTipTimer) {
            clearTimeout(this.hideToolTipTimer);
            this.hideToolTipTimer = null;
        }
    }

    setTootipTimeout = (callback) => {
        if (mxClient.IS_TOUCH && typeof callback === 'function') {
            callback();
        }else if (!this.hideToolTipTimer) {
            this.hideToolTipTimer = setTimeout(callback, 400);
        }
    }

    /**
     * 
     * @param {MouseEvent} evt 
     */
    _onMouseMoveHandler = (evt) => {
        if (!mxClient.IS_TOUCH) {
            let x = evt.clientX + 7;
            let y = evt.clientY + 7;

            if (this.isOverflowX(x)) {
                x -= (this.tooltipSize.width + 10);
            }

            if (this.isOverFlowY(y)) {
                y -= (this.tooltipSize.height + 10);
            }

            this.showTooltipAtPos(x, y);
        }
    }

    /**
     * 
     * @param {TouchEvent} evt 
     */
    _onTouchStartHanler = (evt) => {
        if (mxClient.IS_TOUCH && !this.state.isShowToolTip) {
            const targetBounds = evt.target.getBoundingClientRect();
            let x = targetBounds.left - 10;
            let y = targetBounds.top - 40;

            if (this.isOverflowX(x)) {
                x = document.body.offsetWidth - this.tooltipSize.width - 10;
            }

            if (this.isOverFlowY(y)) {
                y = document.body.offsetHeight - this.tooltipSize.height - 20;
            }

            this.showTooltipAtPos(x, y);
        }

    }

    showTooltipAtPos = (x, y) => {
        if(!this.state.isShowToolTip) {
            this.setTootipTimeout(() => {
                this.setState({ isShowToolTip: true, mousePos: { x, y } });
                this.resetTooltipTimeout();
            });
        }

        if (this.state.isShowToolTip) {
            this.setState({ mousePos: { x, y }});
        }
    }

    isOverflowX = (x) => {
        return x + this.tooltipSize.width >= document.body.offsetWidth;
    }

    isOverFlowY = (y) => {
        return y + this.tooltipSize.height >= document.body.offsetHeight;
    }

    computeToltipSize = () => {
        let size = { width: 0, height: 0 };
        
        if (!this.props.data || !this.props.data.name) {
            return size;
        }

        let text = document.createElement("span"); 
        text.style.cssText = `
            text-align: left;
            border-radius: 3px;
            padding: 5px 6px;
        `;

        text.style.height = 'auto'; 
        text.style.width = 'auto'; 
        text.style.position = 'absolute'; 
        text.style.whiteSpace = 'pre-line';
        text.style.maxWidth = '290px';
        text.style.wordBreak = 'break-all';
        text.innerHTML = this.props.data.name;

        document.body.appendChild(text); 
        size.width = Math.ceil(text.clientWidth); 
        size.height = Math.ceil(text.clientHeight);
        document.body.removeChild(text);

        return size;
    }

    setUserAvatarRef = (ref) => {
        this.userAvatarRef = ref;

        if (this.userAvatarRef instanceof HTMLElement && mxClient.IS_TOUCH) {
            document.addEventListener('touchstart', this.handleDocumentTouchStart);
        }
    }

    handleDocumentTouchStart = (evt) => {
        if (
            this.state.isShowToolTip && 
            !checkElementClicked(evt, this.userAvatarRef)) {
            this.setState({ isShowToolTip: false });
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.data && this.props.data && prevProps.data.name !== this.props.data.name) {
            this.tooltipSize = this.computeToltipSize();
        }
    }

    componentWillUnmount() {
        this.resetTooltipTimeout();
        if (this.userAvatarRef instanceof HTMLElement && mxClient.IS_TOUCH) {
            document.removeEventListener('touchstart', this.handleDocumentTouchStart);
        }
    }

    render() {
        const { isShowToolTip } = this.state;
        const { data } = this.props;

        return (
            <React.Fragment>
                <div 
                    ref={this.setUserAvatarRef} key={data.id} 
                    className={'noselect tooltip-context'} 
                    onMouseMove={this._onMouseMoveHandler} 
                    onMouseLeave={this._onMouseLeaveHandler}
                    onTouchStart={this._onTouchStartHanler}>
                     {this.props.children ? this.props.children : <img className={"item-menu-img noselect img-default"} src={imageUtils.getProfileImage(data.thumbnailUri)}  alt="" />}
                </div>
                {isShowToolTip && <LoadModalBodyEnd>
                    <span className="noselect" style={{
                        position: 'absolute',
                        left: this.state.mousePos.x,
                        top: this.state.mousePos.y,
                        width: 'auto',
                        height: 'auto',
                        zIndex: 99999,
                        whiteSpace: 'pre-line',
                        maxWidth: 290,
                        "textAlign": "center",
                        "borderRadius": "3px",
                        "padding": "5px 6px",
                        "backgroundColor": "red",
                        "color": "#fff",
                        "WebkitBoxShadow": "0px 2px 3px -1px rgba(156, 155, 155, 0.9)",
                        "MozBoxShadow": "0px 2px 3px -1px rgba(156, 155, 155, 0.9)",
                        "boxShadow": "0px 2px 3px -1px rgba(156, 155, 155, 0.9)",
                        "wordBreak" : "break-all"

                    }}>{data.name}</span>
                </LoadModalBodyEnd>}
            </React.Fragment>
        )
    }
}
