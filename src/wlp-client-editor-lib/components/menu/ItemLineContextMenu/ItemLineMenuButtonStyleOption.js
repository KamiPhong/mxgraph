import React, { Component } from 'react';

import {
    ItemLineColorTypeA,
    ItemLineColorTypeB,
    ItemLineColorTypeC,
    ItemLineColorTypeD,
    ItemLineColorTypeE,
} from 'wlp-client-editor-lib/utils/ResourceIconUtils';

import KItemLineColor from 'wlp-client-editor-lib/core/KItemLineColor';
import ItemLineMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemLineMenuButtonBarEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';

const lineMenuButtonBarEventCenter = new ItemLineMenuButtonBarEventCenter();

class ItemLineMenuButtonStyleOption extends Component {
    onClickchangeLineColor = (color) => {
        ActionLogger.click('User_Editor_ItemLineMenuButton', 'Change_Line_Color_Button');
        lineMenuButtonBarEventCenter.fireEvent(lineMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_COLOR_CHANGE, color);
    }

    defaultClassName = "context-action cursor-click";

    textAlign = [
        {
            img: ItemLineColorTypeA,
            className: this.defaultClassName,
            action: KItemLineColor.A
        },
        {
            img: ItemLineColorTypeB,
            className: this.defaultClassName,
            action: KItemLineColor.B
        },
        {
            img: ItemLineColorTypeC,
            className: this.defaultClassName,
            action: KItemLineColor.C
        },
        {
            img: ItemLineColorTypeD,
            className: this.defaultClassName,
            action: KItemLineColor.D
        },
        {
            img: ItemLineColorTypeE,
            className: this.defaultClassName,
            action: KItemLineColor.E
        }
    ]

    render() {

        return (
            <div className="menu-control-action context-menu-style5">
                <div className="context-item-container">
                    {this.textAlign.map((option, i) => {
                        return <div key={i} className={option.className} onClick={() => this.onClickchangeLineColor(option.action)} data-style={option.action}>
                            <img className="control-item noselect" src={option.img} alt="" />
                        </div>
                    })}
                </div>
            </div>
        )
    }
}

export default ItemLineMenuButtonStyleOption;