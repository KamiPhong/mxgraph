import React, { Component } from 'react';

import {
    ItemLineWeightSmall,
    ItemLineWeightMiddle,
    ItemLineWeightLarge
} from 'wlp-client-editor-lib/utils/ResourceIconUtils';

import ItemLineMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemLineMenuButtonBarEventCenter';
import ItemLineWeightType from 'wlp-client-editor-lib/core/KItemLineWeightType';
import ActionLogger from 'wlp-client-common/ActionLogger';

const itemLineMenuButtonBarEventCenter = new ItemLineMenuButtonBarEventCenter();

class ItemLineWeightOption extends Component {
    onClickchangeWeightType = (weightType) => {
        ActionLogger.click('User_Editor_ItemLineMenuButton', 'Change_Weight_Type_Button');
        itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_WEIGHT_CHANGE, weightType);
    }

    defaultClassName = "d-flex align-items-center context-action cursor-click";

    weights = [
        {
            img: ItemLineWeightSmall,
            className: this.defaultClassName,
            action: ItemLineWeightType.LINE_WEIGHT_SMALL
        },
        {
            img: ItemLineWeightMiddle,
            className: this.defaultClassName,
            action: ItemLineWeightType.LINE_WEIGHT_MIDDLE
        },

        {
            img: ItemLineWeightLarge,
            className: this.defaultClassName,
            action: ItemLineWeightType.LINE_WEIGHT_LARGE
        },
    ]

    render() {
        return (
            <div className="menu-control-action context-menu-style1">
                <div className="context-item-container">
                    {this.weights.map((option, i) => {
                        return <div key={i} className={option.className} data-weight={option.action} onClick={() => this.onClickchangeWeightType(option.action)}>
                            <img className="control-item noselect" src={option.img} alt="" />
                        </div>
                    })}
                </div>
            </div>
        )
    }
}

export default ItemLineWeightOption;