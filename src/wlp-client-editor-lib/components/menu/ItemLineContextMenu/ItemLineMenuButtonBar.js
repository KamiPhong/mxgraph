import React from 'react';

import ItemLineMenuButton from './ItemLineMenuButton';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();

class ItemMenuButtonBar extends React.Component {

    menuCreateBaseItemClick = (e) => {
        e.preventDefault();
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_REMOVE);
    }

    render = () => {
        const className = "pos-abs vertext-context";
        return (
            <React.Fragment>
                <div className={className} id="itemLineMenuButtonBar" style={{ display: 'none', top: "0px" }}>
                    <div className="vertex-control" id="itemLineMenuControl">
                        <ItemLineMenuButton />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default ItemMenuButtonBar;