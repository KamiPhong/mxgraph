import React, { Component } from 'react';

import {
    lineTypeLinearNone,
    lineTypeLinearFromTo,
    lineTypeLinearToFrom,
    lineTypeLinearOneWayBidirectional,
    lineTypeSquareNone,
    lineTypeSquareFromTo,
    lineTypeSquareToFrom,
    lineTypeSquareOneWayBidirectional,
    lineTwoWay,
    lineTwoWayReverse,
} from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import KItemLineArrowType from 'wlp-client-editor-lib/core/KItemLineArrowType';
import ItemLineMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemLineMenuButtonBarEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';

const itemLineMenuButtonBarEventCenter = new ItemLineMenuButtonBarEventCenter();

class ItemLineMenuButtonTypeOption extends Component {
    onClickchangeShapeType = (shapeType) => {
        ActionLogger.click('User_Editor_ItemLineMenuButton', 'Change_Shape_Type_Button');
        itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_SHAPE_CHANGE, shapeType);
    }

    defaultClassName = "context-action cursor-click";

    shapes = [
        {
            img: lineTypeLinearNone,
            className: this.defaultClassName,
            action: KItemLineArrowType.LINEAR_NONE
        },
        {
            img: lineTypeSquareNone,
            className: this.defaultClassName,
            action: KItemLineArrowType.SQUARE_NONE
        },
        {
            img: lineTypeLinearFromTo,
            className: this.defaultClassName,
            action: KItemLineArrowType.LINEAR_FROM_TO
        },
        {
            img: lineTypeLinearToFrom,
            className: this.defaultClassName,
            action: KItemLineArrowType.LINEAR_TO_FROM
        },
        {
            img: lineTypeLinearOneWayBidirectional,
            className: this.defaultClassName,
            action: KItemLineArrowType.LINEAR_ONE_WAY_BIDIRECTIONAL
        },
        {
            img: lineTypeSquareOneWayBidirectional,
            className: this.defaultClassName,
            action: KItemLineArrowType.SQUARE_ONE_WAY_BIDIRECTIONAL
        },
        {
            img: lineTypeSquareFromTo,
            className: this.defaultClassName,
            action: KItemLineArrowType.SQUARE_FROM_TO
        },
        {
            img: lineTypeSquareToFrom,
            className: this.defaultClassName,
            action: KItemLineArrowType.SQUARE_TO_FROM
        },
        {
            img: lineTwoWay,
            className: this.defaultClassName,
            action: KItemLineArrowType.TWO_WAY
        },
        {
            img: lineTwoWayReverse,
            className: this.defaultClassName,
            action: KItemLineArrowType.TWO_WAY_REVERSE
        },
    ]

    render() {
        return (
            <div className="menu-control-action context-menu-style4">
                <div className="context-item-container">
                    {this.shapes.map((option, i) => {
                        return <div key={i} className={option.className} onClick={() => this.onClickchangeShapeType(option.action)} data-type={option.action}>
                            <img className="control-item noselect" src={option.img} alt="" />
                        </div>
                    })}
                </div>
            </div>
        )
    }
}

export default ItemLineMenuButtonTypeOption;