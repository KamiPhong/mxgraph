import React, { Component } from 'react';

import {
    imgContextText,
    imgStyleOption,
    itemLineTypeOption,
    itemLineWeightOption,
    itemLineCloseOption

} from 'wlp-client-editor-lib/utils/ResourceIconUtils';

import ItemLineMenuButtonTextOption from './ItemLineMenuButtonTextOption';
import ItemLineMenuButtonTypeOption from './ItemLineMenuButtonTypeOption';
import ItemLineMenuButtonStyleOption from './ItemLineMenuButtonStyleOption';
import ItemLineWeightOption from './ItemLineWeightOption';

import ItemLineMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemLineMenuButtonBarEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';
const itemLineMenuButtonBarEventCenter = new ItemLineMenuButtonBarEventCenter();

class ItemLineMenuButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSubMenu: ""
        }
        this.styleControl = {
            width: 18 * this.props.scale + 'px'
        }

        itemLineMenuButtonBarEventCenter.addListener(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_DO_CLOSE, this.hideSubMenu);
    }

    hideSubMenu = () => {
        this.setState({showSubMenu: null});
    }

    onClickshowSubMenu = (type) => {
        itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_CLICKED, type);

        this.setState({
            showSubMenu: this.state.showSubMenu === type || this.state.showSubMenu === "download" ? null : type
        })
    }


    itemLineDeleteButtonClickHandler = (evt) => {
        ActionLogger.click('User_Editor_ItemLineMenuButton', 'Item_Line_Delete_Button');
        evt.preventDefault();
        itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_REMOVE);
    }

    render() {
        return (
            <div className="d-flex item-fix-line-height">
                <div className={(this.state.showSubMenu === "type" ? "context-control actived" : "context-control")}>
                    <span className="disabled-cover"></span>
                    <div className="context-tooltip" >
                        <img className={"item-menu-img noselect"} src={itemLineTypeOption} style={this.styleControl} onClick={() => {
                            ActionLogger.click('User_Editor_ItemLineMenuButton', 'Show_All_Type_Line');
                            this.onClickshowSubMenu("type")}} alt="" />
                    </div>
                    <ItemLineMenuButtonTypeOption />
                </div>
                <div className={(this.state.showSubMenu === "weight" ? "context-control actived" : "context-control")}>
                    <span className="disabled-cover"></span>
                    <div className="context-tooltip">
                        <img className={"item-menu-img noselect"} src={itemLineWeightOption} style={this.styleControl} onClick={() => {
                            ActionLogger.click('User_Editor_ItemLineMenuButton', 'Show_All_Weight_Line');
                            this.onClickshowSubMenu("weight")}} alt="" />
                    </div>
                    <ItemLineWeightOption />
                </div>
                <div className={(this.state.showSubMenu === "style" ? "context-control actived" : "context-control")}>
                    <span className="disabled-cover"></span>
                    <div className="context-tooltip">
                        <img className="item-menu-img noselect" src={imgStyleOption} style={this.styleControl} onClick={() => {
                            ActionLogger.click('User_Editor_ItemLineMenuButton', 'Show_All_Style_Line');
                            this.onClickshowSubMenu("style")}} alt="" />
                    </div>
                    <ItemLineMenuButtonStyleOption />
                </div>
                <div className={(this.state.showSubMenu === "text" ? "context-control actived" : "context-control")}>
                    <span className="disabled-cover"></span>
                    <div className="context-tooltip">
                        <img className="item-menu-img noselect" src={imgContextText} style={this.styleControl} onClick={() => {
                            ActionLogger.click('User_Editor_ItemLineMenuButton', 'Show_All_Text_Line');
                            this.onClickshowSubMenu("text")}} alt="" />
                    </div>
                    <ItemLineMenuButtonTextOption />
                </div>
                <div className="split-line mx-2px"></div>
                <div className={'context-control open-delete-menu'}>
                    <div className="context-tooltip">
                        <img className="item-menu-img noselect" src={itemLineCloseOption} style={this.styleControl} onClick={this.itemLineDeleteButtonClickHandler} alt="" />
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemLineMenuButton;