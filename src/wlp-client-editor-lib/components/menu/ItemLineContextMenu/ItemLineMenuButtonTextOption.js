import React, { Component } from 'react';

import {
    downText,
    upText,
    textCenter,
    textLeft,
    textRight,

} from 'wlp-client-editor-lib/utils/ResourceIconUtils';

import KItemTextAlign from 'wlp-client-editor-lib/core/KItemTextAlign';
import KItemTextSizeAction from 'wlp-client-editor-lib/core/KItemTextSizeAction';
import ItemLineMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemLineMenuButtonBarEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';

const itemLineMenuButtonBarEventCenter = new ItemLineMenuButtonBarEventCenter();
class ItemLineMenuButtonTextOption extends Component {
    onClickchangeTextSize = (isIncrease) => {
        ActionLogger.click('User_Editor_ItemLineMenuButton', 'Change_Text_Size_Button');
        itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_TEXT_SIZE_CHANGE, isIncrease);
    }

    onClickchangeTextAlign = (align) => {
        ActionLogger.click('User_Editor_ItemLineMenuButton', 'Change_Text_Align_Button');
        itemLineMenuButtonBarEventCenter.fireEvent(itemLineMenuButtonBarEventCenter.ITEM_LINE_MENU_BUTTON_BAR_TEXT_HORIZONTAL_ALIGN_CHANGE, align);
    }

    render() {
        let defaultClassName = "context-action cursor-click btn";
        const textSize = [
            {
                img: downText,
                className: defaultClassName,
                action: KItemTextSizeAction.DECREASE
            },
            {
                img: upText,
                className: defaultClassName,
                action: KItemTextSizeAction.INCREASE
            }
        ]

        const textAlign = [
            {
                img: textLeft,
                className: defaultClassName,
                action: KItemTextAlign.LEFT
            },
            {
                img: textCenter,
                className: defaultClassName,
                action: KItemTextAlign.CENTER
            },
            {
                img: textRight,
                className: defaultClassName,
                action: KItemTextAlign.RIGHT
            }
        ];

        return (
            <div className="menu-control-action context-menu-style3">
                <div className="context-item-container">
                    <div className="btn-group btn-group-ed">
                        {textSize.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => this.onClickchangeTextSize(option.action)}>
                                <img className="control-item noselect" src={option.img} alt="" />
                            </div>
                        })}
                    </div>
                    <div className="split-line mx-2px"></div>
                    <div className="btn-group btn-group-ed">
                        {textAlign.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => this.onClickchangeTextAlign(option.action)} data-textalign={option.action}>
                                <img className="control-item noselect" src={option.img} alt="" />
                            </div>
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemLineMenuButtonTextOption;