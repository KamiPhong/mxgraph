import React, { Component } from 'react';
import i18n from "i18n";
import CommonModal from "wlp-client-common/component/modals/CommonModal";
import alertIcon001 from 'wlp-client-common/images/alertIcon001.png';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
import Logger from 'wlp-client-common/Logger';
import ActionLogger from 'wlp-client-common/ActionLogger';
const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();

class ItemMenuButtonProperties extends Component {

    constructor(props) {
        super(props);

        this.state = {
            password: "",
            rePassword: "",
            enableBtnSettingPassword: false,
            showConfirmModal: false
        }
    }

    onClickSettingPassword = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Setting_Password_Attach_File_Button');
        const {enableBtnSettingPassword, password, rePassword} = this.state;
        if (enableBtnSettingPassword === true && password !== "" && password === rePassword) {
            e.preventDefault();
            itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_PROPERTY_SETTING_PASSWORD, password);
            this.setState({
                password: "",
                rePassword: "",
                enableBtnSettingPassword: false
            })
        }
    }

    onClickRemovePassword = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Remove_Password_Attach_File_Button');
        e.preventDefault();
        this.setState({
            showConfirmModal: true
        }) 
    }

    passwordOnChange = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Password_Attach_File_Input');
        Logger.logConsole(e.target.value)
        this.setState({
            password: e.target.value
        }, () => this.validatePassword())
    }

    rePasswordOnChange = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Re_Password_Attach_File_Input');
        Logger.logConsole(e.target.value)
        this.setState({
            rePassword: e.target.value
        }, () => this.validatePassword())
        
    }

    validatePassword = () => {
        const {password, rePassword} = this.state;
        Logger.logConsole({password, rePassword})
        this.setState({
            enableBtnSettingPassword: (password !== "" && password === rePassword) ? true : false
        })
    }

    handleHideConfirmModal = () => {
        this.setState({
            showConfirmModal: false
        })
    }

    handleOkButton = () => {
        this.handleHideConfirmModal();
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_PROPERTY_REMOVE_PASSWORD);
    }

    renderConfirmModal = () => {
        return (<CommonModal
            show={true}
            backGroundClass="bg-modal bg-modal-editor"
            backDropModal="modal-action"
            headerClass=" "
            noBorderClass=" "
            classNameImage=" "
            onHide={this.handleHideConfirmModal}>
            <div className="ltext-center d-flex-center-fl d-flex-center-fl-editor mb-21px mb-21px-editor modal-alert">
                <img src={alertIcon001} alt="" className="d-inline-block img-alert img-alert-editor"/> <span
                className="fs-12 fs-12-editor line-height-1rem line-height-1rem-editor txt-alert txt-alert-editor">{i18n.t('editor.attach.clearPassword')}</span>
            </div>
            <div className="text-center modal-gr-btn modal-gr-btn-editor">
                <button type="submit" onClick={this.handleHideConfirmModal}
                        className="btn btn-editor btn-whitefl btn-whitefl-editor" >{i18n.t('global.cancel')}</button>
                <button type="submit" onClick={this.handleOkButton} className="btn btn-editor btn-black btn-black-editor">{i18n.t('global.ok')}</button>
            </div>
           
        </CommonModal>);
    }

    render() {
        return (
            <div className="menu-control-action item-properties" id="itemProperties">
                {this.state.showConfirmModal && this.renderConfirmModal()}
                <div className="property-container">
                    <div className="title-property item-title-property noselect"><strong>{i18n.t('editor.itemPropertiy.base.itemLabel')}</strong></div>
                    <hr className="border-bottom-solid-1 border-hr" />
                    <div className="d-flex mt-5px mt-5px-editor noselect">
                        <div className="col-70px label-property">
                        <strong>{i18n.t('editor.itemPropertiy.base.createDate')}</strong>
                        </div>
                        <div className="fill-data property-created-on"></div>
                    </div>
                    <div className="d-flex mt-2px mt-2px-editor">
                        <div className="col-70px label-property noselect">
                            <strong>{i18n.t('editor.itemPropertiy.base.updateDate')}</strong>
                        </div>
                        <div className="fill-data property-Last-update noselect"></div>
                    </div>
                    <div className="attachFile-content">
                        <div className="title-property mt-12px"><strong>{i18n.t('editor.itemPropertiy.attachfile.label')}</strong></div>
                        <hr className="border-bottom-solid-1 mb-5px border-hr" />
                        <div className="d-flex mt-5px mt-5px-editor noselect">
                            <div className="col-70px label-property noselect">
                                <strong>{i18n.t('editor.itemPropertiy.attachfile.name')}</strong>
                            </div>
                            <div className="fill-data file-name"></div>
                        </div>
                        <div className="d-flex mt-2px mt-2px-editor">
                            <div className="col-70px label-property noselect">
                                <strong>{i18n.t('editor.itemPropertiy.attachfile.type')}</strong>
                            </div>
                            <div className="fill-data file-extension"></div>
                        </div>
                        <div className="d-flex mt-2px mt-2px-editor mb-10px mb-10px-editor">
                            <div className="col-70px label-property noselect">
                                <strong>{i18n.t('editor.itemPropertiy.attachfile.size')}</strong>
                            </div>
                            <div className="fill-data file-size"></div>
                        </div>
                        <hr className="border-bottom-dotted-1 border-hr mb-important-11px" />
                        <div className="setting-password control-property">
                            <div className="col-145px mx-auto mb-8px">
                                <div className="input-password-label"></div>
                                <input type="password" value={this.state.password} onChange={(e) => this.passwordOnChange(e)} className="form-control form-control-shadow form-control-editor form-control-shadow-editor form-control-ed form-control-ed-editor form-control-input-password" placeholder={i18n.t('editor.itemPropertiy.password.enter')} />
                            </div>
                            <div className="col-145px mx-auto mb-8px">
                                <div className="input-password-label"></div>
                                <input type="password" value={this.state.rePassword} onChange={(e) => this.rePasswordOnChange(e)} className="form-control form-control-shadow form-control-editor form-control-shadow-editor form-control-ed form-control-ed-editor form-control-input-password" placeholder={i18n.t('editor.itemPropertiy.password.reEnter')} />
                            </div>
                            <div className="col-128px mx-auto control-property">
                                <button className="btn-property btn-reset btn-reset-editor w-100" disabled={this.state.enableBtnSettingPassword ? false : true} onClick={(e) => this.onClickSettingPassword(e)}>{i18n.t('editor.itemPropertiy.password.setting')}</button>
                            </div>
                        </div>
                        <div className="remove-password">
                        <div className="d-flex mt-10px mb-10px mb-10px-editor">
                            <div className="col-70px label-property">
                                <strong>{i18n.t('editor.itemPropertiy.password.entered')}</strong>
                            </div>
                           
                        </div>
                        <div className="col-128px mx-auto control-property">
                                <button className="btn-property btn-reset btn-reset-editor w-100" onClick={(e) => this.onClickRemovePassword(e)}>{i18n.t('editor.itemPropertiy.password.remove')}</button>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default ItemMenuButtonProperties;