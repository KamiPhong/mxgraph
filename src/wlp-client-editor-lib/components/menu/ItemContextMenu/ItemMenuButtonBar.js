import React, { Component } from 'react';

import { imgClose } from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import ItemMenuButton from './ItemMenuButton';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';
const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();

class ItemMenuButtonBar extends Component {

    menuCreateBaseItemClick = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Icon_Remove_Item');
        e.preventDefault();
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_REMOVE);
    }

    render = () => {
        const className = "pos-abs vertext-context zindex-1";
        return (
            <React.Fragment>
                <div className={className} id="itemMenuButtonBar" style={{ display: 'none', top: "0px" }} >
                    <div id="itemMenuBorderTopGroupItem"></div>
                    <div id="itemMenuBorderRightGroupItem"></div>
                    <div id="itemMenuBorderBottomGroupItem"></div>
                    <div id="itemMenuBorderLeftGroupItem"></div>
                    <div className="vertex-control" id="itemMenuControl">
                            <ItemMenuButton />
                    </div>
                    <img
                        src={imgClose}
                        onClick={this.menuCreateBaseItemClick}
                        id="itemMenuButtonDelete"
                        className="button-remove vertex-close pos-abs noselect"
                        alt=""
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default ItemMenuButtonBar;