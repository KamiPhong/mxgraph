import React from 'react';

export default class ItemMenuIcon extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            disableTooltip: false
        }

        this.hideToolTipTimer = null; 
    }

    _onClickHandler = () => {
        if (typeof this.props.onClick === 'function') {
            this.props.onClick();
        }

        this.setState({ disableTooltip: true });
    }

    _onMouseLeaveHandler = () => {
        if (this.hideToolTipTimer) {
            clearTimeout(this.hideToolTipTimer);
        }
        if (this.state.disableTooltip) {
            this.setState({ disableTooltip: false });
        }
    }

    onMouseEnterHandler = () => {
        this.setTimeOutShowTooltip();
    }

    setTimeOutShowTooltip = () => {
        if(!this.state.disableTooltip) {
            this.hideToolTipTimer = setTimeout(() => {
                this.setState({ disableTooltip: true });
            }, 10000);
        }
    }

    componentWillUnmount() {
        if (this.hideToolTipTimer) {
            clearTimeout(this.hideToolTipTimer);
        }
    }

    render() {
        const { disableTooltip } = this.state;
        const { imgSrc, imgStyle, ariaLabel, id } = this.props;
        let classes = ["context-tooltip noselect"];
        if (!disableTooltip) {
            classes.push("context-tooltip-able");
        }

        return (
            <React.Fragment>
                <span className="disabled-cover"></span>
                <div className={classes.join(' ')} id={id} aria-label={ariaLabel} onMouseLeave={this._onMouseLeaveHandler} onMouseEnter={this.onMouseEnterHandler}>
                    {this.props.children ? this.props.children : <img className={"item-menu-img noselect"} src={imgSrc} style={imgStyle} onClick={this._onClickHandler} alt="" />}
                </div>
            </React.Fragment>
        )
    }
}
