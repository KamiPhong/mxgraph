import React, { Component } from 'react';

import {
    downText,
    upText,
    textBottom,
    textCenter,
    textLeft,
    textMiddle,
    textRight,
    textTop,
} from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import KItemTextAlign from 'wlp-client-editor-lib/core/KItemTextAlign';
import KItemTextVerticalAlign from 'wlp-client-editor-lib/core/KItemTextVerticalAlign';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
import KItemTextSizeAction from 'wlp-client-editor-lib/core/KItemTextSizeAction';
import ActionLogger from 'wlp-client-common/ActionLogger';

const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();

class ItemMenuButtonTextOption extends Component {
    onClickchangeTextSize = (action) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Change_Text_Size_Button');
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_TEXT_SIZE_CHANGE, action);
    }

    onClickchangeTextAlign = (align) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Change_Text_Align_Button');
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_TEXT_ALIGN_CHANGE, align);
    }

    onClickchangeTextVerticalAlign = (verticalAlign) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Change_Text_Vertical_Align_Button');
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_TEXT_VERTICAL_ALIGN_CHANGE, verticalAlign);
    }

    defaultClassName = "context-action cursor-click btn";

    textSize = [
        {
            img: downText,
            className: this.defaultClassName,
            action: KItemTextSizeAction.DECREASE
        },
        {
            img: upText,
            className: this.defaultClassName,
            action: KItemTextSizeAction.INCREASE
        }
    ]

    textAlign = [
        {
            img: textLeft,
            className: this.defaultClassName,
            action: KItemTextAlign.LEFT
        },
        {
            img: textCenter,
            className: this.defaultClassName,
            action: KItemTextAlign.CENTER
        },
        {
            img: textRight,
            className: this.defaultClassName,
            action: KItemTextAlign.RIGHT
        }
    ];

    textVerticalAlign = [
        {
            img: textTop,
            className: this.defaultClassName + " action-baseitem-default",
            action: KItemTextVerticalAlign.TOP
        },
        {
            img: textMiddle,
            className: this.defaultClassName + " action-baseitem-default",
            action: KItemTextVerticalAlign.MIDDLE
        },
        {
            img: textBottom,
            className: this.defaultClassName + " action-baseitem-default",
            action: KItemTextVerticalAlign.BOTTOM
        },
    ];

    render() {
        return (
            <div className="menu-control-action context-menu-style3">
                <div className="context-item-container">
                    <div className="btn-group btn-group-ed">
                        {this.textSize.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => this.onClickchangeTextSize(option.action)}>
                                <img className="control-item noselect" src={option.img} alt="" />
                            </div>
                        })}
                    </div>
                    <div className="split-line mx-2px"></div>
                    <div className="btn-group btn-group-ed">
                        {this.textAlign.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => this.onClickchangeTextAlign(option.action)} data-textalign={option.action}>
                                <img className="control-item noselect" src={option.img} alt="" />
                            </div>
                        })}
                    </div>
                    <div className="split-line mx-2px baseitem-option"></div>
                    <div className="baseitem-option">
                        <div className="btn-group btn-group-ed">
                            {this.textVerticalAlign.map((option, i) => {
                                return <div key={i} className={option.className} onClick={() => this.onClickchangeTextVerticalAlign(option.action)} data-textvalign={option.action}>
                                    <img className="control-item noselect" src={option.img} alt="" />
                                </div>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemMenuButtonTextOption;