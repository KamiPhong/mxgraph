import React, { Component } from 'react';

import {
    imgStyleA,
    imgStyleB,
    imgStyleC,
    imgStyleD,
    imgStyleE,
    imgStyleF,
    imgStyleG,
    imgStyleH,
    imgStyleI,
    imgStyleJ,
    imgStyleBA,
    imgStyleBB,
    imgStyleBC,
    imgStyleBD,
    imgStyleBE,
} from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import KItemStyle from 'wlp-client-editor-lib/core/KItemStyle';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';
const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();

class ItemMenuButtonStyleOption extends Component {
    onClickchangeStyle = (style) => {
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_STYLE_CHANGE, style);
    }

    defaultClassName = "context-action cursor-click";

    options = [
        { img: imgStyleA, style: KItemStyle.A, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleF, style: KItemStyle.F, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleB, style: KItemStyle.B, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleG, style: KItemStyle.G, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleC, style: KItemStyle.C, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleH, style: KItemStyle.H, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleD, style: KItemStyle.D, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleI, style: KItemStyle.I, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleE, style: KItemStyle.E, className: this.defaultClassName + " action-item-default" },
        { img: imgStyleJ, style: KItemStyle.J, className: this.defaultClassName + " action-item-default" }
    ];

    baseItemOptions = [
        { img: imgStyleBA, style: KItemStyle.B_A, className: this.defaultClassName + " action-baseitem-default" },
        { img: imgStyleBB, style: KItemStyle.B_B, className: this.defaultClassName + " action-baseitem-default" },
        { img: imgStyleBC, style: KItemStyle.B_C, className: this.defaultClassName + " action-baseitem-default" },
        { img: imgStyleBD, style: KItemStyle.B_D, className: this.defaultClassName + " action-baseitem-default" },
        { img: imgStyleBE, style: KItemStyle.B_E, className: this.defaultClassName + " action-baseitem-default" },
    ]

    render() {
        const styleImg = {
            width: 24 * this.props.scale + "px"
        }
        return (
            <div className="menu-control-action context-menu-style2">
                <div className="item-options">
                    <div className="context-item-container">
                        {this.options.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => {
                                ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Item_Change_Style_Button');
                                this.onClickchangeStyle(option.style)
                                }} data-style={option.style}>
                                <img className="control-item noselect" src={option.img} style={styleImg} alt="" />
                            </div>
                        })}
                    </div>
                </div>

                <div className="baseitem-option">
                    <div className="context-item-container">
                        {this.baseItemOptions.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => {
                                ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Baseitem_Change_Style_Button');
                                this.onClickchangeStyle(option.style)
                                }} data-style={option.style}>
                                <img className="control-item noselect" src={option.img} style={styleImg} alt="" />
                            </div>
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemMenuButtonStyleOption;