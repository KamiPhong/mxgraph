import React, { Component } from 'react';

import {
    imgShapeOption,
    imgContextText,
    imgStyleOption,
    imgContextUpload,
    imgContextDownload,
    imgContextSetting,
} from 'wlp-client-editor-lib/utils/ResourceIconUtils';

import ItemMenuButtonShapeOption from './ItemMenuButtonShapeOption';
import ItemMenuButtonStyleOption from './ItemMenuButtonStyleOption';
import ItemMenuButtonTextOption from './ItemMenuButtonTextOption';
import FileConfig from 'wlp-client-common/config/FileConfig';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
import ItemMenuButtonProperties from './ItemMenuButtonProperties';
import i18n from "i18n";
import LoadingProgress from './LoadingProgress';
import ItemMenuIcon from './ItemMenuIcon';
import FileService from 'wlp-client-service/service/FileService';
import ActionLogger from 'wlp-client-common/ActionLogger';

const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();
const fileService = new FileService();
class ItemMenuButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSubMenu: "",
            loading: false,
            uploadDone: false,
        }
        this.styleControl = {
            width: 18 * this.props.scale + 'px'
        }
        this.itemUploadFile = React.createRef();

        itemMenuButtonBarEventCenter.addListener(itemMenuButtonBarEventCenter.ITEM_MENU_DO_CLOSE, this.hideSubMenu);
    }

    onClickshowSubMenu = (type) => {
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_CLICKED, type);
        
        this.setState({
            showSubMenu: this.state.showSubMenu === type ? null : type
        });
    }

    hideSubMenu = () => {
        this.setState({ showSubMenu: null });
    }

    handleDownloadFile = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Item_Dowload_File_Button');
        e.preventDefault();
        this.onClickshowSubMenu("download");
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_DOWNLOAD_FILE);
    }

    handleSelectFile = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Item_Select_File_Button');
        e.preventDefault();
        this.onClickshowSubMenu("download")

        this.itemUploadFile.current.click();
    }

    itemUploadFileClickHandler = (e) => {
        ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Item_Upload_File_Button');
        e.preventDefault();
        let file = e.target.files[0];

        if (file) {
            this.uploadFile(file);
        }
    }

    uploadFile = async (file) => {
        if (file.size > FileConfig.maxFileUploadSize) {
            return showWarningDialog('attachfile.oversize.error');
        }
        
        if (FileConfig.bannedFileNameChars.test(file.name)) {
            return showWarningDialog('editor.w0004');
        }

        this.setState({
            loading: true,
        })

        let formData = new FormData();
        formData.append('file', file);

        fileService.fileUploadForm(formData).then(uploadResult => {
            itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_UPLOAD_FILE_COMPLETED, uploadResult);
            this.hiddenLoading();
        });
    }

    handleUploadFileResult = (uploadResult) => {
        console.log("uploadResult", uploadResult)
        this.hiddenLoading();
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_UPLOAD_FILE_COMPLETED, uploadResult);
    }

    hiddenLoading = () => {
        let that = this;
        this.setState({
            uploadDone: true
        }, () => {
            setTimeout(() => that.setState({
                loading: false,
                uploadDone: false,
            }), 400);
        })
    }


    render() {
        return (
            <div className="d-flex item-fix-line-height">
                <div className={(this.state.showSubMenu === "shape" ? "context-control actived" : "context-control") + ' open-shape-menu'}>
                    <ItemMenuIcon
                        ariaLabel={i18n.t('editor.itemMenu.shape')}
                        imgSrc={imgShapeOption}
                        imgStyle={this.styleControl}
                        onClick={() => {
                            ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Show_Sub_Menu_Shape');
                            this.onClickshowSubMenu("shape")}}
                    />
                    <ItemMenuButtonShapeOption />
                </div>
                <div className={(this.state.showSubMenu === "style" ? "context-control actived" : "context-control") + ' open-style-menu'}>
                    <ItemMenuIcon
                        ariaLabel={i18n.t('editor.itemMenu.style')}
                        imgSrc={imgStyleOption}
                        imgStyle={this.styleControl}
                        onClick={() => {
                            ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Show_Sub_Menu_Style');
                            this.onClickshowSubMenu("style")}}
                    />
                    <ItemMenuButtonStyleOption />
                </div>
                <div className={(this.state.showSubMenu === "text" ? "context-control actived" : "context-control") + ' open-text-menu'}>
                    <ItemMenuIcon
                        ariaLabel={i18n.t('editor.itemMenu.text')}
                        imgSrc={imgContextText}
                        imgStyle={this.styleControl}
                        onClick={() => {
                            ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Show_Sub_Menu_Text');
                            this.onClickshowSubMenu("text")}}
                    />
                    <ItemMenuButtonTextOption />
                </div>
                <div className="split-line mx-2px"></div>
                <div className="context-control open-upload-image">
                    <ItemMenuIcon
                        ariaLabel={i18n.t('editor.itemMenu.file')}
                        id="download-control"
                    >
                        <input onChange={(e) => this.itemUploadFileClickHandler(e)} type="file" value="" id="item-upload-file" style={{visibility:"hidden", width: 0, height: 0, position: "absolute"}} ref={this.itemUploadFile} />
                        <img className="item-menu-img noselect" id="itemMenuUploadImg" src={imgContextUpload} style={this.styleControl} onClick={(e) => this.handleSelectFile(e)} alt="" />
                        <img className="item-menu-img noselect" id="itemMenuDownloadImg" src={imgContextDownload} style={this.styleControl} onClick={(e) => this.handleDownloadFile(e)} alt="" />
                    </ItemMenuIcon>
                </div>
                <div className={(this.state.showSubMenu === "property" ? "context-control property-control actived" : "context-control property-control") + ' open-property-menu'}>
                    <ItemMenuIcon
                        ariaLabel={i18n.t('editor.itemMenu.property')}
                        imgSrc={imgContextSetting}
                        imgStyle={this.styleControl}
                        onClick={() => {
                            ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Show_Sub_Menu_Setting');
                            this.onClickshowSubMenu("property")}}
                    />
                    <ItemMenuButtonProperties />
                </div>
                {this.state.loading && <LoadingProgress uploadDone={this.state.uploadDone} /> }
            </div>
        )
    }
}

export default ItemMenuButton;