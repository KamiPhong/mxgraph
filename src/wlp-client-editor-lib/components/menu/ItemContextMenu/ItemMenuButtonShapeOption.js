import React, { Component } from 'react';

import {
    imgRectangle,
    imgEllipse,
    imgRectangleRight,
    imgRectangleLeft,
    imgRhombus,
    imgCloud,
    imgEllipseDraw,
    imgRectangleDraw,
    imgBaseTriangle,
    imgBaseRect,
} from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import KGraphItemShape from 'wlp-client-editor-lib/core/KGraphItemShape';
// import KItemStyle from '../../core/KItemStyle';
import ItemMenuButtonBarEventCenter from 'wlp-client-editor-lib/events/ItemMenuButtonBarEventCenter';
import ActionLogger from 'wlp-client-common/ActionLogger';
const itemMenuButtonBarEventCenter = new ItemMenuButtonBarEventCenter();

class ItemMenuButtonShapeOption extends Component {
    onClickchangeShape = (shape) => {
        itemMenuButtonBarEventCenter.fireEvent(itemMenuButtonBarEventCenter.ITEM_MENU_BUTTON_BAR_SHAPE_CHANGE, shape);
    }

    defaultClassName = "context-action cursor-click";

    options = [
        {
            img: imgRectangle,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.RECT
        },
        {
            img: imgEllipse,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.ELLIPSE
        },
        {
            img: imgRectangleRight,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.SHAPE_ARROW_RIGHT
        },
        {
            img: imgRectangleLeft,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.SHAPE_ARROW_LEFT
        },
        {
            img: imgRhombus,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.DIAMOND
        },
        {
            img: imgCloud,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.CLOUD
        },
        {
            img: imgEllipseDraw,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.HANDWRITING_ELLIPSE
        },
        {
            img: imgRectangleDraw,
            className: this.defaultClassName + " action-item-default",
            shape: KGraphItemShape.HANDWRITING_RECT
        }
    ];

    baseItemOptions = [

        {
            img: imgBaseRect,
            className: this.defaultClassName + " action-baseitem-default",
            shape: KGraphItemShape.BASE_RECT
        },
        {
            img: imgRectangle,
            className: this.defaultClassName + " action-baseitem-default",
            shape: KGraphItemShape.BASE_ROUND_RECT
        },
        {
            img: imgBaseTriangle,
            className: this.defaultClassName + " action-baseitem-default",
            shape: KGraphItemShape.BASE_TRIANGLE
        },
        {
            img: imgEllipse,
            className: this.defaultClassName + " action-baseitem-default",
            shape: KGraphItemShape.BASE_ELLIPSE
        }
    ]

    render() {
        return (
            <div className="menu-control-action context-menu-style1">
                <div className="context-item-container">
                    <div className="item-options">
                        {this.options.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => {
                                ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Item_Change_Shape_Button');
                                this.onClickchangeShape(option.shape)
                                }} data-shape={option.shape}>
                                <img className="control-item noselect" src={option.img} alt="" />
                            </div>
                        })}
                    </div>

                    <div className="baseitem-option">
                        {this.baseItemOptions.map((option, i) => {
                            return <div key={i} className={option.className} onClick={() => {
                                ActionLogger.click('User_Editor_ItemMenuButtonBar', 'Baseitem_Change_Shape_Button');
                                this.onClickchangeShape(option.shape)
                                }} data-shape={option.shape}>
                                <img className="control-item noselect" src={option.img} alt="" />
                            </div>
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemMenuButtonShapeOption;