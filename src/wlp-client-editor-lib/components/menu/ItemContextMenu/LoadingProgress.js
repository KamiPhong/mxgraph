import React from 'react';
import { iconLoadingProgress } from 'wlp-client-editor-lib/utils/ResourceIconUtils';
import LoadModalBodyEnd from 'wlp-client-common/component/LoadModalBodyEnd';
class LoadingProgress extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 90,
            countDone: 0
        }
    }
    
    componentDidMount() {
        this.inter = setInterval(() => {
            if (this.props.uploadDone === true) {
                clearInterval(this.inter);
                this.setState({
                    countDone: 100
                }); 
            } else if (this.state.count < 99) {
                this.setState((prevState) => ({count: prevState.count + 1})); 
            }
        }, 300);
    }
    
    componentWillUnmount() {
        clearInterval(this.inter);
        this.setState({
            countDone: false,
            count: 90,
        })
    }
    
    render() {
      return (<LoadModalBodyEnd>
        <div className="loading-spiner-w loading-spiner-w-editor noselect">
            <div className="align-items-center d-flex h-100 justify-content-center w-100">
                <div className="spinner-w spinner-w-editor">
                    <div className="spinner-icon spinner-icon-editor"><img src={iconLoadingProgress} alt="" className="img-fluid" /></div>
                    <div className="spinner-number spinner-number-editor">{this.state.countDone ? this.state.countDone :this.state.count}%</div>
                </div>
            </div>
           
        </div>
        </LoadModalBodyEnd>
      )
    }
  }
  export default LoadingProgress;