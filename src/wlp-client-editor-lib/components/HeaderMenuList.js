import React from 'react';
import { withTranslation } from 'react-i18next';
import PulldownButton from './PulldownButton';
import Link from 'wlp-client-common/component/Link';
import MainSkinContext from "../../wlp-client-editor/MainSkinContext";
import AvatarUtil from "./AvatarUtil";
import ResponseDto from "../../wlp-client-service/dto/ResponseDto";
import LocalStorageUtils from "../../wlp-client-common/utils/LocalStorageUtils";
import AuthService from "../../wlp-client-service/service/AuthService";
import MypageRequestDto from "../../wlp-client-service/dto/MypageRequestDto";
import CommonModal from "../../wlp-client-common/component/modals/CommonModal";
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import ActionLogger from 'wlp-client-common/ActionLogger';
import AccountPullDownListEventCenter from 'wlp-client-editor-lib/events/AccountPullDownListEventCenter';

const avatarUtil = new AvatarUtil();
const languageUtil = new WlpLanguageUtil();
const strageUtils = new LocalStorageUtils();
const userInfo = strageUtils.getUserSession();
const accountPullDownListEventCenter = new AccountPullDownListEventCenter();
class HeaderMenuList extends React.Component{
    static contextType = MainSkinContext;
    state = {
        isShowLogoutAlert: false
    };
    setShowLogoutAlert = (value) => {
        this.setState({
            isShowLogoutAlert: value
        })
    }
    logoutBtn_clickHandle = () => {
        ActionLogger.click('User_Editor_Header', 'Logout_Button');
        this.setShowLogoutAlert(true)
    }
    renderHeaderMenuItems = () => {
        const { t } = this.props;
        return (<React.Fragment>
            <Link className="dropdown-item dropdown-item-editor noselect" onClick={this.logoutBtn_clickHandle}>{t('global.headerMenu.logout')}</Link>
        </React.Fragment>)
    };
    callLogout = async () => {
        accountPullDownListEventCenter.fireEvent(accountPullDownListEventCenter.ACCOUNT_PULLDOWN_LOG_OUT)
        const authService = new AuthService();
        const result = await authService.logoff(new MypageRequestDto());
        this.callLogout_handleResult(result);
    }

     callLogout_handleResult = (result = new ResponseDto()) => {
        const localStorageUtils = new LocalStorageUtils();
        localStorageUtils.deleteUserSession();
        window.location.reload();
    }
    renderPopupLogout = () => {
        const { t } = this.props;
         return <CommonModal
             show={true}
             backGroundClass="bg-modal bg-modal-editor"
             backDropModal="modal-action"
             headerClass=" "
             noBorderClass=" "
             classNameImage=" "
             onHide={() => this.setShowLogoutAlert(false)}>
             <div className="text-center d-flex-center-fl d-flex-center-fl-editor mb-21px mb-21px-editor modal-alert">
                 <span className="fs-12 fs-12-editor line-height-1rem line-height-1rem-editor txt-alert txt-alert-editor white-space-preline white-space-preline-editor text-left">{t('message.global.c0003')}</span>
             </div>
             <div className="text-center modal-gr-btn modal-gr-btn-editor">
                 <button type="submit" onClick={() => this.setShowLogoutAlert(false)}
                         className="btn btn-whitefl btn-editor btn-whitefl-editor">{t('global.cancel')}</button>
                 <button type="submit" onClick={this.callLogout}
                         className="btn btn-black btn-editor btn-black-editor">{t('global.ok')}</button>
             </div>
         </CommonModal>;
    }
    render() {
        const {logonDto} = this.context;
        const { hideLogoutFlg } = this.props;

        const userName = userInfo.sessionDto ? languageUtil.getUserNameByLanguage(userInfo.sessionDto.firstName, userInfo.sessionDto.lastName) : ``;
        const userNickName = userInfo.sessionDto && (!userInfo.sessionDto.nickName || userInfo.sessionDto.nickName.length === 0) ? userName : userInfo.userName;
        return (<React.Fragment>
            <div onClick={() => {
                ActionLogger.click('User_Editor_Header', 'Avatar_Button');
            }}>
                {hideLogoutFlg ?
                    <div className={"dropdown dropdown-editor dropdown-gr dropdown-gr-editor dropdown-cpn dropdown-cpn-editor dropdown-profile dropdown-profile-editor show show-editor"}>
                        <Link innerRef={this.buttonContainer} className={" dropdown-toggle dropdown-toggle-editor btn-sm btn-dark-dropd btn-dark-dropd-editor dropdown-btn-editor no-cursor no-cursor-editor"}>
                            <img className="mr-2 dropdown-profile-img dropdown-profile-img-editor noselect" width="21" height="21" src={avatarUtil.getSource(logonDto.smallImageUrl)} alt="" />
                            <span className="text-truncate text-truncate-editor noselect">{userNickName}</span>
                        </Link>
                    </div>:
                <PulldownButton
                    img={avatarUtil.getSource(logonDto.smallImageUrl)}
                    title={userNickName}
                    classDropDown="dropdown-profile dropdown-profile-editor show show-editor"
                    classDropDownMenu="dropdown-px-16 dropdown-px-16-editor"
                    classLink="dropdown-btn-editor"
                    pullDownItems={this.renderHeaderMenuItems()} />
                }
                {
                    this.state.isShowLogoutAlert && this.renderPopupLogout()
                }
            </div>
        </React.Fragment>);
    }
}

export default withTranslation()(HeaderMenuList);
