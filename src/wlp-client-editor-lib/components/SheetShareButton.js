import React, { Component} from 'react';
import { withTranslation } from 'react-i18next';
import propTypes from 'prop-types';
import SheetLinkLine from './SheetShareButton/SheetLinkLine';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';
import ActionLogger from 'wlp-client-common/ActionLogger';
import LoadModalBodyEnd from 'wlp-client-common/component/LoadModalBodyEnd';

class SheetShareButton extends Component {

    state = {
        top: 0,
        isShowPopupShare: false
    }

    buttonContainer = React.createRef();
    bodyShare = React.createRef();

    hidePopupShare = (cb) => {
        if(typeof(cb) !== 'function'){
            cb = () => {};
        }

        this.setState({ isShowPopupShare: false }, cb);
    }

    showPopupShare = (cb) => {
        if(typeof(cb) !== 'function'){
            cb = () => {};
        }
        this.setState({ isShowPopupShare: true }, cb);
    }

    toggleDropdownShare = () => {
        ActionLogger.click('User_Editor_SheetView', 'Share_Popup_Button');
        const { onShow, onHide } = this.props;
        const { isShowPopupShare } = this.state;
        if(isShowPopupShare){
            this.hidePopupShare(onHide);
        }else {
            this.showPopupShare(onShow);
        }
    }

    handleDocumentClick = (event) => {
        let checkClickPopup = !checkElementClicked(event, this.bodyShare.current);
        let checkClickButtonShare = !checkElementClicked(event, this.buttonContainer.current);

        if(checkClickPopup && checkClickButtonShare){
            this.hidePopupShare();
        }   
    }

    updateDropdownListPosition = () => {
        if (this.buttonContainer.current) {
            const current = this.buttonContainer.current;
            const buttonBounds = current.getBoundingClientRect();
            
            // tính vùng hiển thị
            let top = buttonBounds.top + buttonBounds.height;

            this.setState({
                top: top,
            });
        }
    }

    componentDidMount() {
        this.updateDropdownListPosition();
        document.addEventListener('mousedown', this.handleDocumentClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleDocumentClick);
    }

    componentDidUpdate(prevProp, prevState) {
        if(this.state.isShowPopupShare === true && this.state.isShowPopupShare !== prevState.isShowPopupShare){
            this.updateDropdownListPosition();
        }
    }

    render() {
        const { t, pulldownIcon, currentSheet, mapInfo } = this.props;
        const { isShowPopupShare, top } = this.state;

        return (<React.Fragment>
            <div className="dropdown dropdown-editor fix-close-popup fix-close-popup-editor" ref={this.buttonContainer}>
                <button className={`btn btn-editor btn-dark-gr btn-dark-gr-editor btn-share btn-share-editor btn-shadow ${isShowPopupShare ? 'btn-hv-none btn-hv-none-editor' : ''}`}
                    id="dropdownMenuButton" onClick={this.toggleDropdownShare}>
                    <i className={`fa fa-editor fa-link fa-flip-horizontal fa-flip-horizontal-editor noselect ${isShowPopupShare ? 'citem-share citem-share-editor' : ''}`} aria-hidden="true"></i>
                    <span className="btn-share-txt btn-share-txt-editor noselect">{t('editor.link.buttonLabel')}</span>
                    <img src={pulldownIcon} alt="" className="ml-10px ml-10px-editor btn-share-arrow btn-share-arrow-editor noselect"/>
                </button>
            </div>
            <LoadModalBodyEnd>
                <div ref={this.bodyShare}>
                    <SheetLinkLine
                    isShow={isShowPopupShare}
                    currentSheet={currentSheet}
                    mapInfo={mapInfo}
                    top={top}
                />
                </div> 
            </LoadModalBodyEnd>
        </React.Fragment>)
    }
}

SheetShareButton.propTypes = {
    onShow: propTypes.func,
    onHide: propTypes.func
}

export default withTranslation()(SheetShareButton);