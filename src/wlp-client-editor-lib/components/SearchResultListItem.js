import React from 'react';
import SearchResultItem from './SearchResultItem';

class SearchResultListItem extends React.Component{
    render(){
        const { t, searchItems, valueSearch } = this.props;
        return (
            <div className="results-right results-right-editor">
                <ul className="results-list results-list-editor ul-reset ul-reset-editor">
                    { 
                        searchItems.map((item, index) => {
                            return (<SearchResultItem item={item} key={index} t={t} valueSearch={valueSearch}/>)
                        })
                    }
                </ul>
            </div>
        )
    }
}
export default SearchResultListItem;