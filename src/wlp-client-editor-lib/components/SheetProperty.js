import React from 'react';
import { withTranslation } from 'react-i18next';
import Link from 'wlp-client-common/component/Link';
import imageBG01 from 'wlp-client-common/images/icon/editBG01.png';
import imageBG02 from 'wlp-client-common/images/icon/editBG02.png';
import imageBG03 from 'wlp-client-common/images/icon/editBG03.png';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import i18n from "i18n";
import { SheetBG } from 'wlp-client-common/consts/BackgroundSheetConsts';
import InputUtils from 'wlp-client-common/utils/InputUtils';
import SheetNameConst from 'wlp-client-common/consts/SheetNameConst';
import SheetEventCenter from 'wlp-client-editor-lib/events/SheetEventCenter';
import {withMapContainerContext} from "./MapContainerContext";
import SheetViewEventCenter from "../events/SheetViewEventCenter";
import EditorConsts from "../../wlp-client-common/consts/EditorConsts";
import EditorClient from "../../wlp-client-common/utils/EditorClientUtils";
import SheetService from 'wlp-client-service/service/SheetService';
import ActionLogger from 'wlp-client-common/ActionLogger';

const inputUtils = new InputUtils();
const wlpDateUtil = new WlpDateUtil();
const sheetEventCenter = new SheetEventCenter();
const sheetViewEventCenter = new SheetViewEventCenter();
const sheetService = new SheetService();
class SheetProperty extends React.Component {
    
    constructor(props) {
        super(props);
        this.isSheetLocked = this.props.isSheetLocked;
        this.state = {
            sheetName: '',
            createTime: null,
            lastUpdateTime: '',
            lastEditUserName: '',
            createUserName: '',
            inputFocusFlag: false
        };
        this.sheetNameField = React.createRef();
        this.sheetUrlField = React.createRef();
    }
    
    closeSheetProperty = () => {
        ActionLogger.click('User_Editor_SheetProperties', 'Close_Button');
        sheetViewEventCenter.fireEvent(sheetViewEventCenter.DO_CLOSE_SHEET_PROPERTY);
    };

    updateSheetName = () => {
        const { currentSheet } = this.props.mapContainerContext;
            if(currentSheet && !this.isSheetLocked){
                currentSheet.name = this.state.sheetName;
                sheetEventCenter.fireEvent(sheetEventCenter.SHEET_UPDATE, currentSheet);
            }
    }

    handleOnChange = (event) => {
        ActionLogger.typing('User_Editor_SheetProperties', 'Sheet_Name_Input');
        const e = event;
        this.handleSetInputName(e, false);
    }

    handleKeyDown = (event) => {
        const e = event;
        if (e.key === 'Enter') {
            this.updateSheetName();
        }
    }

    handleSetInputName = (event, isEnter) => {
        const regex = SheetNameConst.SHEET_NAME_RESTRICT;
        const stateSheetName = this.state.sheetName;
        const isEnterPress = isEnter;

        const { selectionEnd } = event.currentTarget;
        const valSheetName = event.target.value;

        const isCharValid = valSheetName === '' || regex.test(valSheetName);

        if (valSheetName.length > 200) {
            this.setState(
                {
                    sheetName: stateSheetName
                }, () => {
                    inputUtils.setCaretPosition(this.sheetNameField.current, selectionEnd - 1);
                }
            );
        } else {
            if (!isCharValid) {
                this.setStateAndCursorPos(stateSheetName, isCharValid, selectionEnd, isEnterPress);
            } else {
                this.setStateAndCursorPos(valSheetName, isCharValid, selectionEnd, isEnterPress);
            }
            if (isEnterPress) {
                this.updateSheetName();
            }
        }
    }

    setStateAndCursorPos = (value, isCharValid, selectionEnd, isEnterPress) => {
        this.setState(
            {
                sheetName: value
            }, () => {
                let cursorPos = value.length;

                if (isCharValid) {
                    cursorPos = selectionEnd;
                } else {
                    cursorPos = selectionEnd - 1;
                }

                inputUtils.setCaretPosition(this.sheetNameField.current, cursorPos);
            }
        );
    }

    componentDidUpdate(oldProps) {
        const { currentSheet } = this.props.mapContainerContext;
        const { currentSheet: oldCurrentSheet } = oldProps.mapContainerContext;
        if(currentSheet !== oldCurrentSheet){
            let stateData = {...this.state};
            stateData.sheetName = currentSheet.name;
            stateData.createUserName = currentSheet.createUserName;
            stateData.lastUpdateTime = currentSheet.lastEditTime;
            stateData.lastEditUserName = currentSheet.lastEditUserName
            if (currentSheet.id !== oldCurrentSheet.id) {
                stateData.createTime = currentSheet.createTime;
            }
            this.setState(stateData);
        }
    }

    componentDidMount = () => {
        const { currentSheet } = this.props.mapContainerContext;
        this.doGetSheetInfo(currentSheet.id);
    }

    doGetSheetInfo = async (sheetId) => {
        const { setCurrentSheet } = this.props.mapContainerContext;
        const sheetInfo = await sheetService.getInfo(sheetId);
        if (sheetInfo) {
            setCurrentSheet(sheetInfo.sheet);
            this.setState({
                sheetName: sheetInfo.sheet.name,
                createTime: sheetInfo.sheet.createTime,
                lastUpdateTime: sheetInfo.sheet.lastEditTime,
                lastEditUserName: sheetInfo.sheet.lastEditUserName,
                createUserName: sheetInfo.sheet.createUserName
        });
        }

    }

    updateCurrentSheetBg = (sheetBg) => {
        ActionLogger.click('User_Editor_SheetProperties', 'Change_BackGround_Input');
        const { currentSheet } = this.props.mapContainerContext;

        currentSheet.backgroundUri = sheetBg;
        sheetEventCenter.fireEvent(sheetEventCenter.SHEET_UPDATE, currentSheet);
    }

    copyLinkShare = () => {
        ActionLogger.click('User_Editor_SheetProperties', 'Copy_Link_Button');
        const el = document.createElement('textarea');
        el.value = this.sheetUrlField.current.value;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        return false
    };

    updateNameOnblur = (evt) => {
        evt.target.offsetParent.focus();
        this.updateSheetName();
        this.setState({
            inputFocusFlag: false
        })
    }

    onFocusInput = (evt) => {
        evt.target.select();
        this.setState({
            inputFocusFlag: true
        })
    }

    handleOnPaste = (event) => {
        event.clipboardData.getData('text/plain').slice(0, EditorConsts.SHEET_NAME_MAXIMUM_CHARACTER);
    }

    render() {
        const { t } = this.props;
        const {currentSheet } = this.props.mapContainerContext;
        const localeCode = (i18n.language === LocaleConst.en_US) ? LocaleConst.en_US : LocaleConst.ja_JP;
        const {
            id,
            backgroundUri,
        } = currentSheet;
        const { lastUpdateTime, createTime, createUserName, lastEditUserName, inputFocusFlag } = this.state;
        const sheetUrl = process.env.REACT_APP_API_HOST + '/sheet/' + id;
        let inputClassname = inputFocusFlag ? 'form-control form-control-shadow form-control-editor form-control-shadow-editor form-control-ed form-control-ed-editor' : 'form-control form-control-shadow form-control-editor form-control-shadow-editor form-control-ed form-control-ed-editor noselect';
        let lableDateClasses  = ['float-l float-l-editor noselect'];
        if (!EditorClient.IS_SAFARI){
            lableDateClasses.push('fw-bold fw-bold-editor');
        }
        return (
            <div className={`properties-modal properties-modal-editor properties-modal-js show show-editor`}>
                <div className="sheet-properties sheet-properties-editor box-shadow-gr box-shadow-gr-editor no-cursor no-cursor-editor" tabIndex={0}>
                    <Link className="close-properties close-properties-editor btn-reset btn-reset-editor " onClick={this.closeSheetProperty}></Link>
                    <h2 className="sheet-properties-tlt sheet-properties-tlt-editor noselect">{t('editor.sheetProperty.sheetProperty')}</h2>
                    <div className="sheet-properties-ct sheet-properties-ct-editor">
                        <div className="border-bottom-dotted border-bottom-dotted-editor mb-10px mb-10px-editor">
                            <div className="sheet-properties-txt sheet-properties-txt-editor text-truncate text-truncate-editor"><div className={lableDateClasses.join(' ')}>{t('editor.sheetProperty.createDate')}</div>
                                <span className='lh-1-1 lh-1-1-editor noselect'>{createTime && wlpDateUtil.formatDateTime(createTime, localeCode)}</span></div>
                            <div className="ml-4px ml-4px-editor mb-9px mb-9px-editor text-truncate text-truncate-editor h-18px noselect">{createUserName && (`(${t('editor.sheetProperty.by')} ${createUserName})`)}</div>
                            <div className="sheet-properties-txt sheet-properties-txt-editor text-truncate text-truncate-editor"><div className={lableDateClasses.join(' ')}>{t('editor.sheetProperty.updateDate')}</div>
                                <span className='lh-1-1 lh-1-1-editor noselect'>{lastUpdateTime && wlpDateUtil.formatDateTime(lastUpdateTime, localeCode)}</span></div>
                            <div className="ml-4px ml-4px-editor pb-4px pb-4px-editor text-truncate text-truncate-editor h-22px noselect">{lastEditUserName && (`(${t('editor.sheetProperty.by')} ${lastEditUserName})`)}</div>
                        </div>

                        <input
                            type="text"
                            className={inputClassname}
                            value={this.state.sheetName}
                            onBlur={this.updateNameOnblur}
                            onFocus={this.onFocusInput}
                            onChange={this.handleOnChange}
                            onKeyDown={this.handleKeyDown}
                            readOnly={this.props.isSheetLocked}
                            ref={this.sheetNameField}
                            onPaste={this.handleOnPaste}
                            maxLength={EditorConsts.SHEET_NAME_MAXIMUM_CHARACTER}
                        />
                        <strong className='noselect'>{t('editor.sheetProperty.background')}</strong>
                        <div className="d-flex list-radio-bg list-radio-bg-editor">
                            {/* TODO: Refactor duplicate code  */}
                            <div className="custom-radio image-radio image-radio-editor">
                                <input type="radio" className="custom-control-input" id="checkimg1" name="ck2"
                                    onChange={(() => this.updateCurrentSheetBg(SheetBG.sheetBG1))} disabled={this.props.isSheetLocked}
                                    checked={!backgroundUri || backgroundUri === SheetBG.sheetBG1} />
                                <label className="custom-control-label custom-control-label-editor" htmlFor="checkimg1">
                                    <img className='noselect' src={imageBG01} alt="" />
                                </label>
                            </div>
                            <div className="custom-radio image-radio image-radio-editor">
                                <input type="radio" className="custom-control-input" id="checkimg2" name="ck2"
                                    onChange={(() => this.updateCurrentSheetBg(SheetBG.sheetBG2))} disabled={this.props.isSheetLocked}
                                    checked={backgroundUri === SheetBG.sheetBG2} />
                                <label className="custom-control-label custom-control-label-editor" htmlFor="checkimg2">
                                    <img className='noselect' src={imageBG02} alt="" />
                                </label>
                            </div>
                            <div className="custom-radio image-radio image-radio-editor">
                                <input type="radio" className="custom-control-input" id="checkimg3" name="ck2"
                                    onChange={(() => this.updateCurrentSheetBg(SheetBG.sheetBG3))} disabled={this.props.isSheetLocked}
                                    checked={backgroundUri === SheetBG.sheetBG3} />
                                <label className="custom-control-label custom-control-label-editor" htmlFor="checkimg3">
                                    <img className='noselect' src={imageBG03} alt="" />
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="form-copy form-copy-editor">
                        <div className="input-group input-group-editor">
                            <input type="text" value={sheetUrl} ref={this.sheetUrlField} readOnly 
                                   className="form-control-dark form-control form-control-dark-editor form-control-editor no-cursor no-cursor-editor noselect no-pointer no-pointer-editor" />
                            <div className="input-group-append input-group-append-editor">
                                <div className="bg-icon-copy bg-icon-copy-editor" onClick={this.copyLinkShare}>
                                    <div className="popup-copy popup-copy-editor">
                                        <div className="popup-copy-txt popup-copy-txt-editor">{t('editor.link.sheetUrl.copy')}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default withTranslation()(withMapContainerContext(SheetProperty));