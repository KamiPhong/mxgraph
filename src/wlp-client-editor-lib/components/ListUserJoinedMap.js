import React from 'react';
import { withTranslation } from 'react-i18next';
import { browser } from 'wlp-client-common/utils/BrowserUtil';
import ToolTipMember from './menu/ToolTipMember';


class ListUserJoinedMap extends React.Component {
    static defaultProps = {
        activeUsers: []
    }

    render(){
        const { activeUsers } = this.props;

        let memberOnlineClasses =['btn-profile-ps btn-profile-ps-editor member-online']
        if (activeUsers.length > 16) {
            memberOnlineClasses.push('h-online-101');
        }else if (activeUsers.length > 8 && activeUsers.length <= 16){
            memberOnlineClasses.push('h-online-70');
        }else {
            memberOnlineClasses.push('h-online-39');
        }

        if (browser && browser.name.toLowerCase() === 'safari'){
            memberOnlineClasses.push('member-online-safari')
        }

        let membersList = [];

        if (Array.isArray(activeUsers)) {
            for (let i = 0; i < activeUsers.length; i++) {
                membersList.push(<ToolTipMember data={activeUsers[i]} key={activeUsers[i].id}/>)
            }
        }
        
        return membersList.length > 0 ?
            (
                <div className={memberOnlineClasses.join(' ')} >
                    {membersList}
                </div>
            ) : null;
    }
}

export default withTranslation()(ListUserJoinedMap);