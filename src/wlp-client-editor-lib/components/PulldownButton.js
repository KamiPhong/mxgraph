import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import propTypes from 'prop-types';
import Link from 'wlp-client-common/component/Link';
import carretDownIcon from 'wlp-client-common/images/icon/pulldownIcon.png';
import PullDownButtonEvent from 'wlp-client-editor-lib/events/PullDownButtonEvent';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';
const pullDownButtonEvent = new PullDownButtonEvent();

class PulldownButton extends Component {
    state = {
        showItems: false
    }

    buttonContainer = React.createRef();

    hideItems = (cb) => {
        if (this.state.showItems === false) {
            return;
        }
        
        if(typeof(cb) !== 'function'){
            cb = () => {};
        }

        this.setState({ showItems: false }, cb);
    }

    showItems = (cb) => {
        if(typeof(cb) !== 'function'){
            cb = () => {};
        }
        this.setState({ showItems: true }, cb);
    }

    togglePulldownMenu = () => {
        const { onShow, onHide } = this.props;
        const { showItems } = this.state;
        if(showItems){
            this.hideItems(onHide);
        }else {
            this.showItems(onShow);
        }
    }

    handleDocumentClick = (event) => {
        const willHide = !checkElementClicked(event, this.buttonContainer.current);
        if(willHide){
            this.hideItems();
        }
    }

    componentDidMount() {
        document.addEventListener('click', this.handleDocumentClick);
        pullDownButtonEvent.addListener(pullDownButtonEvent.HIDE_MENU, this.doHideMenu);
    }

    doHideMenu = () => { 
        if (this.state.showItems) {
            this.hideItems();
        }
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleDocumentClick);
    }

    render() {
        const { title, img, pullDownItems, classDropDownMenu, classDropDown, classLink } = this.props;
        const { showItems } = this.state;
        return (
            <div className={"dropdown dropdown-editor dropdown-gr dropdown-gr-editor dropdown-cpn dropdown-cpn-editor " + classDropDown}>
                <Link innerRef={this.buttonContainer} className={"btn btn-editor dropdown-toggle dropdown-toggle-editor btn-sm btn-dark-dropd btn-dark-dropd-editor " + classLink + (showItems ? " active active-editor " : "")} onClick={this.togglePulldownMenu}>
                    {img && <img className="mr-2 dropdown-profile-img dropdown-profile-img-editor noselect" width="21" height="21" src={img} alt="" />}
                    <span className="text-truncate text-truncate-editor noselect">{title}</span>
                    <img src={carretDownIcon} alt="" className="dropdown-arrow dropdown-arrow-editor noselect"/>
                </Link>
                {
                    showItems &&
                    <div className={"dropdown-menu dropdown-menu-editor dropdown-menu-bg-black dropdown-menu-bg-black-editor dropdown-menu-pd dropdown-menu-pd-editor d-block kamix " + classDropDownMenu}>
                        {pullDownItems}
                    </div>
                }
            </div>
        )
    }
}

PulldownButton.propTypes = {
    title: propTypes.string,
    onShow: propTypes.func,
    onHide: propTypes.func,
    pullDownItems: propTypes.element,
    img: propTypes.string
}

export default withTranslation()(PulldownButton);
