import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import Link from 'wlp-client-common/component/Link';
import SheetTabConsts from 'wlp-client-common/consts/SheetTabConsts';
import PropTypes from 'prop-types';
import SheetEventCenter from 'wlp-client-editor-lib/events/SheetEventCenter';
import { withMainContext } from 'wlp-client-editor/MainContext';
import SheetPullDownListEventCenter from 'wlp-client-editor-lib/events/SheetPullDownListEventCenter';
import SheetModel from 'wlp-client-service/service/model/SheetModel';
import SheetTabListItem from './renderer/SheetTabListItem';
import {browser} from 'wlp-client-common/utils/BrowserUtil';
import { showErrorDialog } from 'wlp-client-common/utils/DialogUtils';
import SheetConsts from 'wlp-client-editor-lib/consts/SheetConsts';
import i18n from 'i18n';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import SheetViewEventCenter from 'wlp-client-editor-lib/events/SheetViewEventCenter';

const sheetEventCenter = new SheetEventCenter();
const sheetPulldownListEventCenter = new SheetPullDownListEventCenter();

class SheetTabList extends Component {
    static propTypes = {
        mainContext: PropTypes.object
    }

    static defaultProps = {
        mainContext: {}
    }

    nextButtonMouseOver_timmer = React.createRef();
    prevButtonMouseOver_timmer = React.createRef();
    tabSheetList = React.createRef();
    tabScrollBar = React.createRef();
    nextButtonSize = 21;
    horizontalScrollValue = 168; // width click next or prev button
    maskShowWithPaddingValue = 53; // width show background next or prev
    buttonShowWithPaddingValue = 52; // width show button next or prev
    wheelScrollValue = 460 // value scroll on mouse wheel

    state = ({
        sheetLists: [],
        itemsOnDragStart: [],
        translate3dScroll: 0,
        widthScreen: 0,
        showBgNext: false,
        showBgPrev: false,
        disabledBtnPrev: true,
        disabledBtnNext: true,
        isMouseDownNext: false,
        isMouseDownPrev: false,
        overIndex: 0,
        isUpdateOnDrop:false
    });

    constructor(props) {
        super(props);
        this.intervalID = null;
        this.transMaxHorizontal = 0;
        this.mousePoint = { x: 0, y: 0 };
        this.dragedTabClone = null
        this.tabWidth = 0
        this.touchTimer = null
    }

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);

        sheetPulldownListEventCenter.addListener(sheetPulldownListEventCenter.SHEET_PULLDOWN_ADD_SHEET, this.addSheet);
        sheetPulldownListEventCenter.addListener(sheetPulldownListEventCenter.SHEET_PULLDOWN_DELETE_SHEET, this.removeSheet);
        sheetPulldownListEventCenter.addListener(sheetPulldownListEventCenter.SHEET_PULLDOWN_LOCK_SHEET, this.setLockSheet);
    }

    componentDidUpdate(oldProps, oldStates) {
        const { sheetList, currentSheetId } = this.props;
        const { isUpdateOnDrop } = this.state;
        if ( oldProps.currentSheetId !== currentSheetId && isUpdateOnDrop === false) {
            this.sheetTabListDataSetScrollPosition(sheetList);
        }

        if(Array.isArray(sheetList) && Array.isArray(oldProps.sheetList)){
            if(sheetList.length !== oldProps.sheetList.length){
                this.updateDimensions();
            }
        }

        if (oldStates.translate3dScroll !== this.state.translate3dScroll && this.state.dragging) {
            this.checkDropPositionOnScroll();
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }
    
    setMousePoint = (x,y) => {
        this.mousePoint = { x: x, y: y };
    }

    setLockSheet = () => {
        const { currentSheet , lockSheet , unLockSheet } = this.props;
        const isSheetLocked = currentSheet.lockFlag;
        if(!isSheetLocked) {
            lockSheet(currentSheet);
        } else {
            unLockSheet(currentSheet);
        }
    }

    handleOnClickBackgroundNext = () => {
        const { sheetList, switchSheet } = this.props;
        const tabSheetListWidth = this.tabSheetList.current.offsetWidth;
        const valueTranslate = this.state.translate3dScroll < 0 ? -this.state.translate3dScroll : 0;
        const indexTabNext = Number.parseInt((valueTranslate + tabSheetListWidth)/SheetTabConsts.TAB_WIDTH_ITEM);

        if(sheetList){
            const sheetItem = sheetList[indexTabNext];
            switchSheet(sheetItem.id);
        }
    }

    handleOnClickBackgroundPrev = () => {
        const { sheetList, switchSheet } = this.props;
        const valueTranslate = this.state.translate3dScroll < 0 ? -this.state.translate3dScroll : 0;
        const indexTabPrev = Number.parseInt(valueTranslate/SheetTabConsts.TAB_WIDTH_ITEM);

        if(sheetList){
            const sheetItem = sheetList[indexTabPrev];
            switchSheet(sheetItem.id);
        }
    }

    handleChangeSheetNameCurrent = () => {
    const sheetViewEventCenter = new SheetViewEventCenter();
    sheetViewEventCenter.fireEvent(sheetViewEventCenter.CLEAR_SELECT_ITEMS);

    }

    handleSwitchSheet = (id) => {
        ActionLogger.click('User_Editor_SheetTabList', 'SwitchSheet_Tab');
        const { switchSheet } = this.props;
        const { isUpdateOnDrop } = this.state;

        if (isUpdateOnDrop) {
            this.setState({ isUpdateOnDrop: false });
        }
        if (typeof switchSheet === 'function') {
            switchSheet(id);
        }
    }

    updateDimensions = () => {
        const { translate3dScroll } = this.state;

        const tabScrollBarWidth = this.tabScrollBar.current.scrollWidth;
        const tabSheetListWidth = this.tabSheetList.current.offsetWidth;
        const widthNoScroll = tabScrollBarWidth + this.state.translate3dScroll;

        if (widthNoScroll <= (tabSheetListWidth - this.buttonShowWithPaddingValue)) {
            this.setState({
                disabledBtnNext: true,
                disabledBtnPrev: true,
            });
        } else {
            this.setState({ disabledBtnNext: false });
        }

        if (widthNoScroll <= (tabSheetListWidth - this.maskShowWithPaddingValue)) {
            this.setState({
                showBgNext: false ,
                disabledBtnNext: false,
                disabledBtnPrev: false
            });
        } else {
            this.setState({
                showBgNext: true,
                disabledBtnPrev: false,
            });
        }

        if (widthNoScroll + this.maskShowWithPaddingValue < tabSheetListWidth) {
            if (tabScrollBarWidth <= tabSheetListWidth) {
                this.setState({
                    translate3dScroll: 0,
                    showBgPrev: false,
                    disabledBtnNext: true,
                    disabledBtnPrev: true
                });
            } else {
                this.setState({
                    translate3dScroll: translate3dScroll + (tabSheetListWidth - widthNoScroll - this.buttonShowWithPaddingValue)
                });
            }
        }
    };

    sheetTabListDataSetScrollPosition = (sheetLists) => {
        const { currentSheetId } = this.props;
        if (Array.isArray(sheetLists) && sheetLists.length > 0) {
            for (let i = 0; i <= sheetLists.length - 1; i++) {
                const sheet = sheetLists[i];
                if (sheet && sheet.id === currentSheetId) {
                    this.setScrollPosition(i, sheetLists.length - 1);
                }
            }
        }
    }

    setScrollPosition = (setCount, maxCount) => {
        const tabw = SheetTabConsts.TAB_WIDTH;
        const listSheet = this.tabSheetList.current.offsetWidth - SheetTabConsts.PADDING_BTN;
        const scrollBar = this.tabScrollBar.current.scrollWidth;

        const minw = listSheet / 2;
        const maxw = tabw * maxCount;

        let setw = tabw * setCount;
        let scrollAtTheEnd = false;
        let showBgNext = false;
        let showBgPrev = false;
        let disabledBtnNext = false;
        let disabledBtnPrev = false;

        const slotTab = Math.ceil(minw / tabw);

        if (maxCount - slotTab + 1 < setCount && setCount < maxCount) {
            scrollAtTheEnd = true;
        }

        if (scrollBar < listSheet) {
            setw = 0;
            disabledBtnNext = true;
            disabledBtnPrev = true;
        } else {
            if (minw > setw) {
                setw = 0;
                showBgNext = true;
            } else if (maxw <= setw || scrollAtTheEnd) {
                setw = maxw - listSheet + tabw;
                showBgNext = false;
                showBgPrev = true;
            }else {
                setw = setw - minw;
                showBgNext = true;
                showBgPrev = true;
            }
        }

        this.setState({
            translate3dScroll: -setw,
            showBgNext: showBgNext,
            showBgPrev: showBgPrev,
            disabledBtnNext: disabledBtnNext,
            disabledBtnPrev: disabledBtnPrev
        });

    }

    addSheet = () => {
        ActionLogger.click('User_Editor_SheetTabList', 'AddSheet_Button');
        const { getUserSession, sheetList } = this.props;
        const { isUpdateOnDrop } = this.state;
        const sessionDto = getUserSession();
        
        if (!sessionDto) return;
        if (sheetList.length >= SheetConsts.LIMIT_SHEET) {
            showErrorDialog(i18n.t('editor.alert.create.max.sheet'));
            return;
        }

        if (isUpdateOnDrop) {
            this.setState({ isUpdateOnDrop: false })
        }
        this.transMaxHorizontal = 0;

        const NEW_SHEET_NAME = "New Sheet";

        let sheet = new SheetModel();
        sheet.name = NEW_SHEET_NAME;
        if ( sheet ) {
            sheetEventCenter.fireEvent(sheetEventCenter.SHEET_ADD_NEW, sheet);
        }
    }

    removeSheet = (sheetModel) => {
        sheetEventCenter.fireEvent(sheetEventCenter.SHEET_REMOVE, sheetModel);
    }

    updateSheet = (sheet) => {
        sheetEventCenter.fireEvent(sheetEventCenter.SHEET_UPDATE, sheet);
    }

    moveSheet = (sheet) => {
        sheetEventCenter.fireEvent(sheetEventCenter.SHEET_MOVE, sheet);
    }

    renderSheetItems = () => {
        const { sheetList, currentSheetId } = this.props;
        if (!currentSheetId || Array.isArray(sheetList) === false) {
            return null
        }

        return sheetList.map((sheet, index) => {
            if (!sheet) {
                return null;
            }

            const { sheetLockButtonVisible } = this.props;
            return (
                <SheetTabListItem
                    sheetLockButtonVisible={sheetLockButtonVisible}
                    updateSheet={this.updateSheet}
                    switchSheet={this.handleSwitchSheet}
                    isActive={currentSheetId === sheet.id}
                    key={sheet.id}
                    index={index}
                    sheet={sheet}
                    onDragEnd={this.onDragEnd}
                    onDragOver={this.onDragOverTab}
                    onDragStart={(e) => this.onDragStart(index,e)}
                    onTouchStart = {(e) => this.onTouchStart(index, e)}
                    onTouchMove={e => this.onTouchMove(e)}
                    onTouchEnd={this.onTouchEnd}
                    handleChangeSheetNameCurrent={this.handleChangeSheetNameCurrent}
                />
            )
        })
    };

    doCaculatorScrollLimit = () => {
        const scroller = this.tabScrollBar.current;
        const scrollerView = scroller.parentNode;
        const scrollerViewStyle = getComputedStyle(scrollerView)
        
        this.transMaxHorizontal = scroller.scrollWidth - scrollerView.clientWidth;
        this.transMaxHorizontal += parseFloat(scrollerViewStyle['padding-left'].replace('px')) - this.nextButtonSize;
        this.transMaxHorizontal += parseFloat(scrollerViewStyle['padding-right'].replace('px'));
        /** TODO: fix lack element width by position absolute */
        this.transMaxHorizontal += 17;
    }

    createTransitionInterval = (time, step) => {
        if (typeof this.intervalID !== 'number') {
            this.intervalID = setInterval(this.doScrollSheets, time, step)    
        }
    }

    removeTransitionInterval = () => {
        clearInterval(this.intervalID);
        this.intervalID = null;
    }

    checkDropPositionOnScroll = () => {
        const { x, y } = this.mousePoint;
        if (x > 0 && y > 0) {
            let elements;
            if( browser.name === 'ie'){
                elements = document.msElementsFromPoint(x, y);
            } else {
                elements = document.elementsFromPoint(x, y);
            }
            
            let tab = Object.values(elements).filter(function(e) {
                return e.classList.contains('tab-sheet') && typeof e.dataset.index !== 'undefined'
            });
            if (tab.length > 0) {
                const tabTarget = tab.length === 2 ? tab[1] : tab[0];

                const { dragging } = this.state;
                this.removeDropPosition();

                const index = parseInt(tabTarget.dataset.index);
                const tabSheetBounding = tabTarget.getBoundingClientRect();
                const dropPostion = (x - tabSheetBounding.x) > (tabSheetBounding.width / 2) ? 'r' : 'l';
                tabTarget.classList.add(`tab-line-${dropPostion}`)
                let orderIndex = dropPostion === 'r' ? index+1 : index;
                if (orderIndex < 0) {
                    orderIndex = 0;
                }
                if (index !== dragging) {
                    this.setState({ overIndex:  orderIndex});
                }
            }
        }
    }

    updateSheetPosition = () => {
        const { overIndex, dragging } = this.state;
        const { sheetList, switchSheet } = this.props;
        const itemMoved = sheetList[dragging];

        if (itemMoved && overIndex !== null && overIndex > -1) {
            itemMoved.order = dragging < overIndex ? overIndex - 1 : overIndex;
            this.moveSheet(itemMoved);
            this.setState({ overIndex: null, dragging: null, isUpdateOnDrop: true }, () => {
                switchSheet(itemMoved.id);
            });
        }
    }

    removeDropPosition = () => {
        const tabSheets = document.querySelectorAll(".tab-line-r, .tab-line-l");
        if (tabSheets.length > 0) {
            tabSheets.forEach(t => {
                t.classList.remove('tab-line-l','tab-line-r');
            });
        }
    }
    /**
     * 
     * Event on click button
     */
    onMouseUpSheetPrev = (e) => {
        ActionLogger.click('User_Editor_SheetTabList', 'PreviousSheet_Button');
        if (!this.state.disabledBtnPrev) {
            this.onMouseUpPaddingButtonHandler(e);
        }
    }

    onMouseUpSheetNext = (e) => {
        ActionLogger.click('User_Editor_SheetTabList', 'NextSheet_Button');
        if (!this.state.disabledBtnNext) {
            this.onMouseUpPaddingButtonHandler(e);
        }
    }

    onMouseDownPaddingButtonHandler = (e) => {
        const targetClass = e.target.classList;
        const { disabledBtnNext, disabledBtnPrev } = this.state;
        let step;

        if (targetClass.contains('btn-tabscrol-l') && !disabledBtnPrev) {
            step = SheetTabConsts.STEP_PREV;
        } else if (targetClass.contains('btn-tabscrol-r') && !disabledBtnNext) { 
            step = SheetTabConsts.STEP_NEXT;
        }
        this.timerPaddingButtonMouseDown = new Date().getTime();

        if (typeof step !== 'undefined') {
            this.mouseHoldInterval = setTimeout(() => {
                this.createTransitionInterval(5, step);
            }, 700);    
        }
    }

    onMouseUpPaddingButtonHandler = (e) => {
        this.removeTransitionInterval();
        clearTimeout(this.mouseHoldInterval);
        if (typeof this.timerPaddingButtonMouseDown !== 'undefined' && (new Date().getTime() - this.timerPaddingButtonMouseDown) < 700) {
            const targetClass = e.target.classList;

            let translate3dScrollNew = 0;

            if (targetClass.contains('btn-tabscrol-l')) {
                translate3dScrollNew += this.horizontalScrollValue;
            } else if (targetClass.contains('btn-tabscrol-r')) {
                translate3dScrollNew -= parseInt(this.horizontalScrollValue);
            }
            this.doScrollSheets( translate3dScrollNew );

        }
    }

    /**
     * Event on drag
     */

    onDragStart = (i,e) => {
        this.setState({ dragging: i });
    }

    onTouchStart = (i, e) => {
        if(window.navigator.onLine) {
            e.stopPropagation()
            e.preventDefault()
            this.setState({ dragging: i });
            const evt = (typeof e.originalEvent === 'undefined') ? e : e.originalEvent;
            const touch = evt.touches[0] || evt.changedTouches[0]
            let dragedTabCloneText = e.target.innerText
            this.dragedTabClone = document.createElement('div')
            this.dragedTabClone.className = 'tab-sheet-clone'
            this.dragedTabClone.innerHTML = dragedTabCloneText
            this.dragedTabClone.style.top = touch.pageY + 'px'
            this.dragedTabClone.style.left = touch.pageX + 'px'

            this.touchTimer = setTimeout(this.appendCloneTab, 300)
        } else { 
            const cloneTab = document.getElementsByClassName('tab-sheet-clone')[0]
            if(cloneTab) {
                document.body.removeChild(cloneTab)
            }       
            showWarningDialog(('message.global.e0001'))
        }
    }

    appendCloneTab = () => {
        document.body.appendChild(this.dragedTabClone)
        this.tabWidth = this.dragedTabClone.offsetWidth
    }

    allowDrop = (e) => {
        e.preventDefault();
    }

    onDragOverTab = (e) => {
        let tabTarget = e.target;
        if (tabTarget.classList.contains('tab-sheet') !== true) {
            tabTarget = tabTarget.closest('.tab-sheet');
        }

        if (typeof tabTarget.dataset.index !== 'undefined') {
            this.setMousePoint(e.clientX, e.clientY);
            this.checkDropPositionOnScroll();
        }
    }

    onTouchMove = (e) => {
        if(window.navigator.onLine) {
            let tabTarget = e.target;
            if (tabTarget.classList.contains('tab-sheet') !== true) {
                tabTarget = tabTarget.closest('.tab-sheet');
            }

            if (typeof tabTarget.dataset.index !== 'undefined') {
                if(e.type = 'touchmove') {
                    const evt = (typeof e.originalEvent === 'undefined') ? e : e.originalEvent;
                    const touch = evt.touches[0] || evt.changedTouches[0]
                    this.setMousePoint(touch.pageX, touch.pageY);
                    this.checkDropPositionOnScroll();
                    
                    if(this.dragedTabClone && window.innerWidth - touch.pageX >= this.tabWidth) {
                        this.dragedTabClone.style.left = touch.pageX + 'px'
                        this.dragedTabClone.style.top = touch.pageY + 'px'
                    } 
                }
            }
        } 
    }

    onDragEnd = () => {
        this.updateSheetPosition();
        this.removeDropPosition();
    }

    onTouchEnd = () => {
        if(window.navigator.onLine) {
            this.updateSheetPosition();
            this.removeDropPosition();
            if(this.touchTimer) {
                clearTimeout(this.touchTimer)
            }
            const cloneTab = document.getElementsByClassName('tab-sheet-clone')[0]
            if(cloneTab) {
                document.body.removeChild(cloneTab)
            }
        } 
    }

    onDragStartHandler = (e) => {
        this.doCaculatorScrollLimit();
    }

    onDragLeaveHandler = (e) => {
        this.removeTransitionInterval();
    }

    onDragOverPaddingButtonHandler = (e) => {
        const { disabledBtnPrev } = this.state;
        const targetClass = e.target.classList;
        
        this.setMousePoint(e.clientX, e.clientY);
        let time = 0;
        let step = 0;

        if (targetClass.contains('shadow-l')) {
            time = 10;
            step = 2;
        } else if (targetClass.contains('shadow-r')) {
            time = 10;
            step = -2;
        } else if (targetClass.contains('btn-tabscrol-l') || targetClass.contains('tab-sheet-prev')) {
            time = 5;
            step = 4;
        } else if (targetClass.contains('btn-tabscrol-r') || targetClass.contains('tab-sheet-next')) {
            time = 5;
            step = -4;
        }

        switch (browser && browser.name) {
            case 'ie':
                /** change speed on IE */
                step *= 5;
                break;
            default:
                break;
        }

        if (time > 0 && !disabledBtnPrev) {
            this.createTransitionInterval( time, step);
        }
    }

    doScrollSheets = async (step) => {
        if (this.transMaxHorizontal < 50) {
            await this.doCaculatorScrollLimit();
        }
        let translate3dScrollNewValue = this.state.translate3dScroll + step;

        if (translate3dScrollNewValue > 0) {
            translate3dScrollNewValue = 0;
        }
        if (Math.abs(translate3dScrollNewValue) > this.transMaxHorizontal) {
            translate3dScrollNewValue = -this.transMaxHorizontal;
        }

        const scrollerWidth = this.tabScrollBar.current.scrollWidth;
        const scrollerViewWidth = this.tabSheetList.current.offsetWidth;
        
        let showBtnPrevShawdow = false;
        let showBtnNextShawdow = false;

        this.setState(() => {
            if (scrollerViewWidth < scrollerWidth + translate3dScrollNewValue) {
                showBtnNextShawdow = true;
            }
            if (translate3dScrollNewValue < 0 ) {
                showBtnPrevShawdow = true;
            }
            
            return {
                showBgPrev: showBtnPrevShawdow,
                showBgNext : showBtnNextShawdow,
                translate3dScroll : translate3dScrollNewValue
            }
        });
    }

    // handler event on mouse wheel
    onWheelHandler = (e) => {
        const {showBgPrev, showBgNext} = this.state;
        const delta = -Math.sign(e.deltaY);

        // không scroll sang phải nếu nút Next là disabled
        if( delta < 0 && showBgNext !== true){
            return;
        }

        // không scroll sang trải nếu nút Prev  là disabled
        if( delta > 0 && showBgPrev !== true){
            return;
        }

        this.doScrollSheets( delta * this.wheelScrollValue )
    }

    render() {
        const { translate3dScroll, showBgNext, showBgPrev, disabledBtnPrev, disabledBtnNext } = this.state;
        let btnTabScrollLeft = ['btn-reset btn-reset-editor btn-tabscrol-l'];
        let btnTabScrollRight = ['btn-reset btn-reset-editor btn-tabscrol-r'];
        if(disabledBtnPrev){
            btnTabScrollLeft.push('btn-tabscrol-l-disabled');
        }
        if(disabledBtnNext){
            btnTabScrollRight.push('btn-tabscrol-r-disabled');
        }        

        const onDragEvents = {
            onDragStart     : this.onDragStartHandler,
            onDragOver      : this.onDragOverPaddingButtonHandler,
            onDragLeave     : this.onDragLeaveHandler,
            onWheel         : this.onWheelHandler
        };

        let scrollerStyle = {
            left: `${translate3dScroll}px`
        };

        return (<Fragment>
            <div className="tabs-sheet tabs-sheet-editor">
                <div className="tabs-sheet-list tabs-sheet-list-editor show-boxshadow show-boxshadow-editor show-boxshadow-l show-boxshadow-l-editor show-boxshadow-r show-boxshadow-r-editor" ref={this.tabSheetList}  {...onDragEvents}>
                    <div className="tabs-scrollbar tabs-scrollbar-editor" ref={this.tabScrollBar} style={scrollerStyle} onDragOver={this.allowDrop}>
                        {this.renderSheetItems()}
                    </div>

                    {showBgPrev &&
                        <div
                            className = "shadow-l"
                            onClick = {this.handleOnClickBackgroundPrev}
                        />
                    }

                    {showBgNext &&
                        <div
                            className="shadow-r"
                            onClick ={this.handleOnClickBackgroundNext}
                        />
                    }

                    <div className="tab-sheet-prev tab-sheet-bg">
                        <Link
                            className   ={btnTabScrollLeft.join(' ')}
                            onMouseDown={this.onMouseDownPaddingButtonHandler}
                            onMouseUp ={this.onMouseUpSheetPrev}
                        />
                    </div>

                    <div className="tab-sheet-next tab-sheet-next-editor tab-sheet-bg">
                        <Link
                            className   ={btnTabScrollRight.join(' ')}
                            onMouseDown={this.onMouseDownPaddingButtonHandler}
                            onMouseUp ={this.onMouseUpSheetNext}
                        />
                    </div>
                </div>
                <div className="tab-sheet-add">
                    <div className="btn-sheet-add btn-sheet-add-editor">
                        <Link
                            onClick={this.addSheet}
                            className="sheet-add-ic sheet-add-ic-editor btn-reset btn-reset-editor"
                        />
                    </div>
                </div>
            </div>
        </Fragment>)
    }
}

export default withTranslation(null, {withRef: true})(withMainContext(SheetTabList));
