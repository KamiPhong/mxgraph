import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import Link from 'wlp-client-common/component/Link';

class SheetLockButton extends Component {
    render() {
        const { lockSheet } = this.props;

        return (<React.Fragment>
            <Link className="btn btn-editor btn-dark-gr btn-dark-gr-editor btn-lock btn-lock-editor btn-shadow" onClick={lockSheet}>
                <div className="btn-lock-ic btn-lock-ic-editor"></div>
            </Link>
        </React.Fragment>)
    }
}

export default withTranslation()(SheetLockButton);