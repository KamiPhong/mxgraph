import React from 'react';
import PropTypes from 'prop-types';
import RangeSlider from 'wlp-client-common/component/RangeSlider/RangeSlider';
import Link from 'wlp-client-common/component/Link';
import zoomSliderVariables from 'wlp-client-editor-lib/assets/scss/zoomSliderVariables.scss';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { ZoomConfig } from "wlp-client-editor-lib/core/KGraphConfig";
export default class ExpandSlider extends React.Component {
    static propTypes = {
        doZoomGraph: PropTypes.func,
        currentZoomValue: PropTypes.number,
        maxZoomValue: PropTypes.number,
        minZoomValue: PropTypes.number
    }

    increaseZoomBtn = React.createRef();
    decreaseZoomBtn = React.createRef();

    scrollControlAreaRef = Element => React.createRef( this.scrollControlArea = Element);

    /**
     * @author ThuyTV
     * @description handle click zoom out button
     * @param {none}
     * @return void
     */
    handleZoomOut = () => {
        ActionLogger.click('User_Editor_SheetView', 'Zoom_Out_Button');
        this.props.doZoomGraph(1, false);
    }

    /**
     * @author ThuyTV
     * @description handle click zoom in button
     * @param {none}
     * @return void
     */
    handleZoomIn = () => {
        ActionLogger.click('User_Editor_SheetView', 'Zoom_In_Button');
        this.props.doZoomGraph(1, true);
    }

    calculateCurrentValue = (isMouseDown) => {
        const peroid = ZoomConfig.SLIDE_MAX_ZOOM/ZoomConfig.MAX_ZOOM_VALUE;
        const currentValue = this.props.currentZoomValue;
        if (isMouseDown) {
            if (currentValue == 1) {
                return currentValue;
            }
            return currentValue*peroid;
        }

        if (currentValue < peroid) {
            return 1;
        }

        return Math.ceil(currentValue/peroid)
    }


    zoomSlider_handleMouseDown = (mouseFlag = false) => {
        if (this.increaseZoomBtn.current) {
            this.increaseZoomBtn.current.style.display = 'none';
        }

        if (this.decreaseZoomBtn.current) {
            this.decreaseZoomBtn.current.style.display = 'none';
        }

        this.scrollControlArea.style.marginLeft = zoomSliderVariables.zommContainerActivatedMarginLeft;

        const currentValue = this.calculateCurrentValue(true);
        const maxZoomValue = ZoomConfig.SLIDE_MAX_ZOOM
        this.props.setZoomFactor(currentValue, maxZoomValue);
    }

    zoomSlider_handleMouseUp = () => {
        if (this.increaseZoomBtn.current){
            this.increaseZoomBtn.current.style.display = 'block';
        }

        if (this.decreaseZoomBtn.current){
            this.decreaseZoomBtn.current.style.display = 'block';
        }
        this.scrollControlArea.style.marginLeft = zoomSliderVariables.zoomContainerNormalMarginLeft;
        // this.props.setZoomFactor();
        const currentValue = this.calculateCurrentValue(false);
        const maxZoomValue = ZoomConfig.MAX_ZOOM_VALUE
        this.props.setZoomFactor(currentValue, maxZoomValue);
    }


    /**
     * @author ThuyTV
     * @description handle slider zoom button change.
     * zoom in if value is increased
     * zoom out if value is decreased
     * 
     * @param {InputEvent} evt
     * @returns void
     */
    handleZoomSlider_changeValue = (value) => {
        const { currentZoomValue, doZoomGraph } = this.props;
        const newZoomValue = Number(value);
        const isZoomIn = newZoomValue > currentZoomValue;
        doZoomGraph(Math.abs(newZoomValue - currentZoomValue), isZoomIn);
    }

    render() {
        const { currentZoomValue, maxZoomValue, minZoomValue } = this.props;
        return (
            <div className="zoom-control" ref={this.scrollControlAreaRef}>
                <Link innerRef={this.decreaseZoomBtn} className="zoom-btn zoom-out" onClick={this.handleZoomOut} />
                <span className="zoom-slider">
                    <RangeSlider
                        onMouseDown={this.zoomSlider_handleMouseDown}
                        onMouseUp={this.zoomSlider_handleMouseUp}
                        onChange={this.handleZoomSlider_changeValue}
                        value={currentZoomValue}
                        min={minZoomValue}
                        max={maxZoomValue} />
                </span>
                <Link innerRef={this.increaseZoomBtn} className="zoom-btn zoom-in" onClick={this.handleZoomIn} />
            </div>
        )
    }
}