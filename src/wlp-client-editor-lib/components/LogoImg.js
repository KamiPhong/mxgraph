import React from 'react';
import { withTranslation } from 'react-i18next';

const LogoImg = ({ logo, onClickLogo }) => {
    return (
        <div className="btn-reset btn-reset-editor header-logo header-logo-editor noselect"><img onClick={onClickLogo} className="cursor-pointer cursor-pointer-editor noselect" src={logo} alt="logo"/></div>
    )
}

export default withTranslation()(LogoImg);