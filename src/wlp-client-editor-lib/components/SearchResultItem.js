import React from 'react';
import ActionLogger from 'wlp-client-common/ActionLogger';
import Link from 'wlp-client-common/component/Link';
import grayItemIcon from 'wlp-client-common/images/icon/grayItemIcon.png';
import { openEditorPage } from 'wlp-client-common/utils/EdotorUtil';
import ShowAllSearchItem from './ShowAllSearchItem';

class SearchResultItem extends React.Component {
    findFollowingItemsInEditor = (item) => {
        ActionLogger.click('User_Editor_SearchResult', `Open_Editor_Page_With_Item`);
        // TODO task 1056
        openEditorPage(item.mapId, item.sheetId, item.itemId);
    }
    
    render(){
        const { t, item, valueSearch } = this.props;
        return (
            <li onClick={() => { this.findFollowingItemsInEditor(this.props.item) }}>
                <Link className="results-item results-item-editor" >
                    <div className="media media-editor">
                        <div className="media-img">
                            <img src={grayItemIcon} className="align-self-center noselect" alt=""/>
                        </div>
                        <div className="media-body media-body-editor">
                            <ShowAllSearchItem t={t} item={item} showAllItem={false} valueSearch={valueSearch}/>
                        </div>
                    </div>
                </Link>
            </li>
        )
    }
}
export default SearchResultItem;