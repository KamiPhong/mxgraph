import React from 'react';
import { withTranslation } from 'react-i18next';
import SearchService from 'wlp-client-service/service/SearchService';
import SearchConfigDto from 'wlp-client-service/dto/search/SearchConfigDto';
import { COUNT_DEFAULT_ITEM } from 'wlp-client-mypage-lib/consts/UserSetting';
import 'wlp-client-editor/assets/css/styles.css';
import SearchResultPanel from './SearchResultPanel';
import ActionLogger from 'wlp-client-common/ActionLogger';
const searchService = new SearchService();

class SearchInput extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            valueSearch: '',
            searchResult: null,
            searchInput:null,
            selectedContractId:null,
            searchPopup:false,
            inputFocusFlag: false,
        };
    }

    callSearchTeam_handleResult(e) {
        ActionLogger.submit('User_Editor_Header', 'Search_Input');
        this.props.setShowSearchResultPanel(false);
        e.preventDefault();
        if (this.state.valueSearch !== '') {
            this.findReading(-1, this.state.valueSearch);
        }
    }
    async findReading(contractId, text) {
        let requestDto = new SearchConfigDto();
        requestDto.contractId = contractId;
        requestDto.text = text;
        const { mapInfo } = this.props.mapContainerContext;
        requestDto = {
            ...requestDto,
            "contractId": null,
            "count": parseInt(COUNT_DEFAULT_ITEM),
            "mapId": mapInfo.id,
            "start": 0,
            "text": requestDto.text
        };

        const result = await searchService.findItemsInEditor(requestDto);
        this.findReadingResult(result);
    }

    findReadingResult(data) {
        this.setState({
            searchResult:data,
            searchPopup:true
        });
        this.props.setShowSearchResultPanel(true);
    }
    handleChangeInputSearchTeam(text) {
        ActionLogger.typing('User_Editor_Header', 'Search_Input');
        this.setState({
            valueSearch:text
        });
    }
    eventsClickIconSearch() {
        if (this.state.searchInput && this.state.searchInput.current) {
            this.state.searchInput.current.focus();
        }
    }

    onInputBlur (evt) {
        const {t} = this.props;
        evt.target.placeholder = t('search.prompt.editor');
        this.setState({
            inputFocusFlag: false
        })
    }

    onInputFocus (evt) {
        evt.target.placeholder = "";
        evt.target.select();
        this.setState({
            inputFocusFlag: true
        })
    }

    render(){
        const { 
            t,
            isShowSearchResultPanel,
            hideSearchResultPanel,
            setSearchResultPanelRef,
            setSearchListItemViewModalRef
        } = this.props;
        const {inputFocusFlag} = this.state;
        let inputSearchClasses = inputFocusFlag? 'form-control-header form-control-header-editor' : 'form-control-header form-control-header-editor noselect'
        return (<React.Fragment>
            <div className="header-search header-search-editor">
                <form onSubmit={(e) => this.callSearchTeam_handleResult(e)}>
                    <input
                        type="text"
                        className={inputSearchClasses}
                        placeholder={t('search.prompt.editor')}
                        onFocus={(e) => this.onInputFocus(e)}
                        onBlur={(e) => this.onInputBlur(e)}
                        ref={this.searchInput}
                        onClick={() => {
                            ActionLogger.click('User_Editor_Header', 'Search_Input');
                        }}
                        onChange={(e) => this.handleChangeInputSearchTeam(e.target.value)} />
                </form>
            {isShowSearchResultPanel && (
                <SearchResultPanel 
                    setSearchResultPanelRef={setSearchResultPanelRef} 
                    setSearchListItemViewModalRef= {setSearchListItemViewModalRef}
                    hideSearchResultPanel={hideSearchResultPanel} 
                    searchResult={this.state.searchResult}
                    valueSearch = {this.state.valueSearch}
                    t={t} />
            )}
            </div>   
        </React.Fragment>)
    }
}

export default withTranslation()(SearchInput);