import EventCenter from "wlp-client-common/events/EventCenter";
class SheetViewEventCenter extends EventCenter{

    DO_CLOSE_SHEET_PROPERTY = "DO_CLOSE_SHEET_PROPERTY";
    SHEET_PROPERTY_CLOSED = "SHEET_PROPERTY_CLOSED";
    SHEET_PROPERTY_OPENED = "SHEET_PROPERTY_OPENED";
    CLEAR_SELECT_ITEMS = "CLEAR_SELECT_ITEMS";
    constructor(){
        super();
        if(SheetViewEventCenter.instance){
            return SheetViewEventCenter.instance;
        }

        SheetViewEventCenter.instance = this;
    }
}

export default SheetViewEventCenter;