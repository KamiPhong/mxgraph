import EventCenter from "wlp-client-common/events/EventCenter";
class EditPullDownListEventCenter extends EventCenter {
    EDIT_PULLDOWN_CREATE_ITEM = 'editPulldownCreateItem';
    /** <p>「編集」プルダウン「ドキュメントから新しいアイテムを作成」を示すメニューインデックス値</p> */
    EDIT_PULLDOWN_CREATE_ITEM_BY_DOCUMENT = 'editPulldownCreateItemByDocument';
    /** <p>「編集」プルダウン「ベースアイテムを作成」を示すメニューインデックス値</p> */
    EDIT_PULLDOWN_DO_CREATE_BASE_ITEM = 'editPulldownDoCreateBaseItem';
    /** <p>「編集」プルダウン「コピー」を示すメニューインデックス値</p> */
    EDIT_PULLDOWN_COPY_ITEM = 'editPulldownCopyItem';
    /** <p>「編集」プルダウン「ペースト」を示すメニューインデックス値</p> */
    EDIT_PULLDOWN_PASTE_ITEM = 'editPulldownPasteItem';
    /** <p>「編集」プルダウン「削除」を示すメニューインデックス値</p> */
    EDIT_PULLDOWN_DELETE_ITEM = 'editPulldownDeleteItem';

    SET_ITEM_SELECTED='setItemSelected';

    EDIT_PULLDOWN_PASTE_ITEM_ENABLE = 7;

    constructor() {
        super();
        if (EditPullDownListEventCenter.instance) {
            return EditPullDownListEventCenter.instance;
        }

        EditPullDownListEventCenter.instance = this;
    }
}

export default EditPullDownListEventCenter;