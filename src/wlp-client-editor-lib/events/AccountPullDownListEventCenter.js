import EventCenter from "wlp-client-common/events/EventCenter";
class AccountPullDownListEventCenter extends EventCenter {
    ACCOUNT_PULLDOWN_LOG_OUT = 'accountPullDownLogOut';

    constructor() {
        super();
        if (AccountPullDownListEventCenter.instance) {
            return AccountPullDownListEventCenter.instance;
        }

        AccountPullDownListEventCenter.instance = this;
    }
}

export default AccountPullDownListEventCenter;