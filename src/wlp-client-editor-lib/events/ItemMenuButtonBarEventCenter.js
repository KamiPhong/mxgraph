import EventCenter from "wlp-client-common/events/EventCenter";
class ItemMenuButtonBarEventCenter extends EventCenter {
    // remove  item
    ITEM_MENU_BUTTON_BAR_REMOVE = "itemMenuRemove";
    // change shape item
    ITEM_MENU_BUTTON_BAR_SHAPE_CHANGE = "itemMenuButtonShapeChane";

    ITEM_MENU_BUTTON_BAR_STYLE_CHANGE = "itemMenuButtonStyleChange";

    ITEM_MENU_BUTTON_BAR_TEXT_SIZE_CHANGE = "itemMenuButtonTextSizeChange";

    ITEM_MENU_BUTTON_BAR_TEXT_ALIGN_CHANGE = "itemMenuButtonTextAlignChange";

    ITEM_MENU_BUTTON_BAR_TEXT_VERTICAL_ALIGN_CHANGE = "itemMenuButtonTextVerticalAlignChange";

    ITEM_MENU_BUTTON_UPLOAD_FILE_COMPLETED = "itemMenuUploadFileCompleted";

    ITEM_MENU_BUTTON_DOWNLOAD_FILE = "itemMenuDownloadFile";

    ITEM_MENU_CLICKED = "itemMenuClick";

    ITEM_MENU_DO_CLOSE = 'itemMenuDoClose';

    ITEM_MENU_PROPERTY_SETTING_PASSWORD = "itemMenuPropertySettingPassword";

    ITEM_MENU_PROPERTY_REMOVE_PASSWORD = "itemMenuPropertyRemovePassword";

    constructor() {
        super();
        if (ItemMenuButtonBarEventCenter.instance) {
            return ItemMenuButtonBarEventCenter.instance;
        }

        ItemMenuButtonBarEventCenter.instance = this;
    }
}

export default ItemMenuButtonBarEventCenter;