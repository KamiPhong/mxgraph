import EventCenter from "wlp-client-common/events/EventCenter";

class PullDownButtonEvent extends EventCenter {
    HIDE_MENU = 'hideMenu';

    constructor() {
        super();
        if (PullDownButtonEvent.instance) {
            return PullDownButtonEvent.instance;
        }

        PullDownButtonEvent.instance = this;
    }
}

export default PullDownButtonEvent;