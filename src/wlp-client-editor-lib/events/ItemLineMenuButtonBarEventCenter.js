import EventCenter from "wlp-client-common/events/EventCenter";

class ItemLineMenuButtonBarEventCenter extends EventCenter {
    // remove  item
    ITEM_LINE_MENU_BUTTON_BAR_REMOVE = "itemLineMenuRemove";
    // change shape item
    ITEM_MENU_BUTTON_BAR_COLOR_CHANGE = "lineMenuButtonColorChange";

    ITEM_LINE_MENU_BUTTON_BAR_STYLE_CHANGE = "itemLineMenuButtonStyleChange";

    ITEM_LINE_MENU_BUTTON_BAR_TEXT_SIZE_CHANGE = "itemLineMenuButtonTextSizeChange";

    ITEM_LINE_MENU_BUTTON_BAR_TEXT_ALIGN_CHANGE = "itemLineMenuButtonTextAlignChange";

    ITEM_LINE_MENU_BUTTON_BAR_TEXT_VERTICAL_ALIGN_CHANGE = "itemLineMenuButtonTextVerticalAlignChange";
    
    ITEM_MENU_LINE_BUTTON_BAR_REMOVE = "itemLineMenuRemove";

    ITEM_LINE_MENU_BUTTON_BAR_SHAPE_CHANGE = "itemLineMenuButtonBarShapeChange";


    ITEM_LINE_MENU_BUTTON_BAR_TEXT_HORIZONTAL_ALIGN_CHANGE = "itemLineMenuButtonTextHorizontalAlignChange";

    ITEM_LINE_MENU_BUTTON_BAR_WEIGHT_CHANGE = "itemLineMenuButtonBarWeightChange";

    ITEM_LINE_MENU_DO_CLOSE = "itemLineMenuDoClose";

    ITEM_LINE_MENU_CLICKED = "itemLineMenuClicked";

    constructor() {
        super();
        if (ItemLineMenuButtonBarEventCenter.instance) {
            return ItemLineMenuButtonBarEventCenter.instance;
        }

        ItemLineMenuButtonBarEventCenter.instance = this;
    }
}

export default ItemLineMenuButtonBarEventCenter;