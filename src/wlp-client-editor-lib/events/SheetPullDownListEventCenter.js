import EventCenter from "wlp-client-common/events/EventCenter";
class SheetPullDownListEventCenter extends EventCenter {
    SHEET_PULLDOWN_ADD_SHEET = 'sheetPulldownAddSheet';
    /** <p>「シート」プルダウン「シートを複製」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_COPY_SHEET = 'sheetPulldownCopySheet';
    /** <p>「シート」プルダウン「シートを PNG でエクスポート」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_SAVE_PNG = 'sheetPulldownSavePng';
    /** <p>「シート」プルダウン「シートを PNG ( 範囲選択 ) でエクスポートト」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_CHANGE_SAVE_PNG_BY_SELECTED_FLAG = 'sheetPulldownEnableSavePngBySelected';
    /** <p>「シート」プルダウン「シートを CSV でエクスポート」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_SAVE_CSV = 'sheetPulldownSaveCsv';
    /** <p>「シート」プルダウン「シートを削除」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_DELETE_SHEET = 'sheetPulldownDeleteSheet';
    /** <p>「シート」プルダウン「シートをロック」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_LOCK_SHEET = 'sheetPulldownLockSheet';
    /** <p>「シート」プルダウン「ベースアイテムをロック」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_LOCK_BASE_ITEM = 'sheetPulldownLockBaseItem';
    /** <p>「シート」プルダウン「シートプロパティを表示」を示すメニューインデックス値</p> */
    SHEET_PULLDOWN_SETTING_SHEET = 'sheetPulldownSettingSheet';

    SHEET_PULLDOWN_SHOW_PROPERTY = 'sheetPulldownShowProperty';
    
    SHEET_PULLDOWN_LOCK_BASE_ITEM_ENABLE = "sheetPulldownLockBaseItemEnable";

    SHEET_PULLDOWN_SAVE_PNG_BY_SELECTED_ENABLE = "sheetPulldownSavePngBySelectedEnable";
    constructor() {
        super();
        if (SheetPullDownListEventCenter.instance) {
            return SheetPullDownListEventCenter.instance;
        }

        SheetPullDownListEventCenter.instance = this;
    }
}

export default SheetPullDownListEventCenter;