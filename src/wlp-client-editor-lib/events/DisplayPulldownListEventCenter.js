import EventCenter from "wlp-client-common/events/EventCenter";
class DisplayPulldownListEventCenter extends EventCenter {
    DISPLAY_PULLDOWN_SHOW_GOODS = 'displayPulldownShowGoods';
    /** <p>「表示」プルダウン「 2 日前までをハイライト」を示すメニューインデックス値</p> */
    DISPLAY_PULLDOWN_HIGHLIGHTS_NEW_ITEM_BEFORE_2D = 'displayPulldownHighlightsNewItemBefore_2d';
    /** <p>「表示」プルダウン「 6 日前までをハイライト」」を示すメニューインデックス値</p> */
    DISPLAY_PULLDOWN_HIGHLIGHTS_NEW_ITEM_BEFORE_6D = 'displayPulldownHighlightsNewItemBefore_6d';
    /** <p>「表示」プルダウン「印刷ガイド枠表示」を示すメニューインデックス値</p> */
    DISPLAY_PULLDOWN_SHOW_PRINTING_RANGE_GUIDE = 'displayPulldownShowPrintingRangeGuide';

    TOOGLE_DISPLAY_PULLDOWN_SHOW_GOODS_FLAG = 'toogleDisplayPulldownShowGoodsFlag';

    TOOGLE_PULLDOWN_SHOW_PRINTING_RANGE_GUIDE = 'tooglePulldownShowPrintingRangeGuide';
    constructor() {
        super();
        if (DisplayPulldownListEventCenter.instance) {
            return DisplayPulldownListEventCenter.instance;
        }

        DisplayPulldownListEventCenter.instance = this;
    }
}

export default DisplayPulldownListEventCenter;