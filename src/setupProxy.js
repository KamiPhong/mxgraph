const proxy = require('http-proxy-middleware');
const LAMBDA_HOSTED_API = process.env.REACT_APP_REMOTE_HOST;
const SERVLET_HOSTED_API = process.env.REACT_APP_API_HOST;

module.exports = function (app) {
    app.use([
        '/auth',
        '/map',
        '/sheets',
        '/item',
        '/template',
        '/personal-setting',
        '/search',
        '/team',
        '/infomation'
    ],
        proxy({
            target: LAMBDA_HOSTED_API,
            changeOrigin: true,
        })
    );

    app.use(['/api', '/fileUpload', '/xml-to-png'],
        proxy({
            target: SERVLET_HOSTED_API,
            changeOrigin: true,
        })
    );
};