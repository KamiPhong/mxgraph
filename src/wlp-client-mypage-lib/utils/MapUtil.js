class MapUtil{
    getPermissionLabelById = permissionId => {
        return {
            1: 'global.leader', 
            2: 'global.admin',
            3: 'global.member',
            4: 'global.partner',
            5: 'global.owner'
        }[permissionId];
    }
}

export default MapUtil;