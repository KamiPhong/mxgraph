class MapUtilMemberList{
    getPermissionLabelById = permissionId => {
        return {
            1: 'global.member',
            2: 'global.member',
            3: 'global.member',
            4: 'global.partner',
            5: 'global.owner'
        }[permissionId];
    }
}

export default MapUtilMemberList;