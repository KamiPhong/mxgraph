import React from 'react';
import { withTranslation } from 'react-i18next';

import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import ContractDto from 'wlp-client-service/dto/ContractDto';
import MapConsts from 'wlp-client-common/consts/MapConsts';
import DropdownList from 'wlp-client-common/component/Dropdown/DropdownList';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import pulldownIcon from 'wlp-client-common/images/icon/pulldownIcon.png';
import ActionLogger from 'wlp-client-common/ActionLogger';


const strageUtils = new LocalStorageUtils();

class NewMapModal extends React.Component {

    constructor(props){
        super(props);
        this.inputMapname = React.createRef();
        const { selectedTeam, teamList } = this.props;
        let selectedContract = teamList[0];

        if(selectedTeam){
            selectedContract = teamList.find(team => team.contractId === selectedTeam);
        }

        this.state = {
            newMapName: 'new map',
            selectedContract: selectedContract,
            showDropList: false,
            isMapNameValid: true,
            teamList: [],
            inputFocusFlag: false
        }
    }

    componentDidMount() {
        this.setTeamListDataProvider();
    }
   
    newMapNameInput_handleChange = (e) => {
        ActionLogger.typing('User_Mypage_PopupAddMap', 'Input_Map_Name');
        const { value } = e.target;
        this.setState({ newMapName: value.substr(0, MapConsts.MAP_NAME_MAX_CHARS) });
    }

    teamSelectOption_handleChange = (e) => {

    }

    toggleDropdownList = () => {
        this.setState(({ showDropList }) => ({ showDropList: !showDropList }));
    }

    hideDropdownList = () => {
        this.setState({ showDropList: false })
    }

    dropdownListItem_handleSelect = (contract = new ContractDto()) => {
        ActionLogger.click('User_Mypage_PopupAddMap', 'Selected_Team');
        const { selectedContract } = this.state;
        if (
            selectedContract &&
            selectedContract.contractId !== contract.contractId
        ) {
            this.setState(() => ({
                selectedContract: contract,
            }), this.hideDropdownList);
        }
    }

    validateMapName = (mapName, cb) => {
        const isMapNameValid = Boolean(mapName.trim());
        
        this.setState({ 
            isMapNameValid: isMapNameValid,
            inputFocusFlag: false 
        }, () => {
            if (!isMapNameValid) {
                this.inputMapname.current.focus();
                this.inputMapname.current.value = "";
            }

            if (typeof (cb) === 'function') {
                cb();
            }
        });
    }

    onButton_handleClick = () => {
        ActionLogger.click('User_Mypage_PopupAddMap', 'Button_OK');
        const { callCreateMap, cancelHandler } = this.props;
        const { newMapName, } = this.state;

        this.validateMapName(newMapName, () => {
            const { isMapNameValid, selectedContract } = this.state;
            const { contractId } = selectedContract;

            if (isMapNameValid) {
                callCreateMap((newMapName.trim()), null, contractId, null);
                cancelHandler();
            }
        })
    }

    cancelButton_handleClick = () => {
        const { cancelHandler } = this.props;
        ActionLogger.click('User_Mypage_PopupAddMap', 'Button_Cancel');
        cancelHandler();
    }

    setTeamListDataProvider = () => {
        const { selectedTeam } = this.props;
        const userInfo = strageUtils.getUserSession();

        const contractListDp = userInfo.contractList;
        let teamList = [];
        let index;
        if (Array.isArray(contractListDp) && contractListDp.length) {
            const contractListDpLength = contractListDp.length;
            for (let i = 0; i < contractListDpLength; i++) {
                // B to C オーナー 以外、かつ、B to C のパートナー(record.teamName == null)以外
                if (RoleType.B_TO_C_OWNER !== contractListDp[i].role && contractListDp[i].teamName !== null) {
                    teamList.push(contractListDp[i]);
                    if (selectedTeam === contractListDp[i].contractId) {
                        index = teamList.length - 1;
                    }
                }
            }

            let selectedContract = teamList[index];
            if (!selectedContract) {
                selectedContract = teamList[0];
            }

            this.setState({teamList: teamList, selectedContract});
        }
    }
    handleFocusInput = () => {
        this.setState({
            inputFocusFlag: true
        })
    }

    render() {
        const {
            newMapName,
            isMapNameValid,
            selectedContract,
            teamList, //elements ContractDto
            inputFocusFlag
        } = this.state;
        let textInputClasses = inputFocusFlag ? `form-control form-control-shadow ${isMapNameValid === false && "is-invalid"}` : `form-control form-control-shadow noselect ${isMapNameValid === false && "is-invalid"}`

        const {
            t,
            isShow,
            cancelHandler,
        } = this.props;

        return (
            <CommonModal 
                className="w-432px pd-modal-inputwidth"
                onHide={cancelHandler} 
                show={isShow} 
                title={t('mypage.maps.titleInput.create')}>
                <div className="form-group mt-2 mb-2">
                    <input
                        type="text"
                        value={(newMapName)}
                        className={textInputClasses}
                        ref={this.inputMapname}
                        onBlur={e => this.validateMapName(e.target.value)}
                        onFocus={this.handleFocusInput}
                        onChange={this.newMapNameInput_handleChange}/>
                    {isMapNameValid === false && <div className="color-error position-static invaled-error invaled-error-ps noselect">
                        <strong className="noselect">{t('global.errorLabel')}</strong>&nbsp;:&nbsp;{t('message.maps.e0001')}
                    </div>}
                </div>
                <div className="form-group"
                    onClick={() => {
                        ActionLogger.click('User_Mypage_PopupAddMap', 'Select_Team');
                    }}>
                    <DropdownList 
                        itemList={teamList}                                                             
                        selectedItem={selectedContract}
                        dropdownListItem_handleSelect={this.dropdownListItem_handleSelect}
                        className="space-between pl-15px noselect"
                        pulldownIcon={pulldownIcon}
                    />
                </div>
                <div className="text-center mt-2 modal-group-btn">
                    <button type="submit" onClick={this.cancelButton_handleClick} className="btn btn-whitefl w-100px noselect-color-444">{t('global.cancel')}</button>
                    <button type="submit" onClick={this.onButton_handleClick} className="btn btn-dark w-100px noselect-color-white">{t('global.ok')}</button>
                </div>
            </CommonModal>
        );
    }
}

export default withTranslation()(NewMapModal);