import React from 'react';
import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import { withTranslation } from 'react-i18next';
import alertIcon001 from 'wlp-client-common/images/alertIcon001.png';

class DeleteTemplateModal extends React.Component {

    state = {
        isShowConfirmDeleteModal: false
    }

    deleteModal_handleOkBtn = () => {
        this.setState({ isShowConfirmDeleteModal: true });
    }

    confirmDelete_handleClick = () => {
        const { callApiDeleteTemplate, cancelHandler } = this.props;
        callApiDeleteTemplate();
        cancelHandler();
    }

    render() {
        const {
            t,
            isShow,
            cancelHandler,
        } = this.props;
        const { isShowConfirmDeleteModal } = this.state;

        return isShow ? (<React.Fragment>
            <CommonModal
                show={true}
                className="fs-12 max-width-405px"
                onHide={cancelHandler}>
                <div className="d-flex align-items-center justify-content-center content-alert-popup noselect">
                    <img src={alertIcon001} alt="" /> <span className="text-pre-line">{t('message.templates.c0001')}</span>
                </div>
                <div className="text-center mt-2 modal-group-btn">
                    <button type="submit" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-444">{t('global.cancel')}</button>
                    <button type="submit" onClick={this.deleteModal_handleOkBtn} className="btn btn-dark w-100px noselect-color-white">{t('global.ok')}</button>
                </div>
            </CommonModal>

            {/**Confirm delete modal */}
            {
                isShowConfirmDeleteModal &&
                <CommonModal
                    show={true}
                    className="w-355px"
                    backDropClass="modal-backdrop-2nd"
                    backDropModal="zindex-1070"
                    onHide={cancelHandler}>
                    <div className="d-flex align-items-center justify-content-center content-alert-popup noselect">
                        <img src={alertIcon001} alt="" /> <span className="font-size-12">{t('message.global.c0001')}</span>
                    </div>
                    <div className="text-center mt-2 modal-group-btn">
                        <button type="submit" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-444">{t('global.cancel')}</button>
                        <button type="submit" onClick={this.confirmDelete_handleClick} className="btn btn-dark w-100px noselect-color-white">{t('global.ok')}</button>
                    </div>
                </CommonModal>}
        </React.Fragment>) : null
    }
}

export default withTranslation()(DeleteTemplateModal);