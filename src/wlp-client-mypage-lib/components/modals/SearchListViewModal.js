import React, { Component } from 'react';
import backIcon2 from 'wlp-client-common/images/backIcon2@4x.png';
import rightArrowIcon from 'wlp-client-common/images/rightArrowIconWhite.png';
import buttonBarSkinLeftOver from 'wlp-client-common/images/buttonBarSkin_left_over@8x.png';
import buttonBarSkinRightOver from 'wlp-client-common/images/buttonBarSkin_right_over@8x.png';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import { SEARCH_RESULT_TYPE } from 'wlp-client-mypage-lib/consts/UserSetting';
import Link from 'wlp-client-common/component/Link';
import { openEditorPage } from 'wlp-client-common/utils/EdotorUtil';
import {htmlDecode} from 'wlp-client-common/utils/TextUtils'
import ActionLogger from 'wlp-client-common/ActionLogger';

const wlpDateUtil = new WlpDateUtil();

class SearchListViewModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    renderKindDataSearch = (record) => {
        const { kindView, t } = this.props;
        switch(kindView) {
            case SEARCH_RESULT_TYPE.MAPS:
                return (
                    <div className="lh-1d4 noselect">
                        <span className="text-overflow-ms width-100pc noselect">{t('search.map')}: {<span className="noselect" dangerouslySetInnerHTML={{ __html: htmlDecode(record.mapName) }} />}</span><br/>
                    </div>
                );
            case SEARCH_RESULT_TYPE.SHEETS:
                return (
                    <div className="lh-1d4">
                        <span className="text-overflow-ms width-100pc noselect">{t('search.map')}: {<span className="noselect" dangerouslySetInnerHTML={{ __html: htmlDecode(record.mapName) }} />}</span><br/>
                        <span className="text-overflow-ms width-100pc noselect">{t('search.sheet')}: {<span className="noselect" dangerouslySetInnerHTML={{ __html: htmlDecode(record.sheetName) }} />}</span>
                    </div>
                );
            case SEARCH_RESULT_TYPE.ITEMS:
                return (
                    <div className="lh-1d4">
                        <span className="text-overflow-ms width-100pc noselect">{t('search.map')}: {<span className="noselect" dangerouslySetInnerHTML={{ __html: htmlDecode(record.mapName) }} />}</span><br/>
                        <span className="text-overflow-ms width-100pc noselect">{t('search.sheet')}: {<span className="noselect" dangerouslySetInnerHTML={{ __html: htmlDecode(record.sheetName) }} />}</span><br/>
                        <span className="text-overflow-ms width-100pc noselect">{t('search.item')}: {<span className="noselect" dangerouslySetInnerHTML={{ __html: htmlDecode(record.itemText) }} />}</span>
                    </div>
                );
            default:
                return;
        }
    }

    renderDetailSearch = (record) => {
        const { kindView, t } = this.props;
        switch(kindView) {
            case SEARCH_RESULT_TYPE.MAPS:
                return (
                    <span className="text-overflow-ms width-140px noselect">{t('search.owner')}: {htmlDecode(record.createUserName)}</span> 
                );
            case SEARCH_RESULT_TYPE.SHEETS:
            case SEARCH_RESULT_TYPE.ITEMS:
                return ( 
                    <span className="text-overflow-ms width-140px noselect">{t('search.author')}: {htmlDecode(record.createUserName)}</span>
                );
            default:
                return;
        }
    }

    _openEditorPage = (record) => {
        if (record.itemId === null) {
            console.log("open editor map")
            openEditorPage(record.mapId, record.sheetId)
        } else {
            openEditorPage(record.mapId, record.sheetId, record.itemId)
        }
    }

    componentDidMount() {
        //[ThuyTV] prevent body scroll while result modal is showed
        //Following comment on ticket redmine No #66516
        document.body.classList.add('overflow-hidden');
    }

    componentWillUnmount(){
        document.body.classList.remove('overflow-hidden');
    }
    
    render() {
        const {
            searchList, 
            setShowSearchListModal, 
            t, 
            refModalSearch,
            iconView,
            refModal } = this.props;

        const countRecord = searchList && searchList.entries && Array.isArray(searchList.entries) ? searchList.entries.length : 1;

        return (
            (<React.Fragment>
                <div className="modal modal-gr modal-search d-block" ref={refModal}>
                    <div className={`modal-dialog-centered modal-full`} role="document">
                        <div className="modal-content mypage-sidebar-bg">
                            <div className="modal-body modal-body-full pt-0">
                                <div className="modal-full-header">
                                    <span className="link-back-mypage" 
                                            onClick={() => {
                                                ActionLogger.click('User_Mypage_SearchResult', `Button_Back`);
                                                setShowSearchListModal(false);
                                            }}>
                                        <img alt="" className="back-icon-2 noselect" src={backIcon2} width="16" /> <span className="btn-back-sm noselect noselect-color-aqua">{t('search.back')}</span>
                                    </span>
                                    <div className="modal-full-paginate">
                                        <span className="number-paginate noselect noselect-color-cccccc">1-{countRecord} of {countRecord} </span> 
                                        <button className="btn-reset paginate-previous">
                                            <img alt="" className="right-arrow-btn-icon noselect" src={buttonBarSkinLeftOver} width="28" />
                                            <img alt="" className="right-arrow-icon noselect" src={rightArrowIcon} width="6" />
                                        </button>
                                        <button className="btn-reset paginate-next">
                                            <img alt="" className="left-arrow-btn-icon noselect" src={buttonBarSkinRightOver} width="28" />
                                            <img alt="" className="left-arrow-icon noselect" src={rightArrowIcon} width="6" />
                                        </button>
                                    </div>
                                </div>
                                <div ref={refModalSearch} className="modal-full-body">
                                    {
                                        searchList && 
                                        searchList.entries &&
                                        Array.isArray(searchList.entries) &&
                                        searchList.entries.map((record, index) => {
                                            return (
                                                <Link key={index} className="modal-full-item-link" target="_blank" onClick={() => this._openEditorPage(record)}>
                                                    <div className="modal-full-item">
                                                        <div className="modal-search-header noselect">
                                                            <img alt="" className="back-icon-2 noselect" src={iconView} width="37" />
                                                        </div>
                                                        <div className="modal-search-content noselect">
                                                            { this.renderKindDataSearch(record) }
                                                        </div>
                                                        <div className="modal-search-footer noselect">
                                                            <span className="text-overflow-ms width-150px noselect">{t('search.team')}: {htmlDecode(record.teamName)}</span><br/>
                                                            { this.renderDetailSearch(record) }<br/>
                                                            <span className="noselect">{t('search.lastUpdated')}: {wlpDateUtil.formatDateTime(record.updateDate)}</span>
                                                        </div>
                                                    </div>
                                                </Link>
                                            )
                                        })
                                    }
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>)
        )
    }
}

export default SearchListViewModal;
