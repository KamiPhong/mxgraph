import React from 'react';
import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import { withTranslation } from 'react-i18next';
import MapConsts from 'wlp-client-common/consts/MapConsts';

class CopyMapModal extends React.Component {

    state = {
        mapName: `copy ${this.props.selectedMap.mapTitle}`.substr(0, MapConsts.MAP_NAME_MAX_CHARS),
        isMapNameValid: true,
        isFocusInput: false
    }

    constructor(props) {
        super(props);
        this.inputMapname = React.createRef();
    }

    mapName_handleChange = e => {
        const { value } = e.target;
        this.setState({ mapName: value.substr(0, MapConsts.MAP_NAME_MAX_CHARS) });
    }

    validateMapName = (mapName, cb) => {
        this.setState({ isMapNameValid: Boolean(mapName),
            isFocusInput: false }, () => {
            if (!mapName) {
                this.inputMapname.current.focus();
            }

            if (typeof (cb) === 'function') {
                cb();
            }
        });
    }

    copyButton_clickHandler = () => {
        const { mapName } = this.state;
        this.validateMapName(mapName, () => {
            const { isMapNameValid } = this.state;
            const { callCopyMap, selectedMap, cancelHandler } = this.props;
            if (isMapNameValid) {
                callCopyMap((mapName.trim()), selectedMap);
                cancelHandler();
            }
        });
        this.props.goToStartPage();
    }
    focusInputHandle = () => {
        this.setState({
            isFocusInput: true
        })
    }

    render() {
        const {
            t,
            isShow,
            cancelHandler
        } = this.props;

        const { mapName, isMapNameValid, isFocusInput } = this.state;

        return (
            <CommonModal
                className="w-432px pd-modal-inputwidth"
                onHide={cancelHandler}
                show={isShow}
                title={t('mypage.maps.list.operation.duplicate')}>
                <div className="form-group mt-2">
                    <input
                        onBlur={e => this.validateMapName(e.target.value)}
                        ref={this.inputMapname}
                        type="text"
                        className={`form-control form-control-shadow ${isMapNameValid === false && "is-invalid"} ${isFocusInput ? "" : " noselect"}`}
                        value={(mapName)}
                        onChange={this.mapName_handleChange}
                        onFocus={this.focusInputHandle} />
                    {isMapNameValid === false && <div className="color-error position-static invaled-error invaled-error-ps noselect">
                        <strong className="noselect">{t('global.errorLabel')}</strong>&nbsp;:&nbsp;{t('message.maps.e0001')}
                    </div>}
                </div>
                <div className="text-center mt-2 modal-group-btn">
                    <button type="submit" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-444">{t('global.cancel')}</button>
                    <button type="submit" onClick={this.copyButton_clickHandler} className="btn btn-dark w-100px noselect-color-white">{t('global.ok')}</button>
                </div>
            </CommonModal>
        )
    }
}

export default withTranslation()(CopyMapModal);