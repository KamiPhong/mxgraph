import React from 'react';
import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import { withTranslation } from 'react-i18next';
import ChangeInfoTemplateConfigDto from 'wlp-client-service/dto/template/ChangeInfoTemplateConfigDto';
import TemplateService from 'wlp-client-service/service/TemplateService';
import ContractType from 'wlp-client-service/consts/ContractType';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import SessionDto from 'wlp-client-service/dto/SessionDto';
import LogonDto from 'wlp-client-service/dto/auth/LogonDto';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import i18n from "i18n";

const TEMPLATE_INFOS = {
    TEMPLATE_TITLE: 'TEMPLATE_TITLE',
    TEMPLATE_PUBLISHER: 'TEMPLATE_PUBLISHER',
    AUTHOR: 'AUTHOR',
    CATEGORY_NAME: 'CATEGORY_NAME',
    DETAIL: 'DETAIL'
}
const templateService = new TemplateService();
const localStorageUtil = new LocalStorageUtils();

class ModifyTemplateModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDropdownTemplate: false,
            validateInputTemplate: {
                templateTitle: true,
                templatePublisher: true,
                author: true,
                detail: true
            },
            templateInfo: {
                templateTitle: '',
                templatePublisher: '',
                author: '',
                categoryName: '',
                detail: ''
            },
            isShow: true,
            isFocusTemplateTitle: false,
            isFocusTemplatePublisher: false,
            isFocusTemplateAuthor: false,
            isFocusTemplateDetail: false,
        }
        this.bodyTemplate = React.createRef();
        this.inputTemplateTitle = React.createRef();
        this.inputTemplatePublisher = React.createRef();
        this.inputAuthor = React.createRef();
    }

    setStateTemplateInfo = (type, value) => {
        switch(type) {
            case TEMPLATE_INFOS.TEMPLATE_TITLE:
                this.setState(({ templateInfo }) => ({templateInfo: {...templateInfo, templateTitle: value}}));
                break;
            case TEMPLATE_INFOS.TEMPLATE_PUBLISHER:
                this.setState(({ templateInfo }) => ({templateInfo: {...templateInfo, templatePublisher: value}}));
                break;
            case TEMPLATE_INFOS.AUTHOR:
                this.setState(({ templateInfo }) => ({templateInfo: {...templateInfo, author: value}}));
                break;
            case TEMPLATE_INFOS.CATEGORY_NAME:
                this.handelShowDropdownTemplate();
                this.setState(({ templateInfo }) => ({templateInfo: {...templateInfo, categoryName: value}}));
                break;
            case TEMPLATE_INFOS.DETAIL:
                this.setState(({ templateInfo }) => ({templateInfo: {...templateInfo, detail: value}}));
                break;
            default:
                break;
        }
    }

    autoFocusInputError = (templateTitle, templatePublisher, author) => {
        if (!templateTitle) {
            return this.inputTemplateTitle.current.focus();
        } 
        
        if (!templatePublisher) {
            return this.inputTemplatePublisher.current.focus();
        } 
        
        if (!author) {
            return this.inputAuthor.current.focus();
        }
    }

    validateTemplateInfo = (e,isubmit = false) => {
        const templateInfo = {...this.state.templateInfo};
        let templateTitle = templateInfo.templateTitle.trim() !== '';
        let templatePublisher = templateInfo.templatePublisher.trim() !== '';
        let author = templateInfo.author.trim() !== '';
        let {isFocusTemplateTitle, isFocusTemplatePublisher, isFocusTemplateAuthor} = this.state;
        if (e && e.target && this.inputTemplateTitle && this.inputTemplateTitle.current.id === e.target.id) {
            isFocusTemplateTitle = false
        }
        if (e && e.target && this.inputTemplatePublisher && this.inputTemplatePublisher.current.id === e.target.id) {
            isFocusTemplatePublisher = false
        }
        if (e && e.target && this.inputAuthor && this.inputAuthor.current.id === e.target.id) {
            isFocusTemplateAuthor = false
        }

        this.setState({
            validateInputTemplate: {
                templateTitle: templateTitle,
                templatePublisher: templatePublisher,
                author: author,
            },
            templateInfo: {
                ...templateInfo,
                templateTitle: templateTitle ? templateInfo.templateTitle : "",
                templatePublisher: templatePublisher ? templateInfo.templatePublisher : "",
                author: author ? templateInfo.author : ""
            },
            isFocusTemplateTitle: isFocusTemplateTitle,
            isFocusTemplatePublisher: isFocusTemplatePublisher,
            isFocusTemplateAuthor: isFocusTemplateAuthor
        });
        

        if (!templateTitle || !templatePublisher || !author) {
            if (isubmit) {
                this.autoFocusInputError(templateTitle, templatePublisher, author);
            }
            return false;
        } else {
            return true;
        }
    }

    modifyButton_clickHandler = async () => {
        const validated = this.validateTemplateInfo(true);
        const { templateInfo } = this.state;
        const { currentTemplateSelected, currentTemplateOnAction } = this.props;

        if (validated) {
            let requestDto = new ChangeInfoTemplateConfigDto();
            let session = new SessionDto();
            let logonInfo = new LogonDto();
            const { callGetTemplateList, hideCurrenActionModal} = this.props;
            logonInfo = localStorageUtil.getUserSession();
            session = logonInfo.sessionDto;

            requestDto.templateId = currentTemplateOnAction.templateId;
            requestDto.templateTitle = (templateInfo.templateTitle);
            requestDto.publisher = (templateInfo.templatePublisher);
            requestDto.author = (templateInfo.author);
            requestDto.categoryId = currentTemplateSelected.categoryId;
            requestDto.detail = (templateInfo.detail);

            requestDto = {
                ...requestDto,
                "author": requestDto.author,
                "categoryId": requestDto.categoryId,        
                "contractId": -1,
                "contractType": ContractType.team, 
                "currentState": CurrentState.MAP,
                "detail": requestDto.detail,        
                "localeCode":  session.localeCode,        
                "publisher": requestDto.publisher,
                "templateId": requestDto.templateId,        
                "templateTitle": requestDto.templateTitle
            }
            this.setState({isShow: false});
            templateService.changeInfoTemplate(requestDto).then(result => {
                if (result && result.isSucceed) {
                    window.scrollTo(0, 0);
                    callGetTemplateList(currentTemplateSelected.categoryId);
                }
            });

            hideCurrenActionModal();

        }
        
    }

    eventsClick_cancelHandler = () => {
        this.setState({
            validateInputTemplate: {
                templateTitle: true,
                templatePublisher: true,
                author: true,
                detail: true
            }
        });
        this.validateTemplateInfo();
        this.props.hideCurrenActionModal();
    }

    handelShowDropdownTemplate= () => {
        this.setState(({ showDropdownTemplate }) => ({
            showDropdownTemplate: !showDropdownTemplate
        }));
    }

    setWrapperRefTemplate = (node) => {
        this.wrapperRefTemplate = node;
    }

    handleClickOutsideDivTemplate = (event) => {
        if (this.wrapperRefTemplate && !this.wrapperRefTemplate.contains(event.target)) {
            this.setState({showDropdownTemplate: false});
        }
    }

    getCategoryName = (categoryList, categoryId) => {
        const localeCode = i18n.language;

        for (const category of categoryList) {
            if (category.id === categoryId) {
                return category.label = (localeCode === LocaleConst.en_US) ? category.englishLabel : category.japaneseLabel;
            }
        }
    }

    setStateTemplateDetail = () => {
        const { currentTemplateSelected, categoryList } = this.props;
        
        if (currentTemplateSelected) {
            this.setState({
                templateInfo: {
                    templateTitle: (currentTemplateSelected.templateTitle),
                    templatePublisher: (currentTemplateSelected.templatePublisher),
                    author: (currentTemplateSelected.author),
                    categoryName: this.getCategoryName(categoryList, currentTemplateSelected.categoryId),
                    detail: (currentTemplateSelected.detail)
                }
            });
        }
    }

    focusInputTemplateTitle = () => {
        this.setState({
            isFocusTemplateTitle: true
        })
    }
    focusInputTemplatePublisher= () => {
        this.setState({
            isFocusTemplatePublisher: true
        })
    }
    focusInputTemplateAuthor= () => {
        this.setState({
            isFocusTemplateAuthor: true
        })
    }
    focusInputTemplateDetail= () => {
        this.setState({
            isFocusTemplateDetail: true
        })
    }
    blurInputTemplateDetail= () => {
        this.setState({
            isFocusTemplateDetail: false
        })
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutsideDivTemplate);
    }

    componentDidMount() {
        this.setStateTemplateDetail();
        document.addEventListener("mousedown", this.handleClickOutsideDivTemplate);
    }

    render() {
        const { t } = this.props;
        const { isShow } = this.state;


        const { validateInputTemplate, showDropdownTemplate, templateInfo, isFocusTemplateTitle,
            isFocusTemplatePublisher, isFocusTemplateAuthor, isFocusTemplateDetail } = this.state;

        return (<CommonModal
            className="modal-width-380px pd-modal-inputwidth"
            onHide={() => this.eventsClick_cancelHandler()}
            show={isShow}
            title={t('mypage.templates.infoInput.title.update')}>
            <div className="form-group mt-2">
                <input
                    id="inputTitle"
                    type="text"
                    ref={this.inputTemplateTitle}
                    onBlur={(e) => this.validateTemplateInfo(e)}
                    onFocus={this.focusInputTemplateTitle}
                    className={`form-control focus-transparent form-control-shadow ${validateInputTemplate.templateTitle === false && "is-invalid"} ${isFocusTemplateTitle ? "" : " noselect"}`}
                    value={templateInfo.templateTitle}
                    placeholder={t('mypage.templates.infoInput.title.prompt')}
                    onChange={(e) => this.setStateTemplateInfo(TEMPLATE_INFOS.TEMPLATE_TITLE, e.target.value)} />
                {validateInputTemplate.templateTitle === false && <div className="pl-2 color-error position-static invaled-error invaled-error-ps">
                    <strong className='noselect'>{t('global.errorLabel')}</strong>&nbsp;:&nbsp;{t('message.templates.e0001')}
                </div>}
                <input
                    id="inputPublisher"
                    type="text"
                    ref={this.inputTemplatePublisher}
                    onBlur={(e) => this.validateTemplateInfo(e)}
                    onFocus={this.focusInputTemplatePublisher}
                    className={`form-control focus-transparent form-control-shadow mt-3 ${validateInputTemplate.templatePublisher === false && "is-invalid"} ${isFocusTemplatePublisher ? "" : " noselect"}`}
                    value={templateInfo.templatePublisher}
                    placeholder={t('mypage.templates.infoInput.publisher.prompt')}
                    onChange={(e) => this.setStateTemplateInfo(TEMPLATE_INFOS.TEMPLATE_PUBLISHER, e.target.value)} />
                {validateInputTemplate.templatePublisher === false && <div className="pl-2 color-error position-static invaled-error invaled-error-ps">
                    <strong>{t('global.errorLabel')}</strong>&nbsp;:&nbsp;{t('message.templates.e0002')}
                </div>}
                <input
                    type="text"
                    id="inputAuthor"
                    ref={this.inputAuthor}
                    onBlur={(e) => this.validateTemplateInfo(e)}
                    onFocus={this.focusInputTemplateAuthor}
                    className={`form-control focus-transparent form-control-shadow mt-3 ${validateInputTemplate.author === false && "is-invalid"} ${isFocusTemplateAuthor ? "" : " noselect"}`}
                    value={templateInfo.author}
                    placeholder={t('mypage.templates.infoInput.author.prompt')}
                    onChange={(e) => this.setStateTemplateInfo(TEMPLATE_INFOS.AUTHOR, e.target.value)} />
                {validateInputTemplate.author === false && <div className="pl-2 color-error position-static invaled-error invaled-error-ps">
                    <strong>{t('global.errorLabel')}</strong>&nbsp;:&nbsp;{t('message.templates.e0003')}
                </div>}

                <div className="form-group dropdown dropdown-personal mt-3">
                    <div ref={this.setWrapperRefTemplate}>
                        <button className={`btn btn-secondary dropdown-toggle-mt-focus dropdown-toggle ${showDropdownTemplate?'focus-btn-dropdown':''}`} 
                            type="button"
                            onClick={() => this.handelShowDropdownTemplate()}>
                            {
                                templateInfo.categoryName
                            }
                        </button>
                        { showDropdownTemplate && (
                            <div className="dropdown-menu d-block">
                                {   
                                    <div 
                                        onClick={() => this.setStateTemplateInfo(TEMPLATE_INFOS.CATEGORY_NAME, templateInfo.categoryName)}
                                        className={`dropdown-item active font-size-14px noselect`}>
                                        {templateInfo.categoryName}
                                    </div>
                                }
                            </div>
                        )}
                    </div>
                </div>

                <textarea 
                    id='messageTextArea'
                    onChange={(e) => this.setStateTemplateInfo(TEMPLATE_INFOS.DETAIL, e.target.value)}
                    onFocus={this.focusInputTemplateDetail}
                    onBlur={this.blurInputTemplateDetail}
                    className={`form-control height-90px focus-transparent mt-3 form-control-textarea-shadow placeholder-italic content-detail-new-template ${templateInfo.detail!==''?'bg-textarea-isvalid':''} ${isFocusTemplateDetail ? "" : " noselect"}`}
                    cols="30"
                    rows="4" 
                    value={templateInfo.detail}
                    placeholder={t('mypage.templates.infoInput.detail.prompt')}></textarea>
            </div>
            <div className="text-center mt-2 modal-group-btn">
                <button type="submit" onClick={() => this.eventsClick_cancelHandler()} className="btn btn-whitefl w-100px noselect-color-444">{t('global.cancel')}</button>
                <button type="submit" onClick={() => this.modifyButton_clickHandler()} className="btn btn-dark w-100px noselect-color-white">{t('global.ok')}</button>
            </div>
        </CommonModal >);
    }
}

export default withTranslation()(ModifyTemplateModal);