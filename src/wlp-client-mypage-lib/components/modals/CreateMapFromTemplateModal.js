import React from 'react';
import { withTranslation } from 'react-i18next';

import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import ContractDto from 'wlp-client-service/dto/ContractDto';

import MapKind from 'wlp-client-service/consts/MapKind';
import DropdownList from 'wlp-client-common/component/Dropdown/DropdownList';
import pulldownIcon from 'wlp-client-common/images/icon/pulldownIcon.png';

class CreateMapFromTemplateModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedContract: null,
            isFocusInput: false
        }

        this.inputMapNameRef = React.createRef();
    }

    dropdownListItem_handleSelect = (contract = new ContractDto()) => {
        this.setState(() => ({
            selectedContract: contract,
        }));
    }

    onButton_handleClick = async () => {
        const { callCreateMap, cancelHandler, template, setStateIsMapNameValid, validateMapName, newMapName, setHideCount } = this.props;
        const { selectedContract } = this.state;

        await setStateIsMapNameValid(validateMapName(newMapName));
        const { isMapNameValid } = this.props;
        const contractId = selectedContract.contractId;
        setHideCount();

        if (isMapNameValid) {
            callCreateMap((newMapName), template, contractId, MapKind.all);
            cancelHandler();
        }else{
            this.inputMapNameRef.current.focus();
        }
    }

    UNSAFE_componentWillReceiveProps({ teamList, template }) {
        if (teamList && Array.isArray(teamList)) {
            if (!this.state.selectedContract) {
                this.setState({
                    selectedContract: teamList[0]
                })
            }
        }
    }

    componentDidMount(){
        this.UNSAFE_componentWillReceiveProps({teamList: this.props.teamList});
    }
    onBlurHandle = (e) => {
        const {setStateIsMapNameValid, validateMapName} = this.props;
        setStateIsMapNameValid(validateMapName(e.target.value));
        this.setState({
            isFocusInput: false
        })
    }

    onFocusHandle = () => {
        this.setState({
            isFocusInput: true
        })
    }

    render() {
        const {
            selectedContract,
            isFocusInput
        } = this.state;

        const {
            t,
            isShow,
            cancelHandler,
            teamList, //elements ContractDto,
            isMapNameValid,
            newMapNameInput_handleChange,
            newMapName,
        } = this.props;

        return (
            <CommonModal className="w-432px pd-modal-inputwidth" onHide={cancelHandler} show={isShow}>
                <h5 className="font-weight-bold email-color text-create-template font-size-16px noselect">{t('mypage.maps.titleInput.createFromTemplate.title')}</h5>
                <p className="email-color mb-10px fs-14px noselect">※ {t('mypage.maps.titleInput.createFromTemplate.message')}</p>
                <div className="form-group mt-2 custom-form-group">
                    <input
                        ref={this.inputMapNameRef}
                        type="text"
                        maxLength="200"
                        value={(newMapName)}
                        onChange={(e) => newMapNameInput_handleChange(e)}
                        onBlur={this.onBlurHandle}
                        onFocus={this.onFocusHandle}
                        className={`form-control form-control-shadow ${!isMapNameValid ? 'is-invalid' : ''} ${isFocusInput ? "" : " noselect"}`} />
                    {isMapNameValid === false && <div className="custom-invaled-error-ps color-error position-static invaled-error invaled-error-ps noselect">
                        <strong className="noselect">{t('global.errorLabel')}</strong> &nbsp;:&nbsp; {t('message.maps.e0001')}
                    </div>}
                </div>
                <div className="form-group mt-2">
                    <DropdownList 
                        itemList={teamList}
                        selectedItem={selectedContract}
                        dropdownListItem_handleSelect={this.dropdownListItem_handleSelect}
                        className="space-between pl-15px noselect"
                        pulldownIcon={pulldownIcon}
                    />
                </div>
                <div className="text-center mt-22px modal-group-btn">
                    <button type="submit" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-444">{t('global.cancel')}</button>
                    <button type="submit" onClick={this.onButton_handleClick} className="btn btn-dark w-100px noselect-color-white">{t('global.ok')}</button>
                </div>
            </CommonModal>
        );
    }
}

export default withTranslation()(CreateMapFromTemplateModal);