import React from 'react';
import ReactDOM from 'react-dom';
import { withTranslation } from 'react-i18next';

import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import ValidateErrorMessage from 'wlp-client-common/component/message/ValidateErrorMessage';
import CategoryRecordDto from 'wlp-client-service/dto/template/CategoryRecordDto';
import MapRecordDetailDto from 'wlp-client-service/dto/map/MapRecordDetailDto';
import TemplateRecordDetailDto from 'wlp-client-service/dto/template/TemplateRecordDetailDto';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import i18n from "i18n";
import _ from "lodash";
import GetCategoryListConfigDto from 'wlp-client-service/dto/template/GetCategoryListConfigDto';
import TemplateService from 'wlp-client-service/service/TemplateService';
import GetCategoryListDto from 'wlp-client-service/dto/template/GetCategoryListDto';
import LocaleCode from 'wlp-client-service/consts/LocaleCode';

const templateService = new TemplateService();

class NewTemplateModal extends React.Component {
    constructor(props) {
        super(props);
        const { categoryList, selectedMapDetail } = this.props;
        const { mapTitle, templateAuthor } = selectedMapDetail;

        this.state = {
            templateTitle: mapTitle ? mapTitle : '',
            templateAuthor: templateAuthor ? templateAuthor : '',
            templatePublisher: '',
            templateDetail: '',
            selectedCategory: Array.isArray(categoryList) ? categoryList[0] : {},
            showDropList: false,
            isTemplateNameValid: true,
            isPublishedByValid: false,
            isCreatedByValid: false,
            showDropdownTemplate: false,
            showShadowBoder: true,
            categoryList: [],
            isInputNameFocus: false,
            isInputPublishFocus: false,
            isInputCreateFocus: false,
            isTextAreaMessageFocus: false,
        }
    }

    newTemplateNameInput_handleChange = (e) => {
        this.setState({
            templateTitle: e.target.value
        })
    }

    newTemplateNameInput_handleBlur = (e) => {
        this.setState({
            templateTitle: e.target.value.trim(),
            isTemplateNameValid: this.validateTemplateInput(e.target.value.trim()),
            isInputNameFocus: false
        })
    }

    newPublishedByInput_handleChange = (e) => {
        this.setState({
            templatePublisher: e.target.value
        })
    }

    newPublishedByInput_handleBlur = (e) => {
        this.setState({
            templatePublisher: e.target.value.trim(),
            isPublishedByValid: !this.validateTemplateInput(e.target.value.trim()),
            isInputPublishFocus: false
        })
    }

    removePlaceholderDetail = (e) => {
        this.setState({
            showShadowBoder: false,
            isTextAreaMessageFocus: true
        })
    }

    newCreatedByInput_handleChange = (e) => {
        this.setState({
            templateAuthor: e.target.value
        })
    }

    newCreatedByInput_handleBlur = (e) => {
        this.setState({
            templateAuthor: e.target.value.trim(),
            isCreatedByValid: !this.validateTemplateInput(e.target.value.trim()),
            isInputCreateFocus: false
        })
    }


    newTemplateDetailTextarea_handleChange = (e) => {
        this.setState({
            templateDetail: e.target.value
        })
        if (e.target.value === '') {
            this.setState({
                showShadowBoder: false
            })
        }
    }

    newTemplateDetailTextarea_handleBlur = (e) => {
        this.setState({
            templateDetail: e.target.value,
            isTextAreaMessageFocus: false
        })
        if (e.target.value === '') {
            this.setState({
                showShadowBoder: true
            })
        }
    }

    toggleDropdownList = () => {
        this.setState(({ showDropList }) => ({ showDropList: !showDropList }));
    }

    hideDropdownList = () => {
        this.setState({ showDropList: false })
    }

    dropdownListItem_handleSelect = (category = new CategoryRecordDto()) => {
        const { selectedCategory } = this.state;
        if (
            selectedCategory &&
            selectedCategory.categoryId !== category.id
        ) {
            this.setState(() => ({
                selectedCategory: category,
            }), this.hideDropdownList);
        }
    }

    validateTemplateInput = (templateInput) => {
        return Boolean(templateInput);
    }

    setFocusInput = () => {
        const { templateTitle, templatePublisher, templateAuthor } = this.state;
        if (templateTitle === '') {
            ReactDOM.findDOMNode(this.refs.templateTitle).focus();
        } else if (templatePublisher === '') {
            ReactDOM.findDOMNode(this.refs.templatePublisher).focus();
        } else if (templateAuthor === '') {
            ReactDOM.findDOMNode(this.refs.templateAuthor).focus();
        }
    }
    onButton_handleClick = () => {
        const { callCreateTemplate, cancelHandler, selectedMapDetail } = this.props;
        const { templateTitle, templatePublisher, templateAuthor, templateDetail } = this.state;
        this.setState(() => ({
            isTemplateNameValid: this.validateTemplateInput(templateTitle),
            isPublishedByValid: !this.validateTemplateInput(templatePublisher),
            isCreatedByValid: !this.validateTemplateInput(templateAuthor)
        }), () => {
            const { isTemplateNameValid, isPublishedByValid, isCreatedByValid, selectedCategory } = this.state;
            this.setFocusInput();
            if (isTemplateNameValid && !isPublishedByValid && !isCreatedByValid) {
                let newTemplateSrc = new MapRecordDetailDto();
                let inputTemplateDetail = new TemplateRecordDetailDto();

                newTemplateSrc = _.cloneDeep(selectedMapDetail);
                inputTemplateDetail.templateTitle = (templateTitle);
                inputTemplateDetail.categoryId = selectedCategory.id;
                inputTemplateDetail.japaneseLabel = (selectedCategory.japaneseLabel);
                inputTemplateDetail.englishLabel = (selectedCategory.englishLabel);
                inputTemplateDetail.author = (templateAuthor);
                inputTemplateDetail.detail = (templateDetail);
                inputTemplateDetail.templatePublisher = (templatePublisher);

                callCreateTemplate(newTemplateSrc, inputTemplateDetail); // Update MapRecordDetailDto , TemplateRecordDetailDto
                cancelHandler();
            }
        })
    }

    handelShowDropdownTemplate = () => {
        this.setState(({ showDropdownTemplate }) => ({
            showDropdownTemplate: !showDropdownTemplate
        }));
    }

    setWrapperRefTemplate = (node) => {
        this.wrapperRefTemplate = node;
    }

    handleClickOutsideDivTemplate = (event) => {
        if (this.wrapperRefTemplate && !this.wrapperRefTemplate.contains(event.target)) {
            this.setState({ showDropdownTemplate: false });
        }
    }


    callGetCategoryList = async () => {
        const requestDto = new GetCategoryListConfigDto();
        const result = await templateService.getCategoryList(requestDto);
        this.callGetCategoryList_handleResult(result);
    }

    callGetCategoryList_handleResult = (result = new GetCategoryListDto()) => {
        if (result.isSucceed === false) { return };

        const { i18n } = this.props;

        for (let i = 0; i < result.categoryList.length; i++) {
            result.categoryList[i].label = (i18n.language === LocaleCode.en_US) ? result.categoryList[i].englishLabel : result.categoryList[i].japaneseLabel;
        }

        this.setState({categoryList: result.categoryList});
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutsideDivTemplate);
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutsideDivTemplate);
        this.callGetCategoryList();
    }

    newTemplateNameInput_handleFocus = () => {
        this.setState({
            isInputNameFocus: true
        })
    }
    newPublishedByInput_handleFocus = () => {
        this.setState({
            isInputPublishFocus: true
        })
    }
    newCreatedByInput_handleFocus = () => {
        this.setState({
            isInputCreateFocus: true
        })
    }

    render() {
        const {
            templateTitle,
            templatePublisher,
            templateAuthor,
            templateDetail,
            isTemplateNameValid,
            isPublishedByValid,
            isCreatedByValid,
            selectedCategory,
            showDropdownTemplate,
            showShadowBoder,
            categoryList,
            isInputNameFocus,
            isInputPublishFocus,
            isInputCreateFocus,
            isTextAreaMessageFocus,
        } = this.state;

        const {
            t,
            isShow,
            cancelHandler
        } = this.props;

        let category;

        if (selectedCategory) {
            category = selectedCategory;
        } else if (categoryList[0]) {
            category = categoryList[0];
        }
        const localeCode = i18n.language;
        category.label = (localeCode === LocaleConst.en_US) ? category.englishLabel : category.japaneseLabel;

        return (
            <CommonModal className="modal-w380 pd-modal-inputwidth" onHide={cancelHandler} show={isShow} title={t('mypage.templates.infoInput.title.new')}>
                <div className="form-group mt-2 mb-2 position-relative">
                    <input
                        type="text"
                        maxLength="200"
                        ref="templateTitle"
                        value={(templateTitle)}
                        onChange={this.newTemplateNameInput_handleChange}
                        onBlur={this.newTemplateNameInput_handleBlur}
                        onFocus={this.newTemplateNameInput_handleFocus}
                        className={"form-control form-control-shadow" + (isTemplateNameValid === false ? " is-invalid" : "") + (isInputNameFocus ? "" : " noselect")}
                        placeholder={t('mypage.templates.infoInput.title.prompt')} />
                    {isTemplateNameValid === false && <ValidateErrorMessage message={t('message.templates.e0001')} classAdd="position-static" />}
                </div>

                <div className="form-group mt-2 mb-10px5 position-relative">
                    <input
                        type="text"
                        maxLength="200"
                        ref="templatePublisher"
                        value={(templatePublisher)}
                        onChange={this.newPublishedByInput_handleChange}
                        onBlur={this.newPublishedByInput_handleBlur}
                        onFocus={this.newPublishedByInput_handleFocus}
                        className={"form-control" + (templatePublisher === '' ? " form-control-textarea-shadow" : " form-control-shadow") + (isPublishedByValid ? " is-invalid" : "") + (isInputPublishFocus ? "" : " noselect")}
                        placeholder={t('mypage.templates.infoInput.publisher.prompt')} />
                    {isPublishedByValid && <ValidateErrorMessage message={t('message.templates.e0002')} classAdd="position-static" />}
                </div>

                <div className="form-group mt-1 mb-3 position-relative">
                    <input
                        type="text"
                        maxLength="200"
                        ref="templateAuthor"
                        value={(templateAuthor)}
                        onChange={this.newCreatedByInput_handleChange}
                        onBlur={this.newCreatedByInput_handleBlur}
                        onFocus={this.newCreatedByInput_handleFocus}
                        className={"form-control" + (templateAuthor === '' ? " form-control-textarea-shadow" : " form-control-shadow") + (isCreatedByValid ? " is-invalid" : "") + (isInputCreateFocus ? "" : " noselect")}
                        placeholder={t('mypage.templates.infoInput.author.prompt')} />
                    {isCreatedByValid && <ValidateErrorMessage message={t('message.templates.e0003')} classAdd="position-static" />}
                </div>
                <div className="form-group mb-3 dropdown dropdown-personal">
                    <div ref={this.setWrapperRefTemplate}>
                        <button className={`btn btn-secondary dropdown-toggle dropdown-toggle-mt-focus ${showDropdownTemplate ? 'focus-btn-dropdown' : ''}`}
                            type="button" onClick={() => this.handelShowDropdownTemplate()}>
                            {category.label}
                        </button>
                        {showDropdownTemplate && (
                            <div className="dropdown-menu d-block">
                                <div
                                    onClick={() => this.handelShowDropdownTemplate()}
                                    className={`dropdown-item active font-size-14px  noselect`}>
                                    {categoryList[0].label}
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <div className={'form-group mb-3 ' + (showShadowBoder ? 'form-control-textarea-shadow' : '')}>
                    <textarea
                        maxLength="2000"
                        value={(templateDetail)}
                        onChange={this.newTemplateDetailTextarea_handleChange}
                        onBlur={this.newTemplateDetailTextarea_handleBlur}
                        className={`content-detail-new-template form-control form-control-textarea-shadow form-control-textarea-default placeholder-italic 
                            ${templateDetail !== '' ? 'bg-textarea-isvalid' : ''} ${isTextAreaMessageFocus ? '' : ' noselect'}`}
                        onFocus={(e) => this.removePlaceholderDetail(e)}
                        placeholder={t('mypage.templates.infoInput.detail.prompt')} rows="4">
                    </textarea>
                </div>
                <div className="text-center mt-2 modal-group-btn">
                    <button type="submit" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-444">{t('global.cancel')}</button>
                    <button type="submit" onClick={this.onButton_handleClick} className="btn btn-dark w-100px noselect-color-white">{t('global.ok')}</button>
                </div>
            </CommonModal>
        );
    }
}

export default withTranslation()(NewTemplateModal);