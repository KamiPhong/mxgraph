import React from 'react';
import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import iconErrorLogout from 'wlp-client-common/images/alertIcon001.png';
import { withTranslation } from 'react-i18next';

class DeleteMembersAlertModal extends React.Component {

    deleteButton_clickHandler = () => {
        const { cancelHandler, userIdMember, callDeleteShareMember } = this.props;
        callDeleteShareMember(userIdMember);
        cancelHandler();
    }

    render() {
        const {
            t,
            cancelHandler,
            messageDelelteAlertMembers,
        } = this.props;
        const backGround =  "deleteMemberBackground";

        return (
            <CommonModal
                onHide={cancelHandler}
                backDropClass = {backGround}
                show={true}>
                <div className="d-flex align-items-center justify-content-center content-alert-popup noselect">
                    <img src={iconErrorLogout} alt="" /><span>{messageDelelteAlertMembers}</span>
                </div>
                <div className="text-center mt-2 modal-group-btn">
                    <button type="button" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-444">{ t('global.cancel') }</button>
                    <button type="button" onClick={this.deleteButton_clickHandler} className="btn btn-dark w-100px noselect-color-white">{ t('global.ok') }</button>
                </div>
            </CommonModal>
        )
    }
}

export default withTranslation()(DeleteMembersAlertModal);