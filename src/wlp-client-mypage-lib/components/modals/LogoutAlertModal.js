import React from 'react';
import CommonLogoutModal from 'wlp-client-common/component/modals/CommonLogoutModal';
import iconErrorLogout from 'wlp-client-common/images/alertIcon001.png';
import { withTranslation } from 'react-i18next';

class LogoutAlertModal extends React.Component {
    render() {
        const {
            t,
            cancelHandler,
            callLogout
        } = this.props;

        return (
            <CommonLogoutModal
                className="pd-modal-maxwidth"
                onHide={cancelHandler}
                show={true}>
                <div className="d-flex align-items-center justify-content-center content-alert-popup noselect">
                    <img src={iconErrorLogout} alt="" /><span>{t('message.global.c0003')}</span>
                </div>
                <div className="text-center mt-2 modal-group-btn">
        <button type="submit" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-444">{ t('global.cancel') }</button>
                    <button type="submit" onClick={callLogout} className="btn btn-dark w-100px noselect-color-white">{ t('global.ok') }</button>
                </div>
            </CommonLogoutModal>
        )
    }
}

export default withTranslation()(LogoutAlertModal);