import React from 'react';
import CommonLogoutModal from 'wlp-client-common/component/modals/CommonLogoutModal';
import iconErrorLogout from 'wlp-client-common/images/alertIcon001.png';
import { withTranslation } from 'react-i18next';

class DeleteMemberAlertModal extends React.Component {
    render() {
        const {
            t,
            cancelHandler,
            callChange,
        } = this.props;
        const backGround =  "deleteMemberBackground";

        return (
            <CommonLogoutModal
                onHide={cancelHandler}
                backDropClass = {backGround}
                show={true}>
                <div className="d-flex align-items-center justify-content-center content-alert-popup noselect">
                    <img src={iconErrorLogout} alt="" /><span>{t('message.team.c0002')}</span>
                </div>
                <div className="text-center mt-2 modal-group-btn">
                    <button type="button" onClick={cancelHandler} className="btn btn-whitefl w-100px noselect-color-white">{ t('global.cancel') }</button>
                    <button type="button" onClick={callChange} className="btn btn-dark w-100px noselect-color-444">{ t('global.ok') }</button>
                </div>
            </CommonLogoutModal>
        )
    }
}

export default withTranslation()(DeleteMemberAlertModal);