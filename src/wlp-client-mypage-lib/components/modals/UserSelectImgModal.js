import React from 'react';
import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import { withTranslation } from 'react-i18next';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { avatorsWithNames } from 'wlp-client-common/utils/ImageDefault';


class UserSelectImgModal extends React.Component {

    state = {
        selectedImg: null,
        defaultImg: null
    }

    UNSAFE_componentWillReceiveProps({ imageUrl, isShow }) {
        const { selectedImg } = this.state;
        if ((imageUrl && !selectedImg) || (isShow === false)) {

            const defaultImg = imageUrl && avatorsWithNames.find(img => imageUrl.indexOf(img.img) === 0);
            this.setState({
                selectedImg: defaultImg ? defaultImg : avatorsWithNames[0],
                defaultImg: defaultImg ? defaultImg : avatorsWithNames[0]
            });
        }
    }

    setSelectedImg = img => {
        ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Selected_Image`);
        this.setState({ selectedImg: img });
    }

    render() {
        const {
            t,
            isShow,
            cancelHandler,
            changePictureBtnClickHandler,
        } = this.props;
        const { selectedImg } = this.state;

        return (<CommonModal
            onHide={cancelHandler}
            show={isShow}
            className={"modal-lg-ed set-width-select-image"}
            classNameImage={"set-margin-image"}>
            <span className="font-weight-bold change-picture-title noselect">{t('mypage.personalSettings.user.changePicture')}</span>
            <div className="modal-personal-img mt-3">
                <form action="">
                    <div className="d-flex justify-content-center">
                        {
                            selectedImg &&
                            selectedImg.img &&
                            avatorsWithNames.map((img, index) => {
                                return (
                                    <div key={index} className="custom-radio image-radio">
                                        <input
                                            onChange={() => this.setSelectedImg(img)}
                                            type="radio"
                                            checked={selectedImg.img === img.img}
                                            className="custom-control-input" name="ck2" />
                                        <label onClick={() => this.setSelectedImg(img)} className="custom-control-label" htmlFor={img.img}>
                                                <div className="border-image noselect">
                                                    <img src={img.src} alt="#" className="img-fluid fix-margin-img" />
                                                </div>
                                        </label>
                                    </div>
                                );
                            })
                        }
                    </div>
                </form>
            </div>
            <div className="text-center mt-34px">
                <button onClick={cancelHandler} type="button" className="btn btn-whitefl btm w-100px noselect-color-444">{t('global.cancel')}</button>
                <button onClick={() => changePictureBtnClickHandler(selectedImg.img)} type="submit" className="btn btn-dark w-100px ml-10 noselect-color-white">{t('global.ok')}</button>
            </div>
        </CommonModal >);
    }
}

export default withTranslation()(UserSelectImgModal);