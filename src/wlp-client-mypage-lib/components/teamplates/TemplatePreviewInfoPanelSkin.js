import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import imageError from 'wlp-client-common/images/category-img.jpg';
import i18n from "i18n";

const wlpDateUtil = new WlpDateUtil();

class TemplatePreviewInfoPanelSkin extends Component {

    render() {
        const { t, selectedTemplate } = this.props;
        const localeCode = i18n.language;
        return selectedTemplate ? (
            <div>
                <div className="border-bottom border-color-mp details-templates-img">
                    <div className="bg-category d-flex align-items-center justify-content-center noselect">
                    <img src={
                            selectedTemplate.sheetList &&
                            selectedTemplate.sheetList[0] &&
                            selectedTemplate.sheetList[0].mediumThumbnailUrl
                        } onError={(e) => e.target.src = imageError} alt="" className="img-fluid noselect" />
                    </div>
                </div>
                <div className="border-bottom border-color-mp details-templates">
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.templates.preview.templateTitle')}</label>
                        <div className="text-truncate noselect">
                            {(selectedTemplate.templateTitle)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.templates.preview.category')}</label>
                        <div className="text-truncate noselect">
                        { 
                            selectedTemplate.category ? (
                                (localeCode === LocaleConst.en_US) ? 
                                selectedTemplate.category.englishLabel : 
                                selectedTemplate.category.japaneseLabel
                            ) : ''
                        }
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.templates.preview.author')}</label>
                        <div className="text-truncate noselect">
                        {(selectedTemplate.author)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.templates.preview.publisher')}</label>
                        <div className="text-truncate noselect">
                        {(selectedTemplate.templatePublisher)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.templates.preview.releaseDate')}</label>
                        <div className="text-truncate noselect">
                            {wlpDateUtil.formatDateTime(selectedTemplate.releaseDate, localeCode)}
                        </div>
                    </div>
                </div>

                <div className="details-templates-frm noselect">
                    {(selectedTemplate.detail)}
                </div>
            </div>
        ): null;
    }
}

export default withTranslation()(TemplatePreviewInfoPanelSkin);