import i18n from 'i18n';
import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import TemplatePreviewInfoPanelSkin from 'wlp-client-mypage-lib/components/teamplates/TemplatePreviewInfoPanelSkin';

class TemplatePreviewPanel extends Component {
    
    render() {
        const { selectedTemplate, isShowTemplateDetail } = this.props;
        return (<Fragment>
            <div className="mypage-sidebar mypage-sidebar-right border-left">
                { isShowTemplateDetail && selectedTemplate && selectedTemplate.templateId ? <Fragment>
                    <TemplatePreviewInfoPanelSkin selectedTemplate={selectedTemplate} />
                </Fragment>: <h2 className="no-map-title noselect noselect-color-aaaaaa">{i18n.t('mypage.title.no.template')}</h2>}
            </div>
        </Fragment>);
    }
}

export default withTranslation()(TemplatePreviewPanel);