import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import MapPreviewInfoPanelSkin from './MapPreviewInfoPanelSkin';
import InvitePanel from './InvitePanel';
import i18n from 'i18n';

class MapPreviewPanelSkin extends Component {


    render() {
        const {
            selectedMapDetail,
            callCreateTemplate,
            categoryList, userInfo,
            callDeleteShareMember,
            callAddShareMember,
            setSelectedMapDetail,
            callDeleteSharePartner
        } = this.props;
        return (<Fragment>
            <div className="mypage-sidebar mypage-sidebar-right border-left">
                {selectedMapDetail && selectedMapDetail.mapId ? <Fragment>
                    <MapPreviewInfoPanelSkin
                        userInfo={userInfo}
                        selectedMapDetail={selectedMapDetail}
                        categoryList={categoryList}
                        callCreateTemplate={callCreateTemplate} />
                    <InvitePanel
                        callDeleteSharePartner={callDeleteSharePartner}
                        callAddShareMember={callAddShareMember}
                        callDeleteShareMember={callDeleteShareMember}
                        setSelectedMapDetail={setSelectedMapDetail}
                        selectedMapDetail={selectedMapDetail} />
                </Fragment>: <h2 className="no-map-title noselect noselect-color-aaaaaa">{i18n.t('mypage.title.no.map')}</h2>}
            </div>
        </Fragment>);
    }
}

export default withTranslation()(MapPreviewPanelSkin);