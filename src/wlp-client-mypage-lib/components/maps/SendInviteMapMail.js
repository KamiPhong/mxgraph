import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';

// client-common
import ValidateUtils from 'wlp-client-common/component/ValidateUtils';

// dto and service
import SendInviteMapMailConfigDto from 'wlp-client-service/dto/map/SendInviteMapMailConfigDto';
import GetMapListConfigDto from 'wlp-client-service/dto/map/GetMapListConfigDto';
import MapListSearchConditionDto from 'wlp-client-service/dto/map/MapListSearchConditionDto';
import MapSortColumn from 'wlp-client-service/consts/MapSortColumn';
import PagingConfig from 'wlp-client-common/config/PagingConfig';
import ContractType from 'wlp-client-service/consts/ContractType';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import EmailConst from 'wlp-client-common/consts/EmailConst';
import MapService from 'wlp-client-service/service/MapService';
import pointArrowIcon from 'wlp-client-common/images/pointArrowIcon.png';
import LocaleCode from 'wlp-client-service/consts/LocaleCode';
import MessageType from 'wlp-client-service/consts/MessageType';
import MapKind from 'wlp-client-service/consts/MapKind';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import TextUtils from 'wlp-client-common/utils/TextUtils';
import ActionLogger from 'wlp-client-common/ActionLogger';

const textUtils = new TextUtils();
const validateUtils = new ValidateUtils();
const mapService = new MapService();
const FORM_INVATE_MAIL = {
    EMAIL: 'EMAIL',
    ADD_MESSAGE: 'ADD_MESSAGE'
}
const MAX_PARTNER = 0;
 
class SendInviteMapMail extends Component {
    
    constructor(props) {
        super(props);
        this.state = ({
            showFormFlag: false,
            mailaddress: '',
            messageTextArea: '',
            isEmailValid: true,
            isSendCopyToMe: false,
            isTextAreaValid: false,
            isExcept: false,
            dataResponse: {
                isSucceed: true,
                isShowModal: false,
                message: '',
            },
            statusMember: {
                totalMember: false,
                message: '',
            },
            isFocusInput: false,
            isFocusTextArea: false
        });
        this.inputInviteEmail = React.createRef();
    }

    toggleFormAddMember = () => {
        ActionLogger.click('User_Mypage_MapInfomation', 'Toggle_Form_Add_Member');
        this.setState(({ showFormFlag }) => ({
            showFormFlag: !showFormFlag
        }));
    }

    // set email address state
    setEmailAddress = (event) => {
        ActionLogger.typing('User_Mypage_MapInfomation', 'Textarea_Message');
        event.preventDefault();
        const regex = EmailConst.EMAIL_RESTRICT;
        let valueEmail = textUtils.filterWithRestrict(event.target.value, EmailConst.EMAIL_RESTRICT);
        if (regex.test(valueEmail)) {
            this.setState(() => ({ mailaddress: valueEmail }));
        } else {
            event.preventDefault();
            const val = this.state.mailaddress,
                start = event.target.selectionStart,
                end = event.target.selectionEnd;

            this.setState(
                {
                    "mailaddress": valueEmail ? val.substring(0, start) + val.substring(end) : valueEmail
                }, () => {
                    this.inputInviteEmail.current.selectionStart = this.inputInviteEmail.current.selectionEnd = start - 1;
                }
            );
        }
    }

    // set messageTextArea state
    setMessageTextArea = (event) => {
        ActionLogger.typing('User_Mypage_MapInfomation', 'Input_Email');
        event.preventDefault();
        const textArea = event.target.value;
        this.setState(() => ({ messageTextArea: textArea }));
    }

    validateField = (field) => {
        const { mailaddress, messageTextArea } = this.state;
        const errorEmptyEmail = validateUtils.validateEmptyEmail(mailaddress);
        const errorEmailValid = validateUtils.validateEmail(mailaddress);
        const errors = errorEmptyEmail && errorEmailValid;

        if (field === FORM_INVATE_MAIL.ADD_MESSAGE) {
            if (messageTextArea === '') {
                this.setState(() => ({ isTextAreaValid: false}));
            } else {
                this.setState(() => ({ isTextAreaValid: true}));
            }
            this.setState({
                isFocusTextArea: false
            })
        } else if (field === FORM_INVATE_MAIL.EMAIL) {
            this.setState(() => ({ isEmailValid: errors, isExcept: true, isFocusInput: false }));
        }
        
        return errors;
    };

    validateFieldSucceed = () => {
        this.setState(() => ({ isEmailValid: true }));
    }

    validateFieldError = () => {
        this.setState(() => ({ isEmailValid: false }));
    }

    toggleSendCopyToMe = () => {
        ActionLogger.typing('User_Mypage_MapInfomation', 'Checkbox_Send_Copy');
        this.setState(({ isSendCopyToMe }) => ({
            isSendCopyToMe: !isSendCopyToMe
        }));
    }

    cancelAddMembers = () => {
        ActionLogger.click('User_Mypage_MapInfomation', 'Button_Cancel');
        this.setState({
            mailaddress: '',
            messageTextArea: '',
            isSendCopyToMe: false,
            isEmailValid: true,
            isTextAreaValid: false,
            isExcept: false
        })
        const messageTextArea = document.getElementById('messageTextArea')
        if (messageTextArea) {
            messageTextArea.value = '';
        }
    }

    showNotificationModal = (status, message) => {
        if (status) {
            this.setState({dataResponse: {isSucceed: status, isShowModal: true, message: message}});
        } else {
            this.setState({dataResponse: {isSucceed: status, isShowModal: true, message: message}});
        }
    }

    hideNotificationModal = () => {
        this.setState({dataResponse: {isShowModal: false}});
    }

    callSendInviteMapMail = async () => {
        ActionLogger.click('User_Mypage_MapInfomation', 'Button_OK');
        const { mailaddress, messageTextArea, isSendCopyToMe } = this.state;
        const validateSucceed = this.validateField();

        if (validateSucceed) {
            this.validateFieldSucceed();
            this.setState({isExcept: true});
            const { selectedMapDetail } = this.props;
            let paramDto = new SendInviteMapMailConfigDto();
            let requestDto = new GetMapListConfigDto();
            let searchConditionDto = new MapListSearchConditionDto();

            searchConditionDto = {
                "contractId": selectedMapDetail.contractInfo.contractId, 
                "contractType": ContractType.team,
                "descending": true, 
                "endNum": PagingConfig.pageRecordNum, 
                "mapIdList": null,
                "mapKind": MapKind.all,
                "pageRecordNum": PagingConfig.pageRecordNum,
                "sortColumn": MapSortColumn.lastUpdateTime,
                "startNum": PagingConfig.startNum,
                "teamId": null,
                "userId": null
            }
            
            paramDto.mapId = selectedMapDetail.mapId;
            paramDto.mapTitle = selectedMapDetail.mapTitle;
            paramDto.email = mailaddress;
            paramDto.optionalMsg = messageTextArea;
            paramDto.sendCopyToMe = isSendCopyToMe;
            paramDto.searchCondition = searchConditionDto;

            requestDto = {
                "accessToken": paramDto.accessToken,
                "contractId": selectedMapDetail.contractInfo.contractId,
                "contractType": ContractType.team,
                "currentState": CurrentState.MAP,
                "email": paramDto.email,
                "localeCode": paramDto.localeCode,
                "mapId": paramDto.mapId,
                "mapTitle": paramDto.mapTitle,
                "optionalMsg": paramDto.optionalMsg,
                "sendCopyToMe": paramDto.sendCopyToMe,
                "searchCondition": paramDto.searchCondition
            };

            const result = await mapService.sendInviteMapMail(requestDto);
            this.sendInviteMapMailResultHandler(result);
        } else {
            this.validateFieldError();
            this.inputInviteEmail.current.focus();
        }
    }

    setConditionResetInputMail = (messageType) => {
        switch (messageType) {
            case MessageType.success:
            case MessageType.error:
                return this.cancelAddMembers();
            case MessageType.warn:
            default:
                return;
          }
    }

    sendInviteMapMailResultHandler = (result) => {
        if (result.isSucceed) {
            const { setSelectedMapDetail } = this.props;
            setSelectedMapDetail(result.mapRecordDetail);
            if (!result.message || result.message.length === 0) return;
            this.showNotificationModal(true, result.message);
        } else {
            this.showNotificationModal(false, result.message);
        }
        showWarningDialog(
            result.messageKey,
            {
                onOKCallback: () => {
                    this.setConditionResetInputMail(result.messageType);
                }
            }
        )
    }

    UNSAFE_componentWillReceiveProps({ selectedMapDetail }) {
        if (
            selectedMapDetail &&
            typeof selectedMapDetail === 'object' &&
            Object.values(selectedMapDetail).length > 0
        ) {
            this.cancelAddMembers();
        }
    }

    componentDidMount() {
        this.cancelAddMembers();
    }

    onFocusInput = () => {
        this.setState({
            isFocusInput: true
        })
    }
    onFocusTextArea = () => {
        this.setState({
            isFocusTextArea: true
        })
    }

    render() {
        const { t, i18n, selectedMapDetail } = this.props;
        const { showFormFlag, mailaddress, isEmailValid, isSendCopyToMe, isExcept, isTextAreaValid, isFocusInput, isFocusTextArea } = this.state;
        const { addableShareNum, maxShareNum } = selectedMapDetail;

        return (<Fragment>
            <div className="">
                <div className={`d-flex p-add-member align-items-center`}>
                    <button onClick={this.toggleFormAddMember} className={`btn ${showFormFlag?'btn-add-member-minus':'btn-add-member-plus'} btn-sm`}
                        data-toggle="collapse"><i className={`fas ${showFormFlag?'fa-minus':'fa-plus'} noselect`}></i></button>
                    <div className="ml-auto">
                        <span className="badge badge-pill badge-pill-custom badge-secondary d-flex align-items-center font-size-10 noselect">
                        <img alt="" className="back-icon-2 noselect" src={pointArrowIcon} />&nbsp;{ t('mypage.maps.preview.share.addableUserNumber') }&nbsp;:&nbsp;<strong
                        className="text-white fs-22 noselect noselect-color-white"> {addableShareNum}</strong>/{maxShareNum}{ t('mypage.maps.preview.share.userUnit') }</span>
                    </div>
                </div>
                { addableShareNum === MAX_PARTNER && (
                    <div className="pl-2 color-error-bold position-static invaled-error invaled-error-ps fs-11px noselect">
                        ※ {t('mypage.maps.preview.share.inviteMaxPartner')}
                    </div>
                )}
            </div>
            {showFormFlag && 
                <div id="member-collapse">
                    <div className="p-2">
                        <input 
                            value={mailaddress}
                            maxLength="100"
                            autoComplete="off"
                            ref={this.inputInviteEmail}
                            onChange={e => this.setEmailAddress(e)}
                            onBlur={() => this.validateField(FORM_INVATE_MAIL.EMAIL)}
                            type="text" 
                            id="inputInviteEmail"
                            onFocus={this.onFocusInput}
                            className={`form-control focus-transparent font-invite-email ${!isEmailValid ? " is-invalid is-invalid-invite-mail" : ""} ${isEmailValid&&isExcept?" invalid-success bg-focus-input":""} ${isFocusInput?"":" noselect"}`}
                            placeholder={t('mypage.maps.preview.share.mailInputGuide')} 
                        />
                        {!isEmailValid ? (
                            <div className={'color-error invaled-error invaled-error-ps mb-s5px validate-invite-email position-static pl-1'}>
                                <div className={`font-weight-bold noselect ${i18n.language===LocaleCode.en_US?'width-80px':'width-45px'}`}>{t('global.errorLabel')} :</div><div className="noselect">{t('message.global.e0002')}</div>
                            </div>
                        ) : ''}
                        <textarea 
                            id='messageTextArea'
                            onChange={e => this.setMessageTextArea(e)}
                            onFocus={this.onFocusTextArea}
                            onBlur={() => this.validateField(FORM_INVATE_MAIL.ADD_MESSAGE)}
                            className={'form-control focus-transparent custom-textarea placeholder-italic no-resize mt-3 font-invite-email' + (isTextAreaValid?" invalid-success bg-focus-input":"") + (isFocusTextArea?"":" noselect")}
                            cols="30"
                            rows="4" 
                            placeholder={t('mypage.maps.preview.share.optionalMessage')}></textarea>
                        <div className="form-group form-check custom-form-checkbox mt-3">
                            <label className="form-check-label font-size-12 form-invite-email" htmlFor="exampleCheck1">
                                <input 
                                    checked={isSendCopyToMe}
                                    onChange={this.toggleSendCopyToMe}
                                    type="checkbox" 
                                    className="form-check-input checkbox-invite-mail" 
                                    id="exampleCheck1" />
                                <span className="checkmark"></span>
                                <span className="ml-5px noselect">{t('mypage.maps.preview.share.sendCopyToMe')}</span>
                            </label>
                        </div>
                        <div className="text-right group-btn-invite-mail">
                            <button type="button" onClick={this.cancelAddMembers} className="btn btn-whitefl w-100px noselect-color-white">{ t('global.cancel') }</button>
                            <button type="button" onClick={this.callSendInviteMapMail} className="btn btn-dark w-100px noselect-color-white">{ t('global.ok') }</button>
                        </div>
                    </div>
                </div>
            }
        </Fragment>);
    }
}

export default withTranslation()(SendInviteMapMail);