import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import MapPreviewPanelSkin from './MapPreviewPanelSkin';

class MapPreviewPanel extends Component {

    render() {
        const {
            selectedMapDetail,
            callCreateTemplate,
            categoryList, userInfo,
            callDeleteShareMember,
            callAddShareMember,
            setSelectedMapDetail,
            callDeleteSharePartner
        } = this.props;
        return (<MapPreviewPanelSkin
            callDeleteSharePartner={callDeleteSharePartner}
            callAddShareMember={callAddShareMember}
            callDeleteShareMember={callDeleteShareMember}
            userInfo={userInfo}
            selectedMapDetail={selectedMapDetail}
            categoryList={categoryList}
            setSelectedMapDetail={setSelectedMapDetail}
            callCreateTemplate={callCreateTemplate} />);
    }
}

export default withTranslation()(MapPreviewPanel);