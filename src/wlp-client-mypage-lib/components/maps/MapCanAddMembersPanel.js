import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import presonaluser from 'wlp-client-common/images/avators/noimage.png';
import ImageDefault from 'wlp-client-common/utils/ImageDefault';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import ActionLogger from 'wlp-client-common/ActionLogger';

class MapCanAddMembersPanel extends Component {

    state = {
        userTypeOfMap: this.props.selectedMapDetail ? this.props.selectedMapDetail.contractInfo.role : 1,
        disableAddMembers: (this.props.selectedMapDetail && this.props.selectedMapDetail.contractInfo.role <= RoleType.ADMIN) ? false : true,
    }

    addMembersButton_clickHandler = (userIdMember) => {
        ActionLogger.click('User_Mypage_MapInfomation', 'Button_Add_Member');
        const { userTypeOfMap } = this.state;
        const { callAddShareMember } = this.props;
        if(userTypeOfMap <= RoleType.ADMIN){
            callAddShareMember(userIdMember);
        }
    }

    render() {
        const { disableAddMembers } = this.state;
        const { membersSharingRecord, indexMembersSharingRecord } = this.props;

        return (<Fragment>
            <div key={indexMembersSharingRecord} className={"d-flex info_members p-2" + ((indexMembersSharingRecord + 1) % 2 !== 0 ? ' list-group-bg-hover' : '')}>
                <span className="fs-12 text-truncate w-120">
                    <span className="box-img-mem mr-2 noselect">
                        <img src={
                                    membersSharingRecord && membersSharingRecord.smallImageUrl && ImageDefault[membersSharingRecord.smallImageUrl] ?
                                    ImageDefault[membersSharingRecord.smallImageUrl] :
                                    membersSharingRecord.smallImageUrl
                                } 
                                onError={(e) => e.target.src = presonaluser} alt="" width="25" className="img-mem-detail noselect" />
                    </span>
                    <span className="name-member-shareing text-truncate noselect">{membersSharingRecord.userName}</span>
                </span>
                <div className="ml-auto">
                    <div className={"badge badge-can-add badge-pill badge-secondary none-select " + (disableAddMembers ? "badge-disable noselect-color-7f7f7f" : "badge-hover noselect-color-white")}
                        onClick={() => this.addMembersButton_clickHandler(membersSharingRecord.userId)} >
                        +
                    </div>
                </div>
            </div>
            </Fragment>);
    }
}

export default withTranslation()(MapCanAddMembersPanel);
