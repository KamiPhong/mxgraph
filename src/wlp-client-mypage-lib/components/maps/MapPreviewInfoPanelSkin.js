import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import MapRecordDetailDto from 'wlp-client-service/dto/map/MapRecordDetailDto';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import GetSize from 'wlp-client-common/utils/GetSize';
import NewTemplateModal from 'wlp-client-mypage-lib/components/modals/NewTemplateModal';
import Link from 'wlp-client-common/component/Link';
import imageError from 'wlp-client-common/images/category-img.jpg';
import ActionLogger from 'wlp-client-common/ActionLogger';

import i18n from "i18n";

const wlpDateUtil = new WlpDateUtil();
const getSize = new GetSize();

class MapPreviewInfoPanelSkin extends Component {
    state = {
        isShowNewTemplateModal: false
    }

    showNewTemplateModal = () => {
        ActionLogger.click('User_Mypage_MapInfomation', 'Button_Save_Template');
        this.setState({ isShowNewTemplateModal: true })
    }

    hideNewTemplateModal = () => {
        this.setState({ isShowNewTemplateModal: false })
    }

    render() {
        const { t, selectedMapDetail, categoryList, callCreateTemplate, userInfo } = this.props;
        let mapRecord = Object.assign(new MapRecordDetailDto(), selectedMapDetail);
        const localeCode = (i18n.language === LocaleConst.en_US) ? LocaleConst.en_US : LocaleConst.ja_JP;
        const { isShowNewTemplateModal } = this.state;
        let isShowSaveAsTemplate = false;
        let setValueImage = mapRecord.sheetList[0].templateImageUrl ? 
            mapRecord.templateImageUrl :
            ( mapRecord.sheetList &&
            mapRecord.sheetList[0] &&
            mapRecord.sheetList[0].mediumThumbnailUrl );

        if (userInfo.sessionDto) {
            if (userInfo.sessionDto.specialUserFlag || selectedMapDetail.templateCreateFlag) {
                isShowSaveAsTemplate = true;
            }
        }
        return (
            <div>
                {isShowNewTemplateModal && (
                    <NewTemplateModal
                        selectedMapDetail={selectedMapDetail}
                        categoryList={categoryList}
                        cancelHandler={this.hideNewTemplateModal}
                        callCreateTemplate={callCreateTemplate}
                        isShow={isShowNewTemplateModal} />
                )}
                <div className="border-bottom border-color-mp details-templates-img">
                    <div className="bg-category d-flex align-items-center justify-content-center noselect">
                        <img src={setValueImage} alt="" onError={(e) => e.target.src = imageError} className="img-fluid noselect" />
                    </div>
                </div>
                <div className="border-bottom border-color-mp text-muted-6 details-templates">
                    <div className="d-flex">
                        <label htmlFor="" className="d-flex align-items-center mb-0 min-w88 pr-1 noselect">{t('mypage.maps.preview.mapTitle')}</label>
                        <div className="detail-map-info-right noselect">
                            {(mapRecord.mapTitle)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.maps.preview.teamName')}</label>
                        <div className="detail-map-info-right noselect text-truncate">
                            {(mapRecord.contractInfo.teamName)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.maps.preview.popular')}</label>
                        <div className="detail-map-info-right noselect text-truncate">
                            {
                                [1, 2, 3, 4, 5].map((item, index) => {
                                    return (<i key={index} className={"fas fa-star noselect " + (mapRecord.popular >= item ? 'text-color-red ' : 'noselect-color-a5a5a5')}></i>)
                                })
                            }
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.maps.preview.size')}</label>
                        <div className="detail-map-info-right noselect text-truncate">
                            {getSize.getFileSizeToMB(mapRecord.mapSizeTotal)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.maps.preview.created')}</label>
                        <div className="detail-map-info-right noselect text-truncate">
                            {wlpDateUtil.formatDateTime(mapRecord.createdDate, localeCode)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="d-flex align-items-center mb-0 min-w88 pr-1 noselect">{t('mypage.maps.preview.owner')}</label>
                        <div className="detail-map-info-right noselect">
                            {(mapRecord.ownerName)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect">{t('mypage.maps.preview.lastModified')}</label>
                        <div className="detail-map-info-right noselect text-truncate">
                            {wlpDateUtil.formatDateTime(mapRecord.lastUpdateTime, localeCode)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="d-flex align-items-center mb-0 min-w88 pr-1 noselect noselect">{t('mypage.maps.preview.updateUserName')}</label>
                        <div className="detail-map-info-right noselect">
                            {(mapRecord.lastUpdateName)}
                        </div>
                    </div>
                    <div className="d-flex">
                        <label htmlFor="" className="mb-0 min-w88 pr-1 noselect"><span className="w-80-block noselect">{t('mypage.maps.preview.ipControl')}</span></label>
                        <div className="d-flex detail-map-info-right noselect text-truncate align-items-center">
                            {mapRecord.ipControlFlag ? t('mypage.maps.preview.ipControl.on') : t('mypage.maps.preview.ipControl.off')}
                        </div>
                    </div>
                    {isShowSaveAsTemplate && <Link
                        onClick={this.showNewTemplateModal}
                        data-toggle="modal"
                        className="btn btn-secondary btn-sm font-weight-bold btn-darkx noselect noselect-color-white">
                        {t('mypage.maps.preview.operation.saveTemplate')}
                    </Link>}
                </div>
            </div>
        );
    }
}

export default withTranslation()(MapPreviewInfoPanelSkin);