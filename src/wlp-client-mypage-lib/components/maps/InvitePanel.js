import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import SendInviteMapMail from 'wlp-client-mypage-lib/components/maps/SendInviteMapMail';
import MapCanAddMembersPanel from './MapCanAddMembersPanel';
import littleSquareIcon from 'wlp-client-common/images/icon/little-square.svg';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import MapSharingPanel from './MapSharingPanel';
import SharememberTypes from 'wlp-client-common/consts/SharememberTypes';

class InvitePanel extends Component {

    state = {
        membersSharing: [],
        sharePartnerList: [],
        addableMemberList: [],
        selectedCurrentMapDetail: []
    }

    UNSAFE_componentWillReceiveProps({ selectedMapDetail }) {
        if (selectedMapDetail) {
            this.setState({
                membersSharing: selectedMapDetail.shareMemberList,
                sharePartnerList: selectedMapDetail.sharePartnerList,
                addableMemberList: selectedMapDetail.addableMemberList,
                selectedCurrentMapDetail: selectedMapDetail
            })
        }
    }

    componentDidMount(){
        const { selectedMapDetail } = this.props;
        this.UNSAFE_componentWillReceiveProps({selectedMapDetail});
    }

    render() {
        const { t, selectedMapDetail, callDeleteShareMember, callAddShareMember, setSelectedMapDetail, callDeleteSharePartner } = this.props;
        const { membersSharing, sharePartnerList, addableMemberList, selectedCurrentMapDetail } = this.state;
        
        return (
            <div className="pt-3px details_templates-frm">
                <p className="pl-2 pr-2 title-memberlist">
                    <span className="pr-8 noselect"><img src={littleSquareIcon} alt="" className="noselect"/></span>
                    <span className="font-weight-bold noselect">{t('mypage.maps.preview.share.memberShared')}</span>
                </p>
                {
                    membersSharing.map((mapItem, index) => {
                        return (<MapSharingPanel
                            shareMemberType={SharememberTypes.MEMBER}
                            selectedMapDetail={selectedMapDetail}
                            callDeleteShareMember={callDeleteShareMember}
                            key={mapItem.userId}
                            membersSharingRecord={mapItem}
                            indexMembersSharingRecord={index} />)
                    })
                }
                {addableMemberList.length > 0  && (<p className="pl-2 pr-2 mt-1 title-memberlist">
                    <span className="pr-8 noselect"><img src={littleSquareIcon} alt="" className="noselect"/></span>
                    <span className="font-weight-bold noselect">{t('mypage.maps.preview.share.addableMember')}</span>
                </p>)}
                {
                    addableMemberList.map((mapItem, index) => {
                        return (<MapCanAddMembersPanel 
                            callAddShareMember={callAddShareMember}
                            selectedMapDetail={selectedMapDetail}
                            key={mapItem.userId}
                            membersSharingRecord={mapItem}
                            indexMembersSharingRecord={index} />)
                    })
                }
                {sharePartnerList.length > 0  && (<p className="pl-2 pr-2 title-memberlist">
                    <span className="pr-8 noselect"><img src={littleSquareIcon} alt="" className="noselect"/></span>
                    <span className="font-weight-bold noselect">{t('mypage.maps.preview.share.partnerShared')}</span>
                </p>)}
                {
                    sharePartnerList.map((mapItem, index) => {
                        return (<MapSharingPanel
                            shareMemberType={SharememberTypes.PARTNER}
                            selectedMapDetail={selectedMapDetail}
                            callDeleteSharePartner={callDeleteSharePartner}
                            key={mapItem.userId}
                            membersSharingRecord={mapItem}
                            indexMembersSharingRecord={index} />)
                    })
                }
                {
                    selectedCurrentMapDetail.contractInfo &&
                    RoleType.MEMBER > selectedCurrentMapDetail.contractInfo.role &&
                    <SendInviteMapMail 
                        selectedMapDetail={selectedCurrentMapDetail}
                        setSelectedMapDetail={setSelectedMapDetail}
                        t={t} />
                }
            </div>
        );
    }
}

export default withTranslation()(InvitePanel);