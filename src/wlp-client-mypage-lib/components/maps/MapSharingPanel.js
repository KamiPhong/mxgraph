import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import presonalUser from 'wlp-client-common/images/avators/noimage.png';
import MapUtil from 'wlp-client-mypage-lib/utils/MapUtil';
import DeleteMembersAlertModal from 'wlp-client-mypage-lib/components/modals/DeleteMembersAlertModal';
import SharememberTypes from 'wlp-client-common/consts/SharememberTypes';
import ActionLogger from 'wlp-client-common/ActionLogger';
import ImageDefault from 'wlp-client-common/utils/ImageDefault';

const mapUtil = new MapUtil();

class MapSharingPanel extends Component {
    state = {
        userTypeOfMap: this.props.selectedMapDetail ? this.props.selectedMapDetail.contractInfo.role : 1,
        deleteBtnLabel: '',
        isMoseOverDeleteBtn: false,
        isShowDeleteMembersAlertModal: false,
        messageDelelteAlertMembers: '',
    }

    deleteBtnRollOverHandler = (userType) => {
        const { t, selectedMapDetail } = this.props;
        const { isMoseOverDeleteBtn } = this.state;
        if(selectedMapDetail.contractInfo.role <= RoleType.ADMIN  && userType > RoleType.ADMIN){
            if(isMoseOverDeleteBtn === false){
                this.setState({isMoseOverDeleteBtn: true}, () => {
                    if(userType === RoleType.MEMBER) {
                        this.setState({ deleteBtnLabel: t("mypage.maps.preview.share.private") });
                        this.setState({ messageDelelteAlertMembers: t("message.maps.c0001") });
                    } else if(userType === RoleType.PARTNER) {
                        this.setState({ deleteBtnLabel: t("mypage.maps.preview.share.excommunication") });
                        this.setState({ messageDelelteAlertMembers: t("message.maps.c0002") })
                    }
                });
            }
        }
    }

    deleteBtnRollOutHandler = () => {
        const { isMoseOverDeleteBtn } = this.state;
        if(isMoseOverDeleteBtn === true){
            this.setState({isMoseOverDeleteBtn: false}, () => {
                this.setState({ deleteBtnLabel: ''  })
            });
        }
    }

    hideDeleteMembersAlertModal = () => {
        this.setState({ isShowDeleteMembersAlertModal: false })
    }

    showDeleteMembersAlertModal = (userType) => {
        ActionLogger.click('User_Mypage_MapInfomation', 'Button_Delete_Member');
        const { selectedMapDetail } = this.props;
        if(selectedMapDetail.contractInfo.role <= RoleType.ADMIN && userType > RoleType.ADMIN){
            if(userType === RoleType.MEMBER) {
                this.setState({ isShowDeleteMembersAlertModal: true })
            } else if(userType === RoleType.PARTNER) {
                this.setState({ isShowDeleteMembersAlertModal: true })
            }
        }
        
    }

    render() {
        const { t, membersSharingRecord, indexMembersSharingRecord, callDeleteSharePartner, callDeleteShareMember, shareMemberType  } = this.props;
        const { deleteBtnLabel, isShowDeleteMembersAlertModal, messageDelelteAlertMembers } = this.state;
        const callDeleteAction = shareMemberType === SharememberTypes.MEMBER ? callDeleteShareMember : callDeleteSharePartner;

        return (<Fragment>
            {isShowDeleteMembersAlertModal && (
                <DeleteMembersAlertModal
                    callDeleteShareMember={callDeleteAction}
                    userIdMember={membersSharingRecord.userId}
                    messageDelelteAlertMembers={messageDelelteAlertMembers}
                    cancelHandler={this.hideDeleteMembersAlertModal}
                    isShow={isShowDeleteMembersAlertModal} />
            )}
            <div key={indexMembersSharingRecord} className={"d-flex info_members p-2" + ((indexMembersSharingRecord + 1) % 2 !== 0 ? ' list-group-bg-hover' : '')}>
                <span className="fs-12 text-truncate w-120">
                    <span className="box-img-mem mr-2 noselect">
                        <img src={
                                    membersSharingRecord.invitingFlg ? presonalUser : (
                                    membersSharingRecord && membersSharingRecord.smallImageUrl && ImageDefault[membersSharingRecord.smallImageUrl] ?
                                    ImageDefault[membersSharingRecord.smallImageUrl] :
                                    membersSharingRecord.smallImageUrl)
                                } 
                                onError={(e) => e.target.src = presonalUser} alt="" width="25" className="img-mem-detail noselect" />
                    </span>
                    <span className="name-member-shareing text-truncate noselect">{membersSharingRecord.userName}</span>
                </span>
                <div className="ml-auto">
                    <div className={"badge badge-pill badge-secondary noselect noselect-color-white " + (deleteBtnLabel ? "badge-hover" : "")} 
                        onClick={() => this.showDeleteMembersAlertModal(membersSharingRecord.userType)} 
                        onMouseOut={() => this.deleteBtnRollOutHandler()} 
                        onMouseOver={() => this.deleteBtnRollOverHandler(membersSharingRecord.userType)}>
                        { 
                            deleteBtnLabel ? deleteBtnLabel: 
                            t(membersSharingRecord.invitingFlg ? 'global.inviting' : mapUtil.getPermissionLabelById(membersSharingRecord.userType)) 
                        }
                    </div>
                </div>
            </div>
            </Fragment>);
    }
}

export default withTranslation()(MapSharingPanel);
