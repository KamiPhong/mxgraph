import React, { useState, useEffect, useRef } from 'react';
import { FaSearch } from 'react-icons/fa';
import { withTranslation } from 'react-i18next';

import Header from 'wlp-client-common/component/Header';
import AvatarButtonSkin from 'wlp-client-common/component/pulldown/AvatarButtonSkin';

import logo from 'wlp-client-common/images/logo.jpg';
import helpIcon from 'wlp-client-common/images/icon/helpIcon.svg';
import ResponseDto from 'wlp-client-service/dto/ResponseDto';
import AuthService from 'wlp-client-service/service/AuthService';
import MypageRequestDto from 'wlp-client-service/dto/MypageRequestDto';
import LogoutAlertModal from './modals/LogoutAlertModal';
import MyPageView from 'wlp-client-mypage-lib/consts/MyPageView';
import SearchConfigDto from 'wlp-client-service/dto/search/SearchConfigDto';
import SearchListViewBase from 'wlp-client-common/component/search/SearchListViewBase';
import SearchService from 'wlp-client-service/service/SearchService';
import { COUNT_DEFAULT } from 'wlp-client-mypage-lib/consts/UserSetting';
import Link from 'wlp-client-common/component/Link';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import mypageStateStorage from 'wlp-client-mypage/MypageStateStorage';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import ActionLogger from 'wlp-client-common/ActionLogger';

const searchService = new SearchService();
const SEARCH_INPUT_MAX_LENGTH = 512;

const MyPageHeader = ({ t, contractList, user, setMyPageView, selectedContractId, setTeamData, searchPopup, setSearchPopup, setShowBackDropCs }) => {
    const [isShowLogoutAlert, setShowLogoutAlert] = useState(false);
    const [valueSearch, setValueSearch] = useState('');
    const [searchResult, setSearchResult] = useState(null);
    const [focusInputSearch, setFocusInputSearch] = useState(false);
    const [showSearchListModal, setShowSearchListModal] = useState(false);
    const searchInput = useRef(null);

    function callSearchTeam_handleResult(e) {
        ActionLogger.submit('Mypage_Header', 'Input_Search');
        e.preventDefault();
        if (valueSearch !== '') {
            setShowSearchListModal(false);
            findReading(-1, valueSearch);
        }
    }

    function handleChangeInputSearchTeam(text) {
        ActionLogger.typing('Mypage_Header', 'Input_Search');
        setValueSearch(text);
    }

    async function findReading(contractId, text) {
        let requestDto = new SearchConfigDto();
        requestDto.contractId = contractId;
        requestDto.text = text;

        requestDto = {
            ...requestDto,
            "contractId": requestDto.contractId,
            "count": parseInt(COUNT_DEFAULT), // mặc định tối đa là 3 kết quả có thể chỉnh thêm
            "mapId": null,
            "start": 0,
            "text": requestDto.text
        };

        const result = await searchService.findLeading(requestDto);
        findReadingResult(result);
    }

    function findReadingResult(data) {
        setSearchResult(data);
        setSearchPopup(true);
        setShowBackDropCs(true);
    }

    function logoutBtn_clickHandle() {
        ActionLogger.click('Mypage_Header', 'Logout_Button');
        setShowLogoutAlert(true)
    }

    async function callLogout() {
        const authService = new AuthService();
        const result = await authService.logoff(new MypageRequestDto());
        callLogout_handleResult(result);
    }

    function callLogout_handleResult(result = new ResponseDto()) {
        const localStorageUtils = new LocalStorageUtils();
        localStorageUtils.deleteUserSession();
        window.location.href = '/';
    }

    function eventsClickIconSearch() {
        if (searchInput && searchInput.current) {
            searchInput.current.focus();
        }
    }

    function getValidContract(ctl) {
        return ctl.filter(({ role }) => role === RoleType.ADMIN || role === RoleType.LEADER)
    }

    function renderConTractList() {
        let newContractList = (contractList && Array.isArray(contractList)) ? getValidContract(contractList) : [];
        const isContractListValid = newContractList.length > 0;

        return isContractListValid ? (<React.Fragment>
            <div className="dropdown-divider"></div>
            {
                newContractList.map(team => (
                    <Link
                        key={team.contractId}
                        className="dropdown-item dropdown-item-myteams text-truncate noselect"
                        onClick={((e) => {
                            setMyPageView(MyPageView.STATE_TEAM_SETTING);
                            setTeamData(team)
                        })}>
                        {team.teamName}
                    </Link>
                ))
            }
        </React.Fragment>) : null;
    }

    useEffect(() => {
        if (contractList && Array.isArray(contractList)) {
            mypageStateStorage.setItem(mypageStateStorage.HEADER_MENU_TEAMLIST, getValidContract(contractList));
        }
    }, [contractList])

    const avatarDropdownOverlay = (
        <div className="dropdown-menu-bg-black">
            <Link
                onClick={logoutBtn_clickHandle}
                className="dropdown-item first-el noselect">
                {t('global.headerMenu.logout')}
            </Link>
            <div className="dropdown-divider"></div>
            <Link
                onClick={() => {
                    ActionLogger.click('Mypage_Header', 'Personal_Setting_Button');
                    setMyPageView(MyPageView.STATE_PERSONAL_SETTING);
                }}
                className="dropdown-item noselect">
                {t('mypage.headerMenu.personalSettings')}
            </Link>
            {renderConTractList()}
        </div>
    )

    const headerRight = (<React.Fragment>
        <AvatarButtonSkin overlay={avatarDropdownOverlay} user={user} />
        <div className="mypage-header-search">
            <button
                onBlur={() => setFocusInputSearch(false)}
                onClick={() => { setFocusInputSearch(true); eventsClickIconSearch() }}
                className="btn-reset bg-transparent border-0 pl-1 header-search-ic"
                type="submit">
                <FaSearch />
            </button>

            <form onSubmit={(e) => callSearchTeam_handleResult(e)}>
                <input
                    ref={searchInput}
                    type="text"
                    onBlur={() => setFocusInputSearch(false)}
                    onFocus={() => {
                        ActionLogger.click('Mypage_Header', 'Input_Search');
                        setFocusInputSearch(true);
                    }}
                    onChange={(e) => handleChangeInputSearchTeam(e.target.value)}
                    className={`input-search-mypage form-control form-control-sm custom-form-control-sm bg-transparent border-0 pl-4 ${focusInputSearch ? 'input-search-focus' : ' noselect'}`}
                    placeholder={!focusInputSearch ? t('search.prompt.mypage') : ''} 
                    maxLength={SEARCH_INPUT_MAX_LENGTH}/>
            </form>
        </div>
        {searchPopup &&
            <div className="d-flex box-search">
                <div className="scroll-bar-search">
                    <SearchListViewBase
                    t={t}
                    searchResult={searchResult}
                    setSearchPopup={setSearchPopup}
                    valueSearch={valueSearch}
                    showSearchListModal={showSearchListModal}
                    setShowSearchListModal={setShowSearchListModal}
                />
                </div>
                
            </div>
        }

        {/* Guid page */}
            <Link
                className="btn mypage-header-right-btn h-100"
                href="https://www.accelatech.com/support/KB/kb_manual.html" 
                target="_blank" 
                onClick={() => {
                    ActionLogger.click('Mypage_Header', 'Suport_Button');
                }}
                tabIndex={3} title="" style={{ cursor: "default" }}>
                    <img src={helpIcon} alt="" className="noselect" />
                </Link>
    </React.Fragment>)

    return (<React.Fragment>
        <Header onClickLogo={() => {
            ActionLogger.click('Mypage_Header', 'Logo_Button');
            setMyPageView(MyPageView.STATE_MAPS);
        }} logo={logo} rightComponent={headerRight} />
        {
            isShowLogoutAlert &&
            <LogoutAlertModal cancelHandler={() => setShowLogoutAlert(false)} callLogout={callLogout} />
        }
    </React.Fragment>)
}

export default withTranslation()(MyPageHeader);