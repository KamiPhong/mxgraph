import React from 'react';
import Link from 'wlp-client-common/component/Link';
import i18n from 'i18n';

const Footer = () => {
    return (
        <footer className="mypage-footer fixed-bottom">
            <div className="mypage-footer-left">
                <span className="light-gray noselect noselect-color-white" >{i18n.t('mypage.footer.text')}</span>
            </div>
            <div className="mypage-footer-right ml-auto">
                <Link target="_blank" href="https://www.accelatech.com/privacy" className="text-uppercase text-white white-gray btn-reset padding-right-0 noselect noselect-color-white">{i18n.t('mypage.footer.button')}</Link>
            </div>
        </footer>
    )
}

export default Footer;