import React, { Component } from 'react';
import CommonModal from 'wlp-client-common/component/modals/CommonModal';
import InfoIcon from 'wlp-client-common/images/icon/infoIcon001.png';
import { withTranslation } from 'react-i18next';
import InformationDto from 'wlp-client-service/dto/InformationDto';
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import InformationService from 'wlp-client-service/service/InformationService';
import { checkElementClicked } from 'wlp-client-common/utils/EventUtils';
import ActionLogger from 'wlp-client-common/ActionLogger';


const languageUtil = new WlpLanguageUtil();
const  informationService = new InformationService();

class InformationPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contentHTML: '',
            isShow: false
        }
    }

    componentDidMount() {
        this.load();
        document.addEventListener('click', this.onDocumentClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.onDocumentClick);
    }

    load = () => {
        this.getInformationService();
    }

    getInformationService = () => {
        informationService.getInformation().then(response => {
            this.getInformationResultHandler(response);
        });
    }

    getInformationResultHandler = (response) => {
        this.drawInformation(response);
    }

    drawInformation = (dto = new InformationDto()) => {
        if (dto.informationContentJp.length > 0) {
            let html = "";
            let date = "";
            let content = "";

            if (languageUtil.getCurrentLanguage() === LocaleConst.ja_JP) {
                for (let i = 0; i < dto.informationContentJp.length; i++) {
                    date = dto.informationDateJp[i];
                    content = dto.informationContentJp[i];

                    html = html + "<p><font font-size='14' color='#AAAAAA'><b>" + date + "</b></font></p>";
                    html = html + "<p class='content'><font font-size='14' color='#666666'>" + content + "</font></p>";
                }
            } else {
                for (let i = 0; i < dto.informationContentEn.length; i++) {
                    date = dto.informationDateEn[i];
                    content = dto.informationContentEn[i];
                    html = html + "<p style='font-size: 14px; color: #AAAAAA'><b>" + date + "</b></p>";
                    html = html + "<p class='content' style='font-size: 14px; color: #666666'>" + content + "</p>";
                }
            }

            if (html) {
                this.setState({contentHTML: html, isShow: true});
            }
        }
    }

    onCloseInformation = () => {
        ActionLogger.click('Mypage_Infomation', 'OK_Button');
        this.setState({isShow: false});
    }

    onBackgroundClick = () => {
        ActionLogger.click('Mypage_Infomation', 'BackGroundClick');
    }

    onDocumentClick = (evt) => {
        if (!checkElementClicked(evt, this.modalBody) && this.hideOnBlur) {
            this._onBlur();
        }
    }

    render() {
        const { t } = this.props;
        const { contentHTML, isShow } = this.state;
        return (
            <div>
                <CommonModal
                    hideOnBlur={true}
                    show={isShow}
                    onBackgroundClick={this.onBackgroundClick}
                    backGroundClass="w-auto mypage-modal-bg-information"
                    classNameImage="information-body"
                >
                    <div className="none-select">
                        <div className="d-flex align-items-center">
                            <img src={InfoIcon} alt="" />
                            <div className="alert-title">{t('mypage.information.title')}</div>
                        </div>
                        <div
                            className="form-control focus-transparent mt-3 placeholder-italic content-detail-new-template information-textarea none-select r-border"
                            dangerouslySetInnerHTML={{ __html: contentHTML}}
                        />
                        <div className="text-center pt-21px">
                            <button
                                onClick={this.onCloseInformation}
                                className="btn btn-dark w-100px noselect-color-white"
                                type="button">
                                {t('global.ok')}
                            </button>
                        </div>
                    </div>
                </CommonModal>
            </div>
        );
    }
}

export default withTranslation()(InformationPanel);