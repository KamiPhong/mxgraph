import React from 'react';
import i18n from 'i18n';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import { memberDgItemRendererEvent } from 'wlp-client-mypage-lib/events/MemberDgItemRendererEvent';
import ActionLogger from 'wlp-client-common/ActionLogger';


class MemberDgDeleteBtnRenderer extends React.Component { 
    enabled = false;
    constructor(props) {
        super(props);

        const { data } = props;
        this.enabled = (RoleType.ADMIN === data.userType || RoleType.LEADER === data.userType) ? false : true;
    }

    componentDidUpdate(prevProps, prevState){
        if (this.props.data.userId !== prevProps.data.userId) {
            this.enabled = (RoleType.ADMIN === this.props.data.userType || RoleType.LEADER === this.props.data.userType) ? false : true;
        }
    }

    onClickHandler = (e) => {
        ActionLogger.click('User_Mypage_TeamSetting_[メンバーリスト]', `Button_Delete_Member`);
        if (this.enabled) {
            const { data } = this.props;
            console.log(`MemberDgDeleteBtnRenderer clicekd`)
            memberDgItemRendererEvent.fireEvent(memberDgItemRendererEvent.DELETE_CLICK, data);
        }
    }
    
    render() { 
        return <div className="float-left">
                    <button type="button"
                            onClick={this.onClickHandler}
                            disabled={ !this.enabled}
                            className="btn btn-sm btn-sm text-nowrap custom-button-delete">
                        <i className="fas fa-times-circle reset-icon-delete noselect noselect-color-212529"/>
                        <span className="button-delete noselect noselect-color-white">{i18n.t('mypage.teamSettings.member.deleteBtnLbl')}</span>
                    </button>
                </div>;
    }
}


export default MemberDgDeleteBtnRenderer;