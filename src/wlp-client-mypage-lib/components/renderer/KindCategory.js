import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";
import CategoryRecordDto from "wlp-client-service/dto/template/CategoryRecordDto";
import LocaleCode from "wlp-client-service/consts/LocaleCode";
import downArrowIcon from 'wlp-client-common/images/icon/downArrowIcon.svg';
import rightArrowIconActive from 'wlp-client-common/images/icon/rightArrowIconActive.svg';
import Link from "wlp-client-common/component/Link";

class KindCategory extends Component {
    state = {
        showTemplateListFlag: true
    }

    toggleShowTemplateList = (e) => {
        e.preventDefault();
        this.setState(({ showTemplateListFlag }) => ({ showTemplateListFlag: !showTemplateListFlag }))
    }

    categoryRecord_handleClick = (e, categoryRecord = new CategoryRecordDto()) => {
        e.preventDefault();
        const { callGetTemplateList } = this.props;
        callGetTemplateList(categoryRecord.id);
    }

    render() {
        const { t, categoryList, i18n, selectedCategory } = this.props;
        const { showTemplateListFlag } = this.state;

        return (<Fragment>
            <div className="mypage-sidebar-left-item mypage-left-panel category-list pt-10">
                <Link onClick={this.toggleShowTemplateList} className="list-group-title magin-right-reset text-uppercase pb-7 collapsed cursor-df">
                    <span className="list-group-icon magin-right-reset">
                        <img src={showTemplateListFlag && categoryList.length > 0 ? downArrowIcon : rightArrowIconActive} className="carret-icon noselect"  alt="" />
                    </span>
                    <span className="text-uppercase noselect"> {t('mypage.listLabel.templates')} </span>
                </Link>
                <div className="list-group list-group-none-bd mypage-sidebar-left-list border-0">
                    {
                        showTemplateListFlag &&
                        categoryList &&
                        Array.isArray(categoryList) &&
                        categoryList.map((category, key) => {
                            return (<Link
                                className={`list-group-item pl-20 noselect cursor-df ${selectedCategory === category.id ? 'item-active' : ''}`}
                                onClick={e => this.categoryRecord_handleClick(e, category)}
                                key={key}>
                                {(
                                    i18n.language === LocaleCode.en_US ?
                                        category.englishLabel :
                                        category.japaneseLabel
                                )}
                                <span className="ml-1 noselect"> ({category.count}) </span>
                            </Link>)
                        })
                    }
                </div>
            </div>
        </Fragment>)
    }
}

export default withTranslation()(KindCategory);