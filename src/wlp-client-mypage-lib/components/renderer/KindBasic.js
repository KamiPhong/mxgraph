import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";

import BasicKindRecord from "./BasicKindRecord";
import MapKind from "wlp-client-service/consts/MapKind";
import iconAllMap from 'wlp-client-common/images/icon/ic-allmap.svg';
import iconRecentMap from 'wlp-client-common/images/icon/ic-recentmap.svg';
import iconBookMap from 'wlp-client-common/images/icon/ic-bookmark.svg';
import MapSummary from "wlp-client-service/consts/MapSummary";
import ActionLogger from 'wlp-client-common/ActionLogger';

class KindBasic extends Component {

    callGetMapList = (mapKind, contractId, startNum) => {
        ActionLogger.click('User_Mypage_ListCategory', `mapkind_${mapKind}`);
        const {callGetMapList} = this.props;
        if( callGetMapList && typeof callGetMapList ==='function'){
            callGetMapList(mapKind, contractId, startNum);
        }
    }

    render() {
        const {
            mapSummary,
            selectedGroupBasic,
        } = this.props;

        const BASIC_KINDS = [
            { kindLabel: 'mypage.maps.mapKind.all', mapCount: mapSummary[MapSummary.all] || 0, icon: iconAllMap, mapKind: MapKind.all, sizeImage: 20 },
            { kindLabel: 'mypage.maps.mapKind.recent', icon: iconRecentMap, mapKind: MapKind.recent,  sizeImage: 18 },
            { kindLabel: 'mypage.maps.mapKind.bookmarks', mapCount: mapSummary[MapSummary.bookmaks] || 0, icon: iconBookMap, mapKind: MapKind.bookmarks,  sizeImage: 18 }
        ];

        return (<Fragment>
            <div className="list-group mypage-sidebar-left-list list-group-none-bd pt-12 pb-8">
                {
                    BASIC_KINDS.map((kind, ind) => {
                        const { mapKind } = kind;
                        return (<BasicKindRecord
                            isActive={selectedGroupBasic === mapKind}
                            onClick={() => this.callGetMapList(mapKind, null, 0)}
                            kind={kind} key={ind} />)
                    })
                }
            </div>
        </Fragment>)
    }
}

export default withTranslation()(KindBasic);