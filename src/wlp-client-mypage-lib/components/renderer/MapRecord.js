import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';

import MapRecordDto from 'wlp-client-service/dto/map/MapRecordDto';
import UpIcon from 'wlp-client-common/component/Icons/UpIcon';
import NewIcon from 'wlp-client-common/component/Icons/NewIcon';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import MapRecordActions from './MapRecordActions';
import starIconS_off from 'wlp-client-common/images/icon/starIconS_off.png';
import starIconS from 'wlp-client-common/images/icon/starIconS.png';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import { IS_TOUCH } from 'wlp-client-common/utils/BrowserUtil';

const wlpDateUtil = new WlpDateUtil();

class MapRecord extends Component {
    state = Object.freeze({
        isShowActions: false,
        mouse: { x: 0, y: 0 },
    });

    callGetMapDetail = (e) => {
        const { onClick } = this.props;
        const target = e.target;
        if (target.name === 'btnOpenMap') {
            return;
        }
        onClick();
    }


    render() {
        const {
            map,
            renameButtonClickHandler,
            duplicateButtonClickHandler,
            deleteButtonClickHandler,
            currentTeamColor,
            callChangeBookmark,
            isActive = false,
        } = this.props;

        const mapRecord = Object.assign(new MapRecordDto(), map);

        const {
            mapTitle,
            contractInfo,
            newFlg,
            updateFlg,
            bookmark,
        } = mapRecord;

        const { teamName, role } = contractInfo;

        let teamNameClasses = ['map-rec-team-name noselect'];
        if (map.ipControlFlag) {
            teamNameClasses.push('team-ip-control noselect-color-cc0033');
        }

        return (<Fragment>
            <div className={'w-listmap-item' + (!IS_TOUCH ? ' listmap-item' : '')}>
                <div className="favitar-icon">
                    <div className="start-toggle noselect" onClick={() => callChangeBookmark(mapRecord)}>
                        {bookmark ?
                            <img src={starIconS} alt="" className="noselect"/> :
                            <img src={starIconS_off} alt="" className="noselect" />
                        }
                    </div>
                </div>
                <div
                    onClick={this.callGetMapDetail}
                    className={`listmap-item-ct cursor-df ${isActive ? 'listmap-item-active' : ''}`}>
                    <h3 className="listmap-title text-truncate noselect noselect-color-777777">
                        {/* Render map title */}{(mapTitle)}
                    </h3>
                    <div className="w-listmap-color text-truncate">
                        <div className={`color-box-items ${currentTeamColor} noselect`}></div>
                        <span className={teamNameClasses.join(' ')}>{/* Render teamname */}{(teamName)}</span>
                    </div>
                
                    <div className={`listmap-item-action`}>

                        {this.renderRecordDateInfo()}
                        {this.renderOpenEditorButton()}
                    </div>
                </div>
                    <div className="group-btn-action d-flex justify-content-end">
                        <div className="btn-group btn-group-sm group-btn-skew" role="group">
                            <div className="btn-group btn-group-sm group-btn-skew" role="group">
                                {updateFlg && <UpIcon />}
                                {newFlg && <NewIcon />}

                                {
                                    role !== RoleType.PARTNER &&
                                    <MapRecordActions
                                        map={mapRecord}
                                        renameButtonClickHandler={renameButtonClickHandler}
                                        duplicateButtonClickHandler={duplicateButtonClickHandler}
                                        deleteButtonClickHandler={deleteButtonClickHandler}/>
                                }
                            </div>
                        </div>
                    </div>
            </div>
        </Fragment>);
    }


    renderRecordDateInfo = () => {
        const { map, isActive } = this.props;
        const isShow = !IS_TOUCH || (IS_TOUCH && !isActive);

        return isShow && (
            <span className="hover-dnone">
                <h4 className="listmap-date noselect noselect-color-777777">
                    {/* format date */}{wlpDateUtil.formatDate(map.lastUpdateTime)}
                </h4>
                <h5 className="listmap-time noselect noselect-color-777777">
                    {/* format time */}{wlpDateUtil.format(map.lastUpdateTime, 'HH:mm')}
                </h5>
            </span>
        );
    }

    renderOpenEditorButton = () => {
        const { t, map, openButtonClickHandler, isActive } = this.props;
        const dBlockClassName = IS_TOUCH ? 'd-block' : 'hover-dblock';
        let isShow = !IS_TOUCH || (IS_TOUCH && isActive);
        
        return isShow && (
            <div className={'btn-open-map ' + dBlockClassName}>
                <button
                    name="btnOpenMap"
                    onClick={() => openButtonClickHandler(map.mapId)}
                    className="btn btn-darkx btn-sm  cursor-df noselect noselect-color-white">
                    {t('mypage.maps.list.operation.openMap')}
                </button>
            </div>
        )
    }

}

export default withTranslation()(MapRecord);