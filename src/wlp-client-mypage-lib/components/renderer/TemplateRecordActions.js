import React, { Fragment } from "react";
import { withTranslation } from "react-i18next";
import TemplateRecordDto from "wlp-client-service/dto/template/TemplateRecordDto";
import LoadModalBodyEnd from "wlp-client-common/component/LoadModalBodyEnd";
import { FaCaretDown } from "react-icons/fa";
import { IS_TOUCH } from "wlp-client-common/utils/BrowserUtil";
import { checkElementClicked } from "wlp-client-common/utils/EventUtils";

class TemplateRecordActions extends React.Component {
    static ACTION_MODIFY = 'modify';
    static ACTION_DELETE = 'delete';

    constructor(props) {
        super(props);
        this.state = {
            isShowActions: false,
            mouse: { x: 0, y: 0 },
            isMouseOnMenu: false
        }

        this.actionsContainerRef = null;
        this.btnOpenActionsRef = null;
    }

    hideActions = () => {
        if (this.state.isShowActions) {
            const cb = () => this.setState({ isShowActions: false });
            
            //[ThuyTV] immidiate hide actions on touch devices
            if (IS_TOUCH) {
                cb();
            } else if (!this.state.isMouseOnMenu) {
                setTimeout(cb, 500);
            }
        }
    }

    showActions = (e) => {
        let mousePos = { x: 0, y: 0 };
        const { pageX, pageY, clientX, clientY } = e;

        if (typeof (pageX) === 'number' && typeof (pageY) === 'number') {
            mousePos.x = pageX;
            mousePos.y = pageY;
        } else if (typeof (clientX) === 'number' && typeof (clientY) === 'number') {
            mousePos.x = clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            mousePos.y = clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }

        const disToBottom = window.innerHeight - mousePos.y;
        const popupHeight = 130;
        if (disToBottom < popupHeight) {
            mousePos.y = mousePos.y - (popupHeight - disToBottom)
        }

        if (this.state.isShowActions === false) {
            this.setState(() => ({
                isShowActions: true,
                mouse: mousePos
            }))
        }
    }

    handleMouseOverMenu = () => {
        if (this.state.isMouseOnMenu === false) {
            this.setState({ isMouseOnMenu: true });
        }
    }

    handleMouseLeaveMenu = () => {
        this.setState({ isMouseOnMenu: false }, this.hideActions);
    }

    handleUserSelectedAction = (actionName, data) => {
        if (actionName === TemplateRecordActions.ACTION_MODIFY) {
            this.props.modifyButtonClickHandler(data);
        } else if (actionName === TemplateRecordActions.ACTION_DELETE) {
            this.props.deleteButtonClickHandler(data);
        }

        this.hideActions();
    }

    setBtnOpenActionsRef = (ref) => {
        this.btnOpenActionsRef = ref;
    } 

    setActionsContainerRef = (ref) => {
        this.actionsContainerRef = ref;

        if (this.actionsContainerRef instanceof HTMLElement && IS_TOUCH) {
            document.addEventListener('touchstart', this.handleDoucmentTouchStart);
        }
    }

    handleDoucmentTouchStart = (evt) => {
        if (this.state.isShowActions &&
            !checkElementClicked(evt, this.actionsContainerRef) &&
            !checkElementClicked(evt, this.btnOpenActionsRef)
        ) {
            this.hideActions();
        }
    }

    render() {
        const { template, t } = this.props;

        const templateRecord = Object.assign(new TemplateRecordDto(), template);
        const { isMouseOnMenu, isShowActions, mouse } = this.state;

        const style = { position: 'absolute', top: `${mouse.y - 5}px`, left: `${mouse.x - 175}px`, maxWidth: '166px' };
        let className = ['map-rec-menu dropdown-menu dropdown-menu-right dropdown-menu-bg-black d-block'];

        return (<Fragment>
            <button
                ref={this.setBtnOpenActionsRef}
                onClick={this.showActions}
                onBlur={() => !isMouseOnMenu && this.hideActions()}
                type="button"
                className="btn bg-light btn-skew-l dropdown-toggle pr-0 pl-3 cursor-df">
                <span className="btn-skew-text"><i className="fas fa-bars noselect"></i></span>
                <FaCaretDown size={14} />
            </button>
            {isShowActions &&
                <LoadModalBodyEnd>
                    <div
                        ref={this.setActionsContainerRef}
                        style={style}
                        onMouseOver={this.handleMouseOverMenu}
                        onMouseLeave={this.handleMouseLeaveMenu}
                        className={className.join(' ')}>
                        <button
                            onClick={() => this.handleUserSelectedAction(TemplateRecordActions.ACTION_MODIFY, templateRecord)}
                            className="dropdown-item dropdown-item-overflow custom-dropdown-item noselect">
                            {t('mypage.templates.list.operation.changeInfo')}
                        </button>
                        <div className="dropdown-divider"></div>
                        <button
                            onClick={() => this.handleUserSelectedAction(TemplateRecordActions.ACTION_DELETE ,templateRecord)}
                            className="dropdown-item custom-dropdown-item noselect">
                            {t('mypage.templates.list.operation.delete')}
                        </button>
                    </div>
                </LoadModalBodyEnd>
            }
        </Fragment>)
    }
}

export default withTranslation()(TemplateRecordActions);