import React, { Component, createRef } from "react";
import { withTranslation } from "react-i18next";

import TeamRecord from "./TeamListRenderer/TeamRecord";
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import MapKind from "wlp-client-service/consts/MapKind";
import TeamListDto from "wlp-client-service/dto/team/TeamListDto";
import KindTypes from 'wlp-client-mypage-lib/consts/KindTypes';
import downArrowIcon from 'wlp-client-common/images/icon/downArrowIcon.svg';
import rightArrowIconActive from 'wlp-client-common/images/icon/rightArrowIconActive.svg';
import Link from "wlp-client-common/component/Link";
import MypageStateStorage from "wlp-client-mypage/MypageStateStorage";
import { detect } from 'detect-browser';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { IS_TOUCH } from "wlp-client-common/utils/BrowserUtil";

const strageUtils = new LocalStorageUtils();
const browser = detect();

class TeamListRenderer extends Component {

    // init component state with immutable object
    state = ({
        showTeamListFlag: true,
        isDragging: false,
        teamLists: [],
        itemsOnDragStart: [],
        touchTimer: 0, 
        teamListWrapperTouchAction: 'none'
    });

    listGroup = createRef();

    componentDidMount() {
        document.addEventListener('dragover', this.onDragOverSafari);
    }

    componentWillUnmount() {
        document.removeEventListener('dragover', this.onDragOverSafari);
    }

    toggleShowTeamList = () => {
        this.setState(({ showTeamListFlag }) => ({
            showTeamListFlag: !showTeamListFlag
        }));
    };

    getCurrentTeamColor = (teamsColors = {}, currentTeam) => {
        const { colorTeamDefault } = this.props;
        const { contractId } = currentTeam;
        return (teamsColors && teamsColors[contractId]) ? teamsColors[contractId] : colorTeamDefault;
    };

    onDragOverSafari = (e) => {
        if (browser.name.toLowerCase() !== "safari") {
            return;
        }
        e.preventDefault();
    }

    allowDrop = (e) => {
        e.preventDefault();
    }

    onDragStart = (i, e) => {
        this.setState({touchTimer: Date.now()})
        const target = e.target.closest('.list-group-ps')
        this.setState({
            dragging: i,
            isDragging: true,
            itemsOnDragStart: this.state.teamLists
        });
        target.classList.add('team-dragging');

        target.style.opacity = 0.3;
        setTimeout(function () {
            target.style.opacity = 1;
        }, 1);
    }

    onDragEnd = (contractId, e) => {
        this.setState({
            overIndex: null
        });
        const target = e.target.closest('.list-group-ps')
        target.classList.remove('team-dragging');
    }

    onDragOver = (i, e) => {
        if(!IS_TOUCH) {
            e.preventDefault();
        }
        this.setState({
            overIndex: this.calculateDropIndex(e,i)
        });
    }

    onDragEnter = (i, e) => {
        e.preventDefault();
        this.setState({
            overIndex: this.calculateDropIndex(e,i)
        });
    }

    calculateDropIndex = (e, i) => {
        let itemIndex = i;

        let target = e.target.closest('.list-group-ps');
        if(IS_TOUCH) {
            target = document.elementFromPoint(e.touches[0].clientX, e.touches[0].clientY).closest('.list-group-ps')
        }
        
        if(target) {
            itemIndex = parseInt(target.getAttribute('data-team-index'))

            const pageY = (e.clientY || e.touches[0].clientY) - target.offsetTop - target.offsetHeight; 
            const wh = target.offsetHeight / 2;
            if (pageY < wh) {
                itemIndex = itemIndex - 1;
            }
        } else {
            if((e.clientY || e.touches[0].clientY) <= this.listGroup.offsetTop + 40) {
                itemIndex = -1
            } else if ((e.clientY || e.touches[0].clientY) > this.listGroup.offsetTop + this.listGroup.offsetHeight) {
                itemIndex = this.state.teamLists.length -1
            }
        }

        return itemIndex;
    }

    /**
     * mapping to jp.co.kokuyo.wlp.mypage.view.MapsView[contractListDataCollectionChangeHandler:284]
     * @param {MouseEvent} e 
     */g
    onTouchStart = () => {
        this.setState({touchTimer: Date.now()})
    }

    onDropHandler = (e) => {
        if(Date.now() - this.state.touchTimer > 500) {
            this.setState({teamListWrapperTouchAction: 'none'})
            const {dragging, overIndex} = this.state;
            const { callGetMapList, setCurrentKindTypes } = this.props;
            const item = this.state.itemsOnDragStart[dragging];
            let newItems = [...this.state.itemsOnDragStart];
            newItems.splice(dragging, 1);
            if( overIndex < 0){
                newItems = [item, ...newItems];
            } else {
                if( dragging > overIndex){
                    newItems.splice(overIndex+1, 0, item);
                } else {
                    newItems.splice(overIndex, 0, item);
                }
            }
            this.setState({ 
                teamLists: newItems, 
                isDragging: false
            }, () => {
                this.convertDataTeamList(this.state.teamLists);
                setCurrentKindTypes(KindTypes.TeamKind, item.contractId);
                callGetMapList(MapKind.all, item.contractId);
            });
        } else {
            this.setState({teamListWrapperTouchAction: 'auto'})
        }
        if(this.listGroup) {
            this.listGroup.classList.remove('team-list-dragging')
        }
    }

    convertDataTeamList = (teamsList) => {
        teamsList = teamsList.filter(team=> typeof team !== 'undefined');
        teamsList = teamsList.map(team => team.contractId);
        this.setDataSortTeamListLocal(teamsList);
    }

    setDataSortTeamListLocal = (data) => {
        strageUtils.setLocalStrage(strageUtils.TEAM_ORDER, data);
    }

    getDataSortTeamListLocal = (teamLists, data = []) => {
        if (!teamLists) {
            this.convertDataTeamList(teamLists);
        }

        const teamListsLength = teamLists.length;
        const teamsListOld1 = [];
        const teamsListOld1WithoutEmpty = [];
        const teamsListOld2 = [];

        if (data && Array.isArray(data) === true && data.length > 0) {
            for (let i = 0; i < teamListsLength; i++) {
                let findInLocalList = null;

                //Find in local storage list
                for (let key in data) {
                    if (teamLists[i].contractId === data[key]) {
                        findInLocalList = teamLists[i];
                        teamsListOld1[key] = teamLists[i];
                    }
                }

                //If current team is not exist and sorted in local team
                if (!findInLocalList) {
                    teamsListOld2.push(teamLists[i]);
                }
            }

            for (let i = 0; i < teamsListOld1.length; i++) {
                if (teamsListOld1[i]) {
                    teamsListOld1WithoutEmpty.push(teamsListOld1[i]);
                }
            }
            
        } else {
            return teamLists;
        }
        
        return teamsListOld1WithoutEmpty.concat(teamsListOld2);
    }

    handleClickTeamRecord = (team, selectedIndex) => {
        ActionLogger.click('User_Mypage_ListCategory', `Team_item`);
        MypageStateStorage.setItem(MypageStateStorage.CURRENT_CONTRACT, team);
        this.props.callGetMapList(MapKind.all, team.contractId, 0, selectedIndex);
    }

    UNSAFE_componentWillReceiveProps({ teamList }) {
        if (
            teamList &&
            typeof teamList === 'object' &&
            Object.values(teamList).length > 0
        ) {
            const localTeamList = strageUtils.getLocalStrage(strageUtils.TEAM_ORDER);

            this.setState({
                teamLists: this.getDataSortTeamListLocal(teamList, localTeamList),
            });
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.teamList !== prevProps.teamList) {
            const localTeamList = strageUtils.getLocalStrage(strageUtils.TEAM_ORDER);
            const teamLists = this.getDataSortTeamListLocal(this.props.teamList, localTeamList);
            if (typeof this.props.setSelectedTeam === 'function') {
                this.props.setSelectedTeam(teamLists);
            }
        }
    }

    render() {
        const { t, teamColor, rectFillColor, selectedTeam } = this.props;
        const { showTeamListFlag, teamLists, overIndex, isDragging } = this.state;

        const dragDropEvents = {
            onDragEnd:this.onDragEnd,
            onDragOver:this.onDragOver,
            onDragStart:this.onDragStart,
            onDragEnter:this.onDragEnter
        };
        return (
            <div className="mypage-sidebar-left-item mypage-left-panel team-list fs-1rem" onDrop={this.onDropHandler} onTouchEnd={this.onDropHandler} onTouchStart={this.onTouchStart}>
                <Link onClick={() => this.toggleShowTeamList()} className="list-group-title text-uppercase collapsed pb-6 cursor-df">
                    <span className="list-group-icon magin-right-reset">
                    <img src={showTeamListFlag && teamLists.length > 0 ? downArrowIcon : rightArrowIconActive} className="carret-icon noselect"  alt="" />
                    </span>
                    <span className="text-uppercase noselect"> {t("mypage.listLabel.teams")} </span>
                </Link>
                {showTeamListFlag && (
                    <div ref={ref => {this.listGroup = ref}} onDragOver={this.allowDrop} style={{touchAction: this.state.teamListWrapperTouchAction}} className={'list-group list-group-none-bd mypage-sidebar-left-list border-bottom-0 team-group ' + (isDragging ? 'team-list-dragging' : '')}>
                        <div  className="position-relative list-group-ps" >
                            <div className={'team-index ' + ((( overIndex === -1) && isDragging) ? 'team-index-active' : '')}></div>
                        </div>
                        {       
                            teamLists && 
                            Array.isArray(teamLists) &&
                            teamLists.map((item, index) => {
                                let team = new TeamListDto();
                                team = item;
                                if( !team ){
                                    return false;
                                }
                                const { contractId } = team;

                                return (
                                    <TeamRecord
                                        onClick={() => this.handleClickTeamRecord(team, index)}
                                        isActive={selectedTeam === contractId}
                                        key={index}
                                        contractId={contractId}
                                        rectFillColor={rectFillColor}
                                        team={team}
                                        isDragging={isDragging}
                                        teamIndex={index}
                                        overIndex={overIndex}
                                        currentTeamColor={this.getCurrentTeamColor(teamColor, team)}
                                        isLabelBold={team.updateMap && team.mapCount > 0}
                                        dragDropEvents={dragDropEvents}
                                    />
                                );
                            })
                        }
                    </div>
                )}
            </div>
        );
    }
}

export default withTranslation()(TeamListRenderer);
