import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import TemplatePreviewPanel from 'wlp-client-mypage-lib/components/teamplates/TemplatePreviewPanel';
import TemplateRecord from './TemplateRecord';
import ModifyTemplateModal from '../modals/ModifyTemplateModal';
import DeleteTemplateModal from '../modals/DeleteTemplateModal';
import TemplateService from 'wlp-client-service/service/TemplateService';
import GetTemplateRecordDetailConfigDto from 'wlp-client-service/dto/template/GetTemplateRecordDetailConfigDto';
import DeleteTemplateConfigDto from 'wlp-client-service/dto/template/DeleteTemplateConfigDto';
import ContractType from 'wlp-client-service/consts/ContractType';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import SessionDto from 'wlp-client-service/dto/SessionDto';
import LogonDto from 'wlp-client-service/dto/auth/LogonDto';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import { showWarningDialog, showErrorDialog } from 'wlp-client-common/utils/DialogUtils';
import i18n from 'i18n';

const actionTypes = { modify: 'modify', delete: 'delete' };
const templateService = new TemplateService();
const localStorageUtil = new LocalStorageUtils();
class TemplateListRenderer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currenActionModal: null, /**modify, delete */
            currentTemplateOnAction: null,
            currentTemplateSelected: null
        }

        this.templateListBody = React.createRef();
    }
    
    componentDidUpdate({isReloadTemplateList}){
        if(isReloadTemplateList && this.templateListBody.current){
            this.templateListBody.current.scrollTop = 0;
        }
    }


    modifyButtonClickHandler = async (templateRecord) => {
        let requestDto = new GetTemplateRecordDetailConfigDto();
        requestDto.templateId = templateRecord.templateId;
        const result = await templateService.getTemplateRecordDetail(requestDto);
        if (result.isSucceed) {
            this.setState({
                currentTemplateSelected: result.templateRecordDetail,
                currenActionModal: actionTypes.modify,
                currentTemplateOnAction: templateRecord
            });
        }
        else {
            showWarningDialog(result.message)
        }
    }

    reloadCategoryList = (currentCategoryId) => {
        const { setCategoryList, categoryList } = this.props;

        const categoryListNew = categoryList.map((cate, index) => {
            if (cate.id === currentCategoryId) {
                cate.count = cate.count - 1;
            }
            return cate;
        });

        return setCategoryList(categoryListNew);
    }

    callApiDeleteTemplate = async () => {
        let requestDto = new DeleteTemplateConfigDto();
        const { currentTemplateOnAction } = this.state;
        let session = new SessionDto();
        let logonInfo = new LogonDto();
        logonInfo = localStorageUtil.getUserSession();
        session = logonInfo.sessionDto;
        requestDto.templateId = currentTemplateOnAction.templateId;

        requestDto = {
            ...requestDto,
            "contractId": -1,
            "contractType": ContractType.team,
            "currentState": CurrentState.MAP,
            "localeCode": session.localeCode,
            "templateId": requestDto.templateId
        }

        const result = await templateService.deleteTemplate(requestDto);

        if (result && result.isSucceed) {
            window.scrollTo(0, 0);
            this.props.callGetTemplateList(currentTemplateOnAction.categoryId);
            this.reloadCategoryList(currentTemplateOnAction.categoryId);
        }

        if (!result.isSucceed) {
            showErrorDialog(i18n.t(result.messageKey))
        }
    }

    deleteButtonClickHandler = (templateRecord) => {
        this.setState({
            currenActionModal: actionTypes.delete,
            currentTemplateOnAction: templateRecord
        });
    }

    hideCurrenActionModal = () => {
        this.setState({ currenActionModal: null });
    }

    render() {
        const {
            currenActionModal,
            currentTemplateOnAction,
            currentTemplateSelected
        } = this.state;

        const {
            templateList = [],
            teamList,
            selectedTemplate,
            callGetTemplateDetail,
            callCreateMap,
            isShowHiddenNavRight,
            categoryList,
            callGetTemplateList,
            isShowTemplateDetail,
            selectTemplateId
        } = this.props;

        return (<Fragment>
            <div className={'mypage-content-list mypage-content-list-js d-flex ' + (isShowHiddenNavRight ? 'offcanvas' : '')}>
                <div ref={this.templateListBody} className="w-list-templates">
                    <div className="w-list-templates-content">
                        {
                            templateList &&
                            Array.isArray(templateList) &&
                            templateList.map((template, index) => {
                                return <TemplateRecord
                                    key={index}
                                    onClick={() => callGetTemplateDetail(template.templateId)}
                                    callGetTemplateDetail={callGetTemplateDetail}
                                    selectedTemplate={selectedTemplate}
                                    modifyButtonClickHandler={this.modifyButtonClickHandler}
                                    deleteButtonClickHandler={this.deleteButtonClickHandler}
                                    templateId={template.templateId}
                                    isActive={selectTemplateId === template.templateId}
                                    teamList={teamList}
                                    callCreateMap={callCreateMap}
                                    template={template} />
                            })
                        }
                    </div>
                </div>
                <TemplatePreviewPanel isShowTemplateDetail={isShowTemplateDetail} selectedTemplate={selectedTemplate} />
            </div>
            {
                /**Modals operation area */
                currentTemplateOnAction &&
                currenActionModal && {
                    /*******************/
                    /**Show modify popup*/
                    /*******************/
                    [actionTypes.modify]: (<ModifyTemplateModal
                        hideCurrenActionModal={this.hideCurrenActionModal}
                        currentTemplateOnAction={currentTemplateOnAction}
                        currentTemplateSelected={currentTemplateSelected}
                        callGetTemplateList={callGetTemplateList}
                        categoryList={categoryList} />),
                    /*******************/
                    /**Show delete popup*/
                    /*******************/
                    [actionTypes.delete]: (<DeleteTemplateModal
                        isShow={true}
                        currentTemplateOnAction={currentTemplateOnAction}
                        callGetTemplateList={callGetTemplateList}
                        callApiDeleteTemplate={this.callApiDeleteTemplate}
                        cancelHandler={this.hideCurrenActionModal} />)
                }[currenActionModal]}
        </Fragment>)
    }
}

export default withTranslation()(TemplateListRenderer);

