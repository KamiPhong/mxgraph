import React from 'react';
import i18n from 'i18n';
import MapUtilMemberList from 'wlp-client-mypage-lib/utils/MapUtilMemberList';
import presonalUser from 'wlp-client-common/images/avators/noimage.png';
import ImageDefault from 'wlp-client-common/utils/ImageDefault';

const mapUtilMemberList = new MapUtilMemberList();

class MemberDgUserRenderer extends React.Component { 
    render() { 
        const { data } = this.props;

        let avatar = data.invitingFlg ? presonalUser : (ImageDefault[data.userImgUrl] ? ImageDefault[data.userImgUrl] : data.userImgUrl);
        return <div className="d-flex">
                    <div className="flex-auto">
                        <img className="mr-10px noselect" src={avatar} alt="" width="37"/>
                    </div>
                    <div className="media media-user-info">
                        <div className="media-body d-flex">
                            <h6 className={`font-weight-bold reset-user-name text-truncate noselect white-space-pre`}>
                                {data.userName}
                            </h6>
                            <div className="ml-auto flex-auto">
                                <div className="badge badge-bg-gray noselect noselect-color-5d5959">{i18n.t(mapUtilMemberList.getPermissionLabelById(data.userType))}</div>
                                {
                                    data.invitingFlg && (
                                        <div className="badge badge-bg-inviting ml-2 noselect noselect-color-a0802d">{i18n.t('global.inviting')}</div>
                                    )
                                }
                            </div>
                        </div>
                        <div className="font-size-12 email-color d-block text-truncate noselect noselect-color-777777">{data.email}</div>
                    </div>
                </div>;
    }
}


export default MemberDgUserRenderer;