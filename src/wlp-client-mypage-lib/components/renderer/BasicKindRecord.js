import React from 'react';
import { useTranslation } from 'react-i18next';

const BasicKindRecord = ({ kind, onClick, isActive }) => {
    const { kindLabel, mapCount, icon, sizeImage } = kind;
    const { t } = useTranslation();

    return (
        <div
            onClick={onClick}
            className={`list-group-item list-group-item-action text-uppercase mb-1 btn-custom-links txt-shadow-245 ${isActive ? 'item-active' : ''}`}>
            <span className="list-group-icon">
                <img src={icon} alt="" className="img-fluid noselect" width={sizeImage} />
            </span>
            <strong className="noselect">{t(kindLabel)}</strong>
            {
                typeof mapCount === 'number' &&
                (<span className="ml-1 noselect">({mapCount})</span>)
            }
        </div>
    )
}

export default BasicKindRecord;