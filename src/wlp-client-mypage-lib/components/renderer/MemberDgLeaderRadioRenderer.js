import React, { Fragment } from 'react';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import { memberDgLeaderRadioRendererEvent } from 'wlp-client-mypage-lib/events/MemberDgLeaderRadioRendererEvent';
import ActionLogger from 'wlp-client-common/ActionLogger';

class MemberDgLeaderRadioRenderer extends React.Component { 
    state = {
        selectedOption: null,
        selectedCheckbox: null,
    };

    onChangeHandler = (e) => {
        ActionLogger.click('User_Mypage_TeamSetting_[メンバーリスト]', `Checked_Leader`);
        const { data } = this.props;
        const target = e.target;
        let leaderFlg = false;
        if (data == null){
            return;
        }

        if (data.adminFlg) {
            leaderFlg = false;
        } else { 
            leaderFlg = target.checked;
        }

        memberDgLeaderRadioRendererEvent.fireEvent(memberDgLeaderRadioRendererEvent.CHECK_BOX_ON_CHANGE, data.userId, leaderFlg);

    }

    render() { 
        const { data } = this.props;
        let enabled = false;

        if (data.adminFlg){
            enabled = false;
        } else if (data.invitingFlg) {
            enabled = false;
        } else if ( RoleType.ADMIN === data.userType || RoleType.LEADER === data.userType || RoleType.MEMBER === data.userType ) {
            enabled = true;
        }
        
        return (
            <Fragment>
                <label className="input-radio-rs radio-facilitator">
                    <input className="resize-cb-rd reset-button-radio" type="radio"
                           disabled={!enabled}
                           name="gridRadios" id="gridRadios1" value={data.userType}
                           checked={data.leaderFlg}
                           onChange={this.onChangeHandler}
                    />
                    <span className="checkmark"></span>
                </label>
            </Fragment>
        );
    }
}


export default MemberDgLeaderRadioRenderer;