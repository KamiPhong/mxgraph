import React, { Component } from "react";
import { withTranslation } from "react-i18next";

import TeamListRenderer from 'wlp-client-mypage-lib/components/renderer/TeamListRenderer';

class KindTeam extends Component {

    render() {
        const {
            teamColor,
            rectFillColor,
            colorTeamDefault,
            teamList,
            callGetMapList,
            selectedTeam,
            setCurrentKindTypes,
            setSelectedTeam
        } = this.props;
        
        return (
            <TeamListRenderer
                callGetMapList={callGetMapList}
                selectedTeam={selectedTeam}
                teamList={teamList}
                teamColor={teamColor}
                rectFillColor={rectFillColor}
                setCurrentKindTypes={setCurrentKindTypes}
                colorTeamDefault={colorTeamDefault}
                setSelectedTeam={setSelectedTeam} />
        )
    }
}

export default withTranslation()(KindTeam);