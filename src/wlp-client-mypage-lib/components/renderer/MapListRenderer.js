import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';

import MapPreviewPanel from '../maps/MapPreviewPanel';
import MapRecord from './MapRecord';
import MapListDto from 'wlp-client-service/dto/map/MapListDto';
import RenameMapModal from '../modals/RenameMapModal';
import CopyMapModal from '../modals/CopyMapModal';
import DeleteMapModal from '../modals/DeleteMapModal';
import ActionLogger from 'wlp-client-common/ActionLogger';

const actionTypes = { rename: 'rename', duplicate: 'duplicate', delete: 'delete' }
class MapListRenderer extends Component {
    state = {
        maps: this.props.mapList,
        currenActionModal: null,/**rename, duplicate, delete */
        currentMapOnAction: null
    }

    constructor(props){
        super(props);
        this.mapListBody = React.createRef();
    }

    componentDidUpdate(){
        if(this.props.isReloadMapList && this.mapListBody.current){
            this.mapListBody.current.scrollTop = 0;
        }
    }

    getCurrentTeamColor = (teamsColors = {}, currentMap) => {
        const { colorTeamDefault } = this.props;
        const { contractId } = currentMap.contractInfo;
        
        return (teamsColors && teamsColors[contractId]) ? teamsColors[contractId] : colorTeamDefault;
    };


    openButtonClickHandler = (mapId) => {
        ActionLogger.click('User_Mypage_ListMap', 'Button_OpenMap');
        const { callCheckExistMap } = this.props;
        callCheckExistMap(mapId);
        this.props.setRecentMaps(mapId);
           
    }

    renameButtonClickHandler = (mapRecord) => {
        ActionLogger.click('User_Mypage_ListMap', 'Rename_Map_Option');
        this.setState({
            currenActionModal: actionTypes.rename,
            currentMapOnAction: mapRecord
        });
    }

    duplicateButtonClickHandler = (mapRecord) => {
        ActionLogger.click('User_Mypage_ListMap', 'Duplicate_Map_Option');
        this.setState({
            currenActionModal: actionTypes.duplicate,
            currentMapOnAction: mapRecord
        });
    }

    deleteButtonClickHandler = (mapRecord) => {
        ActionLogger.click('User_Mypage_ListMap', 'Delete_Map_Option');
        this.setState({
            currenActionModal: actionTypes.delete,
            currentMapOnAction: mapRecord
        });
    }

    setSelectedRecord = (recordIndex) => {
        this.setState(() => ({ selectedRecord: recordIndex }));
    }

    mapRecord_clickHandler = (index, map = new MapListDto()) => {
        ActionLogger.click('User_Mypage_ListMap', 'Selected_Item_Map');
        const { callGetMapRecordDetail, selectedMapDetail } = this.props;
        // Chặn click vào Map đang được Select
        if(map.mapId === selectedMapDetail.mapId) {
            return;
        }
        
        this.setSelectedRecord(index);

        callGetMapRecordDetail(map);
    }

    hideCurrenActionModal = () => {
        this.setState({ currenActionModal: null });
    }

    callChangeBookmark = (bookmarkChangeRecord) => {
        ActionLogger.click('User_Mypage_ListMap', 'Button_ChangeBookmark');
        if (typeof this.props.callChangeBookmark === "function") {
            this.props.callChangeBookmark(bookmarkChangeRecord)
        }
    }

    render() {
        const {
            currenActionModal,
            currentMapOnAction
        } = this.state;

        const {
            userInfo,
            callCreateTemplate,
            callDeleteShareMember,
            callAddShareMember,
            teamColor,
            selectedMap,
            callRenameMap,
            callCopyMap,
            callDeleteMap,
            setSelectedMap,
            categoryList,
            mapList,
            isShowHiddenNavRight,
            selectedMapDetail,
            setSelectedMapDetail,
            callDeleteSharePartner,
            goToStartPage
        } = this.props;

        return (<Fragment>
            <div className={'mypage-content-list mypage-content-list-js d-flex ' + (isShowHiddenNavRight ? 'offcanvas' : '')}>
                <div ref={this.mapListBody} className="w-list-templates">
                    <Fragment>
                    {
                        mapList &&
                        Array.isArray(mapList) &&
                        mapList.map((map, index) => {
                            return (<MapRecord
                                setSelectedMap={setSelectedMap}
                                onClick={() => this.mapRecord_clickHandler(index, map)}
                                isActive={selectedMap && selectedMap.mapId === map.mapId}
                                openButtonClickHandler={this.openButtonClickHandler}
                                renameButtonClickHandler={this.renameButtonClickHandler}
                                duplicateButtonClickHandler={this.duplicateButtonClickHandler}
                                deleteButtonClickHandler={this.deleteButtonClickHandler}
                                handleStarClick={this.handleStarClick}
                                key={map.mapId}
                                callChangeBookmark={this.callChangeBookmark}
                                currentTeamColor={this.getCurrentTeamColor(teamColor, map)}
                                map={map} />
                            )
                        })
                    }
                    </Fragment>
                </div>
                <MapPreviewPanel
                    callDeleteSharePartner={callDeleteSharePartner}
                    callAddShareMember={callAddShareMember}
                    callDeleteShareMember={callDeleteShareMember}
                    userInfo={userInfo}
                    selectedMapDetail={selectedMapDetail}
                    categoryList={categoryList}
                    setSelectedMapDetail={setSelectedMapDetail}
                    callCreateTemplate={callCreateTemplate} />
            </div>
            {
                /**Modals operation area */
                currentMapOnAction &&
                currenActionModal && {
                    /*******************/
                    /**Show rename popup*/
                    /*******************/
                    [actionTypes.rename]: (<RenameMapModal
                        cancelHandler={this.hideCurrenActionModal}
                        isShow={true}
                        selectedMap={currentMapOnAction}
                        callRenameMap={callRenameMap}
                        goToStartPage={goToStartPage} />),
                    /*******************/
                    /**Show copy popup */
                    /*******************/
                    [actionTypes.duplicate]: (<CopyMapModal
                        cancelHandler={this.hideCurrenActionModal}
                        isShow={true}
                        selectedMap={currentMapOnAction}
                        callCopyMap={callCopyMap}
                        goToStartPage={goToStartPage} />),
                    /*******************/
                    /**Show delete popup*/
                    /*******************/
                    [actionTypes.delete]: (<DeleteMapModal
                        isShow={true}
                        callDeleteMap={callDeleteMap}
                        selectedMap={currentMapOnAction}
                        cancelHandler={this.hideCurrenActionModal} />)
                }[currenActionModal]}
        </Fragment>)
    }
}

export default withTranslation()(MapListRenderer);

