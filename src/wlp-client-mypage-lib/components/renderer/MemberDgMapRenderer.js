import React from 'react';
import i18n from 'i18n';
import { memberDgItemRendererEvent } from 'wlp-client-mypage-lib/events/MemberDgItemRendererEvent';


class MemberDgMapRenderer extends React.Component { 

    state = {
        focusBtn: false
    }

    onClickHandler = (e) => {
        const { data } = this.props;
        memberDgItemRendererEvent.fireEvent(memberDgItemRendererEvent.MAP_CLICK, data, e);
        this.setState({focusBtn: true});
    }

    componentDidMount() {
        memberDgItemRendererEvent.addListener(memberDgItemRendererEvent.MAP_CLOSE, () => {
            this.setState({ focusBtn: false })
        });
    }

    render() { 
        const { data } = this.props;
        const { focusBtn } = this.state;

        let attr = {
            className:`btn btn-hover-wt w-100 pt-1 pb-1 reset-pd-btn ${focusBtn ? 'reset-pd-btn-focus' : ''} `
        };

        if (data.maps > 0) {
            attr.onClick = this.onClickHandler;
        }
        return  <div className="dropdown">
                    <div {...attr} style={{boxShadow: 'none'}}>
                        <span className="display-4 font-weight-bold map-font-size noselect noselect-color-bdbdbd">{data.maps}</span>
                        <sub className="maps-sub noselect">{i18n.t('mypage.teamSettings.member.mapUnit')}</sub>
                    </div>
                </div>;
    }
}


export default MemberDgMapRenderer;