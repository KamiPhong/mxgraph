import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";

import KindTeam from "./KindTeam";
import KindCategory from "./KindCategory";
import KindBasic from "./KindBasic";

class KindListRenderer extends Component {
    render() {
        const {
            teamColor,
            rectFillColor,
            colorTeamDefault,
            teamList,
            callGetMapList,
            mapSummary,
            currentMapKind,
            selectedGroupBasic,
            selectedTeam,
            selectedCategory,
            categoryList,
            callGetTemplateList,
            setCurrentKindTypes,
            setSelectedTeam
        } = this.props;

        return (<Fragment>
            <KindBasic
                currentMapKind={currentMapKind}
                selectedGroupBasic={selectedGroupBasic}
                callGetMapList={callGetMapList}
                mapSummary={mapSummary} />
            <div className="mypage-sidebar-left-collapse pt-8 pb-12">
                <KindTeam
                    callGetMapList={callGetMapList}
                    selectedTeam={selectedTeam}
                    teamList={teamList}
                    teamColor={teamColor}
                    rectFillColor={rectFillColor}
                    setCurrentKindTypes={setCurrentKindTypes}
                    colorTeamDefault={colorTeamDefault}
                    setSelectedTeam={setSelectedTeam}/>
                <KindCategory
                    selectedCategory={selectedCategory}
                    callGetTemplateList={callGetTemplateList}
                    categoryList={categoryList}/>
            </div>
        </Fragment>)
    }
}

export default withTranslation()(KindListRenderer);