import React, { Component } from 'react';
import ColorBoxList from "./ColorBoxList";
import ActionLogger from 'wlp-client-common/ActionLogger';

let zIndex = 1;
class TeamRecord extends Component {
    //init component state with immutable object
    state = Object.freeze({
        showColorPickerFlag: false,
    });

    toggleShowColorPicker = () => {
        ActionLogger.click('User_Mypage_ListCategory', `button_team_color`);
        this.setState(({ showColorPickerFlag }) => ({ showColorPickerFlag: !showColorPickerFlag }));
    }

    setZindexForColorPicker = () => {
        let zIndexNew = zIndex + 1;
        zIndex = zIndexNew;
    }

    onDragStartHandler = (e) => {
        this.callDragDropEvent(e,'onDragStart');
    }

    onDragEnterHandler = (e) => {
        this.callDragDropEvent(e,'onDragEnter');
    }

    onDragOverHandler = (e) => {
        this.callDragDropEvent(e,'onDragOver');
    }

    onDragEndHandler = (e) => {

        this.callDragDropEvent(e,'onDragEnd', this.props.contractId );
    }

    callDragDropEvent = (event, callback , key) => {
        const { dragDropEvents, teamIndex } = this.props;
        if (typeof dragDropEvents[callback] === 'function') {
            if (typeof key === 'undefined') {
                key = teamIndex;
            }
            dragDropEvents[callback](key, event);
        }
    }

    render() {
        const {
            teamIndex,
            overIndex,
            isDragging,
            team,
            isActive,
            currentTeamColor,
            rectFillColor,
            onClick,
            isLabelBold
        } = this.props;

        const { showColorPickerFlag } = this.state;
        const { teamName, mapCount, ipControlFlag } = team;

        return (
            <div 
                className="position-relative list-group-ps" 
                draggable="true"
                onDragStart={this.onDragStartHandler}
                onDragEnter={this.onDragEnterHandler}
                onDragOver={this.onDragOverHandler}
                onDragEnd={this.onDragEndHandler}
                data-team-index={teamIndex}
                onTouchStart={this.onDragStartHandler}
                onTouchMove={this.onDragOverHandler}
                onTouchEnd={this.onDragEndHandler}
            >
                <div className={`color-box-items mr-2 color-box-pos ${currentTeamColor} noselect`}
                    data-toggle="collapse" onClick={() => {this.toggleShowColorPicker(); this.setZindexForColorPicker()}} 
                    style={{ zIndex: zIndex }}>
                    <ColorBoxList
                        isShow={showColorPickerFlag}
                        team={team}
                        zIndex={zIndex}
                        toggleShowColorPicker={this.toggleShowColorPicker}
                        rectFillColor={rectFillColor} />
                </div>

                <div onClick={!isActive ? onClick : () => { }} className={`list-group-item pl-20 w-100 pd-left-40px ${isActive ? 'item-active' : ''} pl-40 team-btn`}>
                    <span className={`${isLabelBold ? 'font-weight-bold' : ''} noselect text-truncate team-color ${ipControlFlag ? 'team-ip-control noselect-color-cc0033' : ''}`}>{teamName}</span>
                    <span className="ml-1 noselect">({mapCount})</span>
                </div>
                <div className={'team-index ' + (((teamIndex === overIndex) && isDragging) ? 'team-index-active' : '')}></div>
            </div>
        )
    }
}

export default TeamRecord;