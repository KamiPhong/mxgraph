import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class ColorBoxItem extends Component {
    render() {
        const boxItem = this.props.boxItem;
        const { rectFillColor, team, className } = this.props;

        return (
            <div className={`w-25pt text-center padding-color-box ${className}`}>
                <span className={`color-box-items ${boxItem.color} noselect`}
                    onClick={() => rectFillColor(team.contractId, boxItem.color)}>
                </span>
            </div> 
        );
    }
}

export default withTranslation()(ColorBoxItem);
