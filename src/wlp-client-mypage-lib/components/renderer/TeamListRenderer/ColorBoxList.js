import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import ColorBoxItem from "./ColorBoxItem";
import { teamListColor } from "wlp-client-common/Colors/teamColorList";

class ColorBoxList extends Component {

    render() {
        const { rectFillColor, isShow, team, toggleShowColorPicker } = this.props;
        
        return isShow && (
            <div onMouseLeave={toggleShowColorPicker} className="color-box-list color-box-wh" style={{zIndex: 99}}>
                <div className="d-flex flex-wrap h-100">
                    { teamListColor.map((item, index) => {
                        if (index < 4) {
                            return (
                                <ColorBoxItem boxItem={item} key={index} team={team} rectFillColor={rectFillColor} />
                            );
                        } else {
                            return (
                                <ColorBoxItem boxItem={item} key={index} team={team} rectFillColor={rectFillColor} className={'padding-bottom-2px'}/>
                            );
                        }
                    })}
                </div>
            </div>
        );
    }
}

export default withTranslation()(ColorBoxList);
