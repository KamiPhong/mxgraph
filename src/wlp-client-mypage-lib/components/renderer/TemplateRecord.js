import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';

import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import CreateMapFromTemplateModal from 'wlp-client-mypage-lib/components/modals/CreateMapFromTemplateModal';
import NewIcon from 'wlp-client-common/component/Icons/NewIcon';
import TemplateRecordActions from './TemplateRecordActions';
import MapConsts from 'wlp-client-common/consts/MapConsts';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';

const wlpDateUtil = new WlpDateUtil();
const localStorageUtil = new LocalStorageUtils();

class TemplateRecord extends Component {

    state = {
        newMapName: '',
        isShowCreateMapModal: false,
        isMapNameValid: true,
        hideCount: 0,
        isShowActions: false
    }

    constructor(props) {
        super(props);
        const logonInfo = localStorageUtil.getUserSession();
        this.session = (logonInfo && logonInfo.sessionDto) || {};
    }

    showCreateMapModal = () => {
        const { teamList } = this.props;
        if (!teamList || teamList.length === 0) {
            return;
        }
        this.setState({ isShowCreateMapModal: true });
    }

    callGetTemplateDetail = (e) => {
        const { onClick } = this.props;
        const target = e.target;
        if (target.id === 'showCreateMapModal') {
            return;
        }
        onClick();
    }

    setHideCount = () => {
        this.setState(({ hideCount }) => ({ hideCount: hideCount + 1 }));
    }

    hideCreateMapModal = () => {
        const { newMapName } = this.state;
        const { template } = this.props;
        const validateMapName = this.validateMapName(newMapName);

        if (!validateMapName) {
            this.setState(({ hideCount }) => ({ hideCount: hideCount + 1 }), () => {
                const { hideCount } = this.state;
                if (hideCount >= 1) {
                    this.setState({
                        isShowCreateMapModal: false,
                        isMapNameValid: true,
                        hideCount: 0,
                        newMapName: template.templateTitle
                    });
                }
            });
        } else {
            this.setState({
                isShowCreateMapModal: false,
                isMapNameValid: true,
                hideCount: 0,
                newMapName: template.templateTitle
            });
        }
    }

    setStateIsMapNameValid = (isMapNameValid) => {
        this.setState({ isMapNameValid: isMapNameValid });
    }

    validateMapName = (mapName) => {
        return Boolean(mapName);
    }

    newMapNameInput_handleChange = (e) => {
        const { value } = e.target;
        if (value.length <= MapConsts.MAP_NAME_MAX_CHARS) {
            this.setState({
                newMapName: e.target.value
            });
        }
    }

    hideMenuActions = () => {
        if (this.state.isShowActions === true) {
            this.setState(() => ({ isShowActions: false }))
        }
    }

    showMenuActions = () => {
        if (this.state.isShowActions === false) {
            this.setState(() => ({ isShowActions: true }))
        }
    }

    isShowAction() {
        const { template, selectedTemplate } = this.props;
        const { hasEditFlg } = template;
        const { categoryId } = selectedTemplate;


        if (categoryId !== 0) {
            return Boolean(this.session.specialUserFlag);
        }
        return Boolean(hasEditFlg)
    }

    componentDidMount() {
        const { template } = this.props;
        if (template && template.templateTitle) {
            this.setState({
                newMapName: template.templateTitle
            })
        };
    }

    componentDidUpdate(prevProps, prevState) {
        const { template } = prevProps;
        const { templateTitle } = this.props.template;
        if (template && (template.templateTitle !== templateTitle)) {
            this.setState({
                newMapName: templateTitle
            })
        };
    }

    render() {
        const { t,
            template,
            teamList,
            callCreateMap,
            templateId,
            isActive,
            selectedTemplate,
            modifyButtonClickHandler,
            deleteButtonClickHandler
        } = this.props;

        const { isShowCreateMapModal, isMapNameValid, newMapName, isShowActions } = this.state;

        const {
            templateTitle,
            releaseDate,
            companyName,
            teamName,
            categoryName,
            newFlg
        } = template;

        return (<Fragment>
            <div className={`w-listmap-item listmap-item listmap-item-ed`} >
                <div
                    onClick={this.callGetTemplateDetail}
                    onMouseLeave={this.hideMenuActions}
                    className={`listmap-item-ct ${isActive && "listmap-item-active"}`}>
                    <h3 className="listmap-title text-truncate listmap-title-record noselect noselect-color-777777">
                        {(templateTitle)}
                    </h3>
                    <div className="w-listmap-color text-truncate">
                        {companyName ?
                            (
                                <React.Fragment>
                                    {companyName && <div className="w-listmap-color w-listmap-line-height noselect">
                                        {(companyName)}
                                    </div>}
                                    {teamName && <div className="w-listmap-color w-listmap-line-height noselect">
                                        {(teamName)}
                                    </div>}
                                </React.Fragment>
                            ) : (
                                <div className="w-listmap-color">
                                    <span className="map-rec-team-name noselect"> {(categoryName)}</span>
                                </div>
                            )
                        }
                    </div>
                    <div className={`listmap-item-action zindex-0`}>

                        <h4 className="listmap-date hover-dnone noselect noselect-color-777777">
                            {wlpDateUtil.formatDate(releaseDate)}
                        </h4>
                        <h5 className="listmap-time hover-dnone noselect noselect-color-777777">
                            {wlpDateUtil.format(releaseDate, 'HH:mm')}
                        </h5>
                        <div className="hover-dblock btn-open-map">
                            <button id="showCreateMapModal" onClick={this.showCreateMapModal} className={'btn btn-darkx btn-sm cursor-df noselect noselect-color-white' + ((!teamList || teamList.length === 0) ? 'btn-disabled' : '')}>
                                {t('mypage.templates.list.operation.createMap')}
                            </button>
                        </div>
                    </div>
                </div>
                <div className="group-btn-action d-flex justify-content-end">

                    <div className="btn-group btn-group-sm group-btn-skew" role="group">
                        <div className="btn-group btn-group-sm group-btn-skew" role="group">
                            {newFlg && <NewIcon />}
                            {selectedTemplate && this.isShowAction() &&
                                <TemplateRecordActions
                                    template={template}
                                    selectedTemplate={selectedTemplate}
                                    modifyButtonClickHandler={modifyButtonClickHandler}
                                    deleteButtonClickHandler={deleteButtonClickHandler}
                                    onMouseLeave={this.hideMenuActions} show={isShowActions} />
                            }
                        </div>
                    </div>
                </div>

            </div>

            {isShowCreateMapModal &&
                (() => {
                    return <CreateMapFromTemplateModal
                        t={t}
                        template={template}
                        isShow={isShowCreateMapModal}
                        cancelHandler={this.hideCreateMapModal}
                        teamList={teamList}
                        templateId={templateId}
                        callCreateMap={callCreateMap}
                        isMapNameValid={isMapNameValid}
                        setStateIsMapNameValid={this.setStateIsMapNameValid}
                        newMapNameInput_handleChange={this.newMapNameInput_handleChange}
                        validateMapName={this.validateMapName}
                        newMapName={newMapName}
                        setHideCount={this.setHideCount}
                    />
                })()
            }
        </Fragment>);
    }

}

export default withTranslation()(TemplateRecord);