import React from 'react';
import i18n from 'i18n';
import MapUtil from 'wlp-client-mypage-lib/utils/MapUtil';
const mapUtil = new MapUtil();

class TeamDgUserTypeRenderer extends React.Component { 
    render() { 
        const { data, dataField } = this.props;

        const value = data[dataField];
        const roleLabel = mapUtil.getPermissionLabelById(value)
        return <span className="badge badge-bg-gray noselect noselect-color-5d5959">{i18n.t(roleLabel)}</span>;
    }
}


export default TeamDgUserTypeRenderer;