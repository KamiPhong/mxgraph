import React, { Fragment } from "react";
import { withTranslation } from "react-i18next";
import MapRecordDto from "wlp-client-service/dto/map/MapRecordDto";
import RoleType from "wlp-client-mypage-lib/consts/RoleType";
import LoadModalBodyEnd from "wlp-client-common/component/LoadModalBodyEnd";
import { FaCaretDown } from "react-icons/fa";
import ActionLogger from 'wlp-client-common/ActionLogger';
import { IS_TOUCH } from "wlp-client-common/utils/BrowserUtil";
import { checkElementClicked } from "wlp-client-common/utils/EventUtils";

class MapRecordActions extends React.Component {
    static DELETE = 'delete';
    static RENAME = 'rename';
    static DUPLICATE = 'duplicate';

    constructor(props) {
        super(props);
        this.state = {
            isShowActions: false,
            mouse: { x: 0, y: 0 },
            isMouseOverMenu: false
        }

        this.btnOpenMenuActionsRef = null;
        this.hideActionsThread = null;
        this.menuActionsRef = null;
    }

    setBtnOpenMenuActionsRef = (ref) => {
        this.btnOpenMenuActionsRef = ref;

        if (this.btnOpenMenuActionsRef instanceof HTMLElement && IS_TOUCH) {
            window.addEventListener('touchstart', this.handleWindowTouchStart);
        }
    }

    setMenuActionsRef = (ref) => {
        this.menuActionsRef = ref;
    }

    handleWindowTouchStart = (evt) => {
        if (this.state.isShowActions &&
            !checkElementClicked(evt, this.btnOpenMenuActionsRef) &&
            !checkElementClicked(evt, this.menuActionsRef)
        ) {
            this.hideActions();
        }
    } 

    hideActions = () => {
        //[ThuyTv] immediate hide menu actions on touch devices for better UX
        if (IS_TOUCH && this.state.isShowActions) {
            this.setState({ isShowActions: false });
            return;
        }

        if (this.hideActionsThread) {
            clearTimeout(this.hideActionsThread);
        }

        this.hideActionsThread = setTimeout(() => {
            if (this.state.isShowActions && !this.state.isMouseOverMenu) {
                this.setState({ isShowActions: false });
            }

            if (this.hideActionsThread) {
                this.hideActionsThread = null;
            }
        }, 500);
    }

    handleUserSelectedAction = (action, data) => {
        if (action === MapRecordActions.DUPLICATE) {
            this.props.duplicateButtonClickHandler(data);
        } else if (action === MapRecordActions.DELETE) {
            this.props.deleteButtonClickHandler(data);
        } else if (action === MapRecordActions.RENAME) {
            this.props.renameButtonClickHandler(data);
        }

        //[ThuyTV] manually hide menu actions on touch devices
        //on PC, the menu will be hide automatically on handleMouseLeaveMenu fn
        if (IS_TOUCH) {
            this.hideActions();
        }
    }

    showActions = (e) => {
        ActionLogger.click('User_Mypage_ListMap', 'Menu_Option_Map');
        let mousePos = { x: 0, y: 0 };
        const { pageX, pageY, clientX, clientY } = e;

        if (typeof (pageX) === 'number' && typeof (pageY) === 'number') {
            mousePos.x = pageX;
            mousePos.y = pageY;
        } else if (typeof (clientX) === 'number' && typeof (clientY) === 'number') {
            mousePos.x = clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            mousePos.y = clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }

        const disToBottom = window.innerHeight - mousePos.y;
        const popupHeight = 130;
        if (disToBottom < popupHeight) {
            mousePos.y = mousePos.y - (popupHeight - disToBottom)
        }

        if (this.state.isShowActions === false) {
            this.setState(() => ({
                isShowActions: true,
                mouse: mousePos
            }))
        }
    }

    handleMouseOverMenu = () => {
        if (this.state.isMouseOverMenu === false) {
            this.setState({ isMouseOverMenu: true });
        }
    }

    handleMouseLeaveMenu = () => {
        if (this.state.isMouseOverMenu === true) {
            this.setState({ isMouseOverMenu: false }, this.hideActions);
        }
    }

    renderRenameButton = (map) => {
        if (RoleType.LEADER === map.contractInfo.role || RoleType.ADMIN === map.contractInfo.role || RoleType.MEMBER === map.contractInfo.role) {
            return (<button
                onClick={() => this.handleUserSelectedAction(MapRecordActions.RENAME, map)}
                className="dropdown-item noselect">
                {this.props.t('mypage.maps.list.operation.rename')}
            </button>)
        }
        return null;
    }

    renderDuplicateButton = (map) => {
        if (RoleType.LEADER === map.contractInfo.role || RoleType.ADMIN === map.contractInfo.role || RoleType.MEMBER === map.contractInfo.role) {
            return (<Fragment>
                <div className="dropdown-divider"></div>
                <button
                    onClick={() => this.handleUserSelectedAction(MapRecordActions.DUPLICATE, map)}
                    className="dropdown-item noselect">
                    {this.props.t('mypage.maps.list.operation.duplicate')}
                </button>
            </Fragment>)
        }
        return null;
    }

    renderDeleteButton = (map) => {
        if (RoleType.LEADER === map.contractInfo.role || RoleType.ADMIN === map.contractInfo.role) {
            return (<Fragment>
                <div className="dropdown-divider"></div>
                <button
                    onClick={() => this.handleUserSelectedAction(MapRecordActions.DELETE, map)}
                    className="dropdown-item noselect">
                    {this.props.t('mypage.maps.list.operation.delete')}
                </button>
            </Fragment>)
        }
        return null;
    }

    componentWillUnmount() {
        if (this.btnOpenMenuActionsRef instanceof HTMLElement && IS_TOUCH) {
            window.removeEventListener('touchstart', this.handleWindowTouchStart);
        }
    }

    render() {
        const { map } = this.props;
        const { mouse, isShowActions } = this.state;

        const mapRecord = Object.assign(new MapRecordDto(), map);
        const style = { position: 'absolute', top: `${mouse.y - 5}px`, left: `${mouse.x - 135}px`, maxWidth: '140px'};
        let className = ['map-rec-menu dropdown-menu dropdown-menu-right dropdown-menu-bg-black d-block'];

        return (<Fragment>
            <button
                ref={this.setBtnOpenMenuActionsRef}
                onClick={this.showActions}
                type="button"
                className="btn bg-light btn-skew-l dropdown-toggle pr-0 pl-3 cursor-df">
                <span className="btn-skew-text"><i className="fas fa-bars noselect"></i></span>
                <FaCaretDown size={14} />
            </button>
            {isShowActions &&
                <LoadModalBodyEnd>
                    <div
                        ref={this.setMenuActionsRef}
                        style={style}
                        onMouseOver={this.handleMouseOverMenu}
                        onMouseLeave={this.handleMouseLeaveMenu}
                        className={className.join(' ')}>
                        {this.renderRenameButton(mapRecord)}
                        {this.renderDuplicateButton(mapRecord)}
                        {this.renderDeleteButton(mapRecord)}
                    </div>
                </LoadModalBodyEnd>
            }
        </Fragment>)
    }
}

export default withTranslation()(MapRecordActions);