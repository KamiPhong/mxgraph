import React, { Fragment } from 'react';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import { memberDgAdminRadioRendererEvent } from 'wlp-client-mypage-lib/events/MemberDgAdminRadioRendererEvent';
import ActionLogger from 'wlp-client-common/ActionLogger';

class MemberDgAdminRadioRenderer extends React.Component { 
    state = {
        selectedOption: null,
        selectedCheckbox: null,
    };

    onChangeHandler = (e) => {
        ActionLogger.click('User_Mypage_TeamSetting_[メンバーリスト]', `Checked_Admin`);
        const { data } = this.props;
        const target = e.target;
         if (data == null){
            return;
        }
        let value = false;
        if (data.leaderFlg) {
            value = false;
        } else { 
            value = target.checked;
        }
        memberDgAdminRadioRendererEvent.fireEvent(memberDgAdminRadioRendererEvent.ADMIN_SELECT, data.userId, value);

    }

    render() { 
        const { data } = this.props;

        let enabled = false;
        if (data.leaderFlg){
            enabled = false;
        } else if (data.invitingFlg){
            enabled = false;
        } else if (RoleType.ADMIN === data.userType
            || RoleType.LEADER === data.userType
            || RoleType.MEMBER === data.userType ){
            enabled = true;

        } else {
            enabled = false;
        }
        
        return (
            <Fragment>
                <label className="form-check-label font-size-12 checkbox-facilitator"
                       htmlFor={`gridCheck-${data.userId}`}>
                    <input className={`resize-cb-rd reset-button-radio`} type="checkbox"
                           disabled={!enabled}
                           checked={data.adminFlg}
                           onChange={ this.onChangeHandler}
                           id={`gridCheck-${data.userId}`}
                    />
                    <span className={`checkmark ${!enabled ? 'disabled-checkbox' : ''}`}></span>
                </label>
            </Fragment>
        );
    }
}


export default MemberDgAdminRadioRenderer;