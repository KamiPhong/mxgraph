import React, {Component, Fragment} from 'react';
import {withTranslation} from 'react-i18next';
import GetMemberListConfigDto from 'wlp-client-service/dto/user/GetMemberListConfigDto';
import MemberDgDto from 'wlp-client-service/dto/team/MemberDgDto';
import DeleteMemberConfigDto from 'wlp-client-service/dto/user/DeleteMemberConfigDto';
import ChangeMemberRoleConfigDto from 'wlp-client-service/dto/user/ChangeMemberRoleConfigDto';
import TeamService from 'wlp-client-service/service/TeamService';
import DeleteMemberAlertModal from 'wlp-client-mypage-lib/components/modals/DeleteMemberAlertModal';
import ChangeMemberAlertModal from 'wlp-client-mypage-lib/components/modals/ChangeMemberAlertModal';
import ContractType from 'wlp-client-service/consts/ContractType';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import GetMemberMapListConfigDto from 'wlp-client-service/dto/team/GetMemberMapListConfigDto';
import MemberDto from 'wlp-client-service/dto/team/MemberDto';
import grayMapIcon from 'wlp-client-common/images/grayMapIcon@4x.png';
import TableResize from 'wlp-client-common/component/TableResize/TableResize'
import { tableResizeEvent } from 'wlp-client-common/events/TableResizeEvent';
import { dynamicSort, sortArrayItems } from 'wlp-client-common/utils/ArrayUtil';
import MemberDgUserRenderer from '../renderer/MemberDgUserRenderer';
import MemberDgMapRenderer from '../renderer/MemberDgMapRenderer';

import i18n from "i18n";
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import MemberDgLeaderRadioRenderer from '../renderer/MemberDgLeaderRadioRenderer';
import MemberDgAdminRadioRenderer from '../renderer/MemberDgAdminRadioRenderer';
import MemberDgDeleteBtnRenderer from '../renderer/MemberDgDeleteBtnRenderer';
import { memberDgLeaderRadioRendererEvent } from 'wlp-client-mypage-lib/events/MemberDgLeaderRadioRendererEvent';
import { memberDgAdminRadioRendererEvent } from 'wlp-client-mypage-lib/events/MemberDgAdminRadioRendererEvent';
import RoleType from 'wlp-client-mypage-lib/consts/RoleType';
import { memberDgItemRendererEvent } from 'wlp-client-mypage-lib/events/MemberDgItemRendererEvent';
import { THEAD_HEIGHT } from 'wlp-client-common/consts/TableConsts';
import AuthService from 'wlp-client-service/service/AuthService';

const teamService = new TeamService();
const localStorageUtil = new LocalStorageUtils();
const authService = new AuthService();

const ROLE = {
    LEADER: 1,
    ADMIN: 2,
    MEMBER: 3,
    PARTNER: 4,
    OWNER: 5,
};

class MemberListPanel extends Component {
    tableColumns = {
        userName: {
            dataField: "userName",
            headerText: i18n.t('mypage.teamSettings.member.header.user'),
            itemRenderer: <MemberDgUserRenderer />,
        },
        maps: {
            dataField: "maps",
            headerText: i18n.t('mypage.teamSettings.member.header.mapNum'),
            itemRenderer: <MemberDgMapRenderer />,
            width:120
        },
        leaderFlg: {
            dataField: "leaderFlg",
            headerText: i18n.t('mypage.teamSettings.member.header.leader'),
            itemRenderer: <MemberDgLeaderRadioRenderer />,
            width:80
            
        },
        adminFlg: {
            dataField: "adminFlg",
            headerText: i18n.t('mypage.teamSettings.member.header.admin'),
            itemRenderer: <MemberDgAdminRadioRenderer />,
            width:80
        },
        delete: {
            headerText: '',
            itemRenderer: <MemberDgDeleteBtnRenderer />,
            width: 120,
            minWidth:500,
            className: "text-center vertical-align-middle td-p-17",
            sortable: false
        }
    }

    groupTimer = null;

    successMessageGroupTimer = 5000;
    
    state = {
        selectedColumnIndex: null,
        dataTableList: null,
        dataDelete: null,
        isShowDeleteMemberAlert: false,
        isShowChangeMemberAlert: false,
        contractId: null,
        teamId: null,
        memberList: this.props.memberList || [],
        selectedOption: null,
        selectedCheckbox: null,
        showMessageSuccess: false,
        memberListDefault: [],
        checkSelectedOption: false,
        isSucceed: false,
        isShowListMap: false,
        mapNameList: [],
        top: 0,
        left: 0,
        currentSortKey : '',
        currentSortOrder : '',
        maxHeight: null
    }

    constructor(props) {
        super(props);
        const logonInfo = localStorageUtil.getUserSession();
        this.accessToken = logonInfo.accessToken;
        this.messageRef = React.createRef();

        this.memberDgRef = Element => {
            this.memberDg = Element;
        }
    };

    componentDidMount() {
        const {teamData} = this.props;
        if (teamData) {
            this.callGetMemberList(teamData);
        }
        document.addEventListener("mousedown", this.handleClickOutsideDivPopup);
        document.addEventListener("wheel", this.handleClickOutsideDivPopup);
        memberDgLeaderRadioRendererEvent.addListener(memberDgLeaderRadioRendererEvent.CHECK_BOX_ON_CHANGE, this.memberDgLeaderSelectHandler);
        memberDgAdminRadioRendererEvent.addListener(memberDgAdminRadioRendererEvent.ADMIN_SELECT, this.memberDgAdminSelectHandler);
        memberDgItemRendererEvent.addListener(memberDgItemRendererEvent.MAP_CLICK, this.memberDgMapClickHandler);
        memberDgItemRendererEvent.addListener(memberDgItemRendererEvent.DELETE_CLICK, this.memberDgDeleteClickHandler);
        tableResizeEvent.addListener(tableResizeEvent.TABLE_HEADER_CLICK, this.doSortMemberList);

        this.calculateTableSize();
    }

    initWidthTableColumns = () => {
        const { widthTableColumns } = this.props;
        let tableColumns = {...this.tableColumns};
        if (Array.isArray(widthTableColumns) && widthTableColumns.length) {
            for (let i = 0; i < widthTableColumns.length; i++) {
                tableColumns[widthTableColumns[i].name] = {...tableColumns[widthTableColumns[i].name], width: widthTableColumns[i].width}
            }
        }

        this.tableColumns = tableColumns;
    }


    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutsideDivPopup);
        memberDgLeaderRadioRendererEvent.removeListener(memberDgLeaderRadioRendererEvent.CHECK_BOX_ON_CHANGE);
        memberDgAdminRadioRendererEvent.removeListener(memberDgAdminRadioRendererEvent.ADMIN_SELECT);
        memberDgItemRendererEvent.removeListener(memberDgItemRendererEvent.MAP_CLICK);
        clearTimeout(this.groupTimer);
    }

    componentDidUpdate(prevProps, prevState) {
        if (!Object.is(this.props.teamData, prevProps.teamData)) {
            if (
                this.props.teamData &&
                typeof this.props.teamData === 'object' &&
                Object.values(this.props.teamData).length > 0
            ) {
                this.callGetMemberList(this.props.teamData);
            }
        }
    }

    callGetMemberList = async (teamData) => {
        if (teamData) {
            let requestDto = new GetMemberListConfigDto();
            const localeCode = i18n.language;
            requestDto = {
                ...requestDto,
                "accessToken": this.accessToken,
                "contractId": teamData.contractId,
                "contractType": ContractType.personal,
                "currentState": CurrentState.CONTROL_PANEL,
                "localeCode": localeCode,
                "teamId": teamData.teamId
            };

            this.setState({contractId: teamData.contractId, teamId: teamData.teamId});

            const result = await teamService.getMemberList(requestDto);

            if (result && result.isSucceed) {
                this.getMemberListResultHandler(result);
            }
        }
    };

    // MemberListPanel.as [179]
    getMemberListResultHandler = (result) => {
        let {selectedOption, selectedCheckbox} = this.state;
        let newArray = [];

        result.memberList.forEach( (item) => {
            if (item.userType === ROLE.LEADER) {
                selectedOption = item.userId;
            }
            if (item.userType === ROLE.ADMIN) {
                selectedCheckbox = item.userId;
            }
            let memberDgDto = new MemberDgDto();

            memberDgDto = {
                ...memberDgDto,
                userId: item.userId,
                userType: item.userType,
                invitingFlg: item.invitingFlg,
                userName: item.userName,
                userImgUrl: item.userImgUrl,
                email: item.email,
                maps: item.maps,
                leaderFlg: (item.userType === ROLE.LEADER),
                adminFlg: (item.userType === ROLE.ADMIN)
            };
            newArray.push(memberDgDto);
        });

        this.props.setDataMemberListPanel(newArray);

        this.setState({
            memberList: newArray,
            selectedOption: selectedOption,
            selectedCheckbox: selectedCheckbox,
            memberListDefault: result.memberList,
        });
    };

    showModalListMap = (mapNameList) => {
        this.setState({
            mapNameList: mapNameList,
            isShowListMap: true
        });
    };

    hideModalListMap = () => {
        this.setState({isShowListMap: false});
        memberDgItemRendererEvent.fireEvent(memberDgItemRendererEvent.MAP_CLOSE);
    };

    getMemberMapListResultHandler = (result) => {
        this.showModalListMap(result.mapNameList);

    };

    setWrapperRefPopup = (node) => {
        this.wrapperRefPopup = node;
        return this.wrapperRefPopup;
    };

    handleClickOutsideDivPopup = (event) => {
        if (this.wrapperRefPopup && !this.wrapperRefPopup.contains(event.target)) {
            this.hideModalListMap();
        }
    };

    setShowChangeAlert = () => {
        this.setState({
            isShowChangeMemberAlert: false,
        });
    };

     // MemberListPanel.as [209]
    deleteMemberResultHandler = () => {
        const {teamData} = this.props;

        this.setState({
            isShowDeleteMemberAlert: false,
            dataDelete: null
        }, () => {
            this.callGetMemberList(teamData);
        });
    }

    // MemberListPanel.as [112]
    memberDgLeaderSelectHandler = (userId, value) => {
         this.setState(state => { 
            let { memberList } = state;
            memberList.forEach((member, index) => { 
                if (member.userId === userId) {
                    member.leaderFlg = value;
                } else { 
                    member.leaderFlg = false;
                }
            });
            return memberList;
        });

    };

    // MemberListPanel.as [124]
    memberDgAdminSelectHandler = (userId, value) => {
        this.setState(state => { 
            let { memberList } = state;
            memberList.forEach((member, index) => { 
                if (member.userId === userId) {
                    member.adminFlg = value;
                } else { 
                    member.adminFlg = false;
                }
            });
            return memberList;
        });
    };

    // MemberListPanel.as [222]
    callChangeMemberRole = async (data) => {
        const {contractId, teamId} = this.state;
        let requestDto = new ChangeMemberRoleConfigDto();
        const localeCode = i18n.language;
        let leaderMember = [];
        let adminMember = [];

        const dataLength = data.length;
        for (let i = 0; i < dataLength; i++) {
            if (data[i].leaderFlg) {
                const item = this.memberDgDtoToMemberDto(data[i]);
                leaderMember.push(item);
            }

            if (data[i].adminFlg) {
                const item = this.memberDgDtoToMemberDto(data[i]);
                adminMember.push(item);
            }
        }

        requestDto = {
            ...requestDto,
            "accessToken": this.accessToken,
            "contractId": contractId,
            "contractType": ContractType.personal,
            "currentState": CurrentState.CONTROL_PANEL,
            "localeCode": localeCode,
            "teamId": teamId,
            "leaderMember": leaderMember[0] ? leaderMember[0] : null,
            "adminMember": adminMember[0] ? adminMember[0] : null,

        };

        const result = await teamService.changeMemberRole(requestDto);

        if (result && result.isSucceed) {
            this.setState({
                isShowChangeMemberAlert: false
            });
            this.changeMemberRoleResultHandler(data);
        }
    };

    // MemberListPanel.as [160]
    cancelBtnClickHandler = () => {
        const {teamData} = this.props;
        if (teamData) {
            this.callGetMemberList(teamData);
        }
    };

    // MemberListPanel.as [146]
    memberDgMapClickHandler = (memberDgDto, e) => {
        if (memberDgDto) {
            this.callGetMemberMapList(memberDgDto, e);
        }
    }

    // MemberListPanel.as [257]
    callGetMemberMapList = async (dataItem, e) => {
        try {
            const target = e.currentTarget

            let rect = e.currentTarget.getBoundingClientRect();
            const {contractId, memberListDefault} = this.state;
            let requestDto = new GetMemberMapListConfigDto();
            let targetMember = new MemberDto();

            for (const memberList of memberListDefault) {
                if (memberList.userId === dataItem.userId) {
                    targetMember = memberList;
                }
            }

            requestDto = {
                ...requestDto,
                "contractId": contractId,
                "targetMember": targetMember
            };
            const result = await teamService.getMemberMapList(requestDto);
            if (result && result.isSucceed) {
                let lengthOfMap = result.mapNameList.length;
                let calOfMapList = this.calculatorHeightOfListMap(lengthOfMap);
                let dataScreen = this.setTopAndLeftOfScreen(rect, calOfMapList, lengthOfMap, target);
                
                this.setState({top: dataScreen[0], left: dataScreen[1]});
                this.getMemberMapListResultHandler(result);
            }

        } catch (e) {

        }
    };

    setTopAndLeftOfScreen = (rect, cal, map, target) => {
        let widthOfPopup = 444; //width default of popup
        let calMap = 0;
        if(map === 1){
            calMap = widthOfPopup/2; //width default of popup when has 1 map
        }
        let height = window.innerHeight;
        let width = window.innerWidth;
        let heightOfRow = 35; //height default of each row in popup map list
        let offsetTop = 96; // offset from table to top (35 + 57)
        offsetTop += 22;
        let heightOfPopUp = 187 - (cal * heightOfRow) ;  //  padding & height of Map list pop up
        let heightOfButton = 58; //  width default padding bottom
        let widthOfLeftMenu = width - 40; //width default of left menu + padding
        let windowOffset = 250; // offset from table to bottom and left of windows

        let top = rect.top + rect.height - offsetTop;

        if (height - rect.bottom < windowOffset) {
            top = rect.top + rect.height - offsetTop - heightOfPopUp - heightOfButton;
        }
        let left = rect.left - calMap - rect.width;
        if (map > 1) {
            calMap = widthOfPopup;
            left = rect.left + rect.width - calMap;
        }
        if (width - rect.left < windowOffset && map === 1) {
            left = widthOfLeftMenu - (calMap + rect.width) - calMap;
        }
        if (width - (rect.left + widthOfPopup) < 250 && map > 1) {
            left = widthOfLeftMenu - (calMap + rect.width) - (calMap / 2);
        }
        
        return [top, left];
    };

    changeMemberRoleResultHandler(data) {
        if (this.checkIsAdminOrLeaderInList(data) !== true) {
            authService.checkLoggedOn().then(userInfo => {
                localStorageUtil.updateUserSession(userInfo);
                window.location.reload();
            });
        } else {
            this.callGetMemberList(this.props.teamData);
            this.setState({
                isShowChangeMemberAlert: false,
                isSucceed: true
            });
            this.calculateTableSize();
            this.groupTimer = setTimeout(() => {
                this.setState({isSucceed: false})
                this.calculateTableSize();
            }, this.successMessageGroupTimer);
        }

    }

    /**
     * onClick show confirm modal
     */
    handleClickSubmit = () => {
        this.setState({
            isShowChangeMemberAlert: true,
        });
    };

    // MemberListPanel.as [153]
    saveChangesBtnClickHandler = (evt) => {
        evt.preventDefault();
        const {memberList, selectedOption, selectedCheckbox} = this.state;
        let filteredDataSource = memberList.map((item) => {
            if (item.userId === selectedOption) {
                item.userType = ROLE.LEADER;
            } else if (item.userId === selectedCheckbox) {
                item.userType = ROLE.ADMIN;
            }
            else if (item.userId !== selectedOption && item.userType === ROLE.LEADER) {
                item.userType = ROLE.MEMBER;
            }
            else if (item.userId !== selectedCheckbox && item.userType === ROLE.ADMIN) {
                item.userType = ROLE.MEMBER;
            }
            return item;

        });
        this.callChangeMemberRole(filteredDataSource);
    };

    // MemberListPanel.as [136]
    memberDgDeleteClickHandler = (selectedMemberDto) => {
        this.setState({
            isShowDeleteMemberAlert: true,
            dataDelete: selectedMemberDto
        });
    }

    /**
     * confirm delete
     */
    setShowDeleteAlert = () => {
        this.setState({
            isShowDeleteMemberAlert: false,
        });
    };

    // MemberListPanel.as [196]
    callDeleteMember = async () => {
        const { contractId, teamId, dataDelete } = this.state;
        
        let requestDto = new DeleteMemberConfigDto();
        const localeCode = i18n.language;

        requestDto = {
            ...requestDto,
            "accessToken": this.accessToken,
            "contractId": contractId,
            "contractType": ContractType.personal,
            "currentState": CurrentState.CONTROL_PANEL,
            "localeCode": localeCode,
            "deleteMember": this.memberDgDtoToMemberDto(dataDelete),
            "teamId": teamId,
        };

        const result = await teamService.deleteMember(requestDto);

        if (result && result.isSucceed) {
            this.deleteMemberResultHandler();
        }
    };

    // MemberListPanel.as [313]
    memberDgDtoToMemberDto = (src) => {
        let dest = new MemberDto();
        dest.userId = src.userId;
        dest.userType = src.userType;
        dest.invitingFlg = src.invitingFlg;
        dest.userName = src.userName;
        dest.userImgUrl = src.userImgUrl;
        dest.email = src.email;
        dest.maps = src.maps;

        if (src.leaderFlg) {
            dest.userType = RoleType.LEADER;
        } else if (src.adminFlg) {
            dest.userType = RoleType.ADMIN;
        } else {
            dest.userType = src.userType;
        }
        return dest;
    }

    calculatorHeightOfListMap = (maps) =>{
        if(maps >= 9){
            return 0;
        }
        return 5 - (Math.round(maps/2));
    };

    doSortMemberList = (event) => {
          if (event.index < 0 || event.target.nodeName !== 'TH') {
            return;
        }
        
        let { currentSortKey, currentSortDescending, memberList } = this.state;
        const columnInfo = event.column;
        const softIndex = columnInfo.softIndex;

        if ( !memberList || memberList.length < 1) {
            return;
        }
        
        currentSortDescending = (parseInt(softIndex) === parseInt(currentSortKey)) ? !currentSortDescending : true;

        let params = {
            currentSortDescending: currentSortDescending,
            currentSortKey: softIndex,
            currentSortOrder: currentSortDescending ? 'DESC' : 'ASC'
        }
        let sortField = 'userName';

        let sortConfig = {
            key: columnInfo.name,
            direction: currentSortDescending ? 'DESC' : 'ASC',
            foreignKey: sortField,
            hasColumnID: false
        }

        let dataSort = sortArrayItems(memberList, sortConfig);

        // Sort member Leader
        if (softIndex === 3 && this.memberDg !== null) {
            this.memberDg.clickOrder = true;
        }

        this.setState({
            ...params,
            memberList: dataSort,
        });
    }

    checkIsAdminOrLeaderInList = (memberList) => {
        const userInfo = localStorageUtil.getUserSession();
        const user = memberList.filter(u => ( u.userId === userInfo.sessionDto.userId ) && (u.leaderFlg || u.adminFlg));
        return user.length > 0;
    }
    
    onColumnResized = (widthTableColumns) => { 
        this.widthTableColumns = widthTableColumns;
        if (typeof this.props.setWidthTableColumns === "function") {
            this.props.setWidthTableColumns(widthTableColumns);
        }
    }
    
    /**
     * calculate table dimension and update state on window resize 
     */
    updateTableSizeOnWindowResizeHanlder=()=>{
        this.calculateTableSize();
    }

    /**
     * calculate table dimension and update state 
     */
    calculateTableSize=()=>{
        const tableContainer = document.querySelector('.card.fix-padding-map-list');
        let tableMaxHeight = 0;
        let tableMaxWidth = 0;
        if (tableContainer instanceof HTMLElement && this.messageRef instanceof HTMLElement) {
            if(window.innerHeight >= 350) {
                const tableBound = tableContainer.getBoundingClientRect();
                const styles = window.getComputedStyle(tableContainer);
                tableMaxHeight = tableContainer.offsetHeight - tableBound.top - parseInt(styles.paddingTop) - THEAD_HEIGHT - (this.messageRef.offsetHeight * 2 + 15);
                tableMaxWidth = tableContainer.offsetWidth - parseFloat(styles.paddingLeft) - parseFloat(styles.paddingRight);
                this.setState({tableMaxWidth,tableMaxHeight})
            }
        }
    }

    render() {

        const {t} = this.props;
        let {
            isSucceed,
            isShowDeleteMemberAlert,
            isShowChangeMemberAlert,
            isShowListMap,
            mapNameList, top, left,

            memberList,
            currentSortKey,
            currentSortOrder,
            tableMaxWidth,
            tableMaxHeight

        } = this.state;

        const classNamesListMap = ['dropdown-menu dropdown-menu-right dropdown-menu-resize'];
        const classNameColumn = [''];

        if(mapNameList.length > 1){
            classNameColumn.push('col-6');
        }else{
            classNamesListMap.push('dropdown-menu-resize-fix');
            classNameColumn.push('col-12');
        }
        return (<Fragment>
                <div  id="personal-tabs3" className="member-map-list">
                    <div className="personal-tabs-status personal-tabs-item">
                        <h2 ref={r => this.messageRef = r} className={'save-message presonal-title-tabs save-message-teamInformation noselect' + (isSucceed ? " d-block" : " d-none")}>
                            {t('mypage.personalSettings.user.saveMessage')}
                    </h2>
                    
                    <TableResize
                        ref={this.memberDgRef}
                        name="memberDg"
                        columns={this.tableColumns}
                        className="table-resizable"
                        dataProvider={memberList}
                        sortOrder={currentSortOrder}
                        sortIndex={currentSortKey}
                        rowHeight={60}
                        maxHeight={tableMaxHeight}
                        maxWidth={tableMaxWidth}
                        onWindowResizeHandler={this.updateTableSizeOnWindowResizeHanlder}
                        isShowListMap={isShowListMap}
                    />


                    <hr className="border-style-dotted mt-4" />
                    
                    <div className="mt-20px">
                        <button
                            onClick={() => this.cancelBtnClickHandler()}
                            type="button"
                            className="btn btn-whitefl w-100px reset-border noselect noselect-color-464646">
                            {t('global.cancel')}
                        </button>
                        <button
                            type="button"
                            onClick={this.handleClickSubmit}
                            className="btn btn-dark ml-10 reset-border noselect noselect-color-white">
                            {t('mypage.personalSettings.user.save')}
                        </button>
                    </div>

                        <div>
                            {isShowDeleteMemberAlert &&
                            <DeleteMemberAlertModal cancelHandler={() => this.setShowDeleteAlert()}
                                                    callApiDeleteMember={this.callDeleteMember}/>}
                            {isShowChangeMemberAlert &&
                            <ChangeMemberAlertModal cancelHandler={() => this.setShowChangeAlert()}
                                                    callChange={this.saveChangesBtnClickHandler}/>}
                        </div>
                        {isShowListMap && (
                            <div ref={this.setWrapperRefPopup}
                                 className={classNamesListMap.join(' ')}
                                 style={{top: top, left: left}}>
                                <div className="d-flex w-dropdown-item-maps pl-12px pr-7px">
                                    <div className="row no-gutters w-100">
                                        {mapNameList.map((map, index) => {
                                            return (
                                                <div key={index} className={classNameColumn.join(' ')}>
                                                    <div className="p-3px">
                                                        <div className="border rounded p-3px dropdown-item-maps">
                                                            <div className="d-flex align-items-center">
                                                                <img alt="" className="mr-2 icon-gray-map noselect"
                                                                     src={grayMapIcon}/>
                                                                <div className="w-100 text-center">
                                                                    <p className="mb-0 font-weight-normal text-member-map noselect">
                                                                        {map}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        )
                        }
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default withTranslation()(MemberListPanel);