import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import LocaleCode from "wlp-client-service/consts/LocaleCode";
import i18n from "i18n";

class AccountStatusInfo extends Component {
    constructor() {
        super();
        this.state = {
            currenLang: LocaleCode.en_US
        };
    };

    checkCurrentLanguageSettings = () => {
        const language = i18n.language;
        this.setState({currenLang: language});
    }

    UNSAFE_componentWillReceiveProps({ textMax, textReal }) {
        if (
            textMax && textReal
        ) {
            this.checkCurrentLanguageSettings();
        }
    }

    componentDidMount() {
        this.checkCurrentLanguageSettings();
    }

    render() {
        const { 
            t,
            valueMax,
            valueReal,
            textMax,
            textReal,
            diskFlag
        } = this.props;
        const { currenLang } = this.state;
        const unitJP = !diskFlag ? t('mypage.teamSettings.accountStatus.usage.unit.person') : '';

        return (<Fragment>
            {currenLang === LocaleCode.en_US ? (
                <div>
                    <small className="text-warning-gb font-size-10 noselect noselect-color-b9a200">
                        {textMax} <span className="font-weight-bold noselect-color-b9a200">{valueMax}</span>
                    </small>
                    <small className="text-warning-gb font-size-10 ml-2 noselect noselect-color-b9a200">
                        {textReal} <span className="font-weight-bold noselect noselect-color-b9a200">{valueReal}</span>
                    </small>
                </div>
            ) : (
                <div>
                    <small className="text-warning-gb font-size-10 noselect noselect-color-b9a200">
                        <span className="font-weight-bold noselect noselect-color-b9a200">{valueMax+unitJP}</span>&nbsp;&nbsp;{textMax}
                    </small>&nbsp;&nbsp;
                    <small className="text-warning-gb font-size-10 noselect noselect-color-b9a200">
                        <span className="font-weight-bold noselect noselect-color-b9a200">{valueReal+unitJP}</span>&nbsp;&nbsp;{textReal}
                    </small>
                </div>
            )}
        </Fragment>);
    }
}

export default withTranslation()(AccountStatusInfo);