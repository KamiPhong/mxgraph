import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import TeamService from 'wlp-client-service/service/TeamService';
import ChangeTeamInfoConfigDto from 'wlp-client-service/dto/user/ChangeTeamInfoConfigDto';
import GetTeamInfoConfigDto from 'wlp-client-service/dto/team/GetTeamInfoConfigDto';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import ValidateErrorMessage from 'wlp-client-common/component/message/ValidateErrorMessage';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import GetTeamInfoDto from 'wlp-client-service/dto/team/GetTeamInfoDto';
import ChangeTeamInfoDto from 'wlp-client-service/dto/team/ChangeTeamInfoDto';
import ActionLogger from 'wlp-client-common/ActionLogger';


const teamService = new TeamService();
const wlpDateUtil = new WlpDateUtil();

class TeamInfoPanel extends Component {
    constructor(props) {
        super(props);
        this.rollbackTeamData = {};
        this.state = {
            isSucceed: false,
            teamInfo: {},
            isShowErrorTeamName: false,
            touched: {
                teamName: false,
            },
            showShadowBoder: true,
            isFocusTeamNameInput: false,
            isFocusAboutTeamTextArea: false
        };
    };

    callGetTeamInfo = async (teamData) => {
        if (teamData) {
            let requestDto = new GetTeamInfoConfigDto();
            requestDto.teamId = teamData.teamId;
            requestDto.contractId = teamData.contractId;
            const result = await teamService.getTeamInfo(requestDto);

            if (result && result.isSucceed) {
                this.getTeamInfoDataResultHandler(result);
            }
        }

    };

    getTeamInfoDataResultHandler = (result = new GetTeamInfoDto()) => {
        this.rollbackTeamData = result;
        this.setState({
            teamInfo: result,
            isShowErrorTeamName: false,
        });
    };

    validateTeamInfo = () => {
        const { teamInfo } = this.state;
        if(!teamInfo.teamName.trim()){
            this.refs.inputTeamName.focus();
            return false;
        }
        return true;
    }


    callChangeTeamInfo = async () => {
        ActionLogger.click('User_Mypage_TeamSetting_[チーム情報]', 'Button_Save_Change');
        const validateTeamInfo = this.validateTeamInfo();

        if(!validateTeamInfo){ return };

        const { teamInfo } = this.state;
        const { teamData } = this.props;
        let requestDto = new ChangeTeamInfoConfigDto();
        requestDto.teamName = teamInfo.teamName;
        requestDto.companyName = teamInfo.companyName;
        requestDto.description = teamInfo.description;
        requestDto.contractId = teamData.contractId;
        requestDto.teamId = teamData.teamId;

        const result = await teamService.changeTeamInfo(requestDto);
        this.changeTeamInfoResultHandler(result, requestDto);
    };

    changeTeamInfoResultHandler = (result = new ChangeTeamInfoDto(), requestDto) => {
        const { isSucceed } = result;
        const { setTeamData, teamData } = this.props;
        this.setState({ isSucceed });
        if (isSucceed) {
            this.rollbackTeamData = {
                ...this.rollbackTeamData,
                teamName: requestDto.teamName,
                companyName: requestDto.companyName,
                description: requestDto.description
            }

            setTeamData({ ...teamData, teamName: requestDto.teamName });
        } else {
            showWarningDialog(result.message)
        }
    };

    handleChangeDescription = (event) => {
        ActionLogger.typing('User_Mypage_TeamSetting_[チーム情報]', 'Input_Descryption');
        this.setState({
            teamInfo: { ...this.state.teamInfo, description: event.target.value }
        });
    };

    setCompanyName = (event) => {
        ActionLogger.typing('User_Mypage_TeamSetting_[チーム情報]', 'Input_Company_Name');
        this.setState({
            teamInfo: { ...this.state.teamInfo, companyName: event.target.value }
        });

    };

    setTeamName = (event) => {
        ActionLogger.typing('User_Mypage_TeamSetting_[チーム情報]', 'Input_Team_Name');
        this.setState({
            teamInfo: { ...this.state.teamInfo, teamName: event.target.value }
        });

    };

    removePlaceholderDetail = (e) => {
        this.setState({
            showShadowBoder: false,
            isFocusAboutTeamTextArea: true
        })
    };

    decriptionDetailTextarea_handleBlur = (event) => {
        this.setState({
            teamInfo: { ...this.state.teamInfo, description: event.target.value },
            isFocusAboutTeamTextArea: false
        });
        if(event.target.value === ''){
            this.setState({
                showShadowBoder: true,
                isFocusAboutTeamTextArea: false
            });
        }
    }

    handleRollbackData = () => {
        ActionLogger.click('User_Mypage_TeamSetting_[チーム情報]', 'Button_Cancel');
        this.setState({
            teamInfo: this.rollbackTeamData,
            isShowErrorTeamName: false
        });
    }

    componentDidMount() {
        this.callGetTeamInfo(this.props.teamData);
    }

    handleBlur = field => evt => {
        this.setState({
            touched: { ...this.state.touched, [field]: true }
        });

        if (field === 'teamName') {
            if (evt.target.value === '') {
                this.setState({ isShowErrorTeamName: true,
                    isFocusTeamNameInput: false });
            } else {
                this.setState({
                    isShowErrorTeamName: false,
                    touched: { ...this.state.touched, [field]: false },
                    isFocusTeamNameInput: false
                });
            }
        }
    };

    handleFocusTeamName = () => {
        this.setState({
            isFocusTeamNameInput: true
        })
    }

    componentDidUpdate = ({teamData}) => {
        let _this = this;
        if (this.state.isSucceed) {
            setTimeout(function () {
                _this.setState({ isSucceed: false })
            }, 5000)
        }

        if(!Object.is(teamData, this.props.teamData)){
            this.callGetTeamInfo(this.props.teamData);
        }
    };

    render() {
        const { teamInfo, isSucceed, isShowErrorTeamName, showShadowBoder, isFocusTeamNameInput, isFocusAboutTeamTextArea } = this.state;
        const { t } = this.props;
        return (<Fragment>
            <div className="personal-tabs-information personal-tabs-item">
                <div className="d-flex">
                    <div className="flex-1 personal-form w-100">
                        <div className="personal-form-el">
                            <div className="form-group mb-0 text-muted-status font-size-13">
                                <label htmlFor="createDate"
                                    className="font-weight-bold text-muted-status mb-3px noselect noselect-color-aaaaaa">{t("mypage.teamSettings.team.createDate")} :</label>
                                <span className="span-team-created noselect noselect-color-aaaaaa"> {teamInfo.createdIn ? wlpDateUtil.formatDate(teamInfo.createdIn) : ""}</span>
                            </div>
                            <hr className="border-style-dotted" />
                            <h2
                                className={'save-message presonal-title-tabs save-message-teamInformation noselect noselect-color-aaaaaa' + (isSucceed ? " d-block" : " d-none")}>
                                {t('mypage.personalSettings.user.saveMessage')}
                            </h2>
                            <div className="w-360px">
                                <div className="form-group">
                                    <label htmlFor="companyName"
                                        className="font-weight-bold text-muted-status font-size-13 mb-3px noselect noselect-color-aaaaaa">{t("mypage.teamSettings.team.companyName")} :</label>
                                    <input type="input"
                                        className="form-control form-control-shadow form-control-company-name noselect"
                                        name="companyName"
                                        ref="inputCompanyName"
                                        aria-describedby="emailHelp"
                                        placeholder=""
                                        readOnly
                                        onChange={(e) => this.setCompanyName(e)}
                                        value={teamInfo.companyName ? teamInfo.companyName : ""}
                                    />
                                </div>
                                <div className={isShowErrorTeamName ? "form-group fix-mr-bottom" : "form-group"}>
                                    <label htmlFor="infoTeamName"
                                        className="font-weight-bold text-muted-status font-size-13 mb-3px noselect noselect-color-aaaaaa">{t("mypage.teamSettings.team.teamName")} :</label>
                                    <input type="input"
                                        className={'form-control form-control-shadow ' + (isShowErrorTeamName ? " is-invalid" : "") + (isFocusTeamNameInput ? "" : " noselect")}
                                        placeholder=""
                                        name="infoTeamName"
                                        ref="inputTeamName"
                                        onBlur={this.handleBlur("teamName")}
                                        onFocus={this.handleFocusTeamName}
                                        onChange={(e) => this.setTeamName(e)}
                                        value={teamInfo.teamName ? teamInfo.teamName : ""} />
                                </div>
                                {isShowErrorTeamName &&
                                    <ValidateErrorMessage classAdd={'add-position position-teamName'}
                                        message={t('message.team.e0002')} />}

                            </div>
                            <div className="w-500px w-form">
                                <div className="form-group">
                                    <label htmlFor="teamDescription"
                                        className="font-weight-bold text-muted-status font-size-13 mb-3px noselect noselect-color-aaaaaa">{t("mypage.teamSettings.team.about.title")} : </label>
                                    <div className={(teamInfo.description === null || teamInfo.description === '') && showShadowBoder ? 'form-control-textarea-shadow' : ''}>
                                        <textarea name="teamDescription" cols="30" rows="4" maxLength="2048"
                                            placeholder={t('mypage.teamSettings.team.about.prompt')}
                                            onChange={(e) => this.handleChangeDescription(e)} value={teamInfo.description ? teamInfo.description : ""}
                                            onFocus={(e) => this.removePlaceholderDetail(e)}
                                            onBlur={this.decriptionDetailTextarea_handleBlur}
                                            className={`description-teaminfo form-control form-control-textarea-shadow textarea-team-info no-resize form-control-textarea-default placeholder-italic
                                                ${teamInfo.description && teamInfo.description !== '' ? ' bg-textarea-isvalid' : ''} ${isFocusAboutTeamTextArea ? "" : " noselect"}`} />
                                    </div>
                                </div>
                            </div>
                            <hr className="border-style-dotted mt-4" />
                            <button onClick={this.handleRollbackData}
                                type="button"
                                className="btn btn-whitefl w-100px reset-border noselect noselect-color-464646">
                                {t('global.cancel')}
                            </button>
                            <button type="submit"
                                onClick={this.callChangeTeamInfo}
                                className="btn btn-dark ml-10 reset-border noselect noselect-color-white">
                                {t('mypage.personalSettings.user.save')}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>);
    }
}

export default withTranslation()(TeamInfoPanel);