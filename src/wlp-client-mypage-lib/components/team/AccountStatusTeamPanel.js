import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import GetTeamAccountStatusConfigDto from 'wlp-client-service/dto/team/GetTeamAccountStatusConfigDto';
import GetTeamAccountStatusDto from 'wlp-client-service/dto/team/GetTeamAccountStatusDto';
import TeamService from 'wlp-client-service/service/TeamService';
import grayHddIcon from 'wlp-client-common/images/grayHddIcon@4x.png';
import grayManIcon from 'wlp-client-common/images/grayManIcon@4x.png';
import GetSize from 'wlp-client-common/utils/GetSize';
import AccountStatusInfo from './AccountStatusTeamPanel/AccountStatusInfo';

const teamService = new TeamService();
const getSize = new GetSize();
class AccountStatusTeamPanel extends Component {
    constructor() {
        super();
        this.state = {
            teamAccountStatus: {
                diskMax: null,
                diskUsed: null,
                userAdded: null,
                userMax: null
            }
        };
    };

    callGetTeamAccountStatus = async () => {
        let requestDto = new GetTeamAccountStatusConfigDto();
        const { teamData } = this.props;
        requestDto.contractId = teamData.contractId;
        requestDto.teamId = teamData.teamId;
        const result = await teamService.getTeamAccountStatus(requestDto);

        if (result) {
            this.getTeamAccountStatusResultHandler(result);
        }
    };

    getTeamAccountStatusResultHandler = (result) => {
        let response = new GetTeamAccountStatusDto();
        response.diskMax = result.diskMax;
        response.diskUsed = result.diskUsed;
        response.userAdded = result.userAdded;
        response.userMax = result.userMax;

        if (result.isSucceed) {
            this.setState({
                teamAccountStatus: {
                    diskMax: response.diskMax,
                    diskUsed: response.diskUsed,
                    userAdded: response.userAdded,
                    userMax: response.userMax
                }
            })
        }
    };

    setPositionPecent = (numberVal) => {
        if (numberVal >= 90) {
            return (numberVal-9)+'%';
        } 
        return (numberVal-1)+'%';
    }

    setColorPecent = (numberVal) => {
        if (numberVal >= 90) {
            return 'text-pecent-white';
        }
        return 'text-pecent-black';
    }

    getValueProgress = (numberVal) => {
        if (numberVal === null) {
            return '';
        }
        return numberVal
    }

    renderDiskSpaceFooter = (diskMax, diskUsed) => {
        const { t } = this.props;
        if (diskMax >= 0 && diskUsed >= 0) {
            return (
                <AccountStatusInfo
                    diskFlag={true}
                    valueMax={this.getValueProgress(getSize.getDiskSizeToMB(diskMax))}
                    valueReal={this.getValueProgress(getSize.bytesToMB(diskUsed) + 'MB')}
                    textMax={t('mypage.teamSettings.accountStatus.usage.disk.max')}
                    textReal={t('mypage.teamSettings.accountStatus.usage.disk.use')}
                />
            );
        }
        return null;
    }

    renderUserFooter = (userMax, userAdded) => {
        const { t } = this.props;
        if (userMax >= 0 && userAdded >= 0) {
            return (
                <AccountStatusInfo
                    diskFlag={false}
                    valueMax={this.getValueProgress(userMax)}
                    valueReal={this.getValueProgress(userAdded)}
                    textMax={t('mypage.teamSettings.addMember.usage.user.max')}
                    textReal={t('mypage.teamSettings.addMember.usage.user.use')}
                />
            );
        }
        return null;
    }

    componentDidMount() {
        this.callGetTeamAccountStatus();
    }

    render() {
        const { t } = this.props;
        const { teamAccountStatus } = this.state;
        const { diskMax, diskUsed, userAdded, userMax} = teamAccountStatus;
        const userAddedPecent = getSize.getDiskPecent(userAdded, userMax);
        const diskUsedPecent = getSize.getDiskPecent(diskUsed, diskMax);

        const userPecent = getSize.getPecent(userAdded, userMax);
        const diskPecent = getSize.getPecent(diskUsed, diskMax);

        return (<Fragment>
        <div className="" id="personal-tabs4" role="tabpanel" aria-labelledby="contact-tab">
            <div className="personal-tabs-accountstatus personal-tabs-item">
                <h2 className="presonal-title-tabs noselect noselect-color-777777">
                    {t('mypage.personalSettings.accountStatus.usage.title')}
                </h2>
                <div className="media align-items-center pl-media-ac">
                    <img className="mr-3 noselect" src={grayHddIcon} width="35" alt="" />
                    <div className="media-body pl-media-body">
                        <h5 className="mt-0 text-muted-status mb-text-muted-status font-size-13 noselect noselect-color-aaaaaa">{t('mypage.personalSettings.accountStatus.usage.disk.title')} :</h5>
                        <div className="progress w-500px progress-ac rounded-progress border-st h-20px w-form">
                            <div className="progress-bar bg-progress-bar color-pecent" role="progressbar" style={{width: diskPecent + '%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100}>
                                <span style={{left: this.setPositionPecent(diskPecent)}} 
                                className={`ml-md-2 progress-ac-child noselect noselect-color-818181 ${this.setColorPecent(diskPecent)}`}>
                                    {diskMax ? diskUsedPecent : '0%'}
                                </span>
                            </div>
                        </div>
                        { this.renderDiskSpaceFooter(diskMax, diskUsed) }
                    </div>
                </div>
                <div className="media align-items-center pl-media-ac mt-media-user">
                    <img className="mr-img-user noselect" src={grayManIcon} width="39" alt="" />
                    <div className="media-body pl-media-body">
                        <h5 className="mt-0 text-muted-status mb-text-muted-status font-size-13 noselect noselect-color-aaaaaa">{t('mypage.teamSettings.accountStatus.usage.user.title')} :</h5>
                        <div className="progress w-500px progress-ac rounded-progress border-st h-20px w-form">
                            <div className="progress-bar bg-progress-bar color-pecent" role="progressbar" style={{width: userPecent + '%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100}>
                                <span style={{left: this.setPositionPecent(userPecent)}} 
                                className={`ml-md-2 progress-ac-child noselect noselect-color-818181 ${this.setColorPecent(userPecent)}`}>
                                    {userMax ? userAddedPecent : '0%'}
                                </span>
                            </div>
                        </div>
                        { this.renderUserFooter(userMax, userAdded) }
                    </div>
                </div>
            </div>
        </div>
        </Fragment>);
    }
}

export default withTranslation()(AccountStatusTeamPanel);