import React, {Component, Fragment} from 'react';
import {withTranslation} from 'react-i18next';
import AddMembersConfigDto from 'wlp-client-service/dto/team/AddMembersConfigDto';
import TeamService from 'wlp-client-service/service/TeamService';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import ValidateErrorMessage from 'wlp-client-common/component/message/ValidateErrorMessage';
import ValidateUtils from 'wlp-client-common/component/ValidateUtils';
import grayManIcon from 'wlp-client-common/images/grayManIcon@4x.png';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import AccountStatusInfo from './AccountStatusTeamPanel/AccountStatusInfo';
import GetAddMembersInfoDto from 'wlp-client-service/dto/team/GetAddMembersInfoDto';
import GetAddMembersInfoConfigDto from 'wlp-client-service/dto/team/GetAddMembersInfoConfigDto';
import AddMembersDto from 'wlp-client-service/dto/team/AddMembersDto';
import AddMultipleMembersDto from 'wlp-client-service/dto/team/AddMultipleMembersDto';
import AddMultipleMembersConfigDto from 'wlp-client-service/dto/team/AddMultipleMembersConfigDto';
import TextAreaField from 'wlp-client-common/utils/TextAreaField';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { hideWarningDialog } from 'wlp-client-common/utils/DialogUtils'; 

const teamService = new TeamService();
const localStorageUtils = new LocalStorageUtils();
const validateUtils = new ValidateUtils();

const INVITE_OPTION = {
    INDIVIDUAL: 'individual',
    BATCH: 'batch'
}

class AddMembersPanel extends Component {
    constructor(props) {
        super(props);

        const currentAddMemberMethod = localStorageUtils.getLocalStrage(localStorageUtils.ADD_MEMBER_METHOD);
        
        this.batchInviteMessageRef = React.createRef();
        this.individualInviteMessageRef = React.createRef();
        this.batchInviteEmailsRef = React.createRef();

        this.defaultState = {
            batchInviteMessage: '',
            individualInviteMessage: '',
            messageTextArea: '',
            messageTextAreaBatch: '',
            listEmail: [],
            listErrorEmail: [],
            isSucceed: false,
            listMultiEmail : [],
            messErrorMultiEmail : "",
            listEmailRef: []
        };

        this.listEmailRef = [];

        this.state = {
            ...this.defaultState,
            addMemberInfo: new GetAddMembersInfoDto(),
            selectOption: currentAddMemberMethod || INVITE_OPTION.INDIVIDUAL,
            isFocusInputEmail: false,
            isFocusIndividualMessage: false,
        };

        this.maxMemberNum = 0;
        this.currentMemberNum = 0;
    };

    handleAddField = () => {
        const { listEmail } = this.state;
        listEmail.push("");
        this.setState({listEmail})
    };


    handleChangeEmail = (i, event) => {
        ActionLogger.typing('User_Mypage_TeamSetting_[メンバーの追加]', 'Input_Email');
        const { listEmail } = this.state;
        listEmail[i] = event.target.value;
        this.setState({listEmail})
    };

    handleChangeInviteMethod = (method) => {
        ActionLogger.click('User_Mypage_TeamSetting_[メンバーの追加]', `Select_${method}`);
        this.setState({ selectOption: method });
        localStorageUtils.setLocalStrage(localStorageUtils.ADD_MEMBER_METHOD, method);
    }

    callAddMembers = async () => {
        this.setState({listErrorEmail: []});
        const {teamData} = this.props;
        const { individualInviteMessage, listEmail } = this.state;

        if(listEmail.length > 0){
            let requestDto = new AddMembersConfigDto();
            
            requestDto.emailList = listEmail.filter(Boolean);
            requestDto.optionalMsg = individualInviteMessage;
            requestDto.teamId = teamData.teamId;
            requestDto.contractId = teamData.contractId;

            const result = await teamService.addMembers(requestDto);
            this.addMembersResultHandler(result);
        }
    };

    callAddMultipleMembers = async () => {
        const {teamData} = this.props;
        const { batchInviteMessage , listMultiEmail} = this.state;
        let listMultiEmailToString = listMultiEmail.toString().trim();

        this.setState({messErrorMultiEmail: null});

        if(listMultiEmailToString.length > 0) {
            let requestDto = new AddMultipleMembersConfigDto();
            requestDto.emailList = listMultiEmailToString;
            requestDto.optionalMsg = batchInviteMessage;
            requestDto.teamId = teamData.teamId;
            requestDto.contractId = teamData.contractId;

            const result = await teamService.addMultipleMembers(requestDto);
            this.addMultipleMembersResultHandler(result);
        }
    };

    resetAddMemberPanel = () => {
        if (3 <= this.maxMemberNum - this.currentMemberNum) {
            this.setState(state => ({
                ...state,
                ...this.defaultState,
                listEmail: ["", "", ""]
            }));
        } else {
            let listEmail = [];
            const showEmailInputNum = this.maxMemberNum - this.currentMemberNum;
            for (let i = 0; i < showEmailInputNum; i++) {
                listEmail.push("");
            }

            this.setState(state => ({
                ...state,
                ...this.defaultState,
                listEmail
            }));
        }
    };

    addMembersResultHandler = (result = new AddMembersDto()) => {
        const {listEmail, listErrorEmail} = this.state;
        let listError = result.errorEmailList;
        
        if(result.isSucceed === false && (!listError || listError.length === 0)){
            return showWarningDialog(result.messageKey);
        }

        this.currentMemberNum = result.currentMemberNum;

        if (listError) {
            for(let item of listError){
                const indexOfEmail = listEmail.indexOf(item.email);
                if(indexOfEmail > -1){
                    listErrorEmail[indexOfEmail] = item.errorMessage;
                }
            }
            this.setState({listErrorEmail});
        } else {
            this.setState({
                isSucceed: true,
                addMemberInfo: result
            });
        }
    };

    addMultipleMembersResultHandler = (result = new AddMultipleMembersDto()) => {
        this.setState({isSucceed: result.isSucceed});
        if(result.isSucceed) {
            this.currentMemberNum = result.currentMemberNum;
            this.setState(state => {
                return {
                    addMemberInfo: {
                        ...state.addMemberInfo,
                        currentMemberNum: result.currentMemberNum
                    }
                }
            });
        }else{
            if (result.errorEmailList == null || result.errorEmailList.length === 0){
                showWarningDialog(result.message);
                return;
            }
            this.setState({
                messErrorMultiEmail: result.messageKey
            })
        }
    };

    setBatchInviteMessage = (value) => {
        ActionLogger.typing('User_Mypage_TeamSetting_[メンバーの追加]', 'Texarea_Batch_Message');
        this.setState({batchInviteMessage: value});
    };

    setIndividualInviteMessage = (value)  => {
        ActionLogger.typing('User_Mypage_TeamSetting_[メンバーの追加]', 'Texarea_Message');
        this.setState({individualInviteMessage: value});
    };

    setEmailTextArea = (value) => {
        ActionLogger.typing('User_Mypage_TeamSetting_[メンバーの追加]', 'Texarea_Email');
        this.setState({listMultiEmail: value});
    };

    handleBlur = (i, event) => {
        const { listEmail, listErrorEmail } = this.state;
        if (!event.target.value) {
            delete listErrorEmail[i];
        } else {
            const isEmailValid = validateUtils.validateEmail(listEmail[i]);
            if(isEmailValid === false) {
                listErrorEmail[i] = 'message.global.e0002';
            }else{
                delete listErrorEmail[i];
            }
        }

        this.setState({ 
            listErrorEmail,
            isFocusInputEmail: false 
        });
    };
    handleFocus = () => {
        this.setState({
            isFocusInputEmail: true
        })
    }

    setColorPecent = (numberVal) => {
        if (numberVal >= 90) {
            return 'text-pecent-white';
        }
        return 'text-pecent-black';
    }
    
    setPositionPecent = (numberVal) => {
        if (numberVal >= 90) {
            return (numberVal-9)+'%';
        } 
        return (numberVal-1)+'%';
    }

    validateEmailArr = () => {
        let { listEmail } = this.state;
        let focusElIndex = null;
        let validatedTextInputArr = [];
        let listError = [];

        for(let emailIndex in listEmail){
            emailIndex = Number(emailIndex);
            const email = listEmail[emailIndex];
            
            if(!email){continue};

            if (email != null && email !== "") {
                validatedTextInputArr.push(email);
            } else {
                if(focusElIndex === null){
                    focusElIndex = emailIndex;
                }
            }

            if(focusElIndex !== null){
                if(validatedTextInputArr.length > 0 && this.listEmailRef[focusElIndex]){
                    this.listEmailRef[focusElIndex].focus();
                }
                return [];
            }

            let validateObj = {};
            for (const [index, validatedCheckInput] of listEmail.entries()) {
                if (validateObj.hasOwnProperty(validatedCheckInput) && validateObj[validatedCheckInput] === true && validatedCheckInput !== "") {
                    if(focusElIndex === null){
                        focusElIndex = index;
                    }
                    listError[index] = 'message.team.e0001';
                } else {
                    validateObj[validatedCheckInput] = true;
                }
            }

            if (listError.length !== 0) {
                this.setState({ listErrorEmail: listError });
            };

            if(focusElIndex !== null){
                if(validatedTextInputArr.length > 0 && this.listEmailRef[focusElIndex]){
                    this.listEmailRef[focusElIndex].focus();
                }
                return [];
            }
        }

        return validatedTextInputArr;
    }

    inviteBtn_handleClick = () => {
        let errorPopup = document.getElementById('error-modal');
        if (errorPopup !== null) {
            hideWarningDialog();
            return true;
        }

        ActionLogger.click('User_Mypage_TeamSetting_[メンバーの追加]', `Button_Invite`);
        const { selectOption } = this.state;
        if(selectOption === INVITE_OPTION.BATCH){
            this.callAddMultipleMembers();
        }else if(selectOption === INVITE_OPTION.INDIVIDUAL){
            const emailArr = this.validateEmailArr();
            if (emailArr.length !== 0) {
                this.callAddMembers();
            }
        }
    }

    /********************************************************
     * 
     * Service method
     * 
     *************************************************/
    callGetAddMembersInfo = () => {
        const requestDto = new GetAddMembersInfoConfigDto();
        requestDto.teamId = this.props.teamData.teamId;
        teamService.getAddMembersInfo(requestDto)
            .then(this.callGetAddMembersInfo_handleResult);
    };

    callGetAddMembersInfo_handleResult = (result = new GetAddMembersInfoDto()) => {
        let maxEmailInputField = result.maxMemberNum - result.currentMemberNum;
        let listEmail = [...Array(maxEmailInputField > 3 ? 3 : maxEmailInputField)].fill('');
        this.maxMemberNum = result.maxMemberNum;
        this.currentMemberNum = result.currentMemberNum;
        this.setState({
            addMemberInfo : result,
            listEmail: listEmail
        });
    };


    renderUserFooter = (userMax, userAdded) => {
        const { t } = this.props;
        if (userMax && userAdded) {
            return (
                <AccountStatusInfo
                    diskFlag={false}
                    valueMax={userMax ? userMax : ''}
                    valueReal={userAdded ? userAdded : ''}
                    textMax={t('mypage.teamSettings.accountStatus.usage.user.max')}
                    textReal={t('mypage.teamSettings.accountStatus.usage.user.use')}
                />
            );
        }
        return null;
    }

    renderIndividualEmailFields(key){
        const {listErrorEmail, listEmail, isFocusInputEmail } = this.state;
        const { t } = this.props;
        let classAdd = ["position-reset"];
        let inputFieldClasses = ['form-control mt-3 form-control-shadow'];
        if(listErrorEmail[key]){
            inputFieldClasses.push('is-invalid');
        }else{
            if(listEmail[key] === ""){
                inputFieldClasses.push('form-control-shadow-member add-member-email-field');
            }
        }
        if (!isFocusInputEmail) {
            inputFieldClasses.push('noselect');
        }
        return (
            <div key={key} className="position-relative">
                <input 
                    value={listEmail[key]}
                    type="input"
                    className={`${inputFieldClasses.join(' ')}`}
                    ref={ref => this.listEmailRef[key] = ref}
                    onChange={e => this.handleChangeEmail(key, e)}
                    onBlur={e => this.handleBlur(key, e)}
                    onFocus={e => this.handleFocus()}
                    placeholder={t('mypage.teamSettings.addMember.member.prompt')}/>
                {listErrorEmail[key] && <ValidateErrorMessage classAdd={classAdd} message={t(listErrorEmail[key])}/>}

            </div>
        );
    }

    renderInviteIndividual(){
        const { listEmail, individualInviteMessage } = this.state;
        const { t } = this.props;
        const addEmailFields =  listEmail.map((__, key) => this.renderIndividualEmailFields(key));
        const maxEmailField = this.maxMemberNum - this.currentMemberNum
        const isShowAddFieldBtn = listEmail.length < maxEmailField;
        let individualInviteMessageClasses = ['form-control form-control-shadow no-resize'];

        if(individualInviteMessage.length === 0){ individualInviteMessageClasses.push('form-control-shadow-member')};

        return (<React.Fragment>
            <div className="w-300px">
                {addEmailFields}
                {
                    isShowAddFieldBtn &&
                    <div className="mt-3">
                        <button
                            type="button"
                            onClick={() => this.handleAddField()}
                            className="btn btn-link text-dark pl-0">
                            <i className="fas fa-plus-circle noselect"/>
                            <span className="button-add font-size-12 pl-3px noselect">{t('mypage.teamSettings.addMember.member.add')}</span></button>
                    </div>
                }
            </div>
            <div className="w-500px mt-2 w-form">
                <TextAreaField 
                    heightField="100px" 
                    maxLengthStr={2048} 
                    placeholderText={t('editor.link.messagePrompt')}
                    rowSpan='4'
                    value={individualInviteMessage}
                    setValueTextArea={this.setIndividualInviteMessage}
                    classTextArea={individualInviteMessageClasses.join(' ')}
                />
            </div>
        </React.Fragment>);
    }

    renderInviteBatch(){
        const { messErrorMultiEmail, listMultiEmail, batchInviteMessage } = this.state;
        const { t } = this.props;
        const classAdd = "position-reset pt-10px";
        const listMultiEmailToString = listMultiEmail.toString().split(',').join('\n');
        let batchEmailTextAreaClasses = ['form-control form-control-shadow no-resize'];
        let batchInviteMessageClasses = ['form-control form-control-shadow no-resize'];
        
        if(messErrorMultiEmail){ batchEmailTextAreaClasses.push('is-invalid-area reset-border-color') }
        else if(listMultiEmailToString.length === 0){ batchEmailTextAreaClasses.push('form-control-shadow-member') }

        if(batchInviteMessage.length === 0){ batchInviteMessageClasses.push('form-control-shadow-member')}

        return (<React.Fragment>
            <div className="w-300px mb-17px">
                <TextAreaField 
                    heightField="200px" 
                    maxLengthStr={5000} 
                    placeholderText={t('mypage.teamSettings.addMember.member.prompt')}
                    rowSpan='10'
                    value={listMultiEmail}
                    setValueTextArea={this.setEmailTextArea}
                    classTextArea={batchEmailTextAreaClasses.join(' ')}
                />
                {messErrorMultiEmail &&  <ValidateErrorMessage check={true} classAdd={classAdd} message={t(messErrorMultiEmail)}/>}
            </div>
            <div className="w-500px mt-2 w-form">
                <TextAreaField 
                    heightField="100px" 
                    maxLengthStr={2048} 
                    placeholderText={t('editor.link.messagePrompt')}
                    rowSpan='4'
                    value={batchInviteMessage}
                    setValueTextArea={this.setBatchInviteMessage}
                    classTextArea={batchInviteMessageClasses.join(' ')}
                />
            </div>
        </React.Fragment>)
    }

    renderIndividualRadio(){
        const isIndividualInvite = this.state.selectOption === INVITE_OPTION.INDIVIDUAL;
        let labelClasses = ['btn btn-default d-flex align-items-center font-size-12 padding-label-select radio-add-member'];
        if(isIndividualInvite){ labelClasses.push('active') }

        return (
            <label className={labelClasses.join(' ')} htmlFor="option1">
                <input 
                    type="radio" 
                    name="options" 
                    id="option1"
                    className="resize-cb-rd" 
                    value="individual"
                    checked={isIndividualInvite}
                    onChange={(e) => this.handleChangeInviteMethod(INVITE_OPTION.INDIVIDUAL)}
                />
                <span className="checkmark"></span>
                <span className="ml-20px noselect">{this.props.t('mypage.teamSettings.addMember.invite.individual')}</span>
            </label>
        )
    }

    renderBatchRadio(){
        const isBatchInvite = this.state.selectOption === INVITE_OPTION.BATCH;
        let labelClasses = ['ml-3 btn btn-default d-flex align-items-center font-size-12 padding-label-select radio-add-member'];
        if(isBatchInvite){ labelClasses.push('active') }

        return (
            <label className={labelClasses.join(' ')} htmlFor="option2">
                <input 
                    type="radio" 
                    name="options" 
                    id="option2"
                    className="resize-cb-rd" 
                    value="batch"
                    checked={isBatchInvite}
                    onChange={(e) => this.handleChangeInviteMethod(INVITE_OPTION.BATCH)}
                />
                <span className="checkmark"></span>
                <span className="ml-20px noselect">{this.props.t('mypage.teamSettings.addMember.invite.batch')}</span>
            </label>
        )
    }

    renderInviteFormBody(){
        const { t } = this.props;
        const { selectOption } = this.state;
        const isIndividualInvite = selectOption === INVITE_OPTION.INDIVIDUAL;
        const isBatchInvite = selectOption === INVITE_OPTION.BATCH;
        return (
            <div className="mt-1 pl-10px">
                <div className="tab-content">
                    <div className={`tab-pane fade show active`}>
                        {isIndividualInvite && <p className="text-color-gray noselect">{t('mypage.teamSettings.addMember.member.comments')}</p>}
                        {isBatchInvite && <p className="text-color-gray noselect">{t('mypage.teamSettings.addMember.member.batchComments')}</p>}
                        <div>
                            { isIndividualInvite && this.renderInviteIndividual()}
                            { isBatchInvite && this.renderInviteBatch()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderInviteForm(){
        const { t } = this.props;
        return (
            <div>
                <div className="d-flex">
                    <h2 className="presonal-title-tabs noselect noselect-color-777777">{t('mypage.teamSettings.addMembers')}</h2>
                    <div className="ml-auto">
                        <div className="btn-group align-items-center"
                             data-toggle="buttons">
                            {this.renderIndividualRadio()}
                            {this.renderBatchRadio()}
                        </div>
                    </div>
                </div>
                {this.renderInviteFormBody()}
                <hr className="border-style-dotted"/>
                <button 
                    type="button"
                    onClick={this.inviteBtn_handleClick}
                    className="btn btn-dark w-100px reset-box-shadow noselect-color-white">
                    {t('mypage.teamSettings.addMember.invite.btnLbl')}
                </button>
            </div>
        )
    }

    renderInviteSuccessMessage(){
        const { t } = this.props;
        return (
            <div>
                <div className="d-flex">
                    <h2 className="presonal-title-tabs personal-tabs-item fixed-title-tabs noselect">{t('mypage.teamSettings.addMembers')}</h2>
                </div>
                <div className="mt-1">
                    <div className="tab-content">
                        <div className="tab-pane fade show active" id="tabs-rdi1">
                            <form action="">
                                <h2
                                    className={'save-message presonal-title-tabs mb-20px border-radius-5px d-block noselect'}>
                                    {t('mypage.teamSettings.addMember.member.complete')}
                                </h2>
                            </form>
                        </div>
                    </div>
                </div>
                <hr className="border-style-dotted border-weight"/>
                <button type="button"
                        onClick={this.resetAddMemberPanel}
                        className="btn btn-dark btn-add-member-sc w-100px mb-4px mt-4px">{t('global.ok')}
                </button>
            </div>
        )
    }

    renderMaxMemberMessage(){
        const { t } = this.props;

        return (
            <React.Fragment>
                <div className="d-flex">
                    <h2 className="presonal-title-tabs  personal-tabs-item fixed-title-tabs noselect">{t('mypage.teamSettings.addMembers')}</h2>
                </div>
                <div className="mt-1">
                    <div className="tab-content">
                        <div className="tab-pane fade show active">
                            <div className="text-red noselect">{t('mypage.teamSettings.addMember.member.maxMessage')}</div>
                            <div className="text-666666 noselect">{t('mypage.teamSettings.addMember.member.maxMessageOption')}</div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }

    renderPercenStatusBar = (value) => {
        return (
            <div className="progress w-500px progress-ac rounded-progress border-st h-20px w-form">
                <div className="progress-bar bg-progress-bar color-pecent" role="progressbar" style={{width: `${value}%`}}>
                    <span style={{left: this.setPositionPecent(value)}} className={`ml-md-2 progress-ac-child noselect noselect-color-818181 ${this.setColorPecent(value)}`}>
                        {value}%
                    </span>
                </div>
            </div>
        )
    }

    handleReloadPage = () => {
        localStorageUtils.setLocalStrage(localStorageUtils.ADD_MEMBER_METHOD, INVITE_OPTION.INDIVIDUAL);
    }

    componentDidMount() {
        this.callGetAddMembersInfo();
        window.addEventListener("beforeunload", this.handleReloadPage);
    }

    componentWillUnmount() {
        window.removeEventListener("beforeunload", this.handleReloadPage)
    }

    render() {
        const {t} = this.props;
        const {isSucceed, addMemberInfo } = this.state;
        const {currentMemberNum} = addMemberInfo;
        return (<Fragment>
                <div className="tab-pane fade show active sc-add-members" id="personal-tabs1" role="tabpanel"
                     aria-labelledby="home-tab">
                    <div className="personal-tabs-addmembers">
                        <h2 className="presonal-title-tabs noselect noselect-color-777777">{t('mypage.teamSettings.accountStatus.usage.title')}</h2>

                        <div className="media align-items-center pl-14px">
                            <img className="mr-3 noselect" alt="" src={grayManIcon} width="40" />
                            <div className="media-body">
                                <h5 className="mt-0 text-muted-status font-size-13 noselect noselect-color-aaaaaa">{t('mypage.teamSettings.accountStatus.usage.user.title')}: </h5>
                                {this.renderPercenStatusBar(currentMemberNum)}
                                {this.renderUserFooter(this.maxMemberNum, currentMemberNum)}
                            </div>
                        </div>
                        <hr className="border-style-dotted"/>
                        {(() => {
                            if(isSucceed){
                                return this.renderInviteSuccessMessage();
                            }

                            if(this.maxMemberNum === currentMemberNum && this.maxMemberNum !== 0){
                                return this.renderMaxMemberMessage();
                            }

                            return this.renderInviteForm()
                        })()}
                    </div>
                </div>

            </Fragment>
        );
    }
}

export default withTranslation()(AddMembersPanel);