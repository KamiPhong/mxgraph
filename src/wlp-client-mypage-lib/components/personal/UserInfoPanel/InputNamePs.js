import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';

class InputNamePs extends Component {
    constructor(props) {
        super(props);
        this.state={
            isFocusFirstName: false,
            isFocusLastName: false
        }
        this.firstNameRef = React.createRef();
        this.lastNameRef = React.createRef();
    }
    componentDidUpdate() {
        const { validateFirstName, validateLastName } = this.props;
        if (this.props.changeUserError) {
            if (!validateFirstName) {
                this.firstNameRef.focus();
            } else if (!validateLastName) {
                this.lastNameRef.focus();
            }
        }
    }

    onBlurFirstNameHandler = (evt) => {
        this.firstNameRef.blur();
        if (typeof this.props.setChangeUserError === "function") {
            this.props.setChangeUserError(false);
        }

        if (typeof this.props.validateUserInfo === "function") {
            this.props.validateUserInfo();
        }
        this.setState({
            isFocusFirstName: false
        })
    }

    onBlurLastNameHandler = () => {
        this.lastNameRef.blur();
        if (typeof this.props.setChangeUserError === "function") {
            this.props.setChangeUserError(false);
        }
        
        if (typeof this.props.validateUserInfo === "function") {
            this.props.validateUserInfo();
        }
        this.setState({
            isFocusLastName: false
        })
    }

    onFocusFirstNameHandle = () => {
        this.setState({
            isFocusFirstName: true
        })
    }

    onFocusLastNameHandle = () => {
        this.setState({
            isFocusLastName: true
        })
    }

    render() {
        const { 
            firstName, lastName,
            labelFirstName, labelLastName, 
            validateFirstName, validateLastName, 
            plFirstName, plLastName,
            infoFirstName, infoLastName,
            setStateUserInfo,
        } = this.props;

        const { isFocusFirstName, isFocusLastName } = this.state;

        return (<Fragment>
            <div className="form-group custom-form-group w-160px">
                <label htmlFor="exampleInputPassword1" className="font-weight-bold custom-text-mute fs-label-ps noselect noselect-color-9da1a5">
                    {labelFirstName} :
                </label>
                <input type="text"
                    ref={r => this.firstNameRef = r}
                    value={firstName}
                    onChange={(e) => setStateUserInfo(infoFirstName, e.target.value)}
                    className={`form-control form-control-shadow ${!validateFirstName ? " is-invalid" : ""} ${isFocusFirstName ? "" : " noselect"}`}
                    maxLength="64"
                    onBlur={this.onBlurFirstNameHandler}
                    onFocus={this.onFocusFirstNameHandle}
                    placeholder={plFirstName} />
            </div>
            <div className="form-group custom-form-group w-160px ml-10">
                <label htmlFor="exampleInputPassword1" className="font-weight-bold custom-text-mute fs-label-ps noselect noselect-color-9da1a5">
                    {labelLastName} :
                </label>
                <input type="text"
                    ref={r => this.lastNameRef = r}
                    value={lastName}
                    onChange={(e) => setStateUserInfo(infoLastName, e.target.value)}
                    className={`form-control form-control-shadow ${!validateLastName ? " is-invalid" : ""} ${isFocusLastName ? "" : " noselect"}`}
                    maxLength="64"
                    onBlur={this.onBlurLastNameHandler}
                    onFocus={this.onFocusLastNameHandle}
                    placeholder={plLastName} />
            </div>
        </Fragment>);
    }

}

export default withTranslation()(InputNamePs);