import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import ValidateUtils from 'wlp-client-common/component/ValidateUtils';
import ValidateErrorMessage from 'wlp-client-common/component/message/ValidateErrorMessage';
import ChangePasswordConfigDto from 'wlp-client-service/dto/password/ChangePasswordConfigDto';
import PasswordService from 'wlp-client-service/service/PasswordService';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import InputUtils from 'wlp-client-common/utils/InputUtils';
import ActionLogger from 'wlp-client-common/ActionLogger';
import { hideWarningDialog } from 'wlp-client-common/utils/DialogUtils'; 

const localStorageUtil = new LocalStorageUtils();
const validateUtils = new ValidateUtils();
const passwordService = new PasswordService();
const inputUtils = new InputUtils();


class PasswordPanel extends Component {

    constructor() {
        super();
        this.initialState = {
            password: '',
            newPassword: '',
            confirmNewPassword: '',
            isShowErrorPassword: false,
            isSucceed: false,
            messageKey: '',
            touched: {
                password: false,
                newPassword: false,
                confirmNewPassword: false,
            },
            isFocusPassWord: false,
            isFocusNewPassWord: false,
            isFocusConfirmPassWord: false,
            isRequesting:false
        };
        const logonInfo = localStorageUtil.getUserSession();
        this.accessToken = logonInfo.accessToken;
        this.state = this.initialState;

        this.inputPassword = React.createRef();
        this.inputNewPassword = React.createRef();
        this.inputConfirmNewPassword = React.createRef();
    };


    handleBlur = field => evt => {
        this.setState({
            touched: { ...this.state.touched, [field]: true }
        });

        if (field === 'password') {

            if (evt.target.value === '') {
                this.setState({
                    isShowErrorPassword: false ,
                    touched: { ...this.state.touched, [field]: true },
                    isFocusPassWord: false
                });

            } else {
                this.setState({
                    touched: { ...this.state.touched, [field]: false },
                    isFocusPassWord: false
                });
            }
        }
        if (field === 'newPassword') {
            let regex = "(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,})";
            if (Boolean(evt.target.value.match(regex))) {
                this.setState({
                    touched: { ...this.state.touched, [field]: false },
                });

            }
            this.setState({
                isFocusNewPassWord: false
            })
        }
        if (field === "confirmNewPassword") {
            this.setState({
                isFocusConfirmPassWord: false
            })
        }
    };

    handleFocus = field => evt => {
        if (field === 'password') {
            this.setState({
                isFocusPassWord: true
            });
        }
        if (field === 'newPassword') {
            this.setState({
                isFocusNewPassWord: true
            });
        }
        if (field === 'confirmNewPassword') {
            this.setState({
                isFocusConfirmPassWord: true
            });
        }
    }

    replaceJpCharacter = (input) => {
        return input.replace(/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u, "");
    }

    shouldMarkError = field => {
        const { password } = this.state;
        const errors = validateUtils.validatePassword(password);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];
        if (field === 'password') {
            return (
                hasError ? shouldShow : false
            );
        }
    };

    shouldMarkNewPassError = field => {
        const { newPassword } = this.state;
        const errorsNewPass = validateUtils.validateNewPassword(newPassword);
        const shouldShow = this.state.touched[field];
        if (field === 'newPassword') {
            return (
                errorsNewPass ? shouldShow : false
            );
        }
    };

    shouldMarkConfirmNewPassError = field => {
        const { confirmNewPassword, newPassword } = this.state;
        const errors = validateUtils.validateConfirmNewPassword(confirmNewPassword, newPassword);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];
        if (field === 'confirmNewPassword') {
            return (
                hasError ? shouldShow : false
            );
        }
    };


    setPassWord = (event) => {
        ActionLogger.typing('User_Mypage_PersonalSetting_[パスワード]', 'Input_Set_Password');
        let valuePassword = this.replaceJpCharacter(event.target.value);
        const isCharValid = valuePassword.indexOf(' ') > 0 ? false : true;
        const { selectionEnd } =  event.currentTarget;
        const val = this.state.password;

        if(valuePassword.length > 100){
            this.setState(
                {
                    password: val,
                    isShowErrorPassword: false
                }, () => {
                    inputUtils.setCaretPosition(this.inputPassword, selectionEnd);
                }
            );
        }else{
            let newValPassword = valuePassword;
            if(!isCharValid){
                newValPassword = valuePassword.slice(0, selectionEnd - 1) + valuePassword.slice(selectionEnd, valuePassword.length);
            }
            this.setState(
                {
                    password: newValPassword.trim(),
                    isShowErrorPassword: false
                }, () => {
                    let cursorPosPass = valuePassword.length;
                    if(isCharValid){
                        cursorPosPass = selectionEnd;
                    }else {
                        cursorPosPass = selectionEnd - 1;
                    }
                    inputUtils.setCaretPosition(this.inputPassword, cursorPosPass);
                }
            );
        }
    };

    setNewPassWord = (event) => {
        ActionLogger.typing('User_Mypage_PersonalSetting_[パスワード]', 'Input_Set_New_Password');
        let valuePassword = this.replaceJpCharacter(event.target.value);
        const isCharValid = valuePassword.indexOf(' ') > 0 ? false : true;
        const { selectionEnd } =  event.currentTarget;
        const val = this.state.password;

        if(valuePassword.length > 100){
            this.setState(
                {
                    newPassword: val,
                    touched: {...this.state.touched, confirmNewPassword: false }
                }, () => {
                    inputUtils.setCaretPosition(this.inputNewPassword, selectionEnd);
                }
            );
        }else{
            let newValPassword = valuePassword;
            if(!isCharValid){
                newValPassword = valuePassword.slice(0, selectionEnd - 1) + valuePassword.slice(selectionEnd, valuePassword.length);
            }
            this.setState(
                {
                    newPassword: newValPassword.trim(),
                    touched: {...this.state.touched, confirmNewPassword: false }
                }, () => {
                    let cursorPosPass = valuePassword.length;
                    if(isCharValid){
                        cursorPosPass = selectionEnd;
                    }else {
                        cursorPosPass = selectionEnd - 1;
                    }
                    inputUtils.setCaretPosition(this.inputNewPassword, cursorPosPass);
                }
            );
        }
    };

    setConfirmNewPassWord = (event) => {
        ActionLogger.typing('User_Mypage_PersonalSetting_[パスワード]', 'Input_Set_Confirm_New_Password');
        let valuePassword = this.replaceJpCharacter(event.target.value);
        const isCharValid = valuePassword.indexOf(' ') > 0 ? false : true;
        const { selectionEnd } =  event.currentTarget;
        const val = this.state.password;

        if(valuePassword.length > 100){
            this.setState(
                {
                    confirmNewPassword: val
                }, () => {
                    inputUtils.setCaretPosition(this.inputConfirmNewPassword, selectionEnd);
                }
            );
        }else{
            let newValPassword = valuePassword;
            if(!isCharValid){
                newValPassword = valuePassword.slice(0, selectionEnd - 1) + valuePassword.slice(selectionEnd, valuePassword.length);
            }
            this.setState(
                {
                    confirmNewPassword: newValPassword.trim()
                }, () => {
                    let cursorPosPass = valuePassword.length;
                    if(isCharValid){
                        cursorPosPass = selectionEnd;
                    }else {
                        cursorPosPass = selectionEnd - 1;
                    }
                    inputUtils.setCaretPosition(this.inputConfirmNewPassword, cursorPosPass);
                }
            );
        }
    };

    onButton_handleClick = () => {
        ActionLogger.click('User_Mypage_PersonalSetting_[パスワード]', 'Button_Cancel');
        this.setState(this.initialState);
    };



    ChangePassword_requestHandler = async (accessToken, currentPassword, newPassword) => {
        this.setState({isRequesting : true});
        const changePasswordConfigDto = new ChangePasswordConfigDto();
        changePasswordConfigDto.accessToken = accessToken;
        changePasswordConfigDto.currentPassword = currentPassword;
        changePasswordConfigDto.newPassword = newPassword;
        const changePassword = await passwordService.changePassword(changePasswordConfigDto);
        this.changePassword_resultHandler(changePassword);
    };

    handleClickSubmit = () => {
        ActionLogger.click('User_Mypage_PersonalSetting_[パスワード]', 'Button_OK');
        this.setState(() => {
            return {
                touched: {
                    password: true,
                    newPassword: true,
                    confirmNewPassword: true
                }
            }
        }, () => {
            let isEmptyPassword = this.shouldMarkError("password");
            let isErrorNewPassword = this.shouldMarkNewPassError("newPassword");
            let isErrorConfirmNewPassword = this.shouldMarkConfirmNewPassError("confirmNewPassword");
    
            if (isEmptyPassword) {
                this.inputPassword.current.select();
                return;
            } 
            if (isErrorNewPassword) {
                this.inputNewPassword.current.select();
                return;
            } 
            if (isErrorConfirmNewPassword) {
                this.inputConfirmNewPassword.current.select();
                return;
            }
        });

    };


    changePassword_handleClick = (evt) => {
        evt.preventDefault();
        let errorPopup = document.getElementById('error-modal');
        if (errorPopup !== null) {
            hideWarningDialog();
            return true;
        }

        let isErrorNewPassword = this.shouldMarkNewPassError("newPassword");
        const { password, newPassword, confirmNewPassword, isRequesting } = this.state;
        if ((password !== "" && newPassword !== "" && confirmNewPassword !== "") && (newPassword === confirmNewPassword) && !isErrorNewPassword && !isRequesting ) {
            this.ChangePassword_requestHandler(this.accessToken, password, newPassword)
        }
    };



    changePassword_resultHandler = (result) => {
        const { isSucceed } = result;
        if (!isSucceed) {
            this.setState({ isShowErrorPassword: true, messageKey: result.message })
        } else {
            this.setState({ isSucceed: true, isShowErrorPassword: false })
        }
        this.setState({isRequesting : false});
    };

    componentDidUpdate = () => {
        let _this = this;
        if (this.state.isSucceed) {
            setTimeout(function () {
                _this.setState({ isSucceed: false })
            }, 5000)
        }
    };

    render() {
        const { shouldMarkError } = this;
        const { shouldMarkNewPassError } = this;
        const { shouldMarkConfirmNewPassError } = this;
        const { t } = this.props;
        const { password, newPassword, confirmNewPassword, isShowErrorPassword, isSucceed, messageKey, isFocusPassWord, isFocusNewPassWord, isFocusConfirmPassWord } = this.state;
        let isEmptyPassword = shouldMarkError("password");
        let isErrorNewPassword = shouldMarkNewPassError("newPassword");
        let isErrorConfirmNewPassword = shouldMarkConfirmNewPassError("confirmNewPassword");
        return (<Fragment>
            <div className="" id="personal-tabs2">
                <div className="personal-tabs-password personal-tabs-item">

                    <form onSubmit={this.changePassword_handleClick}>
                        <div className="w-360px ">
                            <h2 className="presonal-title-tabs noselect noselect-color-777777">{t('mypage.personalSettings.password.title')}</h2>
                        </div>
                        <h2
                            className={'save-message presonal-title-tabs noselect noselect-color-777777' + (isSucceed ? " d-block" : " d-none")}>
                            {t('mypage.personalSettings.user.saveMessage')}
                        </h2>
                        <div className="w-360px ml-10 ">
                            <div className="form-group">
                                <label className="font-weight-bold text-muted text-pasword noselect noselect-color-aaaaaa">
                                    {t('mypage.personalSettings.password.currentPassword')}: </label>
                                <input type="password"
                                    ref={this.inputPassword}
                                    value={password}
                                    onChange={this.setPassWord}
                                    onBlur={this.handleBlur("password")}
                                    onFocus={this.handleFocus("password")}
                                    className={'form-control ' + 
                                    (password === '' ? " form-control-textarea-shadow" : " form-control-shadow") + 
                                    (isShowErrorPassword || isEmptyPassword ? " is-invalid" : "") +
                                    (isFocusPassWord ? "" : " noselect")}/>

                                {(isEmptyPassword) &&
                                    <ValidateErrorMessage classError ={'set-width-strong'} classAdd={'add-position'}
                                        message={t('message.personal.e0004')} />}
                                {isShowErrorPassword &&
                                    <ValidateErrorMessage classError ={'set-width-strong'} classAdd={'add-position'}
                                        message={messageKey !== "" ? t(messageKey) : t('message.personal.e0002')} />}
                                <small className="form-text text-muted mt-2 noselect">
                                    ※ {t('mypage.personalSettings.password.passComment')}
                                </small>

                            </div>
                            <div className="form-group">
                                <label className="font-weight-bold text-muted text-pasword noselect noselect-color-aaaaaa">
                                    {t('mypage.personalSettings.password.newPassword')}: </label>
                                <input type="password"
                                    value={newPassword}
                                    ref={this.inputNewPassword}
                                    maxLength="100"
                                    onChange={this.setNewPassWord}
                                    onBlur={this.handleBlur("newPassword")}
                                    onFocus={this.handleFocus("newPassword")}
                                    className={'form-control ' + 
                                    (newPassword === '' ? " form-control-textarea-shadow" : " form-control-shadow") + 
                                    (isErrorNewPassword ? " is-invalid" : "") +
                                    (isFocusNewPassWord ? "" : " noselect")}/>
                                {isErrorNewPassword && <ValidateErrorMessage classError ={'set-width-strong'} classAdd={'add-position'}
                                    message={t('message.personal.e0003')} />}
                                <input type="password"
                                    value={confirmNewPassword}
                                    ref={this.inputConfirmNewPassword}
                                    maxLength="100"
                                    onChange={this.setConfirmNewPassWord}
                                    onBlur={this.handleBlur("confirmNewPassword")}
                                    onFocus={this.handleFocus("confirmNewPassword")}
                                    placeholder={t('mypage.personalSettings.password.reEnterNewPassword')}
                                    className={'form-control mt-3 ' + 
                                        (confirmNewPassword === '' ? " form-control-textarea-shadow" : " form-control-shadow") + 
                                        (isErrorConfirmNewPassword ? " is-invalid" : "") +
                                        (isFocusConfirmPassWord ? "" : " noselect")} />
                                {isErrorConfirmNewPassword &&
                                    <ValidateErrorMessage classError ={'set-width-strong noselect'} classAdd={'add-position'}
                                        message={t('message.personal.e0002')} />}
                            </div>
                        </div>
                        <hr className="border-style-dotted" />
                        <button type="button" onClick={this.onButton_handleClick}
                            className="btn btn-whitefl w-100px reset-border noselect-color-444">{t('global.cancel')}</button>
                        <button type="submit" onClick={this.handleClickSubmit}
                            className="btn btn-dark w-100px ml-10 reset-border noselect-color-white">{t('global.ok')}</button>
                    </form>
                </div>
            </div>
        </Fragment>);
    }
}

export default withTranslation()(PasswordPanel);