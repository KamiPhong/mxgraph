import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import GetJoinTeamInfoConfigDto from 'wlp-client-service/dto/user/GetJoinTeamInfoConfigDto';
import SessionDto from 'wlp-client-service/dto/SessionDto';
import LogonDto from 'wlp-client-service/dto/auth/LogonDto';
import ContractType from 'wlp-client-service/consts/ContractType';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import UserService from 'wlp-client-service/service/UserService';
import GetJoinTeamInfoDto from 'wlp-client-service/dto/user/GetJoinTeamInfoDto';
import MapUtil from 'wlp-client-mypage-lib/utils/MapUtil';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import TableResize from 'wlp-client-common/component/TableResize/TableResize'
import i18n from 'i18n';
import TeamDgUserTypeRenderer from '../renderer/TeamDgUserTypeRenderer';
import { tableResizeEvent } from 'wlp-client-common/events/TableResizeEvent';
import { dynamicSort, sortArrayItems } from 'wlp-client-common/utils/ArrayUtil';
import { useMainContext } from 'wlp-client-mypage/MainContext';
import { THEAD_HEIGHT } from 'wlp-client-common/consts/TableConsts';

const mapUtil = new MapUtil();
const localStorageUtil = new LocalStorageUtils();
const userService = new UserService();

class AccountStatusPersonalPanel extends Component {
    ROW_HEIGHT = 31;
    MAX_HEIGHT_TABLE_TEAM = 310;
    tableColumns = {
        name: {
            dataField: "name",
            headerText: i18n.t('mypage.personalSettings.accountStatus.team.name'),
            headerRenderer : "jp.co.kokuyo.wlp.mypage.component.renderer.DgHeaderRenderer",
            className: "noselect"
        },
        role: {
            dataField: "role",
            headerText: i18n.t('mypage.personalSettings.accountStatus.team.role'),
            headerRenderer: "jp.co.kokuyo.wlp.mypage.component.renderer.DgHeaderRenderer",
            itemRenderer: <TeamDgUserTypeRenderer />,
            width:100
        },
        map: {
            dataField: "map",
            headerText: i18n.t('mypage.personalSettings.accountStatus.team.map'),
            headerRenderer: "jp.co.kokuyo.wlp.mypage.component.renderer.DgHeaderRenderer",
            className: "noselect",
            minWidth: 300
        }
    }

    state = {
        teamList: [],
        teamInfoList:this.props.mainContext.teamInfoList,
        columns: [
            {
                title: this.props.t('mypage.personalSettings.accountStatus.team.name'),
                dataIndex: "name",
                width: 47,
                sortField: 'name'
            },
            {
                title: this.props.t('mypage.personalSettings.accountStatus.team.role'),
                dataIndex: "role",
                width:6.5,
                minWidth:100,
                sortField: 'roleLabel.value'
            },
            {
                title: this.props.t('mypage.personalSettings.accountStatus.team.map'),
                dataIndex: "map",
                width: 46.5,
                sortField: 'map'
            }
        ],
        selectedColumnIndex: null,
        dataTableList: null,
        maxHeight: 0
    }

    constructor(props) {
        super(props);
        this.dataGridRef = Element => {
            this.dataGrid = Element;
        }

        this.personalContentRef = React.createRef();
        this.initWidthTableColumns();
    }

    initWidthTableColumns = () => {
        const { widthTableColumns } = this.props;
        let tableColumns = {...this.tableColumns};
        if (Array.isArray(widthTableColumns) && widthTableColumns.length) {
            for (let i = 0; i < widthTableColumns.length; i++) {
                tableColumns[widthTableColumns[i].name] = {...tableColumns[widthTableColumns[i].name], width: widthTableColumns[i].width}
            }
        }

        this.tableColumns = tableColumns;
    }
    
    setStateDataTableList = (dataTableList) => {
        this.setState({dataTableList});
    }

    callGetJoinTeamStatus = async () => {
        let requestDto = new GetJoinTeamInfoConfigDto();
        let session = new SessionDto();
        let logonInfo = new LogonDto();
        logonInfo = localStorageUtil.getUserSession();
        session = logonInfo.sessionDto;

        requestDto = {
            ...requestDto,
            "contractId": session.contractId,
            "contractType": ContractType.personal,
            "currentState": CurrentState.CONTROL_PANEL,
            "localeCode": session.localeCode
        };

        const result = await userService.getJoinTeamStatus(requestDto);

        if (result) {
            this.getJoinTeamStatusResultHandler(result);
        }
    }

    getJoinTeamStatusResultHandler = (result) => {
        let response = new GetJoinTeamInfoDto();
        response.teamInfoList = result;
        this.setState({teamList: response.teamInfoList, teamInfoList: result.teamInfoList}, () => {
            this.dataTable();
        });
    }

    dataTable = () => {
        const { teamList } = this.state;
        if ( teamList && teamList.teamInfoList ) {
            return this.formatDataTable(teamList.teamInfoList);
        }
    };

    formatDataTable = (teamInfoList) => {

        const { t } = this.props;
        const formatTeamInfoList = teamInfoList.map((team, i) => {
            return {
                name: team.name,
                role: {
                    isShow: true, 
                    value: <span className="badge badge-bg-gray">{t(mapUtil.getPermissionLabelById(team.role))}</span>
                },
                map: team.map,
                roleLabel: {
                    isShow: false, 
                    value: t(mapUtil.getPermissionLabelById(team.role)),
                }
            }
        });
        this.setState({dataTableList: formatTeamInfoList});

        return formatTeamInfoList;
    }

    setStateSelectedColumnIndex = (index) => {
        this.setState({ selectedColumnIndex: index});
    }

    setStateDataColumns = (columns) => {
        this.setState({columns: columns});
    }

    componentDidMount() {
        // this.setMaxHeightTableResize();
        this.callGetJoinTeamStatus();
        tableResizeEvent.addListener(tableResizeEvent.TABLE_HEADER_CLICK, this.doSortTeamList);
        this.calculateTableSize();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.teamInfoList !== prevState.teamInfoList) {
            this.setMaxHeightTableResize();
        }
    }

    getDataGridWidth = () => {
        // personal-main-tlt
        let width = 0;
        const mainContent = document.querySelector('.personal-content');
        const leftContent = document.querySelector('.personal-content-l');
        if (mainContent  && leftContent ) {
            width = mainContent.offsetWidth - leftContent.offsetWidth - 21;
        }

        width -= 42; // content padding
        return width;
    }

    doSortTeamList = (event) => {
        if (event.index < 0 || event.target.nodeName !== 'TH') {
            return;
        }
        
        let { currentSortKey, currentSortDescending, teamInfoList } = this.state;
        const columnInfo = event.column;
        const softIndex = columnInfo.softIndex;

        if ( !teamInfoList || teamInfoList.length < 1) {
            return;
        }
        
        currentSortDescending = (parseInt(softIndex) === parseInt(currentSortKey)) ? !currentSortDescending : true;

        let params = {
            currentSortDescending: currentSortDescending,
            currentSortKey: softIndex,
            currentSortOrder: currentSortDescending ? 'DESC' : 'ASC'
        }
        let sortField = columnInfo.name;
        if (sortField !== 'name') {
            sortField = 'name';
        }

        let sortConfig = {
            key: columnInfo.name,
            direction: currentSortDescending ? 'DESC' : 'ASC',
            foreignKey: sortField,
            hasColumnID: false
        }

        let dataSort = sortArrayItems(teamInfoList, sortConfig);

        this.setState({
            ...params,
            teamInfoList: dataSort,
        }, () => { 
                this.dataGrid.setScroll('center');
        });


    }

    setMaxHeightTableResize = () => {
        const {teamInfoList} = this.state;
        if (Array.isArray(teamInfoList)) {
            // cộng 1 để không có thanh scroll khi số item không vượt quá 310 (max height)
            let maxHeight = teamInfoList.length * this.ROW_HEIGHT + 1;
            if (maxHeight > this.MAX_HEIGHT_TABLE_TEAM) {
                maxHeight = this.MAX_HEIGHT_TABLE_TEAM;
            }

            this.setState({maxHeight});
        }
    }

    onColumnResized = (widthTableColumns) => { 
        this.widthTableColumns = widthTableColumns;
        if (typeof this.props.setWidthTableColumns === "function") {
            this.props.setWidthTableColumns(widthTableColumns);
        }
    }

    /**
     * calculate table dimension and update state on window resize 
     */
    updateTableSizeOnWindowResizeHanlder=()=>{
        this.calculateTableSize();
    }

    /**
     * calculate table dimension and update state 
     */
    calculateTableSize=()=>{
        const tableContainer = document.querySelector('.account-status-table-content');
        let tableMaxHeight = 0;
        let tableMaxWidth = 0;
        if (tableContainer instanceof HTMLElement) {
            const tableBound = tableContainer.getBoundingClientRect();
            const styles = window.getComputedStyle(tableContainer);
            tableMaxHeight = tableContainer.offsetHeight - tableBound.top - parseInt(styles.paddingTop) - THEAD_HEIGHT;
            tableMaxWidth = tableContainer.offsetWidth - parseFloat(styles.paddingLeft) - parseFloat(styles.paddingRight);
            this.setState({tableMaxWidth,tableMaxHeight})
        }
    }

    render() {
        const { t } = this.props;
        const {
            currentSortOrder,
            currentSortKey,
            teamInfoList,
            tableMaxWidth
        } = this.state;
        return (<Fragment>
            <div id="personal-tabs3">
                <div className="personal-tabs-status personal-tabs-item">
                    <h2 className="presonal-title-tabs noselect noselect-color-777777">{t('mypage.personalSettings.accountStatus.team.title')}</h2>

                    <TableResize
                        ref={this.dataGridRef}
                        name="teamDg"
                        columns={this.tableColumns}
                        className="table-resizable"
                        dataProvider={teamInfoList}
                        sortOrder={currentSortOrder}
                        sortIndex={currentSortKey}
                        rowHeight={this.ROW_HEIGHT}
                        maxWidth={tableMaxWidth}
                        maxHeight={this.MAX_HEIGHT_TABLE_TEAM}
                        containerWidth={this.getDataGridWidth}
                        onWindowResizeHandler={this.updateTableSizeOnWindowResizeHanlder}
                    />

                </div>
            </div>
        </Fragment>);
    }
}

export default withTranslation()(useMainContext(AccountStatusPersonalPanel));