import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';

import GetUserInfoConfigDto from 'wlp-client-service/dto/user/GetUserInfoConfigDto';
import UserService from 'wlp-client-service/service/UserService';
import SessionDto from 'wlp-client-service/dto/SessionDto';
import LogonDto from 'wlp-client-service/dto/auth/LogonDto';
import ContractType from 'wlp-client-service/consts/ContractType';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import UserSelectImgModal from '../modals/UserSelectImgModal';
import ChangeUserInfoConfigDto from 'wlp-client-service/dto/user/ChangeUserInfoConfigDto';
import ValidateErrorMessage from 'wlp-client-common/component/message/ValidateErrorMessage';
import AuthService from 'wlp-client-service/service/AuthService';
import UpdateAvatarConfigDto from 'wlp-client-service/dto/user/UpdateAvatarConfigDto';
import rightArrowIcon from 'wlp-client-common/images/rightArrowIcon4.png';
import LocaleCode from "wlp-client-service/consts/LocaleCode";
import InputNamePs from './UserInfoPanel/InputNamePs';
import FileService from 'wlp-client-service/service/FileService';
import FileConfig from 'wlp-client-common/config/FileConfig';
import SaveUploadAvatarConfigDto from 'wlp-client-service/dto/user/SaveUploadAvatarConfigDto';
import SaveUploadAvatarDto from 'wlp-client-service/dto/user/SaveUploadAvatarDto';
import i18n from "i18n";
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import { useMainContext } from 'wlp-client-mypage/MainContext';
import Dropdown from 'wlp-client-common/component/Dropdown/Dropdown';
import {createLoadingMouse} from 'wlp-client-service/service/request';
import ActionLogger from 'wlp-client-common/ActionLogger';
import ImageDefault from 'wlp-client-common/utils/ImageDefault';

const userService = new UserService();
const localStorageUtil = new LocalStorageUtils();
const wlpDateUtil = new WlpDateUtil();
const authService = new AuthService();
const fileService = new FileService();
const languageUtils = new WlpLanguageUtil();

const USER_INFOS = {
    NICK_NAME: 'nick_name',
    FIRST_NAME: 'first_name',
    LAST_NAME: 'last_name',
    IMAGE_URL: 'image_url',
    LANGUAGE_SETTING: 'language_setting',
    TIME_ZONE: 'time_zone'
};
class UserInfoPanel extends Component {
    constructor(props) {
        super(props);
        const contextValue = this.props.mainContext.values;
        this.state = {
            userResult: contextValue,
            userInfo: {
                imageUrl:contextValue.imageUrl,
                nickName:contextValue.nickname,
                firstName:contextValue.firstName,
                lastName:contextValue.lastName,
                timezoneId: contextValue.timezoneId,
                languageSettingId: parseInt(contextValue.languageId),
                languageList: contextValue.languageList,
                timezoneList: contextValue.timezoneList
            },
            showUserSelectImgModal: false,
            validateField: {
                isError: false,
                messageError: '',
                firstName: true,
                lastName: true
            },
            file: '',
            showMessageSuccess: false,
            showDropdownLanguage: false,
            showDropdownTimezone: false,
            changeUserError: false,
            isFocusNickNameInput: false
        }
        this.bodyTable = React.createRef();
        this.timeOutSuccess = React.createRef();
    }

    callGetUserInfoData = async () => {
        let requestDto = new GetUserInfoConfigDto();
        let session = new SessionDto();
        let logonInfo = new LogonDto();
        logonInfo = localStorageUtil.getUserSession();
        session = logonInfo.sessionDto;

        requestDto = {
            ...requestDto,
            "contractId": session.contractId,
            "contractType": ContractType.personal,
            "currentState": CurrentState.CONTROL_PANEL,
            "localeCode": session.localeCode
        };

        if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
            return;
        }

        const result = await userService.getUserInfo(requestDto);

        if (result && result.isSucceed) {
            this.getUserInfoDataResultHandler(result);
        }
    };

    getUserInfoDataResultHandler = (result) => {

        const { languageList } = result;
        const { localeCode } = languageList[parseInt(result.languageId) - 1];
        languageUtils.setLocaleLanguage(localeCode);
        this.setState({
            userResult: result,
            userInfo: {
                imageUrl: result.imageUrl,
                nickName: result.nickname,
                firstName: result.firstName,
                lastName: result.lastName,
                timezoneId: result.timezoneId,
                languageSettingId: parseInt(result.languageId),
                languageList: result.languageList,
                timezoneList: result.timezoneList
            }
        });
    };



    showUserSelectImgModal = () => {
        ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_Select_Image`);
        this.setState({ showUserSelectImgModal: true });
    };

    hideUserSelectImgModal = () => {
        ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_Cancel_Select_Image`);
        this.setState({ showUserSelectImgModal: false });
    };

    setStateUserInfo = (type, value, index = null) => {
        ActionLogger.typing('User_Mypage_PersonalSetting_[ユーザー情報]', `Input_${type}`);
        switch(type) {
            case USER_INFOS.NICK_NAME:
                this.setState(({ userInfo }) => ({userInfo: {...userInfo, nickName: value}}));
                break;
            case USER_INFOS.FIRST_NAME:
                this.setState(({ userInfo }) => ({userInfo: {...userInfo, firstName: value}}));
                break;
            case USER_INFOS.LAST_NAME:
                this.setState(({ userInfo }) => ({userInfo: {...userInfo, lastName: value}}));
                break;
            case USER_INFOS.IMAGE_URL:
                this.setState(({ userInfo }) => ({userInfo: {...userInfo, imageUrl: value}}));
                break;
            case USER_INFOS.LANGUAGE_SETTING:
                this.handelShowDropdownLanguage();
                this.setState(({ userInfo }) => ({userInfo: {...userInfo, languageSettingId: value}}));
                break;
            case USER_INFOS.TIME_ZONE:
                this.handelShowDropdownTimezone();
                this.setState(({ userInfo }) => ({userInfo: {...userInfo, timezoneId: value}}));
                break;
            default:
                break;
        }
    }

    validateUserInfo = () => {
        const { userInfo } = this.state;
        const { t } = this.props;

        if (userInfo.firstName === "" && userInfo.lastName !== "") {
            this.setState({validateField: { 
                isError: true,
                firstName: false, 
                lastName: true,
                messageError: t('message.personal.e0006')}});
            return false;
        } else if (userInfo.firstName !== "" && userInfo.lastName === "") {
            this.setState({validateField: { 
                isError: true,
                firstName: true,
                lastName: false, 
                messageError: t('message.personal.e0005')}});
            return false;
        } else if (userInfo.firstName === "" && userInfo.lastName === "") {
            this.setState({validateField: { 
                isError: true,
                firstName: false,
                lastName: false, 
                messageError: t('message.personal.e0007')}});
            return false;
        } else {
            this.setState({validateField: { 
                isError: false,
                firstName: true,
                lastName: true, }});
            return true;
        }
    }

    callChangeUserInfo = async () => {
        ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_Change_User_Info`);
        const validateSuccess = this.validateUserInfo();
        
        if (validateSuccess) {
            const { userInfo } = this.state;
            let requestDto = new ChangeUserInfoConfigDto(); 
            let session = new SessionDto();
            let logonInfo = new LogonDto();
            logonInfo = localStorageUtil.getUserSession();
            session = logonInfo.sessionDto;
            
            requestDto.nickname = userInfo.nickName;
            requestDto.firstName = userInfo.firstName;
            requestDto.lastName = userInfo.lastName;
            requestDto.languageId = userInfo.languageSettingId;
            requestDto.timezoneId = userInfo.timezoneId;
            let accessToken = requestDto.accessToken;
    
            requestDto = {
                "accessToken": accessToken,
                "businessCategoryId": null,
                "companyName": null,
                "contractId": session.contractId,
                "contractType": ContractType.personal,
                "currentState": CurrentState.CONTROL_PANEL,
                "firstName": requestDto.firstName,
                "jobClassId": null,
                "languageId": parseInt(requestDto.languageId),
                "lastName": requestDto.lastName,
                "nickname": requestDto.nickname,
                "timezoneId": requestDto.timezoneId,
                "localeCode": session.localeCode
            };

            if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
                return;
            }

            const result = await userService.changeUserInfo(requestDto);
    
            if (result) {
                this.props.mainContext.setValuesUserInfo(requestDto)
                this.changeUserInfoResultHandler(result, requestDto.languageId);
            }
        } else {
            this.setState({changeUserError: true});
        }
    }

    checkBeforeUserLoggedOut = (accessToken) => {
        const { userInfo } = this.props;
        if (userInfo.accessToken !== accessToken) {
            window.location.reload();
            return false;
        }
        return true;
    }

    changeUserInfoResultHandler = (result, languageId) => {

        if (result.isSucceed) {
            const { setUserInfo } = this.props;
            const { userInfo } = this.state;
            const { languageList } = userInfo;
            const { localeCode } = languageList[languageId - 1];
            languageUtils.setLocaleLanguage(localeCode);
            authService.checkLoggedOn().then(userInfo => {
                setUserInfo(userInfo);
            })

            this.setState({showMessageSuccess: true}, () => {
                this.timeOutSuccess.current = setTimeout(() => { 
                    this.setState({showMessageSuccess: false}); 
                }, 4000);
            });
        }
    }

    handelShowDropdownLanguage = () => {
        this.setState(({ showDropdownLanguage }) => ({
            showDropdownLanguage: !showDropdownLanguage
        }));
        this.setState({showDropdownTimezone: false});
    }

    handelShowDropdownTimezone = () => {
        this.setState(({ showDropdownTimezone }) => ({
            showDropdownTimezone: !showDropdownTimezone
        }), () => {
            if (this.bodyTable && this.bodyTable.current !== null) {
                const { userInfo } = this.state;
                if (userInfo && userInfo.timezoneList) {
                    for (const index in userInfo.timezoneList) {
                        const time = userInfo.timezoneList[index];
                        if (userInfo.timezoneId === time.timezoneId) {
                            this.bodyTable.current.scrollTop = index * 30;
                            break;
                        }
                    }
                }
            }
        });
        this.setState({showDropdownLanguage: false});
    }

    setWrapperRefLanguage = (node) => {
        this.wrapperRefLanguege = node;
    }

    setWrapperRefTimezone = (node) => {
        this.wrapperRefTimezone = node;
    }

    handleClickOutsideDivLanguage = (event) => {
        if (this.wrapperRefLanguege && !this.wrapperRefLanguege.contains(event.target)) {
            this.setState({showDropdownLanguage: false});
        }
    }

    handleClickOutsideDivTimezone = (event) => {
        if (this.wrapperRefTimezone && !this.wrapperRefTimezone.contains(event.target)) {
            this.setState({showDropdownTimezone: false});
        }
    }

    getBtnLabelById = (resultList, id) => {
        if (resultList) {
            for (const result of resultList) {
                const btnId = result.timezoneId || result.id;
                if (btnId === id) {
                    return result.label;
                }
            }
        }
    }

    callUpdateAvatarImage = () => {

    }

    updateDefaultImageResultHandler = () => {

    }

    callSaveUploadAvatar = (s3Path) => {
        let requestDto = new SaveUploadAvatarConfigDto();
        const { sessionDto } = localStorageUtil.getUserSession();
        
        requestDto.userId = sessionDto.userId;
        requestDto.accessToken = sessionDto.sessionId;
        requestDto.s3Path = s3Path;
        userService.saveUploadAvatar(requestDto).then(this.callSaveUploadAvatar_hanelResult);
    }

    callSaveUploadAvatar_hanelResult = (result = new SaveUploadAvatarDto()) => {
        if(result.isSucceed){
            const { setUserInfo } = this.props;
            let userInfo = localStorageUtil.getUserSession();
            userInfo.smallImageUrl = result.smallImageUrl;
            userInfo.imageUrl = result.largeImageUrl;
            this.props.mainContext.setValuesUserInfo(userInfo);
            this.setState({userInfo: {...this.state.userInfo, imageUrl: result.largeImageUrl}});
            setUserInfo(userInfo);
        }
    }


    cancelBtnClickHandler = () => {
        ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_Cancel`);
        this.callGetUserInfoData();
        this.setState({
            validateField: {
                isError: false,
                messageError: '',
                firstName: true,
                lastName: true
            }
        })
    }

    changePictureBtnClickHandler = (img) => {
        ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_OK_Selected_Image`);
        let updateAvatarParams = new UpdateAvatarConfigDto();
        updateAvatarParams.avatarId = img;

        this.callUpdateAvatar(updateAvatarParams);
    }

    callUpdateAvatar = (paramsDto = new UpdateAvatarConfigDto()) => {

        userService.updateAvatar(paramsDto).then(res => {
            if(res.isSucceed){
                const { setUserInfo } = this.props;
                const userInfoUpdatedImage = {...this.props.userInfo, smallImageUrl: paramsDto.avatarId};
                setUserInfo(userInfoUpdatedImage);
                this.setState(({userInfo}) => {
                    return ({userInfo: {...userInfo, imageUrl: paramsDto.avatarId}});
                }, this.hideUserSelectImgModal);
            }
        });
    }

    uploadPictureBtnClickHandler = (e) => {
        ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_Upload_Picture`);
        e.preventDefault();
        let file = e.target.files[0];
        if(file){
            this.callUploadAvatarFile(file);
        }
    };

    callUploadAvatarFile = async (file) => {
        if(file.size > FileConfig.maxFileAvatarUploadSize) {
            return showWarningDialog('message.personal.e0001');
        }
        
        if(FileConfig.bannedFileNameChars.test(file.name)) {
            return showWarningDialog('editor.w0004');
        }
        
        if (!FileConfig.isFileExtValid.test(file.name)) {
            return showWarningDialog('message.personal.e0008');
        }
        

        let formData = new FormData();
        let  loadingMouse = createLoadingMouse();

        formData.append('file', file);
        loadingMouse.show();

        fileService.fileUploadForm(formData).then(uploadResult => {
            if (!uploadResult) {
                showWarningDialog('editor.w0003');
            } else {
                if (uploadResult.s3Path) {
                    this.callUploadAvatarFile_handleResult(uploadResult);
                }
            }
            
            loadingMouse.hide();
        });
        
    }

    callUploadAvatarFile_handleResult = (uploadResult) => {
        this.callSaveUploadAvatar(uploadResult.s3Path);
    }

    getTimeByLocalStorage = (time) => {
        const language = i18n.language;
        return wlpDateUtil.formatDate(time, language);
    }

    displayNameByLanguageSettings = () => {
        const { userInfo, validateField, changeUserError } = this.state;
        const { t } = this.props;
        const language = i18n.language;
        const labelFirstName = t('mypage.personalSettings.user.firstName');
        const labelLastName = t('mypage.personalSettings.user.lastName');
        const firstName = userInfo.firstName;
        const lastName = userInfo.lastName;
        const validateFirstName = validateField.firstName;
        const validateLastName = validateField.lastName;
        const infoFirstName = USER_INFOS.FIRST_NAME;
        const infoLastName = USER_INFOS.LAST_NAME;
        const languageFlag = language === LocaleCode.en_US;

        return (
            <InputNamePs 
                setChangeUserError={this.setChangeUserError}
                changeUserError={changeUserError}
                firstName={languageFlag ? firstName : lastName} 
                lastName={languageFlag ? lastName : firstName} 
                labelFirstName={languageFlag ? labelFirstName : labelLastName} 
                labelLastName={languageFlag ? labelLastName : labelFirstName}
                validateFirstName={languageFlag ? validateFirstName : validateLastName} 
                validateLastName={languageFlag ? validateLastName : validateFirstName} 
                plFirstName="" 
                plLastName=""
                infoFirstName={languageFlag ? infoFirstName : infoLastName} 
                infoLastName={languageFlag ? infoLastName : infoFirstName}
                setStateUserInfo={this.setStateUserInfo}
                validateUserInfo={this.validateUserInfo}
            />
        )
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutsideDivLanguage);
        document.removeEventListener("mousedown", this.handleClickOutsideDivTimezone);
        if(this.timeOutSuccess.current){
            clearTimeout(this.timeOutSuccess.current);
        }
    }

    componentDidMount() {
        this.callGetUserInfoData();
        document.addEventListener("mousedown", this.handleClickOutsideDivLanguage);
        document.addEventListener("mousedown", this.handleClickOutsideDivTimezone);
    }

    setChangeUserError = (isError) => {
        this.setState({changeUserError: isError});
    }

    onClickItemTimeZone = (index, timezone) => {
        this.setStateUserInfo(USER_INFOS.TIME_ZONE, timezone.timezoneId, index);
    }
    onBlurNickNameHandle = () => {
        this.setState({
            isFocusNickNameInput: false
        })
    }

    onFocusNickNameHandle = () => {
        this.setState({
            isFocusNickNameInput: true
        })
    }

    render() {
        const { 
            userInfo, 
            userResult, 
            showUserSelectImgModal, 
            validateField, 
            showDropdownLanguage,
            showMessageSuccess,
            isFocusNickNameInput } = this.state;
        const { t } = this.props;

        return (<Fragment>
            <div className="tab-pane fade show active ml-s3px" id="personal-tabs1" role="tabpanel" aria-labelledby="home-tab">
                <div className="personal-tabs-information personal-tabs-item">
                    <div className="d-flex">
                    <div className="w-150px mr-4 personal-uploadimg">
                        <div className="avatar-highlight">
                            <img src={
                                ImageDefault[userInfo.imageUrl] ?
                                ImageDefault[userInfo.imageUrl] :
                                userInfo.imageUrl
                            } alt="" className="avatar-content noselect" />
                        </div>
                        <div className="w-130px ml-sm-2">
                            <button onClick={() => this.showUserSelectImgModal()} 
                                    className="w-100 btn-select-image ml-4px personal-btn-select d-flex align-items-center mt-3 btn-sm" >
                                    <img src={rightArrowIcon} alt="" className="mr-3px noselect noselect-color-444" />
                                    <span className="text-select-image font-size-10-5 noselect noselect-color-white">{t('mypage.personalSettings.user.changePicture')}</span>
                            </button>
                            <label className="w-100 btn-select-image ml-4px personal-btn-select d-flex align-items-center pt-2 pb-2 btn-sm mt-2" htmlFor="upload-image-field">
                                <img src={rightArrowIcon} alt="" className="mr-3px noselect noselect-color-444" />
                                <span className="text-select-image font-size-10-5 noselect noselect-color-white">{t('mypage.personalSettings.user.upload')}</span>
                                <input onChange={(e) => this.uploadPictureBtnClickHandler(e)}
                                    type="file"
                                    value=""
                                    id="upload-image-field"
                                    accept="image/x-png,image/gif,image/jpeg,image/jpg"
                                    className="d-none" />
                            </label>
                        </div>
                        <div className="w-130px ml-sm-2">
                            <div className="w-100 ml-4px border-pi mt-3 pt-2 pb-2 font-per-gray pl-sm-2">
                                <p className="mb-0 noselect"><span className="icon-pi noselect">©</span> {t('mypage.personalSettings.user.lastLogin')} :</p>
                                <p className="mb-0 noselect"><span className="opacity-0 noselect">©</span> {userResult && this.getTimeByLocalStorage(userResult.lastLogonTime)}</p>
                                <div className="dropdown-divider" />
                                <p className="mb-0 noselect"><span className="icon-pi noselect">©</span> {t('mypage.personalSettings.user.menberSince')} :</p>
                                <p className="mb-0 noselect"><span className="opacity-0 noselect">©</span> {userResult && this.getTimeByLocalStorage(userResult.registerTime)}~</p>
                            </div>
                        </div>
                    </div>
                    <div className="flex-1 personal-form">
                        <div className="personal-form-el pt-7px">
                        {showMessageSuccess && <div className="alert alert-warning alert-mess-success custom-alert-warning noselect" role="alert">
                            {t('mypage.personalSettings.user.saveMessage')}
                        </div>}
                        <form>
                            <div className="w-360px w-form">
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1" className="font-weight-bold custom-text-mute fs-label-ps noselect noselect-color-9da1a5">
                                        {t('mypage.personalSettings.user.nickname')} :
                                    </label>
                                    <input type="text"
                                        value={userInfo.nickName}
                                        onChange={(e) => this.setStateUserInfo(USER_INFOS.NICK_NAME, e.target.value)}
                                        className={`form-control ${userInfo.nickName === ''?'input-empty-p':'form-control-shadow'} ${isFocusNickNameInput ? "" : " noselect"}`}
                                        aria-describedby="emailHelp"
                                        maxLength="64"
                                        placeholder=""
                                        onBlur={this.onBlurNickNameHandle}
                                        onFocus={this.onFocusNickNameHandle} />
                                </div>
                            </div>
                            <div className="w-330px w-form">
                                <div className="gr-form-group d-flex">
                                    {this.displayNameByLanguageSettings()}
                                </div>
                                {validateField.isError ? <ValidateErrorMessage classAdd="custom-mess-error position-static" message={validateField.messageError} /> : ''}
                                <div className="form-group mt-1rem">
                                    <label htmlFor="exampleInputPassword1" className="font-weight-bold custom-text-mute fs-label-ps noselect noselect-color-9da1a5">
                                        {t('mypage.personalSettings.user.email')} :
                                    </label>
                                    <p className="color-mail-p noselect">{userResult && userResult.mailaddress}</p>
                                </div>
                            </div>
                            <hr className="border-style-dotted" />
                            <div className="w-164px">
                                <div className="form-group dropdown dropdown-personal">
                                    <label className="font-weight-bold custom-text-mute fs-label-ps noselect noselect-color-9da1a5">
                                        {t('mypage.personalSettings.user.language')} :</label>
                                    <div ref={this.setWrapperRefLanguage}>
                                        <button className={`btn btn-secondary dropdown-toggle noselect ${showDropdownLanguage?'focus-btn-dropdown':''}`}
                                            type="button"
                                            onClick={() => this.handelShowDropdownLanguage()}>
                                            {   userInfo &&
                                                userInfo.languageList &&
                                                (this.getBtnLabelById(userInfo.languageList, userInfo.languageSettingId))
                                            }
                                        </button>
                                        { showDropdownLanguage && (
                                            <div className="dropdown-menu d-block">
                                                {
                                                    userInfo &&
                                                    userInfo.languageList &&
                                                    Array.isArray(userInfo.languageList) &&
                                                    userInfo.languageList.map((language, index) => {
                                                        return (
                                                            <div key={index}
                                                                onClick={() => this.setStateUserInfo(USER_INFOS.LANGUAGE_SETTING, language.id)}
                                                                className={`dropdown-item noselect ${ this.getBtnLabelById(userInfo.languageList, userInfo.languageSettingId) === language.label ? 'active':''}`}
                                                            >
                                                                {(language.label)}
                                                            </div>
                                                        );
                                                    })
                                                }
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="w-435px w-form">
                                <div className="form-group dropdown dropdown-personal">
                                    <label className="font-weight-bold custom-text-mute fs-label-ps noselect noselect-color-9da1a5">
                                        {t('mypage.personalSettings.user.timezone')} :</label>
                                    <div ref={this.setWrapperRefTimezone}>
                                        <Dropdown
                                            bodyClassName="dropdown-menu d-none"
                                            itemList={userInfo.timezoneList}
                                            buttonClassName="btn btn-secondary dropdown-toggle"
                                            activeButtonClassName="focus-btn-dropdown"
                                            className="dropdown-personal noselect"
                                            onClickItem={this.onClickItemTimeZone}
                                            selectedItemName={userInfo &&
                                                userInfo.timezoneList &&
                                                this.getBtnLabelById(userInfo.timezoneList, userInfo.timezoneId)
                                            }
                                        />
                                    </div>
                                </div>
                            </div>
                            <hr className="border-style-dotted" />
                            <button onClick={() => this.cancelBtnClickHandler()} 
                                type="button" 
                                className="btn btn-whitefl w-100px reset-border noselect noselect-color-464646">
                                {t('global.cancel')}
                            </button>
                            <button type="button" 
                                onClick={() => this.callChangeUserInfo()} 
                                className="btn btn-dark ml-10 h-36px reset-border noselect noselect-color-white">
                                {t('mypage.personalSettings.user.save')}
                            </button>
                        </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <UserSelectImgModal
                t={t}
                imageUrl={userInfo.imageUrl}
                isShow={showUserSelectImgModal}
                cancelHandler={this.hideUserSelectImgModal}   
                changePictureBtnClickHandler={this.changePictureBtnClickHandler}             
            />
        </Fragment>);
    }
}

export default withTranslation()(useMainContext(UserInfoPanel));