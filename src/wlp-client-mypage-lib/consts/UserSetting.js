export const PERSONAL_SETTINGS = {
    USER_INFOMATIONS: "User Information",
    PASSWORD: "Password",
    ACCOUNT_STATUS: "Acount status"
}

export const SEARCH_RESULT_TYPE = {
    MAPS: 'MAPS',
    SHEETS: 'SHEETS',
    ITEMS: 'ITEMS'
}

export const COUNT_DEFAULT = 3;
export const COUNT_DEFAULT_ITEM = 6;

export const DOWNLOAD_CSV_URL = 'https://dev2-user.kakiage.jp/uploadExportFile';

export const DEFAULT_ALL_RESULT = 50;