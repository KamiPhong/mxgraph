const TEAM_SETTINGS = {
    TEAM_INFOMATIONS: "User Information",
    MEMBER_LIST: "Member list",
    ADD_MEMBER: "Add member",
    ACCOUNT_STATUS: "Acount status"
}

export default {
    TEAM_SETTINGS
};