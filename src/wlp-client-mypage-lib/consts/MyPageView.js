export default {
    STATE_MAPS: 'maps',
    STATE_PERSONAL_SETTING: 'personalSetting',
    STATE_TEAM_SETTING: 'teamSetting',
}