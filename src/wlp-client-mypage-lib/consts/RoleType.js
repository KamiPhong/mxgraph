export default {
    LEADER: 1,
    ADMIN: 2,
    MEMBER: 3,
    PARTNER: 4,
    B_TO_C_OWNER: 5
}