import EventCenter from "wlp-client-common/events/EventCenter";


class MemberDgItemRendererEvent extends EventCenter {
    MAP_CLICK = "MAP_CLICK";
    MAP_CLOSE = "MAP_CLOSE";
    DELETE_CLICK = "DELETE_CLICK";
    
    constructor() {
        super();
        if (MemberDgItemRendererEvent.instance) {
            return MemberDgItemRendererEvent.instance;
        }

        MemberDgItemRendererEvent.instance = this;
    }
}

const memberDgItemRendererEvent = new MemberDgItemRendererEvent();

export { memberDgItemRendererEvent }