import EventCenter from "wlp-client-common/events/EventCenter";


class MemberDgLeaderRadioRendererEvent extends EventCenter {
    CHECK_BOX_ON_CHANGE = "CHECK_BOX_ON_CHANGE";

    constructor() {
        super();
        if (MemberDgLeaderRadioRendererEvent.instance) {
            return MemberDgLeaderRadioRendererEvent.instance;
        }

        MemberDgLeaderRadioRendererEvent.instance = this;
    }
}

const memberDgLeaderRadioRendererEvent = new MemberDgLeaderRadioRendererEvent();

export { memberDgLeaderRadioRendererEvent }