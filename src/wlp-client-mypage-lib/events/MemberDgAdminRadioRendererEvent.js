import EventCenter from "wlp-client-common/events/EventCenter";


class MemberDgAdminRadioRendererEvent extends EventCenter {
    ADMIN_SELECT = "ADMIN_SELECT";

    constructor() {
        super();
        if (MemberDgAdminRadioRendererEvent.instance) {
            return MemberDgAdminRadioRendererEvent.instance;
        }

        MemberDgAdminRadioRendererEvent.instance = this;
    }
}

const memberDgAdminRadioRendererEvent = new MemberDgAdminRadioRendererEvent();

export { memberDgAdminRadioRendererEvent }