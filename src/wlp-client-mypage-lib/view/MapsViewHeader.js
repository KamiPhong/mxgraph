import React from 'react';
import RefreshButtonSkin from 'wlp-client-common/component/paging/RefreshButtonSkin';
import NewMapModal from 'wlp-client-mypage-lib/components/modals/NewMapModal';
import newMapButton from 'wlp-client-common/images/icon/ic-newmapbutton.svg';
import newMapButtonDisabled from 'wlp-client-common/images/icon/newMapBtnDisable.png';
import rightArrowIconActive from 'wlp-client-common/images/icon/rightArrowIconActive.svg';
import rightArrowIcon from 'wlp-client-common/images/icon/rightArrowIcon.svg';
import leftArrowIconActive from 'wlp-client-common/images/icon/leftArrowIconActive.svg';
import leftArrowIcon from 'wlp-client-common/images/icon/leftArrowIcon.svg';
import navbarToggleIcon from 'wlp-client-common/images/icon/navbarToggleIcon.svg';
import Link from 'wlp-client-common/component/Link';
import ActionLogger from 'wlp-client-common/ActionLogger';
import EditorClient from "../../wlp-client-common/utils/EditorClientUtils";


class MapsViewHeader extends React.Component {
    state = {
        isShowNewMapModal: false
    }

    showNewMapModal = () => {
        ActionLogger.click('User_Mypage_TopMenu', 'Button_Create_Map');
        this.setState({ isShowNewMapModal: true })
    }

    hideNewMapModal = () => {
        this.setState({ isShowNewMapModal: false })
    }


    render() {
        const {
            callGetMapList,
            currentMapKind,
            currentView,
            selectedContractId,
            selectedCategoryId,
            callGetTemplateList,
            callCreateMap,
            teamList,
            toggleShowHidden_navRight,
            addStartPageNum,
            minusStartPageNum,
            totalMapsCount,
            startPageNum,
            endPageNum,
            selectedTeam,
            callGetCategoryList
        } = this.props;
        const { isShowNewMapModal } = this.state;
        
        const disablePrevius = startPageNum === 1;
        const disableNext = endPageNum >= totalMapsCount;
        const disableNewMapBtn = teamList.length === 0;

        let pagePreviosClasses = ['mypage-btn-action border-left border-secondary border-right-0 cursor-df min-width-32px'];
        let pageNextClasses = ['mypage-btn-action border-0 noselect cursor-df min-width-32px'];
        let currentPageClasses = ['noselect noselect-color-7f7f7f']

        if (EditorClient.IS_SAFARI) {
            currentPageClasses.push('fs-14px');
        }
        if (disablePrevius) {
            pagePreviosClasses.push('in-active');
        }
        if (disableNext) {
            pageNextClasses.push('in-active');
        } 
   
        return (<React.Fragment>
            {isShowNewMapModal && (
                <NewMapModal
                    selectedTeam={selectedTeam}
                    teamList={teamList}
                    cancelHandler={this.hideNewMapModal}
                    callCreateMap={callCreateMap}
                    isShow={isShowNewMapModal} />
            )}
            <div className="mypage-content-nav d-flex">
                <div className="mypage-btnaction d-flex align-items-center">
                    <Link disabled={disableNewMapBtn}  onClick={this.showNewMapModal} className="mypage-btn-action cursor-df">
                        <img src={disableNewMapBtn ? newMapButtonDisabled : newMapButton} alt="" className="img-fluid hover-opacity noselect" width="22" />
                    </Link>
                    <RefreshButtonSkin
                        callGetMapList={callGetMapList}
                        currentMapKind={currentMapKind}
                        currentView={currentView}
                        selectedContractId={selectedContractId}
                        selectedCategoryId={selectedCategoryId}
                        callGetTemplateList={callGetTemplateList}
                        callGetCategoryList={callGetCategoryList}
                    />
                </div>
                <div className="ml-auto mypage-pagition">
                    <div className="d-flex box-pagition float-left align-items-center">
                        <Link
                            onClick={e => !disablePrevius && minusStartPageNum()}
                            className={pagePreviosClasses.join(' ')}
                            >
                            <img src={
                                disablePrevius ?
                                    leftArrowIcon :
                                    leftArrowIconActive
                            } className="pagination-icon pagig-previos noselect margin-right-18px" alt="" />
                        </Link>
                        <div className="w-100 text-center box-pagition-nb no-wrap">
                            <span className={currentPageClasses.join(' ')}>{startPageNum}-{endPageNum} of {totalMapsCount ? totalMapsCount : 0}</span>
                        </div>
                        <Link
                            onClick={e => !disableNext && addStartPageNum()}
                            className={pageNextClasses.join(' ')}
                            >
                            <img src={
                                disableNext ?
                                    rightArrowIcon :
                                    rightArrowIconActive
                            } className="pagination-icon pagig-next noselect margin-left-18px" alt="" />
                        </Link>
                    </div>
                    <Link  onClick={toggleShowHidden_navRight} className="mypage-btn-action border-left border-secondary float-right no-border-right cursor-df">
                        <img className="noselect" src={navbarToggleIcon} alt="" />
                    </Link>
                </div>
            </div>
        </React.Fragment>)
    }
}

export default MapsViewHeader;