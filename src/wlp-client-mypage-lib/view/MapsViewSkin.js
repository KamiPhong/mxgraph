import React, { Fragment } from 'react';
import { withTranslation } from 'react-i18next';

import KindListRenderer from 'wlp-client-mypage-lib/components/renderer/KindListRenderer';
import MapListRenderer from 'wlp-client-mypage-lib/components/renderer/MapListRenderer';
import TemplateListRenderer from 'wlp-client-mypage-lib/components/renderer/TemplateListRenderer';
import { colorTeamDefault } from "wlp-client-common/Colors/teamColorList";
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import MapsViewHeader from './MapsViewHeader';
import GetMapListDto from 'wlp-client-service/dto/map/GetMapListDto';
import GetMapListConfigDto from 'wlp-client-service/dto/map/GetMapListConfigDto';
import MapListSearchConditionDto from 'wlp-client-service/dto/map/MapListSearchConditionDto';
import SessionDto from 'wlp-client-service/dto/SessionDto';
import LogonDto from 'wlp-client-service/dto/auth/LogonDto';
import MapKind from 'wlp-client-service/consts/MapKind';
import MapSortColumn from 'wlp-client-service/consts/MapSortColumn';
import PagingConfig from 'wlp-client-common/config/PagingConfig';
import ContractType from 'wlp-client-service/consts/ContractType';
import CurrentState from 'wlp-client-service/consts/CurrentState';
import MapService from 'wlp-client-service/service/MapService';
import GetTemplateListDto from 'wlp-client-service/dto/template/GetTemplateListDto';
import ViewTypes from 'wlp-client-mypage-lib/consts/ViewTypes';
import TemplateKind from 'wlp-client-service/consts/TemplateKind';
import GetCategoryListDto from 'wlp-client-service/dto/template/GetCategoryListDto';
import GetCategoryListConfigDto from 'wlp-client-service/dto/template/GetCategoryListConfigDto';
import TemplateService from 'wlp-client-service/service/TemplateService';
import CreateMapConfigDto from 'wlp-client-service/dto/map/CreateMapConfigDto';
import CreateMapDto from 'wlp-client-service/dto/map/CreateMapDto';
import CreateTemplateDto from 'wlp-client-service/dto/template/CreateTemplateDto';
import CreateTemplateConfigDto from 'wlp-client-service/dto/template/CreateTemplateConfigDto';
import GetMapRecordDetailConfigDto from 'wlp-client-service/dto/map/GetMapRecordDetailConfigDto';
import GetMapRecordDetailDto from 'wlp-client-service/dto/map/GetMapRecordDetailDto';
import RenameMapDto from 'wlp-client-service/dto/map/RenameMapDto';
import RenameMapConfigDto from 'wlp-client-service/dto/map/RenameMapConfigDto';
import MapRecordDto from 'wlp-client-service/dto/map/MapRecordDto';
import CopyMapDto from 'wlp-client-service/dto/map/CopyMapDto';
import CopyMapConfigDto from 'wlp-client-service/dto/map/CopyMapConfigDto';
import DeleteMapDto from 'wlp-client-service/dto/map/DeleteMapDto';
import DeleteMapConfigDto from 'wlp-client-service/dto/map/DeleteMapConfigDto';
import MapRecordDetailDto from 'wlp-client-service/dto/map/MapRecordDetailDto';
import TemplateRecordDetailDto from 'wlp-client-service/dto/template/TemplateRecordDetailDto';
import TemplateListSearchConditionDto from 'wlp-client-service/dto/template/TemplateListSearchConditionDto';
import GetTemplateListConfigDto from 'wlp-client-service/dto/template/GetTemplateListConfigDto';
import KindTypes from 'wlp-client-mypage-lib/consts/KindTypes';
import ChangeBookmarkConfigDto from 'wlp-client-service/dto/map/ChangeBookmarkConfigDto';
import ChangeBookmarkDto from 'wlp-client-service/dto/map/ChangeBookmarkDto';
import LocaleConst from 'wlp-client-common/consts/LocaleConst';
import GetTemplateRecordDetailConfigDto from 'wlp-client-service/dto/template/GetTemplateRecordDetailConfigDto';
import GetTemplateRecordDetailDto from 'wlp-client-service/dto/template/GetTemplateRecordDetailDto';
import DeleteShareMemberDto from 'wlp-client-service/dto/map/DeleteShareMemberDto';
import DeleteShareMemberConfigDto from 'wlp-client-service/dto/map/DeleteShareMemberConfigDto';
import AddShareMemberDto from 'wlp-client-service/dto/map/AddShareMemberDto';
import AddShareMemberConfigDto from 'wlp-client-service/dto/map/AddShareMemberConfigDto';
import MypageStateStorage from 'wlp-client-mypage/MypageStateStorage';
import i18n from "i18n";
import { openEditorPage } from 'wlp-client-common/utils/EdotorUtil';
import { showErrorDialog, showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import DeleteSharePartnerConfigDto from 'wlp-client-service/dto/map/DeleteSharePartnerConfigDto';
import CheckExistMapConfigDto from 'wlp-client-service/dto/map/CheckExistMapConfigDto';
import ResponseDto from 'wlp-client-service/dto/ResponseDto';
import MapSummary from "wlp-client-service/consts/MapSummary";
import ActionLogger from 'wlp-client-common/ActionLogger';
import { DEFAULT_ALL_RESULT } from 'wlp-client-mypage-lib/consts/UserSetting';
const { detect } = require('detect-browser');
const browser = detect();


const localStorageUtils = new LocalStorageUtils();
const mapService = new MapService();
const templateService = new TemplateService();

class MapsViewSkin extends React.Component {

    constructor(props) {
        super(props);
        const oldState = MypageStateStorage.getItem(MypageStateStorage.MYPAGE_STATE);

        this.state = oldState || {
            mapList: [],
            templateList: [], //element TemplateRecordDto
            categoryList: [], //element CategoryRecordDto
            currentView: ViewTypes.map,
            teamColor: {},
            teamList: [],
            mapSummary: {
                allNum: 0,
                bookmarksNum: 0,
                newMapsNum: 0,
                ownerByMeNum: 0,
                ownerByOtherNum: 0
            },
            currentMapKind: MapKind.all,
            currentTemplateKind: TemplateKind.all,
            selectedCategory: null,
            selectedTeam: null,
            selectedGroupBasic: MapKind.all,
            selectedCategoryId: -1,
            selectedMap: {},
            selectedMapDetail: {},
            currentActive: 1,
            isShowHiddenNavRight: false,
            selectedTemplate: {},
            totalMapsCount: 0,
            startPageNum: 1,
            endPageNum: PagingConfig.pageRecordNum,
            isCreateMapByTemplate: false,
            isReloadMapList: false,
            isReloadTemplateList: false,
            isShowTemplateDetail: true,
            selectTemplateId: null,
            selectedIndex: null
        }
    }

    setState(state, cb) {
        super.setState(state, () => {
            if (cb && typeof (cb) === 'function') {
                cb();
            }
            MypageStateStorage.setItem(MypageStateStorage.MYPAGE_STATE, this.state);
        });
    }

    componentDidMount() {
        const teamColorArr = localStorageUtils.getLocalStrage(localStorageUtils.TEAM_COLOR);
        const getCategoryListDto = new GetCategoryListConfigDto();
        const { currentView, selectedMapDetail } = this.state;

        if (currentView === ViewTypes.map) {
            const requestDto = this.createGetMapListDto();
            const requestList = [
                mapService.getMapList(requestDto),
                templateService.getCategoryList(getCategoryListDto)
            ];

            if(selectedMapDetail && selectedMapDetail.mapId){
                const getMapDetailDto = new GetMapRecordDetailConfigDto();
                getMapDetailDto.mapId = selectedMapDetail.mapId;
                requestList.push(mapService.getMapRecordDetail(getMapDetailDto));
            }

            Promise.all(requestList).then(([mapList, categoryList, mapDetail]) => {
                this.callGetMapList_handleResult(mapList);
                this.callGetCategoryList_handleResult(categoryList);

                if(mapDetail){
                    this.callGetMapRecordDetail_handlResult(mapDetail);
                }
            });
        } else {
            const requestDto = new GetTemplateListConfigDto();
            requestDto.searchCondition = this.createTemplateSearchCondition(1);
            templateService.getTemplateList(requestDto).then(this.callGetTemplateList_handleResult)
        }

        this.setColorTeamState(teamColorArr);
    }

    setMapList = mapList => {
        this.setState({ mapList: mapList, isReloadMapList: true }, () => {
            this.setState({ isReloadMapList: false });
        })
    }

    setMapSummary = (mapSummary) => {
        const {
            allNum,
            bookmarksNum,
            newMapsNum,
            ownerByMeNum,
            ownerByOtherNum
        } = mapSummary;

        this.setState({
            mapSummary: {
                allNum,
                bookmarksNum,
                newMapsNum,
                ownerByMeNum,
                ownerByOtherNum
            }
        });
    }

    setSelectedMapDetail = selectedMapDetail => {
        this.setState(() => ({ selectedMapDetail }));
    }

    setSelectedMap = mapRecord => {
        this.setState(() => ({ selectedMap: mapRecord }));
    }

    callMapPagination = ({ isNext = true }) => {
        const { startPageNum, selectedGroupBasic, selectedTeam } = this.state;
        let contractId = null;
        let mapKind = MapKind.all;

        if (selectedTeam) {
            contractId = selectedTeam;
        } else if (selectedGroupBasic) {
            mapKind = selectedGroupBasic;
        }
        let startNum = isNext ? startPageNum + PagingConfig.pageRecordNum : startPageNum - PagingConfig.pageRecordNum
        this.callGetMapList(mapKind, contractId, startNum);
    }

    callTemplatePagination = ({ isNext = true }) => {
        const { startPageNum, selectedCategoryId } = this.state;
        let startNum = isNext ? startPageNum + PagingConfig.pageRecordNum : startPageNum - PagingConfig.pageRecordNum
        this.callGetTemplateList(selectedCategoryId, startNum);

    }


    addStartPageNum = () => {
        ActionLogger.click('User_Mypage_TopMenu', 'Button_Next_Page');
        const { currentView } = this.state;

        if (currentView === ViewTypes.map) {
            this.callMapPagination({isNext: true});
        }else if(currentView === ViewTypes.template){
            this.callTemplatePagination({isNext: true});
        }
    }

    minusStartPageNum = () => {
        ActionLogger.click('User_Mypage_TopMenu', 'Button_Previous_Page');
        const { currentView } = this.state;
        if(currentView === ViewTypes.map){
            this.callMapPagination({isNext: false});
        }else if(currentView === ViewTypes.template){
            this.callTemplatePagination({isNext: false});
        }
    }

    // filter language here
    setCategoryList = categoryList => {
        const localeCode = i18n.language;
        for (let category of categoryList) {
            category.label = localeCode === LocaleConst.en_US ? category.englishLabel : category.japaneseLabel;
        }
        this.setState({ categoryList });
    }

    updateMapList = mapList => {
        this.setState(() => ({ mapList }));
    }

    setSelectedTemplate = (template, isShowTemplateDetail = true) => {
        const { categoryList } = this.state;
        if (template) {
            template.category = categoryList.find(category => category.id === template.categoryId);
        }
        this.setState(() => ({ selectedTemplate: template, isShowTemplateDetail }));
    }
    setSelectTemplateId = (selectTemId) => {
        
        this.setState(() => ({ selectTemplateId: selectTemId, }));
    }

    setCurrentKindTypes = (kindType, currentKind, selectedIndex) => {
        switch (kindType) {
            case KindTypes.MapKind:
                this.setStateCurrentKindTypes(currentKind, null, null);
                break;
            case KindTypes.TemplateKind:
                this.setStateCurrentKindTypes(null, currentKind, null);
                break;
            case KindTypes.TeamKind:
                this.setStateCurrentKindTypes(null, null, currentKind, selectedIndex);
                break;
            default:
                this.setStateCurrentKindTypes(MapKind.all, null, null);
                break;
        }
    }

    setStateCurrentKindTypes = (groupKind, categoryKind, teamKind, selectedIndex) => {
        this.setState(() => ({
            selectedGroupBasic: groupKind,
            selectedCategory: categoryKind,
            selectedTeam: teamKind,
            selectedIndex: selectedIndex
        }));
    }

    setRecentMaps = (mapId) => {
        //reload map list
        this.setMapList(this.state.mapList);
    }

    setTemplateList = templateList => {
        this.setState({ templateList: templateList, isReloadTemplateList: true }, () => {
            this.setState({ isReloadTemplateList: false });
        });
    }

    setCurrentView = currentView => {
        this.setState(() => ({ currentView }));
    }

    setTeamList = (teamList) => {
        // if (typeof this.props.updateUserSession === 'function') {
        //     this.props.updateUserSession();
        // }
        this.setState({ teamList });
    }

    setSelectedTeam = (teamList) => {
        const { selectedIndex } = this.state;
        if (selectedIndex !== undefined) {
            const selectedTeam = teamList[selectedIndex].contractId;
            this.setState({ selectedTeam });
        }
    }

    createMapSearchCondition = (startNum = this.state.startPageNum, mapKind = null, contractId = null, contractType = ContractType.team) => {
        const { selectedGroupBasic, selectedTeam } = this.state;
        let session = new SessionDto();
        let logonInfo = new LogonDto();
        let searchConditionDto = new MapListSearchConditionDto();


        if (mapKind === null && selectedGroupBasic) {
            mapKind = selectedGroupBasic;
        }

        if (contractId === null) {
            contractId = selectedTeam ? selectedTeam : -1;
        }

        if (mapKind === MapKind.recent) {
            startNum = 0;
        }else if(startNum < 1){
            startNum = 1;
        }

        logonInfo = localStorageUtils.getUserSession();

        if (!logonInfo) {
            return searchConditionDto;
        }

        session = logonInfo.sessionDto;

        searchConditionDto = {
            "contractId": contractId, //-1 to get all maps
            "contractType": contractType,
            "descending": true, // kiểu sắp xếp DSC và ASC
            "endNum": startNum + PagingConfig.pageRecordNum - 1, // số lượng record cần lấy mặc định là 50
            "mapIdList": null,
            "mapKind": mapKind ? mapKind : MapKind.all,
            "pageRecordNum": PagingConfig.pageRecordNum,
            "sortColumn": MapSortColumn.lastUpdateTime,
            "startNum": startNum,
            "teamId": null,
            "userId": session.userId // Id của user
        };

        if (searchConditionDto.mapKind === MapKind.recent) {
            searchConditionDto.mapIdList = localStorageUtils.getLocalStrage(localStorageUtils.RECENT_MAPS) || [];
        }

        return searchConditionDto;
    }

    rectFillColor = (contractId, colorTeam) => {
        ActionLogger.click('User_Mypage_ListCategory', `button_select_team_color`);
        const teamColorArr = localStorageUtils.getLocalStrage(localStorageUtils.TEAM_COLOR);
        const parseTeamColorArr = teamColorArr ? teamColorArr : {};
        parseTeamColorArr[contractId] = colorTeam;

        this.setColorTeamState(parseTeamColorArr);
        this.setColorTeamLocalStorage(parseTeamColorArr);
    }

    setColorTeamState = (data) => {
        let dataTeamColor = null;
        let objColorDefault = {};
        const { teamList } = this.state;

        if (data) {
            dataTeamColor = data;
        } else {
            const teamListsLength = teamList.length;
            for (let i = 0; i < teamListsLength; i++) {
                objColorDefault[teamList[i].contractId] = colorTeamDefault;
            }
            dataTeamColor = objColorDefault;
        }

        this.setState({ teamColor: dataTeamColor });
        this.setColorTeamLocalStorage(dataTeamColor);
    }

    setColorTeamLocalStorage = (data) => {
        localStorageUtils.setLocalStrage(localStorageUtils.TEAM_COLOR, data);
    }

    callGetMapRecordDetail = async (map) => {
        MypageStateStorage.setItem(MypageStateStorage.CURRENT_MAP, map);
        let requestDto = new GetMapRecordDetailConfigDto();
        requestDto.mapId = map.mapId;

        if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
            return;
        }

        const mapData = await mapService.getMapRecordDetail(requestDto);
        this.callGetMapRecordDetail_handlResult(mapData);
    }

    callGetSelectedMapRecordDetail = async (map) => {
        MypageStateStorage.setItem(MypageStateStorage.CURRENT_MAP, map);
        let requestDto = new GetMapRecordDetailConfigDto();
        requestDto.mapId = map.mapId;

        if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
            return;
        }

        const mapData = await mapService.getMapRecordDetail(requestDto);
        this.callGetMapRecordDetail_handlResult(mapData);
    }

    callGetMapRecordDetail_handlResult = (mapData = new GetMapRecordDetailDto()) => {
        if (!mapData.isSucceed) {
            this.callGetMapList(this.state.currentMapKind);
            return;
        }

        this.setState({
            selectedMapDetail: mapData.mapRecordDetail,
            selectedMap: mapData.mapRecordDetail,
        })
    }

    callCheckExistMap = async (mapId) => {
        let requestDto = new CheckExistMapConfigDto();
        requestDto.mapId = mapId;

        if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
            return;
        }

        const mapData = await mapService.checkExistMap(requestDto);
        this.callCheckExistMap_handlResult(mapData, mapId);
    }

    callCheckExistMap_handlResult = (mapData = new ResponseDto(), mapId) => {
        if (!mapData.isSucceed) {
            showWarningDialog(
                mapData.message,
                {
                    onOKCallback: () => {
                        this.callGetMapList(this.state.currentMapKind);
                    }
                }
            )
            return;
        }
        //setTimeout here to bypass popup blocker of safari
        if (browser.name === 'safari') {
            setTimeout(() => openEditorPage(mapId), 0)
        } else {
            openEditorPage(mapId)
        }
    }

    createGetMapListDto = (mapKind = null, contractId = -1, startNum, selectedIndex) => {

        let requestDto = new GetMapListConfigDto();
        const { selectedGroupBasic, selectedTeam } = this.state;

        this.props.setSelectedContractId(contractId);

        if (contractId === -1 && selectedTeam && !mapKind) {
            contractId = selectedTeam;
        } else if (contractId === null) {
            contractId = -1;
        }

        if (mapKind === null && selectedGroupBasic) {
            mapKind = selectedGroupBasic;
        }

        if (contractId === -1) {
            this.setCurrentKindTypes(KindTypes.MapKind, mapKind ? mapKind : selectedGroupBasic);
        } else {
            this.setCurrentKindTypes(KindTypes.TeamKind, contractId, selectedIndex);
        }

        requestDto.contractId = contractId;
        requestDto.searchConditionDto = this.createMapSearchCondition(startNum, mapKind);

        return requestDto;
    }

    callGetMapList = async (mapKind, contractId, startNum, selectedIndex) => {
        const requestDto = this.createGetMapListDto(mapKind, contractId, startNum, selectedIndex);

        if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
            return;
        }

        const result = await mapService.getMapList(requestDto);
        this.callGetMapList_handleResult(result, mapKind);
    }

    callGetMapList_handleResult = (result = new GetMapListDto(), mapKind) => {

        if (result.isSucceed) {
            const searchResult = result.searchResult;
    
            this.setSelectedMapAll(searchResult, result.firstRecordDetail);
            this.setCurrentView(ViewTypes.map);
            this.setMapList(searchResult.mapRecordList);
            this.setMapSummary(result.searchResult.mapSummary);
        }
    }

    setSelectedMapAll = (searchResult, mapRecordDetail) => {
        this.setSelectedMap(mapRecordDetail);
        this.setSelectedMapDetail(mapRecordDetail);
        this.setMapList(searchResult.mapRecordList);
        this.setMapSummary(searchResult.mapSummary);

        this.setTeamList(searchResult.mapSummary.teamHavingMapList);
        this.setCurrentView(ViewTypes.map);
        this.setState(() => ({
            totalMapsCount: searchResult.totalNum,
            startPageNum: searchResult.startNum,
            endPageNum: searchResult.endNum
        }));
    }

    callGetCategoryList = async () => {
        const requestDto = new GetCategoryListConfigDto();
        if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
            return;
        }
        const result = await templateService.getCategoryList(requestDto);
        this.callGetCategoryList_handleResult(result);
    }

    callGetCategoryList_handleResult = (result = new GetCategoryListDto()) => {
        this.setCategoryList(result.categoryList);
    }

    callGetTemplateList = (categoryId, startNum = 1) => {
        this.setCurrentKindTypes(KindTypes.TemplateKind, categoryId);
        this.setState(() => (
            { selectedCategoryId: categoryId }
        ), async () => {
            let requestDto = new GetTemplateListConfigDto();

            requestDto.searchCondition = this.createTemplateSearchCondition(startNum);
            requestDto.contractId = -1;
            if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
                return;
            }
            const result = await templateService.getTemplateList(requestDto);

            this.callGetTemplateList_handleResult(result);
        });
    }

    callGetTemplateList_handleResult = (result = new GetTemplateListDto()) => {
        const { selectedTemplate, selectedCategoryId, startPageNum } = this.state;
        if (
            (selectedTemplate && typeof(selectedTemplate.categoryId) === 'number') &&
            (result.firstRecordDetail) &&
            (selectedTemplate.categoryId === result.firstRecordDetail.categoryId) &&
            result.searchResult.startNum === startPageNum
        ) {
            for (let record of result.searchResult.templateRecordList) {
                if (record.templateId === selectedTemplate.templateId) {
                    this.setSelectedTemplate({
                        ...selectedTemplate, 
                        ...record
                    });
                    break;
                }
            }
        } else {
            this.setSelectedTemplate(result.firstRecordDetail);   
        }
        if (result.firstRecordDetail) {
            this.setSelectTemplateId(result.firstRecordDetail.templateId);
        }

        this.callGetCategoryList_handleResult(result.searchResult);
        this.setCurrentView(ViewTypes.template);
        this.setTemplateList(result.searchResult.templateRecordList);
        this.setCategoryList(result.searchResult.categoryList);
        let count = 0;
        for(let category of result.searchResult.categoryList){
            if(selectedCategoryId === category.id){
                count = category.count;
                break;
            }
        }

        this.setState(() => ({
            totalMapsCount: count,
            startPageNum: result.searchResult.startNum,
            endPageNum: result.searchResult.endNum
        }));
    }

    callDeleteShareMember = (targetUserId) => {
        const { selectedMap, startPageNum } = this.state;
        const paramDto = new DeleteShareMemberConfigDto();
        paramDto.contractId = -1;
        paramDto.deleteUserId = targetUserId;
        paramDto.currentState = CurrentState.MAP;
        paramDto.contractType = ContractType.team;
        paramDto.searchCondition = this.createMapSearchCondition(startPageNum, null, selectedMap.contractInfo.contractId);
        paramDto.mapId = selectedMap.mapId;

        mapService.deleteShareMember(paramDto).then(this.callDeleteShareMember_handleResult);
    }
    callDeleteShareMember_handleResult = (result = new DeleteShareMemberDto()) => {
        if (result.isSucceed) {
            this.setState({
                selectedMapDetail: result.mapRecordDetail,
                selectedMap: result.mapRecordDetail,
            })
        } else {
            showWarningDialog(
                result.messageKey,
                {
                    onOKCallback: () => {
                        this.callGetMapList();
                    }
                }
            )
        }
    }

    callAddShareMember = (targetUserId) => {
        const { selectedMap, startPageNum } = this.state;
        const paramDto = new AddShareMemberConfigDto();
        paramDto.contractId = selectedMap.contractInfo.contractId;
        paramDto.addUserId = targetUserId;
        paramDto.currentState = CurrentState.MAP;
        paramDto.contractType = ContractType.team;
        paramDto.searchCondition = this.createMapSearchCondition(startPageNum, null, selectedMap.contractInfo.contractId);
        paramDto.mapId = selectedMap.mapId;

        mapService.addShareMember(paramDto).then(this.addShareMemberResultHandler);
    }

    addShareMemberResultHandler = (result = new AddShareMemberDto()) => {
        if (!result.isSucceed) {
            showWarningDialog(
                result.messageKey,
                {
                    onOKCallback: () => {
                        this.callGetMapList();
                        return;
                    }
                }
            )
        }
        const { selectedMap } = this.state;
        this.callGetMapRecordDetail(selectedMap);
    }

    callDeleteSharePartner = (userId) => {
        const requestDto = new DeleteSharePartnerConfigDto();
        const { selectedMapDetail } = this.state;
        requestDto.contractId = selectedMapDetail.contractId;
        requestDto.mapId = selectedMapDetail.mapId;
        requestDto.deleteUserId = userId;
        requestDto.searchCondition = this.createMapSearchCondition();
        mapService.deleteSharePartner(requestDto).then(this.callDeleteSharePartner_handlerResult);
    }

    callDeleteSharePartner_handlerResult = (result = new DeleteShareMemberDto()) => {
        if (result.isSucceed) {
            this.setMapList(result.searchResult.mapRecordList);
            this.setMapSummary(result.searchResult.mapSummary);
            this.setSelectedMap(result.mapRecordDetail);
            this.setSelectedMapDetail(result.mapRecordDetail);
        } else {
            const { selectedMap } = this.state;
            showWarningDialog(
                result.messageKey,
                {
                    onOKCallback: () => {
                        this.callGetSelectedMapRecordDetail(selectedMap);
                    }
                }
            )
        }
    }

    callGetTemplateDetail = (templateId) => {
        if (this.state.selectedTemplate && templateId === this.state.selectedTemplate.templateId) {
            return;
        }

        for (let i = 0; i < this.state.templateList.length; i++) {
            if (this.state.templateList[i].templateId === templateId) {
                this.setSelectTemplateId(templateId);
                break;
            }
        }

        let paramDto = new GetTemplateRecordDetailConfigDto();
        paramDto.templateId = templateId;
        templateService
            .getTemplateRecordDetail(paramDto)
            .then(this.callGetTemplateDetail_handleResult);
    }

    callGetTemplateDetail_handleResult = (result = new GetTemplateRecordDetailDto()) => {
        const { categoryList } = this.state;
        let { templateRecordDetail } = result;
        if (templateRecordDetail) {
            templateRecordDetail.category = categoryList.find(category => category.id === templateRecordDetail.categoryId);
        }
        this.setSelectedTemplate(templateRecordDetail);
    }

    callCreateMap = async (newMapName = String(), templateRecord = null, contractId = Number(-1)) => {
        try {
            let paramDto = new CreateMapConfigDto();
            paramDto.newMapName = newMapName;
            paramDto.contractId = contractId;
            paramDto.templateRecord = templateRecord;
            paramDto.searchCondition = this.createMapSearchCondition(1, MapKind.all, contractId);

            if (!this.checkBeforeUserLoggedOut(paramDto.accessToken)) {
                return;
            }

            const result = await mapService.createMap(paramDto);
            this.callCreateMap_handleResult(result);
        } catch (e) {

        }
    }

    callCreateMap_handleResult = (result = new CreateMapDto()) => {
        if (result.isSucceed) {

            const { mapSummary } = this.state;
            const reloadMapSummary = { ...mapSummary, allNum: mapSummary.allNum + 1 };
            this.setCurrentView(ViewTypes.map);
            this.setState({ mapSummary: reloadMapSummary });
            this.setMapList(result.searchResult.mapRecordList);
            this.setCurrentKindTypes(KindTypes.TeamKind, result.newRecordDetail.contractInfo.contractId);
            this.setSelectedMap(result.newRecordDetail);
            this.setSelectedMapDetail(result.newRecordDetail);
            this.setTeamList(result.searchResult.mapSummary.teamHavingMapList);
            this.setMapSummary(result.searchResult.mapSummary);

            this.setState(() => ({
                totalMapsCount: result.searchResult.totalNum,
                startPageNum: result.searchResult.startNum,
                endPageNum: result.searchResult.endNum
            }));
        }
    }

    callRenameMap = async (newMapName, mapId) => {
        try {
            const { selectedMap } = this.state;
            let paramDto = new RenameMapConfigDto();
            paramDto.srcMapId = mapId;
            paramDto.newMapName = newMapName;
            paramDto.searchCondition = this.createMapSearchCondition();
            paramDto.currentState = CurrentState.MAP;
            paramDto.contractType = ContractType.team;
            paramDto.contractId = selectedMap.contractInfo.contractId;

            if (!this.checkBeforeUserLoggedOut(paramDto.accessToken)) {
                return;
            }

            const result = await mapService.renameMap(paramDto);

            this.callRenameMap_handleResult(result);
        } catch (e) {

        }
    }

    callRenameMap_handleResult = (result = new RenameMapDto()) => {
        this.setMapList(result.searchResult.mapRecordList);
        this.setSelectedMap(result.renameRecordDetail);
        this.setSelectedMapDetail(result.renameRecordDetail);
    }

    callCopyMap = async (copyMapName, selectedMap) => {
        try {
            let paramDto = new CopyMapConfigDto();
            paramDto.copyMapName = copyMapName;
            paramDto.srcMapId = selectedMap.mapId;
            paramDto.contractId = selectedMap.contractInfo.contractId;
            paramDto.searchCondition = this.createMapSearchCondition(1);
            if (!this.checkBeforeUserLoggedOut(paramDto.accessToken)) {
                return;
            }
            const result = await mapService.copyMap(paramDto);

            this.callCopyMap_handleResult(result);
        } catch (e) {

        }
    }

    callCopyMap_handleResult = (result = new CopyMapDto()) => {
        this.setMapList(result.searchResult.mapRecordList);
        this.setSelectedMap(result.copyRecordDetail);
        this.setSelectedMapDetail(result.copyRecordDetail);
        this.setTeamList(result.searchResult.mapSummary.teamHavingMapList);
        this.setMapSummary(result.searchResult.mapSummary);

        this.setState({
            totalMapsCount: result.searchResult.totalNum,
        })
    }

    callCreateTemplate = async (newTemplateSrc = new MapRecordDetailDto(), inputTemplateDetail = new TemplateRecordDetailDto()) => {
        let paramDto = new CreateTemplateConfigDto();

        paramDto.newTemplateSrc = newTemplateSrc;
        paramDto.newTemplateSrc.newFlg = false;
        paramDto.newTemplateSrc.updateFlg = false;

        paramDto.newTemplateSrc.templateTitle = inputTemplateDetail.templateTitle;
        paramDto.newTemplateSrc.templateCategoryId = inputTemplateDetail.categoryId;
        paramDto.newTemplateSrc.templateCategoryNameJapanese = inputTemplateDetail.japaneseLabel;
        paramDto.newTemplateSrc.templateCategoryNameEnglish = inputTemplateDetail.englishLabel;
        paramDto.newTemplateSrc.templateAuthor = inputTemplateDetail.author;
        paramDto.newTemplateSrc.templateReleaseDate = + new Date();

        paramDto.templateTitle = inputTemplateDetail.templateTitle;
        paramDto.templateCotegoryId = inputTemplateDetail.categoryId;
        paramDto.templateAuthor = inputTemplateDetail.author;
        paramDto.templatePublisher = inputTemplateDetail.templatePublisher;
        paramDto.templateDetail = inputTemplateDetail.detail;

        paramDto.searchCondition = this.createTemplateSearchCondition(1);
        paramDto.contractId = newTemplateSrc.contractInfo.contractId;
        paramDto.currentState = CurrentState.MAP;
        paramDto.contractType = ContractType.team;
        paramDto.searchCondition.categoryId = inputTemplateDetail.categoryId;

        if (!this.checkBeforeUserLoggedOut(paramDto.accessToken)) {
            return;
        }

        const result = await templateService.createTemplate(paramDto);

        this.callCreateTemplate_handleResult(result);
    }

    callCreateTemplate_handleResult = (result = new CreateTemplateDto()) => {
        if (result.isSucceed) {
            let isShowTemplateDetail = false;

            for(let template of result.searchResult.templateRecordList){
                if(template.templateId === result.newRecordDetail.templateId){
                    isShowTemplateDetail = true;
                    break;
                }
            }

            let categoryCount = 0;
            for(let category of result.searchResult.categoryList){
                if(result.newRecordDetail.categoryId === category.id){
                    categoryCount = category.count;
                    break;
                }
            }
            if (categoryCount > DEFAULT_ALL_RESULT) {
                isShowTemplateDetail = false;
            }

            this.setCurrentView(ViewTypes.template);
            this.setCategoryList(result.searchResult.categoryList);
            this.setSelectedTemplate(result.newRecordDetail,isShowTemplateDetail);
            this.setSelectTemplateId(result.newRecordDetail.templateId);
            this.setTemplateList(result.searchResult.templateRecordList);
            this.setCurrentKindTypes(KindTypes.TemplateKind, result.newRecordDetail.categoryId);

            this.setState(() => ({
                totalMapsCount: categoryCount,
                startPageNum: result.searchResult.startNum,
                endPageNum: result.searchResult.endNum
            }));
        }
    }

    createTemplateSearchCondition = (startNum = this.state.startPageNum) => {
        const { selectedCategoryId } = this.state;
        let paramDto = new TemplateListSearchConditionDto();
        paramDto.startNum = startNum;
        paramDto.endNum = startNum + PagingConfig.pageRecordNum - 1;
        paramDto.sortColumn = "templateTitle";

        if (selectedCategoryId) {
            paramDto.categoryId = selectedCategoryId;
        }
        paramDto.descending = false;
        paramDto.pageRecordNum = PagingConfig.pageRecordNum;
        paramDto.templateKind = null;
        return paramDto;
    }

    callDeleteMap = async (mapId) => {
        try {
            let paramDto = new DeleteMapConfigDto();
            paramDto.mapId = mapId;

            mapService.deleteMap(paramDto).then(this.callDeleteMap_handleResult);

        } catch (e) {

        }
    }

    callDeleteMap_handleResult = (result = new DeleteMapDto()) => {
        if(result.isSucceed){
            this.callGetMapList();
        }else {
            showWarningDialog(result.message, {onOKCallback: this.callGetMapList});
        }
    }

    callChangeBookmark = async (bookmarkChangeRecord = new MapRecordDto()) => {
        if (bookmarkChangeRecord === null) {
            return;
        }

        const requestDto = new ChangeBookmarkConfigDto();
        requestDto.mapId = bookmarkChangeRecord.mapId;
        requestDto.bookmark = !bookmarkChangeRecord.bookmark;

        if (!this.checkBeforeUserLoggedOut(requestDto.accessToken)) {
            return;
        }

        const result = await mapService.changeBookmark(requestDto);
        
        this.callChangeBookmark_handleResult(result, bookmarkChangeRecord);
    }

    callChangeBookmark_handleResult = (result = new ChangeBookmarkDto(), bookmarkChangeRecord) => {
        if (result.isSucceed) {
            const bookmarkChanged = bookmarkChangeRecord;
            bookmarkChanged.bookmark = !bookmarkChanged.bookmark;
            const mapSummary = this.state.mapSummary;
            let bookmarksNum = mapSummary[MapSummary.bookmaks];
            if (bookmarkChangeRecord.bookmark) {
                bookmarksNum += 1;
            } else {
                bookmarksNum -= 1;
            }
            mapSummary[MapSummary.bookmaks] = bookmarksNum;
            const mapList = [...this.state.mapList];
            const mapListLength = mapList.length;
            for (let i = 0; i < mapListLength; i++) {
                if (mapList[i].mapId === bookmarkChanged.mapId) {
                    mapList[i] = bookmarkChanged;
                    break;
                }
            }

            const selectedGroupBasic = this.state.selectedGroupBasic;
            if (selectedGroupBasic === MapKind.bookmarks) {
                for (let i = 0; i < mapListLength; i++) {
                    if (mapList[i].bookmark === false) {
                        mapList.splice(i, 1);
                        this.setSelectedMapDetail({});
                        break;
                    }
                }
            }

            this.setState({ 
                mapList,
                mapSummary
            });
        }
    }

    checkBeforeUserLoggedOut = (accessToken) => {
        const { userInfo } = this.props;
        if (userInfo.accessToken !== accessToken) {
            showErrorDialog('session.timeout', { showCancel: false, onOKCallback: () => {
                window.location.reload();
            } });
            return false;
        }
        return true;
    }

    toggleShowHidden_navRight = () => {
        ActionLogger.click('User_Mypage_TopMenu', 'Button_Toggle_Nav_Right');
        this.setState(({ isShowHiddenNavRight }) => ({
            isShowHiddenNavRight: !isShowHiddenNavRight
        }));
    };

    goToStartPage = () => {
        this.setState({
            startPageNum: PagingConfig.startNum,
            endPageNum: PagingConfig.pageRecordNum
        })
    }

    render() {
        const {
            mapList,
            templateList,
            currentView,
            teamList,
            mapSummary,
            currentMapKind,
            selectedGroupBasic,
            selectedCategory,
            selectedTeam,
            categoryList,
            selectedCategoryId,
            selectedMap,
            isShowHiddenNavRight,
            selectedMapDetail,
            selectedTemplate,
            totalMapsCount,
            startPageNum,
            endPageNum,
            isReloadMapList,
            isReloadTemplateList,
            isShowTemplateDetail,
            selectTemplateId
        } = this.state;

        const { classAdd, selectedContractId, userInfo } = this.props;
        const className = [classAdd, 'mypage-main'];
        return (<Fragment>
                <div className={className.join(' ')}>
                    <aside className="mypage-sidebar mypage-sidebar-left">
                        <KindListRenderer
                            currentMapKind={currentMapKind}
                            selectedGroupBasic={selectedGroupBasic}
                            selectedTeam={selectedTeam}
                            selectedCategory={selectedCategory}
                            callGetMapList={this.callGetMapList}
                            callGetTemplateList={this.callGetTemplateList}
                            teamList={teamList}
                            categoryList={categoryList}
                            mapSummary={mapSummary}
                            teamColor={this.state.teamColor}
                            rectFillColor={this.rectFillColor}
                            setCurrentKindTypes={this.setCurrentKindTypes}
                            colorTeamDefault={colorTeamDefault}
                            setCurrentView={this.setCurrentView}
                            setTemplateList={this.setTemplateList}
                            setSelectedTeam={this.setSelectedTeam} />
                    </aside>
                    <div className="mypage-content w-100 overflow-hidden">
                        <MapsViewHeader
                            selectedTeam={selectedTeam}
                            addStartPageNum={this.addStartPageNum}
                            minusStartPageNum={this.minusStartPageNum}
                            totalMapsCount={totalMapsCount}
                            startPageNum={startPageNum}
                            endPageNum={endPageNum}
                            toggleShowHidden_navRight={this.toggleShowHidden_navRight}
                            teamList={teamList}
                            callCreateMap={this.callCreateMap}
                            callGetMapList={this.callGetMapList}
                            currentMapKind={currentMapKind}
                            currentView={currentView}
                            selectedContractId={selectedContractId}
                            selectedCategoryId={selectedCategoryId}
                            callGetTemplateList={this.callGetTemplateList}
                            callGetCategoryList={this.callGetCategoryList}
                        />
                        {
                            currentView === ViewTypes.map ?
                                <MapListRenderer
                                    isReloadMapList={isReloadMapList}
                                    callDeleteSharePartner={this.callDeleteSharePartner}
                                    callAddShareMember={this.callAddShareMember}
                                    callDeleteShareMember={this.callDeleteShareMember}
                                    userInfo={userInfo}
                                    isShowHiddenNavRight={isShowHiddenNavRight}
                                    setSelectedMap={this.setSelectedMap}
                                    callRenameMap={this.callRenameMap}
                                    callCopyMap={this.callCopyMap}
                                    callDeleteMap={this.callDeleteMap}
                                    callGetMapRecordDetail={this.callGetMapRecordDetail}
                                    callChangeBookmark={this.callChangeBookmark}
                                    callCheckExistMap={this.callCheckExistMap}
                                    callCreateTemplate={this.callCreateTemplate}
                                    selectedMap={selectedMap}
                                    selectedMapDetail={selectedMapDetail}
                                    setRecentMaps={this.setRecentMaps}
                                    mapList={mapList}
                                    categoryList={categoryList}
                                    teamColor={this.state.teamColor}
                                    colorTeamDefault={colorTeamDefault}
                                    setSelectedMapDetail={this.setSelectedMapDetail}
                                    updateMapList={this.updateMapList}
                                    goToStartPage={this.goToStartPage} /> :
                                <TemplateListRenderer
                                    isShowTemplateDetail={isShowTemplateDetail}
                                    isReloadTemplateList={isReloadTemplateList}
                                    isShowHiddenNavRight={isShowHiddenNavRight}
                                    callGetTemplateDetail={this.callGetTemplateDetail}
                                    selectedTemplate={selectedTemplate}
                                    teamList={teamList}
                                    callCreateMap={this.callCreateMap}
                                    categoryList={categoryList}
                                    setCategoryList={this.setCategoryList}
                                    callGetTemplateList={this.callGetTemplateList}
                                    templateList={templateList}
                                    selectTemplateId={selectTemplateId} />
                        }
                    </div>
                </div>
        </Fragment>)
    }
}

export default withTranslation()(MapsViewSkin);

