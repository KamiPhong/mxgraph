import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import MyPageView from 'wlp-client-mypage-lib/consts/MyPageView';
import TeamSetting from 'wlp-client-mypage-lib/consts/TeamSetting';
import TeamInfoPanel from 'wlp-client-mypage-lib/components/team/TeamInfoPanel';
import AddMembersPanel from 'wlp-client-mypage-lib/components/team/AddMembersPanel';
import MemberListPanel from 'wlp-client-mypage-lib/components/team/MemberListPanel';
import AccountStatusTeamPanel from 'wlp-client-mypage-lib/components/team/AccountStatusTeamPanel';
import Link from 'wlp-client-common/component/Link';
import MypageStateStorage from 'wlp-client-mypage/MypageStateStorage';
import { useMainContext } from 'wlp-client-mypage/MainContext';
import ActionLogger from 'wlp-client-common/ActionLogger';


const { TEAM_SETTINGS } = TeamSetting;

class TeamSettingView extends Component {
    constructor(props){
        super(props);
        MypageStateStorage.setItem(MypageStateStorage.CURRENT_SETTING_CONTRACT, this.props.teamData);
    }
    
    state = {
        currentTeamSetting: TEAM_SETTINGS.TEAM_INFOMATIONS,
        memberList : []
    }

    setCurrentPersonalSettingKind = (currentView) => {
        this.setState({currentTeamSetting: currentView});
    };


    activeCurrentSettingView = () => {
        const { currentTeamSetting } = this.state;
        const { t, teamData, setTeamData } = this.props;

        switch(currentTeamSetting) {
            case TEAM_SETTINGS.TEAM_INFOMATIONS:
                return <TeamInfoPanel
                            t={t}
                            teamData={teamData}
                            setTeamData={setTeamData}
                        />
            case TEAM_SETTINGS.MEMBER_LIST:
                return <MemberListPanel
                            t={t}
                            teamData={teamData}
                            setDataMemberListPanel={this.setDataMemberListPanel}
                            setWidthTableColumns={this.setWidthTableColumnsMemberListPanel}
                            memberList={this.props.mainContext["pageMemberListPanel"].data}
                            widthTableColumns={this.props.mainContext["pageMemberListPanel"].widthTableColumns}
                        />
            case TEAM_SETTINGS.ADD_MEMBER:
                return <AddMembersPanel
                            t={t}
                            teamData={teamData}
                        />
            case TEAM_SETTINGS.ACCOUNT_STATUS:
                return <AccountStatusTeamPanel
                    t={t}
                    teamData={teamData}
                />
            default:
                return <TeamInfoPanel
                    t={t}
                    teamData={teamData}
                />
        }
    }

    componentDidUpdate({teamData}) {
        if(!Object.is(teamData, this.props.teamData)){
            this.setState({currentTeamSetting: TEAM_SETTINGS.TEAM_INFOMATIONS}, () => {
                this.activeCurrentSettingView();
            });
        }
    }

    componentDidMount() {

    }

    setDataMemberListPanel = (memberList) => {
        if (typeof this.props.mainContext["pageMemberListPanel"].setDataMemberListPanel === "function") {
            this.props.mainContext["pageMemberListPanel"].setDataMemberListPanel(memberList);
        }
    }

    setWidthTableColumnsMemberListPanel = (widthTableColumns) => {
        if (typeof this.props.mainContext["pageMemberListPanel"].setWidthTableColumns === "function") {
            this.props.mainContext["pageMemberListPanel"].setWidthTableColumns(widthTableColumns);
        }
    }

    render() {
        const { t, setMyPageView, classAdd, teamData } = this.props;
        const { currentTeamSetting } = this.state;
        const className = [classAdd, 'mypage-main'];
        return (<Fragment>
            <div className={className.join(' ')}>
                <div className="personal">
                    <div className="personal-main-tlt d-flex">
                        <h3 className="personal-tlt mb-0 text-truncate noselect">{teamData.teamName}</h3>
                        <div className="ml-auto">
                            <Link onClick={() => {
                                ActionLogger.click('User_Mypage_TeamSetting_[メンバーの追加]', 'Button_Back');
                                setMyPageView(MyPageView.STATE_MAPS);
                            }} className="btn btn-dark btn-rt-map">

                                <i className="fas fa-caret-right fa-fw noselect noselect-color-white" />
                                <span className="font-weight-bold font-size-12 noselect noselect-color-white">
                                    {t('mypage.personalSettings.back')}
                                </span>
                            </Link>
                        </div>
                    </div>
                    <div className= {`personal-content ${currentTeamSetting === TEAM_SETTINGS.MEMBER_LIST?" set-height-content":""}`}>
                        <div className="personal-content-l">
                            <div className="list-group list-group-tabs list-group-tabs-team" role="tablist">
                                <Link onClick={() => this.setCurrentPersonalSettingKind(TEAM_SETTINGS.TEAM_INFOMATIONS)}
                                    className={`list-group-item list-group-item-dark noselect ${currentTeamSetting === TEAM_SETTINGS.TEAM_INFOMATIONS?"active noselect-color-white":"noselect-color-585857"}`}>
                                    <i className={"fas fa-play-circle fa-fw noselect"+ (currentTeamSetting === TEAM_SETTINGS.TEAM_INFOMATIONS?"active noselect-color-white":"") }/> {t('mypage.teamSettings.teamInfomation')}
                                </Link>
                                <Link onClick={() => {
                                    ActionLogger.click('User_Mypage_TeamSetting_[チーム情報]', 'Button_View_Member_List');
                                    this.setCurrentPersonalSettingKind(TEAM_SETTINGS.MEMBER_LIST);
                                }} className={`list-group-item list-group-item-dark noselect ${currentTeamSetting === TEAM_SETTINGS.MEMBER_LIST?"active noselect-color-white":"noselect-color-585857"}`}>
                                    <i className={"fas fa-play-circle fa-fw noselect"+ (currentTeamSetting === TEAM_SETTINGS.MEMBER_LIST?"active noselect-color-white":"") }/> {t('mypage.teamSettings.memberList')}
                                </Link>
                                <Link onClick={() => this.setCurrentPersonalSettingKind(TEAM_SETTINGS.ADD_MEMBER)}
                                    className={`list-group-item list-group-item-dark noselect ${currentTeamSetting === TEAM_SETTINGS.ADD_MEMBER?"active noselect-color-white":"noselect-color-585857"}`}>
                                    <i className={"fas fa-play-circle fa-fw noselect"+ (currentTeamSetting === TEAM_SETTINGS.ADD_MEMBER?"active noselect-color-white":"") }/> {t('mypage.teamSettings.addMembers')}
                                </Link>
                                <Link onClick={() => {
                                    ActionLogger.click('User_Mypage_TeamSetting_[メンバーの追加]', 'Button_View_Account_Status');
                                    this.setCurrentPersonalSettingKind(TEAM_SETTINGS.ACCOUNT_STATUS);
                                }} className={`list-group-item list-group-item-dark fix-border-bottom noselect ${currentTeamSetting === TEAM_SETTINGS.ACCOUNT_STATUS?"active noselect-color-white":"noselect-color-585857"}`}>
                                    <i className={"fas fa-play-circle fa-fw noselect"+ (currentTeamSetting === TEAM_SETTINGS.ACCOUNT_STATUS?"active noselect-color-white":"") }/> {t('mypage.teamSettings.accountStatus')}
                                </Link>
                            </div>
                        </div>
                        <div className="personal-content-r personal-content-member">
                            <div className={`card shadow-lg rounded-lg w-100 ${currentTeamSetting === TEAM_SETTINGS.MEMBER_LIST?"fix-padding-map-list":"p-25px"}`}>
                                    { this.activeCurrentSettingView() }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>);
    }

}

export default withTranslation()(useMainContext(TeamSettingView));