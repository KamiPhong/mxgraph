import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import MyPageView from 'wlp-client-mypage-lib/consts/MyPageView';
import { PERSONAL_SETTINGS } from 'wlp-client-mypage-lib/consts/UserSetting';
import UserInfoPanel from 'wlp-client-mypage-lib/components/personal/UserInfoPanel';
import PasswordPanel from 'wlp-client-mypage-lib/components/personal/PasswordPanel';
import AccountStatusPersonalPanel from 'wlp-client-mypage-lib/components/personal/AccountStatusPersonalPanel';
import Link from 'wlp-client-common/component/Link';
import rightArrowIcon from 'wlp-client-common/images/rightArrowIcon4.png';
import Main from 'wlp-client-mypage/Main';
import { useMainContext } from 'wlp-client-mypage/MainContext';
import ActionLogger from 'wlp-client-common/ActionLogger';

class PersonalSettingView extends Component {

    state = {
        currentPersonalSettingKind: PERSONAL_SETTINGS.USER_INFOMATIONS,
    }

    setCurrentPersonalSettingKind = (currentView) => {
        this.setState({currentPersonalSettingKind: currentView});
    }

    activeCurrentSettingView = () => {
        const { currentPersonalSettingKind } = this.state;
        const { t, setUserInfo, userInfo } = this.props;
        const userInfoPanelProps = { userInfo, setUserInfo, t };

        switch(currentPersonalSettingKind) {
            case PERSONAL_SETTINGS.USER_INFOMATIONS:
                return <UserInfoPanel {...userInfoPanelProps}/>
            case PERSONAL_SETTINGS.PASSWORD:
                return <PasswordPanel t={t}/>
            case PERSONAL_SETTINGS.ACCOUNT_STATUS:
                return <AccountStatusPersonalPanel 
                t={t}
                setWidthTableColumns={this.setWidthTableColumnsAccountStatusPersonalPanel}
                widthTableColumns={this.props.mainContext[Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL].widthTableColumns}
                />
            default:
                return <UserInfoPanel {...userInfoPanelProps} />
        }
    }

    setWidthTableColumnsAccountStatusPersonalPanel = (widthTableColumns) => {
        if (typeof this.props.mainContext[Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL].setWidthTableColumns === "function") {
            this.props.mainContext[Main.PAGE_MEMBER_ACCOUNT_STATUS_TEAM_PANEL].setWidthTableColumns(widthTableColumns);
        }
    }

    componentDidMount() {
        
    }

    render() {
        const { t, setMyPageView, classAdd } = this.props;
        const { currentPersonalSettingKind } = this.state;
        const className = [classAdd, 'mypage-main'];
        return (<Fragment>
            <div className={className.join(' ')}>
                <div className="personal">
                    <div className="personal-main-tlt">
                        <h3 className="personal-tlt mb-0 font-size-24 noselect noselect-color-333">{t('mypage.headerMenu.personalSettings')}</h3>
                        <div className="ml-auto">
                            <Link onClick={() => {
                                ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', 'Button_Back');
                                setMyPageView(MyPageView.STATE_MAPS);
                            }} className="btn btn-dark btn-rt-map">
                                <img alt="" className="mr-5px noselect noselect-color-white" src={rightArrowIcon} width="6" /> 
                                <span className="font-weight-bold font-size-12 noselect noselect-color-white">
                                    {t('mypage.personalSettings.back')}
                                </span>
                            </Link>
                        </div>
                    </div>
                    <div className="personal-content"> 
                        <div className="personal-content-l">
                            <div className="list-group list-group-tabs w-250px" role="tablist">
                                <Link onClick={() => this.setCurrentPersonalSettingKind(PERSONAL_SETTINGS.USER_INFOMATIONS)} 
                                    className={`list-group-item pl-2 list-group-item-dark fs-label-ps noselect ${currentPersonalSettingKind === PERSONAL_SETTINGS.USER_INFOMATIONS?"active noselect-color-white":"noselect-color-585857"}`}>
                                    <i className={"fas fa-play-circle fa-fw font-size-12 noselect " + (currentPersonalSettingKind === PERSONAL_SETTINGS.USER_INFOMATIONS?"active noselect-color-white":"")} /> {t('mypage.personalSettings.userInfomation')}
                                </Link>
                                <Link onClick={() => {
                                    ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_View_Change_Password`);
                                    this.setCurrentPersonalSettingKind(PERSONAL_SETTINGS.PASSWORD);
                                }} className={`list-group-item pl-2 list-group-item-dark fs-label-ps ${currentPersonalSettingKind === PERSONAL_SETTINGS.PASSWORD?"active noselect-color-white":"noselect-color-585857"}`}>
                                    <i className={"fas fa-play-circle fa-fw font-size-12 noselect "+ (currentPersonalSettingKind === PERSONAL_SETTINGS.PASSWORD?"active noselect-color-white":"") }/> {t('mypage.personalSettings.password')}
                                </Link>
                                <Link onClick={() => {
                                    ActionLogger.click('User_Mypage_PersonalSetting_[ユーザー情報]', `Button_View_Account_Status`);
                                    this.setCurrentPersonalSettingKind(PERSONAL_SETTINGS.ACCOUNT_STATUS);
                                }} className={`list-group-item pl-2 list-group-item-dark fs-label-ps list-group-item-no-bb ${currentPersonalSettingKind === PERSONAL_SETTINGS.ACCOUNT_STATUS?"active noselect-color-white":"noselect-color-585857"}`}>
                                    <i className={"fas fa-play-circle fa-fw font-size-12 noselect " + (currentPersonalSettingKind === PERSONAL_SETTINGS.ACCOUNT_STATUS?"active noselect-color-white":"")} /> {t('mypage.personalSettings.accountStatus')}
                                </Link>
                            </div>
                        </div>
                        <div className="personal-content-r personal-content-member">
                            <div className={`card shadow-lg p-4 rounded-lg w-100 ${currentPersonalSettingKind === PERSONAL_SETTINGS.ACCOUNT_STATUS?"account-status-table-content":""}`}>
                                { this.activeCurrentSettingView() }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>);
    }

}

export default withTranslation()(useMainContext(PersonalSettingView));