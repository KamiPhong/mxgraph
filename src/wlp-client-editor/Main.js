import React from 'react';

//static files
import './assets/css/styles.css';

import MapService from 'wlp-client-service/service/MapService';
import GetMapConfigDto from 'wlp-client-service/dto/map/GetMapConfigDto';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import { KKGSocketIOServiceCenter } from 'wlp-client-service/service/messaging/Messaging';
import { showWarningDialog } from 'wlp-client-common/utils/DialogUtils';
import MainContext from './MainContext';
import MainSkin from './MainSkin';
import SheetEventCenter from 'wlp-client-editor-lib/events/SheetEventCenter';
import URLGetparams from 'wlp-client-common/utils/URLGetParams';
import Logger from 'wlp-client-common/Logger';
import { v4 as uuidv4} from 'uuid';
import { mxClient } from 'wlp-client-editor-lib/core/KClient';


const mapService = new MapService();
const strageUtils = new LocalStorageUtils();
const userInfo = strageUtils.getUserSession();
const sheetEventCenter = new SheetEventCenter();

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            commonUserInfo: null,
        }

        this.mapSocket = null;
        this.mapContainer = React.createRef();
        this.mainSkin = React.createRef();
        this.sheetId = null;
        this.itemId = null;
        this.debugConsoleRef = null;
        this.socketConnection = null
    }

    componentDidMount() {
        window.addEventListener('storage', this.handleLocalStorageChange)
        if (strageUtils.getHideLogout() === false) {
            strageUtils.setHideLogout(true);
            this.mapContainer.setHideLogoutFlag(true);
        } else {
            this.mapContainer.setHideLogoutFlag(false);
        }

        let urlParams = new URLGetparams(window.location.href);
        let {
            mapId,
            sheetId,
            itemId
        } = this.props.match.params;


        if (!sheetId) {
            sheetId = urlParams.getParam('sheetId');
        }

        if (!itemId) {
            itemId = urlParams.getParam('itemId');
        }

        if (!mapId) {
            mapId = urlParams.getParam('mapId');
        }

        this.sheetId = sheetId;
        this.itemId = itemId;
        // TODO Task 1056
        this.doGetMap(mapId);
        // TODO Task 1056

        this.initSheetEventHandlers();
        this.initItemEventHandlers();
        this.initBaseItemEventHandlers();
        this.initLineEventHandlers();
    }

    componentWillUnmount() {
        window.removeEventListener('storage', this.handleLocalStorageChange)
    }

    handleLocalStorageChange = () => {
        //[huyvq-bug_ominext_65932]--- destroy socket connection in all editor tab when user logout in other mypage/editor tab
        let localUss = strageUtils.getUserSession()
        if(!localUss) {
            this.socketConnection.destroy()
        } 
    }

    setupMapInfo = (currentMap) => {
        let teamName = "";
        const contractList = userInfo.contractList;
        for (let i = 0; i < contractList.length; i++) {
            const contract = contractList[i];
            if (contract.contractId === currentMap.contractId){
                teamName = contract.teamName;
                break;
            }
        }

        document.title = currentMap.name + "/" + teamName;
    }

    //sheet event handlers
    initSheetEventHandlers = () => {
        sheetEventCenter.addListener(sheetEventCenter.SHEET_SWITCH, this.switchSheetHandler);
        sheetEventCenter.addListener(sheetEventCenter.SHEET_ADD_NEW, this.sheetTabListAddHandler);
        sheetEventCenter.addListener(sheetEventCenter.SHEET_ADD_BY_COPY, this.sheetTabListAddByCopyHandler);
        sheetEventCenter.addListener(sheetEventCenter.SHEET_UPDATE, this.sheetTabListUpdateHandler);
        sheetEventCenter.addListener(sheetEventCenter.SHEET_MOVE, this.sheetTabListMoveHandler);
        sheetEventCenter.addListener(sheetEventCenter.SHEET_LOCK, this.sheetTabListLockHandler);
        sheetEventCenter.addListener(sheetEventCenter.SHEET_UNLOCK, this.sheetTabListUnlockHander);
        sheetEventCenter.addListener(sheetEventCenter.SHEET_REMOVE, this.sheetTabListRemoveHandler);
    }

    //item event handlers
    initItemEventHandlers = () => {
        sheetEventCenter.addListener(sheetEventCenter.ITEM_ADDED_NEW, this.addItemHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_ADDED_BY_COPY, this.addItemHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_LOCK_CHANGED, this.lockItemChangedHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_UPDATED, this.updateItemHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_REMOVED, this.removeItemHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_LIKE_CHANGED, this.likeItemChangedHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_UPLOAD_ATTACH_FILE, this.uploadItemAttachFileHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_SET_ATTACH_FILE_PASSWORD, this.settingPasswordAttachFileHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_CLEAR_ATTACH_FILE_PASSWORD, this.clearAttachFilePasswordHandler);
    }

    //base item event handlers
    initBaseItemEventHandlers = () => {
        sheetEventCenter.addListener(sheetEventCenter.BASE_ITEM_ADDED_NEW, this.addBaseItemHandler);
        sheetEventCenter.addListener(sheetEventCenter.BASE_ITEM_UPDATED, this.updateBaseItemHandler);
        sheetEventCenter.addListener(sheetEventCenter.BASE_ITEM_LOCK_CHANGED, this.lockBaseItemChangedHandler);
        sheetEventCenter.addListener(sheetEventCenter.BASE_ITEM_REMOVED, this.removeBaseItemHandler);
    }

    //line event handler
    initLineEventHandlers = () => {
        sheetEventCenter.addListener(sheetEventCenter.ITEM_LINE_ADD_NEW, this.addItemLineHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_LINE_UPDATED, this.updatedItemLineHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_LINE_REMOVE, this.removeItemLineHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_LINE_LOCKED, this.lockedItemLineHandler);
        sheetEventCenter.addListener(sheetEventCenter.ITEM_LINE_UNLOCKED, this.unlockedItemLineHandler);

    }

    /**
     * @author ThuyTV
     * @description call api and get map info by map id
     * @param {Number} mapId
     */
    doGetMap = (mapId ) => {
        const paramDto = new GetMapConfigDto();
        paramDto.mapId = mapId;
        mapService.getMap(paramDto)
            .then(this.getMapResult)
            .catch(error => {
                showWarningDialog('message.global.e0001');
            });
    }

    /**
     * @author ThuyTV
     * @param {GetMapDto} getMapDto
     * @return void
     */
    getMapResult = (getMapDto) => {
        if (getMapDto.isSucceed && getMapDto.map) {
            this.setupMapInfo(getMapDto.map);

            if(this.sheetId) {
                getMapDto.map.currentSheetId = parseInt(this.sheetId);
            }
            this.initMapSocket(getMapDto.map, () => {
                this.mapContainer.setMap(getMapDto.map);
            });
            // this.mapContainer.setMap(getMapDto.map);

            this.setState({
                commonUserInfo: {
                    contractType: getMapDto.contractType,
                    contractUserRole: getMapDto.contractUserRole,
                    currentDate: getMapDto.currentDate,
                    mapId: getMapDto.map.id
                }
            }, () => {
                this.mapContainer.setAuthority(this.state.commonUserInfo);
                strageUtils.setMapUserInfo(this.state.commonUserInfo);
            });
        } else {
            this.mapContainer.setHideLogoutFlag(false);
        }
    }

    initMapSocket = (map, socketInitWithSuccess) => {
        const socketCenter = new KKGSocketIOServiceCenter();
        this.socketConnection = socketCenter

        const { sessionDto } = userInfo;

        socketCenter.initWithPort(
            map.id,
            map.contractId,
            sessionDto.userId,
            sessionDto.sessionId,
            sessionDto.name,
            'Default1S', //TODO:[ThuyTV] fix hard code
            uuidv4()
        );

        //connect socket to server
        socketCenter.connectWithSuccess(() => {

            //bind socket event listener
            socketCenter.setSheetMessageReciever(
                this.sheetAdded, 
                this.sheetUpdated, 
                this.sheetRemoved, 
                this.sheetLockStateChanged, 
                this.sheetMoved, 
                this.sheetCopied
            );
            socketCenter.setItemMessageReciever(
                this.itemAdded, 
                this.itemUpdated, 
                this.itemRemoved, 
                this.itemLockStateChanged, 
                this.itemLikeStateChanged, 
                this.itemAttachFileUploaded,
                this.itemAttachFilePasswordChanged,
            );
            socketCenter.setBaseItemMessageReciever(
                this.baseItemAdded, 
                this.baseItemUpdated, 
                this.baseItemRemoved,
                this.baseItemLockStateChanged
            );
            socketCenter.setLineMessageReciever(
                this.lineAdded, 
                this.lineUpdated, 
                this.lineRemoved, 
                this.lineLockStateChanged
            );
            socketCenter.setEditorLoungeReciever(this.memberJoined, this.memberLeft, this.memberList);
            socketCenter.setErrorMessageReciever(this.onFault);


            if (typeof (socketInitWithSuccess) === 'function') {
                socketInitWithSuccess(socketCenter);
            }

        }, () => {
            showWarningDialog('Connect socket failed!');
        });

        this.mapSocket = socketCenter;
    }

    /******************************************** LOUNGE EVENTS *************************************************************** */

    /**
     * //TODO: migrate from flash
     * <p>メンバー参加のメッセージ受信処理。</p>
     * @param user 参加したユーザー情報
     *
     */
    memberJoined = (user, isMine, isFault) => {
        Logger.logConsole("memberJoined ", user);
        this.mapContainer.loungeMemberJoin(user, isMine, isFault);
    }


    /**
     * //TODO: migrate from flash
     * <p>メンバー退出のメッセージ受信処理。</p>
     * @param user 退出したユーザー情報
     *
     */
    memberLeft = (user) => {
        Logger.logConsole("memberLeft ", user);
        this.mapContainer.loungeMemberLeave(user);
        this.mapContainer.memberLeaveUnlockItems(user)
    }


    memberList = (users) => {
        this.mapContainer.loungeMemberList(users);
    }


    /******************************************** SHEET EVENTS *************************************************************** */

    /**
     * handle event when user adds new sheet
     */
    sheetTabListAddHandler = (sheetModel) => {
        if (this.mapSocket) {
            this.mapSocket.sendAddedNewSheet(sheetModel);
        }
    }

    /**
     * handle event when user copies a sheet
     */
    sheetTabListAddByCopyHandler = (sheetModel) => {
        if (this.mapSocket) {
            this.mapSocket.sendAddedByCopySheet(sheetModel);
        }
    }

    /**
     *  //TODO: migrate from flash
     * <p>シート追加時のメッセージ受信処理。</p>
     * @param sheet     追加されたシート情報
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     *
     */
    sheetAdded = (sheetModel, isMine, fault) => {
        Logger.logConsole("sheet added event ", sheetModel);
        this.mapContainer.sheetAdded(sheetModel, isMine, fault);
    }

    /**
     *  //TODO: migrate from flash
     * <p>シート追加時のメッセージ受信処理。</p>
     * @param sheet     追加されたシート情報
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     *
     */
    sheetCopied = (sheetModel, isMine, fault) => {
        this.mapContainer.sheetCopied(sheetModel, isMine, fault);
    }

    /**
     * //TODO: migrate from flash
     * <p>シート更新時のメッセージ受信処理。</p>
     * @param sheet     更新されたシート情報
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     */
    sheetUpdated = (sheetModel, isMine) => {
        this.mapContainer.sheetUpdated(sheetModel, isMine);
    }

    /**
     * handle event when user updates a sheet
     */
    sheetTabListUpdateHandler = (sheetModel) => {
        if (this.mapSocket) {
            this.mapSocket.sendUpdatedSheet(sheetModel);
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>シート削除時のメッセージ受信処理。</p>
     * @param sheet     削除されたシート
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     */
    sheetRemoved = (sheetModel, isMine) => {
        this.mapContainer.sheetRemoved(sheetModel, isMine);
    }

    /**
     * handle event when user removes a sheet
     */
    sheetTabListRemoveHandler = (sheetModel) => {
        this.mapContainer.beforeHandleRemoveSheet();
        if (this.mapSocket) {
            setTimeout(() => {
                this.mapSocket.sendRemovedSheet(sheetModel);
            }, 0);
        }    
    }

    /**
     * //TODO: migrate from flash
     * <p>シート順序を移動する。</p>
     * @param sheet     移動するシート情報
     * @param locked    
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     */
    sheetLockStateChanged = (sheet, locked, isMine) => {
        this.mapContainer.sheetLockStateChanged(sheet, locked, isMine);
    }

    /**
     * handle event when user locks a sheet
     */
    sheetTabListLockHandler = (sheetModel) => {
        if (this.mapSocket) {
            this.mapSocket.sendLockedSheet(sheetModel);
        }
    }

    /**
     * handle event when user unlocks a sheet
     */
    sheetTabListUnlockHander = (sheetModel) => {
        if (this.mapSocket) {
            this.mapSocket.sendUnlockedSheet(sheetModel);
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>シート順序移動時のメッセージ受信処理。</p>
     * @param ids       順に並んだシートIDの配列
     * @param isMine    自分の操作によって送信されたメッセージであることを示す
     *
     */
    sheetMoved = (ids, isMine) => {
        this.mapContainer.sheetMoved(ids, isMine);
    }

    /**
     * handle event when user moves a sheet
     */
    sheetTabListMoveHandler = (sheetModel) => {
        if (this.mapSocket) {
            this.mapSocket.sendMovedSheet(sheetModel);
        }
    }

    /**
     * switch sheet when user select other sheet
     * @param sheetId sheet id
     */
    switchSheetHandler = (sheetId) => {
        if (this.mapSocket) {
            this.mapSocket.sendSheetSwitchedByUser(sheetId);
        }
    }

    /******************************************** ITEM EVENTS *************************************************************** */


    /**
     * send add item message when create new items
     */
    addItemHandler = (items) => {
        if (this.mapSocket) {
            this.mapSocket.sendAddedNewItems(items);
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテム追加のメッセージ受信処理。</p>
     * @param models    追加されたアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemAdded = (models, isMine, isFault) => {
        this.mapContainer.itemAdded(models, isMine, isFault);
    }

    /**
     * send update item message
     */
    updateItemHandler = (items) => {
        if(Array.isArray(items) && items.length > 0){
            if (this.mapSocket) {
                this.mapSocket.sendUpdatedItems(items);
            }
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテム更新のメッセージ受信処理。</p>
     * @param models    更新があったアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemUpdated = (models, isMine) => {
        // Logger.logConsole({models, isMine});
        this.mapContainer.itemUpdated(models, isMine);
    }

    removeItemHandler = (items) => {
        this.mapSocket.sendRemovedItems(items);
    }

    removeBaseItemHandler = (items) => {
        this.mapSocket.sendRemovedBaseItem(items);
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテム削除のメッセージ受信処理。</p>
     * @param models    削除されたアイテムIDの配列
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemRemoved = (removedItemIds, isMine, fault) => {
        this.mapContainer.itemRemoved(removedItemIds, isMine, fault);
    }

    /**
     * send lock/unlock item message
     */
    lockItemChangedHandler = (items, lockFlag) => {
        if (this.mapSocket) {
            if (lockFlag === true) {
                this.mapSocket.sendLockedItems(items);
            } else if (lockFlag === false) {
                this.mapSocket.sendUnlockItems(items);
            }
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテムロック状態変更のメッセージ受信処理。</p>
     * @param models    ロック状態が変更されたアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    itemLockStateChanged = (models, isMine, fault) => {
        this.mapContainer.itemLockStateChanged(models, isMine, fault);
    }

    likeItemChangedHandler = (itemLikeModel) => {
        // console.log(itemLikeModel)
        if (this.mapSocket) {
            this.mapSocket.sendItemLikeModel(itemLikeModel);
        }
    }

    uploadItemAttachFileHandler = (itemModel, fileName, s3Path, virusCheckResult) => {
        if (this.mapSocket) {
            this.mapSocket.sendFileAttachedItem(itemModel, fileName, s3Path, virusCheckResult);
        }
    }

    settingPasswordAttachFileHandler = (itemModel, password) => {
        if (this.mapSocket) {
            this.mapSocket.setAttachFilePassword(itemModel, password);
        }
    }

    clearAttachFilePasswordHandler = (itemModel) => {
        if (this.mapSocket) {
            this.mapSocket.clearAttachFilePassword(itemModel);
        }
    }

    /**
     * //TODO: migrate from flash
     * <p>アイテムのgood状態変更のメッセージ受信処理。</p>
     * @param itemLikeModel    good状態の更新情報
     * @param isMine           自分の操作によって送信されたメッセージであることを示す。
     */
    itemLikeStateChanged = (itemLikeModel, isMine, fault) => {
        this.mapContainer.itemLikeStateChanged(itemLikeModel, isMine, fault);
    }

    itemAttachFileUploaded = (model, isMine) => {
        this.mapContainer.itemAttachFileUploaded(model, isMine);
    }

    itemAttachFilePasswordChanged = (model, isMine, fault) => {
        this.mapContainer.itemAttachFilePasswordChanged(model, isMine, fault);
    }
    /******************************************** ITEM BASE EVENTS *************************************************************** */
    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム追加のメッセージ受信処理。</p>
     * @param models    追加されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemAdded = (models, isMine, fault) => {
        this.mapContainer.baseItemAdded(models, isMine, fault);
    }

    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム更新のメッセージ受信処理。</p>
     * @param models    更新されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemUpdated = (models, isMine, fault) => {
        this.mapContainer.baseItemUpdated(models, isMine, fault);
    }

    /**
     * //TODO: migrate from flash
     * <p>ベースアイテム削除のメッセージ受信処理。</p>
     * @param removedItemIds    削除されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemRemoved = (removedItemIds, isMine, fault) => {
        this.mapContainer.baseItemRemoved(removedItemIds, isMine, fault);
    }


    /**
     * //TODO: migrate from flash
     * <p>ベースアイテムロック状態変更のメッセージ受信処理。</p>
     * @param models    ロック状態が変更されたベースアイテム
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    baseItemLockStateChanged = (models, isMine, fault) => {
        this.mapContainer.baseItemLockStateChanged(models, isMine, fault);
    }

    updateBaseItemHandler = (items) => {
        Logger.logConsole('Main => updateBaseItemHandler', { items })
        if(Array.isArray(items) && items.length > 0){
            if (this.mapSocket) {
                this.mapSocket.sendUpdatedBaseItems(items);
            }
        }
    }

    /**
     * send lock/unlock item message
     */
    lockBaseItemChangedHandler = (items, lockFlag) => {
        if (this.mapSocket) {
            if (lockFlag === true) {
                this.mapSocket.sendLockedBaseItems(items);
            } else if (lockFlag === false) {
                this.mapSocket.sendUnlockedBaseItems(items);
            }

        }
    }

    /******************************************** LINE EVENTS *************************************************************** */
    /**
     * //TODO: migrate from flash
     * <p>関係線作成のメッセージ受信処理。</p>
     * @param model     作成された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineAdded = (model, fault, isMine) => {
        this.mapContainer.lineAdded(model, fault, isMine);
    }

    /**
     * //TODO: migrate from flash
     * <p>関係線更新のメッセージ受信処理。</p>
     * @param model     更新された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineUpdated = (model, isMine, fault) => {
        this.mapContainer.lineUpdated(model, isMine, fault);
    }


    /**
     * //TODO: migrate from flash
     * <p>関係線削除のメッセージ受信処理。</p>
     * @param model     削除された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineRemoved = (model, isMine, fault) => {
        this.mapContainer.lineRemoved(model, isMine, fault);
    }

    /**
     * //TODO: migrate from flash
     * <p>関係線ロック状態変更のメッセージ受信処理。</p>
     * @param model     ロック状態が変更された関係線情報。
     * @param isMine    自分の操作によって送信されたメッセージであることを示す。
     */
    lineLockStateChanged = (model, isMine, fault) => {
        this.mapContainer.lineLockStateChanged(model, isMine, fault);
    }

    addItemLineHandler = (itemLine) => {
        if (this.mapSocket) {
            this.mapSocket.sendAddedNewRelationLine(itemLine);
        }
    }

    updatedItemLineHandler = (itemLine) => {
        if (this.mapSocket) {
            this.mapSocket.sendUpdatedRelationLine(itemLine);
        }
    }

    removeItemLineHandler = (itemLine) => {
        if (this.mapSocket) {
            this.mapSocket.sendRemovedRelationLine(itemLine);
        }
    }

    lockedItemLineHandler = (itemLine) => {
        if (this.mapSocket) {
            this.mapSocket.sendLockedRelationLine(itemLine);
        }
    }

    unlockedItemLineHandler = (itemLine) => {
        if (this.mapSocket && itemLine) {
            this.mapSocket.sendUnlockedRelatiolnLine(itemLine);
        }
    }

    /******************************** BASE ITEM EVENT ***************************************** */

    addBaseItemHandler = (baseItems) => {
        if (this.mapSocket) {
            this.mapSocket.sendAddedNewBaseItems(baseItems);
        }
    }

    updateBaseItemHandler = (baseItems) => {
        if (this.mapSocket) {
            this.mapSocket.sendUpdatedBaseItems(baseItems);
        }
    }

     /******************************** END BASE ITEM EVENT ***************************************** */

    
    onFault = (sendedData, isMine, fault) => {
        this.mapContainer.onFault(sendedData, isMine, fault);
    }

    updateMainSkinRef = (ref) => {
        if (ref) {
            this.mainSkin = ref;
            if (ref.mapContainer) {
                this.updateMapContainerRef(ref.mapContainer);
            }
        }
    }

    updateMapContainerRef = (ref) => {
        this.mapContainer = ref;
    }

    setDebugConsoleRef = (debugConsoleRef) => {//[huyvq] --- this console support for tablet (created by thuytv) will break the touch event on tablet
                                               // only uncomment if needed, then comment back again.
        this.debugConsoleRef = debugConsoleRef;
        const consoleLog = window.console.log;
        const addLog = (params) => {
            if (debugConsoleRef instanceof HTMLElement) {
                const logElm = document.createElement('p');
                logElm.innerHTML =  params.map(val => JSON.stringify(val)).join('|') ;
                debugConsoleRef.appendChild(logElm);

                debugConsoleRef.scrollTop = debugConsoleRef.scrollHeight;
            }
        }

        // Create a proxy for console.log
        window.console.log = function(...params) {
            addLog(params);
            consoleLog.apply(window.console, arguments);
        }
    }

    clearDebugConsole = () => {
        if (this.debugConsoleRef instanceof HTMLElement) {
            this.debugConsoleRef.innerHTML = '';
        }
    }

    render() {
        return (<MainContext.Provider value={{
            doFetchSheet: this.doFetchSheet,
            commonUserInfo: this.state.commonUserInfo
        }}>
            <MainSkin ref={this.updateMainSkinRef} />
            {/* Debugger for touch devices */}
            {process.env.NODE_ENV === 'development-' && mxClient.IS_TOUCH &&
                <div className="debug-console">
                    <button onClick={this.clearDebugConsole}>clear</button>
                    <div ref={this.setDebugConsoleRef}></div>
                </div>
            }
        </MainContext.Provider>)
    }

}

export default Main;