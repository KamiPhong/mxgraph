import React, { Component } from 'react';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';

import 'wlp-client-editor/assets/css/download.css';
const localStorage = new LocalStorageUtils();
class DownloadPage extends Component {

    constructor(props) {
        super(props);

        this.downloadResult = localStorage.getLocalStrage(localStorage.DOWNLOAD_RESULT);
        localStorage.setLocalStrage(localStorage.DOWNLOAD_RESULT, "")
    }

    beforeunload = e => { 
        localStorage.setLocalStrage(localStorage.DOWNLOAD_RESULT, this.downloadResult)
    }

    componentDidMount() {
        document.title = "";
        window.addEventListener("beforeunload", this.beforeunload);
    }

    renderDownloadPage = (downloadResult) => {
        const fileUrl = downloadResult.downloadUrl;
        return <div className="upload-exrport-page" >
            <p>
                以下のURLをクリックし、ダウンロードを行ってください。
                <br></br>
                <br/>
                セキュリティ上、このURLは1分間のみ有効です。<br/>
                期限切れの場合、再度ダウンロード/エクスポートを行ってください。
            </p>
            <p><a href={fileUrl}>ダウンロードする</a></p>
            <br></br>
            <p>
                <div className="mb-12px">Click on the download link below, please download</div>
                <span>This URL is valid for 1 minute for security reason.</span>
                <br/>
                <span>Please download / export again when expired.</span>
            </p>
            <p><a href={fileUrl}>GO! DOWNLOAD</a></p>
        </div>;
    }
    render() {
        return (
            this.downloadResult ? this.renderDownloadPage(this.downloadResult) : <div></div> 
        )
    }
} 


export default DownloadPage;