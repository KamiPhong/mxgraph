import React, { Component } from 'react';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';

import 'wlp-client-editor/assets/css/uploadExportPage.css';
const localStorage = new LocalStorageUtils();
class UploadExportPage extends Component {
    constructor(props) {
        super(props);

        this.uploadExportPageResult = localStorage.getLocalStrage(localStorage.UPLOAD_EXPORT_FILE_RESULT);
        localStorage.setLocalStrage(localStorage.UPLOAD_EXPORT_FILE_RESULT, "")
    }

    beforeunload = e => { 
        localStorage.setLocalStrage(localStorage.UPLOAD_EXPORT_FILE_RESULT, this.uploadExportPageResult)
    }

    componentDidMount() {
        window.addEventListener("beforeunload", this.beforeunload);
    }

    render() {
        return (
            <div className="upload-exrport-page" dangerouslySetInnerHTML={{ __html: this.uploadExportPageResult }} />
        )
    }
} 
export default UploadExportPage;