import React from 'react';

import pulldownIcon from 'wlp-client-common/images/icon/pulldownIcon.png';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import MainSkinContext from './MainSkinContext';

import EditorGuideDisplay from 'wlp-client-editor-lib/components/EditorGuideDisplay';
import MapContainer from 'wlp-client-editor-lib/components/MapContainer';
import ActionLogger from 'wlp-client-common/ActionLogger';

const strageUtils = new LocalStorageUtils();
const userInfor = strageUtils.getUserSession();
const skipTourEditor = strageUtils.getLocalStrage(strageUtils.SKIP_TOUR_EDITOR);

class MainSkin extends React.Component {
    constructor(props) {
        super(props);

        if ( userInfor) {
            this.logonDto = {
                smallImageUrl: userInfor.smallImageUrl,
                userName: userInfor.userName,
            };
        }
        

        this.state = {
            hideMenuClass: 'offcanvas offcanvas-editor',
            guideDisplay: !skipTourEditor
        };
        this.mapContainer = React.createRef();
    }

    tourViewCloseHandler = () => {
        ActionLogger.click('Mypage_TourGuide', 'Close_Button');

        this.setState({guideDisplay: false});
    }

    renderGuideDisplay = () => {
        const {guideDisplay} = this.state;

        return guideDisplay && <EditorGuideDisplay
        tourViewCloseHandler={this.tourViewCloseHandler}
        eventsLetsStartUsingSystem={this.tourViewCloseHandler} />
    }

    render() {
        const { hideMenuClass, guideDisplay } = this.state;
        return (
            <MainSkinContext.Provider value={{ logonDto: this.logonDto }}>
                <div className="wrapper wrapper-editor">
                    {this.renderGuideDisplay()}
                    <div className={`main main-editor ${hideMenuClass}`}>
                        {/**<MemoList pulldownIcon={pulldownIcon} toggleOpen={this.toggleOpenMenu} /> */}
                        <MapContainer
                            ref={mc => { this.mapContainer = mc; }}
                            guideDisplay={guideDisplay}
                            pulldownIcon={pulldownIcon}
                            toggleLockSheet={this.toggleLockSheet}/>

                    </div>
                </div>
            </MainSkinContext.Provider>);
    }
};

export default MainSkin;
