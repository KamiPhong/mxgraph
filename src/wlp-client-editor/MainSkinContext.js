import React from 'react';
import { createContext } from 'react';
const MainSkinContext = createContext({});


export const withMainSkinContext = Component => {
    return props => {
        return (
            <MainSkinContext.Consumer>
                {(context) => {
                    return <Component {...props} mainSkinContext={context}/>;
                }}
            </MainSkinContext.Consumer>
        );
    };
};

export default MainSkinContext;