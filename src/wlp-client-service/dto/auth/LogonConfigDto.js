class LogonConfigDto {
    mailaddress = null;
    password = null;
    localeCode = null;
    isSaveMailaddress = null;
}

export default LogonConfigDto;