import ResponseDto from 'wlp-client-service/dto/ResponseDto'

class LogonDto extends ResponseDto {
    sessionDto = String();
    /**
    * アイコン画像(小)URL
    */
    smallImageUrl = String();

    contractList = [];

    userName = String();

    userUrl = String();
    /** マイページURL */
    mypageUrl = String();

    /** エディタURL */
    editorUrl = String();

    editorSheetLinkUrl = String();

    /** ポータル個人無償登録 */
    portalIndividualUrl = String();

    portalFaqUrl = String();

    portalShortCutUrl = String();

    portalUpgradeUrl = String();

    kokuyoCopyrightUrl = String();

    kakiageHelpUrl = String();

    kokuyoTermsOfUseUrl = String();

    kokuyoPrivacyPolicyUrl = String();

    kakiageVersion = String();

    mailaddress = String();

    minimumVersion = Number();

    distributionDomain = String();

    /** 所属しているチームが存在するかどうか */
    existJoinTeam = Boolean();

    /** パスワード変更フラグ */
    changePassword = Boolean();

    /** iPad使用可能ライセンス */
    hasIpadLicense = Boolean();

    /** サーバのグローバルIPアドレス */
    globalIpAddress = String();
}

export default LogonDto;