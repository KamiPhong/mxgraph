import RequestDto from "wlp-client-service/dto/RequestDto";
import i18n from "i18n";

class CheckLoggedOnConfigDto extends RequestDto {
    localeCode = i18n.language;
}

export default CheckLoggedOnConfigDto;