class ContractDto {
    contractId = Number();

    teamId =  Number();

    teamName =  String();

    role =  Number();
}

export default ContractDto;