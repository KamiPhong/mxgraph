class SessionDto {
    /**
    * セッションID (ユーザーIDとタイムスタンプのSHA1ハッシュ)
    */
    sessionId = String();

    /**
    * ユーザーID
    */
    userId = Number();

    /**
    * 姓
    */
    lastName = String();

    /**
    * 名
    */
    firstName = String();

    /**
    * ニックネーム
    */
    nickName = String();

    /**
    * 選択している契約ID
    */
    contractId = Number();

    /**
    * 特別ユーザフラグ
    */
    specialUserFlag = Boolean();

    /**
    * ロケール
    */
    localeCode = String();

    /**
    * タイムゾーンID
    */
    timezoneId = String();

    /**
    * トークンストア
    *
    * <p>
    * キーは各サービスのドメインを逆にしたものから始めること。 com.twitter.accessToken のように
    * </p>
    */
    tokenStore = Object();

    /** ユーザーの最終ログアウト日時 */
    lastLogoutTime = Date();

    /**
     * IPアドレス
     */
    ipAddress = String();

    /**
     * UserAgent
     */
    userAgent = String();
}

export default SessionDto;