
import RequestDto from 'wlp-client-service/dto/RequestDto';

class DownloadAttachFileConfigDto extends RequestDto
{
    itemId = Number()
    contractId = Number()
    mapId = Number()
    filePassword = String()
}

export default DownloadAttachFileConfigDto;