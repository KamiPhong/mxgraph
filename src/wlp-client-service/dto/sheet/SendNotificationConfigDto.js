class SendNotificationConfigDto{

    users = [];

    message = String();

    sheetId = String();
}

export default SendNotificationConfigDto;