class FileUploadConfigDto{
    base64 = String();
    fileName = String();
}

export default FileUploadConfigDto;