import ResponseDto from "wlp-client-service/dto/ResponseDto";

class GetJoinTeamInfoDto extends ResponseDto
{
	teamInfoList = [];
}

export default GetJoinTeamInfoDto;
