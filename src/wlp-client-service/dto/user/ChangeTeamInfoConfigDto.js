import MypageRequestDto from "../MypageRequestDto";

class ChangeTeamInfoConfigDto extends MypageRequestDto
{
    /** ニックネーム */
    teamId = Number();

    /** ファーストネーム */
    teamName = String();

    /** ラストネーム */
    companyName = String();

    /** 企業名 */
    description = String();


}

export default ChangeTeamInfoConfigDto;
