import MypageRequestDto from "../MypageRequestDto";

class ChangeUserInfoConfigDto extends MypageRequestDto
{
    /** ニックネーム */
    nickname = String();

    /** ファーストネーム */
    firstName = String();

    /** ラストネーム */
    lastName = String();

    /** 企業名 */
    companyName = String();

    /** 業種ID */
    businessCategoryId = String();

    /** 部署名 */
    jobTitle = String();

    /** 職種ID */
    jobClassId = String();

    /** 電話番号 */
    telNo = String();

    /** 言語 */
    languageId = String();

    /** タイムゾーン */
    timezoneId = String();

}

export default ChangeUserInfoConfigDto;
