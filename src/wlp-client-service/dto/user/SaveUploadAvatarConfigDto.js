import MypageRequestDto from "../MypageRequestDto";

class SaveUploadAvatarConfigDto extends MypageRequestDto
{
    s3Path = String();

}

export default SaveUploadAvatarConfigDto;
