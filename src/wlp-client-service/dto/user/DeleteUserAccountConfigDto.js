import MypageRequestDto from "../MypageRequestDto";
import QuestionnaireConfigDto from "../questionnaire/QuestionnaireConfigDto";

class DeleteUserAccountConfigDto extends MypageRequestDto
{
    questionnaireConfigDto = new QuestionnaireConfigDto();
}

export default DeleteUserAccountConfigDto;
