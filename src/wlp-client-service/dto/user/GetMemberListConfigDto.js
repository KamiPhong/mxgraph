import MypageRequestDto from "../MypageRequestDto";

class GetMemberListConfigDto extends MypageRequestDto
{
    teamId = Number();
}

export default GetMemberListConfigDto;
