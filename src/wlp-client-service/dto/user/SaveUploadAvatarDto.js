import ResponseDto from "wlp-client-service/dto/ResponseDto";

class SaveUploadAvatarDto extends ResponseDto
{
    largeImageUrl = String();

    smallImageUrl = String();

}

export default SaveUploadAvatarDto;