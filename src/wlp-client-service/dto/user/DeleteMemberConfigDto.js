import MypageRequestDto from "../MypageRequestDto";
import MemberDgDto from "../team/MemberDgDto";

class DeleteMemberConfigDto extends MypageRequestDto
{
    deleteMember = new MemberDgDto();
    teamId = Number();
}

export default DeleteMemberConfigDto;
