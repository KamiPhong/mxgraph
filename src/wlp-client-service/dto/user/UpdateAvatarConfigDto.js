import MypageRequestDto from "../MypageRequestDto";

class UpdateAvatarConfigDto extends MypageRequestDto
{
    /** "Default1"など */
    avatarId = String();
}

export default UpdateAvatarConfigDto;
