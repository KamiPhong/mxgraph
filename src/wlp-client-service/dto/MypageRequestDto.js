import RequestDto from "wlp-client-service/dto/RequestDto";
import i18n from 'i18n';
import MypageStateStorage from "wlp-client-mypage/MypageStateStorage";


class MypageRequestDto extends RequestDto {

    constructor() {
        super();
        this.localeCode = i18n.language;
        this.contractId = MypageStateStorage.getRequestContractId();
        this.currentState = MypageStateStorage.getRequestCurrentState();
        this.contractType = MypageStateStorage.getRequestContractType();
    }

    /** ロケールコード "en_US", "ja_JP" */
    localeCode = String();

    /** 契約ID */
    contractId = Number();

    /** 契約タイプ "personal", "team" */
    contractType = String();

    /** 操作している画面の分類 "maps", "templates", "controlPanel" */
    currentState = String();
}

export default MypageRequestDto;