import RequestDto from "wlp-client-service/dto/RequestDto";


/**
 * パスワード変更リクエストDTO
 */
class LoginChangePasswordConfigDto extends RequestDto{

    /** 現在のパスワード */
    currentPassword = String();

    /** 新しいパスワード */
    newPassword = String();
}

export default LoginChangePasswordConfigDto;