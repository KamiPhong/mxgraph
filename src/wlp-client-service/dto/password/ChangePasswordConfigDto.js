import MypageRequestDto from "../MypageRequestDto";

class ChangePasswordConfigDto extends MypageRequestDto {

    /** ユーザID */
    userId = Number();

    /** 現在のパスワード */
    currentPassword = String();

    /** 新しいパスワード */
    newPassword = String();

}

export default ChangePasswordConfigDto;