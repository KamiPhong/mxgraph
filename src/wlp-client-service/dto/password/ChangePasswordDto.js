import ResponseDto from 'wlp-client-service/dto/ResponseDto'

/**
 * パスワード変更レスポンスDTO
 */
class ChangePasswordDto extends ResponseDto {}

export default ChangePasswordDto;