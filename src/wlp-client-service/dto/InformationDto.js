class InformationDto {

    /** 情報パネルに表示するテキスト(ja_JP) */
    informationContentJp = [];

    /** 情報パネルに表示するテキスト(en_US) */
    informationContentEn = [];

    /** 情報パネルに表示するテキストの更新日(ja_JP) */
    informationDateJp = [];

    /** 情報パネルに表示するテキストの更新日(en_US) */
    informationDateEn = [];
}

export default InformationDto;