import MypageRequestDto from "../MypageRequestDto";
import MemberDto from "./MemberDto";

class GetMemberMapListConfigDto extends MypageRequestDto
{
    targetMember = new MemberDto();
}
export default GetMemberMapListConfigDto;

