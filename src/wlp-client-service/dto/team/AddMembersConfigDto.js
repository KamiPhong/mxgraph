import MypageRequestDto from "../MypageRequestDto";

class AddMembersConfigDto extends MypageRequestDto
{
    /** チームID */
    teamId = Number();

    /** Eメールリスト */
    emailList = [];
    
    /** 追加メッセージ */
    optionalMsg = String();

}

export default AddMembersConfigDto;
