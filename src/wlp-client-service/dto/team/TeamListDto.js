import ResponseDto from 'wlp-client-service/dto/ResponseDto'

class TeamListDto extends ResponseDto {

    teamName = String();

    updateMap = Boolean();

    companyId = Number();

    teamId = Number();

    contractId = Number();

    mapCount = Number();

    ipControlFlag = Boolean();

}

export default TeamListDto;