import MypageRequestDto from "../MypageRequestDto";

class GetTeamAccountStatusConfigDto extends MypageRequestDto
{
    teamId = Number();

}
export default GetTeamAccountStatusConfigDto;

