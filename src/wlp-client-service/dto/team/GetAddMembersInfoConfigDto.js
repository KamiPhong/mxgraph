import MypageRequestDto from "../MypageRequestDto";

class GetAddMembersInfoConfigDto extends MypageRequestDto
{
    teamId = Number();
}

export default GetAddMembersInfoConfigDto;