import MypageRequestDto from "../MypageRequestDto";

class GetTeamInfoConfigDto extends MypageRequestDto
{
    teamId = Number();
}

export default GetTeamInfoConfigDto;