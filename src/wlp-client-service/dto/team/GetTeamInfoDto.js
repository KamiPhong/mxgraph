import ResponseDto from "wlp-client-service/dto/ResponseDto";

class GetTeamInfoDto extends ResponseDto
{
    teamName = String();

    companyId = Number();

    companyName = String();

    description = String();

    createdIn = Date();

}

export default GetTeamInfoDto;