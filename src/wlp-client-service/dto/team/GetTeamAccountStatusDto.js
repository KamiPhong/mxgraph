import ResponseDto from "wlp-client-service/dto/ResponseDto";

class GetTeamAccountStatusDto extends ResponseDto
{
    /** ストレージ使用量 */
    diskUsed = Number();

    /** 最大ストレージ使用量 */
    diskMax = Number();

    /** ユーザー数 */
    userAdded = Number();

    /** 最大ユーザー数 */
    userMax = Number();

}
export default GetTeamAccountStatusDto;
