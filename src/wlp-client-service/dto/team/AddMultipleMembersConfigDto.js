import MypageRequestDto from "../MypageRequestDto";

class AddMultipleMembersConfigDto extends MypageRequestDto
{
    /** チームID */
    teamId = Number();

	/** \nで区切られたEmailアドレスリストの文字列 */
    emailList = String();

    /** 追加メッセージ */
    optionalMsg = String();

}
export default AddMultipleMembersConfigDto;