import ResponseDto from "wlp-client-service/dto/ResponseDto";

class AddMultipleMembersDto extends ResponseDto
{
    /** ユーザー数 */
    currentMemberNum = Number();

    /** 登録でErrorとなったEmailアドレスのString配列 */
    errorEmailList = [];

}

export default AddMultipleMembersDto;