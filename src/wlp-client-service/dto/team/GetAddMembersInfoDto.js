import ResponseDto from "wlp-client-service/dto/ResponseDto";

class GetAddMembersInfoDto extends ResponseDto
{
    /** ユーザー数 */
    currentMemberNum = Number();

    /** 最大ユーザー数 */
    maxMemberNum = Number();

}

export default GetAddMembersInfoDto;