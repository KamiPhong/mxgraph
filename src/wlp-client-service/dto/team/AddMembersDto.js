import ResponseDto from "wlp-client-service/dto/ResponseDto";

class AddMembersDto extends ResponseDto
{
    /** ユーザー数 */
    currentMemberNum = Number();

    // element of CheckedEmailInfo
    errorEmailList = [];

}

export default AddMembersDto;