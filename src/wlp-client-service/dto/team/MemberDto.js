class MemberDto {
    userId = Number();
    /** 1:リーダー 2:アドミン 3:メンバー 4:パートナー */
    userType = Number();

    /** 招待中フラグ */
    invitingFlg = Boolean();

    userName = String();

    userImgUrl = String();

    email = String();

    maps = Number();
}

export default MemberDto;
