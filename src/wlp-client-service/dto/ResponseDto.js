class ResponseDto {

    messageType = String();

    messageKey = String();

    message = String();

    isSucceed = Boolean();

    /** 検査用アクセストークン */
    accessToken = String();
}

export default ResponseDto;