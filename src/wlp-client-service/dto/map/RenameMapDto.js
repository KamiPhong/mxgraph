import ResponseDto from "wlp-client-service/dto/ResponseDto";
import MapRecordDetailDto from "./MapRecordDetailDto";
import MapListSearchResultDto from "./MapListSearchResultDto";

class RenameMapDto extends ResponseDto
{
    renameRecordDetail = new MapRecordDetailDto();

    searchResult = new MapListSearchResultDto();

}

export default RenameMapDto;