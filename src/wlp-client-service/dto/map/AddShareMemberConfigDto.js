import MapListSearchConditionDto from "./MapListSearchConditionDto";
import MypageRequestDto from "../MypageRequestDto";

class AddShareMemberConfigDto extends MypageRequestDto
{
    addUserId = Number();

    mapId = Number();

    searchCondition = new MapListSearchConditionDto();

}
export default AddShareMemberConfigDto;