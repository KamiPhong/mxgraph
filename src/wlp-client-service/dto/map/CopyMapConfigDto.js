import MypageRequestDto from "../MypageRequestDto";
import MapListSearchConditionDto from "./MapListSearchConditionDto";

class CopyMapConfigDto extends MypageRequestDto {
    srcMapId = Number;

    copyMapName = String;

    searchCondition = new MapListSearchConditionDto();
}

export default CopyMapConfigDto;