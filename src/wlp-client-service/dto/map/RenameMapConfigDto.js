import MypageRequestDto from "../MypageRequestDto";
import MapListSearchConditionDto from "./MapListSearchConditionDto";

class RenameMapConfigDto extends MypageRequestDto
{
    srcMapId = Number();

    newMapName = String();

    searchCondition = new MapListSearchConditionDto();;

}

export default RenameMapConfigDto;