import MapListSearchConditionDto from "./MapListSearchConditionDto";
import MypageRequestDto from "../MypageRequestDto";

class SendInviteMapMailConfigDto extends MypageRequestDto
{
    mapId = Number();

    mapTitle = String();

    email = String();

    optionalMsg = String();

    sendCopyToMe = Boolean();

    searchCondition = new MapListSearchConditionDto();

}

export default SendInviteMapMailConfigDto;
