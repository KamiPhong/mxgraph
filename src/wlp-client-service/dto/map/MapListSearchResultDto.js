import MapSummaryDto from "./MapSummaryDto";

class MapListSearchResultDto
{
    mapSummary = new MapSummaryDto();

    startNum = Number();

    endNum = Number();
	
	/** 現在検索した結果のマップ数 */
	totalNum = Number();

    // element MapRecordDto
    mapRecordList = [];

}


export default MapListSearchResultDto;