import ResponseDto from 'wlp-client-service/dto/ResponseDto'
import MapRecordDetailDto from './MapRecordDetailDto';

class GetMapRecordDetailDto extends ResponseDto
{
    mapRecordDetail = new MapRecordDetailDto();
}
export default GetMapRecordDetailDto;