import MapRecordDto from "./MapRecordDto";

class MapRecordDetailDto extends MapRecordDto
{
    createdDate = Date();

    /** 最終更新者名 */
    lastUpdateName = String();

    /** 最終更新者会社名 */
    lastUpdateUserCompany = String();

    /** 最終更新者部署名 */
    lastUpdateUserDept = String();

    /**
     * 共有中リスト(B to C の場合に使用)
     * element MapUserDto
     */
    shareUserList = [];

    /**
     * 共有中のメンバーリスト(B to B の場合に使用)
     * element MapUserDto
     */
    shareMemberList = [];

    /**
     * 追加可能なメンバーリスト(B to B の場合に使用)
     * element MapUserDto
     */
    addableMemberList = [];

    /**
     * 共有中のパートナーリスト(B to B の場合に使用)
     * element MapUserDto
     */
    sharePartnerList = [];

    /** 追加可能人数 */
    addableShareNum = Number();

    /** 最大共有人数 */
    maxShareNum = Number();

    /**
     * シートリスト
     * element SheetRecordDto
     */
    sheetList = [];

    /** 使用テンプレート画像 */
    templateImageUrl = String();

    /** 使用テンプレート名 */
    templateTitle = String();

    /** 使用テンプレートカテゴリーID 未設定の場合は -1 を格納 */
    templateCategoryId = Number();

    /** 使用テンプレートカテゴリー */
    templateCategoryNameJapanese = String();

    /** 使用テンプレートカテゴリー */
    templateCategoryNameEnglish = String();

    /** 使用テンプレート作者 */
    templateAuthor = String();

    /** 使用テンプレートリリース日 */
    templateReleaseDate = Date();

	/** テンプレート作成の有無  */
	templateCreateFlag = Boolean();
}

export default MapRecordDetailDto;