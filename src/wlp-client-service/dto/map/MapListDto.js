import ResponseDto from 'wlp-client-service/dto/ResponseDto'
import TeamListDto from 'wlp-client-service/dto/team/TeamListDto';
import ContractDto from '../ContractDto';

class MapListDto extends ResponseDto {
    
    contractInfo = new ContractDto();

    companyName = String();

    jobTitle = null;

    updateFlg = Boolean();

    mapTitle = String();

    bookmark = Boolean();

    mediumThumbnailUrl = String();

    ownerName = String();

    newFlg = Boolean();

    mapSizeTotal = Number();

    mapId = Number();

    popular = Number();

    ipControlFlag = Boolean();

    lastUpdateTime = Number();

    mapSummary = new TeamListDto();
    
}

export default MapListDto;