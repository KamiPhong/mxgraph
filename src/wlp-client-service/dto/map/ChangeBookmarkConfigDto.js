import MypageRequestDto from "../MypageRequestDto";

class ChangeBookmarkConfigDto extends MypageRequestDto
{
    mapId = Number();

    bookmark = Boolean();

}

export default ChangeBookmarkConfigDto;