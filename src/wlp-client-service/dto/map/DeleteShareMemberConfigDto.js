  
import MypageRequestDto from "../MypageRequestDto";
import MapListSearchConditionDto from "./MapListSearchConditionDto";

class DeleteShareMemberConfigDto extends MypageRequestDto
{
    deleteUserId = Number();

    mapId = Number();

    searchCondition = new MapListSearchConditionDto();

}
export default DeleteShareMemberConfigDto;