import MypageRequestDto from "../MypageRequestDto";
import MapListSearchConditionDto from "./MapListSearchConditionDto";

class GetMapListConfigDto extends MypageRequestDto {
    searchConditionDto = new MapListSearchConditionDto();
}

export default GetMapListConfigDto;