import MypageRequestDto from "../MypageRequestDto";

class DeleteMapConfigDto extends MypageRequestDto {
    mapId = Number();
}

export default DeleteMapConfigDto;