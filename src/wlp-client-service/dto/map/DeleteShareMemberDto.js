import ResponseDto from "wlp-client-service/dto/ResponseDto";
import MapListSearchResultDto from "./MapListSearchResultDto";
import MapRecordDetailDto from "./MapRecordDetailDto";

class DeleteShareMemberDto extends ResponseDto
{
    mapRecordDetail = new MapRecordDetailDto();

    searchResult = new MapListSearchResultDto();
}
export default DeleteShareMemberDto;
