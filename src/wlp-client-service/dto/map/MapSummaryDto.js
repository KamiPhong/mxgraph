import TeamListDto from "../team/TeamListDto";

class MapSummaryDto
{
    allNum = Number();

    ownerByMeNum = Number();

    ownerByOtherNum = Number();

    newMapsNum = Number();

    updateMapsNum = Number();

    bookmarksNum = Number();
	
	/** TeamHavingMapDtoのリスト */
	teamHavingMapList = new TeamListDto();

}

export default MapSummaryDto;
