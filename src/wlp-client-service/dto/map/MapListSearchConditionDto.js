class MapListSearchConditionDto {
    // "personal" or "team"
    contractType = String();

    teamId = Number();

    contractId = Number(-1);

    /**
     * マップの種類
     * "all"
     * "ownerByMe"
     * "ownerByOther"
     * "newMaps"
     * "updateMaps"
     * "bookmarks"
     */
    mapKind = String();

    startNum = Number();

    endNum = Number();

    /**
     * ソートカラム名
     * "bookmark"
     * "mapTitle"
     * "popular"
     * "ownerName"
     * "companyName"(1:会社名, 2:部署名)
     * "lastUpdateTime"
     */
    sortColumn = String();

    descending = Boolean();

    /** 1ページのレコード数 */
    pageRecordNum = Number();

    /** mapKind="recent" の時にMapIDListから検索する */
    mapIdList = [];

}

export default MapListSearchConditionDto;