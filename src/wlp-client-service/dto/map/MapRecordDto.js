import ContractDto from "../ContractDto";

class MapRecordDto {
    mapId = Number();

    bookmark = Boolean();

    newFlg = Boolean();

    updateFlg = Boolean();

    mapTitle = String();

    popular = Number();

    ownerName = String();

    companyName = String();

    jobTitle = String();

    lastUpdateTime = Date();

    /** Map size */
    mapSizeTotal = Number();

    /** Team information */
    contractInfo = new ContractDto();

    /** Thumbnail image path of the first sheet to possess */
    mediumThumbnailUrl = String();

    ipControlFlag = Boolean();
}

export default MapRecordDto;