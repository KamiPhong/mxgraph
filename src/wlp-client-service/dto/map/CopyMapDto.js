import ResponseDto from "wlp-client-service/dto/ResponseDto";
import MapListSearchResultDto from "./MapListSearchResultDto";
import MapRecordDetailDto from "./MapRecordDetailDto";

class CopyMapDto extends ResponseDto {
    searchResult = new MapListSearchResultDto();

    copyRecordDetail = new MapRecordDetailDto();
}

export default CopyMapDto;