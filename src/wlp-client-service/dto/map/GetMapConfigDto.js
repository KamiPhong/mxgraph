import RequestDto from "wlp-client-service/dto/RequestDto";

class GetMapConfigDto extends RequestDto {
    /** マップID */
    mapId =  Number();

    /** 契約ID */
    contractId =  Number();

    /** シートID */
    sheetId =  Number();
}

export default GetMapConfigDto;