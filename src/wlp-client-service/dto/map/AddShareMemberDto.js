import MapRecordDetailDto from "./MapRecordDetailDto";
import ResponseDto from "wlp-client-service/dto/ResponseDto";
import MapListSearchResultDto from "./MapListSearchResultDto";

class AddShareMemberDto extends ResponseDto
{
    mapRecordDetail = new MapRecordDetailDto();

    searchResult = new MapListSearchResultDto();
}
export default AddShareMemberDto;
