import MypageRequestDto from "../MypageRequestDto";

class CheckExistMapConfigDto extends MypageRequestDto
{
    mapId = Number();
}
export default CheckExistMapConfigDto;
