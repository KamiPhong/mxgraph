import ResponseDto from 'wlp-client-service/dto/ResponseDto';
import MapListSearchResultDto from './MapListSearchResultDto';
import MapRecordDetailDto from './MapRecordDetailDto';

class CreateMapDto extends ResponseDto
{
    searchResult = new MapListSearchResultDto();

    newRecordDetail = new MapRecordDetailDto();

}

export default CreateMapDto;