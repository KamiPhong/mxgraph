import MypageRequestDto from "../MypageRequestDto";
import MapListSearchConditionDto from "./MapListSearchConditionDto";

class DeleteSharePartnerConfigDto extends MypageRequestDto
{
    deleteUserId = Number();

    mapId = Number();

    searchCondition = new MapListSearchConditionDto();

}
export default DeleteSharePartnerConfigDto;
