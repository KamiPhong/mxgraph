
import MypageRequestDto from '../MypageRequestDto';
import TemplateRecordDto from '../template/TemplateRecordDto';
import MapListSearchConditionDto from './MapListSearchConditionDto';
class CreateMapConfigDto extends MypageRequestDto {
    newMapName = String();

    templateRecord = new TemplateRecordDto();

    searchCondition = new MapListSearchConditionDto();

}

export default CreateMapConfigDto;