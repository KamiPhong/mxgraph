import ResponseDto from 'wlp-client-service/dto/ResponseDto'
import MapListSearchResultDto from './MapListSearchResultDto';
import MapRecordDetailDto from './MapRecordDetailDto';

class GetMapListDto extends ResponseDto {

    searchResult = new MapListSearchResultDto();

    firstRecordDetail = new MapRecordDetailDto();
    
}

export default GetMapListDto;