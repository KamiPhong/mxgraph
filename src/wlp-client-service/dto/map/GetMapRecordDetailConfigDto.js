import MypageRequestDto from 'wlp-client-service/dto/MypageRequestDto';

class GetMapRecordDetailConfigDto extends MypageRequestDto
{
    mapId = Number();
}

export default GetMapRecordDetailConfigDto;