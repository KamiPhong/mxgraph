import RequestDto from "wlp-client-service/dto/RequestDto";

class SearchConfigDto extends RequestDto
{
	/** 検索文字列 */
	text = String();
	
	/** 契約ID */
	contractId = Number();
	
	/** マップID */
	mapId = Number();
	
	/** 開始インデックス */
	start = Number();
	
	/** 取得項目数 */
	count = Number();
}

export default SearchConfigDto;
