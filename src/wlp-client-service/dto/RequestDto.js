import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";

const localStorageUtil = new LocalStorageUtils();
class RequestDto {
    accessToken = String();

    constructor() {
        const logonInfo = localStorageUtil.getUserSession();
        this.accessToken = logonInfo ? logonInfo.accessToken : null;
    }
}

export default RequestDto;