import IdLabelDto from "../IdLabelDto";

class CategoryRecordDto extends IdLabelDto
{
     japaneseLabel = String();

     englishLabel = String();

     count = Number();;

}

export default CategoryRecordDto