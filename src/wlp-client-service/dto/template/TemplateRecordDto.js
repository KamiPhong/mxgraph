class TemplateRecordDto {
    templateId = Number();

    bookmark = Boolean();

    newFlg = Boolean();

    templateTitle = String();

    author = String();

    releaseDate = Date();

    /** Category情報をDetailから移植 */
    categoryId = Number();

    categoryName = String();

    companyId = Number();

    companyName = String();

    hasEditFlg = Boolean();

    teamName = String();
}

export default TemplateRecordDto;