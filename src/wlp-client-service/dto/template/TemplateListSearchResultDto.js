import TemplateSummaryDto from "./TemplateSummaryDto";

class TemplateListSearchResultDto
{
    templateSummary = new TemplateSummaryDto();

    // element CategoryRecordDto
    categoryList = [];

    startNum = Number();

    endNum = Number();

    // element TemplateRecordDto
    templateRecordList = [];

}

export default TemplateListSearchResultDto