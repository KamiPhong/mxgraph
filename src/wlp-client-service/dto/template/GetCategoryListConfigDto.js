import MypageRequestDto from "../MypageRequestDto";

class GetCategoryListConfigDto extends MypageRequestDto
{
	allFlag  = Boolean(true);
}

export default GetCategoryListConfigDto;