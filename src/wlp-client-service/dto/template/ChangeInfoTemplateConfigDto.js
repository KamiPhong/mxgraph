import MypageRequestDto from "../MypageRequestDto";

class ChangeInfoTemplateConfigDto extends MypageRequestDto
{
    templateId = Number();

    templateTitle = String();

    publisher = String();

    author = String();

    categoryId = Number();

    detail = String();

}
export default ChangeInfoTemplateConfigDto;
