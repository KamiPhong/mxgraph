import MypageRequestDto from "../MypageRequestDto";

class DeleteTemplateConfigDto extends MypageRequestDto
{
    templateId = Number();

}
export default DeleteTemplateConfigDto;

