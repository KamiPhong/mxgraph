import MypageRequestDto from "../MypageRequestDto";
import MapRecordDetailDto from "../map/MapRecordDetailDto";
import TemplateListSearchConditionDto from "./TemplateListSearchConditionDto";

class CreateTemplateConfigDto extends MypageRequestDto
{
    newTemplateSrc = new MapRecordDetailDto();

    templateTitle = String();

    templatePublisher = String();

    templateAuthor = String();

    templateCotegoryId = Number();

    templateDetail = String();

    searchCondition = new TemplateListSearchConditionDto();

}
export default CreateTemplateConfigDto;
