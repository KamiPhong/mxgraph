import TemplateRecordDetailDto from "./TemplateRecordDetailDto";
import ResponseDto from "wlp-client-service/dto/ResponseDto";

class GetTemplateRecordDetailDto extends ResponseDto
{
    templateRecordDetail = new TemplateRecordDetailDto();

}

export default GetTemplateRecordDetailDto;