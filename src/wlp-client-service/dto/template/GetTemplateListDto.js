import ResponseDto from "wlp-client-service/dto/ResponseDto";
import TemplateListSearchResultDto from "./TemplateListSearchResultDto";
import TemplateRecordDetailDto from "./TemplateRecordDetailDto";

class GetTemplateListDto extends ResponseDto
{
    searchResult = new TemplateListSearchResultDto();;

    firstRecordDetail = new TemplateRecordDetailDto();
}

export default GetTemplateListDto;