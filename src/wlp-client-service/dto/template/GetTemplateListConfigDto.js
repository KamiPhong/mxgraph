import MypageRequestDto from "wlp-client-service/dto/MypageRequestDto";
import TemplateListSearchConditionDto from 'wlp-client-service/dto/template/TemplateListSearchConditionDto';

class GetTemplateListConfigDto extends MypageRequestDto {
    searchCondition = new TemplateListSearchConditionDto();
}

export default GetTemplateListConfigDto;