import ResponseDto from "wlp-client-service/dto/ResponseDto";

class GetCategoryListDto extends ResponseDto
{
    categoryList = [];
}

export default GetCategoryListDto;