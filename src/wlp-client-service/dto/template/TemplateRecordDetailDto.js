import TemplateRecordDto from "./TemplateRecordDto";

class TemplateRecordDetailDto extends TemplateRecordDto
{
     templatePublisher = String();

    /**
     * シートリスト
     * element SheetRecordDto
     */
     sheetList = [];

     detail = String();

}

export default TemplateRecordDetailDto;