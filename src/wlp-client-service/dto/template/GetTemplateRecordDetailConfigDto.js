import MypageRequestDto from "../MypageRequestDto";

class GetTemplateRecordDetailConfigDto extends MypageRequestDto
{

    templateId = Number();

}

export default GetTemplateRecordDetailConfigDto;