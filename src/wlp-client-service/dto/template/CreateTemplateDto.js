import ResponseDto from "wlp-client-service/dto/ResponseDto";
import TemplateListSearchResultDto from "./TemplateListSearchResultDto";
import TemplateRecordDetailDto from "./TemplateRecordDetailDto";

class CreateTemplateDto extends ResponseDto
{

    searchResult =  new TemplateListSearchResultDto();

    newRecordDetail = new TemplateRecordDetailDto();

}
export default CreateTemplateDto;