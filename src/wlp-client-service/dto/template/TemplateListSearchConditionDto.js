class TemplateListSearchConditionDto {
    /**
     * テンプレートの種類
     * "all"
     * "newTemplate"
     * "bookmarks"
     */
    templateKind = String();

    categoryId = Number();

    startNum = Number();

    endNum = Number();

    /**
     * ソートカラム名
     * "bookmark"
     * "templateTitle"
     * "author"
     * "releaseDate"
     */
    sortColumn = String();

    descending = Boolean;

    /** 1ページのレコード数 */
    pageRecordNum = Number();


}

export default TemplateListSearchConditionDto;