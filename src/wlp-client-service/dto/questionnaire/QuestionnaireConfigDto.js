class QuestionnaireConfigDto
{
    /** 価格 */
    kakaku = Boolean();

    /** ストレージ容量 */
    strage = Boolean();

    /** 添付ファイルの容量制限 */
    attachment = Boolean();

    /** 外部サービスとの連携（EVERNOTE,DropBox,YouTubeなど） */
    exService = Boolean();

    /** 対応OS */
    os = Boolean();

    /** 対応ブラウザ */
    browser = Boolean();

    /** セキュリティの信頼性 */
    security = Boolean();

    /** アイテムや線種のバリエーション */
    item = Boolean();

    /** サムネイルやプレビュー機能 */
    thumb = Boolean();

    /** 動作が遅い */
    slow = Boolean();

    /** 動作が不安定 */
    instability = Boolean();

    /** 同期しない */
    noSynchro = Boolean();

    /** 機能が足りない */
    ability = Boolean();

    /** 希望される機能を教えてください */
    functionalityDesired = String();

    /** 会議支援 */
    conference = Boolean();

    /** メールの代替 */
    email = Boolean();

    /** 個人の思考整理 */
    organize = Boolean();

    /** そもそも利用用途がわからなかった。 */
    understand = Boolean();

    /** 他社サービスに乗り換えた */
    switched = Boolean();

    /** サービス名を教えてください */
    switchedServiceName = String();

    /** 招待者とトラブルがあった */
    trouble = Boolean();

    /** 利用頻度が少なくなった */
    lessFrequently = Boolean();

    /** メインOS win,mac,others */
    mainOs = Number();

    /** メインブラウザ ie,forefox,chrome,others */
    mainBrowser = Number();

    /** 主に一人 one,multi */
    userNum = Number();

    /** 具体的な御意見、改善要望点がありましたら、是非お聞かせ下さい。 */
    option = String();

}

export default QuestionnaireConfigDto;
