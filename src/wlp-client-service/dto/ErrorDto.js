class ErrorDto {
    email = String();
    actionStack = String();
    errorInfo = String();
    excTime = String();
}

export default ErrorDto;