import { requestLambdaApi, requestSevletApi } from 'wlp-client-service/service/request';
import GetMapListConfigDto from 'wlp-client-service/dto/map/GetMapListConfigDto';
import GetMapRecordDetailConfigDto from 'wlp-client-service/dto/map/GetMapRecordDetailConfigDto';
import CreateMapConfigDto from 'wlp-client-service/dto/map/CreateMapConfigDto';
import SendInviteMapMailConfigDto from 'wlp-client-service/dto/map/SendInviteMapMailConfigDto';
import RenameMapConfigDto from 'wlp-client-service/dto/map/RenameMapConfigDto';
import CopyMapConfigDto from 'wlp-client-service/dto/map/CopyMapConfigDto';
import DeleteMapConfigDto from 'wlp-client-service/dto/map/DeleteMapConfigDto';
import ChangeBookmarkConfigDto from 'wlp-client-service/dto/map/ChangeBookmarkConfigDto';
import DeleteShareMemberConfigDto from 'wlp-client-service/dto/map/DeleteShareMemberConfigDto';
import AddShareMemberConfigDto from 'wlp-client-service/dto/map/AddShareMemberConfigDto';
import DeleteSharePartnerConfigDto from 'wlp-client-service/dto/map/DeleteSharePartnerConfigDto';
import CheckExistMapConfigDto from 'wlp-client-service/dto/map/CheckExistMapConfigDto';
import GetMapConfigDto from 'wlp-client-service/dto/map/GetMapConfigDto';

class MapService {

    /**
    * マップを取得します。
    *
    * <p>必要パラメータはmapIdのみ</p>
    * @return {GetMapDto}
    */
    getMap = (paramDto = new GetMapConfigDto()) => {
        const requestRoute = '/map/get-map';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto,
            closeWindownError: true
        })
    }

    /**
    * マップを共有できるユーザリストを取得します。
    *
    * <p>必要パラメータはmapId, contractId</p>
    */
    getSharedUsers = (paramDto = new GetMapRecordDetailConfigDto()) => {
        const requestRoute = '/map/shared-users';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
        })
    }

    getMapList(paramDto = new GetMapListConfigDto()) {
        const requestRoute = '/map/all';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
        })
    }

    changeBookmark = (paramDto = new ChangeBookmarkConfigDto()) => {
        return requestLambdaApi({
            url: '/map/change-bookmark',
            method: 'POST',
            data: paramDto
        })

    }

    getMapRecordDetail = (paramDto = new GetMapRecordDetailConfigDto()) => {
        const requestRoute = '/map/map-detail';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
        })
    }

    createMap = (paramDto = new CreateMapConfigDto()) => {
        return requestSevletApi({
            url: '/api/map/create-map',
            method: 'POST',
            data: paramDto
        })
    }

    renameMap = (paramDto = new RenameMapConfigDto()) => {
        return requestLambdaApi({
            url: '/map/rename',
            method: 'POST',
            data: paramDto
        })
    }

    copyMap = (paramDto = new CopyMapConfigDto()) => {
        return requestSevletApi({
            url: '/api/map/copy-map',
            method: 'POST',
            data: paramDto
        })
    }

    deleteMap = (paramDto = new DeleteMapConfigDto()) => {
        return requestLambdaApi({
            url: '/map/delete-map',
            method: 'POST',
            data: paramDto
        })
    }

    sendInviteMapMail = (paramDto = new SendInviteMapMailConfigDto()) => {
        return requestLambdaApi({
            url: '/map/send-invite-map-mail',
            method: 'POST',
            data: paramDto,
            showErrorDialog: false
        })
    }

    deleteShareMember = (paramDto = new DeleteShareMemberConfigDto()) => {
        return requestLambdaApi({
            url: '/map/delete-share-member',
            method: 'POST',
            data: paramDto
        })
    }

    addShareMember = (paramDto = new AddShareMemberConfigDto()) => {
        return requestLambdaApi({
            url: '/map/add-share-member',
            method: 'POST',
            data: paramDto
        })
    }

    deleteSharePartner = (paramDto = new DeleteSharePartnerConfigDto()) => {
        return requestLambdaApi({
            url: '/map/delete-share-partner',
            method: 'POST',
            data: paramDto
        })
    }

    deleteShareUser = () => {

    }

    checkExistMap = (paramDto = new CheckExistMapConfigDto()) => {
        return requestLambdaApi({
            url: '/map/exist-map',
            method: 'POST',
            data: paramDto
        })
    }

    getMapListByMapIds = () => {

    }
}

export default MapService;