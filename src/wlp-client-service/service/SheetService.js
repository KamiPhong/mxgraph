import { requestLambdaApi, requestSevletApi } from 'wlp-client-service/service/request';
import SendNotificationConfigDto from 'wlp-client-service/dto/sheet/SendNotificationConfigDto';
import DownloadAttachFileConfigDto from 'wlp-client-service/dto/sheet/DownloadAttachFileConfigDto';

class SheetService {

    /**
     * シートのエンティティ情報だけを取得する。
     * 
     * @param sheetId シートID
     * 
     * 戻りは<code>GetSheetDto</code>です。
     */
    getInfo(sheetId) {
        return requestLambdaApi({
            url: '/sheets/property/' + sheetId,
            method: 'GET',
        })
    }

    addItems(paramDto = new Array()) {
        return requestLambdaApi({
            url: '/item/add-new',
            method: 'POST',
            data: paramDto
        })
    }

    removeItems(paramDto = new Array()) {
        return requestLambdaApi({
            url: '/item/remove',
            method: 'POST',
            data: paramDto
        });
    }

    lockItems(items) {
        return requestLambdaApi({
            url: '/item/lock',
            method: 'POST',
            data: items
        });
    }

    unLockItems(items) {
        return requestLambdaApi({
            url: '/item/unlock',
            method: 'POST',
            data: items
        });
    }

    updateItems(items) {

        return requestLambdaApi({
            url: '/item/update',
            method: 'POST',
            data: items
        });
    }

    fileUpload(requestDto) {
        return requestLambdaApi({
            url: '/item/file-upload',
            method: 'POST',
            data: requestDto
        })
    }

    downloadAttachFile(requestDto = new DownloadAttachFileConfigDto()) {
        return requestLambdaApi({
            url: '/item/download-attach-file',
            method: 'POST',
            data: requestDto
        })
    }

    uploadExportFile(requestDto) {
        return requestLambdaApi({
            url: '/sheets/upload-export-file',
            method: 'POST',
            data: requestDto,
            showErrorDialog: false
        })
    }

    convertBase64SvgToPng(requestDto) {
        return requestLambdaApi({
            url: '/sheets/svg-to-png',
            method: 'POST',
            data: requestDto,
            showErrorDialog: false
        })
    }


    sendNotification(requestDto = new SendNotificationConfigDto()) {
        return requestLambdaApi({
            url: '/sheets/send-notification',
            method: 'POST',
            data: requestDto
        });
    }

    convertXmlToImage(requestDto) {
        return requestLambdaApi({
            url: '/item/export-png',
            method: 'POST',
            data: requestDto,
            showErrorDialog: false
        })
    }
}

export default SheetService;