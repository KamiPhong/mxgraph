import axios from "axios";
import GatewayConstants from "wlp-client-service/consts/GatewayConstants";
import utf8 from 'utf8';

import { nestedMap } from 'wlp-client-common/utils/ArrayUtil';
import { showWarningDialog } from "wlp-client-common/utils/DialogUtils";
import httpErrorCode from "wlp-client-common/consts/httpErrorCode";
import LocalStorageUtils from "wlp-client-common/utils/LocalStorageUtils";
import exceptionHandler from "wlp-client-service/exceptionHandler";
import { IS_TOUCH } from "wlp-client-common/utils/BrowserUtil";

const localStorageUtil = new LocalStorageUtils();
const API_URL = (process.env.NODE_ENV === 'production') ? process.env.REACT_APP_API_HOST : "";
const LAMDA_API_URL = (process.env.NODE_ENV === 'production') ? process.env.REACT_APP_REMOTE_HOST : "";
const defaultsConfig = {
    headers: {
        "Content-Type": "application/json"
    }
};

let requestCount = 0;
let loadingMouse = createLoadingMouse();
let isPreloadDisplayIsUp = false;

document.addEventListener('preload-display-mounted', () => {
    isPreloadDisplayIsUp = true;
});

document.addEventListener('preload-display-unmounted', () => {
    isPreloadDisplayIsUp = false;
});

function increaseRequestCount() {
    requestCount++;

    if (requestCount === 1) {
        // Disable loading on touch screen if preload display screen is on
        if (!IS_TOUCH || (IS_TOUCH && !isPreloadDisplayIsUp)) {
            loadingMouse.show();
        }     
    }
}
function decreaseRequestCount() {
    requestCount--;

    if (requestCount <= 0) {
        loadingMouse.hide();
    }
}


const encodeUTF8Object = (obj) => {
    return nestedMap(obj, val => {
        if (typeof (val) === 'string') {
            return utf8.encode(val);
        }

        return val;
    });
}

function strHasUnicode(str) {
    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 127)
            return true;
    }
    return false;
}

const decodeUTF8Object = (obj) => {
    return nestedMap(obj, val => {
        if (typeof (val) === 'string') {
            try {
                return strHasUnicode(val) ? val : utf8.decode(val);
            } catch (error) {
            }
        }
        return val;
    });
}

const updateUserAccessToken = (data) => {
    let logonInfo = localStorageUtil.getUserSession();
    if (logonInfo && data && data.accessToken) {
        logonInfo.accessToken = data.accessToken;
        localStorageUtil.setUserSession(logonInfo);
    }
}

const updateCookiesString = (cookiesString) => {
    if (cookiesString) {
        // cookie hạn sử dụng 6 tháng
        let expireTime = new Date(Date.now() + 180 * 24 * 60 * 60 * 1000).toGMTString();
        let domain = cookiesString.match(/Domain=[^;]*/)[0].slice(7)
        const cookieList = cookiesString.split("|").map(item => {
            const [key, val] = item.split("=");
            if (key && val) {
                let cookie = `${key}=${val};expires=${expireTime};Domain=${domain};path=/`
                if(process.env.NODE_ENV !== 'production') {
                    cookie = `${key}=${val};expires=${expireTime};path=/`
                }
                return cookie

            } else {
                return null
            }
        }).filter(Boolean);
        for (let cookie of cookieList) {
            document.cookie = cookie;
        }
    }
}

const setUrlParam = (url, key, value) => {
    if (typeof url === 'string') {
        if (url.indexOf(key) === -1) {
            url += url.indexOf('?') === -1 ? `?${key}=${value}` : `&${value}`;
        }
    }

    return url;
}

export function createLoadingMouse() {
    let loadingBackdrop = document.createElement('div');
    let isShowing = false;
    loadingBackdrop.style.height = '100%'
    loadingBackdrop.classList.add('cursor-container');
    loadingBackdrop.style.cursor = 'wait';

    if (IS_TOUCH) {
        loadingBackdrop.style.cssText = `
            background-attachment: fixed;
            background-color: #80808073;
        `;

        // Add loading image in center of the screen on touch devices
        // Because on touch devices, there are no mouse to show loading status
        // Recommended by Accelatech
        let imgLoading = document.createElement('img');
        // Image source from ~/public folder
        imgLoading.src = '/static/media/loading.gif';
        imgLoading.style.cssText = `
            position: absolute;
            top: 40%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: white;
            padding: 30px;
            border-radius: 10px;
        `;
        loadingBackdrop.appendChild(imgLoading);
    }

    const showLoadingMouse = () => { 
        document.body.appendChild(loadingBackdrop);
        isShowing = true;
    };
    
    const hideLoadingMouse = () =>  {
        if (isShowing) {
            loadingBackdrop.parentNode.removeChild(loadingBackdrop);
            isShowing = false;
        }
    }

    return {
        hide: hideLoadingMouse,
        show: showLoadingMouse
    };
}

const request = ({ url, method, data, onDownloadProgress, onErrorMessage, showErrorDialog = true, closeWindownError = false, ...res }) => {
    increaseRequestCount();
    const utd8EncodedData = encodeUTF8Object(data);

    let customOptions = { ...defaultsConfig, url, method, data: utd8EncodedData, ...res };

    customOptions.url = setUrlParam(customOptions.url, '_t', Date.now());
    if (onDownloadProgress && typeof onDownloadProgress === 'function') {
        customOptions = { ...customOptions, onDownloadProgress };
    }

    return axios({ ...customOptions, withCredentials: true })
        .then(response => {

            if (response.status === httpErrorCode.NOT_FOUND || response.status >= httpErrorCode.INTERNAL_SERVER_ERROR) {
                return Promise.reject(response);
            }
            const responseData = decodeUTF8Object(response.data);
            
            if (showErrorDialog && responseData.isSucceed === false) {
                const warningConfig = {};
                warningConfig.showCancel = false;
                if (closeWindownError === true) {
                    warningConfig.onOKCallback = () => window.close();
                }
                showWarningDialog(onErrorMessage || response.data.messageKey || 'message.global.e0001', warningConfig);
            }
            const setCookieString = response.headers[GatewayConstants.SET_COOKIE];
            updateCookiesString(setCookieString);
            updateUserAccessToken(response.data);

            return responseData;
        })
        .catch(e => {
            let errorData = e.response && e.response.data;
            if (e.isAxiosError) {
                if (!errorData) errorData = {};

                if (typeof errorData === 'object' && !errorData.message) {
                    errorData.message = 'message.global.e0001';
                }
            }

            exceptionHandler(errorData, showErrorDialog);
            throw e;
        }).finally(() => {
            decreaseRequestCount();
        });
};

export function requestLambdaApi(options) {
    if (typeof options.url === 'string') {
        options.url = LAMDA_API_URL + options.url;
    }
    return request(options);
}

export function requestSevletApi(options) {
    if (typeof options.url === 'string') {
        options.url = API_URL + options.url;
    }
    return request(options);
}

export function fetchServletApi(url, options) {
    if (typeof url === 'string') {
        url = API_URL + url;
    }
    
    return fetch(url, options)
}

export default request;

