import { requestLambdaApi } from 'wlp-client-service/service/request';
class InformationService {

    getInformation = () => {
        const getInformationAPI = '/infomation';
        return requestLambdaApi({
            url: getInformationAPI,
            method: 'GET',
            showErrorDialog: false
        });
    }
}

export default InformationService;