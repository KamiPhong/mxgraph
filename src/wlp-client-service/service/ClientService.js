import { requestSevletApi } from 'wlp-client-service/service/request';
import ErrorDto from 'wlp-client-service/dto/ErrorDto';

class ClientService {
    sendLogError = (paramDto = new ErrorDto()) => {
        const requestRoute = "/api/logging";
        return requestSevletApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto,
                headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
            }
        })
    }
}

export default ClientService;