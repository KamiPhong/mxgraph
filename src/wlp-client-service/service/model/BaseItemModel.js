import ObjectPrototype from 'wlp-client-editor-lib/utils/ObjectPrototype';
import EditorModel from './EditorModel';

class BaseItemModel extends EditorModel { 

    /** クライアントがセットする一意キー */
    uid = String();

    /** アイテムID */
    id = Number();

    /** シートID */
    sheetId = Number();

    /** マップID */
    mapId = Number();

    /** 契約ID */
    contractId = Number();

    /** テキスト */
	text = String();
		
    /** 座標(x) */
    x = Number();
    
    /** 座標(y) */
    y = Number();
    
    /** 幅 */
    width = Number();
    
    /** 高さ */
    height = Number();
    
    /** 形 */
    shape = Number();
    
    /** デザインプリセット */
    style = Number();
    
    /** 背景色 */
    backgroundColor = Number();
    
    /** 線色 */
    lineColor = Number();
    
    /** テキストサイズ */
    textSize = Number();
    
    /** テキスト色 */
    textColor = Number();
    
    /** テキスト座標(x) */
    textX = Number();
    
    /** テキスト座標(y) */
    textY = Number();
    
    /** テキスト幅 */
    textWidth = Number();
    
    /** テキスト高さ */
    textHeight = Number();
    
    /** テキスト位置 水平 */
    textHorizontalAlign = Number();
    
    /** テキスト位置 垂直 */
    textVerticalAlign = Number();
    
    /** 編集中 */
    editing = Boolean();
    
    /** 編集開始日時 */
    editStartTime = Date();
    
    /** 編集中クライアントID */
    editClientId = String();
    
    /** 編集者名 */
    editUserName = String();
    
    /** 作成者名 */
    createUserName = String();
    
    /** 作成者ID */
    createUserId = Number();
    
    /** 作成日時 */
    createTime = Date();
    
    /** 更新者名 */
    updateUserName = String();
    
    /** 更新者ID */
    updateUserId = Number();
    
    /** 更新日時 */
    updateTime = Date();
    
    /** 関係線のリスト */
    relationLines = [];
    
    constructor(object) {
        super();
        let time = new Date().getTime();
        this.createTime = time;
        this.updateTime = time;
        this.editStartTime = null;
        this.width = 330;
        this.height = 220;
        // model.backgroundColor = ;
        // model.lineColor = ;
        this.textSize = 15;
        // model.textColor = ;
        this.textWidth = 300;
        this.textHeight = 50;
        this.textHorizontalAlign = 1;
        this.textVerticalAlign = 1;
        this.__proto__.importFromGraphItem = ObjectPrototype.__proto__.importFromObject

        if (typeof object === 'object') {
            this.importFromGraphItem(object);
        }
    }
}

export default BaseItemModel;