import ErrorTypes from "wlp-client-service/consts/ErrorTypes";

class WlpFault {
		
    messageType = String();
    
    messageKey = String();
    
    message = String();
    
    /** 送信されたデータ */
    sendedData = Object();

    sendFrom = String();
    
    constructor(){
        //default is server error
        this.sendFrom = ErrorTypes.SERVER_ERROR;
    }
}

export default WlpFault;