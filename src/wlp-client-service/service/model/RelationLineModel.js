import EditorModel from './EditorModel';

class RelationLineModel extends EditorModel {
    /** クライアントがセットする一意キー */
    uid = String();

    /** idプロパティ */
    id = Number();

    /** シートID */
    sheetId = Number();

    /** マップID */
    mapId = Number();

    /** 契約ID */
    contractId = Number();

    /** kindプロパティ */
    kind = Number();

    /** startIdプロパティ */
    startId = Number();

    /** endIdプロパティ */
    endId = Number();

    /** styleプロパティ */
    style = Number();

    /** largeプロパティ */
    large = Number();

    /** colorプロパティ */
    color = Number();

    /** startStyleプロパティ */
    startStyle = Number();

    /** endStyleプロパティ */
    endStyle = Number();

    /** テキスト */
    text = String();

    /** テキスト位置 水平 */
    textHorizontalAlign = Number();

    /** テキスト位置 垂直 */
    textVerticalAlign = Number();

    /** テキストサイズ */
    textSize = Number();

    /** editingプロパティ */
    editing = Boolean();

    /** editStartTimeプロパティ */
    editStartTime = Date();

    /** 編集中クライアントID */
    editClientId = String();

    /** 編集者名 */
    editUserName = String();

    /** createUserNameプロパティ */
    createUserName = String();

    /** updateUserNameプロパティ */
    updateUserName = String();

}

export default RelationLineModel;