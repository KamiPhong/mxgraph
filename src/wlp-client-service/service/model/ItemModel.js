import AttachFileModel from './AttachFileModel';
import ObjectPrototype from 'wlp-client-editor-lib/utils/ObjectPrototype';
import EditorModel from './EditorModel';

class ItemModel extends EditorModel {

    /** クライアントがセットする一意キー */
    uid = String();

    /** アイテムID */
    id = Number();

    /** シートID */
    sheetId = Number();

    /** マップID */
    mapId = Number();

    /** 契約ID */
    contractId = Number();

    /** 企業ID */
    companyId = Number();

    /** ハイパーリンク先サムネイルURI */
    linkUri = String();

    /** テキスト */
    text = String();

    /** アイテム種別 */
    kind = Number();

    /** アイテム生成種別 */
    originKind = Number();

    /** 外部連携種別 */
    exServiceKind = Number();

    /** 座標(x) */
    x = Number();

    /** 座標(y) */
    y = Number();

    /** 形 */
    shape = Number();

    /** デザインプリセット */
    style = Number();

    /** 背景色 */
    backgroundColor = Number();

    /** 線色 */
    lineColor = Number();

    /** textSizeプロパティ */
    textSize = Number()

    /** テキスト色 */
    textColor = Number()

    /** textHorizontalAlignプロパティ */
    textHorizontalAlign = Number()

    /** linkThumbnailUriプロパティ */
    linkThumbnailUri = String();

    /** 外部サービスリンク先URI */
    exServiceLinkUri = String();

    /** 外部サービス先サムネイルURI */
    exServiceThumbnailUri = String();

    /** 添付ファイル */
    attachFile = new AttachFileModel();;

    /** hasAttachFilePasswordプロパティ */
    hasAttachFilePassword = Boolean();

    /** attachFilePasswordUserIdプロパティ */
    attachFilePasswordUserId = Number();

    /** attachFilePasswordUserNameプロパティ */
    attachFilePasswordUserName = String();

    /** originIdプロパティ */
    originId = Number();

    /** 編集中 */
    editing = Boolean();

    /** 編集開始日時 */
    editStartTime = Date();

    /** 編集中クライアントID */
    editClientId = String();

    /** 編集者名 */
    editUserName = String();

    /** 作成者名 */
    createUserName = String();

    /** 作成者ID */
    createUserId = Number();

    /** 作成日時 */
    createTime = Date();

    /** 更新者名 */
    updateUserName = String();

    /** 更新者ID */
    updateUserId = Number();

    /** 更新日時 */
    updateTime = Date();

    /** 関係線のリスト */
    relationLines = Array();

    /** いいねしてるかどうか */
    liked = Boolean();

    /** いいね数 */
    likeCount = Number();

    /** いいねレベル */
    likeLevel = Number();

    constructor(object) {
        super();
        let time = new Date().getTime();
        this.createTime = time;
        this.updateTime = time;
        this.editStartTime = null;
        this.attachFile = null;

        this.__proto__.importFromGraphItem = ObjectPrototype.__proto__.importFromObject

        if (typeof object === 'object') {
            this.importFromGraphItem(object);
        }
    }
}


export default ItemModel;