class ItemLikeModel {
  /** IDプロパティ */
  contractId = Number()

  itemId = Number()

  mapId = Number()

  userId = Number()

  liked = Boolean()

  likeCount = Number()

  likeLevel = Number()
}

export default ItemLikeModel;