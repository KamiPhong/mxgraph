class AttachFileModel {
  /** IDプロパティ */
  id = Number()

  /** URIプロパティ */
  uri = String()

  /** サムネイルURIプロパティ */
  thumbnailUri = String()

  /** nameプロパティ */
  name = String()

  /** filetypeプロパティ */
  filetype = Number()

  /** sizeプロパティ */
  size = Number()

  /** scanResultプロパティ */
  scanResult = Number()
}

export default AttachFileModel;