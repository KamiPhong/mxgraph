import WlpDateUtil from "wlp-client-common/utils/WlpDateUtil";

const dateUtils = new WlpDateUtil();

class SheetModel {
    /** シートID */
    id = Number();

    /** シート名 */
    name = String();

    /** 背景URL */
    backgroundUri = String();

    /** ロックフラグ */
    lockFlag = Boolean();

    /** マップID／テンプレートID */
    parentId = Number();

    /** シート種別 1 = マップ 2 = テンプレート */
    kind = Number();

    /** 契約ID */
    contractId = Number();

    /** 企業ID */
    companyId = Number();

    /** シート順 */
    order = Number();

    /** アイテム群 */
    items = [];

    /** ベースアイテム群 */
    baseItems = [];

    /** snapshotImageUriプロパティ */
    snapshotImageUri = String();

    /** snapshotImageSizeプロパティ */
    snapshotImageSize = Number();

    /** mediumThumbnailUriプロパティ */
    mediumThumbnailUri = String();

    /** mediumThumbnailSizeプロパティ */
    mediumThumbnailSize = Number();

    /** smallThumbnailUriプロパティ */
    smallThumbnailUri = String();

    /** smallThumbnailSizeプロパティ */
    smallThumbnailSize = Number();

    /** 最終編集者名 */
    lastEditUserName = String();

    /** 作成者名 */
    createUserName = String();

    /** 更新者名 */
    updateUserName = String();

    /** 更新日時 */
    updateTime = dateUtils.formatyyyyMMddHHmms(Date());

    /** 作成日時 */
    createTime = dateUtils.formatyyyyMMddHHmms(Date());

    /** 最終編集日時 */
    lastEditTime = dateUtils.formatyyyyMMddHHmms(Date());
}
export default SheetModel;
