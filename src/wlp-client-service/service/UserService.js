import GetUserInfoConfigDto from "wlp-client-service/dto/user/GetUserInfoConfigDto";
import { requestLambdaApi, requestSevletApi } from 'wlp-client-service/service/request';
import ChangeUserInfoConfigDto from "wlp-client-service/dto/user/ChangeUserInfoConfigDto";
import GetAccountStatusConfigDto from "wlp-client-service/dto/user/GetAccountStatusConfigDto";
import CheckDeleteUserAccountConfigDto from "wlp-client-service/dto/user/CheckDeleteUserAccountConfigDto";
import DeleteUserAccountConfigDto from "wlp-client-service/dto/user/DeleteUserAccountConfigDto";
import UpdateAvatarConfigDto from "wlp-client-service/dto/user/UpdateAvatarConfigDto";
import SaveUploadAvatarConfigDto from "wlp-client-service/dto/user/SaveUploadAvatarConfigDto";
import GetJoinTeamInfoConfigDto from "wlp-client-service/dto/user/GetJoinTeamInfoConfigDto";

class UserService {

    getUserInfo = (requestDto = new GetUserInfoConfigDto()) => {    
        const requestRoute = '/personal-setting/user-info';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    changeUserInfo = (requestDto = new ChangeUserInfoConfigDto()) => {
        const requestRoute = '/personal-setting/change-user-info';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    getAccountStatus = (requestDto = new GetAccountStatusConfigDto()) => {
        
    }

    checkDeleteUserAccount = (requestDto = new CheckDeleteUserAccountConfigDto()) => {
        
    }

    deleteUserAccount = (requestDto = new DeleteUserAccountConfigDto()) => {
        
    }

    updateAvatar = (requestDto = new UpdateAvatarConfigDto()) => {
        const requestRoute = '/personal-setting/update-avatar';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    saveUploadAvatar(requestDto = new SaveUploadAvatarConfigDto()) {
        const requestRoute = '/api/user/save-upload-avatar';
        return requestSevletApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    getJoinTeamStatus(requestDto = new GetJoinTeamInfoConfigDto()) {
        const requestRoute = '/personal-setting/account-status';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }
}

export default UserService;