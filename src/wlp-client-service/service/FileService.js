// import request from 'wlp-client-service-core/service/request';
import { fetchServletApi } from 'wlp-client-service/service/request';
import SendExportFileUtils from 'wlp-client-common/utils/SendExportFileUtils';
import SendDownloadFileUtils from 'wlp-client-common/utils/SendDownloadFileUtils';
import FileUploadConfigDto from 'wlp-client-service/dto/item/FileUploadConfigDto';
import { UPLOAD_EXPORT_FILE_PATH_PREFIX, DOWNLOAD_PATH_PREFIX } from 'wlp-client-common/config/EditorConfig';
import AWS from 'aws-sdk';
import {createLoadingMouse} from 'wlp-client-service/service/request';
import InvokeLambda from 'wlp-client-service/consts/InvokeLambda';
let loadingMouse = createLoadingMouse();

const sendExportFileUtils = new SendExportFileUtils();
const sendDownloadFileUtils = new SendDownloadFileUtils();

class FileService {

    sendExportFile = (requestDto = new FileUploadConfigDto()) => {
        sendExportFileUtils.sendForm({
            url: UPLOAD_EXPORT_FILE_PATH_PREFIX,
            method: 'POST',
            data: requestDto
        });
    }

    sendDownloadFile = (requestDto = new FileUploadConfigDto()) => {
        sendDownloadFileUtils.sendForm({
            url: DOWNLOAD_PATH_PREFIX,
            method: 'POST',
            data: requestDto
        });
    }

    fileUploadForm = (formData) => {
        return fetchServletApi('/fileUpload', {
            method: 'POST',
            body: formData
        }).then(response => response.text())
        .then(str => (new window.DOMParser()).parseFromString(str, "text/xml"))
        .then(xmlDoc => {
            let uploadResult = {
                virusCheckResult:  typeof xmlDoc.getElementsByTagName("virusCheckResult")[0] !== "undefined" ? xmlDoc.getElementsByTagName("virusCheckResult")[0].childNodes[0].nodeValue : null,
                fileName: typeof xmlDoc.getElementsByTagName("fileName")[0] !== "undefined" ? xmlDoc.getElementsByTagName("fileName")[0].childNodes[0].nodeValue : null,
                s3Path: typeof xmlDoc.getElementsByTagName("s3Path")[0] !== "undefined" ?  xmlDoc.getElementsByTagName("s3Path")[0].childNodes[0].nodeValue : null,
            }

            if (parseInt(uploadResult.virusCheckResult) === 0 && uploadResult.s3Path) {
                return uploadResult;
            } 

            return false; 
        })
        .catch(e => {
            return false;
        })
    }


    invokeXmlToBase64 = (request) => {
        return new Promise((resolve, reject) => {
            loadingMouse.show();
            
            AWS.config.region = InvokeLambda.REGION; 
            AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId: InvokeLambda.IDENTITY_POOL_ID
            });
            
            let lambda = new AWS.Lambda();
            let params = {
                FunctionName: InvokeLambda.FUNCTION_NAME,
                InvocationType: 'RequestResponse',
                Payload: JSON.stringify({ 
                    path:"/",
                    httpMethod:"POST",
                    body: request,
                    isBase64Encoded: false,
                })
            };
            lambda.invoke(params).promise().then(function(response) {
                if (response.Payload) {
                    resolve(response.Payload);
                } else {
                    reject();
                }
                loadingMouse.hide();
            }).catch(function(error) {
                console.log(error);
                loadingMouse.hide();
                reject();
            });
        });
    }

}

export default FileService;