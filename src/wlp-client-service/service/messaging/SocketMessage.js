class SocketMessage {
    initWithData(data, type) {
        this.data = data;
        this.type = type;
    }
}
export default SocketMessage;