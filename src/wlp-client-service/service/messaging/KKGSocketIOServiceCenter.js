import SocketMessage from './SocketMessage';
import SocketMessageType from './SocketMessageType';
import Envelope from './Envelope';
import KKGSocketIOEnvelopeRouter from './KKGSocketIOEnvelopeRouter';
import KKGSocketIOEnvelopeRearranger from './KKGSocketIOEnvelopeRearranger';
import {
    SOCKET_HOST,
    SOCKET_MAX_RECONNECT_ATTEMP,
    SOCKET_RECONNECTION_DELAY,
} from './Messaging';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
import WlpFault from '../model/WlpFault';
import Logger from 'wlp-client-common/Logger';
import ErrorTypes from 'wlp-client-service/consts/ErrorTypes';
import WlpLanguageUtil from 'wlp-client-common/utils/WlpLanguageUtil';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import { chunkingArray } from 'wlp-client-common/utils/ArrayUtil';
const languageUtil = new WlpLanguageUtil();

(function() {
    let proxied = window.XMLHttpRequest.prototype.send;
    let openProxied = window.XMLHttpRequest.prototype.open;
    window.XMLHttpRequest.prototype.open = function(method, url) {
        // get request url
        this._url = url;
        return openProxied.apply(this, [].slice.call(arguments));
    }
    window.XMLHttpRequest.prototype.send = function() {
        //If request url is socket io then add Accept-Language into request header 
        //and server get Accept-Language to set session localeCode.
        if (this._url.indexOf('socket.io') > -1) {
            this.setRequestHeader("Accept-Language", languageUtil.getCurrentLanguageForXhrHeader());
        }
        return proxied.apply(this, [].slice.call(arguments));
    };
})();

//visual studio code cache newest socket.io client versions
const io = require('socket.io-client/dist/socket.io');
const wlpDateUltil = new WlpDateUtil();

const EnvelopesIngoreOrderSequence = -1;

const EnterMapEventName = "enterMap";
const MapIdStringFormat = "map_";
const MessagingEventName = "sendMessage";


const SheetModelKindMap = 1;
// const SheetModelKindTemplate = 2;

class KKGSocketIOServiceCenter {
    constructor() {
        this.enterMapSocketIO = null;
        this.messageSocketIO = null;
        this.router = new KKGSocketIOEnvelopeRouter();
        this.rearrenger = new KKGSocketIOEnvelopeRearranger();
        this.messageHandlers = {};

    }

    /**
     * init socket
     */
    initWithPort = (mapId, contractId, userId, clientId, clientName, clientThumbnail, senderId) => {
        this.mapId = mapId;
        this.userId = userId;
        this.clientId = LocalStorageUtils.CLIENT_ID;
        this.contractId = contractId;
        this.clientName = clientName;
        this.clientThumbnail = clientThumbnail;
        this.senderId = this.clientId;
    }

    /**
     * connect socket with success and failure callback
     */
    connectWithSuccess = (successCallback, failureCallback) => {

        this.enterMapSocketIO = io.connect(SOCKET_HOST, {
            'force new connection': true
        });
        this.enterMapSocketIO.on('connect', () => {
            this.enterMapWithSuccess(successCallback, failureCallback);
        });
    }

    /**
     * connect socket to map
     */
    enterMapWithSuccess = (successCallback, failureCallback) => {
        let enterMapInfo = {
            map: MapIdStringFormat + this.mapId,
            userId: '' + this.userId,
            clientId: this.clientId
        }
        var jsonData = JSON.stringify(enterMapInfo);

        this.enterMapSocketIO.emit(EnterMapEventName, jsonData);

        this.enterMapSocketIO.on(EnterMapEventName, (message) => {
            // Logger.logConsole("enter map socket successfully");
            this.connectMessageSocketIOWithSuccess(successCallback, failureCallback);
        });

    }

    /**
     * connect socket to receive event from server
     */
    connectMessageSocketIOWithSuccess = (successCallback, failureCallback) => {
        if (this.enterMapSocketIO) {
            this.enterMapSocketIO.disconnect();
        }

        this.enterMapSocketIO = null;

        let namespace = MapIdStringFormat + this.mapId;
        let namespaceUrl = SOCKET_HOST + '/' + namespace + '?query=' + this.clientId;

        this.messageSocketIO = io.connect(namespaceUrl, { 
            'force new connection': true,
            'reconnection delay': SOCKET_RECONNECTION_DELAY,
            'max reconnection attempts': SOCKET_MAX_RECONNECT_ATTEMP,
        });

        this.messageSocketIO.on('connect', () => {
            // Logger.logConsole("connect messsage socket successfully");
            successCallback();
        });

        this.messageSocketIO.on('pushMessage', (jsonData) => {
            // Logger.logConsole("receive push message from server ", jsonData);
            this.routesDelegateWithJSONString(jsonData);
        });
        this.messageSocketIO.on('error', (error) => {
            console.error('socket error: ', error);
        });

        this.messageSocketIO.on('reconnecting', () => {
            const socket = this.messageSocketIO.socket;

            //handle socket disconnected
            if (socket && socket.reconnectionAttempts === SOCKET_MAX_RECONNECT_ATTEMP && navigator.onLine) {
                let fault = new WlpFault();
                fault.messageKey = 'editor.e0002';
                fault.sendedData = null;
                fault.messageType = SocketMessageType.MessageTypeStringError;
                fault.sendFrom = ErrorTypes.CLIENT_ERROR;
                this.onFault(fault);
            }
        });
    }

    /**
     * @param {Envelope} envelope
     */
    messageSocketIOEmitMsg = (envelope) => {
        if(envelope instanceof Envelope){
            const message = envelope.messages[0];
            //check socket connecton before emit message
            if (this.messageSocketIO.socket.connected === false || navigator.onLine === false) {
                let fault = new WlpFault();
                fault.messageKey = 'editor.e0001';
                fault.sendedData = message.data;
                fault.messageType = message.type;
                fault.sendFrom = ErrorTypes.CLIENT_ERROR;
                this.onFault(fault);
            } else {
                const jsonEnvelope = envelope.toJson();
                this.messageSocketIO.emit(MessagingEventName, jsonEnvelope);
            }
        }
    }

    /**
     * @param {WlpFault} fault
     */
    onFault = (fault) => {
        Logger.logConsole('onEmmitFault', fault);
        //post message back to sender with fault flag
        this.router.postMessage(fault.messageType, fault.sendedData, true, fault, this.messageHandlers);
    }

    /**
     * route message
     */
    routesDelegateWithJSONString = (jsonString) => {
        let envelope = JSON.parse(jsonString);
        if (envelope.senderId !== this.senderId) {
            this.router.postEnvelope(envelope, this.messageHandlers);
        } else if (envelope.sequence === EnvelopesIngoreOrderSequence) {
            this.router.postSelfEnvelope(envelope, this.messageHandlers);
        } else {
            this.rearrenger.storeEnvelope(envelope, this.envelopeRearranger);
        }
    }

    envelopeRearranger = (rearranger, callbackEnvelope) => {
        // Logger.logConsole('process evelope after re-arragnered: ', callbackEnvelope);
        this.router.postSelfEnvelope(callbackEnvelope, this.messageHandlers);
    }

    /**
     * set sheet event receiver
     */
    setSheetMessageReciever = (sheetAdded, sheetUpdated, sheetRemoved, sheetLockStateChanged, sheetMoved, sheetCopied) => {
        this.messageHandlers[SocketMessageType.MessageTypeStringSheetAddNew] = sheetAdded;
        this.messageHandlers[SocketMessageType.MessageTypeStringSheetAddByCopy] = sheetCopied;
        this.messageHandlers[SocketMessageType.MessageTypeStringSheetUpdate] = sheetUpdated;
        this.messageHandlers[SocketMessageType.MessageTypeStringSheetRemove] = sheetRemoved;
        this.messageHandlers[SocketMessageType.MessageTypeStringSheetLock] = sheetLockStateChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringSheetUnlock] = sheetLockStateChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringSheetMove] = sheetMoved;
    }

    /**
     * set item event receiver
     */
    setItemMessageReciever = (itemAdded, itemUpdated, itemRemoved, itemLockStateChanged, itemLikeStateChanged, itemAttachFileUploaded, itemAttachFilePasswordChanged) => {
        this.messageHandlers[SocketMessageType.MessageTypeStringItemAddNew] = itemAdded;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemAddByCopy] = itemAdded;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemAddByMemo] = itemAdded;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemUpdate] = itemUpdated;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemRemove] = itemRemoved;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemLock] = itemLockStateChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemUnlock] = itemLockStateChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemLike] = itemLikeStateChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemUploadAttachFile] = itemAttachFileUploaded;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemSetAttachFilePassword] = itemAttachFilePasswordChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringItemClearAttachFilePassword] = itemAttachFilePasswordChanged;
        // this.messageHandlers[SocketMessageType.MessageTypeStringItemLike] = itemLikeStateChanged;
    };

    /**
     * set base item event receiver
     */
    setBaseItemMessageReciever = (baseItemAdded, baseItemUpdated, baseItemRemoved, baseItemLockStateChanged) => {
        this.messageHandlers[SocketMessageType.MessageTypeStringBaseItemAddNew] = baseItemAdded;
        this.messageHandlers[SocketMessageType.MessageTypeStringBaseItemUpdate] = baseItemUpdated;
        this.messageHandlers[SocketMessageType.MessageTypeStringBaseItemRemove] = baseItemRemoved;
        this.messageHandlers[SocketMessageType.MessageTypeStringBaseItemLock] = baseItemLockStateChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringBaseItemUnlock] = baseItemLockStateChanged;
    };

    /**
     * set line event receiver
     */
    setLineMessageReciever = (lineAdded, lineUpdated, lineRemoved, lineLockStateChanged) => {
        this.messageHandlers[SocketMessageType.MessageTypeStringRelationLineAddNew] = lineAdded;
        this.messageHandlers[SocketMessageType.MessageTypeStringRelationLineUpdate] = lineUpdated;
        this.messageHandlers[SocketMessageType.MessageTypeStringRelationLineRemove] = lineRemoved;
        this.messageHandlers[SocketMessageType.MessageTypeStringRelationLineLock] = lineLockStateChanged;
        this.messageHandlers[SocketMessageType.MessageTypeStringRelationLineUnlock] = lineLockStateChanged;
    };

    /**
     * set editor loung event receiver
     */
    setEditorLoungeReciever = (memberJoined, memberLeft, memberList, swithSheet) => {
        this.messageHandlers[SocketMessageType.MessageTypeStringLoungeMemberJoin] = memberJoined;
        this.messageHandlers[SocketMessageType.MessageTypeStringLoungeMemberLeave] = memberLeft;
        this.messageHandlers[SocketMessageType.MessageTypeStringLoungeMemberList] = memberList;
        this.messageHandlers[SocketMessageType.LoungeSwitchSheet] = swithSheet;
    }

    setErrorMessageReciever = (onFault) => {
        this.messageHandlers[SocketMessageType.MessageTypeStringError] = onFault;
    }

    /************************* SEND ITEM BASE MESSAGE **************************************** */
    /**
     * send add new item base message
     */
    sendAddedNewBaseItems = (baseItemModels) => {
        this.sendBaseItems(baseItemModels, SocketMessageType.MessageTypeStringBaseItemAddNew);
    }

    /**
     * send lock item base message
     */
    sendLockedBaseItems = (baseItemModel) => {
        this.sendBaseItems(baseItemModel, SocketMessageType.MessageTypeStringBaseItemLock);
    }

    /**
     * send unlock item base message
     */
    sendUnlockedBaseItems = (baseItemModels) => {
        //[huyvq-bug_ominext_69300]---Case 2 clients select an base item at the same time, base item can be still selected though the editUser(show in lock label) not this current client
        //Then should not send socket unlock_base_item message when this base item is unselected (if msg is sent, it will return back an error message: entity lock)
        if(baseItemModels[0] && baseItemModels[0].editClientId && (baseItemModels[0].editClientId !== this.clientId || baseItemModels[0].preventSendUnlock)) {
            delete baseItemModels[0].preventSendUnlock
            return
        }

        this.sendBaseItems(baseItemModels, SocketMessageType.MessageTypeStringBaseItemUnlock);
    }

    /**
     * send update item base message
     */
    sendUpdatedBaseItems = (baseItemModels) => {
        this.sendBaseItems(baseItemModels, SocketMessageType.MessageTypeStringBaseItemUpdate);
    }

    /**
     * send remove item base message
     */
    sendRemovedBaseItem = (item) => {
        this.sendBaseItems(item, SocketMessageType.MessageTypeStringBaseItemRemove);
    }

    /**
     * send base item message
     */
    sendBaseItems = (baseItems, messageTypeString) => {
        baseItems.forEach(item => {
            item.mapId = this.mapId;
            item.editClientId = this.clientId;
            item.contractId = this.contractId;
            try {
                //TODO: need to check format of editStartTime
                item.editStartTime = wlpDateUltil.formatyyyyMMddHHmms(item.editStartTime);
                item.createTime = wlpDateUltil.formatyyyyMMddHHmms(item.createTime);
                item.updateTime = wlpDateUltil.formatyyyyMMddHHmms(item.updateTime);
                if(Array.isArray(item.relationLines)){
                    item.relationLines.forEach(line => {
                        line.editStartTime = wlpDateUltil.formatyyyyMMddHHmms(line.editStartTime);
                    });
                }

            } catch (ex) {
                //TODO: handle exception here
                console.error("parse date exception ", ex);
            }
        });

        let arrayItems = chunkingArray(baseItems);
        for (let i = 0; i < arrayItems.length; i++) {

            let message = new SocketMessage();
            message.initWithData(arrayItems[i], messageTypeString);

            let envelope = new Envelope();
            envelope.pushMessage(message);
            envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
            envelope.isOnlySheet = true;
            envelope.sync = true;
            envelope.senderId = this.senderId;

            if (this.messageSocketIO != null) {
                // Logger.logConsole("send base item event ", envolopeJson);
                this.messageSocketIOEmitMsg(envelope);
            }
        }
    }

    /************************* SEND ITEM MESSAGE **************************************** */

    /**
     * send added new item message
     */
    sendAddedNewItems = (itemModels) => {
        this.sendItems(itemModels, SocketMessageType.MessageTypeStringItemAddNew);
    }

    /**
     * unused
     */
    sendAddedByCopyItems = (itemModels) => {
        // @throw [NSException exceptionWithName:NSInvalidArgumentException
        //                             reason:@"This method is not available"
        //                             userInfo:nil];
    }

    /**
     * unsed
     */
    sendAddedByMemoItems = (itemModels) => {
        // @throw [NSException exceptionWithName:NSInvalidArgumentException
        //                             reason:@"This method is not available"
        //                             userInfo:nil];
    }

    /**
     * send lock item message
     */
    sendLockedItems = (itemModels) => {
        this.sendItems(itemModels, SocketMessageType.MessageTypeStringItemLock);
    }

    /**
     * send unlock item message
     */
    sendUnlockItems = (itemModels) => {
        //[huyvq-bug_ominext_69300]---Case 2 clients select an item at the same time, item can be still selected though the editUser(show in lock label) not this current client
        //Then should not send socket unlock_base_item message when this item is unselected (if msg is sent, it will return back an error message: entity lock)
        if(itemModels[0] && itemModels[0].editClientId && (itemModels[0].editClientId !== this.clientId || itemModels[0].preventSendUnlock)) {
            delete itemModels[0].preventSendUnlock
            return
        }
        this.sendItems(itemModels, SocketMessageType.MessageTypeStringItemUnlock);
    }

    /**
     * send update item message
     */
    sendUpdatedItems = (itemModels) => {
        this.sendItems(itemModels, SocketMessageType.MessageTypeStringItemUpdate);
    }

    /**
     * send remove item message
     */
    sendRemovedItems = (itemModels) => {
        this.sendItems(itemModels, SocketMessageType.MessageTypeStringItemRemove);
    }

    /**
     * send item like message
     */
    sendItemLikeModel = (itemLikeModel) => {

        itemLikeModel.contractId = this.contractId;
        itemLikeModel.mapId = this.mapId;

        let message = new SocketMessage();
        message.initWithData(itemLikeModel, SocketMessageType.MessageTypeStringItemLike);

        let envelope = new Envelope();
        envelope.pushMessage(message);
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.isOnlySheet = true;
        envelope.sync = true;
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            Logger.logConsole("send base item event ", envelope);
            this.messageSocketIOEmitMsg(envelope);
        }
    }

    
    /**
     * send file attached item message
     */
    sendFileAttachedItem = (item, fileName, s3FileKey, virusScanResult) => {
        Logger.logConsole({item, fileName, s3FileKey, virusScanResult})
        item.mapId = this.mapId;
        item.editClientId = this.clientId;
        item.contractId = this.contractId;

        let message = new SocketMessage();
        message.initWithData([item, fileName, s3FileKey, virusScanResult], SocketMessageType.MessageTypeStringItemUploadAttachFile);

        // let message = new SocketMessage([item, fileName, s3FileKey, virusScanResult], SocketMessageType.MessageTypeStringItemUploadAttachFile);
        let envelope = new Envelope();
        envelope.pushMessage(message);
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.isOnlySheet = true;
        envelope.sync = true;
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            Logger.logConsole("send file attach item event ", envelope);
            this.messageSocketIOEmitMsg(envelope);
        }
    }

    /**
     * send file attached item message
     */
     setAttachFilePassword = (item, password) => {
        Logger.logConsole({item, password})

        item.mapId = this.mapId;
        item.editClientId = this.clientId;
        item.contractId = this.contractId;

        let message = new SocketMessage();
        message.initWithData([item, password], SocketMessageType.MessageTypeStringItemSetAttachFilePassword);

        let envelope = new Envelope();
        envelope.pushMessage(message);
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.isOnlySheet = true;
        envelope.sync = true;
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            Logger.logConsole("setAttachFilePassword ", envelope);
            this.messageSocketIOEmitMsg(envelope);
        }
    }

    clearAttachFilePassword = (item) => {
        Logger.logConsole({item})

        item.mapId = this.mapId;
        item.editClientId = this.clientId;
        item.contractId = this.contractId;

        let message = new SocketMessage();
        message.initWithData([item], SocketMessageType.MessageTypeStringItemClearAttachFilePassword);

        let envelope = new Envelope();
        envelope.pushMessage(message);
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.isOnlySheet = true;
        envelope.sync = true;
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            Logger.logConsole("setAttachFilePassword ", envelope);
            this.messageSocketIOEmitMsg(envelope);
        }
    }
    /**
     * unused
     */
    // sendFileAttachedItem = (item, password) => {
        // @throw [NSException exceptionWithName:NSInvalidArgumentException
        //                             reason:@"This method is not available"
        //                             userInfo:nil];
    // }

    /**
     * unused
     */
    sendPassClearedFileAttachedItem = (item) => {
        // @throw [NSException exceptionWithName:NSInvalidArgumentException
        //                             reason:@"This method is not available"
        //                             userInfo:nil];
    }

    sendItems = (items, messageTypeString) => {
        items.forEach(item => {
            item.mapId = this.mapId;
            item.editClientId = this.clientId;
            item.contractId = this.contractId;
            try {

                //TODO: need to check format of editStartTime (NamNX)
                item.createTime = wlpDateUltil.formatyyyyMMddHHmms(item.createTime);
                item.updateTime = wlpDateUltil.formatyyyyMMddHHmms(item.updateTime);
                item.editStartTime = wlpDateUltil.formatyyyyMMddHHmms(item.editStartTime);
                if(Array.isArray(item.relationLines)){
                    item.relationLines.forEach(line => {
                        line.editStartTime = wlpDateUltil.formatyyyyMMddHHmms(line.editStartTime);
                    });
                }

            } catch (ex) {
                //TODO: handle exception here
                console.error("parse date exception ", ex);
            }
        });
        
        let arrayItems = chunkingArray(items);
        for (let i = 0; i < arrayItems.length; i++) {
            
            let message = new SocketMessage();
            message.initWithData(arrayItems[i], messageTypeString);

            let envelope = new Envelope();
            envelope.pushMessage(message);
            envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
            envelope.isOnlySheet = true;
            envelope.sync = true;
            envelope.senderId = this.senderId;

            if (this.messageSocketIO != null) {
                this.messageSocketIOEmitMsg(envelope);
            }
        }
    }


    /************************* SEND LINE MESSAGE **************************************** */

    /**
     * send add new relation line message
     */
    sendAddedNewRelationLine = (relationLine) => {
        this.sendRelationLine(relationLine, SocketMessageType.MessageTypeStringRelationLineAddNew);
    }

    /**
     * send locked relation line message
     */
    sendLockedRelationLine = (relationLine) => {
        this.sendRelationLine(relationLine, SocketMessageType.MessageTypeStringRelationLineLock);
    }

    /**
     * send unlocked relation line message
     */
    sendUnlockedRelatiolnLine = (relationLine) => {
        //[huyvq-bug_ominext_69300]---Case 2 clients select a line at the same time, line can be still selected though the editUser not this current client
        //Then should not send socket unlock_line message when this line is unselected (if msg is sent, it will return back an error message: entity lock)
        if(relationLine && relationLine.editClientId && (relationLine.editClientId !== this.clientId || relationLine.preventSendUnlock)) {
            delete relationLine.preventSendUnlock
            return
        }
        this.sendRelationLine(relationLine, SocketMessageType.MessageTypeStringRelationLineUnlock);
    }

    sendUpdatedRelationLine = (relationLine) => {
        this.sendRelationLine(relationLine, SocketMessageType.MessageTypeStringRelationLineUpdate);
    }

    /**
     * send remove relation line message
     */
    sendRemovedRelationLine = (relationLine) => {
        this.sendRelationLine(relationLine, SocketMessageType.MessageTypeStringRelationLineRemove);
    }

    /**
     * send relation line messsage
     */
    sendRelationLine = (line, messageTypeString) => {
        line.mapId = this.mapId;
        line.editClientId = this.clientId;
        line.contractId = this.contractId;

        line.editStartTime = wlpDateUltil.formatyyyyMMddHHmms(line.editStartTime);

        let message = new SocketMessage();
        message.initWithData(line, messageTypeString);

        let envelope = new Envelope();
        envelope.pushMessage(message);
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.isOnlySheet = true;
        envelope.sync = true;
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            // Logger.logConsole("send set relation line event ", envelope);
            this.messageSocketIOEmitMsg(envelope);
        }
    }


    /************************* SEND SHEET MESSAGE **************************************** */

    sendEventForSheet = (user, eventName) => {
        let message = new SocketMessage();
        message.initWithData(user, eventName);

        let envelope = new Envelope(this.clientId);
        envelope.isOnlySheet = true;
        envelope.sync = false;
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.pushMessage(message);
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            // Logger.logConsole("send switch sheet event by user ", envolopeJson);
            this.messageSocketIOEmitMsg(envelope);
        }
    }

    sendSheetSwitchedByUser = (sheetId) => {
        let userModel = {
            userId: this.userId,
            name: this.clientName,
            thumbnailUri: this.clientThumbnail,
            showSheetId: sheetId,
            clientId: this.clientId
        };
        this.sendEventForSheet(userModel, SocketMessageType.LoungeSwitchSheet);
    }

    sendSheetJoinedByUser = (sheetId) => {
        let userModel = {
            userId: this.userId,
            name: this.clientName,
            showSheetId: sheetId,
        };

        this.sendEventForSheet(userModel, SocketMessageType.MessageTypeStringLoungeMemberJoin);
    }

    /**
     * send add new sheet message
     */
    sendAddedNewSheet = (sheet) => {
        this.sendSheet(sheet, SocketMessageType.MessageTypeStringSheetAddNew);
    }

    /**
     * send copy sheet msg
     */
    sendAddedByCopySheet = (sheet) => {
        sheet.kind = SheetModelKindMap;
        sheet.lastEditTime = wlpDateUltil.formatyyyyMMddHHmms(sheet.lastEditTime);
        sheet.createTime = wlpDateUltil.formatyyyyMMddHHmms(sheet.createTime);
        sheet.updateTime = wlpDateUltil.formatyyyyMMddHHmms(sheet.updateTime);

        sheet.items = null;
        sheet.baseItems = null;
        let message = new SocketMessage();
        message.initWithData(sheet, SocketMessageType.MessageTypeStringSheetAddByCopy);
        

        let envelope = new Envelope();
        envelope.isOnlySheet = false;
        envelope.sync = true;
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.pushMessage(message);
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            Logger.logConsole("send sheet event ", envelope);
            this.messageSocketIOEmitMsg(envelope);
        }
    }


    /**
     * send lock sheet message
     *  @param sheet model
     */
    sendLockedSheet = (sheet) => {
        this.sendSheet(sheet, SocketMessageType.MessageTypeStringSheetLock);
    }

    /**
     * send unlock sheet ,essage
     * @param sheet model
     */
    sendUnlockedSheet = (sheet) => {
        this.sendSheet(sheet, SocketMessageType.MessageTypeStringSheetUnlock);
    }

    /**
     * send update sheet message
     *  @param sheet model
     */
    sendUpdatedSheet = (sheet) => {
        this.sendSheet(sheet, SocketMessageType.MessageTypeStringSheetUpdate);
    }

    /**
     * send remove sheet message
     *  @param sheet model
     */
    sendRemovedSheet = (sheet) => {
        this.sendSheet(sheet, SocketMessageType.MessageTypeStringSheetRemove);
    }

    /**
     * send moved sheet message
     *  @param sheet model
     */
    sendMovedSheet = (sheet) => {
        this.sendSheet(sheet, SocketMessageType.MessageTypeStringSheetMove);
    }

    /**
     * send sheet message
     *  @param sheet model
     */
    sendSheet = (sheet, messageTypeString) => {
        Logger.logConsole("sheet", sheet);
        sheet.contractId = this.contractId;
        sheet.parentId = this.mapId;
        sheet.kind = SheetModelKindMap;
        // try {

        sheet.lastEditTime = wlpDateUltil.format(sheet.lastEditTime, "yyyy/MM/dd HH:mm:s", false);

        sheet.createTime = null;

        sheet.updateTime =  wlpDateUltil.format(sheet.updateTime, "yyyy/MM/dd HH:mm:s", false);

        // } catch (ex) {
        //     //TODO: handle exception here
        //     console.error("parse date exception ", ex);
        // }

        sheet.items = null;
        sheet.baseItems = null;
        let message = new SocketMessage();
        message.initWithData(sheet, messageTypeString);

        let envelope = new Envelope();
        envelope.isOnlySheet = false;
        envelope.sync = true;
        envelope.setSequence(this.rearrenger.sendEnvelopeSequence++);
        envelope.pushMessage(message);
        envelope.senderId = this.senderId;

        if (this.messageSocketIO != null) {
            Logger.logConsole("send sheet event ", envelope);
            this.messageSocketIOEmitMsg(envelope);
        }
    }

    /**
     * destroy socket
     */
    destroy() {

        if (this.enterMapSocketIO != null) {
            this.enterMapSocketIO.disconnect();
            this.enterMapSocketIO = null;
        }
        if (this.messageSocketIO != null) {
            this.messageSocketIO.disconnect();
            this.messageSocketIO = null;
        }

        this.router = null;
        this.rearrenger = null;
        this.messageHandlers = {};
    }
    
    isDate = (input) => {
        if (Object.prototype.toString.call(input) === "[object Date]") {
            return true;
        }
        return false;
    };

    convertDateTimeToString = (datetimeObj) => {
        let date = datetimeObj.getDate();
        date = date < 10 ? '0' + date : date
        let month = datetimeObj.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        let year = datetimeObj.getFullYear();

        let hour = datetimeObj.getHours();
        hour = hour < 10 ? '0' + hour : hour;

        let minute = datetimeObj.getMinutes();
        minute = minute < 10 ? '0' + minute : minute;

        let seconds = datetimeObj.getSeconds();
        seconds = seconds < 10 ? '0' + seconds : seconds;

        let timeStr = year + '/' + month + '/' + date + ' ' + hour + ':' + minute + ':' + seconds;

        return timeStr;
    }
}

export default KKGSocketIOServiceCenter;