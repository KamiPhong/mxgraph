import KKGSocketIOServiceCenter from './KKGSocketIOServiceCenter';
import SocketMessageType from './SocketMessageType';

const SOCKET_MAX_RECONNECT_ATTEMP = 5;
const SOCKET_RECONNECTION_DELAY = 500;
const SOCKET_HOST = process.env.REACT_APP_SOCKET_HOST;


const MESSAGE_TYPE_SEQUENCE = [
    SocketMessageType.LoungeSwitchSheet,
    SocketMessageType.ItemLock,
    SocketMessageType.ItemUnlock,
    SocketMessageType.ItemAddNew,
];

export {
    KKGSocketIOServiceCenter,
    SocketMessageType,
    SOCKET_HOST,
    MESSAGE_TYPE_SEQUENCE,
    SOCKET_MAX_RECONNECT_ATTEMP,
    SOCKET_RECONNECTION_DELAY
}