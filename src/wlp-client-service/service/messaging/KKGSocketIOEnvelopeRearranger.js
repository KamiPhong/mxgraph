import Logger from "wlp-client-common/Logger";

const StoredEnvelopesMaximumCapacity = 3;

class KKGSocketIOEnvelopeRearranger {
    constructor() {
        this.storedEnvelopes = [];
        this.receivedEnvelopeSequence = 0;
        this.sendEnvelopeSequence = 0;
    }
    
    storeEnvelope = (envelope, envelopeRearranger) => {
        // Logger.logConsole("envelope sequence:", envelope.sequence, this.receivedEnvelopeSequence);
        if (envelope.sequence === this.receivedEnvelopeSequence) {
            //callback
            envelopeRearranger(this, envelope);
            this.receivedEnvelopeSequence++;

            if (this.storedEnvelopes.length > 0) {
                //sort envelopes
                this.storedEnvelopes =  this.storedEnvelopes.sort((env1, env2) => env1.sequence - env2.sequence);
                // Logger.logConsole("sorted envelope: ", this.storeEnvelope);
                const storedEnvelopesCount = this.storedEnvelopes.length;

                for (let i = storedEnvelopesCount - 1; i >= 0; i --) {
                    const envelope = this.storedEnvelopes[i];
                    let sequence = envelope.sequence;
                    if (this.receivedEnvelopeSequence === sequence) {
                        envelopeRearranger(this, envelope);
                        this.receivedEnvelopeSequence++;
                    } else if (this.receivedEnvelopeSequence < sequence) {
                        Logger.logConsole("some messages before are not received: envelop sequence: ", sequence, ", current rearranger sequence: ", this.receivedEnvelopeSequence);
                        this.storedEnvelopes = this.storedEnvelopes.slice(0, i);
                        break;
                    }
                }

                if (this.storedEnvelopes.length === storedEnvelopesCount) { 
                    this.storedEnvelopes = [];
                }
            }
        } else {
            this.storedEnvelopes.push(envelope);
            if (this.storedEnvelopes.length > StoredEnvelopesMaximumCapacity) {
                this.storedEnvelopes = this.storedEnvelopes.sort((env1, env2) => env1.sequence - env2.sequence);
                for (let storedEnvelope in this.storedEnvelopes) {
                    this.receivedEnvelopeSequence = storedEnvelope.sequence;
                    envelopeRearranger(this, storedEnvelope);
                }
                this.receivedEnvelopeSequence++;
                this.storedEnvelopes = [];
            }
        }
    }
}
export default KKGSocketIOEnvelopeRearranger;