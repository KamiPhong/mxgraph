
export default {
    MessageTypeStringBaseItemAddNew: "baseItem_addNew",
    MessageTypeStringBaseItemLock: "baseItem_lock",
    MessageTypeStringBaseItemUnlock: "baseItem_unlock",
    MessageTypeStringBaseItemUpdate: "baseItem_update",
    MessageTypeStringBaseItemRemove: "baseItem_remove",
    
    MessageTypeStringItemAddNew : "item_addNew",
    MessageTypeStringItemAddByCopy: "item_addByCopy",
    MessageTypeStringItemAddByMemo: "item_addByMemo",
    MessageTypeStringItemLock: "item_lock",
    MessageTypeStringItemUnlock: "item_unlock",
    MessageTypeStringItemUpdate: 'item_update',
    MessageTypeStringItemRemove: "item_remove",
    MessageTypeStringItemLike: "item_like",

    MessageTypeStringItemUploadAttachFile: "item_uploadAttachFile",
    MessageTypeStringItemSetAttachFilePassword: "item_setAttachFilePassword",
    MessageTypeStringItemClearAttachFilePassword: "item_clearAttachFilePassword",

    MessageTypeStringRelationLineAddNew: "relationLine_addNew",
    MessageTypeStringRelationLineLock: "relationLine_lock",
    MessageTypeStringRelationLineUnlock: "relationLine_unlock",
    MessageTypeStringRelationLineUpdate: "relationLine_update",
    MessageTypeStringRelationLineRemove: "relationLine_remove",

    MessageTypeStringLoungeMemberList: "lounge_member_list",
    LoungeSwitchSheet: "lounge_switch_sheet",
    MessageTypeStringLoungeMemberJoin: "lounge_member_join",
    MessageTypeStringLoungeMemberLeave: "lounge_member_leave",

    MessageTypeStringSheetAddNew: "sheet_addNew",
    MessageTypeStringSheetAddByCopy: "sheet_addByCopy",
    MessageTypeStringSheetLock: "sheet_lock",
    MessageTypeStringSheetUnlock: "sheet_unlock",
    MessageTypeStringSheetUpdate: "sheet_update",
    //SheetUpdate: "sheet_update",
    MessageTypeStringSheetRemove: "sheet_remove",
    MessageTypeStringSheetMove: "sheet_move",

    MessageTypeStringError: "error"
}


