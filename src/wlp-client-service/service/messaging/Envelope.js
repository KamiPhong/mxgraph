import { MESSAGE_TYPE_SEQUENCE } from './Messaging';
import WlpDateUtil from 'wlp-client-common/utils/WlpDateUtil';
// import moment from 'moment';

const dateUtil = new WlpDateUtil();
class Envelope {
    sequence = 0;
    isOnlySheet = true;
    sync = true;
    senderId = "";
    messages = [];

    constructor(senderId) {
        if (typeof senderId !== 'undefined') {
            this.senderId = senderId;
        }

    }
    
    toJson = () => {
        return JSON.stringify(this);
    }

    initWithMessages(messages, sequence, isOnlySheet, sync, senderId) {
        this.messages = messages;
        this.sequence = sequence;
        this.isOnlySheet = isOnlySheet;
        this.sync = sync;
        this.senderId = senderId;
    }

    setSequence = (sequence) => {
        this.sequence = sequence;
    }

    setSequenceIndex = (messageType) => {
        const index = MESSAGE_TYPE_SEQUENCE.indexOf(messageType);
        if (typeof index !== 'undefined') {
            this.sequence = index;
        }
    }

    pushMessage = (messages) => {
        this.messages.push(messages);
    }

    setMessageType = (messageType) => {
        if (typeof messageType !== 'string') {
            return;
        }
        if (this.messages.length < 1) {
            let message = {
                type: messageType,
                data: []
            };
            this.messages.push(message);
            this.setSequenceIndex(messageType);
        }
    }

    setMessageData = (data) => {
        if (this.messages.length < 1) {
            return;
        }

        if (Array.isArray(data) !== true) {
            data = [data];
        }
        this.messages[0].data = data;
        this.validateData();
    }

    validateData = () => {
        if (this.messages[0].data.length > 0) {
            const nullValidation = ['attachFilePasswordUserId', 'originId', 'backgroundColor', 'lineColor', 'textColor'];
            const dateValidation = ['createTime', 'updateTime', 'lastEditTime', 'editStartTime'];
            const arrayCollectionValidation = ['items', 'baseItems'];

            this.messages[0].data.forEach((row) => {
                for (const property in row) {
                    if (nullValidation.indexOf(property) > -1 && row[property] === null) {
                        row[property] = 0;
                    }

                    if (dateValidation.indexOf(property) > -1 && row[property] !== null) {
                        row[property] = dateUtil.format(row[property], 'yyyy/MM/dd HH:mm:s');
                    }
                    if (arrayCollectionValidation.indexOf(property) > -1 && row[property] !== null) {
                        row[property] = null;
                    }
                }
            });
        }
    }
}

export default Envelope;