import SocketMessageType from "./SocketMessageType";
import ObjectPrototype from 'wlp-client-editor-lib/utils/ObjectPrototype';
import ErrorTypes from 'wlp-client-service/consts/ErrorTypes';
import Logger from "wlp-client-common/Logger";
import WlpFault from 'wlp-client-service/service/model/WlpFault';
class KKGSocketIOEnvelopeRouter {
    constructor() {
        ObjectPrototype.appendPrototype(this);
    }

    postEnvelope(envelope, messageHandlers) {
        let message = envelope.messages[0];
        let messageTypeString = message.type;
        let sheetAddedMessageTypeStrings = [SocketMessageType.MessageTypeStringSheetAddNew, SocketMessageType.MessageTypeStringSheetAddByCopy];
        //TODO: handle error
        /**
         * NamNX comment: need to check if socket messages as same as flash messages
         * some messsage may be implement on flash but not in ios
         */
        if (messageTypeString === SocketMessageType.MessageTypeStringBaseItemAddNew) {
            Logger.logConsole('base item msg - add new base item: ', message);
            // self.baseItemDelegate socketIODidAddNewBaseItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringBaseItemLock) {
            Logger.logConsole('base item msg - lock base item: ', message);
            // self.baseItemDelegate socketIODidLockBaseItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringBaseItemUnlock) {
            Logger.logConsole('base item msg - unlock base item: ', message);
            // self.baseItemDelegate socketIODidUnlockBaseItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringBaseItemUpdate) {
            Logger.logConsole('base item msg - update base item: ', message);
            // self.baseItemDelegate socketIODidUpdateBaseItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringBaseItemRemove) {
            Logger.logConsole('base item msg - remove base item with ids: ', message);
            // self.baseItemDelegate socketIODidRemoveBaseItemWithIds:message.data;
        } else if (messageTypeString === SocketMessageType.ItemAddNew) {
            Logger.logConsole('item msg - add new item: ', message);
            // self.itemDelegate socketIODidAddNewItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemLock) {
            Logger.logConsole('item msg - lock item: ', message);
            // self.itemDelegate socketIODidLockItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemUnlock) {
            Logger.logConsole('item msg - unlock item: ', message);
            // self.itemDelegate socketIODidUnlockItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemUpdate) {
            Logger.logConsole('item msg - udpate item: ', message);
            // self.itemDelegate socketIODidUpdateItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemRemove) {
            Logger.logConsole('item msg - remove item with ids: ', message);
            // self.itemDelegate socketIODidRemoveItemWithIds:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemLike) {
            Logger.logConsole('item msg - change item like model: ', message);
            // self.itemDelegate socketIODidChangeItemLikeModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemUploadAttachFile) {
            Logger.logConsole('item msg - upload attach file with model: ', message);
            // self.itemDelegate socketIODidUploadAttachFileWithModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemSetAttachFilePassword) {
            Logger.logConsole('item msg - set attach file password with model: ', message);
            // self.itemDelegate socketIODidSetAttachFilePasswordWithModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemClearAttachFilePassword) {
            Logger.logConsole('item msg - clear attach file password with model: ', message);
            // self.itemDelegate socketIODidClearAttachFilePasswordWithModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringRelationLineAddNew) {
            Logger.logConsole('relation line msg - add new relation line: ', message);
            // self.relationLineDelegate socketIODidAddNewRelationLine:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringRelationLineLock) {
            // Logger.logConsole('relation line msg - lock relation line: ', message);
            // self.relationLineDelegate socketIODidLockRelationLine:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringRelationLineUnlock) {
            // Logger.logConsole('relation line msg - unlock relation line: ', message);
            // self.relationLineDelegate socketIODidUnlockRelationLine:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringRelationLineUpdate) {
            // Logger.logConsole('relation line msg - update relation line: ', message);
            // self.relationLineDelegate socketIODidUpdateRelationLine:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringRelationLineRemove) {
            Logger.logConsole('relation line msg - remove relation line: ', message);
            // self.relationLineDelegate socketIODidRemoveRelationLine:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringLoungeMemberJoin) {
            // Logger.logConsole('lounge msg - join lounge user: ', message);
            // self.loungeDelegete socketIODidJoinLoungeUser:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringLoungeMemberLeave) {
            Logger.logConsole('lounge msg - leave lounge user: ', message);
            // self.loungeDelegete socketIODidLeaveLoungeUser:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringLoungeMemberList) {
            Logger.logConsole('lounge msg - list lounge user: ', message);
            // self.loungeDelegete socketIODidLeaveLoungeUser:message.data;
        } else if (sheetAddedMessageTypeStrings.includes(messageTypeString)) {
            Logger.logConsole('sheet msg - add new sheet: ', message);
            // self.sheetDelegete socketIODidAddNewSheet:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetLock) {
            Logger.logConsole('sheet msg - lock sheet: ', message);
            // self.sheetDelegete socketIODidLockSheet:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetUnlock) {
            Logger.logConsole('sheet msg - unlock sheet: ', message);
            // self.sheetDelegete socketIODidUnlockSheet:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetUpdate) {
            Logger.logConsole('sheet msg - update sheet: ', message);
            // self.sheetDelegete socketIODidUpdateSheet:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetRemove) {
            Logger.logConsole('sheet msg - remove sheet: ', message);
            // self.sheetDelegete socketIODidRemoveSheetWithSheets:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetMove) {
            Logger.logConsole('sheet msg - move sheet: ', message);
            // self.sheetDelegete socketIODidMoveSheetWithSheetIds:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringError) {
            Logger.logConsole('error msg - wlp fault: ', message);
            let fault = new WlpFault();
            fault.messageKey = message.data.messageKey;
            fault.sendedData = message.data;
            fault.messageType = message.type;
            fault.sendFrom = ErrorTypes.SERVER_ERROR;

            this.postMessage(messageTypeString, message.data, false, fault, messageHandlers);
            return;
            // self.errorDelegate socketIODidReceiveWlpFault:message.data;
        } else {
            // Logger.logConsole('no process for this message ', messageTypeString);
        }


        this.postMessage(messageTypeString, message.data, false, false, messageHandlers);
    }

    postSelfEnvelope(envelope, messageHandlers) {

        if (typeof envelope.messages === 'undefined') {
            return;
        }
        //TODO: handle error
        let message = envelope.messages[0];
        let messageTypeString = message.type;

        if (messageTypeString === SocketMessageType.MessageTypeStringItemAddNew) {
            Logger.logConsole('self item msg - attach item id for new items: ', message);
            // self.itemDelegate socketIODidAttachItemIdForNewItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemLike) {
            Logger.logConsole('self item msg - change item like model: ', message);
            // self.itemDelegate socketIODidChangeItemLikeModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemUploadAttachFile) {
            Logger.logConsole('self item msg - upload attach file with model: ', message);
            // self.itemDelegate socketIODidUploadAttachFileWithModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemUpdate) {
            Logger.logConsole('self item msg - update items: ', message);
            // self.itemDelegate socketIODidUpdateItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringBaseItemAddNew) {
            Logger.logConsole('self base item msg - attach base item ids for new base items: ', message);
            // self.baseItemDelegate socketIODidAttachBaseItemIdForNewBaseItems:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringBaseItemUpdate) {
            Logger.logConsole('self base item msg - attach base item ids for new base items: ', message);
            // self.baseItemDelegate socketIODidUpdateBaseItems:message.data;
        }  else if (messageTypeString === SocketMessageType.MessageTypeStringItemSetAttachFilePassword) {
            Logger.logConsole('self item msg - set attach file password with model: ', message);
            // self.itemDelegate socketIODidSetAttachFilePasswordWithModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringItemClearAttachFilePassword) {
            Logger.logConsole('self item msg - clear attach file password with model: ', message);
            // self.itemDelegate socketIODidClearAttachFilePasswordWithModel:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringRelationLineAddNew) {
            // Logger.logConsole('self relation line msg - attach id for new line: ', message);
            // self.relationLineDelegate socketIODidAttatchIdForNewLine:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringRelationLineUpdate) {
            Logger.logConsole('self relation line msg - update relation line: ', message);
            // self.relationLineDelegate socketIODidUpdateRelationLine:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetAddNew) {
            Logger.logConsole('self sheet msg - attach sheet id for new sheet: ', message);
            // self.sheetDelegete socketIODidAttatchSheetIdForNewSheet:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetRemove) {
            // 自分からシート削除した時も他のデバイスからメッセージが来た時と同様の処理を行う
            Logger.logConsole('self sheet msg - remove sheet with sheets: ', message);
            // self.sheetDelegete socketIODidRemoveSheetWithSheets:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetLock) {
            // 自分からシートロックした時も他のデバイスからメッセージが来た時と同様の処理を行う
            Logger.logConsole('self sheet msg - lock sheet: ', message);
            // self.sheetDelegete socketIODidLockSheet:message.data;
        } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetUnlock) {
            // 自分からシートアンロックした時も他のデバイスからメッセージが来た時と同様の処理を行う
            Logger.logConsole('self sheet msg - unlock sheet: ', message);
            // self.sheetDelegete socketIODidUnlockSheet:message.data;
        } 
        
        if (messageTypeString === SocketMessageType.MessageTypeStringError) {
            Logger.logConsole('self error msg - receive wlp fault: ', message);
            // self.errorDelegate socketIODidReceiveWlpFault:message.data;
            // this.postMessage(messageTypeString, message.data, true, true, messageHandlers);
            let fault = new WlpFault();
            fault.messageKey = message.data.messageKey;
            fault.sendedData = message.data;
            fault.messageType = message.type;
            fault.sendFrom = ErrorTypes.SERVER_ERROR;

            this.postMessage(messageTypeString, message.data, false, fault, messageHandlers);
        } else {
            this.postMessage(messageTypeString, message.data, true, false, messageHandlers);
        }
        
        
    }
    
    /**
     * post message to callback 
     * @param {*} messageTypeString 
     * @param {*} data 
     * @param {*} isMine 
     * @param {*} isFault 
     * @param {*} messageHandlers 
     */
    postMessage(messageTypeString, data, isMine, isFault, messageHandlers) {
        const callback = messageHandlers[messageTypeString];
        if (callback != null && typeof (callback) === 'function') {
            if (messageTypeString === SocketMessageType.MessageTypeStringSheetLock) {
                callback(data, true, isMine, isFault);
            } else if (messageTypeString === SocketMessageType.MessageTypeStringSheetUnlock) {
                callback(data, false, isMine, isFault);
            } else {
                callback(data, isMine, isFault);
            }
        }
    }
}
export default KKGSocketIOEnvelopeRouter;