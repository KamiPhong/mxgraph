import GetTemplateListConfigDto from "wlp-client-service/dto/template/GetTemplateListConfigDto";
import { requestLambdaApi } from 'wlp-client-service/service/request';
import GetCategoryListConfigDto from "wlp-client-service/dto/template/GetCategoryListConfigDto";
import CreateTemplateConfigDto from "wlp-client-service/dto/template/CreateTemplateConfigDto";
import GetTemplateRecordDetailConfigDto from "wlp-client-service/dto/template/GetTemplateRecordDetailConfigDto";
import ChangeInfoTemplateConfigDto from "wlp-client-service/dto/template/ChangeInfoTemplateConfigDto";
import DeleteTemplateConfigDto from "wlp-client-service/dto/template/DeleteTemplateConfigDto";

class TemplateService {
    /**
     * 
     * @param {GetTemplateListConfigDto} paramDto 
     * @return {GetTemplateListDto}
     */
    getTemplateList = (paramDto = new GetTemplateListConfigDto()) => {
        //TODO: fill url with real get template list url
        return requestLambdaApi({
            url: '/template/template-list',
            method: 'POST',
            data: paramDto
        })
    }

    getTemplateRecordDetail = (requestDto = new GetTemplateRecordDetailConfigDto()) => {
        const requestRoute = '/template/template-detail';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    // changeBookmark= (paramDto = new ChangeBookmarkConfigDto()) => {

    // }

    createTemplate = (paramDto = new CreateTemplateConfigDto()) => {
        return requestLambdaApi({
            url: '/template/create-template',
            method: 'POST',
            data: paramDto
        })
    }

    getCategoryList = (paramDto = new GetCategoryListConfigDto()) => {
        return requestLambdaApi({
            url: '/template/category-list',
            method: 'POST',
            data: paramDto
        })
    }

    deleteTemplate = (requestDto = new DeleteTemplateConfigDto()) => {
        const requestRoute = '/template/delete-template';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    changeInfoTemplate = (requestDto = new ChangeInfoTemplateConfigDto()) => {
        const requestRoute = '/template/change-info';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto,
            onErrorMessage: 'system.fatalerror'
        })
    }

}

export default TemplateService;