import { requestLambdaApi } from 'wlp-client-service/service/request';

class PasswordService {
    resetPassword = (ResetPasswordConfigDto) => {
        const resetPasswordAPI = '/auth/reset-password';
        return requestLambdaApi({
            url: resetPasswordAPI,
            method: 'POST',
            data: ResetPasswordConfigDto
        })
    }

    changePassword = (changePassword) => {
        const changePasswordAPI = '/auth/change-password';
        return requestLambdaApi({
            url: changePasswordAPI,
            method: 'POST',
            data: changePassword
        })
    }

    loginChangePassword = async (requestDto) => {
        const loginChangePasswordAPI = '/auth/login-change-password';
        return requestLambdaApi({
            url: loginChangePasswordAPI,
            method: 'POST',
            data: requestDto,
            showErrorDialog: false
        }) 
    }
    // getExServiceConnectInfo() {
    //
    // }
}

export default PasswordService;
