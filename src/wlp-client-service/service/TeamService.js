import { requestLambdaApi } from 'wlp-client-service/service/request';
import GetTeamAccountStatusConfigDto from 'wlp-client-service/dto/team/GetTeamAccountStatusConfigDto';
import AddMultipleMembersConfigDto from 'wlp-client-service/dto/team/AddMultipleMembersConfigDto';
import GetMemberListConfigDto from 'wlp-client-service/dto/user/GetMemberListConfigDto';
import AddMembersConfigDto from 'wlp-client-service/dto/user/AddMembersConfigDto';
import ChangeMemberRoleConfigDto from 'wlp-client-service/dto/user/ChangeMemberRoleConfigDto';
import GetMemberMapListConfigDto from 'wlp-client-service/dto/team/GetMemberMapListConfigDto';
import DeleteMemberConfigDto from 'wlp-client-service/dto/user/DeleteMemberConfigDto';
import ChangeTeamInfoConfigDto from 'wlp-client-service/dto/user/ChangeTeamInfoConfigDto';
import GetTeamInfoConfigDto from 'wlp-client-service/dto/user/GetTeamInfoConfigDto';
import GetAddMembersInfoConfigDto from 'wlp-client-service/dto/team/GetAddMembersInfoConfigDto';

class TeamService {

    getTeamInfo(requestDto = new GetTeamInfoConfigDto()) {
        const requestRoute = '/team/team-info';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    changeTeamInfo(requestDto = new ChangeTeamInfoConfigDto()) {
        const requestRoute = '/team/change-team-info';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto,
            showErrorDialog: false
        })
    }

    getTeamAccountStatus(requestDto = new GetTeamAccountStatusConfigDto()) {
        const requestRoute = '/team/account-status';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    deleteMember(requestDto = new DeleteMemberConfigDto()) {
        const requestRoute = '/team/delete-member';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    getMemberMapList(requestDto = new GetMemberMapListConfigDto()) {
        const requestRoute = '/team/member-map-list';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    changeMemberRole(requestDto = new ChangeMemberRoleConfigDto()) {
        const requestRoute = '/team/change-member-role';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    /**
     * 
     * @param {GetAddMembersInfoConfigDto} requestDto 
     * @return {GetAddMembersInfoDto}
     */
    getAddMembersInfo(requestDto = new GetAddMembersInfoConfigDto()) {
        const requestRoute = '/team/members-info';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    addMembers(requestDto = new AddMembersConfigDto()) {
        const requestRoute = '/team/add-members';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }

    getMemberList(requestDto = new GetMemberListConfigDto()) {
        const requestRoute = '/team/member-list';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
        
    }

    addMultipleMembers(requestDto = new AddMultipleMembersConfigDto()) {
        const requestRoute = '/team/add-multiple-members';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: requestDto
        })
    }
}

export default TeamService;
