import { requestLambdaApi } from 'wlp-client-service/service/request';
import LogonConfigDto from 'wlp-client-service/dto/auth/LogonConfigDto';
import MypageRequestDto from 'wlp-client-service/dto/MypageRequestDto';
import { deleteCookieJSESSIONID } from 'wlp-client-common/utils/CookiesUtil';
import CheckLoggedOnConfigDto from 'wlp-client-service/dto/auth/CheckLoggedOnConfigDto';

class AuthService {
    logon = (logonConfigDto = new LogonConfigDto()) => {
        const logonAPI = '/auth/logon';
        return requestLambdaApi({
            url: logonAPI,
            method: 'POST',
            data: logonConfigDto,
            showErrorDialog: false
        })
    }

    checkLoggedOn = (paramDto = new CheckLoggedOnConfigDto()) => {
        return requestLambdaApi({
            url: '/auth/check-logged-on',
            method: 'POST',
            data: paramDto,
            showErrorDialog: false
        });
    }

    logoff = (paramDto = new MypageRequestDto()) => {
        return requestLambdaApi({
            url: '/auth/logoff',
            method: 'POST',
            data: paramDto
        }).then(() => {
            deleteCookieJSESSIONID();
        })
    }
}

export default AuthService;