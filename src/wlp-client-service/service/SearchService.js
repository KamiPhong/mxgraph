import SearchConfigDto from "wlp-client-service/dto/search/SearchConfigDto";
import { requestLambdaApi } from 'wlp-client-service/service/request';

class SearchService {
	SearchService() {
		
	}
	
	/**
	 * 名詞を抽出してAタグ化して返します。
	 * 
	 * 結果：ConvertNounToLinkDto
	 */
	convertNounToLink() {
		
	}
	
	/**
	 * 各先頭３項目を取得して返します。
	 * 
	 * 結果：LeadingSearchResultsDto
	 */
	findLeading = (paramDto = new SearchConfigDto()) => {
		const requestRoute = '/search/find-leading';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
		})
	}
	
	/**
	 * 続くアイテムの検索結果を取得します。
	 * 
	 * 結果：SearchResultDto
	 */
	findFollowingItems(paramDto = new SearchConfigDto()) {
		const requestRoute = '/search/find-following-items';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
		})
	}
	
	/**
	 * 続くシートの検索結果を取得します。
	 * 
	 * 結果：SearchResultDto
	 */
	findFollowingSheets(paramDto = new SearchConfigDto()) {
		const requestRoute = '/search/find-following-sheets';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
		})
	}
	
	/**
	 * 続くマップの検索結果を取得します。
	 * 
	 * 結果：SearchResultDto
	 */
	findFollowingMaps(paramDto = new SearchConfigDto()) {
		const requestRoute = '/search/find-following-maps';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
		})
	}
	
	/**
	 * エディタ内でのアイテム検索
	 * 
	 * 結果：SearchResultDto
	 */
	findItemsInEditor = (paramDto = new SearchConfigDto()) => {
		const requestRoute = '/search/find-items-in-editor';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
            data: paramDto
		})
	}
	
	/**
	 * コピー先マップ検索
	 * 
	 * 結果：SearchResultDto
	 */
	findCopyDestinationMaps(paramDto = new SearchConfigDto()) {
		const requestRoute = '/search/find-copy-destination-maps';
        return requestLambdaApi({
            url: requestRoute,
            method: 'POST',
			data: paramDto,
			showErrorDialog: false
		})
	}
}

export default SearchService;
