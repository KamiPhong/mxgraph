import { showWarningDialog, showErrorDialog } from "wlp-client-common/utils/DialogUtils";

function exceptionHandler(error, isShowWarning) {
    if (typeof (error) !== 'object') {
        return;
    }

    let message = error.message;

    let reloadCall = () => {
        if (error.reloadFlag === true) {
            window.location.reload();
        }
    }

    let onClose = () => {
        if (error.reloadFlag === true) {
            window.location.reload();
        }
    }

    if (isShowWarning) {
        if (error.reloadFlag === true) {
            showErrorDialog(error.messageKey || message, { showCancel: false, onOKCallback: reloadCall });
        } else {
            showWarningDialog(error.messageKey || message, { showCancel: false, onOKCallback: reloadCall, onClose });
        }
    }
}

export default exceptionHandler;