export default {
    all: 'all',
    ownerByMe: 'ownerByMe',
    ownerByOther: 'ownerByOther',
    newMaps: 'newMaps',
    updateMaps: 'updateMaps',
    bookmarks: 'bookmarks',
    recent: 'recent'
}