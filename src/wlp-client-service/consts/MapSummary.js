export default {
    all: 'allNum',
    bookmaks: 'bookmarksNum',
    new: 'newMapsNum',
    ownByMe: 'ownerByMeNum',
    ownByOther: 'ownerByOtherNum'
}