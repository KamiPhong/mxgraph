import React from 'react';
import httpErrorCode from 'wlp-client-common/consts/httpErrorCode';
import LocalStorageUtils from 'wlp-client-common/utils/LocalStorageUtils';
import { jsonToQueryString } from 'wlp-client-common/utils/TextUtils';
import ErrorDto from 'wlp-client-service/dto/ErrorDto';
import ClientService from 'wlp-client-service/service/ClientService';

class ErrorBoundary extends React.Component {
    readyState = 4;
    
    constructor(){
        super();
        this.state = {
            error: '',
            errorInfo: '',
            hasError: false,
        }

        const openProxied = window.XMLHttpRequest.prototype.open;
        const _this = this;
        window.XMLHttpRequest.prototype.open = function(method, url) {
            if (!this._proxied) {
                this.addEventListener('loadend', _this.handleXHRRequest);
                this.addEventListener('error', _this.handleXHRRequest);
            }

            this._url = url;
            this._proxied = true;
            return openProxied.apply(this, [].slice.call(arguments));
        }
    }
    
    /**
     * 
     * @param {ProgressEvent} xhrResponse 
     */
    handleXHRRequest = (xhrResponse) =>{
        const target = xhrResponse.target;
        if (target instanceof XMLHttpRequest) {
            if (
                target._url && target._url.indexOf('api/logging') === -1 &&
                target.readyState === 4 && (target.status === httpErrorCode.BAD_REQUEST || 
                target.status === httpErrorCode.NOT_FOUND || 
                target.status === httpErrorCode.REQUEST_TIMEOUT||
                target.status === httpErrorCode.INTERNAL_SERVER_ERROR||
                target.status === httpErrorCode.BAD_GATEWAY ||
                target.status === httpErrorCode.GATEWAY_TIMEOUT)
    
            ) {
                this.sendErrorInfoToServer(JSON.stringify(target.response));
            }
        }
    }

    handleWindowError = (error) => {
        if (error instanceof ErrorEvent) {
            this.sendErrorInfoToServer(error.message);
        }
    }

    sendErrorInfoToServer = async (messageError) => {
        const localStorageUtils = new LocalStorageUtils();
        const clientService = new ClientService();
        const userInfo = localStorageUtils.getUserSession();
        const userAction = localStorageUtils.getUserAction();

        let errorData = new ErrorDto();
        if (userInfo) {
            errorData.email = userInfo.mailaddress;
        }
        errorData.actionStack = userAction.toString();
        errorData.errorInfo = messageError;
        errorData.excTime = Date.now().toString();
        
        let queryStringLog = jsonToQueryString(errorData)

        if (navigator.onLine) {
            await clientService.sendLogError(queryStringLog);
        }
    }

    componentDidMount() {
        window.onerror = this.handleErrorEnter;
     }

     handleErrorEnter = (message, source, lineno, colno, error) => {
        this.sendErrorInfoToServer(message);
    }

    static getDerivedStateFromError(error) {
        return { hasError: true, error };
    }

    componentDidCatch(error, errorInfo) {
        this.setState({ errorInfo });
    }

    render() {
        const { hasError } = this.state;
        if (hasError) {
            return (
                <div style={{
                    marginBottom: '3rem',
                    "position": "relative",
                    "display": "flex",
                    "flexDirection": "column",
                    "minWidth": "0",
                    "wordWrap": "break-word",
                    "backgroundColor": "#fff",
                    "backgroundClip": "border-box",
                }}>
                    <div style={{
                        "padding": "0.75rem 1.25rem",
                        "marginBottom": "0",
                        "backgroundColor": "rgba(0, 0, 0, 0.03)",
                        "borderBottom": "1px solid rgba(0, 0, 0, 0.125)"
                    }}>
                        <p>
                            There was an error in loading this page.{' '}
                            <span
                                style={{ cursor: 'pointer', color: '#0077FF' }}
                                onClick={() => {
                                    window.location.reload();
                                }}>
                                Reload this page
                            </span>{' '}
                        </p>
                    </div>
                </div>
            );
        }
        
        return this.props.children;
    }
}
export default ErrorBoundary;