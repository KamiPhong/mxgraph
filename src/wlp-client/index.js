//supoort ie11
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/ie9';
import 'react-app-polyfill/stable';

import React from 'react';
import ReactDOM from 'react-dom';
import Main from './Main';
import * as serviceWorker from '../serviceWorker';
import i18n from '../i18n';

i18n.on('languageChanged', (currLang) => {
    if (typeof currLang === 'string') {
        const langCode = currLang.slice(0, 2);
        const metaLangElm = document.getElementById('meta_lang');
        if (metaLangElm) {
            metaLangElm.setAttribute('content', langCode);
        }
        document.documentElement.lang = langCode;
    }
});

ReactDOM.render(<Main />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
