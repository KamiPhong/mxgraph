import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import { EDITOR_PATH_PREFIX, UPLOAD_EXPORT_FILE_PATH_PREFIX, DISABLE_UPLOAD_EXPORT_FILE_PATH_PREFIX } from 'wlp-client-common/config/EditorConfig';
import { normalizeUrl } from 'wlp-client-common/utils/UrlUtil';
import PrealoadDisplay from 'wlp-client-common/component/PreloadDisplay';
import { isProductEnv } from 'wlp-client-editor-lib/utils/Utils';
import './reset.css';
import './main.css';
import 'wlp-client-editor/assets/css/bootstrap.css';
import AuthRoute from 'wlp-client-common/component/AuthRoute';
import ErrorBoundary from './ErrorBoundary';

const LoginPanel = React.lazy(() => import('wlp-client-common/component/LoginPanel'));
const MypageMain = React.lazy(() => import('wlp-client-mypage/Main'));
const EditorMain = React.lazy(() => import('wlp-client-editor/Main'));
const DownloadPage = React.lazy(() => import('wlp-client-editor/DownloadPage'));
const UploadExportPage = React.lazy(() => import('wlp-client-editor/UploadExportPage'));
const pathUploadExportFile = isProductEnv() ? DISABLE_UPLOAD_EXPORT_FILE_PATH_PREFIX : UPLOAD_EXPORT_FILE_PATH_PREFIX

export default class Main extends Component {
    render() {

        return (<React.Suspense fallback={<PrealoadDisplay recallFlag={true} />}>
            <ErrorBoundary>
                <Router>
                    <Switch>
                        <AuthRoute path={normalizeUrl('/mypage')} component={MypageMain} />
                        <AuthRoute path={normalizeUrl(EDITOR_PATH_PREFIX, ':mapId/:sheetId?/:itemId?')} component={EditorMain} />
                        <AuthRoute path={normalizeUrl('/download')} component={DownloadPage} />
                        <AuthRoute path={normalizeUrl(pathUploadExportFile)} component={UploadExportPage} />
                        <AuthRoute path={'/login/:next?'} component={LoginPanel} />
                        <Route path={'/'}><Redirect to={'/mypage'} /></Route>
                    </Switch>
                </Router>
            </ErrorBoundary>
        </React.Suspense>)
    }
}
