
# [Step by step to setup development enviroment](#1)
> OS: windows 10
> Text editor: Visual studio code
> NodeJS: 12.x.x

1. Make a copy of *.env.example* file and named it *.env.local*
2. Set appropriate value for: 
   > REACT_APP_SOCKET_HOST --> socket server for /editor
   > REACT_APP_API_HOST  --> domain of servlet APIs
   > REACT_APP_REMOTE_HOST  --> domain of lambda APIs
   > PORT  --> this variable can be remote without risk
3. Open Powershell, cd to project root folder and run following commands by sequence
   `npm install`
   `npm run dev`

# [Project folders structure](#2)

> Each screen build on two folder
> Ex: **/editor** is include wlp-client-editor and wlp-client-editor-lib
> **wlp-client-common** is used by all screen

```
.
├── README.md  --> you are here
├── config  --> webpack config for development and production build
├── package.json --> defind dependences and npm commands
├── .env.example  --> defind necessary enviroment variable
├── public  --> template and public assests
└── src/
    ├── i18n.js  --> config file for react-i18next (multiple language support)
    ├── setupProxy.js  --> proxy config (by pass CORS xhr request)
    ├── wlp-client  --> routing
    │   ├── ErrorBoundary.js  --> handle error and uncaught exceptions
    │   ├── Main.js  --> router config
    ├── wlp-client-common  --> common components, utilities and assests for /editor and /mypage
    ├── wlp-client-editor  --> root folder of /editor screen
    │   ├── Main.js  --> editor main
    ├── wlp-client-editor-lib  --> logic code for /editor
    │   ├── components --> React components and template
    │   ├── core  --> mxGraph is implemented here
    │   ├── events  --> defind custom events used in /editor
    ├── wlp-client-mypage  --> root folder of /mypage screen
    ├── wlp-client-mypage-lib  --> logic code for /mypage
    └── wlp-client-service  --> middleware to  call APIs
        ├── exceptionHandler.js  --> exception intercepter
        └── service  --> all APIs services definded here
```

